var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;
var ROOT_CATEGORY_ID = 134;
if(typeof(CSP_Root_Category)!="undefined"&&CSP_Root_Category!=""&&!isNaN(CSP_Root_Category)) 
	ROOT_CATEGORY_ID = CSP_Root_Category;
var PRODUCT_CATEGORY_ID = 134;
var WHAT_NEW_ITEM_LIMIT = 30;
var SYNDICATION_TYPE = 'intranet';
var COOKIES_INFO = {
	username: 'username',
	password: 'pwd'
}
var EMBEDED_IMG_COUNT = 6;
var MISSED_EMBEDED_IMG = [];
var STANDARD_PARTNER_TYPES = new Array("Reseller", "MSP", "Distributor");

function reAdjustColorBoxSize(minWidth, maxWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 970)
		wdWidth = 700;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if (maxWidth == undefined) maxWidth=0;
	if (maxWidth > 0 && wdWidth > maxWidth) {
		wdWidth = maxWidth;
	}
	
	if (wdWidth > jQuery(window).width())
		wdWidth = jQuery(window).width();
	if (wdHeight > jQuery(window).height())
		wdHeight = jQuery(window).height();
		
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

function initJCarousel(targetLink, wrapperNode) {
	if (jQuery(window).width() > 480) {
		if (targetLink.hasClass('jcarouseled')) {
			wrapperNode.find('.jcarousel').jcarousel('destroy');
			wrapperNode.find('li').attr('style', '');
			if (wrapperNode.find('.fake-asset-item').length == 0 && wrapperNode.find('li').length % 2 == 1)
				jQuery('#trunk-wrapper .fake-asset-item').insertAfter(wrapperNode.find('li:last'));
			targetLink.removeClass('jcarouseled');
		}
		wrapperNode.find('div.csp-item-asset').removeClass('jcarousel');
		return;
	}
	
	wrapperNode.find('div.csp-item-asset').addClass('jcarousel');
	wrapperNode.find('.fake-asset-item').remove();
	var jcarousel = wrapperNode.find('.jcarousel');
	if (!targetLink.hasClass('jcarouseled')) {
		jcarousel
		.on('jcarousel:reload jcarousel:create', function () {
			var width = jQuery(window).width();//jcarousel.innerWidth();

			if (width >= 450) {
				width = width / 3;
			} else {
				width = width / 2;
			}
			width = width - 36;
			jcarousel.jcarousel('items').css('width', width + 'px');
		})
		.jcarousel({
			wrap: 'circular'
		});

		wrapperNode.find('.jcarousel-pagination')
		.on('jcarouselpagination:active', 'a', function() {
			jQuery(this).addClass('active');
		})
		.on('jcarouselpagination:inactive', 'a', function() {
			jQuery(this).removeClass('active');
		})
		.on('click', function(e) {
			e.preventDefault();
		})
		.jcarouselPagination({
			perPage: 1,
			item: function(page) {
				return '<a href="#' + page + '">' + page + '</a>';
			}
		});
		jcarousel.on( "swipeleft", function() { jcarousel.jcarousel('scroll', '+=1'); } );
		jcarousel.on( "swiperight", function() { jcarousel.jcarousel('scroll', '-=1'); } );
		targetLink.addClass('jcarouseled');
	}
	else {
		jcarousel.jcarousel('reload');
	}
}

function InitEmbededBanners(thisEl) {
	CURRENT_CSP_UNIQUE_KEY = thisEl.attr('cspuniquekey');
	reAdjustColorBoxSize(550);
	var thumbnailList = ApplyEmbededThumbnails(thisEl.siblings('.thumbnail-holder').text().split(','));
	
	var embededScriptSrc = thumbnailList.length > 0 ? jQuery('#embedscript-placeholder').text() : jQuery('#embededscript-withoutbanner').text();
	embededScriptSrc = embededScriptSrc.replace('[keyword]', CURRENT_CSP_UNIQUE_KEY);
	if (thumbnailList.length > 0) {
		var defaultThumbnail = thisEl.siblings('.default-thumbnail-holder').text()
		var liTemplate = '<li><div class="center-align"><img class="embeded-thumb selected" rel="Content_Image_Thumbnail" src="' + defaultThumbnail + '" /></div></li>';
		/*
		if (ProjectId != "michelin") {
			jQuery('#banner-carousel').find('ul').prepend(liTemplate);
			embededScriptSrc = embededScriptSrc.replace('[embeded_thumbnail]', 'Content_Image_Thumbnail');
		}
		else {
			liTemplate = '<li><div class="center-align"><img class="embeded-thumb" rel="Content_Image_Thumbnail" src="' + defaultThumbnail + '" /></div></li>';
			jQuery('#banner-carousel').find('ul').append(liTemplate);
		}
		*/
		liTemplate = '<li><div class="center-align"><img class="embeded-thumb" rel="Content_Image_Thumbnail" src="' + defaultThumbnail + '" /></div></li>';
		jQuery('#banner-carousel').find('ul').append(liTemplate);
	}
	var scriptText = jQuery('#csp-generated-code-placeholder').val();
	scriptText = scriptText.replace('[embeded_script]', embededScriptSrc);
	jQuery('#csp-generated-code').val(scriptText);
	
	jQuery.colorbox({
		className: 'csp-asset-embed-detail', 
		width: wdWidth + "px",
		inline: true, 
		href: '#csp-asset-embed-holder', 
		onComplete: function() {
			var bannerCarousel = jQuery('#banner-carousel');
			bannerCarousel.jcarousel('destroy');
			bannerCarousel
			.on('jcarousel:reload jcarousel:create', function () {
				var width = jQuery('#csp-asset-embed-holder').width();
				bannerCarousel.jcarousel('items').css('width', width + 'px');
			})
			.jcarousel({
				wrap: 'circular'
			});
			bannerCarousel.jcarousel('scroll', 0);
			bannerCarousel.find('.jcarousel-pagination')
			.on('jcarouselpagination:active', 'a', function() {
				var currentIndex = bannerCarousel.find('.jcarousel-pagination a').index(jQuery(this));
				var activeItem = bannerCarousel.find(' > ul > li:eq(' + currentIndex + ')');
				var crrHeight = activeItem.find(' > div.center-align').outerHeight();
				activeItem.find('img.embeded-thumb').click();
				bannerCarousel.height(crrHeight);
				jQuery(this).addClass('active');
			})
			.on('jcarouselpagination:inactive', 'a', function() {
				jQuery(this).removeClass('active');
			})
			.on('click', function(e) {
				e.preventDefault();
			})
			.jcarouselPagination({
				perPage: 1,
				item: function(page) {
					return '<a href="#' + page + '">' + page + '</a>';
				}
			});
			ColorboxResizeInterval = setInterval(function(){ 
				jQuery.colorbox.resize(); 
				var width = jQuery('#csp-asset-embed-holder').width();
				jQuery('#banner-carousel').jcarousel('items').css('width', width + 'px');
			}, 1000);
		} ,
		onClose: function() {
			clearInterval(ColorboxResizeInterval);
		}
	});
}

function ApplyEmbededThumbnails(tList) {
	jQuery('#banner-carousel').find('ul li').remove();
	var isEmpty = true;
	for (var i = 0; i < tList.length; i++) {
		if (tList[i] != '') {
			isEmpty = false;
			var liTemplate = '<li><div class="center-align"><img class="embeded-thumb" rel="Embeded_Thumbnail_Image_' + (i + 1) + '" src="' + tList[i] + '" /></div></li>';
			jQuery('#banner-carousel').find('ul').append(liTemplate);
		}
	}
	if (isEmpty)
		tList = new Array();
	return tList;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

function sortNumber(a, b) {
    return a - b;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function AssetSecureCheck(cid, cbtype) {
	//Check if cookies disabled
	/*
	var cookieEnabled=(navigator.cookieEnabled)? true : false   
    if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){ 
        document.cookie="testcookie";
        cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false;
    }
	if (!cookieEnabled) {
		alert('Please enable your browser cookies');
		return;
	}
	//DNN_Portal_Id = '0';
	if (DNN_Portal_Id == '' || isNaN(DNN_Portal_Id)) {
		alert('Invalid DNN Portal Id');
		return;
	}
	
	if (getCookie(COOKIES_INFO.username) == '') {
		reAdjustColorBoxSize(500, 580);
		jQuery('#csp-auth-first-cid').val(cid);
		jQuery('#csp-auth-first-cbtype').val(cbtype);
		jQuery.colorbox({ className: 'asset-auth-form', width: wdWidth + "px", height: "300px", inline: true, href: jQuery('#csp-login-form-holder-inner')});
	}
	else 
	{
		jQuery.post(("dnnservices/api/user/UserAuthenticate"), { username: getCookie(COOKIES_INFO.username), password: window.btoa(getCookie(COOKIES_INFO.password)), portalid: DNN_Portal_Id, instance: ProjectId, contentid: cid, fieldname: "Content_Link_Url" }, function (response) {
			TreatAssetLink(response.ContentValue, cbtype);
        });
	}
	*/
	reAdjustColorBoxSize(500, 580);
	jQuery('#csp-auth-first-cid').val(cid);
	jQuery('#csp-auth-first-cbtype').val(cbtype);
	jQuery.colorbox({ className: 'asset-auth-form', width: wdWidth + "px", height: "300px", inline: true, href: jQuery('#csp-login-form-holder-inner')});
}

function CspAuthSubmit() {
	jQuery.post(("dnnservices/api/user/UserAuthenticate"), { username: jQuery('#csp-user').val(), password: jQuery('#csp-password').val(), portalid: DNN_Portal_Id, instance: ProjectId, contentid: jQuery('#csp-auth-first-cid').val(), fieldname: "Content_Link_Url" }, function (response) {
		if (response.User == null) {
			alert(response.StatusMessage);
			return;
		}
		var exdays = 1;
		//setCookie(COOKIES_INFO.username, jQuery('#csp-user').val(), exdays);
		//setCookie(COOKIES_INFO.password, window.atob(jQuery('#csp-password').val()), exdays);
		//TreatAssetLink(response.ContentValue, jQuery('#csp-auth-first-cbtype').val());
		TriggerLinkByLoadType(response.ContentValue, jQuery('#csp-auth-first-cid').val());
		jQuery.colorbox.close();
	});
}

function TreatAssetLink (assetLink, cbtype) {
	if (assetLink == '') return;
	if (cbtype.toLowerCase()=='cobrand_html')
		assetLink = "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(assetLink);
		//assetLink = "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(assetLink);
	//window.location = assetLink;
	TriggerLinkByLoadType(assetLink, jQuery('#csp-auth-first-cid').val());
}

function TriggerLinkByLoadType(targetLink, cid) {
	var targetElement = jQuery('[cid="' + cid + '"]:first');
	if (targetElement.length == 0) return;
	
	var launchTypeClass = targetElement.attr('class') != undefined ? targetElement.attr('class') : '';
	//var launchTypeClass = targetElement.attr('ng-class') != undefined ? targetElement.attr('ng-class') : '';
	
	if (targetElement.attr('cobrandtype').toLowerCase() == 'cobrand_html')
		targetLink = "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(targetLink);
		//targetLink = "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(targetLink);
	if (launchTypeClass.indexOf('newwindow') != -1)
		window.open(targetLink);
	else if (launchTypeClass.indexOf('lightbox') != -1) {
		reAdjustColorBoxSize(650);
		var videoid = targetLink.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
		if(videoid != null) {
			jQuery.colorbox({width: wdWidth + "px", height: wdHeight + "px", iframe:true, href: 'http://www.youtube.com/embed/' + videoid[1]});
		} else { 
			jQuery.colorbox({width: wdWidth + "px", height: wdHeight + "px", iframe:true, href: targetLink});
		}
	}
	else
		window.location = targetLink;
}

var intranetAssetModule = angular.module('ngIntranetAssets', ['ngSanitize']).config(function($routeProvider, $locationProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/category/:categorykey', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/category/:categorykey/i/:tick', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/partner-type/:partnertype', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/partner-type/:partnertype/i/:tick', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey/i/:tick', {controller:MainCtrl, templateUrl:'ui/assets/custom/Shared/main.html'}).
		when('/search/:searchkey', {controller:SearchCtrl, templateUrl:'ui/assets/custom/Shared/search.html'}).
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i >= 0)
					return path.substring(i + 9);
			} catch(e) {}
			return '/';
		} });
});

intranetAssetModule.directive('myBlockResize', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			var maxHeight = 0;
			var blocksWrapper = jQuery(angular.element(element)).parent('ul');
			setTimeout(function() {
				blocksWrapper.find('li').each(function() {
					if (jQuery(this).outerHeight() > maxHeight)
						maxHeight = jQuery(this).outerHeight();
				});
				blocksWrapper.find('li').each(function() {
					jQuery(this).outerHeight(maxHeight);
				});
			}, 1500);
		}
	};
});

intranetAssetModule.directive('onLastBlock', function() {
	return function(scope, element, attrs) {
		if (scope.$last) setTimeout(function(){
			scope.$emit('onLastBlockCallback', element, attrs);
		}, 1);
	};
});

intranetAssetModule.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

intranetAssetModule.factory('Searcher', function ($rootScope) {
	var dataCategories = [];
	var dataAssets = [];
	var resultCategories = [];
	var resultAssets =[];
	
	return {
		allCategories: function () {
			return dataCategories;
		},
		allAssets: function () {
			return dataAssets;
		},
		setCategories: function (dataList) {
			dataCategories = [];
			if ($rootScope.CurrentPartnerType == '')
				dataCategories = dataList;
			else {
				for (var i = 0; i < dataList.length; i++) {
					var catId = parseInt(dataList[i].categoryId);
					if ($rootScope.LocalPartnerTypesCategories[$rootScope.CurrentPartnerType].indexOf(catId) >= 0)
						dataCategories.push(dataList[i]);
				}
			}
		},
		setAssets: function (dataList) {
			dataAssets = [];
			if ($rootScope.CurrentPartnerType == '')
				dataAssets = dataList;
			else {
				var addedAsset = []
				for (var i = 0; i < dataList.length; i++) {
					if ($rootScope.LocalPartnerTypesAssets[$rootScope.CurrentPartnerType].indexOf(dataList[i].contentid) >= 0 && addedAsset.indexOf(dataList[i].contentid) < 0) {
						dataAssets.push(dataList[i]);
						addedAsset.push(dataList[i].contentid);
					}
				}
			}
		},
		getAssetResult: function () {
			return resultAssets;
		},
		getCategoryResult: function () {
			return resultCategories;
		},
		doSearch: function (keyWord) {
			resultCategories = [];
			resultAssets = [];
			var lowerKeyWord = keyWord.toLowerCase();
			for (var k = 0; k < dataAssets.length; k++) {
				if (dataAssets[k].title.toLowerCase().indexOf(lowerKeyWord) >= 0 || dataAssets[k].descriptionlong.toLowerCase().indexOf(lowerKeyWord) >= 0)
					resultAssets.push(dataAssets[k]);
			}
			for (var i = 0; i < dataCategories.length; i++) {
				var isFound = false;
				if (dataCategories[i].title.toLowerCase().indexOf(lowerKeyWord) >= 0 || dataCategories[i].description.toLowerCase().indexOf(lowerKeyWord) >= 0)
					isFound = true;
				if (isFound)
					resultCategories.push(dataCategories[i]);
			};
		}
	}
});

intranetAssetModule.factory('TranslationData', function ($http) {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		}
	}
});

intranetAssetModule.factory('Translation', function () {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
});

intranetAssetModule.factory('Products', function ($rootScope, $http) {
	var rootId = ROOT_CATEGORY_ID;
	var categoryProductId = PRODUCT_CATEGORY_ID;
	var data = [];
	return {
		promise: {},
		rootid: function () {
			return rootId;
		},
		mainproductid: function () {
			return categoryProductId;
		},
		all: function () {
			return data;
		},
		setData: function (productList) {
			//data = productList;
			var resultData = $rootScope.ParseCategory(productList);
			data = JSON.parse(resultData);
		},
		convertJSONtoTree: function (arry) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(arry);
			//var fullArray = arry;
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));
				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			
			return roots[0].children;
		},
		filterByPartner: function (entry) {
			var isValid = false;
			var entryId = 0;
			// ltu 01/27/14: sometimes entry.length == 0 causing the filter to break;
			if (entry == undefined) return;
			
			if (typeof(entry.value) == "undefined")
				entryId = parseInt(entry.categoryId);
			else
				entryId = parseInt(entry.value.categoryId);

			if ($rootScope.CurrentPartnerType == '') {
				for (var j = 0; j < $rootScope.LocalPartnerTypes.length; j++) {
					if ($rootScope.LocalPartnerTypesCategories[$rootScope.LocalPartnerTypes[j]].indexOf(entryId) >= 0) {
						return true;
					}
				}
			}
			else if ($rootScope.LocalPartnerTypesCategories[$rootScope.CurrentPartnerType].indexOf(entryId) >= 0)
				return true;
			
			if (entry.children != undefined) {
				for (var i = 0; i < entry.children.length; i++) {
					isValid = this.filterByPartner(entry.children[i]);
					if (isValid) {
						return true;
					}
				}
			}
			
			return isValid;
		}
	}
});

intranetAssetModule.factory('Asset', function ($http, $rootScope) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (assetList) {
			//data = assetList;
			var resultData = $rootScope.ParseAsset(assetList);// JSON.stringify(eval(parseAsset(resultData)));
			//var myJsonString = JSON.stringify(yourArray);
			//resultData = JSON.stringify(eval(resultData));
			data = JSON.parse(resultData);
			//$rootScope.RemoveDuplicatedAssets(data);
		}
	};
});

intranetAssetModule.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

intranetAssetModule.filter('filterByCategory', function () {
    return function (items, categoryId) {
        var newItems = [];
        for (var i = 0; i < items.length; i++) {
			var cspCatIds = items[i].cspcategoryid.split(',');
			for (var key = 0; key < cspCatIds.length; key++) {
				if (parseInt(cspCatIds[key]) == parseInt(categoryId)) {
					newItems.push(items[i]);
				}
			}
        };
		
        return newItems;
    }
});

intranetAssetModule.factory('CSPLanguage', function () {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		}
	}
});

intranetAssetModule.directive('eatClick', function() {
	return function(scope, element, attrs) {
		jQuery(element).click(function(event) {
			event.preventDefault();
		});
	}
});

intranetAssetModule.run(function ($rootScope, $q, $http, $filter, Translation, Products, Asset, LeadgenForm, Searcher, CSPLanguage) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
		var temp = currentUrl.split('/');
		for (var i = 0; i < temp.length; i++) {
			if (temp[i] == 'i') {
				currentTick = '&i=' + temp[i + 1];
				break;
			}
		}
	}
	
	$rootScope.pageTitle = 'Home';
	$rootScope.AllProducts = [];
	$rootScope.NotAssetProducts = [];
	$rootScope.LocalPartnerTypes = [];
	$rootScope.LocalPartnerTypeStat = [];
	$rootScope.LocalPartnerTypesCategories = new Array();
	$rootScope.LocalPartnerTypesAssets = [];
	$rootScope.AllTabsAndNavigation = [];
	$rootScope.Dictionary = [];
	$rootScope.rootProductId = Products.mainproductid();
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.AjaxLoading = true;
	$rootScope.CurrentPartnerType = '';
	$rootScope.CurrentCategoryKey = '';
	$rootScope.CurrentCategoryItem = [];
	$rootScope.FeaturedAssets = [];
	$rootScope.ConsumerPartnerId = ConsumerPartnerId;
	$rootScope.ProjectId = ProjectId;
	$rootScope.IsSearch = false;
	$rootScope.SearchResultCategory = [];
	$rootScope.SearchResultAsset = [];
	$rootScope.SearchResultShowLimit = 3;
	$rootScope.DisableAssetDownload = false; 
	if (typeof(CSPDisableAssetDownload) != 'undefined')
		$rootScope.DisableAssetDownload = CSPDisableAssetDownload.toLowerCase() == 'yes';
	$rootScope.EnableAssetSyndication = EnableAssetSyndication.toLowerCase() == 'yes';
	$rootScope.EnableLanguageSelection = CSPEnableLanguageSelection.toLowerCase() == 'yes';
	$rootScope.LanguageList = [];
	$rootScope.CurrentCspLang = {
		Id: '',
		Code: '',
		Desc: ''
	};
	$rootScope.GlobalLabel = {
		privacypolicy: 'privacypolicy',
		termsofuse: 'termsofuse',
		privacypolicylink: 'privacypolicylink',
		termsofuselink: 'termsofuselink',
		LearnMore: 'learnmore',
		EmbedThisAsset: 'Embed Asset',
		Back: 'Back',
		CopyCodeToClipboard: 'Copy Code',
		CodeCopied: 'Code copied',
		BannerSelect: 'pleaseselectabanner'
	};
	$rootScope.AssetSyndicationUrl = document.location.protocol + "//" + document.location.host + "/Csp/?mfrname="+ProjectId+"&t=AssetSyndication&keyword=[keyword]&banner=[embeded_thumbnail]";
	$rootScope.AssetSyndicationUrlWithoutBanner = document.location.protocol + "//" + document.location.host + "/Csp/?mfrname="+ProjectId+"&t=AssetSyndication&keyword=[keyword]";
	$rootScope.byPartnerType = function (entry) {
		if ($rootScope.CurrentPartnerType == '') return true;
		var isInPartnerList = Products.filterByPartner(entry);
		return isInPartnerList;
	};
	
	$rootScope.DoSearch = function (eventTarget) {
		var minLength = 3;
		var searchKey = eventTarget.currentTarget.value;
		$rootScope.SearchResultCategory = [];
		$rootScope.SearchResultAsset = [];
		if (searchKey.length < minLength) return;
		
		Searcher.doSearch(searchKey);
		$rootScope.SearchResultCategory = Searcher.getCategoryResult();
		$rootScope.SearchResultAsset = Searcher.getAssetResult();
		setTimeout(function() {
			jQuery("#csp-search-result-list a.result-title").highlight(searchKey);
		}, 700);
		//console.log(Searcher.getAssetResult(), Searcher.getCategoryResult());
	};
	
	$rootScope.GetTranslation = function (text) {
		return Translation.translate (Translation.all(), text);
	};
	
	$rootScope.GetPartnerTypeLink = function (pType) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/partner-type/' + pType;
	};
	
	$rootScope.HidePartnerType = function (pType) {
		if (ConsumerPartnerTypeList.length > 0) {
			if (ConsumerPartnerTypeList.indexOf(pType) < 0)
				return true;
			return false;
		}
		
		if (ConsumerPartnerType == '' || ConsumerPartnerType.toLowerCase() == 'all')
			return false;
		
		if (pType.toLowerCase() != ConsumerPartnerType.toLowerCase())
			return true;
			
		return false;
	};
	
	$rootScope.GetFeaturedAssets = function (assetList, validCategories) {
		$rootScope.FeaturedAssets = $filter('filter')(assetList, {featured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual.toLowerCase();});

		for (var i = $rootScope.FeaturedAssets.length - 1; i >= 0; i--) {
			var isValid = false;
			if (typeof($rootScope.FeaturedAssets[i].cspcategoryid)!="undefined") {
				var catIds = $rootScope.FeaturedAssets[i].cspcategoryid.split(',');
				
				//Check if new asset item is in valid category list
				for (var key = 0; key < catIds.length; key++) {
					catIds[key] = parseInt(catIds[key]);
					for (var rKey = 0; rKey < validCategories.length; rKey++) {
						if (parseInt(validCategories[rKey].categoryId) == catIds[key] && $rootScope.FeaturedAssets[i].localpartnertype.indexOf($rootScope.CurrentPartnerType) >= 0) {
							isValid = true;
							break;
						}
					}
				}
			}
			if (!isValid) {
				$rootScope.FeaturedAssets.splice(i, 1);
				continue;
			}
		};
	};
	
	$rootScope.ValidAssetKeys = function (assetItem) {
		if (assetItem.launchtype.toLowerCase() == 'noembedded') return false;
		var tmpKeys = assetItem.cspuniquekey + assetItem.localkeywords;
		tmpKeys = tmpKeys.trim();
		return tmpKeys != '';
	}
	
	$rootScope.GetExceptionNodes = function (excItemId, fullList) {
		var childrenNodes = $filter('filter')(fullList, {parentId: excItemId}, function (actual, expected) { return parseInt(actual) == parseInt(expected); });
		
		for (var key = 0; key < childrenNodes.length; key++) {
			if ($rootScope.NotAssetProducts.indexOf(childrenNodes[key].categoryId) < 0) {
				$rootScope.NotAssetProducts.push(childrenNodes[key].categoryId);
				$rootScope.GetExceptionNodes(childrenNodes[key].categoryId, fullList);
			}
		}
	};
	
	$rootScope.RemoveExceptionNodes = function (fullList) {
		$rootScope.NotAssetProducts = [];
		var exceptionList = $filter('filter')(fullList, {syndicationtype: SYNDICATION_TYPE}, function (actual, expected) {
			if (actual == '' || actual.toLowerCase() == 'all' || actual.toLowerCase() == expected.toLowerCase()) return false;
			return true;
		});
		
		if (exceptionList.length > 0) {
			for (var k = 0; k < exceptionList.length; k++) {
				$rootScope.NotAssetProducts.push(exceptionList[k].categoryId);
				$rootScope.GetExceptionNodes(exceptionList[k].categoryId, fullList);
			}
		}
		
		for (var i = fullList.length - 1; i >= 0; i--) {
			if ($rootScope.NotAssetProducts.indexOf(fullList[i].categoryId) >= 0) {
				fullList.splice(i, 1);
			}
		}
	};

	$rootScope.GetAssetUrl = function(asset) {
		if (asset.secured != undefined && asset.secured.toLowerCase() == 'yes')
			return '#';
		try {
			if (asset.cobrandType && asset.cobrandType.toLowerCase()=='cobrand_html')
				return "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(asset.linkurl);
				//return "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(asset.linkurl);
			else
				return encodeURI(asset.linkurl);
		} catch(ex) {
			return asset.linkurl;
		}
	};
	
	$rootScope.GetSearchCategoryUrl = function (searchItem) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + searchItem.categoryId;
	}
	
	$rootScope.GetAssetThumbnail = function (itemThumbnail) {
		return (itemThumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(itemThumbnail);
	};

	$rootScope.DecodeHtml = function(items) {
		for(var i=0;i<items.length;i++) {
			items[i].title = jQuery("<div />").html(items[i].title).text();
			items[i].description = jQuery("<div />").html(items[i].description).text();
		}
		return items;
	};
	
	$rootScope.GetDecodedHTML = function(htmlString) {
		var txt = document.createElement("textarea");
		txt.innerHTML = htmlString;
		return txt.value;
	}
	
	$rootScope.RemoveHtml = function(item) {
		//var temp = jQuery("<div />").html(item).text();
		//return  (jQuery(temp).text()!=''?jQuery(temp).text():temp);
		var temp = jQuery("<div />").html(item).text();
		var r = temp;
		try {
			r = (jQuery(temp).text()!=''?jQuery(temp).text():temp);
		}
		catch(e) {
			r = temp;
		}		
		return  r;		
	};
	
	$rootScope.GetAssetEmbededThumbnails = function (assetItem) {
		var imgList = '';
		for (var i = 1; i <= EMBEDED_IMG_COUNT; i++) {
			if (imgList != '') imgList += ',';
			imgList += MISSED_EMBEDED_IMG.indexOf(i) >= 0 ? '' : assetItem['embededthumbnail' + i];
		}
		return imgList;
	}
	
	var GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	}

	$rootScope.ParseAsset = function(rawList) {
		var ctId = '21000';
		var fieldSplit = GetFieldSplitNo(ctId, rawList);
		if (fieldSplit == 0) return '[]';
		var fieldList = ['ContentInstanceID', 'Content_Title', 'Content_Download_Title', 'Content_Link_Url', 
		'Local_File_Type', 'Content_File_Url', 'Content_Image_Url', 'Content_Image_Thumbnail', 'Local_File_LaunchType', 
		'ContentCategoryID', 'Content_Description_Short', 'Content_Description_Long', 'Local_Type', 'Local_Featured', 
		'Status_Featured_Y_N', 'Status_Sort_Order', 'Local_Vertical', 'Status_LeadGen_Y_N', 'Content_Asset_Date', 
		'CSP_Reporting_Id', 'Local_Partner_Type', 'Status_Secure_Y_N', 'CSP_Unique_Key', 'Local_Keywords',
		'Embeded_Thumbnail_Image_1', 'Embeded_Thumbnail_Image_2', 'Embeded_Thumbnail_Image_3', 'Embeded_Thumbnail_Image_4', 
		'Embeded_Thumbnail_Image_5', 'Embeded_Thumbnail_Image_6'];
		var fieldDic = {
			"Content_Title"			    : "title",
			"Content_Download_Title"	: "downloadtitle",
			"Content_Link_Url"			: "linkurl",
			"Local_File_Type"			: "filetype",
			"ContentInstanceID"			: "contentid",
			"Content_Download_Title"	: "filedownloadtitle"		,
			"Content_File_Url"			: "fileurl",
			"Content_Image_Url"			: "imgurl",
			"Content_Image_Thumbnail"	: "thumbnail"		,
			"Local_File_LaunchType"		: "launchtype"	,
			"ContentCategoryID"			: "cspcategoryid",
			"Content_Description_Short"	: "description"		,
			"Content_Description_Long"	: "descriptionlong"		,
			"Local_Type"			    : "type",
			"Local_Featured"			: "localfeatured",
			"Status_Featured_Y_N"		: "featured"	,
			"Status_Sort_Order"			: "sortorder",
			"Local_Vertical"			: "localvertical",
			"Status_LeadGen_Y_N"		: "statusleadgen"	,
			"Content_Asset_Date"		: "datetime"	,
			"CSP_Reporting_Id"			: "reportingId",
			"Local_Partner_Type"		: "localpartnertype",
			"Local_Featured"			: "cobrandType",
			"Status_Secure_Y_N"			: "secured",
			"CSP_Unique_Key"			: "cspuniquekey",
			"Local_Keywords"			: "localkeywords",
			"Embeded_Thumbnail_Image_1"	: "embededthumbnail1",
			"Embeded_Thumbnail_Image_2"	: "embededthumbnail2",
			"Embeded_Thumbnail_Image_3"	: "embededthumbnail3",
			"Embeded_Thumbnail_Image_4"	: "embededthumbnail4",
			"Embeded_Thumbnail_Image_5"	: "embededthumbnail5",
			"Embeded_Thumbnail_Image_6"	: "embededthumbnail6"
		}
		var jsonAssetList = "[";
		//rawList = rawList[0];
		//console.log(rawList); return;
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var assetItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					if (fieldList.indexOf(property) >= 0) {
						if (assetItem != '{') assetItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						
						if (property == 'Content_Download_Title') {
							assetItem += '"downloadtitle":"' + fieldValue + '","filedownloadtitle":"' + fieldValue + '"';
							continue;
						}
						if (property == 'Local_Featured') {
							assetItem += '"localfeatured":"' + fieldValue + '","cobrandType":"' + fieldValue + '"';
							continue;
						}
						
						/*
						if (property == "CSP_Unique_Key") {
							assetItem += '"' +  fieldDic[property] + '":"yes"';
							continue;
						}
						
						if (property.indexOf('Embeded_Thumbnail_Image') >= 0) {
							var randomIndex = Math.floor(Math.random() * 6) + 1;
							var randomWidth = Math.floor(Math.random() * 400) + 100;
							var randomHeight = Math.floor(Math.random() * 200) + 50;
							var randomImg = 'http://placehold.it/' + randomWidth + 'x' + 900;
							if (randomIndex <= 3) {
								assetItem += '"' +  fieldDic[property] + '":"' + randomImg + '"';
								continue;
							}
						}
						*/
						assetItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			assetItem += "}";
			if (jsonAssetList != '[') jsonAssetList += ',';
			jsonAssetList += assetItem;
		}
		jsonAssetList += ']';
		for (var i = 1; i <= EMBEDED_IMG_COUNT; i++) {
			var bannerName = 'embededthumbnail' + i;
			if (jsonAssetList.indexOf(bannerName) < 0)
				MISSED_EMBEDED_IMG.push(i);
		}
		return jsonAssetList;
	}
	
	$rootScope.ParseCategory = function(rawList) {
		//var fieldSplit = 118;
		var ctId = '31000';
		var fieldSplit = GetFieldSplitNo(ctId, rawList);
		if (fieldSplit == 0) return '[]';
		var fieldList = ['CSP_Unique_Key', 'Content_Title', 'Content_Title_Secondary', 'File_Image_Thumbnail_Url', 'File_Image_Url', 'Content_Description_Short', 'Content_Description_Long', 'Local_Type', 'Local_Syndication_Type', 'Status_Sort_Order', 'Status_Promotion_Y_N', 'ContentCategoryID', 'ContentCategoryParentID', 'ContentCategoryLineage', 'ContentCategoryDepth', 'File_Presentation_1_Name', 'CSP_Reporting_Id', 'CSP_Level'];
		var fieldDic = {
			"CSP_Unique_Key"			: "cspuniquekey",		
			"Content_Title"				: "title",	
			"Content_Title_Secondary"	: "secondarytitle",				
			"File_Image_Thumbnail_Url"	: "thumbnail",				
			"File_Image_Url"			: "image",		
			"Content_Description_Short"	: "description",				
			"Content_Description_Long"	: "descriptionlong",				
			"Local_Type"				: "type",	
			"Local_Syndication_Type"	: "syndicationtype",				
			"Status_Sort_Order"			: "displayorder",		
			"Status_Promotion_Y_N"		: "feature",			
			"ContentCategoryID"			: "categoryId",		
			"ContentCategoryParentID"	: "parentId",				
			"ContentCategoryLineage"	: "lineage",				
			"ContentCategoryDepth"		: "depth",			
			"File_Presentation_1_Name"	: "textorientation",				
			"CSP_Reporting_Id"			: "reportingId",		
			"CSP_Level"					: "cspLevel"
		}
		var jsonCategoryList = "[";
		
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var categoryItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					if (fieldList.indexOf(property) >= 0) {
						if (categoryItem != '{') categoryItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						categoryItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			categoryItem += "}";
			if (jsonCategoryList != '[') jsonCategoryList += ',';
			jsonCategoryList += categoryItem;
		}
		jsonCategoryList += ']';
		return jsonCategoryList;
	}

	$rootScope.RemoveDuplicatedAssets = function (allAssetList) {
		var assetUniqueKeys = [];
		for (var i = allAssetList.length - 1; i >= 0; i--) {
			if (allAssetList[i].contentid == undefined) {
				allAssetList.splice(i, 1);
				continue;
			}
			if (assetUniqueKeys.indexOf(allAssetList[i].contentid) < 0) {
				assetUniqueKeys.push(allAssetList[i].contentid);				
			}
			else {
				//ltu: rebuild the category list
				var item = allAssetList.splice(i, 1)[0];				
				for(var j= allAssetList.length-1;j>=i;j--) {
					if (allAssetList[j].contentid==item.contentid) {						
						//ltu: found the item that match, now append the category id if this category doesn't exist already
						if (allAssetList[j].cspcategoryid.indexOf(","+item.cspcategoryid)==-1) {
							allAssetList[j].cspcategoryid = allAssetList[j].cspcategoryid + "," + item.cspcategoryid;							
						}
						break;
					}
				}
			}
		}
	};

	$rootScope.GoCspLang = function (langItem) {
		$rootScope.CurrentCspLang = langItem;
		jQuery('#csp-mobile-lang-list').hide();
		
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var reDirectUrl = '';
		var tmp = baseDomain.split('-');
		baseDomain = tmp[tmp.length - 1];
		
		if (baseDomain.indexOf('c') != 0)
			baseDomain = 'c' + baseDomain;
		
		baseDomain = 'l' + langItem.Code.toLowerCase() + '-p' + ProjectId.toLowerCase() + '-' + baseDomain;
		
		reDirectUrl = crrUrlInfo[0] + '//' + baseDomain;
		
		for (var i = 3; i < crrUrlInfo.length; i++)
			reDirectUrl += '/' + crrUrlInfo[i];
		
		window.location = reDirectUrl;
	};
	
	var GetCurrentLangCode = function () {
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var crrLangCode = '';
		var regexLang = /^l\w{2}/g;
		var tmp = baseDomain.split('-');
		
		for (var i = 0; i < tmp.length; i++) {
			if (tmp[i].match(regexLang)) {
				crrLangCode = tmp[i].substr(1);
				break;
			}
		}
		
		return crrLangCode != '' ? crrLangCode : cspConsumerInfo.lng.toLowerCase();
	};
	
	var StartTranslateGlobalLabel = function () {
		for (label in $rootScope.GlobalLabel)
			$rootScope.GlobalLabel[label] = $rootScope.GetTranslation($rootScope.GlobalLabel[label]);
	};
	
	//CSPLanguage
	var cspLangDeferred = $q.defer();
	CSPLanguage.promise = cspLangDeferred.promise.then(function (dataList) {
		CSPLanguage.setData(dataList);
		$rootScope.LanguageList = dataList;
		if (dataList.length > 0) {
			var crrLangCode = GetCurrentLangCode();
			if (crrLangCode == '')
				$rootScope.CurrentCspLang = dataList[0];
			else {
				$rootScope.CurrentCspLang = $filter('filter')(dataList, {Code: crrLangCode}, function(actual, expected) {
					return actual.toLowerCase() == expected.toLowerCase();
				});
				if ($rootScope.CurrentCspLang.length == 0)
					$rootScope.CurrentCspLang = dataList[0];
				else
					$rootScope.CurrentCspLang = $rootScope.CurrentCspLang[0];
			}
		}
		return dataList;
	});
	
	$http.get('d10.aspx').success(function(responseData) {
		cspLangDeferred.resolve(responseData);
	});
	
	var deferred = $q.defer();
	Products.promise = deferred.promise.then(function (productList) {
		Products.setData(productList);
		return Products.all();
	});
	
	//$http.get('d1.aspx?p2003(ct31000&cd' + Products.rootid() + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
	$http.get('d2.aspx?p2003(ct31000&cd' + Products.rootid() + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		deferred.resolve(responseData);
	});
	
	var assetDeferred = $q.defer();
	Asset.promise = assetDeferred.promise.then(function (assetList) {
		Asset.setData(assetList);
		return Asset.all();
	});
	
	$http.get('d2.aspx?p2004(ct21000&cd' + Products.rootid() + 'i99)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
	//$http.get('d2.aspx?p2004(ct21000&cd189i1)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		assetDeferred.resolve(responseData);
	});
	
	var translationDeferred = $q.defer();
	Translation.promise = translationDeferred.promise.then(function (dataList) {
		Translation.setData(dataList);
		StartTranslateGlobalLabel();
		return dataList;
	});
	
	$http.get('d1.aspx?p2005(ct35000)[]' + currentTick).success(function(responseData) {
		translationDeferred.resolve(responseData);
	});
	
	var leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
		if (data.length <= 1) return null;
		LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
		LeadgenForm.setFields(data[1].contactusformfields);
		return data;
	});
	// $http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		// leadgenFormDeferred.resolve(responseData);
	// });

	$rootScope.getClass = function (obj) {
		var className = "";
		for (var i = obj.length - 1; i >= 0; i--) {
			if (obj[i].value.categoryId==$rootScope.CurrentCategoryKey) {
				className = "sub-menu-show";
				break;
			}
		};
		return className;
	};
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:

					if ($rootScope.CurrentCategoryItem!=null&&typeof($rootScope.CurrentCategoryItem.value)!="undefined") 
						$rootScope.pageTitle = $rootScope.RemoveHtml($rootScope.CurrentCategoryItem.value.title);
					else 
						$rootScope.pageTitle = defaultTitle;
			}
		}

		try {
			//handle deep links
			if (window.location.hash&&window.location.hash.indexOf('#/#')==0) {						
				try {
					var url = decodeURIComponent(window.location.hash.substring(2));
					window.location = url;
				} catch(e) {}
			}			
			jQuery('#csp-report-lib').remove();
			
			window.setTimeout(function() {
				try {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				jQuery(document).attr('title', $rootScope.pageTitle);
				document.getElementsByTagName('head')[0].appendChild(script);
				} catch (err) {}
			}, 1000);
			
			try{
				parent.postMessage(window.location.href,"*");
			}catch(ex){console.log(ex);};
		} catch (Err) {
			console.log(Err);
		}
	};
});

function MainCtrl($rootScope, $scope, $http, $filter, $routeParams, URLHandler, Asset, Products, Translation, Searcher) {
	tickValue = URLHandler.getTick();
	$rootScope.CurrentCategoryKey = (typeof ($routeParams.categorykey) != "undefined") ? $routeParams.categorykey : '';
	if (typeof ($routeParams.partnertype) != "undefined")
		$rootScope.CurrentPartnerType = $routeParams.partnertype;
	
	$rootScope.AjaxLoading = true;
	$rootScope.IsSearch = false;
	$rootScope.SearchResultCategory = [];
	$rootScope.SearchResultAsset = [];
	$scope.currentCategoryTitle = '';
	$scope.assetCount = 0;
	$scope.crrAssetList = [];
	$scope.whatNewList = [];
	$scope.categoryBlocks = [];
	$scope.whatNewLimitTo = WHAT_NEW_ITEM_LIMIT;
	$scope.whatNewStartDate = new Date();
	$scope.CategoryAssets = [];
	var today = new Date();
	$scope.whatNewStartDate.setDate(today.getDate() - $scope.whatNewLimitTo);
	
	$scope.label = {
		WhatNew: 'whatsnew',
		WhatNewDescription: 'whatsnewdesc',
		categorypageinfo: 'categorypageinfo',
		firstcategorypageinfo: '',
		lastcategorypageinfo: '',
		LearnMore: 'learnmore'
	};
	
	$scope.$on('onLastBlockCallback', function(scope, element, attrs){
		jQuery('.ajax-loading').hide();
		$rootScope.AjaxLoading = false;
	});
	
	$scope.customFilter = function (categoryId) {
		return function (item) {
			var realResult = $filter('filterByCategory')($scope.crrAssetList, categoryId);
			
			if ($scope.CategoryAssets[categoryId] == undefined) {
				$scope.CategoryAssets[categoryId] = realResult.length;
				$scope.assetCount += realResult.length;
			}
			return realResult.indexOf(item) >= 0;
		};
	};
	
	$scope.GetAssetThumbnail = function (itemThumbnail) {
		return (itemThumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(itemThumbnail);
	};
	
	$scope.GetButtonLabel = function (assetItem) {
		return $rootScope.GetTranslation('download');
	};
	
	$scope.SetFakeItemStatus = function (categoryId, itemCount) {
		var totalCount = 0;
		if (itemCount > 0)
			totalCount = itemCount;
		else {
			var assetByPartner = $scope.crrAssetList;//$filter('filter')($scope.crrAssetList, {localpartnertype: $rootScope.CurrentPartnerType}, function (expected, actual) {return expected.indexOf(actual) >= 0;});
			totalCount = $filter('filter')(assetByPartner, {cspcategoryid: categoryId}, function (expected, actual) {
				var expectedIds = expected.split(',');
				for (var i = 0; i < expectedIds.length; i++) {
					if (parseInt(expectedIds[i]) == parseInt(actual))
						return true;
				}
				return false;
			}).length;
		}
		if (totalCount % 2 == 1)
			return true;
		return false;
	};
	
	/*
	$scope.GetDecodedHTML = function (htmlString) {
		return $rootScope.GetDecodedHTML(htmlString);
	}
	*/
	/*
	$scope.byPartnerType = function (entry) {
		if ($rootScope.CurrentPartnerType == '') return true;
		var isInPartnerList = Products.filterByPartner(entry);
		return isInPartnerList;
	};
	*/
	
	var StartTranslating = function () {
		for (label in $scope.label) {
			$scope.label[label] = $rootScope.GetTranslation($scope.label[label]);
			if ($scope.label[label].indexOf('[assets_count]') >= 0) {
				$scope.label.firstcategorypageinfo = $scope.label[label].split('[assets_count]')[0];
				$scope.label.lastcategorypageinfo = $scope.label[label].split('[assets_count]')[1];
			}
		}
	};
	
	var findInChildrenList = function (catItem, childrenList) {
		var isChild = false;
		if (childrenList == undefined || childrenList.length == 0)
			return isChild;
		if (childrenList.indexOf(catItem) >= 0)
			isChild = true;
		else {
			for (var i = 0; i < childrenList.length; i++) {
				isChild = findInChildrenList(catItem, childrenList[i].children);
				if (isChild) break;
			}
		}
		return isChild;
	};
	
	var GetCategoryInTree = function (prodTree) {
		for (var i = 0; i < prodTree.length; i++) {
			//if (prodTree[i].value.cspuniquekey.toLowerCase() == $rootScope.CurrentCategoryKey.toLowerCase()) {
			if (parseInt(prodTree[i].value.categoryId) == parseInt($rootScope.CurrentCategoryKey)) {
				$rootScope.CurrentCategoryItem = prodTree[i];
				return;
			}
			if (prodTree[i].children != undefined)
				GetCategoryInTree(prodTree[i].children);
		}
	};
	
	var isNewCategoryAsset = function (assetItem, categoryNode) {
		var isNewAsset = false;
		if (categoryNode == undefined)
			return false;

		// ltu 01/27/14: categoryNode is an empty array
		if(typeof(categoryNode.value)=="undefined") 
			return false;

		if (parseInt(assetItem.cspcategoryid) == parseInt(categoryNode.value.categoryId))
			isNewAsset = true;
		else if (categoryNode.children != undefined) {
			for (var i = 0; i < categoryNode.children.length; i++) {
				isNewAsset = isNewCategoryAsset(assetItem, categoryNode.children[i]);
				if (isNewAsset) {
					break;
				}
			}
		}
		
		return isNewAsset;
	}
	
	Translation.promise.then(function (translationData) {
		StartTranslating();
		Asset.promise.then(function (resultData) {
			//var resultData = $rootScope.ParseAsset(resultData);// JSON.stringify(eval(parseAsset(resultData)));
			//var myJsonString = JSON.stringify(yourArray);
			//resultData = JSON.stringify(eval(resultData));
			//resultData = JSON.parse(resultData);
			//resultData = Asset.all;
			//Remove duplicated assets
			//console.log(resultData);
			//removeDuplicatedAssets(resultData);
			
			//Set asset list for Searcher
			Searcher.setAssets(resultData.slice(0));
			
			var allAssets = resultData.slice(0);
			$scope.crrAssetList = allAssets;
			
			//If user has Local_Partner_Type value 
			if (ConsumerPartnerType != '') {
				//Multi types
				if (ConsumerPartnerTypeList.length > 0) {
					$scope.crrAssetList = $filter('filter')(allAssets, {contentid: $rootScope.LocalPartnerTypesAssets}, function(actual, expected) {
						var isValid = false;
						for (var typeKey in expected) {
							if (expected[typeKey].indexOf(actual) >= 0) {
								isValid = true;
								break;
							}
						}
						return isValid;
					});
				}
				//Single type
				else if (ConsumerPartnerType.toLowerCase() != 'all') {
					$scope.crrAssetList = $filter('filter')(resultData, {localpartnertype: ConsumerPartnerType}, function (actual, expected) {
						var actualList = actual.split(',');
						for (var key = 0; key < actualList.length; key++) {
							if (actualList[key].toLowerCase() == expected.toLowerCase())
								return true;
						}
						return false;
					});
				}
			}
			

			if ($rootScope.CurrentPartnerType == '')
				$rootScope.CurrentPartnerType = $rootScope.LocalPartnerTypes[0];
				
			if (typeof ($routeParams.partnertype) != "undefined" && $rootScope.AllProducts.length > 0)
				$rootScope.AjaxLoading = false;
			
			$scope.crrAssetList = $filter('filter')($scope.crrAssetList, {localpartnertype: $rootScope.CurrentPartnerType}, function (actual, expected) { 
				var actualList = actual.split(',');
				/*
				for (var key in actualList) {
					if (actualList[key].toLowerCase() == expected.toLowerCase())
						return true;
				}
				*/
				for (var i = 0; i < actualList.length; i++) {
					if (actualList[i].toLowerCase() == expected.toLowerCase())
						return true;
				}
				return false;
			});
			
			Products.promise.then(function (resultData) {
				var rawData = resultData.slice(0);
				//rawData.pop();
				
				//Remove invalid category items
				$rootScope.RemoveExceptionNodes(rawData);
				
				//Set category list for Searcher
				Searcher.setCategories(rawData.slice(0));
				
				//Build tree
				var productTree = Products.convertJSONtoTree(rawData);
				
				$rootScope.GetFeaturedAssets($scope.crrAssetList, rawData);
				
				if ($rootScope.CurrentCategoryKey != '') {
					GetCategoryInTree(productTree);
					if ($rootScope.CurrentCategoryItem.length > 0) 
						$scope.currentCategoryTitle = $rootScope.CurrentCategoryItem.value.title;
					$scope.categoryBlocks.push($rootScope.CurrentCategoryItem);
				}
				// else
					// $scope.categoryBlocks = productTree;
				else {
					//Get Whats New List
					var uniqueNewList = [];
					var whatNewData = $scope.crrAssetList.length > 0 ? $scope.crrAssetList.slice(0) : allAssets.slice(0);
					var newByDate = $filter('filter')(whatNewData, {datetime: $scope.whatNewStartDate}, function (expected, actual) {
						return new Date(expected) >= actual;
					});
					
					if ($rootScope.CurrentCategoryKey != '') {
						for (var i = 0; i < newByDate.length; i++) {
							if (isNewCategoryAsset(newByDate[i], $rootScope.CurrentCategoryItem))
								$scope.whatNewList.push(newByDate[i]);
						}
					}
					else
						$scope.whatNewList = newByDate;
				}
				//Remove invalid items
				for (var j = $scope.whatNewList.length - 1; j >= 0 ; j--) {
					var isValid = false;
					var catIds = $scope.whatNewList[j].cspcategoryid.split(',');
					
					//Check if new asset item is in valid category list
					for (var key = 0; key < catIds.length; key++) {
						catIds[key] = parseInt(catIds[key]);
						for (var rKey = 0; rKey < rawData.length; rKey++) {
							if (parseInt(rawData[rKey].categoryId) == catIds[key]) {
								isValid = true;
								break;
							}
						}
					}
					if (!isValid) {
						$scope.whatNewList.splice(j, 1);
						continue;
					}
					
					//Remove duplication
					if (uniqueNewList.indexOf($scope.whatNewList[j].contentid) < 0)
						uniqueNewList.push($scope.whatNewList[j].contentid);
					else
						$scope.whatNewList.splice(j, 1);
				}
				
				//Reset Partner Types menu
				for (var pType in $rootScope.LocalPartnerTypeStat) {
					$rootScope.LocalPartnerTypeStat[pType] = false;
				}
				
				$rootScope.LocalPartnerTypeStat[$rootScope.CurrentPartnerType] = true;
				$rootScope.updatePageInfo(this);
				
				jQuery(document).click();
				setTimeout(function() {
					jQuery(document).click();
					if (jQuery('#csp-left-section-mobile-inner').height() < jQuery(document).height())
						jQuery('#csp-left-section-mobile-inner').height(jQuery(document).height());
				}, 1000);
			});
		});
	});
	
	jQuery('#csp-search-key').val('');
}

function SearchCtrl($rootScope, $scope, $http, $filter, $routeParams, URLHandler, Asset, Products, Translation, Searcher) {
	$rootScope.IsSearch = true;
	$scope.searchKey = typeof($routeParams.searchkey)=="undefined"?'':$routeParams.searchkey;
	if ($scope.searchKey == '') {
		var currentUrl = document.URL;
		window.location = currentUrl.substring(0, currentUrl.indexOf('#/'));
		return;
	}
	$rootScope.AjaxLoading = true;
	
	$scope.lowerSearchKey = $scope.searchKey.toLowerCase();
	$scope.searchResult = [];
	$scope.crrAssetList = [];
	$scope.label = {
		WhatNew: 'whatsnew',
		WhatNewDescription: 'whatsnewdesc',
		categorypageinfo: 'categorypageinfo',
		firstcategorypageinfo: '',
		lastcategorypageinfo: '',
		LearnMore: 'learnmore'
	};
	$scope.productReadyCheck;
	
	$rootScope.RemoveExceptionNodes = function (fullList) {
		$rootScope.NotAssetProducts = [];
		var exceptionList = $filter('filter')(fullList, {syndicationtype: SYNDICATION_TYPE}, function (actual, expected) {
			if (actual == '' || actual.toLowerCase() == 'all' || actual.toLowerCase() == expected.toLowerCase()) return false;
			return true;
		});
		
		if (exceptionList.length > 0) {
			for (var k = 0; k < exceptionList.length; k++) {
				$rootScope.NotAssetProducts.push(exceptionList[k].categoryId);
				$rootScope.GetExceptionNodes(exceptionList[k].categoryId, fullList);
			}
		}
		
		for (var i = fullList.length - 1; i >= 0; i--) {
			if ($rootScope.NotAssetProducts.indexOf(fullList[i].categoryId) >= 0) {
				fullList.splice(i, 1);
			}
		}
	};
	/*
	var removeDuplicatedAssets = function (allAssetList) {
		var assetUniqueKeys = [];
		for (var i = allAssetList.length - 1; i >= 0; i--) {
			if (allAssetList[i].contentid == undefined) {
				allAssetList.splice(i, 1);
				continue;
			}
			if (assetUniqueKeys.indexOf(allAssetList[i].contentid) < 0) {
				assetUniqueKeys.push(allAssetList[i].contentid);				
			}
			else {
				//ltu: rebuild the category list
				var item = allAssetList.splice(i, 1)[0];				
				for(var j= allAssetList.length-1;j>=i;j--) {
					if (allAssetList[j].contentid==item.contentid) {						
						//ltu: found the item that match, now append the category id if this category doesn't exist already
						if (allAssetList[j].cspcategoryid.indexOf(","+item.cspcategoryid)==-1) {
							allAssetList[j].cspcategoryid = allAssetList[j].cspcategoryid + "," + item.cspcategoryid;							
						}
						break;
					}
				}
			}
		}
	};
	*/
	var StartTranslating = function () {
		for (label in $scope.label) {
			$scope.label[label] = $rootScope.GetTranslation($scope.label[label]);
			if ($scope.label[label].indexOf('[assets_count]') >= 0) {
				$scope.label.firstcategorypageinfo = $scope.label[label].split('[assets_count]')[0];
				$scope.label.lastcategorypageinfo = $scope.label[label].split('[assets_count]')[1];
			}
		}
	};
	
	Translation.promise.then(function (translationData) {
		StartTranslating();
		if ($rootScope.SearchResultCategory.length > 0 || $rootScope.SearchResultAsset.length > 0) {
			$scope.searchResult = $rootScope.SearchResultCategory;
			$scope.crrAssetList = $rootScope.SearchResultAsset;
			setTimeout(function() {
				jQuery("body p").highlight($scope.searchKey);
				jQuery(document).click();
				resizeAssetBlocks(jQuery('.csp-item-asset'));
			}, 1000);
			$rootScope.AjaxLoading = false;
			return;
		}
		
		Asset.promise.then(function (resultData) {
			//Remove duplicated assets
			//removeDuplicatedAssets(resultData);
			
			//Set asset list for Searcher
			Searcher.setAssets(resultData.slice(0));
			
			var allAssets = resultData.slice(0);
			for (var k = 0; k < allAssets.length; k++) {
				if (allAssets[k].downloadtitle.indexOf($scope.lowerSearchKey) >= 0 || allAssets[k].descriptionlong.indexOf($scope.lowerSearchKey) >= 0)
					$scope.crrAssetList.push(allAssets[k]);
			}
		
			Products.promise.then(function (resultData) {
				//resultData.pop();
				var rawData = resultData.slice(0);
				//rawData.pop();
				
				$rootScope.RemoveExceptionNodes(rawData);
				
				//Set category list for Searcher
				Searcher.setCategories(rawData.slice(0));
				
				for (var i = 0; i < rawData.length; i++) {
					var isFound = false;
					if (rawData[i].title.toLowerCase().indexOf($scope.lowerSearchKey) >= 0 || rawData[i].description.toLowerCase().indexOf($scope.lowerSearchKey) >= 0)
						isFound = true;
					if (isFound)
						$scope.searchResult.push(rawData[i]);
				};
				//console.log($scope.searchResult);
				//$scope.MainMenuItems = $rootScope.AllProducts.slice(0);
				setTimeout(function() {
					jQuery("body p").highlight($scope.searchKey);
					jQuery(document).click();
					resizeAssetBlocks(jQuery('.csp-item-asset'));
				}, 1000);
				$rootScope.AjaxLoading = false;
			});
		});
	});
}

function ContactUsCtrl($rootScope, $scope, $routeParams, $timeout, $http, $location, LeadgenForm, Translation, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	$scope.ajaxLoading = false;
	$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		if (LeadgenForm.submit('form1', $scope.submitted) == 1)
			$scope.ajaxLoading = true;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
}

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Products, Asset, Translation, URLHandler) {
	if (typeof($routeParams.searchkey) != "undefined") return;
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.myDisplayOrder = function (menuItem) {
		try {
			var val = parseInt(menuItem.value.displayorder);
			return val;
		} catch(e) { return menuItem.value.displayorder; }			
	}

	var GetLocalPartnerTypes = function (assetList) {
		for (var i = 0; i < assetList.length; i++) {
			if (assetList[i].localpartnertype == undefined) continue;
			var typeList = assetList[i].localpartnertype.split(',');
			if (assetList[i].localpartnertype.indexOf(',') < 0)
				typeList = new Array(assetList[i].localpartnertype);
			var crrCatId = assetList[i].cspcategoryid.split(',');
			for (var j = 0; j < typeList.length; j++) {
				if (typeList[j] != '') {
					if (ConsumerPartnerType != '') {
						/*
						if (ConsumerPartnerTypeList.length > 0 && ConsumerPartnerTypeList.indexOf(typeList[j]) < 0)
							continue;
						else if (ConsumerPartnerTypeList.length == 0 && typeList[j].toLowerCase() != ConsumerPartnerType.toLowerCase())
							continue;
						*/
						if (ConsumerPartnerTypeList.length > 0) {
							if (ConsumerPartnerTypeList.indexOf(typeList[j]) < 0)
								continue;
						}
						else if (typeList[j].toLowerCase() != ConsumerPartnerType.toLowerCase()) {
							continue;
						}
					}
					
					if ($rootScope.LocalPartnerTypes.length == 0 || $rootScope.LocalPartnerTypes.indexOf(typeList[j]) < 0) {
						$rootScope.LocalPartnerTypes.push(typeList[j]);
						$rootScope.LocalPartnerTypeStat[typeList[j]] = false;
						//$rootScope.LocalPartnerTypesCategories.push(typeList[j]);
						$rootScope.LocalPartnerTypesCategories[typeList[j]] = [];
						//$rootScope.LocalPartnerTypesAssets.push(typeList[j]);
						$rootScope.LocalPartnerTypesAssets[typeList[j]] = [];
					}
					for (var cKey = 0; cKey < crrCatId.length; cKey++) {
						crrCatId[cKey] = parseInt(crrCatId[cKey]);
						if ($rootScope.LocalPartnerTypesCategories[typeList[j]].length == 0 || $rootScope.LocalPartnerTypesCategories[typeList[j]].indexOf(crrCatId[cKey]) < 0)
							$rootScope.LocalPartnerTypesCategories[typeList[j]].push(crrCatId[cKey]);
					}
					if ($rootScope.LocalPartnerTypesAssets[typeList[j]].indexOf(assetList[i].contentid) < 0)
						$rootScope.LocalPartnerTypesAssets[typeList[j]].push(assetList[i].contentid);
				}
			}
		}
		
		if ($rootScope.LocalPartnerTypes.length == 0) return;
		
		/*
		$rootScope.LocalPartnerTypes.sort();
		$rootScope.LocalPartnerTypes.reverse();
		*/
		
		for (var k = STANDARD_PARTNER_TYPES.length - 1; k >= 0; k--) {
			if ($rootScope.LocalPartnerTypes.indexOf(STANDARD_PARTNER_TYPES[k]) < 0)
				STANDARD_PARTNER_TYPES.splice(k, 1);
		}
		
		if (STANDARD_PARTNER_TYPES.length > 0)
			$rootScope.LocalPartnerTypes = STANDARD_PARTNER_TYPES;
		
		//HTML list of partner types floats right, do reverse to show the right order
		$rootScope.LocalPartnerTypes.reverse();
		
		for (var j = 0; j < $rootScope.LocalPartnerTypes.length; j++)
			$rootScope.LocalPartnerTypesCategories[$rootScope.LocalPartnerTypes[j]].sort(sortNumber);
	};
	
	$scope.GetNavItemLink = function (navItem) {
		if (ProjectId.toLowerCase() == 'assettest' && navItem.value.depth == 1) return '';
		var currentUrl = document.URL;
		//return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/partner-type/';
		if ($rootScope.CurrentPartnerType != '')
			return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/partner-type/' + $rootScope.CurrentPartnerType + '/category/' + navItem.value.categoryId;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + navItem.value.categoryId;
	};
	
	/*
	$scope.byPartnerType = function (entry) {
		return Products.filterByPartner(entry);
	};
	*/
	
	Translation.promise.then(function (data) {
		Asset.promise.then(function (assetList) {
			try {
				GetLocalPartnerTypes(assetList);
			} catch (err) { console.log('Bug in GetLocalPartnerTypes(): ', err); }
			
			if (typeof ($routeParams.partnertype) != "undefined")
				$rootScope.CurrentPartnerType = $routeParams.partnertype;
			if ($rootScope.CurrentPartnerType == '')
				$rootScope.CurrentPartnerType = $rootScope.LocalPartnerTypes[0];
			
			Products.promise.then(function (resultData) {
				var rawData = resultData.slice(0);
				
				$rootScope.RemoveExceptionNodes(rawData);
				$rootScope.AllProducts = Products.convertJSONtoTree(rawData);
				if (typeof $rootScope.AllProducts != 'undefined')
					$scope.MainMenuItems = $rootScope.AllProducts.slice(0);
				$rootScope.AjaxLoading = false;
			});
		});
	});
}