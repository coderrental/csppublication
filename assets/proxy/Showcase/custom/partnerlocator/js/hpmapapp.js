var BING_MAP_SKEY = 'Ag6_ygqCFo_BGK2fxdo2wNhkwODZNhEzzIIYO8VpDofJ8y6OUpppTejfTTx2BsoP';
var BING_MAP_REST_LOCATION = 'http://dev.virtualearth.net/REST/v1/Locations?query=[location_query]&key=' + BING_MAP_SKEY + '&jsonp=?';
var BING_MAP_REST_AUTO_COUNTRY = 'http://dev.virtualearth.net/REST/v1/Locations?output=json&c=en&q=[search_key]&key=' + BING_MAP_SKEY + '&jsonp=?';
var NEARBY_STORE_KM = 50;
var HPMainMap;
var SearchMode = 1;
var pinInfoBox = null;
var infoboxLayer = new Microsoft.Maps.EntityCollection();
var pinLayer = new Microsoft.Maps.EntityCollection();

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

//Init Map
function GetMap() {
	Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: themesModuleLoaded });
}

function onview(){
    pinInfobox.setOptions({visible:false});
}

function themesModuleLoaded() {
	HPMainMap = new Microsoft.Maps.Map(document.getElementById("mapDiv"), 
							   {credentials: BING_MAP_SKEY,
								//center: new Microsoft.Maps.Location(result.resourceSets[0].resources[0].geocodePoints[0].coordinates[0], result.resourceSets[0].resources[0].geocodePoints[0].coordinates[1]),
								enableSearchLogo: false,
								showMapTypeSelector: false,
								showCopyright: false,
								zoom: 1,
								theme: new Microsoft.Maps.Themes.BingTheme()});
								
	Microsoft.Maps.Events.addHandler(HPMainMap, "viewchangeend", onview);
	pinInfobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
	infoboxLayer.push(pinInfobox);
	HPMainMap.entities.push(infoboxLayer);
	/*
	//Get current location (country)
	jQuery.ajax({ 
		url: '//freegeoip.net/json/', 
		type: 'POST', 
		dataType: 'jsonp',
		success: function(location) {
			jQuery('#crr-country').text(location.country_name);
			var countryRESTUrl = BING_MAP_REST_LOCATION.replace('[location_query]', location.country_name);
			jQuery.getJSON(countryRESTUrl, function (result) {
				//Loading map with current country
				if (result.resourceSets[0].estimatedTotal > 0) {
					HPMainMap = new Microsoft.Maps.Map(document.getElementById("mapDiv"), 
							   {credentials: BING_MAP_SKEY,
								center: new Microsoft.Maps.Location(result.resourceSets[0].resources[0].geocodePoints[0].coordinates[0], result.resourceSets[0].resources[0].geocodePoints[0].coordinates[1]),
								enableSearchLogo: false,
								showMapTypeSelector: false,
								showCopyright: false,
								zoom: 6,
								theme: new Microsoft.Maps.Themes.BingTheme()});
				}
				else {
					HPMainMap = new Microsoft.Maps.Map(document.getElementById("mapDiv"), 
							   {credentials: BING_MAP_SKEY,
								enableSearchLogo: false,
								zoom: 1});
				}
				Microsoft.Maps.Events.addHandler(HPMainMap, "viewchangeend", onview);
				pinInfobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
				infoboxLayer.push(pinInfobox);
				HPMainMap.entities.push(infoboxLayer);
			});
		}
	});
	*/
}

function showPinInfo (e) {
	pinInfobox.setOptions({title: e.target.Title, description: e.target.Description, visible:true, offset: new Microsoft.Maps.Point(0,0), width: 175});
    pinInfobox.setLocation(e.target.getLocation());
};

//START ANGULARJS MODULE
var hpmapapp = angular.module('ngHPDemo', []);

hpmapapp.filter('HPMapFilter', function() {
    return function( items, searchKey, searchType) {
		var filtered = [];
		searchKey = searchKey.toLowerCase();
		angular.forEach(items, function(item) {
			
			if (searchType == 1) {
				if (typeof(item.address1) != 'undefined' && item.address1 != null && item.address1.toLowerCase().indexOf(searchKey) != -1)
					filtered.push(item);
			}
			else if (typeof(item.companyname) != 'undefined' && item.companyname != null && item.companyname.toLowerCase().indexOf(searchKey) != -1) {
					filtered.push(item);
			}
		});
		return filtered;
	};
});

//Main Controller
hpmapapp.controller('MainCtrl', ['$http', '$filter', '$log', '$interval', function($http, $filter, $log, $interval) {
	//API URL with place holder of project name
	this.apiUrl = 'http://cspwebapi.tiekinetix.com/cspwebapi_v5/api/json/subscriber/[prj_name]/GetAllSubs/1?jsonp=JSON_CALLBACK';
	this.apiUrl = this.apiUrl.replace('[prj_name]', PROJECT_NAME);//replace place holder with project name
	
	//hard code to debug
	//this.apiUrl = 'http://cspwebapi.tiekinetix.com/cspwebapi_v5/api/json/subscriber/digitalchannel/GetAllSubs/1?jsonp=JSON_CALLBACK';
	
	this.storeData = [];
	this.addressSuggest = [];
	this.testShow = this.addressSuggest.length > 0;
	this.searchKey = '';
	this.currentStore = [];
	this.nearbyStores = [];
	this.searchType = 1;
	var aliasMainC = this;
	var HPMainMapWaiting;
	
	//Switch search type
	this.setSearchType = function (typeId) {
		aliasMainC.searchType = typeId;
		aliasMainC.currentStore = [];
		aliasMainC.nearbyStores = [];
		aliasMainC.searchKey = '';
		aliasMainC.mapDispose();
	};
	
	//Key up event handler of search box
	this.doSearchKeyUp = function (e) {
		aliasMainC.currentStore = [];
		jQuery('#btnSearch').removeClass('primary-search');
	};
	
	//User select a result from suggestion
	this.getMapLocation = function (store) {
		aliasMainC.currentStore = store;
		if (aliasMainC.searchType == 1)
			aliasMainC.searchKey = aliasMainC.currentStore.address1;
		else
			aliasMainC.searchKey = aliasMainC.currentStore.companyname;
	};
	
	this.getStoreUrl = function (store) {
		if (typeof(store.deeplink_Landingpage_Url) != 'undefined' && store.deeplink_Landingpage_Url != '')
			return store.deeplink_Landingpage_Url;
		return store.website_url;
	};
	
	this.getBaseDomain = function(store) {
		var url = aliasMainC.getStoreUrl(store);
		var i = url.indexOf('/',7);
		if (i == -1)
			return url;
		return url.substring(0, i);
	}
	
	//Due to search type, return suggestion text
	this.GetResultText = function (store) {
		if (aliasMainC.searchType == 1)
			return store.address1;
		return store.companyname;
	};
	
	this.calculateDistance = function (obj) {
		var R = 6371; // km
		var dLat = (obj.lat2 - obj.lat1) * Math.PI / 180;
		var dLon = (obj.lon2 - obj.lon1) * Math.PI / 180;
		var lat1 = obj.lat1 * Math.PI / 180;
		var lat2 = obj.lat2 * Math.PI / 180;
	 
		var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c;
		var m = d * 0.621371;
		return {
			km: d,
			m: m
		}
	};
	
	this.findNearBy = function () {
		aliasMainC.nearbyStores = [];
		var dist;
		for (var i = 0; i < aliasMainC.storeData.length; i++) {
			dist = aliasMainC.calculateDistance({
				lat1: aliasMainC.currentStore.latitude,
				lon1: aliasMainC.currentStore.longitude,
				lat2: aliasMainC.storeData[i].latitude,
				lon2: aliasMainC.storeData[i].longitude
			});
			if (dist.km < NEARBY_STORE_KM) {
				aliasMainC.nearbyStores.push(aliasMainC.storeData[i]);
				aliasMainC.pourResultToMap(aliasMainC.storeData[i]);
			}
		}
		
	};
	
	this.replaceAll = function (find, replace, str) {
		find = find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
		return str.replace(new RegExp(find, 'g'), replace);
	}
	
	this.pourResultToMap = function (storeItem) {
		var foundLocation = new Microsoft.Maps.Location(storeItem.latitude, storeItem.longitude);
		var pushpin = new Microsoft.Maps.Pushpin(foundLocation);
		
		//Get info box template
		var infoDescTemplate = jQuery('#pininfobox-tmp').html();
		infoDescTemplate = aliasMainC.replaceAll ('[address]', storeItem.address1, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[city]', storeItem.city, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[state]', storeItem.state, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[country]', storeItem.country, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[zipcode]', storeItem.zipcode, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[email]', storeItem.personalize_Contact_Emailaddress, infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[website]', aliasMainC.getStoreUrl(storeItem), infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[website_text]', aliasMainC.getBaseDomain(storeItem), infoDescTemplate);
		infoDescTemplate = aliasMainC.replaceAll ('[phonenumber]', storeItem.personalize_Contact_Phonenumber == null ? '' : storeItem.personalize_Contact_Phonenumber, infoDescTemplate);
		
		pushpin.Title = storeItem.companyname;
		pushpin.Description = infoDescTemplate;
		
		pinLayer.push(pushpin);
		Microsoft.Maps.Events.addHandler(pushpin, 'click', showPinInfo);
	};
	
	//Remove all last search pushpins
	this.mapDispose = function () {
		try {
			pinInfobox.setOptions({visible:false});
			pinLayer.clear();
			HPMainMap.entities.removeAt(HPMainMap.entities.indexOf(pinLayer));
		} catch (err) {
			pinLayer = new Microsoft.Maps.EntityCollection();
			$log.debug('Map entities clear failed');
		}
	};
	
	//Button "Find" clicked
	this.goSearch = function () {
		if (aliasMainC.currentStore.length == 0) {
            dcsMultiTrack('WT.oss', aliasMainC.searchKey, 'WT.oss_r', '0');
			alert('Invalid store information or store not found');
			return;
		}
        
        dcsMultiTrack('WT.oss', aliasMainC.searchKey, 'WT.oss_r', '1');
		
		//Clear all map entities
		aliasMainC.mapDispose();
		
		var foundLocation = new Microsoft.Maps.Location(aliasMainC.currentStore.latitude, aliasMainC.currentStore.longitude);
		
		//Get the existing options.
		var options = HPMainMap.getOptions();
		
		//Add a pushpins from search result
		aliasMainC.pourResultToMap(aliasMainC.currentStore);
		aliasMainC.nearbyStores.push(aliasMainC.currentStore);
		aliasMainC.findNearBy();
		
		HPMainMap.entities.push(pinLayer);
		
		//Navigate the map to search result location
		options.center = foundLocation;
		options.zoom = 8;
		HPMainMap.setView(options);
	};
	
	this.loadAllStoreToMap = function () {
		if (HPMainMap == undefined) return;
		
		//Clear timer
		if (angular.isDefined(HPMainMapWaiting)) {
			$interval.cancel(HPMainMapWaiting);
			HPMainMapWaiting = undefined;
		}
		
		//Add all stores into map
		aliasMainC.mapDispose();
		for (var i = 0; i < aliasMainC.storeData.length; i++) {
			aliasMainC.pourResultToMap(aliasMainC.storeData[i]);
		}
		HPMainMap.entities.push(pinLayer);
	};
	
	$http.jsonp(this.apiUrl).success(function(responseData) {
		aliasMainC.storeData = responseData;
		
		//Set timer to wait until HPMainMap is available
		HPMainMapWaiting = $interval(function() { aliasMainC.loadAllStoreToMap(); }, 1000);
	}).error(function(err) {
		console.log('*** error', err);
	});
	
}]);