function initApp() {
	initMenu();
	resetToutBlockDisplay();
}

function vAlignNavText() {
	$('.root-item-text').each(function() {
		var thisHeight = $(this).height();
		var wrapperHeight = $(this).closest('li').height();
		var topPos = (wrapperHeight - thisHeight) / 2;
		$(this).css({ 'padding-top': topPos + 'px' });
	});
}

function syncToutBlockHeight() {
	var theHighestHeight = 0;
	$('#csp-tout-area li').each(function() {
		if ($(this).outerHeight() > theHighestHeight)
			theHighestHeight = $(this).outerHeight();
	});
	
	$('#csp-tout-area li').attr('style', 'height: ' + theHighestHeight + 'px !important');
}

function resetToutBlockDisplay() {
	//Reset block Height
	syncToutBlockHeight();
	
	//Reset block width
	var paddingSpace = 10;
	var borderWidth = 1;
	var pageWidth = $('#csp-wrapper').width();
	var toutBlockCount = $('#csp-tout-area li').length;
	var blockWidth = (pageWidth / toutBlockCount) - paddingSpace - borderWidth;
	var lastStyle = $('#csp-tout-area li').attr('style') + '; ';
	$('#csp-tout-area li').attr('style', lastStyle + 'width: ' + blockWidth + 'px !important');
	
	//Set block color
	/*
	var colorArr = new Array('#EEE', '#DDD', '#CCC', '#BBB');
	
	$('#csp-tout-area li').each(function() {
		//$(this).css('background-color', colorArr[$('#csp-tout-area li').index($(this))] + ' !important');
		lastStyle = $(this).attr('style') + '; ';
		$(this).attr('style', lastStyle + 'background-color: ' + colorArr[$('#csp-tout-area li').index($(this))] + '; width: ' + blockWidth + 'px !important');
		//alert($('#csp-tout-area li').index($(this)));
	});
	*/
}

function initMenu() {
	$('li.root-item').hover(function() {
		$(this).find('div.sub-menu-wrapper').show();
	}, function() {
		$(this).find('div.sub-menu-wrapper').hide();
	});
	
	//var navWidth = 510;
	var navWidth = 660;
	var menuItemCount = $('li.root-item').length;
	var itemWidth = (navWidth / menuItemCount) - 1;
	//var crrStyle = $('li.root-item').attr('style');
	$('li.root-item').attr('style', 'width: ' + itemWidth + 'px !important');
	$('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
	
	var subMenuItemCount;
	var widthPercent;
	$('ul.sub-menu').each(function() {
		subMenuItemCount = $(this).find('li.sub-item').length;
		widthPercent = (100 / subMenuItemCount) - 3;
		$(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '% !important; padding-right: 3%');
	});
	
	vAlignNavText();
}

function removeParentCatFromLeftCol() {
	var pIdArr = new Array();
	$('#csp-left-col a').each(function() {
		pIdArr.push($(this).attr('pid'));
	});
	var sorted_arr = pIdArr.sort();
                             
	var results = [];
	for (var i = 0; i < pIdArr.length - 1; i++) {
		if (sorted_arr[i + 1] != sorted_arr[i]) {
			results.push(sorted_arr[i]);
		}
	}

	$('#csp-left-col a[pid="'+results+'"]').closest('li').remove();
}