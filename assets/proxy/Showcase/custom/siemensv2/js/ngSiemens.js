var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;


function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function UpdateUserCookies() {
	jQuery('#frmleadgen .tieValidate').each(function() {
		setCookie(jQuery(this).attr('id'), jQuery(this).val());
	});
}

function GetUserInfo() {
	return { firstname: getCookie('firstname'), lastname: getCookie('lastname'), companyname: getCookie('companyname'), employees: getCookie('employees'), email: getCookie('email') };
}

function AutoPopulateUserInfo() {
	if (jQuery('#frmleadgen').length == 0 && jQuery('#contactus-page').length == 0) return;
	var userInfo = GetUserInfo();
	jQuery('#firstname').val(userInfo.firstname);
	jQuery('#lastname').val(userInfo.lastname);
	jQuery('#companyname').val(userInfo.companyname);
	jQuery('#employees').val(userInfo.employees);
	jQuery('#email').val(userInfo.email);
}

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

//Sync Prod blocks height
function syncProdBlockHeight (blockRow) {
	var maxHeight = 0;
	blockRow.children().each(function() {
		var blockHeight = jQuery(this).find('.prod-img img').height();
		var imgSrc = jQuery(this).find('.prod-img img').attr('src');
		if (blockHeight == 0) {
			window.setTimeout(function() {
				syncProdBlockHeight(blockRow);
			}, 500);
			return;
		}
		if (blockHeight > maxHeight)
			maxHeight = blockHeight;
	});
	
	if (maxHeight == 0) return;
	
	blockRow.children().each(function() {
		jQuery(this).find('.prod-img img').height(maxHeight);
	});
}

//Fix Prod block with over height banner text
function adjustProdBlockSize (blockEl) {
	var blockRow = jQuery(blockEl).parent('.row');
	if(jQuery(blockEl).children().length == 0) {
		window.setTimeout(function() { adjustProdBlockSize(blockEl); }, 500);
		return;
	}
	blockRow.children().each(function(itemIndex, itemEl) {
		var prodBlock = jQuery(this);
		jQuery(this).find('.prod-img img').one("load", function() {
			blockHeight = jQuery(this).height();
			var overlayTextHeight = prodBlock.find('.prod-text').outerHeight() + 10;
			
			if (overlayTextHeight > blockHeight) {
				jQuery(this).height(overlayTextHeight);
			}
			
			if (blockRow.children().length == 1) return;
			
			syncProdBlockHeight (blockRow);
		}).each(function() {
			if(this.complete) jQuery(this).load();
		});
	});
}

function syncProdContentBlockHeight(blockRow) {
	var parentRow = blockRow;
	var maxHeight = 0;
	var imagesLoaded = true;
	blockRow.find('img').each(function() {
		if (jQuery(this).height() == 0)
			imagesLoaded = false;
	});
	if (!imagesLoaded) {
		window.setTimeout(function () {
			syncProdContentBlockHeight(blockRow);
		}, 500);
		return;
	}
	parentRow.find('.prod-block').each(function() {
		var blockHeight = jQuery(this).height();
				
		if (blockHeight > maxHeight)
			maxHeight = blockHeight;
	});
	
	parentRow.find('.prod-block').each(function() {
		jQuery(this).height(maxHeight);
	});
}

//Fix Prod content block with over height banner text
function adjustProdContentBlockSize (blockEl) {
	var blockRow = jQuery(blockEl).parent('.row');
	if(jQuery(blockEl).children().length == 0) {
		window.setTimeout(function() { adjustProdContentBlockSize(blockEl); }, 500);
		return;
	}
	blockRow.children().each(function(itemIndex, itemEl) {
		var prodBlock = jQuery(this);
		jQuery(this).find('.prod-img img').one("load", function() {
			blockHeight = jQuery(this).height();
			var overlayTextHeight = prodBlock.find('.prod-text').outerHeight() + 10;
			
			if (overlayTextHeight > blockHeight) {
				jQuery(this).height(overlayTextHeight);
			}
			
			syncProdContentBlockHeight (blockRow);
		}).each(function() {
			if(this.complete) jQuery(this).load();
		});
		
		jQuery(this).find('.asset-img img').one("load", function() {
			blockHeight = jQuery(this).height();
			var overlayTextHeight = prodBlock.find('.asset-text').outerHeight() + 10;
			
			if (overlayTextHeight > blockHeight) {
				jQuery(this).height(overlayTextHeight);
			}
			
			syncProdContentBlockHeight (blockRow);
		}).each(function() {
			if(this.complete) jQuery(this).load();
		});
		
		if (jQuery(this).find('.prod-img img').length == 0 && jQuery(this).find('.asset-img img').length == 0)
			syncProdContentBlockHeight (blockRow);
	});
}

var ngSiemensv2 = angular.module('ngSiemens', ['ngRoute', 'ngSanitize'])
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/i/:tick', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/home', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/home/i/:tick', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/companyid/:coid/messageid/:msgid', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/messageid/:msgid/cspid/:coid/i/:tick', {controller: 'MainController', templateUrl: 'ui/showcase/Custom/Siemensv2/main.html', controllerAs: 'mainc'})
	.when('/products', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/:id', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
    .when('/products/:id/pl', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/:id/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	
	.when('/products/parentlist/:plist/prodid/:id', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/parentlist/:plist/prodid/:id/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/parentlist/:plist/prodid/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/products/parentlist/:plist/prodid/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/insights/:assettype/:id', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/insights/:assettype/:id/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/insights/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/insights/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	
	.when('/newwaystowork/:assettype/:id', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/newwaystowork/:assettype/:id/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/newwaystowork/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	.when('/newwaystowork/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ProductsController', templateUrl:'ui/showcase/Custom/Siemensv2/products.html', controllerAs: 'prodc'})
	
	.when('/insights/:assettype', {controller: 'AssetController', templateUrl:'ui/showcase/Custom/Siemensv2/asset.html', controllerAs: 'ac'})
	.when('/insights/:assettype/i/:tick', {controller: 'AssetController', templateUrl:'ui/showcase/Custom/Siemensv2/asset.html', controllerAs: 'ac'})
	.when('/insights/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'AssetController', templateUrl:'ui/showcase/Custom/Siemensv2/asset.html', controllerAs: 'ac'})
	.when('/insights/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'AssetController', templateUrl:'ui/showcase/Custom/Siemensv2/asset.html', controllerAs: 'ac'})
	
	.when('/casestudy/:assettitle/:id', {controller: 'AssetDetailController', templateUrl:'ui/showcase/Custom/Siemensv2/assetdetail.html', controllerAs: 'adc'})
	.when('/casestudy/:assettitle/:id/i/:tick', {controller: 'AssetDetailController', templateUrl:'ui/showcase/Custom/Siemensv2/assetdetail.html', controllerAs: 'adc'})
	.when('/casestudy/:assettitle/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'AssetDetailController', templateUrl:'ui/showcase/Custom/Siemensv2/assetdetail.html', controllerAs: 'adc'})
	.when('/casestudy/:assettitle/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'AssetDetailController', templateUrl:'ui/showcase/Custom/Siemensv2/assetdetail.html', controllerAs: 'adc'})
		
	.when('/contactus', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/:info', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/i/:tick', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/title/:titlevalue', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/title/:titlevalue/i/:tick', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/title/:titlevalue/messageid/:msgid/cspid/:coid/socialid/:scid', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/contactus/title/:titlevalue/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller: 'ContactUsController', templateUrl:'ui/showcase/Custom/Siemensv2/contactus.html', controllerAs: 'contc'})
	.when('/comingsoon', {controller: 'ComingSoonCtrl', templateUrl:'ui/showcase/Custom/Siemensv2/comingsoon.html'})
	.otherwise({ redirectTo: function(obj, path) {
		try {
			var i = path.indexOf("csp_page:");
			if (i>=0)
				return path.substring(i+9);
			
			//Contact Us page with dynamic params
			if (path.indexOf('contactus[]') != -1)
				return path.replace('[]&', '/');
				
		} catch(e) {}
		return '/';
	}});
})
.run(['$rootScope', '$http', '$q', '$filter', '$sce', 'TabsAndNavigation', 'Products', 'LeadgenForm', 'Translation', 
function($rootScope, $http, $q, $filter, $sce, TabsAndNavigation, Products, LeadgenForm, Translation) {
	var currentUrl = document.URL;
	$rootScope.currentTick = '';
	$rootScope.baseDomain = '';
	$rootScope.ProductDeferLoaded = false;
	$rootScope.LeadgenDeferLoaded = false;
	$rootScope.TranslationDeferLoaded = false;
	$rootScope.AllProducts = [];
	$rootScope.menuLevelToShow = 2;
	$rootScope.Supplier = SupplierInfo;
	$rootScope.DGShopButton = false;
	$rootScope.DGShopScriptUrl = '';
	$rootScope.DGShopScriptUrlTemplate = 'https://[shopId].shop.tiekinetix.net/start.js';
	$rootScope.DGShopId = '';
	while(MainNavFontColor.charAt(0) === "#"){
		MainNavFontColor = MainNavFontColor.slice(1);
	}
	while(MainNavBackgroundColor.charAt(0) === "#"){
		MainNavBackgroundColor = MainNavBackgroundColor.slice(1);
	}
	$rootScope.menuFontColor = MainNavFontColor;
	$rootScope.menuBgColor = MainNavBackgroundColor;
	
	if (currentUrl.indexOf('#/') != -1)
		$rootScope.baseDomain = currentUrl.split('#')[0];
	else
		$rootScope.baseDomain = (currentUrl.indexOf('http') == -1) ? 'http://' + currentUrl : currentUrl;
	
	if (typeof($rootScope.Supplier.telephone) == 'undefined')
		$rootScope.Supplier.telephone = '';
	if (typeof($rootScope.Supplier.twitter) == 'undefined')
		$rootScope.Supplier.twitter = '';
	if ($rootScope.Supplier.twitter != '' && $rootScope.Supplier.twitter.indexOf('http') == -1)
		$rootScope.Supplier.twitter = 'http://' + $rootScope.Supplier.twitter;
	
	if (typeof(cspConsumerInfo) != 'undefined') {
		$rootScope.DGShopScriptUrl = $rootScope.DGShopScriptUrlTemplate.replace('[shopId]', cspConsumerInfo.companies_Id);
		//$rootScope.DGShopScriptUrl = $sce.trustAsResourceUrl($rootScope.DGShopScriptUrl);
		if (parseInt(cspConsumerInfo.companies_Id) == 170) {
			$rootScope.DGShopButton = true;
			$rootScope.DGShopId = parseInt(cspConsumerInfo.companies_Id);
		}
	}
	
	if($rootScope.menuBgColor != null || $rootScope.menuFontColor != null ){
		jQuery("#main-nav-bar").css("visibility","hidden");
		window.setTimeout(function() {
			jQuery("#main-nav-bar").css("background","#" + $rootScope.menuBgColor);
			jQuery("#main-nav-bar .dropdown-menu").css("background","#" + $rootScope.menuBgColor);
			jQuery(".navbar li a").each(function () {
				this.style.setProperty( 'color',"#" +  $rootScope.menuFontColor, 'important' );
			});
		},100);
	}
	
	//Get ticks value from URL if exists
	if (QueryString.ticks != undefined)
		$rootScope.currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
		var temp = currentUrl.split('/');
		for (var i = 0; i < temp.length; i++) {
			if (temp[i] == 'i') {
				$rootScope.currentTick = '&i=' + temp[i + 1];
				break;
			}
		}
	}
	tickValue = $rootScope.currentTick.replace('&i=', '');
	
	$rootScope.msieversion = function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        // If another browser, return 0
        return 0;
	};
	
	$rootScope.GetBaseDomain = function () {
		return $rootScope.msieversion() > 0 && $rootScope.msieversion() < 9 ? $rootScope.baseDomain : '';
	}
	
	$rootScope.validateCspKey = function (cspKey) {
		if (cspKey.indexOf('=') == -1) return cspKey;
		var tmpKey = cspKey.split('=')[0];
		tmpKey = tmpKey.split('&')[0];
		if (typeof(tmpKey) == 'undefined') return cspKey;
		return tmpKey.replace(/[\[\]]/g, '');
	};
	
	$rootScope.TrustHtmlContent = function (htmlString) {
		if (typeof(htmlString) == 'undefined') return '';
		htmlString = htmlString.replace(/href=\"\/#/g, 'href="' + $rootScope.GetBaseDomain() + '#');
		htmlString = htmlString.replace(/href=\'\/#/g, "href='" + $rootScope.GetBaseDomain() + "#");
		return $sce.trustAsHtml(htmlString);
	};
	
	$rootScope.TrustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			if (typeof(value) != 'undefined')
				$rootScope.pageTitle = value;
			if ($rootScope.pageTitle.indexOf('Unify -') == -1)
				$rootScope.pageTitle = 'Unify - ' + $rootScope.pageTitle;
		}
		else {
			switch (theCtrl.name) {
				case 'MainController':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ProductsController':
					$rootScope.pageTitle = 'Product';
					break;
				case 'AssetController':
					$rootScope.pageTitle = 'Asset';
					break;
				case 'AssetDetailController':
					$rootScope.pageTitle = 'Asset Detail';
					break;
				case 'ContactUsController':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:
					$rootScope.pageTitle = 'Unify';
			}
		}

		try {
			//handle deep links
			if (window.location.hash&&window.location.hash.indexOf('#/#')==0) {						
				try {
					var url = decodeURIComponent(window.location.hash.substring(2));
					window.location = url;
				} catch(e) {}
			}
			//if (currentSyndicationLng.toLowerCase()=="zh") window.location="/#/comingsoon";
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				jQuery(document).attr('title', $rootScope.pageTitle);
				document.getElementsByTagName('head')[0].appendChild(script);
			}, 1000);
			try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
		} catch (Err) {
		
		}
	};
	
	$rootScope.GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	};
	
	$rootScope.ParseObject = function (rawList, fieldSplit, ctId, fieldList, fieldDic) {
		var objectList = "[";
		
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var objectItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					property = property.trim();
					if (fieldList.indexOf(property) >= 0) {
						if (objectItem != '{') objectItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						objectItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			objectItem += "}";
			if (objectList != '[') objectList += ',';
			objectList += objectItem;
		}
		objectList += ']';
		return JSON.parse(objectList);
	};
	
	//Factories Init
	
	//TabsAndNavigationFactory
	var TabsNavDeferred = $q.defer();
	TabsAndNavigation.promise = TabsNavDeferred.promise.then(function (data) {
		var tabnavList = data.length == 0 ? [] : TabsAndNavigation.parse(data);
		return tabnavList;
	});
	
	$http.get('d2.aspx?p2001(ct34000)[]' + $rootScope.currentTick).success(function(responseData) {
		TabsNavDeferred.resolve(responseData);
	});
	
	//Products (Category Tree) Factory
	$rootScope.ProductDeferred = $q.defer();
	Products.promise = $rootScope.ProductDeferred.promise.then(function (productList) {
		Products.setData(productList);
		return Products.all();
	});
	
	//To control when to call this Products request
	$rootScope.CallProductDefer = function () {
		$rootScope.ProductDeferLoaded = true;
		$http.get('d2.aspx?p2003(ct31000&cd' + Products.rootid() + 'i99)[st(ct31000*Status_Sort_Order*)]' + $rootScope.currentTick).success(function(responseData) {
			$rootScope.ProductDeferred.resolve(responseData);
			window.setTimeout(function() {
				jQuery(".navbar li a").each(function () {
					this.style.setProperty( 'color',"#" +  $rootScope.menuFontColor, 'important' );
				});
			},100);
		});
	};
	
	//Leadgen Factory
	$rootScope.leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = $rootScope.leadgenFormDeferred.promise.then(function (data) {
		var parsedData = LeadgenForm.parseLeadgenItem(data);
		//parsedData = JSON.parse(parsedData);
		
		LeadgenForm.setSupplierInfo(SupplierInfo);
		LeadgenForm.setFields(parsedData);
		
		return parsedData;
	});
	
	//To control when to call this Leadgen request
	$rootScope.CallLeadgenDefer = function () {
		$rootScope.LeadgenDeferLoaded = true;
		$http.get('d2.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + $rootScope.currentTick).success(function(responseData) {
			$rootScope.leadgenFormDeferred.resolve(responseData);
		});
	};
	
	//Translation Factory
	$rootScope.translationDeferred = $q.defer();
	Translation.promise = $rootScope.translationDeferred.promise.then(function (dataList) {
		var translationList = Translation.parse(dataList);
		return translationList;
	});
	
	//To control when to call this Translation request
	$rootScope.CallTranslationDefer = function () {
		$rootScope.TranslationDeferLoaded = true;
		$http.get('d2.aspx?p2005(ct35000)[]' + $rootScope.currentTick).success(function(responseData) {
			$rootScope.translationDeferred.resolve(responseData);
		});
	};
}]);

//Home page directive - init carousel swiping for mobile & call iframe resize after all block loaded
ngSiemensv2.directive('myHomeDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			jQuery("#home-carousel").swiperight(function() {  
				jQuery("#home-carousel").carousel('prev');  
			});  
			jQuery("#home-carousel").swipeleft(function() {  
				jQuery("#home-carousel").carousel('next');  
			});
			jQuery('.carousel-control').on('click', function(e) {
				e.preventDefault();
			});
			
			window.setTimeout(function() {
				jQuery(document).click();
			}, 2000);
		}
	};
});

//Product page - call Report after all children loaded
ngSiemensv2.directive('myProdchildDirective', ['$rootScope', function($rootScope) {
	return function(scope, element, attrs) {
		if (scope.$last){
			$rootScope.updatePageInfo(null, $rootScope.pageTitle);
			window.setTimeout(function() {
				jQuery(document).click();
			}, 1500);
		}
	};
}]);

//Product page - call iframe resize after all blocks (if exist) loaded
ngSiemensv2.directive('myProdblockDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			window.setTimeout(function() {
				jQuery(document).click();
			}, 1500);
		}
	};
});

//Prod block with banner & overlay - height issue fix
ngSiemensv2.directive('myProdcolDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			adjustProdBlockSize(element);
		}
	};
});

//Prod content blocks height issue fix
ngSiemensv2.directive('myContentblockDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			adjustProdContentBlockSize(element);
		}
	};
});

//Tab and Nav Factory
ngSiemensv2.factory('TabsAndNavigation', ['$rootScope', '$http', function TabsAndNavigationFactory ($rootScope, $http) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		parse: function (rawList) {
			var fieldSplit = 51;
			var ctId = '34000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["CSP_Unique_Key", "Content_Title", "Content_Item_Link", "Status_Navigation_ID", "Status_Level", "Status_Sort_Order"];
			var fieldDic = {
				"CSP_Unique_Key"		: "identifier",
				"Content_Title"			: "title",
				"Content_Item_Link"		: "link",
				"Status_Navigation_ID"	: "parent",
				"Status_Level"			: "level",
				"Status_Sort_Order"		: "displayorder"
			}
			var tabNavList = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return tabNavList;
		},
		setData: function (tnList) {
			data = tnList;
		}
	}
}]);

//Products Factory
ngSiemensv2.factory('Products', ['$rootScope', '$http', '$filter', function ProductsFactory($rootScope, $http, $filter) {
	var rootId = 1;
	var categoryProductId = 2;
	var data = [];
	
	var GetCountryFilteredList = function (resultData) {
		if (resultData.length == 0) return;
		if (typeof(cspConsumerInfo) == 'undefined' || typeof(cspConsumerInfo.country) == undefined || typeof(resultData[0].availablecountry) == 'undefined') {
			//console.log('Invalid cspConsumerInfo');
			return;
		}
		
		var consumerCountry = cspConsumerInfo.country;
		for (var w = resultData.length - 1; w >= 0; w--) {
			if (resultData[w].availablecountry != '') {
				var countryList = resultData[w].availablecountry.split(',');
				if (countryList.indexOf(consumerCountry) == -1) {
					resultData.splice(w, 1);
				}
			}
		}
	};
	
	return {
		promise: {},
		rootid: function () {
			return rootId;
		},
		mainproductid: function () {
			return categoryProductId;
		},
		all: function () {
			return data;
		},
		setData: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 117;
			var ctId = '31000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ['CSP_Unique_Key', 'Content_Title', 'Content_Title_Secondary', 'File_Image_Thumbnail_Url', 'File_Image_Url', 'Content_Description_Short', 'Content_Description_Long', 'Local_Type'
			, 'Local_Syndication_Type', 'Status_Sort_Order', 'Status_Promotion_Y_N', 'ContentCategoryID', 'ContentCategoryParentID', 'ContentCategoryLineage', 'ContentCategoryDepth'
			, 'File_Presentation_1_Name', 'CSP_Reporting_Id', 'CSP_Level','Content_Description_Extended', 'Content_Information_Detail', 'Local_Target_Group', 'File_Video_Url'
			, 'File_Image_Button_Name', 'File_Image_Button_Url'];
			var fieldDic = {
				"CSP_Unique_Key"			: "cspuniquekey",		
				"Content_Title"				: "title",	
				"Content_Title_Secondary"	: "secondarytitle",				
				"File_Image_Thumbnail_Url"	: "thumbnail",				
				"File_Image_Url"			: "image",
				"File_Video_Url"			: "filevideourl",
				"Content_Description_Short"	: "description",				
				"Content_Description_Long"	: "descriptionlong",				
				"Local_Type"				: "type",	
				"Local_Syndication_Type"	: "syndicationtype",				
				"Status_Sort_Order"			: "displayorder",		
				"Status_Promotion_Y_N"		: "feature",			
				"ContentCategoryID"			: "categoryId",		
				"ContentCategoryParentID"	: "parentId",				
				"ContentCategoryLineage"	: "lineage",				
				"ContentCategoryDepth"		: "depth",			
				"File_Presentation_1_Name"	: "textorientation",				
				"CSP_Reporting_Id"			: "reportingId",		
				"CSP_Level"					: "cspLevel",
				"Content_Information_Detail": "Content_Information_Detail",
				"Content_Description_Extended" : "Content_Description_Extended",
				"Local_Target_Group"			: "availablecountry",
				"File_Image_Button_Name"	: "buttontitle",
				"File_Image_Button_Url"		: "buttonlink"
			}
			data = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			data = $filter('filter')(data, {syndicationtype: 'showcase'}, function (expected, actual) {return (expected == '' || expected.toLowerCase() == 'all' || expected.toLowerCase() == actual);});
			GetCountryFilteredList(data);
		},
		convertJSONtoTree: function (arry) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(arry);
			//var fullArray = arry;
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			
			return roots[0].children;
		}
	}
}]);

//Leadgen Factory
ngSiemensv2.factory('LeadgenForm', ['$rootScope', '$http', function LeadgenForm ($rootScope, $http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		parseLeadgenItem: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 45;
			var ctId = '15000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["Status_Form_Contact_Us", "Status_FormField_Id", "Content_FormFieldLabel", "Status_Formfield_Name", "Content_Title"    , "Content_Description_Short", "Status_Formfield_Type", "Content_RequiredField_ErrorMessage", "Status_Formfield_Validation_Type", "Status_Formfield_Validation_Y_N", "Status_Form_View_Asset", "Status_Form_Trial"];
			var fieldDic = {
				"Status_Form_Contact_Us"			: "statuscontactus",
				"Status_FormField_Id"				: "fieldid",
				"Content_FormFieldLabel"			: "fieldlabel",
				"Status_Formfield_Name"				: "fieldname",
				"Content_Title"					    : "contenttitle",
				"Content_Description_Short"			: "description",
				"Status_Formfield_Type"				: "type",
				"Content_RequiredField_ErrorMessage": "fielderrormessage",			
				"Status_Formfield_Validation_Type"	: "fieldvalidationtype",
				"Status_Formfield_Validation_Y_N"	: "statusvalidation",
				"Status_Form_View_Asset"			: "statusformviewasset",
				"Status_Form_Trial"					: "statusformtrialrequest"
			}
			var jsonLeadgenList = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return jsonLeadgenList;
		},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			//formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
}]);

ngSiemensv2.factory('Translation', ['$rootScope', function Translation ($rootScope) {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		parse: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 50;
			var ctId = '35000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["CSP_Context_Id", "Content_Title", "Content_Description_Long"];
			var fieldDic = {
				"CSP_Context_Id"			: "contextid",
				"Content_Title"    			: "value",
				"Content_Description_Long"	: "descriptionlong"
			}
			data = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return data;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
}]);

//Main Navigation Controller
ngSiemensv2.controller('MainNavController', ['$http', '$rootScope', 'TabsAndNavigation', 'Products', function($http, $rootScope, TabsAndNavigation, Products){
	var self = this;
	this.navitems = [];
	this.allNavItems = [];
	this.prodListLoaded = false;
	
	this.GetFullLink = function (data, i) {
		if (data[i].parent == '') {
			if (data[i].link != '')
				data[i].link = "#/" + data[i].link;
			else
				data[i].link = "#/";
			return data[i].link;
		}
		
		for (var j = 0; j < data.length; j++) {
			if (data[j].identifier == data[i].parent) {
				var bDomain = data[j].link.indexOf('#') != 0 ? $rootScope.GetBaseDomain() + "#/" : $rootScope.GetBaseDomain();
				if (data[j].link.indexOf(document.domain) == -1)
					data[i].link = bDomain + data[j].link + '/' + data[i].link;
				else
					data[i].link = data[j].link + '/' + data[i].link;
					
				return data[i].link;
			}
		}
	}
	
	this.manipulateTopNav = function (rawList) {
		var tabNavList = rawList;
		
		for (var i = 0; i < tabNavList.length; i++) {
			tabNavList[i].displayorder = parseInt(tabNavList[i].displayorder);
			if (tabNavList[i].level == '') tabNavList[i].level = 1;
			tabNavList[i].level == parseInt(tabNavList[i].level);
			tabNavList[i].nodes = [];
			tabNavList[i].hideSubMenu = false;
			tabNavList[i].hasNodes = false;
			tabNavList[i].dataToggleValue = '';
			if (tabNavList[i].level == '' || tabNavList[i].level < 2) {
				for (var j = 0; j < tabNavList.length; j++) {
					if (tabNavList[j].parent == tabNavList[i].identifier || tabNavList[i].identifier == 'products') {
						tabNavList[i].hasNodes = true;
						tabNavList[i].dataToggleValue = 'dropdown';
						break;
					}
				}
			}
			tabNavList[i].link = self.GetFullLink(tabNavList, i);
			if (tabNavList[i].level <= 1) {
				self.navitems.push(tabNavList[i]);
			}	
			else
				self.allNavItems.push(tabNavList[i]);
		}
		
		for (var i = 0; i < self.navitems.length; i++) {
			if (self.navitems[i].hasNodes == true)
				self.navitems[i].link = '';
		}
	};
	
	//Show/hide for sub menu
	this.showSubItems = function (data) {
		data.hideSubMenu = false;
		if (data.nodes.length > 0) return;
		for (var i = 0; i < self.allNavItems.length; i++) {
			if (self.allNavItems[i].parent == data.identifier) {
				data.nodes.push(self.allNavItems[i]);
			}
		}
		window.setTimeout(function() {
			jQuery(".navbar li a").each(function () {
				this.style.setProperty( 'color',"#" +  $rootScope.menuFontColor, 'important' );
			});
		},100);	
	};
	
	this.GetChildNodes = function (parentNode, treeNode, currentLevel, maxLevel) {
		var childNodes = [];
		var productNode;
		
		for (var i = 0; i < treeNode.children.length; i++) {
			var childNode = treeNode.children[i];
			if (childNode.value.syndicationtype != '' && childNode.value.syndicationtype.toLowerCase() != 'all' && childNode.value.syndicationtype.toLowerCase() != 'showcase') continue;

			var temp =
				{
					"identifier" : childNode.value.categoryId,
					"title" : childNode.value.title,
					"link" : $rootScope.GetBaseDomain() + "#/products/" + convertCSPUniqueKey(childNode.value.cspuniquekey),// childNode.value.categoryId,
					"parent" : parentNode.identifier,
					"level" : childNode.value.depth,
					"displayorder" : childNode.value.displayorder
				};
			temp.nodes = [];
			temp.hideSubMenu = false;
			temp.hasNodes = (childNode.children != undefined && currentLevel < maxLevel);
			temp.dataToggleValue = '';
			if (temp.hasNodes)
				this.GetChildNodes(temp, childNode, currentLevel + 1, maxLevel);
			childNodes.push(temp);
		}
		parentNode.nodes = childNodes;
	}
	
	this.UpdateProductsMenu = function (maxLevel) {
		if ($rootScope.AllProducts == undefined) return;
		var prodItemIndex = 0;
		for (var i = 0; i < self.navitems.length; i++) {
			if (self.navitems[i].identifier == 'products') {
				prodItemIndex = i;
				break;
			}
		}
		
		for (var j = 0; j < $rootScope.AllProducts.length; j++) {
			if ($rootScope.AllProducts[j].value.categoryId == Products.mainproductid()) {
				self.GetChildNodes(self.navitems[prodItemIndex], $rootScope.AllProducts[j], 1, maxLevel);
				self.prodListLoaded = true;
				break;
			}
		}
		window.setTimeout( function(){
			jQuery("#main-nav-bar").css("visibility","visible");
		},400);
	};

	TabsAndNavigation.promise.then(function (responseData) {
		self.manipulateTopNav(responseData);
		if (!$rootScope.ProductDeferLoaded)
			$rootScope.CallProductDefer();
	});
	
	Products.promise.then(function (prodData) {
		$rootScope.AllProducts = Products.convertJSONtoTree(prodData);
		self.UpdateProductsMenu($rootScope.menuLevelToShow);
	});
}]);

//Home Page Controller
ngSiemensv2.controller('MainController', ['$http', '$rootScope', '$filter', 'Products', function($http, $rootScope, $filter, Products){
	var self = this;
	this.tileBlocks = [];
	this.homeBlocks = [];
	
	this.GetLinkTarget = function (itemLink) {
		if (self.IsExternalLink(itemLink))
			return '_blank';
		return '_self';
	};
	
	this.IsExternalLink = function (theLink) {
		if (theLink.indexOf('#') == 0) return false;
		var comp = new RegExp(location.host);
		return !comp.test(theLink);
	};
	
	this.GetTileBlockList = function (categoryBlocks, defaultBlocks) {
		if (categoryBlocks.length == 0) return defaultBlocks;
		
		categoryBlocks.sort(function(x, y){
			var sortOrder_1 = parseInt(x.displayorder);
			var sortOrder_2 = parseInt(y.displayorder);
			if (x < y) {
				return -1;
			}
			if (x > y) {
				return 1;
			}
			return 0;
		});
		
		for (var i = 0; i < categoryBlocks.length; i++)
			categoryBlocks[i].displayorder = parseInt(categoryBlocks[i].displayorder);
		
		var nextOrder = categoryBlocks[categoryBlocks.length - 1].displayorder + 1;
		for (var j = 0; j < defaultBlocks.length; j++)
			defaultBlocks[j].displayorder = nextOrder + j;
		
		return categoryBlocks.concat(defaultBlocks);
	};
	
	//Home blocks
	$http.get('d2.aspx?p2002(ct37000&cd1i99)[st(ct37000*Status_Sort_Order*)]' + $rootScope.currentTick).success(function(data) {
		if (data.length == 0) return;
		var ctId = '37000';
		var fieldSplit = 0;
		var fieldList = ["Content_Title", "File_Image_Thumbnail_Url", "File_Image_Url", "Content_Description_Short", "Content_Description_Long", "Local_Type", "File_Presentation_1_Url", "ContentCategoryID", "CSP_Reporting_Id", "Status_Sort_Order"];
		var fieldDic = {
			"Content_Title"				: "title",
			"File_Image_Thumbnail_Url"	: "thumbnail",
			"File_Image_Url"			: "image",
			"Content_Description_Short"	: "description",
			"Content_Description_Long"	: "descriptionlong",
			"Local_Type"				: "type",
			"File_Presentation_1_Url"	: "link",
			"ContentCategoryID"			: "cspcategoryid",
			"CSP_Reporting_Id"			: "reportingId",
			"Status_Sort_Order"			: "displayorder"
		};
		fieldSplit = $rootScope.GetFieldSplitNo(ctId, data);
		data = $rootScope.ParseObject(data, fieldSplit, ctId, fieldList, fieldDic);
		
		self.homeBlocks = data;
		var tileBlocksData = $filter('filter')(data, {type: 'tile'}, function (expected, actual) {return expected == actual;});
		
		var categoryBlocks = $filter('filter')(tileBlocksData, {cspcategoryid: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) != parseInt(actual);});
		var defaultBlocks = $filter('filter')(tileBlocksData, {cspcategoryid: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});

		self.tileBlocks = self.GetTileBlockList(categoryBlocks, defaultBlocks);
		if (self.tileBlocks.length > 3)
			self.tileBlocks = self.tileBlocks.slice(0, 3);
			
		$rootScope.updatePageInfo(null, 'Home');				
	});
}]);

//Products Controller
ngSiemensv2.controller('ProductsController', 
['$http', '$rootScope', '$filter', '$routeParams', '$timeout', '$sce', 'Products', 'LeadgenForm', 'Translation', 
function($http, $rootScope, $filter, $routeParams, $timeout, $sce, Products, LeadgenForm, Translation){
	var self = this;
	
	this.cspKey = typeof($routeParams.id) == "undefined" ? 1 : $routeParams.id;
	this.cspKey = $rootScope.validateCspKey(this.cspKey);
	this.exceptionValue = '';
	this.currentItem = [];
	this.prodRows = [];
	this.breadcrumbs = [];
	this.blockData = [];
	this.prodContentRows = [];
	this.prodContentFeaturedBlocks = [];
	this.assetRows = [];
	this.pageNotFound = false;
	
	//Contact us block
	this.btnContactUsText = 'Contact Us';
	this.btnTweetUsText = 'Tweet Us'
	this.readyToBuy = 'readytobuy';
	this.textLearnMore = 'learnmore';
	this.Supplier = $rootScope.Supplier;
	
	if (typeof($routeParams.assettype)!="undefined") {
		this.exceptionValue = $routeParams.assettype;
	}
	
	if (!$rootScope.ProductDeferLoaded)
		$rootScope.CallProductDefer();
		
	this.getUniqueKey = function (cspKey) {
		if (cspKey == undefined) return '';
		return convertCSPUniqueKey(cspKey);
	};
		
	this.fillImages = function (prodItem) {
		if (prodItem == undefined || prodItem.image == undefined || prodItem.thumbnail == undefined) return;
		if (prodItem.image == '')
			prodItem.image = 'http://placehold.it/656x292';
		if (prodItem.thumbnail == '')
			prodItem.thumbnail = 'http://placehold.it/322x259';
	};
	
	
	this.GetBlockStyle = function(blockBgColor,blockFontColor){
		var blockStyle;
		var bgCode;
		var bgCodeStyle = '';
		var fontColor;
		var fontColorStyle = '';
		
		if (typeof(blockBgColor) != 'undefined' && blockBgColor != ''){
			bgCode = blockBgColor.replace('#','');
			bgCodeStyle = 'background-color: #' + bgCode + ";";
		}
		
		if (typeof(blockFontColor) != 'undefined' && blockFontColor != ''){
			fontColor = blockFontColor.replace('#','');
			fontColorStyle = 'color: #' + fontColor + ';';
		}
		
		return bgCodeStyle + fontColorStyle;
		
	};
	
	//console.log(this.GetBlockStyle());
	// this.TrustHtmlContent = function (htmlString) {
		// console.log(htmlString);
		// return $sce.trustAsHtml(htmlString);
	// };
	
	this.GetChildrenList = function (data) {
		self.blockData = $filter('filter')(data, {parentId: self.currentItem.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		
		//Call report because no child
		if (self.blockData.length == 0) {
			$rootScope.updatePageInfo(null, self.currentItem.title);
			return;
		}
		
		var addedItem = 0;
		while (addedItem < self.blockData.length) {			
			var temp = [];
			
			self.fillImages(self.blockData[addedItem]);
			self.fillImages(self.blockData[addedItem + 1]);
			
			temp.push(self.blockData[addedItem]);
			addedItem++;
			if (self.blockData[addedItem - 1].feature.toLowerCase() == 'yes' || self.blockData[addedItem] == undefined) {
				self.prodRows.push(temp);
				continue;
			}
			
			if (self.blockData[addedItem].feature.toLowerCase() != 'yes') {
				temp.push(self.blockData[addedItem]);
				addedItem++;
			}
			self.prodRows.push(temp);
		}
	};
	
	this.RequestForBreadcrumb = function (categoryId, breadcrumbInfo, productList) {
		for (var i = 0; i < productList.length; i++) {
			if (productList[i].categoryId == categoryId) {
				breadcrumbInfo.unshift({title: productList[i].title, link: $rootScope.GetBaseDomain() + '#/products/' + convertCSPUniqueKey(productList[i].cspuniquekey)});
				if (productList[i].parentId != Products.rootid())
					self.RequestForBreadcrumb(productList[i].parentId, breadcrumbInfo, productList);
				break;
			}
		}
		
		if (categoryId == Products.mainproductid() || categoryId == Products.rootid()) {
			breadcrumbInfo[0].link = $rootScope.GetBaseDomain() + '#/';//Disable the link of "Products" item
			breadcrumbInfo[0].isProduct = true;
			breadcrumbInfo.unshift({title: 'Home', link: $rootScope.GetBaseDomain() + '#/'});
			self.breadcrumbs = breadcrumbInfo;
		}
	};
	
	this.GenerateBreadcrumb = function (data) {
		var requestedBreadcrumb = [];
		if (self.exceptionValue == '')
			self.RequestForBreadcrumb(self.currentItem.parentId, requestedBreadcrumb, data);
		else {
			self.breadcrumbs.push({title: 'Home', link: $rootScope.GetBaseDomain() + '#/'});
		}
	};
	
	//New update: build for Case Study
	this.GetRelatedAssets = function (categoryId) {
		$http.get('d1.aspx?p2004(ct21000&cd' + categoryId + 'i0)[st(ct21000*Status_Sort_Order*)]' + $rootScope.currentTick).success(function(data) {
			data.pop();
			
			for (var i = 0; i < Math.round((data.length) / 2); i++) {
				var temp = [];
				temp.push(data[i * 2]);
				if (i * 2 + 1 < data.length) {
					temp.push(data[i * 2 + 1]);
				}
				for (var j = 0; j < temp.length; j++) {
					temp[j].imgurl = (temp[j].imgurl == '') ? 'http://www.placehold.it/660x300' : temp[j].imgurl;
					switch (temp[j].launchtype) {
						case 'newwindow': 
							temp[j].target = "_blank";
							break;
						case 'lightbox':
							temp[j].target = "_blank";
							break;
						default:
							temp[j].target = "_self";
					}
					temp[j].launchtype = 'csp-' + temp[j].launchtype;
					temp[j].leadgen = '';
					if (temp[j].statusleadgen.toLowerCase() == 'yes') {
						temp[j].leadgen = 'call-leadgen';
					}
					if (temp[j].type == 'casestudy') {
						var tempTitleArray = temp[j].title.toLowerCase().split(" ");
						var urlTitle = encodeURIComponent(tempTitleArray.join('-'));
						temp[j].linkurl = '#/casestudy/' + urlTitle + '/' + temp[j].cspuniquekey;
						temp[j].target = '';
						temp[j].launchtype = '';
						temp[j].leadgen = '';
					}
				}
				
				self.assetRows.push(temp);
			};
		});
	};
	
	this.GetPageContentBlocks = function (categoryId) {
		self.prodContentRows = [];
		self.prodContentFeaturedBlocks = [];
		$http.get('d1.aspx?p2009(ct38000&cd' + categoryId + ')[st(ct38000*Status_Sort_Order*)]' + $rootScope.currentTick).success(function(data) {
			if (data.length > 1)
				data.pop();
			var addedItem = 0;
			while (addedItem < data.length) {
				var temp = [];
				temp.push(data[addedItem]);
				addedItem++;
				if (data[addedItem - 1].imageUrl != '' || data[addedItem] == undefined) {
					self.prodContentRows.push(temp);
					continue;
				}
				if (data[addedItem].imageUrl == '') {
					temp.push(data[addedItem]);
					addedItem++;
				}
				self.prodContentRows.push(temp);
			}
		});
	};
	
	this.GetBlocks = function (data) {
		if (self.blockData.length > 0) {
			return;
		}
		if (self.prodRows.length == 0) {
			var rowTemp = [];
			var temp = {
				categoryId: self.currentItem.categoryId,
				image: self.currentItem.image,
				filevideourl: self.currentItem.filevideourl,
				title: (self.currentItem.secondarytitle != '') ? self.currentItem.secondarytitle : self.currentItem.title,
				description: self.currentItem.description,
				textorientation: self.currentItem.textorientation,
				feature: 'yes',
				lastItem: true,
				buttontitle: self.currentItem.buttontitle,
				buttonlink: self.currentItem.buttonlink
			}
			rowTemp.push(temp)
			self.prodRows.push(rowTemp);
		}
		
		self.GetPageContentBlocks(self.currentItem.categoryId);
		self.GetRelatedAssets(self.currentItem.categoryId);
	};
		
	Products.promise.then(function(data){
		self.currentItem = $filter('filter')(data, {cspuniquekey: self.cspKey}, function (expected, actual) {
			return convertCSPUniqueKey(expected).toLowerCase() == actual.toLowerCase();
		});
		
		//Category not found - Redirect to Home page
		if (self.currentItem.length == 0) {
			self.currentItem = [];
			self.currentItem.title = '404 - Page Not Found';
			self.pageNotFound = true;
		}
		
		if (!self.pageNotFound) {
			self.currentItem = self.currentItem[0];
			
			//Set page title
			$rootScope.pageTitle = self.currentItem.title;
			
			//Get all children item
			self.GetChildrenList(data);
			
			//Get blocks if there is no child item
			self.GetBlocks(data);
			
			self.GenerateBreadcrumb(data);
			
			//Init Leadgen request
			if (!$rootScope.LeadgenDeferLoaded)
				$rootScope.CallLeadgenDefer();
		}
		
		//Init Translation request
		if (!$rootScope.TranslationDeferLoaded)
			$rootScope.CallTranslationDefer();
	});
	
	//Translation for Contact Us block at footer
	Translation.promise.then(function (resultData) {
		self.btnContactUsText = Translation.translate(resultData, self.btnContactUsText);
		self.btnTweetUsText = Translation.translate(resultData, self.btnTweetUsText);
		self.readyToBuy = Translation.translate(resultData, self.readyToBuy);
		self.textLearnMore = Translation.translate(resultData, self.textLearnMore);
		self.readyToBuy = self.readyToBuy.replace('{consumer:FPersonalize_Contact_Phonenumber}', $rootScope.Supplier.telephone);
	});
	
	//Submit gated asset leadgen form & call report
	this.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionType"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionContent"] = 'ContactUs';
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};

	this.submit = function() {
		LeadgenForm.submitLeadgen('frmleadgen', self.submitted);
	};
	
	//Get gated asset leadgen form
	LeadgenForm.promise.then(function (resultData) {
		downloadFormFields = LeadgenForm.getDownloadFormFields();
	});
}]);

//Asset Controller
ngSiemensv2.controller('AssetController', 
['$http', '$rootScope', '$filter', '$routeParams', '$timeout', '$sce', 'TabsAndNavigation', 'LeadgenForm', 'Translation', 
function($http, $rootScope, $filter, $routeParams, $timeout, $sce, TabsAndNavigation, LeadgenForm, Translation){
	var self = this;
	var labelCaseStudy = 'casestudy';
	this.assetType = typeof($routeParams.assettype)=="undefined"?'':$routeParams.assettype;
	this.ajaxLoading = true;
	this.assetLongDescription = '';
	this.assetTitle = '';
	this.breadcrumbs = [];
	var assetData = [];
	var featuredData = [];
	var localVertical = new Array();
	this.localVerticalName = [];
	var assetLocalTypes = []
	var currentItem;
	this.featuredRows = [];
	this.featuredAssets = [];
	
	switch (this.assetType) {
		case 'assets':
			assetLocalTypes = ['tools', 'brochure', 'datasheet', 'video', 'podcast'];
			break;
		default:
			assetLocalTypes = [this.assetType];
	}
	
	this.getBreadcrumbItem = function (dataList, currentItem, crrBreadcrumbs) {
		var nextItem;
		for (var i = 0; i < dataList.length; i++) {
			if (currentItem.parent == '') {
				if (dataList[i].identifier.toLowerCase() == 'home') {
					crrBreadcrumbs.push(dataList[i]);
					break;
				}
			}
			else if (dataList[i].identifier == currentItem.parent) {
				crrBreadcrumbs.push(dataList[i]);
				nextItem = dataList[i];
				break;
			}
		}
		if (nextItem != undefined)
			self.getBreadcrumbItem(dataList, nextItem, crrBreadcrumbs);
	};
	
	//Title
	$http.get('d1.aspx?p2001(ct34000)[]' + $rootScope.currentTick).success(function(data) {
		data.pop();
		for (var i = 0; i < data.length; i++) {
			if (data[i].identifier == self.assetType) {
				self.assetTitle = data[i].title;
				$rootScope.updatePageInfo(null, data[i].title);
				break;
			}
		}
	});
	
	TabsAndNavigation.promise.then(function (data) {
		var defaultBreadcrumb = [];
		var crrAsset;
		for (var i = 0; i < data.length; i++) {
			if (data[i].identifier == self.assetType) {
				crrAsset = data[i];
				break;
			}
		}
		//defaultBreadcrumb.push(crrAsset);
		self.getBreadcrumbItem(data, crrAsset, defaultBreadcrumb);
		defaultBreadcrumb.reverse();
		self.breadcrumbs = defaultBreadcrumb;
	});
	
	//Asset description
	Translation.promise.then(function (resultData) {
		var textItem = $filter('filter')(resultData, {contextid: self.assetType}, function (expected, actual) {return expected == actual;});
		if (textItem.length > 0)
			self.assetLongDescription = textItem.descriptionlong;
	});
	
	//Get assets
	$http.get('d1.aspx?p2004(ct21000)[st(ct21000*CSP_DeDup_Key,Status_Sort_Order*)]' + $rootScope.currentTick).success(function(data) {
		data.pop();
		if (data.length == 0) return;
		data = $filter('filter')(data, {type: self.assetType}, function (expected, actual) { 
			if (self.assetType == labelCaseStudy)
				return expected.toLowerCase() == labelCaseStudy; 
			return expected.toLowerCase() != labelCaseStudy;
		});
		
		for (var j = 0; j < data.length; j++) {
			if (assetLocalTypes.length == 1 && assetLocalTypes[0] == labelCaseStudy) {
				var tempTitleArray = data[j].title.toLowerCase().split(" ");
				var urlTitle = encodeURIComponent(tempTitleArray.join('-'));
				data[j].linkurl = '#/casestudy/' + urlTitle + '/' + data[j].cspuniquekey;
				data[j].launchtype = 'currentwindow';
			}
			switch (data[j].launchtype) {
				case 'newwindow': 
					data[j].target = "_blank";
					break;
				case 'lightbox':
					data[j].target = "_blank";
					break;
				default:
					data[j].target = "_self";
			}
			data[j].launchtype = 'csp-' + data[j].launchtype;
			
			data[j].leadgen = '';
			if (data[j].statusleadgen.toLowerCase() == 'yes') {
				data[j].leadgen = 'call-leadgen';
			}
				
			var blockType = data[j].localvertical;
			if (assetLocalTypes.length > 1)
				blockType = data[j].type;
			if (localVertical[blockType] == undefined) {
				localVertical[blockType] = [];
				self.localVerticalName.push(blockType);
			}
			if (data[j].featured.toLowerCase() == "yes")
				featuredData.push(data[j]);
			else {
				assetData.push(data[j]);
				localVertical[blockType].push(data[j]);
			}
		}

		featuredData.sort(function(a, b) {
			return (a.sortorder<b.sortorder)?-1:((a.sortorder>b.sortorder)?1:0);
		});

		
		for (var k = self.localVerticalName.length - 1; k >= 0; k--) {
			if (localVertical[self.localVerticalName[k]].length == 0)
				self.localVerticalName.splice(k, 1);
		}
		
		var groupOrder = ['datasheet', 'brochure', 'whitepaper', 'report', 'video'];
		for (var i = 0; i < self.localVerticalName.length; i++) {
			var tempItem = '';
			var supposedIndex = groupOrder.indexOf(self.localVerticalName[i]);
			if (supposedIndex != -1 && self.localVerticalName[supposedIndex] != self.localVerticalName[i]) {
				tempItem = self.localVerticalName[supposedIndex];
				self.localVerticalName[supposedIndex] = self.localVerticalName[i];
				self.localVerticalName[i] = tempItem;
			}
		}
		
		self.featuredAssets = featuredData.splice(0, 1);
		
		for (var i = 0; i < Math.round((featuredData.length) / 2); i++) {
			var temp = [];
			temp.push(featuredData[i * 2]);
			if (i * 2 + 1 < featuredData.length) {
				temp.push(featuredData[i * 2 + 1]);
			}
			self.featuredRows.push(temp);
		}
		
		self.groupRows = [];
		for (var i = 0; i < Math.round(self.localVerticalName.length / 2); i++) {
			var tempRow = [];
			tempRow.push({'typename': self.localVerticalName[i * 2], 'assets': localVertical[self.localVerticalName[i * 2]]});
			if (i * 2 + 1 < self.localVerticalName.length) {
				tempRow.push({'typename': self.localVerticalName[i * 2 + 1], 'assets': localVertical[self.localVerticalName[i * 2 + 1]]});
			}
			self.groupRows.push(tempRow);
		}
		
		self.temp = '';
		
		Translation.promise.then(function (resultData) {
			for (var j = 0; j < self.groupRows.length; j++) {
				for (var k = 0; k < self.groupRows[j].length; k++) {
					self.groupRows[j][k].typename = Translation.translate(resultData, self.groupRows[j][k].typename);
				}
			}
			self.ajaxLoading = false;
		});
			
		if (!$rootScope.TranslationDeferLoaded)
			$rootScope.CallTranslationDefer();
			
		if (!$rootScope.LeadgenDeferLoaded)
			$rootScope.CallLeadgenDefer();
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
	
	this.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionType"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionContent"] = 'ContactUs';
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};

	this.submit = function() {
		LeadgenForm.submit('frmleadgen', self.submitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		self.formHeaderTitle = LeadgenForm.getFormTitle();
		self.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		self.supplier = LeadgenForm.getSupplierInfo();
		self.fieldList = $rootScope.TrustHtmlContent(LeadgenForm.getDownloadFormFields());
		jQuery(document).click();
	});
}]);

//Asset Detail Controller
ngSiemensv2.controller('AssetDetailController', 
['$http', '$rootScope', '$filter', '$routeParams', '$timeout', '$sce', 'TabsAndNavigation', 'LeadgenForm', 'Translation', 
function($http, $rootScope, $filter, $routeParams, $timeout, $sce, TabsAndNavigation, LeadgenForm, Translation){
	var self = this;
	var assetTitle = typeof($routeParams.assettitle)=="undefined"?'':$routeParams.assettitle;
	var assetReportId = typeof($routeParams.id)=="undefined"?'':$routeParams.id;
	assetReportId = $rootScope.validateCspKey(assetReportId);
	var isPageValid = false;
	var requestURL = 'd2.aspx?p2004(ct21000&fLocal_Type~1=casestudy!)[]' + $rootScope.currentTick;
	this.assetItem = [];
	this.assetTitle = '';
	this.assetBanner = '';
	this.assetDescription = '';
	this.assetLongDescription = '';
	this.assetFileLinkUrl = '';
	this.assetFileType = '';
	this.assetFileDownloadTitle = '';
	this.assetFileLinkLaunchType = '';
	this.assetFileLinkTarget = '';
	this.breadcrumbs = [];
	
	$http.get(requestURL).success(function(data) {
		var ctId = '21000';
		var fieldSplit = 0;
		var fieldList = ["ContentInstanceID", "Content_Title", "Content_Download_Title", "Content_Link_Url", "Local_File_Type", "Content_Download_Title", "Content_File_Url", "Content_Image_Url", "Content_Image_Thumbnail", "Local_File_LaunchType", "ContentCategoryID", "Content_Description_Short", "Content_Description_Long", "Local_Type", "Local_Featured", "Status_Featured_Y_N", "Status_Sort_Order", "Local_Vertical", "Status_LeadGen_Y_N", "Content_Asset_Date", "CSP_Reporting_Id", "Local_Partner_Type", "Local_Featured", "Status_Secure_Y_N", "CSP_Unique_Key","Content_File_Name", "Content_File_Url_Original"];
		var fieldDic = {
			"ContentInstanceID"			: "contentid",
			"Content_Title"				: "title",
			"Content_Download_Title"	: "downloadtitle",
			"Content_Link_Url"			: "linkurl",
			"Local_File_Type"			: "filetype",
			"Content_Download_Title"	: "filedownloadtitle",
			"Content_File_Url"			: "fileurl",
			"Content_Image_Url"			: "imgurl",
			"Content_Image_Thumbnail"	: "thumbnail",
			"Local_File_LaunchType"		: "launchtype",
			"ContentCategoryID"			: "cspcategoryid",
			"Content_Description_Short"	: "description",
			"Content_Description_Long"	: "descriptionlong",
			"Local_Type"				: "type",
			"Local_Featured"			: "localfeatured",
			"Status_Featured_Y_N"		: "featured",
			"Status_Sort_Order"			: "sortorder",
			"Local_Vertical"			: "localvertical",
			"Status_LeadGen_Y_N"		: "statusleadgen",
			"Content_Asset_Date"		: "datetime",
			"CSP_Reporting_Id"			: "reportingId",
			"Local_Partner_Type"		: "localpartnertype",
			"Local_Featured"			: "cobrandType",
			"Status_Secure_Y_N"			: "secured",
			"CSP_Unique_Key"			: "cspuniquekey",
			"Content_File_Name"			: "buttontitle",
			"Content_File_Url_Original" : "buttonlink"
		};
		fieldSplit = $rootScope.GetFieldSplitNo(ctId, data);
		data = $rootScope.ParseObject(data, fieldSplit, ctId, fieldList, fieldDic);
		
		if (data.length < 1)
			isPageValid = false;
		else {
			for (var i = 0; i < data.length; i++) {
				if (data[i].cspuniquekey.toLowerCase() == assetReportId.toLowerCase() || data[i].reportingId.toLowerCase() == assetReportId.toLowerCase()) {
					var tmpContent = data[i];
					data[i] = data[0];
					data[0] = tmpContent;
					isPageValid = true;
					break;
				}
			}
		}
		
		if (!isPageValid) {
			data = $filter('filter')(data, {contentid: assetReportId}, function (expected, actual) {
				return expected == actual;
			});
			isPageValid = data.length > 0;
		}
		if (!isPageValid) {
			self.assetTitle = '404 - Not Found';
			return;
		}
				
		$rootScope.updatePageInfo(null, data[0].title);
		var getBreadcrumbItem = function (dataList, currentItem, crrBreadcrumbs) {
			var nextItem;
			for (var i = 0; i < dataList.length; i++) {
				if (currentItem.parent == '') {
					if (dataList[i].identifier.toLowerCase() == 'home') {
						crrBreadcrumbs.push(dataList[i]);
						break;
					}
				}
				else if (dataList[i].identifier == currentItem.parent) {
					crrBreadcrumbs.push(dataList[i]);
					nextItem = dataList[i];
					break;
				}
			}
			if (nextItem != undefined)
				getBreadcrumbItem(dataList, nextItem, crrBreadcrumbs);
		};
		
		TabsAndNavigation.promise.then(function (tabsnavData) {
			var defaultBreadcrumb = [];
			var crrAsset;
			for (var i = 0; i < tabsnavData.length; i++) {
				if (tabsnavData[i].identifier == data[0].type) {
					crrAsset = tabsnavData[i];
					break;
				}
			}
			if (crrAsset != undefined) {
				defaultBreadcrumb.push(crrAsset);
				getBreadcrumbItem(tabsnavData, crrAsset, defaultBreadcrumb);
				defaultBreadcrumb.reverse();
				self.breadcrumbs = defaultBreadcrumb;
			}
		});
		
		switch (data[0].launchtype) {
			case 'newwindow': 
				self.assetFileLinkTarget = "_blank";
				break;
			case 'lightbox':
				self.assetFileLinkTarget = "_blank";
				break;
			default:
				self.assetFileLinkTarget = "_self";
		}
		data[0].launchtype = 'csp-' + data[0].launchtype;
		
		self.assetTitle = data[0].title;
		self.assetBanner = data[0].imgurl;
		self.assetDescription = data[0].description;
		self.assetLongDescription = data[0].descriptionlong;
		self.assetFileLinkUrl = data[0].linkurl;
		self.assetFileType = (data[0].filetype != 'video') ? 'document' : data[0].filetype;
		self.assetFileDownloadTitle = data[0].filedownloadtitle;
		self.assetFileLinkLaunchType = data[0].launchtype;
		self.assetItem = data[0];
		setTimeout(function() {
			jQuery(document).click();							
		}, 1000);
	});
}]);

//Contact Us Controller
ngSiemensv2.controller('ContactUsController', ['$rootScope', '$routeParams', '$timeout', '$http', '$location', '$sce', 'LeadgenForm', 
function($rootScope, $routeParams, $timeout, $http, $location, $sce, LeadgenForm){
	var self = this;
	this.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	this.ajaxLoading = false;
	this.supplier = [];
	this.fieldList = '';
	this.formSubmitted = false;
	
	//$timeout(function() { console.log('changed'); }, 3000);
	this.submitted = function (resultMsg) {
		self.formSubmitted = true;
		self.ajaxLoading = false;
		$timeout(function() { self.formSubmitted = false; }, 5000);
		self.contactusMessage = resultMsg;
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionType"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionContent"] = 'ContactUs';
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};

	this.submit = function() {
		if (LeadgenForm.submit('form1', self.submitted) == 1)
			self.ajaxLoading = true;
	};
	
	if (!$rootScope.LeadgenDeferLoaded)
		$rootScope.CallLeadgenDefer();
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(SupplierInfo);
		LeadgenForm.setFields(resultData, self.topicTitle);
		self.formHeaderTitle = LeadgenForm.getFormTitle();
		self.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		self.supplier = LeadgenForm.getSupplierInfo();
		self.fieldList = $sce.trustAsHtml(LeadgenForm.getAllFields());
		$timeout(function() {
			AutoPopulateUserInfo();
			$rootScope.updatePageInfo(null, 'Contact Us');
			jQuery(document).click();							
		}, 1000);
	});
}]);

//manually start up
angular.element(document).ready(function() {
	var mainUnifyApp = document.getElementById("csp-wrapper");
	angular.bootstrap(mainUnifyApp, ["ngSiemens"]);
});