var request;
function cspGetQuerystring(key, default_) {
    if (default_ == null)
        default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}
function loadxml() {
    var partNum = window.unescape(cspGetQuerystring('pn'));
    if (partNum) {
        request = new XMLHttpRequest();
        request.open('GET', "http://lenovo.syndicationtest.com/d2.aspx?p209(ct6&freplacementInfo~1=" + partNum + "!)", false);
        // request.open('GET', "http://sandbox.me.tv.com/lenovoshowcase/xml/d2.xml", false);
        request.onreadystatechange = xmlParser;
        if (request.overrideMimeType) {
            request.overrideMimeType('text/xml');
        }
        request.send();
        var pname = window.unescape(cspGetQuerystring('pname'));
        if (pname && pname != ""){
            setFlyoutTitle(pname);    
        }
    } else {
        //message: the partnumber couldn't be found
    }
}

function xmlParser() {
    if (request.readyState == 4 && request.status === 200) {
        var xmlDoc = null;
        if (request.responseText && window.ActiveXObject) { //process IE (again)
            var xhr;
            try {
                xhr = new ActiveXObject("MSXML2.DOMDocument.3.0");
            } catch (e) {
                try {
                    xhr = new ActiveXObject('Msxml2.XMLHTTP');
                } catch (e) {
                    try {
                        xhr = new ActiveXObject('Microsoft.XMLHTTP');
                    } catch (e2) {
                        try {
                            xhr = new XMLHttpRequest();
                        } catch (e3) {
                            xhr = false;
                        }
                    }
                }
            }
            if (xhr) {
                try { xhr.loadXML(request.responseText); }
                catch (e) {
                    alert("Your IE Browser couldn't handle the response. Please upgrade it to a newer version\n\n" + e);
                }
                if (xhr.xml) {
                    xmlDoc = xhr;
                }
            }
        } else if (request.responseXML) {
            xmlDoc = request.responseXML;
        }
        if (xmlDoc) {
            var rMgr = new recordMgr();
            var ContentPieceIterator = null;
            //var nsResolver = xmlDoc.createNSResolver(xmlDoc.ownerDocument == null ? xmlDoc.documentElement : xmlDoc.ownerDocument.documentElement);
            if (xmlDoc.evaluate) {
                ContentPieceIterator = xmlDoc.evaluate('//ContentPiece6', xmlDoc, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
                //var MfPN = xmlDoc.evaluate('//ContentPiece6[1]/MfPN/text()',xmlDoc , null, XPathResult.STRING_TYPE, null);
                try {
                    var thisNode = ContentPieceIterator.iterateNext();

                    while (thisNode) {
                        var MfPN = thisNode.getElementsByTagName("MfPN")[0].textContent;
                        var ShortDescription = thisNode.getElementsByTagName("ShortDescription")[0].textContent;
                        var ESpecJson = thisNode.getElementsByTagName("ESpecJson")[0].textContent;
                        // rMgr.addRecord(MfPN, ShortDescription, ESpecJson.replace(/\&quot\;/g, '"').replace(/\s{1}\W{1}\s{1}/g, '<br/>'));
                        rMgr.addRecord(MfPN, ShortDescription, ESpecJson.replace(/\&quot\;/g, '"').replace(/\s{1}\¦{1}\s{1}/g, '<br/>'));
                        thisNode = ContentPieceIterator.iterateNext();
                    }
                } catch (e) {
                    dump('Error: Document tree modified during iteration ' + e);
                }
            } else {
                ContentPieceIterator = xmlDoc.documentElement.selectNodes('//ContentPiece6');
                for (var h = 0; h < ContentPieceIterator.length; h++) {
                    var thisNode = ContentPieceIterator[h];
                    var MfPN = thisNode.getElementsByTagName("MfPN")[0].text;
                    var ShortDescription = thisNode.getElementsByTagName("ShortDescription")[0].text;
                    var ESpecJson = thisNode.getElementsByTagName("ESpecJson")[0].text;
                    rMgr.addRecord(MfPN, ShortDescription, ESpecJson.replace(/\&quot\;/g, '"').replace(/\s{1}\W{1}\s{1}/g, '<br/>').replace(/\s{1}\¦{1}\s{1}/g, '<br/>'));
                }
            }

        }
    }
}

function recordMgr() {
    var a = this, b = [];
    a.addRecord = function(MfPN, ShortDescription, ESpecJson) {
        var ind = b.length + 1;
        var cspItem = a.createHTML("div", "csp-Item" + ind, "csp-MainItem" + (ind % 2 == 1 ? ' odd' : ''));
        var cspExtendDetails = a.createExtendedDetailsToggle(ind);
        cspItem.appendChild(cspExtendDetails);
        var cspPN = a.createHTML("div");
        cspPN.innerHTML = "Part Number: <span id=\"csp-PartNumber" + ind + "1\">" + MfPN + "</span>";
        cspItem.appendChild(cspPN);
        var cspTopSellerDesc = a.createHTML("div");
        cspTopSellerDesc.innerHTML = "<b>TopSeller Services Upgrade Option:</b> <span id=\"csp-TopSellerUpgradeOption" + ind + "3\">" + ShortDescription + "</span>";
        cspItem.appendChild(cspTopSellerDesc);
        var cspDetailsContainer = a.createHTML("div", "csp-Item" + ind + "DetailsContainer", "csp-ItemDetailsContainer");
        a.addExtendedDetails(ind, ESpecJson, cspDetailsContainer);
        cspItem.appendChild(cspDetailsContainer);
        var cspMainContainer = document.getElementById("csp-MainContainer");
        cspMainContainer.appendChild(cspItem);
        b.push(cspItem);
    };
    a.createHTML = function(type, id, classname) {
        var n = document.createElement(type);
        if (id) {
            n.setAttribute("id", id);
        }
        if (classname) {
            n.setAttribute("class", classname);
        }
        return n;
    };
    a.createExtendedDetailsToggle = function(i) {
        var x = a.createHTML("div", "", "csp-ExtendDetails");
        x.innerHTML = "<a href=\"#\" onclick=\"document.getElementById(\'csp-Item" + i + "DetailsContainer\').style.display=(document.getElementById(\'csp-Item" + i + "DetailsContainer\').style.display==\'block\' ? \'none\':\'block\')\">Extended Specs&nbsp;<img border=\"0\" align=\"right\" src=\"https://cc.cnetcontent.com/vcs/lenovo/redesign2011/img/symbol_plus-expand.gif\"></a>";
        return x;
    };
    a.addExtendedDetails = function(ind, ESpecJson, cspDetailsContainer) {
        var spec = eval('(' + ESpecJson + ')');
        for (var i = 0; i < spec.Espec.length; i++) {
            var ds = a.createHTML("div", "csp-Item" + ind + "DetailsSection" + i);
            var dsTitle = a.createHTML("div", "", "csp-ItemDetailsSectionTitle");
            dsTitle.innerHTML = spec.Espec[i].SN;
            ds.appendChild(dsTitle);
            for (var k = 0; k < spec.Espec[i].R.length; k++) {
                var dsItemTitle = a.createHTML("div", "", "csp-ItemDetailsSectionItemTitle");
                dsItemTitle.innerHTML = spec.Espec[i].R[k].HT;
                ds.appendChild(dsItemTitle);
                var dsItemValue = a.createHTML("div", "", "csp-ItemDetailsSectionItemData");
                dsItemValue.innerHTML = spec.Espec[i].R[k].BT;
                ds.appendChild(dsItemValue);
                var dsItemBR = a.createHTML("br");
                ds.appendChild(dsItemBR);
            }
            cspDetailsContainer.appendChild(ds);
        }
        return;
    };
}
function setFlyoutTitle(ContentName) {
    var o = document.getElementById("csp-TopSellerTitleItem");
    o.innerHTML = ContentName;
}
