var contentOriginalHeight;
var hasBannerText = false;

function initApp() {
	$('head').append("<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>");
	contentOriginalHeight = $('#csp-content').height();
	initPageEffect();
	initPopupWindows();
	if (detectNoCategory())
		return;
	categories.Render("#csp-nav");
	ResetContentImages();
	initBannerSection();
	if (window.location.href.indexOf('home=true') < 0)
		RenderCustomBreadcrumb ("#csp-breadcrumb", crrCatId, (categoryLevel));
	if (categoryLevel > 1) {
		RenderSideItems();
	}
	else {
		RenderChildrenBlocks();
		ExpandContentPane();
	}
	AddExceptionMenuItems();
	initSideControlsTrigger();
	initSideControls();
	initMenu();
	initContentPageEditor();
	initHeaderTitleEditor();
	initToolTips();
	initContextMenus();
	if (window.location.href.indexOf('home=true') >= 0 || window.location.href.indexOf('cat=') < 0) {
		$('.root-item').removeClass('selected');
		$('#home-item').addClass('selected');
	}
	
	$('#sci-newrootitem').parent('li').hide();
	$('.the-main-menu').sortable( "disable" );
	
	initLightBox();
}

function initBannerSection() {
	if ($('#csp-right-col #page-banner').length > 0 && $('#csp-right-col div.feature').length > 0) {
		if ($('#csp-right-col #page-banner').find('img').length == 0)
			$('#csp-right-col #page-banner').remove();
		if ($('#csp-right-col div.feature').find('img').length == 0)
			$('#csp-right-col div.feature').remove();
	}
	if ($('#csp-right-col #page-banner').length > 0 || $('#csp-right-col div.feature').length > 0) {
		RelocatePageBanner();
		$('#csp-right-col #page-banner').unbind().click(function() {
			openRadFileExplorer();
		});
		if ($('#banner-main-text').length == 1) {
			initBannerTextContextMenu();
		}
		
		$('#csp-right-col').children('div.feature:first').find('.figure').unbind().click(function() {
			openRadFileExplorer();
		});
		
		if ($('#csp-right-col').children('div.feature:first').find('.feature-info').length == 1)
			initBannerTextContextMenu();
	}
	else {
		var sectionWidth = $('#csp-right-col').width();
		var bannerWrapper = $('<div id="page-banner">');
		bannerWrapper.css({
			width: (sectionWidth - 2) + 'px',
			border: '1px dashed #BBB',
			'min-height': '200px',
			position: 'relative'
		});
		bannerWrapper.unbind().click(function() {
			openRadFileExplorer();
		});
		$('#csp-right-col').prepend(bannerWrapper);
	}
	
	$('#side-controls-menu').find('#sci-addpagebanner').parent('li').remove();
	$.contextMenu({
		selector: '#csp-right-col #page-banner, #csp-right-col .feature:first',
		items: {
				"AddContent": { name: "Add Banner Text", icon: "add", callback: function (key, opt) {
						if ($(opt.$trigger).attr('id') == 'page-banner')
							$(opt.$trigger).append($('<div id="banner-main-text" style="position: absolute; z-index: 1; top: 20px; left: 10px;">').html('<h1 style="color: #00447C; font-size: 20px;">Banner Text</h1>'));
						else
							$(opt.$trigger).append($('<div class="feature-info">').html('<h1 style="color: #00447C; font-size: 20px;">Banner Text</h1>'));
							
						initBannerTextContextMenu();
					},
					disabled: function (key, opt) {
						return hasBannerText;
					}
				},
				"AddEditBannerLink": { name: "Add/Edit Banner Link", icon: "add", callback: function (key, opt) {
						var wdBannerUrl = $find(RWBannerUrl);
						wdBannerUrl.show();
					}
				},
				"SwitchBannerStyle": { name: "Switch Banner Style", icon: "edit", callback: function (key, opt) {
						var rightColWidth = $('#csp-right-col').width();
						if ($(opt.$trigger).attr('id') == 'page-banner') {
							var imgWidth = rightColWidth - 255;
							var figureEl = $('<div class="figure">');
							var bannerImg = $(opt.$trigger).find('img');
							if ($('#csp-side-col').find('li').length > 0) {
								figureEl.attr('style', 'width:' + imgWidth + 'px !important');
							}
							bannerImg.attr('width', imgWidth + 'px');
							bannerImg.appendTo(figureEl);
							$(opt.$trigger).append(figureEl);
							
							$(opt.$trigger).children('#banner-main-text').removeAttr('style');
							$(opt.$trigger).children('#banner-main-text').addClass('feature-info').draggable('disable');
							$(opt.$trigger).children('#banner-main-text').removeAttr('id');
							
							$(opt.$trigger).removeAttr('id');
							$(opt.$trigger).unbind().addClass('feature');
							SavePageContent();
						}
						else {
							var bannerImg = $(opt.$trigger).find('img');
							bannerImg.attr('width', rightColWidth + 'px');
							bannerImg.appendTo($(opt.$trigger));
							$(opt.$trigger).find('.figure').remove();
							
							$(opt.$trigger).children('.feature-info').attr({'id': 'banner-main-text', 'style': 'position: absolute'}).removeAttr('class');
							$(opt.$trigger).attr('id', 'page-banner').removeAttr('class');
							$('#banner-main-text').draggable();
							$('#banner-main-text').draggable('enable');
							SavePageContent();
						}
						initBannerSection();
					}
				},
				"RemoveBanner": { name: "Remove Banner", icon: "delete", callback: function (key, opt) {
					$(opt.$trigger).children().remove();
					$(opt.$trigger).remove();
					$('#side-controls-menu ul').append($('<li>').html('<a id="sci-addpagebanner" class="side-controls-item" href="javascript: void(0)"> Add Page Banner </a>'));
					$('#csp-right-col').css('min-height', '250px');
					$('#sci-addpagebanner').unbind().click(function() {
						initBannerSection();
					});
					hasBannerText = false;
					
					SavePageContent();
				}
			}
		}
	});
}

function initBannerTextContextMenu() {
	hasBannerText = true;
	if ($('#banner-main-text').length == 1) {
		$('#banner-main-text').draggable({ containment: '#page-banner', stop: function() { SavePageContent(); } });
	}
	
	$.contextMenu({
		selector: '#banner-main-text, #csp-right-col .feature-info:first',
		items: {
			'Edit': {
				name: 'Edit',
				icon: 'edit',
				callback: function (key,opt) {
					var bannerText = $(opt.$trigger).html();
					var bannerTextareaCols = 70;
					if ($(opt.$trigger).hasClass('feature-info'))
						bannerTextareaCols = 37;
					$(opt.$trigger).html('<textarea id="tbBannerText" class="hide" style="font-size: 12px" rows="4" cols="' + bannerTextareaCols + '">' + $.trim(bannerText) + '</textarea>');
					$('#tbBannerText').show(200, function() {
						$(document).unbind('click').bind("click", function (e) {
							if (!$(e.target).closest('#tbBannerText').length) {
								var newText = $('#tbBannerText').val();
								if (newText == '')
									newText = 'Banner Text';
								$(opt.$trigger).html(newText);
								SavePageContent();
								$(this).unbind('click');
							}
						});
					});
					$('#csp-right-col .feature').unbind();
					$('#csp-right-col #page-banner').unbind();
				}
			},
			'Remove': {
				name: 'Remove',
				icon: 'delete',
				callback: function (key, opt) {
					$(opt.$trigger).remove();
					hasBannerText = false;
					SavePageContent();
				}
			}
		}
	});
}

function CustomSaveBannerUrl() {
	var videoDecor = '<div id="video" class="lightbox" style="display: none;"><div class="close">X Close</div><div id="embed-target"><iframe class="youtube-player" width="640" height="390" frameborder="0" allowfullscreen="" src="{youtube_src}" type="text/html" title="YouTube Video Player"></div><br style="clear: both;"></div>';
	/*var bannerLinkTriggerId = $('#textBannerUrlId').val();
	var bannerLinkTriggerClassName = $('#textBannerUrlClassName').val();*/
	var bannerHref = $('#textBannerUrlHref').val();
	var bannerElement = $('#page-banner');
	if ($('#csp-right-col').children('.feature').length > 0) {
		bannerElement = $('#csp-right-col').children('.feature:first');
	}
	var bannerImg = bannerElement.find('img');
	
	if (bannerHref.toLowerCase().indexOf("youtube.com") >= 0) {
		$('#video').remove();
		var bannerTrigger = $('<a class="play-video" href="javascript: void(0)">');
		videoDecor = videoDecor.replace("{youtube_src}", bannerHref);
		if (bannerElement.find('a.play-video').length > 0)
			bannerElement.find('a.play-video').replaceWith(bannerImg);
		bannerElement.prepend(bannerTrigger);
		bannerImg.appendTo(bannerTrigger);
		bannerElement.append(videoDecor);
	}
	else {
		var bannerTrigger = $('<a href="' + bannerHref + '" target="_blank">');
		if (bannerImg.parent('a').length > 0) {
			bannerImg.parent('a').replaceWith(bannerTrigger);
		}
		else {
			bannerElement.prepend(bannerTrigger);
		}
		bannerImg.appendTo(bannerTrigger);
	}
	SavePageContent();
	closeBannerUrlWD();
	//initLightBox();
}

function SavePageContent() {
	var contentId = $('.content-page-detail:first').attr('contentId');
	var contentPage = $('.content-page-detail:first').html();
	updateContentPage(contentId, contentPage);
}

function RelocatePageBanner() {
	if ($('.content-page-detail:first').find('div#page-banner').length > 0) {
		$('div#page-banner').removeAttr('class');
		$('div#page-banner').children('.figure').remove();
		$('div#page-banner div#banner-main-text').removeAttr('class');
		$('.content-page-detail:first').find('div#page-banner').prependTo($('#csp-right-col'));
		$('.RadEditor').find('div#page-banner').remove();
		$('.RadEditor').find('textarea').val('');
	}
	if ($('.content-page-detail:first').find('div.feature').length > 0) {
		if ($('.content-page-detail:first').find('div.feature:first').children('.feature-info').length > 0 || $('.content-page-detail:first').find('div.feature:first').children('.figure').length > 0) {
			$('.content-page-detail:first').find('div.feature:first').removeClass('context-menu-active');
			$('.content-page-detail:first').find('div.feature:first').prependTo($('#csp-right-col'));
			$('.RadEditor').find('div.feature:first').remove();
			$('.RadEditor').find('textarea').val('');
		}
	}
}

function RenderSideItems() {
	// if (categoryLevel >= 4 || categoryLevel == 1) {
		// RenderOldStyleSide();
		// return;
	// }
	// var thisCategory = categories.Find(crrCatId);
	// var secondLevelItems = categories.FindByAttr('pid', thisCategory.lineage[2]);
	// var thirdLevelItems;
	// var parentId;
	// if (categoryLevel == 2)
		// parentId = crrCatId;
	// else
		// parentId = thisCategory.pid;
	// thirdLevelItems = categories.FindByAttr('pid', parentId);
	
	var secondLevelItems = categories.FindByAttr('pid', crrCatId);
	if (secondLevelItems.length > 0) {
		var newSideUL = $('<ul>');
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		$('#csp-side-col').append(newSideUL);
		RenderDetailSideList(newSideUL, 'aside-menu-item', secondLevelItems);
		$('#page-banner').find('img').attr('width', $('#csp-right-col').width() + 'px');
		var featureSection = $('#csp-right-col').find('.feature:first');
		featureSection.find('.figure').attr('style', 'width: 225px');
		featureSection.find('.figure img').attr('width', '225px');
		SetContentTableWidth (200);
	}
	else
		ExpandContentPane();
	
	// if (thirdLevelItems.length > 0) {
		// var parentLiNode = $('#side-col-list li#c-' + parentId);
		// var newSideChildUL = $('<ul>');
		// newSideChildUL.attr({
			// id: 'side-col-child-list',
			// 'class': 'csp-children-items'
		// });
		// parentLiNode.append(newSideChildUL);
		// RenderDetailSideList(newSideChildUL, 'aside-menu-item', thirdLevelItems);
	// }
}

function ExpandContentPane() {
	$('#csp-right-col').css({
		padding: '0px',
		width: '100%'
	});
	
	$('#csp-right-col .RadEditor:first').css({
		width: '99%'
	});
	
	$('#page-banner').css({
		width: '658px'
	});
	if ($('#page-banner').find('img').length > 0) {
		$('#page-banner').css({
			width: '660px'
		});
		$('#page-banner').find('img').attr('width', '660px');
	}
}

function SetContentTableWidth (width) {
	$('.layoutColumn').find('table').each(function() {
		var styleValue = $(this).attr('style');
		if (styleValue.indexOf('float') < 0)
			return;
		$(this).width(width);
		$(this).find('img').width(width);
		$(this).find('td').attr('style', '');
	});
}

function ResetContentImages() {
	var sectionWidth = $('#csp-right-col').width();
	$('#csp-right-col').find('img').each(function() {
		if ($(this).parents('#page-banner').length > 0)
			return;
		var newImg = new Image();
		var height = 0;
		var width = 0;
		var crrImgEl = $(this);
		
		newImg.onload = function() {
			height = newImg.height;
			width = newImg.width;
			if (width >= sectionWidth) {
				crrImgEl.attr('width', (sectionWidth - 30) + 'px');
			}
		}

		newImg.src = $(this).attr('src'); // this must be done AFTER setting onload
	});
}

function RenderDetailSideList(ulParent, liClassName, categoryList) {
	for (var i = 0; i < categoryList.length; i++) {
		var itemClass = liClassName;
		var newSiblingLI = $('<li>');
		newSiblingLI.attr({
			id: 'c-' + categoryList[i].id
		});
		if (categoryList[i].id == crrCatId)
			newSiblingLI.addClass('selected');
		var newSiblingA = $('<a>');
		newSiblingA.html(categoryList[i].text);
		
		
		if (categoryList[i].active == false)
			itemClass += ' inactive-item';
		else if (categoryList[i].published == false)
			itemClass += ' un-published';
		
		newSiblingA.attr({
			'class': itemClass,
			cateid: categoryList[i].id,
			rel: 'CategoryTitleEditor-' + categoryList[i].id,
			href: categoryList[i].href,
			ccid: categoryList[i].cnid
		});
		var newSiblingInput = $('<input type="text">');
		newSiblingInput.val(categoryList[i].text);
		newSiblingInput.attr({
			'class': 'hide',
			id: 'CategoryTitleEditor-' + categoryList[i].id
		});
		newSiblingLI.append(newSiblingA);
		newSiblingLI.append(newSiblingInput);
		ulParent.append(newSiblingLI);
	}
	ulParent.sortable({
		cursor: "move",
		update: function (event, ui) {
			var orderedList = '';
			ulParent.find('li').each(function () {
				var idMixed = $(this).attr('id').split('-');
				if (orderedList != '')
					orderedList += '_';
				orderedList += idMixed[idMixed.length - 1];
			});
			saveCategoryOrder(orderedList);
		}
	});
	ulParent.disableSelection();
}

function RenderOldStyleSide() {
	var siblingItems = categories.FindByAttr('level', categoryLevel);
	var childrenItems = categories.FindByAttr('pid', crrCatId);
	if (childrenItems.length > 0)
		siblingItems = childrenItems;
	var newSideUL = $('<ul>');
	if (siblingItems.length > 0) {
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		for (var i = 0; i < siblingItems.length; i++) {
			var newSiblingLI = $('<li>');
			newSiblingLI.attr({
				id: 'c-' + siblingItems[i].id
			});
			if (siblingItems[i].id == crrCatId)
				newSiblingLI.addClass('selected');
			var newSiblingA = $('<a>');
			newSiblingA.html(siblingItems[i].text);
			newSiblingA.attr({
				'class': 'aside-menu-item',
				cateid: siblingItems[i].id,
				rel: 'CategoryTitleEditor-' + siblingItems[i].id,
				href: siblingItems[i].href,
				ccid: siblingItems[i].cnid
			});
			var newSiblingInput = $('<input type="text">');
			newSiblingInput.val(siblingItems[i].text);
			newSiblingInput.attr({
				'class': 'hide',
				id: 'CategoryTitleEditor-' + siblingItems[i].id
			});
			newSiblingLI.append(newSiblingA);
			newSiblingLI.append(newSiblingInput);
			newSideUL.append(newSiblingLI);
		}

		$('#csp-side-col').append(newSideUL);

		$('#side-col-list').sortable({
			cursor: "move",
			update: function (event, ui) {
				var orderedList = '';
				$('#side-col-list li').each(function () {
					var idMixed = $(this).attr('id').split('-');
					if (orderedList != '')
						orderedList += '_';
					orderedList += idMixed[idMixed.length - 1];
				});
				saveCategoryOrder(orderedList);
			}
		});
		$('#side-col-list').disableSelection();
	}
}

function initPageEffect() {
	var pageWidth = $('#dnn_ContentPane').width();
	var cspWrapperWidth = $('#csp-wrapper').width();
	var cspWrapperHeight = $('#csp-wrapper').height();
	$('#csp-wrapper').css({
		'margin-left': (pageWidth - cspWrapperWidth) / 2
	});

	if (showAll == '')
		$('#sci-showactive-active').show();
	else
		$('#sci-showall-active').show();
}

function initSideControlsTrigger() {
	$('#side-controls-trigger').unbind().click(function () {
		var isReady = true;
		if ($('.content-page-detail:hidden').length > 0)
			isReady = false;
		if (!isReady)
			return;
		$(this).unbind();

		var cspWrapperHeight = $('#csp-wrapper').height() - 5;
		if ($.browser.msie && parseInt($.browser.version, 10) === 7)
			cspWrapperHeight = $('#csp-wrapper').height();
		$('#side-controls-menu').height(cspWrapperHeight);

		var moveLeng = 200;
		var controlsWidth = -1;
		var cspWrapperMaginLeft = '';
		var controlTriggerRightPos = '';

		if ($('#side-controls-menu').width() > 0) {
			controlsWidth = 0;
			cspWrapperMaginLeft = '+=' + parseInt(moveLeng / 2);
			controlTriggerRightPos = '+=' + parseInt((moveLeng / 2) + 3);
		} else {
			controlsWidth = 200;
			cspWrapperMaginLeft = '-=' + parseInt(moveLeng / 2);
			controlTriggerRightPos = '-=' + parseInt((moveLeng / 2) + 3);
		}

		if (controlsWidth > -1 && cspWrapperMaginLeft != '') {
			if (controlsWidth > 0) {
				$('#side-controls-menu').show();
				$('#side-controls-menu ul').show();
			}
			$('#side-controls-menu').stop().animate({
				width: controlsWidth
			}, 200, function () {
				if (controlsWidth == 0)
					$('#side-controls-menu').hide();
			});
			$('#list-controls-inner').stop().animate({
				'right': controlTriggerRightPos
			}, 200);
			$('#csp-wrapper').stop().animate({
				'margin-left': cspWrapperMaginLeft
			}, 200, function () {
				initSideControlsTrigger();
			});
		}
	});
}

function initSideControls() {
	//Show/hide control 'Add Missing Contents'
	$('#sci-addmissingcontents').closest('li').hide();
	if (missingContentList.length > 0)
		$('#sci-addmissingcontents').closest('li').show();

	$('#sci-newrootitem').click(function () {
		$('#txtParentId').val(rootCatId);
		$('#txtCategoryId').val('0');
		$('#lblParentCategory').html('Root');
		openCategoryDetailForm();
	});

	$('#sci-addmissingcontents').click(function () {
		var categoryIds = '';
		for (var i = 0; i < missingContentList.length; i++) {
			if (categoryIds != '')
				categoryIds += ';';
			categoryIds += missingContentList[i];
		}
		
		$.ajax({
			type: 'POST',
			url: document.URL,
			dataType: 'xml',
			data: { ajaxaction: 'add_missing_contents', categoryids: categoryIds },
			beforeSend: function (xhr) {
				xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
				switchLoadingPanel(true);
			},
			success: function (responseMsg) {
				window.location = document.URL;
			}
		});
	});
}

function detectNoCategory() {
	if (categories.FindByAttr('pid', rootCatId).length > 0)
		return false;
	if (originalCategoryCount > 0) {
		switchLoadingPanel(true);
		window.location = controlUrl + '?showall=true';
		return;
	}
	var newLi = $('<li>').attr({
		'class': 'root-item',
		style: 'width: 100%'
	});
	var innerA = $('<a>').attr({
		'class': 'root-item-link mask-item',
		'href': 'javascript: void(0)'
	});
	innerA.html('Click here to add new item');
	innerA.click(function() {
		$('#txtParentId').val(rootCatId);
		$('#txtCategoryId').val('0');
		$('#lblParentCategory').html('Root');
		openCategoryDetailForm();
	});
	newLi.append(innerA);
	var newUl = $('<ul id="main-nav">');
	newUl.append(newLi);
	$('#csp-nav').append(newUl);
	
	return true;
}

function initListButtons() {
	if (window.location.href.indexOf('showall') >= 0) {
		$('#btnGoActiveList').closest('a.list-button-link').show();
	}
	else {
		$('#btnGoAllList').closest('a.list-button-link').show();
	}
}

function initPopupWindows() {
	// var wdDeleteCategoryOption = $('#wdDeleteCategoryOption').kendoWindow({
        // actions: ["Close"],
        // draggable: true,
        // height: "150px",
        // width: "350px",
        // modal: true,
        // resizable: false,
        // visible: false,
        // title: 'UnPublish Options'
    // }).data("kendoWindow");

	// var wdCategoryEditForm = $('#wdCategoryEditForm').kendoWindow({
		// actions: ["Close"],
		// draggable: true,
		// height: "240px",
		// width: "500px",
		// modal: true,
		// resizable: false,
		// visible: false,
		// title: 'Category Detail'
	// }).data("kendoWindow");
	
	// var wdCategoryDescriptionEditor = $('#wdCategoryDescriptionEditor').kendoWindow({
		// actions: ["Close"],
		// draggable: true,
		// height: "160px",
		// width: "350px",
		// modal: true,
		// resizable: false,
		// visible: false,
		// title: 'Category Description'
	// }).data("kendoWindow");
	
	// $('#wdBannerFileMan .k-window-action').unbind().click(function(e) {
		// e.preventDefault();
		// $('#wdBannerFileMan').hide();
	// });
	// $("#wdBannerFileMan").draggable({ handle: "#wdBannerFileMan .k-window-titlebar", containment: "#main-content" });
}

function openRadFileExplorer() {
	var wdBannerFileMan = $find(RWBannerFileMan);
	wdBannerFileMan.show();
}

jQuery.fn.customcenter = function () {
    this.css("position","absolute");
    this.css("top", '50px');
    this.css("left", Math.max(0, (($('#main-content').width() - $(this).outerWidth()) / 2) + 
                                                $('#main-content').scrollLeft()) + "px");
    return this;
}

function initContextMenus() {
	var contextMenuItems = {
		"NewSiblingCategory": {
			name: "New sibling item",
			icon: "add",
			callback: function (key, opt) {
				var cateId = $(opt.$trigger).attr('cateid');
				cateId = parseInt(cateId);
				var categoryNode = categories.Find(cateId);
				$('#txtParentId').val(categoryNode.pid);
				$('#txtCategoryId').val('0');
				if (categoryNode.pid == rootCatId)
					$('#lblParentCategory').html('Root');
				else {
					var parentNode = categories.Find(parseInt(categoryNode.pid));
					$('#lblParentCategory').html(parentNode.text);
				}
				openCategoryDetailForm();
			}
		},
		"NewSubCategory": {
			name: "New sub item",
			icon: "add",
			callback: function (key, opt) {
				var catIdMixed = $(opt.$trigger).attr('rel').split('-');
				$('#txtParentId').val(catIdMixed[catIdMixed.length - 1]);
				$('#txtCategoryId').val('0');
				$('#lblParentCategory').html($(this).html());
				openCategoryDetailForm();
			}
		},
		"Rename": {
			name: "Rename",
			icon: "edit",
			callback: function (key, opt) {
				var reEnableSortableList = function () {
					$('.the-main-menu').sortable('enable');
				};
				openCategoryTitleEditor($(opt.$trigger), reEnableSortableList);
				var editorId = $(opt.$trigger).attr('rel');
				var wrapperHeight = $(opt.$trigger).closest('li.root-item').height();
				var wrapperWidth = $(opt.$trigger).closest('li.root-item').width();
				var editorHeight = $('#' + editorId).height();
				var topSpace = (wrapperHeight - editorHeight) / 2;
				$('#' + editorId).css({
					'margin-top': topSpace + 'px',
					width: wrapperWidth + 'px'
				});
				$('.the-main-menu').sortable('disable');
			}
		},
		"CategoryDescription": {
			name: "Edit Description",
			icon: "edit",
			callback: function (key, opt) {
				var catIdMixed = $(opt.$trigger).attr('rel').split('-');
				var catEl = categories.Find(catIdMixed[catIdMixed.length - 1]);
				$('#txtCategoryIdToEditDesc').val(catIdMixed[catIdMixed.length - 1]);
				$('#lblCategoryOfDesc').html($(this).html());
				$('#textCrrDescription').val(htmlEntitiesDecode(catEl.desc));
				$('#textCrrThumbnail').val(catEl.thumbnail);
				var wdCategoryDescriptionEditor = $find(RWCategoryDescriptionEditor);
				wdCategoryDescriptionEditor.show();
			}
		},
		"Delete": {
			name: "UnPublish",
			icon: "delete",
			callback: function (key, opt) {
				var el = $(opt.$trigger);
				$('#txtDeleteCategoryId').val(el.attr('cateid'));
				var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
				wdDeleteCategoryOption.show();
			}
		}
	};
	
	$.contextMenu('destroy', '.root-item-link');
	$.contextMenu({
		selector: '.ctmenu-item-link:not(.inactive-item, .a-missing-content),.aside-menu-item:not(.inactive-item, .a-missing-content),.root-item-link:not(.inactive-item, .a-missing-content, .exception-item)',
		items: contextMenuItems,
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		},
		build: function ($trigger, e) {
			// if ($($trigger).hasClass('root-item-link')) {
				// this.items.NewSiblingCategory.disabled = true;
				// if ($('.root-item-link:not(.exception-item)').length == 1)
					// this.items.Delete.disabled = true;
			// }
			// else
			this.items.NewSiblingCategory.disabled = $($trigger).hasClass('root-item-link');	
			if ($($trigger).hasClass('un-published')) {
				this.items.Delete.name = 'Publish';
				this.items.Delete.icon = 'publish';
				this.items.Delete.callback = function (key, opt) {
					restoreCategory(opt.$trigger);
				};
			}
			else {
				this.items.Delete.name = 'UnPublish';
				this.items.Delete.icon = 'delete';
				this.items.Delete.callback = function (key, opt) {
					var el = $(opt.$trigger);
					$('#txtDeleteCategoryId').val(el.attr('cateid'));
					if (languagesCount < 2) {
						deleteCategory(false);
						return;
					}
					var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
					wdDeleteCategoryOption.show();
				};
			}
			if ($($trigger).hasClass('root-item-link') && $('.root-item-link:not(.exception-item)').length == 1)
				return;
			return addDynamicItem($trigger, e);
		}
	});

	$.contextMenu({
		selector: '.a-missing-content:not(.inactive-item)',
		items: {
			"AddContent": { name: "Add Content", icon: "add", callback: function (key, opt) {
				switchLoadingPanel(true);
				var categoryId = $(opt.$trigger).attr('cateid');
				$.ajax({
					type: 'POST',
					url: document.URL,
					dataType: 'xml',
					data: { ajaxaction: "add_content", categoryid: categoryId },
					beforeSend: function (xhr) {
						xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
					},
					success: function (responseMsg) {
						window.location = controlUrl + '?cat=' + categoryId + showAll;
					}
				});
			}
			}
		},
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		}
	});

	$.contextMenu({
		selector: '.inactive-item',
		items: {
			"Activate": { name: "Activate", icon: "activate", callback: function (key, opt) {
				var thisEl = $(opt.$trigger);
				var categoryId = thisEl.attr('cateid');
				$.ajax({
					type: 'POST',
					url: document.URL,
					dataType: 'xml',
					data: { ajaxaction: "activate_category", categoryid: categoryId },
					beforeSend: function (xhr) {
						xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
					},
					success: function (responseMsg) {
						var resultCode = parseInt($(responseMsg).find('resultcode').text());
						if (resultCode == 1) {
							$('a[cateid="' + categoryId + '"]').each(function () {
								$(this).removeClass('inactive-item');
								$(this).removeClass('un-published');
								$(this).closest('li').find('a.ctmenu-item-link').removeClass('inactive-item');
								
								if ($(this).parents('div#csp-nav').length > 0)
									activateParentNodes($(this).closest('li'));
								$(this).qtip('destroy');
							});
							initToolTips();
							initContextMenus();
						}
						else {

						}
					}
				});
			}
			}
		},
		events: {
			show: function (opt) {
				$('li.root-item').unbind('hover');
			},
			hide: function (opt) {
				menuEffect();
			}
		}
	});
}

function activateParentNodes(theNode) {
	if (theNode.hasClass('root-item'))
		return;
	var grandLi = theNode.parents('li:first');
	if (grandLi.length == 0)
		return;
	var targetA = grandLi.find('a:first');
	targetA.removeClass('inactive-item');
	targetA.removeClass('un-published');
	targetA.qtip('destroy');
	activateParentNodes(grandLi);
}

function addDynamicItem($trigger, e) {
	var catId = $($trigger).attr('cateid');
	var categoryNode = categories.FindByAttr('id', catId);
	var itemsObj;
	if (categoryNode.length > 0) {
		if (categoryNode[0].issubscribable) {
			itemsObj = {
				"MakeUnSubscribable": {
					name: "UnSubscribable",
					icon: "unsubscribable",
					callback: function(key, opt) {
						changeSubscribeState(categoryNode[0], false);
					}
				}
			};
		}
		else {
			itemsObj = {
				"MakeSubscribable": {
					name: "Subscribable",
					icon: "subscribable",
					callback: function (key, opt) {
						changeSubscribeState(categoryNode[0], true);
					}
				}
			};
		}
	}
	return { items: itemsObj };
}

function changeSubscribeState(targetCategory, status) {
	switchLoadingPanel(true);
	var statusNo = 1;
	if (status == false)
		statusNo = 0;
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: "change_subscribe_status", categoryid: targetCategory.id, status: statusNo },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode == 1) {
				targetCategory.issubscribable = status;
				if (statusNo == 0)
					changeSubscribeStateForChildren(targetCategory.id, statusNo);
				initContextMenus();
			}
			else {

			}
			switchLoadingPanel(false);
		}
	});

}

function changeSubscribeStateForChildren(categoryId, statusNo) {
	if (categoryId < 1)
		return;
	var targetCategory = categories.FindByAttr('id', categoryId);
	if (targetCategory.length < 1)
		return;
	var isSubscribable = true;
	if (statusNo != 1)
		isSubscribable = false;
	targetCategory[0].issubscribable = isSubscribable;
	if (typeof (targetCategory[0].children) != "undefined") {
		var subCategoriesList = categories.FindByAttr('pid', categoryId);
		if (subCategoriesList.length > 0) {
			for (var i = 0; i < subCategoriesList.length; i++)
				changeSubscribeStateForChildren(subCategoriesList[i].id, statusNo);
		}
	}
}

function deleteCategory(isAll) {
	var ajaxAction = 'delete_category';
	if (!isAll)
		ajaxAction = 'unpublish_category';
	
	var categoryId = $('#txtDeleteCategoryId').val();
	var thisEl = $('a[cateid="' + categoryId + '"]:first');
	
	$('a[cateid="' + categoryId + '"]').each(function () {
		if ($(this).hasClass('root-item-link'))
			thisEl = $(this);
	});
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: ajaxAction, categoryid: categoryId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode == 1) {
				if (showAll == '') {
					DetectHomeBlock(categoryId);
					$('[ccid="' + thisEl.attr('ccid') + '"]').closest('li').remove();
					if (categoryId == crrCatId || $('#csp-breadcrumb').find('a[catid="' + categoryId + '"]').length == 1) {
						switchLoadingPanel(true);
						window.location = controlUrl;
						return;
					}
					var parentLi = thisEl.closest('li');
					var isRootLi = parentLi.hasClass('root-item');
					parentLi.remove();
					if (isRootLi) {
						hAlignNavItem();
						vAlignNavText();
					}
				}
				else {
					if (isAll)
						$('a[cateid="' + categoryId + '"]').closest('li').find('a').addClass('inactive-item');
					else
						$('a[cateid="' + categoryId + '"]').closest('li').find('a').addClass('un-published');
					initContextMenus();
					initToolTips();
				}
				closeCategoryDeleteWD(null, null);
			}
			else {
				alert('Delete failed, please try again later.');
			}
		}
	});
}

function DetectHomeBlock(categoryId) {
	if ($('#block-' + categoryId).length <= 0)
		return;
	$('#block-' + categoryId).remove();
	var remainCount = $('#sub-category-blocks').find('.sub-category-block').length;
	var newWidth = 100 / remainCount;
	$('#sub-category-blocks').find('.sub-category-block').each(function() {
		$(this).css('width', newWidth + '%');
	});
	
	$('.sub-menu').find('li.sub-item').each(function () {
		$(this).css('width', (newWidth - 3) + '%');
	});
}

function restoreCategory(thisEl) {
	var categoryId = $(thisEl).attr('cateid');
	$.ajax({
		type: 'POST',
		url: document.URL,
		dataType: 'xml',
		data: { ajaxaction: 'publish_category', categoryid: categoryId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode == 1) {
				$(thisEl).removeClass('un-published');
				$(thisEl).qtip('destroy');
				var parentList = GetCategoryLineage(categoryId);
				for (var i = 0; i < parentList.length; i++) {
					$('li[cateid="' + parentList[i] + '"]').find('a:first').removeClass('un-published');
					$('li[cateid="' + parentList[i] + '"]').find('a:first').qtip('destroy');
				}
				initContextMenus();
				switchLoadingPanel(false);
			}
			else {

			}
		}
	});
}

function openCategoryTitleEditor(thisEl, postBack) {
	var textEditor = $('#' + thisEl.attr('rel'));
	var tmp = thisEl.attr('rel').split('-');
	var categoryId = tmp[tmp.length - 1];
	var ccid = thisEl.attr('ccid');
	var el = thisEl;
	textEditor.fadeIn(function () {
		$(document).unbind('click').bind("click", function (e) {
			if (!$(e.target).closest('#' + el.attr('rel')).length) {
				var val = textEditor.val();
				if (val == "") {
					textEditor.val(el.html());
				}
				else if (val != el.html()) {
					el.html(val);
					renameCategory(categoryId, ccid, val);
				}
				el.fadeIn();
				textEditor.hide();
				$(this).unbind('click');
				if (postBack !== undefined)
					postBack();
			}
		});

		$(this).unbind('keydown').bind('keydown', function (e) {
			if (e.which == 13) {
				e.preventDefault();
				var val = textEditor.val();
				if (val == "") {
					textEditor.val(el.html());
				}
				else if (val != el.html()) {
					el.html(val);
					renameCategory(categoryId, ccid, val, postBack);
				}
				el.fadeIn();
				textEditor.hide();
				$(this).unbind('keydown');
				if (postBack !== undefined)
					postBack();
			}
		});

		$(this).focus();
		$(this).select();
	});
	el.hide();
}

function renameCategory(catId, ccId, catName, postBack) {
	if (catId < 1 || catName == '') return 0;
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'rename_category', categoryid: catId, categoryname: catName, contentcategoryid: ccId },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (responseMsg) {
			var resultCode = parseInt($(responseMsg).find('resultcode').text());
			if (resultCode != 1)
				alert('Rename failed, please try again later.');
			else {
				//console.log(controlUrl + '?cat=' + catId);
				$('#csp-wrapper').find('a[href^="' + controlUrl + '?cat=' + catId + '&"]').each(function () {
					$(this).html(catName);
					$(this).closest('li').find('input[type="text"]').val(catName);
				});
			}
		}
	});
}

function openCategoryDetailForm(catId) {
	var parentId = parseInt($('#txtParentId').val());
	$('#chkIsSubscribable').removeAttr('disabled');
	if (parentId != rootCatId) {
		var parentCat = categories.Find(parentId);
		if (parentCat.issubscribable == false)
			$('#chkIsSubscribable').attr('disabled', 'disabled');
	}
	$('#wdCategoryEditForm').find('.error-msg').hide();
	$('#wdCategoryEditForm').removeClass('hide').show();
	var oWnd = $find(RWCategoryEditForm);
    oWnd.show();
	$('#txtCategoryText').unbind().keydown(function () {
		if ($('#errormsg-txtCategoryText').is(':visible'))
			$('#errormsg-txtCategoryText').hide();
	});
}

function vAlignNavText() {
    $('.root-item-link').each(function () {
        var thisHeight = $(this).height();
        var wrapperHeight = $(this).closest('li').height();
        var topPos = (wrapperHeight - thisHeight) / 2;
    	var newHeight = wrapperHeight - topPos;
        $(this).css({ 'padding-top': topPos + 'px', 'height': newHeight + 'px' });
    });
}

function hAlignNavItem() {
    //var navWidth = 510;
	var navWidth = $('#csp-nav').width();
    var menuItemCount = $('li.root-item').length;
    var itemWidth = (navWidth / menuItemCount) - 1;
    //var crrStyle = $('li.root-item').attr('style');
    $('li.root-item').attr('style', 'width: ' + itemWidth + 'px !important');
   // $('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
}

function initMenu() {
    menuEffect();
	
    hAlignNavItem();
    
    var subMenuItemCount;
    var widthPercent;
    $('ul.sub-menu').each(function () {
        subMenuItemCount = $(this).find('li.sub-item').length;
        widthPercent = (100 / subMenuItemCount) - 3;
        $(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '% !important; padding-right: 3% !important');
    });

    vAlignNavText();
}

function updateMainNavOrder() {
	var orderedList = '';
	$('.root-item').each(function() {
		var idMixed = $(this).find('a.root-item-link').attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList);
}

function updateSubMenuOrder(event, ui) {
	var ulParent = ui.item.closest('ul.sub-menu');
	var orderedList = '';
	ulParent.find('li.sub-item').each(function() {
		var idMixed = $(this).attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList);
}

function updateSubMenuDetailOrder(event, ui) {
	var ulParent = ui.item.closest('ul.sub-menu-detail');
	var orderedList = '';
	ulParent.find('li.ctmenu-item').each(function () {
		var idMixed = $(this).attr('cateid');
		if (orderedList != '')
			orderedList += '_';
		orderedList += idMixed;
	});
	saveCategoryOrder(orderedList);
}

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function saveCategoryOrder(theList) {
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'order_category', orderlist: theList },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			//loadingPanel.show(mainPane);
		},
		success: function (responseMsg) {
			//window.location = document.URL;
			//loadingPanel.hide(mainPane);
		},
		error: function (errorMsg) {
			//alert(errorMsg);
		}
	});
}

function menuEffect() {
	if (window.location.href.indexOf('&home=true') < 0 && window.location.href.indexOf('cat=') >= 0)
		$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
}

function removeParentCatFromLeftCol() {
    var pIdArr = new Array();
    $('#csp-side-col a').each(function () {
        pIdArr.push($(this).attr('pid'));
    });
    var sorted_arr = pIdArr.sort();

    var results = [];
    for (var i = 0; i < pIdArr.length - 1; i++) {
        if (sorted_arr[i + 1] != sorted_arr[i]) {
            results.push(sorted_arr[i]);
        }
    }

    $('#csp-side-col a[pid="' + results + '"]').closest('li').remove();
}

function initContentPageEditor() {
	disableContentLink();
	$(".content-page-detail").click(function () {
		if ($('#side-controls-menu').width() > 0)
			$('#side-controls-trigger').click();
		var el = $(this);
		var targetObject = $('#' + el.attr("editorid"));
		var contentId = el.attr('contentid');
		$('#csp-content').attr('style', 'height: auto !important');
		if (targetObject.length > 0) {
			el.hide();
			$(targetObject).keypress(function (event) {
				if (event.which == 13 && $(this).attr("type") == "text") {
					event.preventDefault();
				} // press enter
			});
			targetObject.fadeIn(function () {
				var editor = $find(el.attr("editorid"));
				editor.set_html(el.html());
				//setTimeout("resizeRadEditor()", 1000);
				resizeRadEditor();
				$(document).unbind('click').bind("click", function (e) {
					if (isRWCategoryTreeOpen || FileExplorerTrigger == 'AddDocLink') {
						return;
					}
					if (!$(e.target).closest('#' + el.attr("editorid")).length) {
						var editor = $find(el.attr("editorid"));
						var val = editor.get_html();
						if (val == "") val = "No content available";
						el.html(val);
						el.fadeIn(function() {
							disableContentLink();
						});
						targetObject.hide();
						$(this).unbind('click');
						$('#csp-content').removeAttr('style');
						updateContentPage(contentId, val);
						
					}
				});
			});
			targetObject.focus();
		}
	});
}

function resizeRadEditor() {
	if ($('.RadEditor .reContentArea').is(':visible')) {
		$('.RadEditor .reContentArea').attr('style', 'width:' + ($('#csp-right-col').width() - 15) + 'px !important; height: ' + $('.RadEditor .reContentCell').height() + 'px');
	}
	else if ($('.RadEditor .reContentCell iframe').is(':visible'))
		$('.RadEditor .reContentCell iframe').attr('style', 'width:' + ($('#csp-right-col').width() - 15) + 'px !important; height: ' + $('.RadEditor .reContentCell').height() + 'px');
}

function disableContentLink() {
	$(".content-page-detail a").unbind('click').click(function (e) {
		e.preventDefault();
		$(this).parents('.content-page-detail').click();
		return false;
	});
}

function updateContentPage(contentId, contentText) {
	var pageBanner = '';
	if ($('#csp-right-col').children('#page-banner').length == 1 && $('#csp-right-col').children('#page-banner').find('img').length > 0) {
		pageBanner = $('#page-banner').wrap('<p/>').parent().html();
		$('#page-banner').unwrap();
		initBannerSection();
	}
	if ($('#csp-right-col').children('div.feature').length > 0) {
		if ($('#csp-right-col div.feature:first').children('.feature-info').length > 0 || $('#csp-right-col div.feature:first').children('.figure').length > 0) {
			$('#csp-right-col div.feature:first').removeClass('context-menu-active');
			pageBanner = $('#csp-right-col div.feature:first').wrap('<p/>').parent().html();
			$('#csp-right-col div.feature:first').unwrap();
			initBannerSection();
		}
	}
	contentText = pageBanner + contentText;

	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: { ajaxaction: 'save_contentpage', contentid: contentId, contenttext: contentText, categoryid: crrCatId },
		beforeSend: function (xhr) {
        	xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			switchLoadingPanel(true);
        },
        success: function (responseMsg) {
        	var resultCode = parseInt($(responseMsg).find('resultcode').text());
        	if (resultCode != 1)
        		alert('Failed, please try again later.');
			else
				UpdateContentMissingList();
			switchLoadingPanel(false);
        }
	});
}

function UpdateContentMissingList() {
	if (missingContentList.length == 0 || missingContentList.indexOf(crrCatId) < 0)
		return;
	var missingCatIndex = missingContentList.indexOf(crrCatId);
	missingContentList.splice(missingCatIndex, 1);
}

function SaveCategoryForm(sender, args) {
	if ($.trim($('#txtCategoryText').val()) == '') {
		$('#errormsg-txtCategoryText').show();
		return;
	}
	sender.set_enabled(false);
	
	var isSubscribable = 1;
	if ($('#chkIsSubscribable').is(':checked') == false)
		isSubscribable = 0;
	var categoryData = {
		ajaxaction: 'save_category',
		categoryid: $('#txtCategoryId').val(),
		categorytext: $('#txtCategoryText').val(),
		categorydesc: $('#textDescription').val(),
		categorythumbnail: $('#txtCategoryThumbnail').val(),
		parentid: $('#txtParentId').val(),
		displayorder: isNaN($('#txtDisplayOrder').val()) ? 1 : $('#txtDisplayOrder').val(),
		issubscribable: isSubscribable
	};
	
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: categoryData,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			closeCategoryFormWD();
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			var newCategory = $(responseMsg).find('category');
			if (newCategory.length == 0) {
				alert('Create failed, please try again.');
				sender.set_enabled(true);
				switchLoadingPanel(false);
				return;
			}
			window.location = controlUrl + '?cat=' + newCategory.find('id').text(); // +'&lvl=' + newCategory.find('level').text();
		},
		error: function () {
			sender.set_enabled(true);
			alert('Create failed, please try again.');
		}
	});	
}

function clearCategoryForm() {
	$('#txtCategoryId').val('0');
	$('#txtCategoryText').val('');
	$('#txtParentId').val('0');
	$('#txtDisplayOrder').val('');
	$('#chkIsSubscribable').attr('checked', false);
	$('#txtCategoryThumbnail').val('');
	$('#textDescription').val('');
}
function closeCategoryFormWD(sender, args) {
	var oWnd = $find(RWCategoryEditForm);
	oWnd.close();
	clearCategoryForm();
}

function SaveCategoryDescForm(sender, args) {
	sender.set_enabled(false);
	var wdCategoryDescriptionEditor = $find(RWCategoryDescriptionEditor);
	var categoryData = {
		ajaxaction: 'save_category_description',
		categoryid: $('#txtCategoryIdToEditDesc').val(),
		categorydesc: $('#textCrrDescription').val(),
		categorythumbnail: $('#textCrrThumbnail').val()
	};
	$.ajax({
		url: document.URL,
		type: 'POST',
		dataType: 'xml',
		data: categoryData,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
			wdCategoryDescriptionEditor.close();
			switchLoadingPanel(true);
		},
		success: function (responseMsg) {
			if (window.location.href.indexOf('home=true') >= 0 || categoryLevel <= 1) {
				location.reload(); 
				return;
			}
			var catEl = categories.Find($('#txtCategoryIdToEditDesc').val());
			catEl.desc = htmlEntities($('#textCrrDescription').val());
			catEl.thumbnail = $('#textCrrThumbnail').val();
			sender.set_enabled(true);
			switchLoadingPanel(false);
		},
		error: function () {
			sender.set_enabled(true);
			switchLoadingPanel(false);
			alert('Update failed, please try again.');
		}
	});
}

function closeCategoryDescFormWD () {
	var oWnd = $find(RWCategoryDescriptionEditor);
	oWnd.close();
}

function closeCategoryDeleteWD(sender, args) {
	var wdDeleteCategoryOption = $find(RWDeleteCategoryOption);
	wdDeleteCategoryOption.close();
	$('#txtDeleteCategoryId').val('0');
}

function deleteCategoryAllLang (sender, args) {
	deleteCategory(true);
}

function deleteCategoryThisLang(sender, args) {
	deleteCategory(false);
}

function initHeaderTitleEditor() {
	$('#header-category-title').click(function() {
		openCategoryTitleEditor($(this));
	});
}

function initToolTips() {
	$('li.missing-content-small').each(function() {
		if ($(this).find('a.inactive-item').length == 0) {
			$(this).qtip({
				content: ttMsgContentMissing
			});
		}
	});

	$('.inactive-item').qtip({
		content: ttMsgInactiveItem,
		position: {
			my: 'left center',
			at: 'right center'
		}
	});

	$('.un-published:not(.inactive-item)').qtip({
		content: ttMsgUnPublishedItem,
		position: {
			my: 'left center',
			at: 'right center'
		}
	});
	
	$('#page-banner').qtip({
		content: 'Click to add/replace image',
		position: {
			my: 'bottom center',
			at: 'top center'
		}
	});
	
	$('#csp-right-col div.feature:first').find('.figure').qtip({
		content: 'Click to add/replace image',
		position: {
			my: 'bottom center',
			at: 'top center'
		}
	});
}

function switchLoadingPanel (flag) {
	var loadingPanel = $find(ajaxLoadingPanelId);
	if (loadingPanel == null) {
		return;
	}
	if (flag) {
		loadingPanel.show(mainPane);
		return;
	}
	loadingPanel.hide(mainPane);
}

function AddExceptionMenuItems() {
	var newUl;
	if ($('#csp-nav').find('#main-nav').length == 0) {
		newUl = $('<ul id="main-nav" class="the-main-menu">');
		$('#csp-nav').append(newUl);
	}
	else
		newUl = $('#csp-nav').find('#main-nav');
	var firstCategories = categories.FindByAttr('level', 1);
	var homeUrl = firstCategories[0].href + '&home=true';
	newUl.prepend($('<li class="root-item" id="home-item">').html('<a class="root-item-link exception-item" href="' + homeUrl + '">Home</a>'));
	var contactUrl = 'javascript: void(0)';
	newUl.append($('<li class="root-item exception-item">').html('<a class="root-item-link exception-item" href="' + contactUrl + '">Contact</a>'));
}

// function OpenCategoryThumbnail(sender, args) {
	// FileExplorerTrigger = 'CategoryThumbnail';
	// if (sender.get_cssClass() == 'btn-crr-thumbnail')
		// FileExplorerTrigger = 'CrrCategoryThumbnail';
	// var radWD = $find(RWBannerFileMan);
	// radWD.show();
// }

function RadFileExplorerFileClicked(sender, args) {
	var item = args.get_item();
	if (!item || item.isDirectory()) return;
	args.set_cancel(true);
	
	if (FileExplorerTrigger == 'AddDocLink') {
		var imageTypes = new Array('pdf', 'doc', 'docx', 'zip');
		var fileExtension = item.get_extension();
		if ($.inArray(fileExtension, imageTypes) == -1) {
			alert('Please select document files (*.pdf, *.doc, *.docx) or zip file only');
			return;
		}
		var fileName = item.get_url();
		var fileNameMixed = fileName.split('/');
		var fileTitle = fileNameMixed[fileNameMixed.length - 1];
		if (crrRadEditor == '')
			return;
		crrRadEditor.getSelection().selectRange(range);
		crrRadEditor.pasteHtml('<a href="' + fileName + '" target="_blank">' + fileTitle + '</a>');
		return;
	}
	
	var imageTypes = new Array('jpg', 'jpeg', 'gif', 'png');
	var fileExtension = item.get_extension();
	if ($.inArray(fileExtension, imageTypes) == -1) {
		alert('Please select image only');
		return;
	}
	var fileName = item.get_url();
	
	if (FileExplorerTrigger == 'CategoryThumbnail' || FileExplorerTrigger == 'CrrCategoryThumbnail') {
		if (FileExplorerTrigger == 'CategoryThumbnail')
			$('#txtCategoryThumbnail').val(hostUrl + fileName);
		else
			$('#textCrrThumbnail').val(hostUrl + fileName);
		var radWD = $find(RWBannerFileMan);
		radWD.close();
		FileExplorerTrigger = '';
		return;
	}
	if ($('#page-banner').length == 1) {
		$('#page-banner').css('border', 'none');
		var defaultWidth = $('#page-banner').width() + 'px';
		var newImg = $('<img width="' + defaultWidth + '" src="' + hostUrl + fileName + '">');
		$('#page-banner img').remove();
		$('#page-banner').prepend(newImg);
	}
	if ($('#csp-right-col').children('div.feature').length == 1) {
		var figureSection = $('#csp-right-col div.feature:first').children('.figure');
		figureSection.find('img').remove();
		var defaultWidth = figureSection.width() + 'px';
		var newImg = $('<img width="' + defaultWidth + '" src="' + hostUrl + fileName + '">');
		figureSection.append(newImg);
	}
	var contentId = $('.content-page-detail:first').attr('contentId');
	var contentPage = $('.content-page-detail:first').html();
	updateContentPage(contentId, contentPage);
}

function RenderCustomBreadcrumb(elId, catId, level) {
	var el = $(elId);
	var categoryEl = categories.Find(catId);
	var theLineage = GetCategoryLineage(catId);
	console.log(theLineage);
	var html = '';
	var theUrl;
	for (var i = 0; i < theLineage.length; i++) {
		if (theLineage[i] == '') continue;
		if (theLineage[i] == rootCatId) {
			categoryEl = categories.Find(theLineage[i + 1]);
			theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href + '&home=true' : "javascript:void(0);");
			html += '<a href="' + theUrl + '">Home</a>  &raquo;  ';
			continue;
		}
		else if (theLineage[i] == catId) {
			categoryEl = categories.Find(catId);
			html += categoryEl.text;
			continue;
		}
		categoryEl = categories.Find(parseInt(theLineage[i]));
		if (categoryEl == null) continue;
		theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href : "javascript:void(0);");
		html += '<a ccid="' + categoryEl.cnid + '" catid="' + categoryEl.id + '" href="' + theUrl + '">' + categoryEl.text + '</a>  &raquo;  ';
	}
	if (html != "")
		el.html(html);
};

function RenderChildrenBlocks () {
	var childrenList = categories.FindByAttr('pid', crrCatId);
	if (childrenList.length <= 0)
		return;
	$('#sub-category-blocks').remove();	
	var contentWidth = $('#csp-right-col').width();
	var blockWidth = 100 / childrenList.length;
	var blocksWrapper = $('<div id="sub-category-blocks">');
	
	for (var i = 0; i < childrenList.length; i++) {
		var blockDetail = '<div class="sub-category-block" id="block-' + childrenList[i].id + '">';
		blockDetail += '<a class="sub-category-block-figure" href="' + childrenList[i].href + '"><img src="' + childrenList[i].thumbnail + '" height="200px"></a>';
		blockDetail += '<h2><a href="' + childrenList[i].href + '">' + childrenList[i].text + '</a></h2>';
		blockDetail += '<p>' + childrenList[i].desc + '</p></div>';
		blocksWrapper.append(blockDetail);
	}
	blocksWrapper.append('<div class="clear"></div>');
	$('#csp-right-col').append(blocksWrapper);
	$('.sub-category-block').css('width', blockWidth + '%');
}

function initLightBox () {
	$('a.play-video').click(function() {
		//$('#screen').css({'display':'block', opacity: 0.75, 'width':$(document).width(), 'height':$(document).height()});
		$('#video').css({'display':'block'});
		$('#video').find('iframe').width(500);
		return false;
	});
	// Close Lightbox
	$('.lightbox .close').click(function() {
		$('#screen, .lightbox').hide();
		$('iframe').attr('src', $('iframe').attr('src'));
	}); 
}

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}


function OpenCategoryThumbnail() {
	
}

function htmlEntitiesDecode(str) {
	var decoded = $('<div/>').html(str).text();
	return decoded;
}

function htmlEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/* Start RadTreeView Event Handlers { */
function CustomClientNodeDragStart(sender, eventArgs) {
	var node = eventArgs.get_node();
	if (node.get_level() <= 1) {
		eventArgs.set_cancel(true);
	}
}

function CustomNodeDragging(sender, args) {
	
}

function CustomNodeDropping(sender, args) {
	var dest = args.get_destNode();
	
	if (dest) {
		var destinationParent = dest.get_parent();
		if (dest.get_level() == 0 || destinationParent.get_level() == 0) {
			args.set_cancel(true);
			return;
		}
		//clientSideEdit(sender, args);                                
		//args.set_cancel(true);
	}
}
/* } EOF RadTreeView Event Handlers */