var contentOriginalHeight;

function initApp() {
	AddExceptionMenuItems();
	categories.Render("#csp-nav");
	
	ExpandContentPane();
	
	initMenu();
}

function ExpandContentPane() {
	$('#csp-side-col').remove();
	$('#csp-right-col').css({
		padding: '0px',
		width: '100%'
	});
	
	$('#page-banner').css({
		width: '658px'
	});
	if ($('#page-banner').find('img').length > 0) {
		$('#page-banner').css({
			width: '660px'
		});
		$('#page-banner').find('img').attr('width', '660px');
	}
}

function vAlignNavText() {
    $('.root-item-link').each(function () {
        var thisHeight = $(this).height();
        var wrapperHeight = $(this).closest('li').height();
        var topPos = (wrapperHeight - thisHeight) / 2;
    	var newHeight = wrapperHeight - topPos;
        $(this).css({ 'padding-top': topPos + 'px', 'height': newHeight + 'px' });
    });
}

function hAlignNavItem() {
    var navWidth = $('#csp-nav').width();
    var menuItemCount = $('li.root-item').length;
	$('#main-nav').addClass('main-nav-' + menuItemCount);
}

function initMenu() {
	$('#main-nav li.root-item:last').addClass('selected');
    menuEffect();
	
    hAlignNavItem();
    
    var subMenuItemCount;
    var widthPercent;
    $('ul.sub-menu').each(function () {
        subMenuItemCount = $(this).find('li.sub-item').length;
        widthPercent = (100 / subMenuItemCount) - 3;
        $(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '%; padding-right: 3%');
    });

    vAlignNavText();
}

function menuEffect() {
	if (window.location.href.indexOf('&home=true') < 0)
		$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
}

function AddExceptionMenuItems() {
	var firstItems = categories.FindByAttr('level', 1);
	if (firstItems.length > 2) {
		for (var i = 0; i < firstItems.length - 1; i++) {
			for (var j = i + 1; j < firstItems.length; j++) {
				if (parseInt(firstItems[j].displayOrder) < parseInt(firstItems[i].displayOrder)) {
					var tempItem = firstItems[j];
					firstItems[j] = firstItems[i];
					firstItems[i] = tempItem;
				}
			}
		}
		var homeUrl = firstItems[1].href + '&home=true';
		var contactUrl = 'd1.aspx?p15():c14(ct31000&cd133i99)[st(ct31000*Status_Sort_Order*)]:c16(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
		firstItems[0].href = homeUrl;
		firstItems[firstItems.length - 1].href = contactUrl;
	}
}

function RenderCustomBreadcrumb(elId, catId, level) {
	var el = $(elId);
	var categoryEl = categories.Find(catId);
	var theLineage = GetCategoryLineage(catId);
	var html = '';
	var theUrl;
	for (var i = 0; i < theLineage.length; i++) {
		if (theLineage[i] == '') continue;
		if (theLineage[i] == rootCatId) {
			categoryEl = categories.Find(catId);
			theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href + '&home=true' : "javascript:void(0);");
			html += '<a href="' + theUrl + '">Home</a>  &raquo;  ';
			continue;
		}
		else if (theLineage[i] == catId) {
			categoryEl = categories.Find(catId);
			html += categoryEl.text;
			continue;
		}
		categoryEl = categories.Find(parseInt(theLineage[i]));
		if (categoryEl == null) continue;
		theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href : "javascript:void(0);");
		html += '<a ccid="' + categoryEl.cnid + '" catid="' + categoryEl.id + '" href="' + theUrl + '">' + categoryEl.text + '</a>  &raquo;  ';
	}
	if (html != "")
		el.html(html);
};

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	if (categoryItem == null)
		return lineageArr;
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}