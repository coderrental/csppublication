var tickValue = '';
var kalturaPId = '';
var kalturaUId = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;
var MAIN_MENU_MAX_LEVEL = 2;
var LANG_RTL = ['ar', 'arc', 'bcc', 'bqi', 'ckb', 'dv', 'fa', 'glk', 'he', 'mzn', 'pnb', 'ps', 'sd', 'ug', 'ur', 'yi', '26'];
var INVALID_EMAIL_LANG = {
	'en': 'Invalid Email address',
	'xu': 'Invalid Email address',
	'de': 'Ongeldig e-mailadres',
	'fr': 'Adresse courriel incorrecte',
	'ru': 'Неверный адрес электронной почты',
	'he': 'כתובת דוא"ל לא חוקית',
	'pl': 'Błędny adres e-mail'
}

Array.prototype.chunk = function(chunkSize) {
    var array=this;
    return [].concat.apply([],
        array.map(function(elem,i) {
            return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
        })
    );
}

String.prototype.escapeSpecialChars = function() {
    return this.replace(/\\n/g, "\\n")
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f")
			   .replace("\\r", "")
			   ;
	};
function fixHElanguage(data) {
            data = JSON.stringify(data);
			data = jQuery.parseJSON(data.escapeSpecialChars());			
			data = data.replace(/\\/g, "\\\\").replace(/\\\\"/g, '\\"');
			data = jQuery.parseJSON(data);
			return data;
			
	};


function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);		
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

function getURLParameter(name, givenstring) {
    return decodeURI(
        (RegExp('(^|&)' + name + '=(.+?)(&|$)').exec(givenstring)||[,,null])[2]
    );
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

function GoReport() {
	try{
		if(typeof(_tag)!="undefined"&&typeof(CspReportLib)!="undefined") 
			CspReportLib.collect(_tag);
	}
	catch(e){};
	
}

function checkEmail(email) {
	var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return re.test(email);
}

var motorolaModule = angular.module('ngMotorola', []).config(function($routeProvider) {
	
	$routeProvider.
		when('/', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/main.html'}).
		when('/i/:tick', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/main.html'}).
		when('/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/main.html'}).
		when('/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/main.html'}).
		
		when('/products/:id', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products.html'}).
		when('/products/:id/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products.html'}).
		
		when('/categorydetail/:id', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/category.html'}).
		when('/categorydetail/:id/i/:tick', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/category.html'}).
		when('/categorydetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/category.html'}).
		when('/categorydetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/category.html'}).
		
		when('/products/:parentid/productdetail/:id', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		
		when('/products/:parentid/productdetail/:id/tab/:tabname', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/motorolasolutions/products-detail.html'}).
		
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		
		when('/contactus/title/:titlevalue', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		when('/contactus/title/:titlevalue/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/motorolasolutions/contactus.html'}).
		//otherwise({ redirectTo: '/' });
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i>=0)
					return path.substring(i+9);
			} catch(e) {}
			return '/';
		} });
	
});

motorolaModule.directive('myPromotionDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			setTimeout(function() {
				jQuery("div.promotionAssets div.csp-tab-link a").each(function(){
					var el = jQuery(this);
					if (!isMobile()) {
						if (el.attr('href').indexOf('.pdf') == -1)
							el.colorbox({iframe:true, height: 600, width: 650});
					}
					else {
						el.attr("target","_blank");
					}
				});
			}, 1000);
		}
	};
});
 
 motorolaModule.directive('myCarouselDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			/*
			setTimeout(function() {
				jQuery("#home-carousel").carousel({
					interval: 6000
				});
				jQuery("#home-carousel").swiperight(function() {  
					jQuery("#home-carousel").carousel('prev');  
				});  
				jQuery("#home-carousel").swipeleft(function() {  
					jQuery("#home-carousel").carousel('next');  
				});
			}, 500);
			*/
			window.setTimeout(function() {
				jQuery(document).click();
			}, 3500);
		}
	};
});

 motorolaModule.directive('myProductDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			setTimeout(function() {
				jQuery(document).click();
			}, 2500);
		}
	};
});

 motorolaModule.directive('myProductdetailDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$first)
			scope.$parent.firstTabName = scope.featuredTabItem.name;
		if (scope.$last){
			if (scope.$parent.featureBlocks.length == 0)
				scope.$parent.showTabContent(scope.$parent.firstTabName);
			setTimeout(function() {
				jQuery(document).click();
			}, 3000);
		}
	};
});
 
function ContactUsCtrl($rootScope, $scope, $http, $location, $routeParams, LeadgenForm, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	
	$rootScope.initCspReportAndDeeplink();

	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.contactusMessage = resultMsg;
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionType"] = 'ContactUs';
			CspReportLib.wt.DCSext["ConversionContent"] = 'ContactUs';
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};

	$scope.submit = function() {
		jQuery('#validateEmail').remove();
		var email = jQuery('#email').val();
		if(!checkEmail(email)){
			var invalidEmailMsg = typeof(INVALID_EMAIL_LANG[$rootScope.GetCurrentLangCode()]) != 'undefined' ? INVALID_EMAIL_LANG[$rootScope.GetCurrentLangCode()] : INVALID_EMAIL_LANG['en'];
			jQuery('#email').after("<span id='validateEmail' style = 'color:red;font-size:12px'>" + invalidEmailMsg + "</span>");
		}
		else{
			$scope.contactusMessage = LeadgenForm.submit('form1', $scope.submitted);
		}
	};
	
	$scope.callWebTrends = function() {
		$rootScope.pageTitle = $rootScope.ContactUsItem.title;
		jQuery(document).attr('title', $rootScope.ContactUsItem.title);
		
		if (typeof(CspReportLib) != 'undefined') {
			window.setTimeout(function() {
				var olDCSext = CspReportLib.wt.DCSext;
				CspReportLib.wt.DCSext["csp_pageTitle"] = 'ContactUs';
				CspReportLib.wt.DCSext["ConversionShown"] = 'ContactUs';
				CspReportLib.wt.DCSext["ConversionType"] = 'ContactUs';
				CspReportLib.wt.DCSext["ConversionContent"] = 'ContactUs';
				CspReportLib.wt.DCSext["ConversionClick"] = null;
				CspReportLib.wt.dcsMultiTrack();
				CspReportLib.wt.DCSext = olDCSext;
			}, 1500);
		}
	};
        
    LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		if (!$rootScope.ProductDeferLoaded)
			$rootScope.CallProductDefer();
		
		$scope.callWebTrends();
		
		setTimeout(function() {
			jQuery(document).click();		
		}, 1000);
	});
}

function CategoryCtrl($scope, $rootScope, $http, $routeParams, $filter, $location, Products, DataProducts, URLHandler, Translation) {
	tickValue = URLHandler.getTick();
	var categoryId = typeof($routeParams.id) == "undefined" ? '' : $routeParams.id;
	$scope.breadcrumbs = [];
	$scope.crrCategoryItem;
	$scope.ajaxLoading = true;
	$scope.productList = [];
	$scope.label = {
		learnmore: 'learnmore'
	};
	
	Translation.promise.then(function(resultData){
		for (key in $scope.label) {
			$scope.label[key] = Translation.translate(resultData, $scope.label[key]);
		}
	});
	
	
	var RequestForBreadcrumb = function ($scope, categoryid, breadcrumbInfo, productList) {
		if (categoryid == Products.rootId) {
			var bcHomeItem = {title: 'home', link: '#/', level: ''};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
			});
			return;
		}
		var cateItem = $filter('filter')(productList, {categoryId: categoryid}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		breadcrumbInfo.unshift({title: cateItem.title, link: '#/products/' + cateItem.cspuniquekey, level: cateItem.depth});
		RequestForBreadcrumb($scope, cateItem.parentId, breadcrumbInfo, productList);
	}
	
	if (!$rootScope.ProductDeferLoaded)
		$rootScope.CallProductDefer();
	
	Products.promise.then(function(resultData){
		var requestedBreadcrumb = [];
		$scope.crrCategoryItem = $filter('filter')(resultData, {cspuniquekey: categoryId}, function (expected, actual) {return expected == actual;})[0];
		$rootScope.updatePageInfo(null, $scope.crrCategoryItem.title);
		RequestForBreadcrumb($scope, $scope.crrCategoryItem.parentId, requestedBreadcrumb, resultData);
		DataProducts.promise.then(function(resultProductDetailData){
			$scope.productList = $filter('filter')(resultProductDetailData, {categoryId: $scope.crrCategoryItem.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$scope.ajaxLoading = false;
		});
	});
}

function ProductDetailCtrl($scope, $rootScope, $http, $routeParams, $filter, $location, Products, DataProducts, URLHandler, Translation, LeadgenForm) {
	tickValue = URLHandler.getTick();
	var productId = typeof($routeParams.id) == "undefined" ? '' : $routeParams.id;
	var parentId = typeof($routeParams.parentid) == "undefined" ? '' : $routeParams.parentid;
	var addedFeatured = [], placeHolder = [], addedPromos =[], productAssets = [];
	$scope.translatedData;
	$scope.count = 1;
	$scope.addedTypes = [];
	$scope.localFeaturedList = [];
	$scope.currentAssetType = '';
	$scope.ajaxLoading = true;
	$scope.firstVideoBlock = null;
	$scope.productImage = [];
	$scope.breadcrumbs = [];
	$scope.defaultTab = typeof($routeParams.tabname) == "undefined" ? '' : $routeParams.tabname;
	$scope.defaultTab = $scope.defaultTab.replace(/\W+/g, '');
	$scope.defaultTab = $scope.defaultTab.replace(' ', '');
	$scope.accessories = [];
	$scope.promosList = [];
	$scope.assetStore = [];
	$scope.featureBlocks = [];
	
	
	
	$scope.GetProductLink = function (prodItem, catItem) {
		var defaultUrl = '#/products/' + $scope.productId + '/productdetail/' + prodItem.id;
		if (catItem != undefined)
			defaultUrl = '#/products/' + catItem.cspuniquekey + '/productdetail/' + prodItem.id;
		return defaultUrl;
	};
	
	$scope.showTabContent = function (tabName) {
		if (tabName == '') return;
		for (var i = 0; i < $scope.assetStore.length; i++) {
			if (tabName == $scope.assetStore[i].name) {
				$scope.localFeaturedList[i].content = $scope.assetStore[i].content.slice(0);
				window.setTimeout(function() {
					$scope.callAssetsWebtrends();
				}, 1000);
			}
			else
				$scope.localFeaturedList[i].content = [];
		}
	};
	
	$scope.tabClick = function(tabName, $event) {
		var currentUrl = decodeURI(document.URL);
		currentUrl = currentUrl.replace('#', '/#/');
		currentUrl = currentUrl.replace('//', '/');
		var urlParts = currentUrl.split('/#/')[1];
		var newUrl = '';
		try {
			urlParts = urlParts.split('/');
			for (var i = 0; i < urlParts.length; i++) {
				if (urlParts[i] == 'tab') continue;
				if (urlParts.indexOf('tab') > 0 && i == urlParts.indexOf('tab') + 1) continue;
				if (tickValue != '') {
					if (urlParts[i] == 'i' || i == urlParts.indexOf('i') + 1) continue;
				}
				if (newUrl != '') newUrl += '/';
				newUrl += urlParts[i];
			}
			
			if (tabName != '')
				newUrl += '/tab/' + tabName;
			if (tickValue != '')
				newUrl += '/i/' + tickValue;
			$location.path(newUrl, false);
			try{parent.postMessage(currentUrl.split('/#/')[0] + '/#/' + newUrl,"*");} catch(ex){};
			
		} catch (exc) {}
		$scope.showTabContent(tabName);
		return false;
	};
	
	$scope.GetAccessoriesList = function () {
		if (typeof($scope.product.recommendedaccess) == 'undefined' || $scope.product.recommendedaccess == '') return;
		
		var accessories = $scope.product.recommendedaccess.split(',');
		var ctId = '51000';
		var fieldSplit = 0;
		var fieldList = ['CSP_Category_ID', 'CSP_Unique_Key', 'Content_Title', 'Content_Title_Secondary', 'Content_Description_Short', 'Content_Description_Long', 'CSP_ExtendedSpecsJson', 'File_Image_Thumbnail_Url', 'File_Image_Url', 'Product_Number_MFPN', 'ContentCategoryID', 'ContentCategoryParentID', 'Status_Sort_Order', 'Status_Featured_Y_N', 'CSP_Reporting_Id', 'Search_RecommendedAccessoriesOptions'];
		var fieldDic = {
			"CSP_Unique_Key"						: "id",
			"Content_Title"						  	: "title",
			"Content_Title_Secondary"				: "secondarytitle",
			"Content_Description_Short"				: "description",
			"Content_Description_Long"				: "descriptionlong",
			"CSP_ExtendedSpecsJson"					: "spec",
			"File_Image_Thumbnail_Url"				: "thumbnailUrl",
			"File_Image_Url"						: "imageUrl",
			"Product_Number_MFPN"					: "partNum",
			"ContentCategoryID"						: "categoryId",
			"ContentCategoryParentID"				: "parentCategoryId",
			"Status_Sort_Order"						: "displayOrder",
			"Status_Featured_Y_N"					: "statusfeatured",
			"CSP_Reporting_Id"						: "reportid",
			"Search_RecommendedAccessoriesOptions"	: "recommendedaccess",
			"CSP_Category_ID"						: "cspcategoryid"
		};
		
		for (var z = 0; z < accessories.length; z++) {
			if(accessories[z] == '') continue;
			$http.get('d2.aspx?p2012(ct51000&fCSP_Unique_Key~1=' + accessories[z] + '!)[st(ct51000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(accessoryData) {
				if (accessoryData.length == 0) return;
				fieldSplit = $rootScope.GetFieldSplitNo(ctId, accessoryData);
				var accessObj = $rootScope.ParseObject(accessoryData, fieldSplit, ctId, fieldList, fieldDic);
				$scope.accessories.push(accessObj[0]);
				if (z == accessories.length - 1)
					window.setTimeout(function() { jQuery(document).click(); }, 2000);
			});
			if (z == accessories.length - 1 && !$rootScope.ProductDeferLoaded)
				$rootScope.CallProductDefer();
		}
	};
	
	$scope.GoToAccessory = function (aItem) {
		//$scope.ajaxLoading = true;
		$http.get('d1.aspx?p2003(ct31000&cd' + aItem.cspcategoryid + 'i0)[st(ct31000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(resultData) {
			var assetCategory = resultData[0];
			window.location = $rootScope.baseDomain + '#/products/' + assetCategory.cspuniquekey + '/productdetail/' + aItem.id;
			//$scope.ajaxLoading = false;
		});
	}
	
	$scope.label = {
		contactus: 'Contact Us',
		contactustoday: 'contactustoday',
		overview: 'overview',
		accessories: 'accessories',
		productgallery: '',
		productvideo: '',
		customerstories: 'Customer Stories'
	};
	
	$scope.CallReport = function () {
		console.log('Call report');
	};
	
	$scope.getVideoLink = function (videoUrl) {
		return (videoUrl != '') ? videoUrl : 'javascript: void(0)';
	}
	
	$scope.getLinkTarget = function (launchType) {
		var targetType = '';
		if (launchType == 'newwindow')
			targetType = '_blank';
		return targetType;
	};
	
	$scope.renderredType = function (assetType) {
		var sample = assetType.localfeatured + '-' + assetType.type;
		if ($scope.addedTypes.indexOf(sample) == -1) {
			$scope.addedTypes.push(sample);
			return true;
		}
		return false;
	};
	
	$scope.getEmbedVideoDimension = function (obj) {
		var videoUrl = (obj.videoUrl != undefined) ? obj.videoUrl : obj.linkurl;
		return getURLParameter('width', videoUrl) + 'x' + getURLParameter('height', videoUrl);
	};
	
	$scope.getEmbedVideo = function(obj) {	
		var videoIdParamName = 'entry_id';
		var videoUrl = (obj.videoUrl != undefined) ? obj.videoUrl : obj.linkurl;
		var videoId = getURLParameter(videoIdParamName, videoUrl);
		var friendlyUrlPart = videoUrl.split('?')[0].split('/');
		
		if (placeHolder.indexOf(obj) == -1 && videoUrl.indexOf('embed') > 0) {
			placeHolder.push(obj);
			
			setTimeout(function() {
				if (jQuery('#generated-playerscript-holder').find('script').length == 0) {
					jQuery.getScript(videoUrl.split('?')[0], function() {
					});
					jQuery('#generated-playerscript-holder').append("<script type='text/javascript' src='" + videoUrl.split('?')[0] + "'><\/script>");
					for (var i = 0; i < friendlyUrlPart.length; i++) {
						if (friendlyUrlPart[i] == 'partner_id')
							kalturaPId = friendlyUrlPart[i + 1];
						if (friendlyUrlPart[i] == 'uiconf_id')
							kalturaUId = friendlyUrlPart[i + 1];
					}
				}
			}, 500);
		}
		return videoId;
	}
	
	$scope.CreateTab = function(assetType) {
		showHeader = (assetType.type != $scope.currentAssetType); 
		$scope.currentAssetType = assetType.type;
		assetType.label = Translation.translate($scope.translatedData, assetType.type);
		return showHeader;
	};
	
	$scope.goEncodeTitle = function (title) {
		return encodeURIComponent(title);
	}
	
	$scope.galleryItemClicked = function (item) {
		$scope.firstGalleryBlock = item;
	};
	
	$scope.GetPDFAssetClassName = function (assetUrl) {
		var pdfExtension = assetUrl.split('.').pop();
		
		return 'asset' + pdfExtension;
	}
	
	$scope.GetBCLink = function (bcItem) {
		var leafNodeIndex = 0;
		var crrItemIndex = 0;
		for (var i = 0; i < $scope.breadcrumbs.length; i++) {
			if (leafNodeIndex == 0) {
				for (var j = 0; j < $rootScope.LeafCategories.length; j++) {
					if ($scope.breadcrumbs[i].cspuniquekey.toLowerCase() == $rootScope.LeafCategories[j].value.cspuniquekey.toLowerCase()) {
						leafNodeIndex = i;
						break;
					}
				}
			}
			if ($scope.breadcrumbs[i].cspuniquekey == bcItem.cspuniquekey)
				crrItemIndex = i;
		}
		
		if (leafNodeIndex > 0 && crrItemIndex > leafNodeIndex)
			return bcItem.link.replace('#/products/', '#/categorydetail/');
		return bcItem.link;
	}
	
	$scope.GetProductImages = function () {
		//Get page content blocks (gallery & feature)
		$http.get('d1.aspx?p2009(ct38000&fLocal_Keywords~1=' + productId + '!)[st(ct38000*Status_Block_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(responseData) {
			var galleryBlocks = $filter('filter')(responseData, {blockId: 'gallery'}, function (expected, actual) {return expected == actual;});
			var videoBlocks = $filter('filter')(responseData, {blockId: 'video'}, function (expected, actual) {return expected == actual;});
			var featureBlocksData = $filter('filter')(responseData, {blockId: 'feature'}, function (expected, actual) {return expected == actual;});
			$scope.contentBlocks = responseData;
			$scope.videoBlocks = videoBlocks;
			$scope.featureBlocks = featureBlocksData;
			
			if ($scope.featureBlocks.length == 0 && $scope.assetStore.length > 0)
				$scope.localFeaturedList[0].content = $scope.assetStore[0].content.slice(0);
			
			if ($scope.featureBlocks.length > 0)
				$scope.callWebTrends();
			
			//Get product detail image from gallery
			if (galleryBlocks.length > 0) {
				$scope.firstGalleryBlock = galleryBlocks[0];
				$scope.label.productgallery = galleryBlocks[0].blockName;
				$scope.productImage.thumbnail = galleryBlocks[0].imageUrl;
				$scope.productImage.image = galleryBlocks[0].imageUrl;
			}
			//Get product detail image from data product
			else {	
				$scope.productImage.thumbnail = $scope.product.thumbnailUrl;
				$scope.productImage.image = $scope.product.imageUrl;
			}
			
			if (videoBlocks.length > 0) {
				$scope.demoVideoBlocks = $filter('filter')(videoBlocks, {blockName: 'demo'}, function (expected, actual) {return expected.toLowerCase().indexOf(actual) >= 0;});
				$scope.firstVideoBlock = $filter('filter')(videoBlocks, {blockName: 'product'}, function (expected, actual) {return expected.toLowerCase().indexOf(actual) >= 0;});
				if ($scope.demoVideoBlocks.length>0)
					$scope.label.productvideo = $scope.demoVideoBlocks[0].blockName;
				else
					$scope.label.productvideo = "";
				if ($scope.firstVideoBlock.length > 0)
					$scope.firstVideoBlock = $scope.firstVideoBlock[0];
				else 
					$scope.firstVideoBlock = null;
			}
			
			$scope.GetAllAssets();
		});
	};
	
	$scope.GetAllAssets = function () {
		$http.get('d1.aspx?p2004(ct21000&fLocal_Keywords~1=' + productId + '!)[st(ct21000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
			data.pop();
			//console.log(data);
			Translation.promise.then(function (resultData) {
				$scope.translatedData = resultData;
				var validTabName = false;
				var promosArr = ["Promos","Акции","פרומואים"];
				for (var i = 0; i < data.length; i++) {
					if (addedFeatured.indexOf(data[i].localfeatured) == -1 && promosArr.indexOf(data[i].localfeatured) == -1) {
						var tabid = data[i].localfeatured.replace(/[`~!@#$%^&*()_|+\-=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '');
						tabid = tabid.replace(/ /g,'');
						var temp = {
							name: tabid,
							label: Translation.translate(resultData, data[i].localfeatured).toUpperCase(),
							content: []//$filter('filter')(data, {localfeatured: data[i].localfeatured}, function (expected, actual) {return expected == actual;})
						};
						var tempWithContent = {
							name: tabid,
							label: Translation.translate(resultData, data[i].localfeatured).toUpperCase(),
							content: $filter('filter')(data, {localfeatured: data[i].localfeatured}, function (expected, actual) {return expected == actual;})
						};
						addedFeatured.push(data[i].localfeatured);
						$scope.localFeaturedList.push(temp);
						$scope.assetStore.push(tempWithContent);
						
						if (data[i].localfeatured.replace(' ','') == $scope.defaultTab.replace(' ',''))
							validTabName = true;
					}
				}

				
				for (var i = 0; i < data.length; i++) {
					if (addedPromos.indexOf(data[i].localfeatured) == -1 && promosArr.indexOf(data[i].localfeatured) != -1) {
						var temp = {
							name: data[i].localfeatured,
							label: Translation.translate(resultData, data[i].localfeatured).toUpperCase(),
							content: $filter('filter')(data, {localfeatured: data[i].localfeatured}, function (expected, actual) {return expected == actual;})
						};
						addedPromos.push(data[i].localfeatured);
						$scope.promosList.push(temp);
					}
				}
			
				
				if ($scope.defaultTab != '') { 
					if (!validTabName) {
						var currentUrl = decodeURI(document.URL);
						currentUrl = currentUrl.replace('#', '/#/');
						currentUrl = currentUrl.replace('//', '/');
						var urlParts = currentUrl.split('/#/')[1];
						urlParts = urlParts.split('/');
						//console.log(urlParts);
						var newUrl = '';
						for (var i = 0; i < urlParts.length; i++) {
							if (urlParts[i] == 'tab' || i == urlParts.indexOf('tab') + 1) continue;
							if (newUrl != '') newUrl += '/';
							newUrl += urlParts[i];
						}
						$scope.defaultTab = '';
						$location.path(newUrl, false);
					}
					else {
						$scope.tabClick($scope.defaultTab.replace(' ',''));
					}
				}
				
				$scope.assets = data;
				$scope.label.contactus = Translation.translate(resultData, $scope.label.contactus);
				$scope.label.contactustoday = Translation.translate(resultData, $scope.label.contactustoday);
				$scope.label.overview = Translation.translate(resultData, $scope.label.overview).toUpperCase();
				$scope.label.customerstories = Translation.translate(resultData, $scope.label.customerstories).toUpperCase();
				$scope.label.accessories = Translation.translate(resultData, $scope.label.accessories).toUpperCase();
				//$scope.ajaxLoading = false;
				
				//Contact Us button label
				$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
					var navContactUs = $filter('filter')(data, {identifier: 'contactus'}, function (expected, actual) {
						expected = expected.toLowerCase();
						expected = expected.replace(/\s/g, '');
						return expected == actual;
					});
					
					if (navContactUs.length > 0) {
						//console.log(navContactUs, navContactUs[0].title);
						$scope.label.contactus = navContactUs[0].title;
					}
				});
				
				if ((typeof($scope.product.recommendedaccess) == 'undefined' || $scope.product.recommendedaccess == '') && !$rootScope.ProductDeferLoaded)
					$rootScope.CallProductDefer();
				
				//$rootScope.updatePageInfo(null, $scope.product.title);
				window.setTimeout(function() {
					jQuery(document).click();
				}, 1500);
			});
		});
	};
	
	$rootScope.initCspReportAndDeeplink();
	
	$scope.callAssetsWebtrends = function () {
		$rootScope.pageTitle = $scope.product.title;
		jQuery(document).attr('title', $rootScope.pageTitle);
		jQuery('#csp-report-lib').remove();
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.id = "csp-report-lib";
		script.src = "js/CspReportLib.js";
		// clear CspReportLib 
		if (typeof (CspReportLib) != "undefined") {
			CspReportLib.wt.DCSext["ConversionContent"] = null;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = null;
			CspReportLib.wt.DCSext["ConversionType"] = null;
			CspReportLib.wt.DCSext["csp_vname"] = null;
			CspReportLib.wt.DCSext["csp_vaction"] = null;
		}
		document.getElementsByTagName('head')[0].appendChild(script);
	};
	
	$scope.callWebTrends = function () {
		$rootScope.pageTitle = $scope.product.title;
		jQuery(document).attr('title', $rootScope.pageTitle);
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["csp_pageTitle"] = $rootScope.pageTitle;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionType"] = null;
			CspReportLib.wt.DCSext["ConversionContent"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = null;
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};
	
	var RequestForBreadcrumb = function ($scope, categoryid, breadcrumbInfo, productList) {
		if (categoryid == Products.rootid()) {
			var bcHomeItem = {title: 'home', link: '#/', level: '', cspuniquekey: ''};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
				// if ($rootScope.isRTLLang) {
					// $scope.breadcrumbs.reverse();
				// }
			});
			return;
		}
		var cateItem = $filter('filter')(productList, {categoryId: categoryid}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		if (cateItem == undefined) {
			RequestForBreadcrumb($scope, Products.rootid(), breadcrumbInfo, productList);
			return;
		}
		breadcrumbInfo.unshift({title: cateItem.title, link: '#/products/' + cateItem.cspuniquekey, level: cateItem.depth, cspuniquekey: cateItem.cspuniquekey});
		RequestForBreadcrumb($scope, cateItem.parentId, breadcrumbInfo, productList);
	};
	
	$scope.InitBreadcrumb = function (crrProductItem, data) {
		var requestedBreadcrumb = [];
		requestedBreadcrumb.push({title: crrProductItem.title, link: '#/products/' + crrProductItem.cspuniquekey, level: crrProductItem.depth, cspuniquekey: crrProductItem.cspuniquekey});
		RequestForBreadcrumb($scope, crrProductItem.parentId, requestedBreadcrumb, data);
	};
	
	$scope.GetBreadcrumb = function () {
		Products.promise.then(function(topLevelData){
			//Breadcrumb
			$http.get('d2.aspx?p2003(ct31000&fCSP_Unique_Key~1=' + parentId + '!)[]' + URLHandler.getUrlTick()).success(function(dataProd) {
				var prodObj = $rootScope.parseProductObj(dataProd);
				
				//Get full item info
				$http.get('d1.aspx?p2003(ct31000&cd' + prodObj[0].categoryId + 'i0)[st(ct31000*Status_Sort_Order*)]' + tickValue).success(function(itemData) {
					//Breadcrumb calculating
					var crrProductItem = itemData[0];
					var mainProductItem = $filter('filter')(topLevelData, {categoryId: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
					var currentLineage = crrProductItem.lineage.split('/');
					var prodLineage = mainProductItem.lineage.split('/');
					var distLength = currentLineage.length - prodLineage.length;
					if (distLength <= 2)
						$scope.InitBreadcrumb(crrProductItem, topLevelData);
					else {
						$http.get('d1.aspx?p2003(ct31000&cd' + Products.mainproductid() + 'i' + distLength + ')[st(ct31000*Status_Sort_Order*)]' + tickValue).success(function(breadcrumbData) {
							$scope.InitBreadcrumb(crrProductItem, breadcrumbData);
						});
					}
				});
			});
		});
	};
	
	$scope.GetBreadcrumb();
	
	$http({
		url: 'd1.aspx?p2012(ct51000&fCSP_Unique_Key~1=' + productId + '!)[st(ct51000*Status_Sort_Order*)]' + URLHandler.getUrlTick() + '',
		method: 'GET',
		transformResponse: function (resultData, headersGetter) {
			resultData = fixHElanguage(resultData);
			var rawData = $filter('filter')(resultData, {id: productId}, function (expected, actual) {return expected == actual;});
			if (rawData.length == 0) {
				window.location = 'http://' + document.domain;
				return;
			}
			$scope.product = rawData[0];
			
			$scope.GetProductImages();
			$scope.callWebTrends();
			$scope.ajaxLoading = false;
			$scope.GetAccessoriesList();
			//$scope.GetAllAssets();
		}
	});

	
	$scope.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		
	};

	$scope.submit = function() {
		LeadgenForm.submitLeadgen('frmleadgen', $scope.submitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		downloadFormFields = LeadgenForm.getDownloadFormFields();
	});
}

function ProductCtrl($rootScope, $scope, $http, $routeParams, $filter, Products, DataProducts, URLHandler, Translation, PromotionAssets) {
	tickValue = URLHandler.getTick();
	$scope.productId = typeof($routeParams.id)=="undefined"?'':$routeParams.id;
	$scope.categoryContent = [];
	$scope.productSpecTypes = [];
	$scope.productSpecTypesHideFrom = 8; //Number of shown spec items (left col)
	$scope.productList = [];
	$scope.childrenProductList = [];
	$scope.selectedSpecs = {spec: []};
	$scope.selectedSpecItems = [];
	$scope.productCount = 0;
	$scope.featuredList = [];
	$scope.featuredCount = -1;
	$scope.baseDomain = $rootScope.baseDomain;
	$scope.breadcrumbs = [];
	$scope.label = {
		narrowresultby: 'narrowresultsby',
		matchedproducts: 'matchedproducts',
		matchedproductspre: 'thereare',
		matchedproductssuf: 'matchselection',
		yourselection: 'yourselections',
		showall: 'showall',
		minimize: 'minimize',
		explore: 'explore',
		learnmore: 'learn more about',
		browseby: 'Browse by'
	};
	$scope.promotionAssets = [];
	$scope.ajaxLoading = true;
	$scope.childrenCategories = [];
	$scope.productTree = [];
	$scope.isLeafNode = false;
	$scope.categoryRow = [];
	$scope.categoryPerRow = 2;
	var productAssets = [];
	$scope.productAssetsList = [];
	
	$scope.GetPDFAssetClassName = function (assetUrl) {
		var pdfExtension = assetUrl.split('.').pop();
		if(pdfExtension == "pdf")
			return 'asset' + pdfExtension;
		return;
	}
	
	$rootScope.initCspReportAndDeeplink();
	
	var RequestForBreadcrumb = function ($scope, categoryId, breadcrumbInfo, productList) {		
		for (var i = 0; i < productList.length; i++) {
			if (productList[i].categoryId == categoryId) {
				breadcrumbInfo.unshift({title: productList[i].title, link: '#/products/' + productList[i].cspuniquekey});
				if (productList[i].parentId != Products.rootid())
					RequestForBreadcrumb($scope, productList[i].parentId, breadcrumbInfo, productList);
				break;
			}
		}
		
		if (categoryId == Products.mainproductid() || categoryId == Products.rootid()) {
			var bcHomeItem = {title: 'home', link: '#/'};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
				// if ($rootScope.isRTLLang) {
					// $scope.breadcrumbs.reverse();
				// }
			});
		}
	};
	
	$scope.GetProductLink = function (prodItem, catItem) {
		var defaultUrl = '#/products/' + $scope.productId + '/productdetail/' + prodItem.id;
		if (catItem != undefined)
			defaultUrl = '#/products/' + catItem.cspuniquekey + '/productdetail/' + prodItem.id;
		return defaultUrl;
	};
	
	
	$scope.InitBreadcrumb = function (data) {
		var requestedBreadcrumb = [];
		RequestForBreadcrumb($scope, $scope.categoryContent.parentId, requestedBreadcrumb, data);
						
		for (var i = $scope.breadcrumbs.length - 1; i >= 0; i--) {
			var bLinkParts = $scope.breadcrumbs[i].link.split('/');
			if (bLinkParts[bLinkParts.length - 1] == mainProductItem.cspuniquekey) {
				$scope.breadcrumbs.splice(i, 1);
				break;
			}
		}
		
		$scope.ajaxLoading = false;
	};
	
	$scope.callWebTrends = function () {
		$rootScope.pageTitle = $scope.categoryContent.title;
		jQuery(document).attr('title', $rootScope.pageTitle);
		if (typeof(CspReportLib) != 'undefined') {
			var olDCSext = CspReportLib.wt.DCSext;
			CspReportLib.wt.DCSext["csp_pageTitle"] = $scope.categoryContent.title;
			CspReportLib.wt.DCSext["ConversionShown"] = null;
			CspReportLib.wt.DCSext["ConversionType"] = null;
			CspReportLib.wt.DCSext["ConversionContent"] = null;
			CspReportLib.wt.DCSext["ConversionClick"] = null;
			CspReportLib.wt.dcsMultiTrack();
			CspReportLib.wt.DCSext = olDCSext;
		}
	};
	
	//This raw request to get current categoryId due to CSP_Unique_Key from the URL param
	$http.get('d2.aspx?p2003(ct31000&fCSP_Unique_Key~1=' + $scope.productId + '!)[]' + URLHandler.getUrlTick()).success(function(dataProd) {
		var prodObj = $rootScope.parseProductObj(dataProd);
		
		//Get next level children
		$http.get('d1.aspx?p2003(ct31000&cd' + prodObj[0].categoryId + 'i1)[st(ct31000*Status_Sort_Order*)]' + tickValue).success(function(resultData) {
			$scope.categoryContent = $filter('filter')(resultData, {categoryId: prodObj[0].categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
			
			
			$scope.callWebTrends();
			
			if (!$rootScope.ProductDeferLoaded)
				$rootScope.CallProductDefer();
			
			//Default request to get Root Product Item
			Products.promise.then(function(topLevelData){
				//Breadcrumb calculating
				var mainProductItem = $filter('filter')(topLevelData, {categoryId: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
				var currentLineage = $scope.categoryContent.lineage.split('/');
				var prodLineage = mainProductItem.lineage.split('/');
				var distLength = currentLineage.length - prodLineage.length;
				
				
				if (distLength <= 2) {
					$scope.InitBreadcrumb(topLevelData);
				}
				else {
					//This request is for Breadcrumb
					$http.get('d1.aspx?p2003(ct31000&cd' + Products.mainproductid() + 'i' + distLength + ')[st(ct31000*Status_Sort_Order*)]' + tickValue).success(function(breadcrumbData) {
						$scope.InitBreadcrumb(breadcrumbData);
					});
				}
			});
			
			//Get children
			$scope.childrenCategories = $filter('filter')(resultData, {parentId: $scope.categoryContent.categoryId}, function (expected, actual) {return expected == actual;});
			$scope.childrenCategories.sort(function(a, b) {
				return parseInt(a.displayorder) - parseInt(b.displayorder);
			});
			
			for (var j = 0; j < $scope.childrenCategories.length; j = j + $scope.categoryPerRow) {
				var temp = [];
				for (var k = j; k < j + $scope.categoryPerRow; k++) {
					if (typeof($scope.childrenCategories[k]) != 'undefined')
						temp.push({
							category: $scope.childrenCategories[k],
							product: []
						});
				}
				$scope.categoryRow.push(temp);
			}
			//Product list of current category item $scope.categoryContent
			$http({
				method : 'GET',
				url: 'd1.aspx?p2012(ct51000&cd' + prodObj[0].categoryId + 'i0)[st(ct51000*Status_Sort_Order*)]' + tickValue + '',
				responseType : 'text',
				transformResponse: function (resultProductDetailData, headersGetter) {
					resultProductDetailData = fixHElanguage(resultProductDetailData);
					
					$scope.featuredList = $filter('filter')(resultProductDetailData, {categoryId: $scope.categoryContent.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
					
					$scope.featuredRow = [];
					 
					$scope.featuredRow = $scope.featuredList.chunk(2);
					
					$scope.featuredCount = $scope.featuredList.length;
					
					//$rootScope.updatePageInfo(null, $scope.categoryContent.title);
					$scope.mergeProductArr = $scope.childrenCategories.concat($scope.featuredList);
					$scope.mergeProductArrRow = $scope.mergeProductArr.chunk(2);
					$scope.GetAllAssets();				
				}
			});
		});
	});
	
	$scope.GetAllAssets = function () {
		$http.get('d1.aspx?p2004(ct21000&cd' + $scope.categoryContent.categoryId + 'i0)[st(ct21000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
			data.pop();
			Translation.promise.then(function (resultData) {
				$scope.translatedData = resultData;
				var promosArr = ["Promos","Акции","פרומואים"];
				for (var i = 0; i < data.length; i++) {
					if (productAssets.indexOf(data[i].localfeatured) == -1 && promosArr.indexOf(data[i].localfeatured) != -1) {
						var temp = {
							name: data[i].localfeatured,
							label: Translation.translate(resultData, data[i].localfeatured).toUpperCase(),
							content: $filter('filter')(data, {localfeatured: data[i].localfeatured}, function (expected, actual) {return expected == actual;})
						};
						productAssets.push(data[i].localfeatured);
						$scope.productAssetsList.push(temp);
					}
				}
				console.log($scope.productAssetsList);
			});
		});
	};
	
	
	$scope.getProdLink = function (prodId) {
		return $rootScope.baseDomain + '#/products/' + $scope.productId + '/productdetail/' + prodId;
	}
	
	$scope.getId = function (spec, specDetailIndex) {
		var specName = spec.name.toLowerCase().split(' ').join('-');
		return (specName + '-' + specDetailIndex);
	}
	
	Translation.promise.then(function (resultData) {
		$scope.label.matchedproducts = Translation.translate(resultData, $scope.label.matchedproducts);
		$scope.label.narrowresultby = Translation.translate(resultData, $scope.label.narrowresultby);
		$scope.label.yourselection = Translation.translate(resultData, $scope.label.yourselection);
		$scope.label.showall = Translation.translate(resultData, $scope.label.showall);
		$scope.label.minimize = Translation.translate(resultData, $scope.label.minimize);
		$scope.label.thereare = Translation.translate(resultData, "thereare") + " ";
		$scope.label.matchselection = Translation.translate(resultData, "matchselection");
		$scope.label.products = " " + Translation.translate(resultData, "products") + " ";
		$scope.label.explore = Translation.translate(resultData, $scope.label.explore);
	});
}

function MainCtrl($scope, $http, $rootScope, $filter, Products, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.productsList = [];
	$scope.blockHero = [];
	$scope.blockTile = [];
	$scope.ajaxLoading = true;
	var blockData = [];
	
	$scope.getHomeBlockLink = function (block, isLastRequest) {
		block.blocklink = '';
		$http.get('d2.aspx?p2003(ct31000&cd' + block.cspcategoryid + 'i0)[]' + URLHandler.getUrlTick()).success(function(dataProd) {
			if (isLastRequest && !$rootScope.ProductDeferLoaded)
				$rootScope.CallProductDefer();
			if (dataProd.length > 0) {
				var prodObj = $rootScope.parseProductObj(dataProd);
				block.blocklink = typeof(prodObj[0]) == 'undefined' ? '' : $rootScope.baseDomain + "#/products/" + prodObj[0].cspuniquekey;
			}
		});
	};
	
	$scope.GoToBlockLink = function (block) {
		$scope.ajaxLoading = true;
		block.blocklink = '';
		$http.get('d2.aspx?p2003(ct31000&cd' + block.cspcategoryid + 'i0)[]' + URLHandler.getUrlTick()).success(function(dataProd) {
			if (dataProd.length > 0) {
				var prodObj = $rootScope.parseProductObj(dataProd);
				block.blocklink = typeof(prodObj[0]) == 'undefined' ? '' : $rootScope.baseDomain + "#/products/" + prodObj[0].cspuniquekey;
				window.location = block.blocklink;
			}
			$scope.ajaxLoading = false;
		});
	};
	
	$scope.GetHomeBlocks = function () {
		var ctId = '37000';
		var fieldSplit = 0;
		var fieldList = ["Content_Title", "File_Image_Thumbnail_Url", "File_Image_Url", "Content_Description_Short", "Content_Description_Long", "Local_Type", "File_Presentation_1_Url", "ContentCategoryID", "CSP_Reporting_Id"];
		var fieldDic = {
			"Content_Title"					: "title",
			"File_Image_Thumbnail_Url"		: "thumbnail",
			"File_Image_Url"				: "image",
			"Content_Description_Short"		: "description",
			"Content_Description_Long"		: "descriptionlong",
			"Local_Type"					: "type",
			"File_Presentation_1_Url"		: "link",
			"ContentCategoryID"				: "cspcategoryid",
			"CSP_Reporting_Id"				: "reportingId"
		};
		$http.get('d2.aspx?p2002(ct37000&cd' + Products.rootid() + 'i99)[st(ct37000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(homeBlockData) {
			if (homeBlockData.length == 0) return;
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, homeBlockData);
			var homeBlockObj = $rootScope.ParseObject(homeBlockData, fieldSplit, ctId, fieldList, fieldDic);
			
			$scope.blockHero = $filter('filter')(homeBlockObj, {type: 'hero'}, function (expected, actual) {return expected == actual;});
			$scope.blockTile = $filter('filter')(homeBlockObj, {type: 'tile'}, function (expected, actual) {return expected == actual;});
			$scope.blockTile = $scope.blockTile.slice(0, 3);
			if (!$rootScope.ProductDeferLoaded) {
				$rootScope.CallProductDefer();
			}
			
			$rootScope.pageTitle = 'Home';
			$rootScope.initCspReportAndDeeplink();
			$scope.ajaxLoading = false;
		});
	};
	
	$scope.GetHomeBlocks();
}

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Products, DataProducts, URLHandler, Translation) {
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	var menuLevelToShow = MAIN_MENU_MAX_LEVEL;
	var allNavItems = [];
	var mainNavItems = [];
	$scope.ProductList = [];
	//console.log(DataProducts.getProductId());
	var GetFullLink = function (data, i) {
		var domainValue = $rootScope.baseDomain;
		domainValue = '';
		if (data[i].parent == '') {
			if (data[i].link != '')
				data[i].link = domainValue + "#/" + data[i].link;
			else
				data[i].link = domainValue + "#/";
			return data[i].link;
		}
		for (var j = 0; j < data.length; j++) {
			if (data[j].identifier == data[i].parent) {
				data[i].link = data[j].link + '/' + data[i].link;
				if (data[i].link.indexOf('#') != 0)
					data[i].link = domainValue + '#/' + data[i].link;
				return data[i].link;
			}
		}
	}
	
	var GetChildNodes = function (parentNode, treeNode, currentLevel, maxLevel) {
		var childNodes = [];
		if (treeNode.children == undefined) return;
		for (var i = 0; i < treeNode.children.length; i++) {
			var childNode = treeNode.children[i];
			var temp =
					{
						"identifier" : childNode.value.categoryId,
						"title" : childNode.value.title,
						"link" : $rootScope.baseDomain + "#/products/" + childNode.value.cspuniquekey,
						"parent" : parentNode.identifier,
						"level" : childNode.value.depth,
						"displayorder" : childNode.value.displayorder,
						"reportid" : childNode.value.reportingId,
						"cspuniquekey" : childNode.value.cspuniquekey
					};
			/*
			if (temp.level == 2)
				temp.link = '';
			*/
			temp.nodes = [];
			temp.hideSubMenu = false;
			temp.hasNodes = true;//(childNode.children != undefined && currentLevel < maxLevel);
			temp.dataToggleValue = '';
			
			if (temp.hasNodes && currentLevel < maxLevel)
				GetChildNodes(temp, childNode, currentLevel + 1, maxLevel);
			childNodes.push(temp);
		}
		parentNode.nodes = childNodes;
	}
	
	var UpdateProductsMenu = function (maxLevel) {
		var prodItemIndex = 0;
		for (var i = 0; i < $scope.navitems.length; i++) {
			if ($scope.navitems[i].identifier.toLowerCase() == 'products') {
				prodItemIndex = i;
				break;
			}
		}
		
		GetChildNodes($scope.navitems[prodItemIndex], $rootScope.AllProducts[0], 1, maxLevel);
		//return;
		if ($scope.navitems[prodItemIndex].nodes.length <= 3) return;
		
		var productListColCount = 3;
		$scope.navitems[prodItemIndex].blocks = [];
		for (var j = 0; j < $scope.navitems[prodItemIndex].nodes.length; j = j + productListColCount) {
			for (var k = 0; k < productListColCount; k++) {
				var crrIndex = k + j;
				if ($scope.navitems[prodItemIndex].nodes[crrIndex] == undefined) break;
				if ($scope.navitems[prodItemIndex].blocks[k] == undefined) $scope.navitems[prodItemIndex].blocks[k] = [];
				$scope.navitems[prodItemIndex].blocks[k].push($scope.navitems[prodItemIndex].nodes[crrIndex]);
			}
		}
		//console.log($scope.navitems[prodItemIndex]);
	};
	
	$scope.IsValidMainNavItem = function (navitem) {
		return navitem.identifier.toLowerCase() != 'contactus' && navitem.title.trim().length > 0;
	};
	
	$scope.GetChildrenList = function (navItem) {
		if (navItem.level != 2) return;
		//if (navItem.nodes != undefined && navItem.nodes.length > 0) return;
		if (navItem.childrenLoaded != undefined && navItem.childrenLoaded == true) return;
		navItem.childrenLoaded = true;
		DataProducts.promise.then(function(resultData){
			var childNodes = [];
			var productList = $filter('filter')(resultData, {categoryId: navItem.identifier}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			var featuredProducts = $filter('filter')(productList, {statusfeatured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual;});

			//No featured item
			if (featuredProducts.length == 0) {
				var tempChild = 
						{
							"identifier" : '',
							"title" : "Featured item not found",
							"link" : '',
							"parent" : '',
							"level" : parseInt(navItem.level) + 1,
							"displayorder" : '',
							"featured" : '',
							"reportid" : '',
							"cspuniquekey" : ''
						};
				tempChild.nodes = [];
				tempChild.hideSubMenu = false;
				tempChild.hasNodes = false;
				tempChild.dataToggleValue = '';
				navItem.nodes.push(tempChild);
				return;
			}
			
			for (var j = 0; j < featuredProducts.length; j++) {
				var isDup = false;
				for (var k = 0; k < navItem.nodes.length; k++) {
					if (featuredProducts[j].title.toLowerCase() == navItem.nodes[k].title.toLowerCase()) {
						isDup = true;
						break;
					}
				}
				if (isDup) continue;
				var tempChild = 
						{
							"identifier" : featuredProducts[j].id,
							"title" : featuredProducts[j].title,
							"link" : $rootScope.baseDomain + "#/products/" + navItem.cspuniquekey + "/productdetail/" + featuredProducts[j].id,
							"parent" : featuredProducts[j].parentCategoryId,
							"level" : parseInt(navItem.level) + 1,
							"displayorder" : featuredProducts[j].displayOrder,
							"featured" : featuredProducts[j].statusfeatured,
							"reportid" : featuredProducts[j].reportid,
							"cspuniquekey" : ""
						};
				tempChild.nodes = [];
				tempChild.hideSubMenu = false;
				tempChild.hasNodes = false;
				tempChild.dataToggleValue = '';
				navItem.nodes.push(tempChild);
			}
		});
	};
	
	//Get JSON data for navigation
	$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
		data.pop();
		for (var i = 0; i < data.length; i++) {
			data[i].displayorder = parseInt(data[i].displayorder);
			if (data[i].level == '') data[i].level = 1;
			data[i].level == parseInt(data[i].level);
			data[i].nodes = [];
			data[i].hideSubMenu = false;
			data[i].hasNodes = false;
			data[i].dataToggleValue = '';
			if (data[i].level == '' || data[i].level < 2) {
				for (var j = 0; j < data.length; j++) {
					if (data[i].identifier.toLowerCase() == 'products') {
						data[i].hasNodes = true;
						data[i].dataToggleValue = 'dropdown';
						break;
					}
				}
			}
			data[i].link = GetFullLink(data, i);
			
			if (data[i].identifier.toLowerCase() == 'contactus')
				$rootScope.ContactUsItem = data[i];
			
			if (data[i].level <= 1) {
				mainNavItems.push(data[i]);
			}       
			else
				allNavItems.push(data[i]);
		}
		
		for (var i = 0; i < mainNavItems.length; i++) {
			if (mainNavItems[i].hasNodes == true)
				mainNavItems[i].link = '';
		}
		
		$scope.navitems = mainNavItems;

		Translation.promise.then(function (resultData) {								
			for(var b in $scope.navitems) {						
				for(var c in resultData) {
					if ($scope.navitems[b].identifier==resultData[c].contextid && $scope.navitems[b].identifier=="home")
						$scope.navitems[b].title=resultData[c].value;
				}
			}
		});	
			
			
		Products.promise.then(function(resultData){
			resultData.pop();
			$rootScope.AllProducts = Products.convertJSONtoTree(resultData);
			UpdateProductsMenu(menuLevelToShow);
		});
	});
	
	//Show/hide for sub menu
	$scope.showSubItems = function (data) {
		data.hideSubMenu = false;
		if (data.nodes.length > 0) return;
		for (var i = 0; i < allNavItems.length; i++) {
			if (allNavItems[i].parent == data.identifier) {
				data.nodes.push(allNavItems[i]);
			}
		}
	}
	
	$scope.hasNoFeaturedItem = function (nodeItem) {
		if (nodeItem.level < 2) return false;
		var noFeaturedItem = true;
		if (!nodeItem.hasNodes) return noFeaturedItem;
		for (var i = 0; i < nodeItem.nodes.length; i++) {
			if (nodeItem.nodes[i].statusfeatured != undefined && nodeItem.nodes[i].statusfeatured.lowercase == 'yes') {
				noFeaturedItem = false;
				break;
			}
		}
		return noFeaturedItem;
	}
}

motorolaModule.factory('DataProducts', function ($http, Products) {
	var categoryProductId = Products.mainproductid();
	var data = [];
	return {
		promise: {},
		getProductId: function() {
			return categoryProductId;
		},
		setData: function (productList) {
			data = productList;
		},
		getProductSpecTypes: function (productList) {
			var specTypeList = [];
			var addedName = [];
			var addedDetail = [];
			for (var i = 0; i < productList.length; i++) {
				if (productList[i].id === undefined) continue;
				if (typeof productList[i].spec == 'string') {
					var itemSpec = unescape(productList[i].spec);
					var itemSpecJson = JSON.parse(itemSpec);
					productList[i].spec = itemSpecJson;
				}
				else
					itemSpecJson = productList[i].spec;
				for (var j = 0; j < itemSpecJson.length; j++) {
					for (var specName in itemSpecJson[j]) {
						if (addedName.indexOf(specName) == -1) {
							var specFormat = {
								name: specName,
								detail: []
							};
							specTypeList.push(specFormat);
							addedName.push(specName);
						}
						if (addedDetail[specName] === undefined) addedDetail[specName] = [];
						for (var k = 0; k < itemSpecJson[j][specName].length; k++) {
							var specDetail = [];
							for (var specKey in itemSpecJson[j][specName][k]) {
								if (addedDetail[specName][specKey] === undefined) addedDetail[specName][specKey] = [];
								if (addedDetail[specName][specKey].indexOf(itemSpecJson[j][specName][k][specKey]) == -1) {
									var specDetailFormat = {
										key: specKey,
										value: itemSpecJson[j][specName][k][specKey]
									};
									for (var l = 0; l < specTypeList.length; l++) {
										if (specTypeList[l].name == specName) {
											specTypeList[l].detail.push(specDetailFormat);
											break;
										}
									}
									addedDetail[specName][specKey].push(itemSpecJson[j][specName][k][specKey]);
								}
							}
						}
					}
				}
			}
			return specTypeList;
		}
	}
});

motorolaModule.factory('Products', function ($http) {
	/*
	var rootId = 999;
	var categoryProductId = 1001;
	*/
	
	var rootId = 1001;
	var categoryProductId = 1231;
	
	var data = [];
	return {
		promise: {},
		rootid: function () {
				return rootId;
		},
		mainproductid: function () {
				return categoryProductId;
		},
		all: function () {
				return data;
		},
		setData: function (productList) {
				data = productList;
		},
		convertJSONtoTree: function (arry) {
			var rootNode = [
					{
							"title" : "Root",
							"categoryId" : rootId,
							"parentId" : ""
					}
			]
			var fullArray = rootNode.concat(arry);
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
					var item = fullArray[i],
							p = item.parentId,
							target = !p ? roots : (children[p] || (children[p] = []));

					target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
					if (children[parent.value.categoryId]) {
							parent.children = children[parent.value.categoryId];
							for (var i = 0, len = parent.children.length; i < len; ++i) {
									findChildren(parent.children[i]);
							}
					}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
					findChildren(roots[i]);
			}
			return roots[0].children;
		}
	}
}).directive('eatClick', function() {
        return function(scope, element, attrs) {
			jQuery(element).click(function(event) {
				event.preventDefault();
			});
        }
});

motorolaModule.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Avaya Showcase" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			
			this.downloadFormHeader = this.downloadFormHeader == undefined ? '' : this.downloadFormHeader;
			this.downloadFormDescription = this.downloadFormDescription == undefined ? '' : this.downloadFormDescription;
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

motorolaModule.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

motorolaModule.factory('Translation', function () {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
});

// ltu 10/29/2013 added PromotionAssets factory
motorolaModule.factory('PromotionAssets', function ($filter) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		get: function(dataList, id) {
			//var result = dataList.filter(function(e) {return e.categoryId==id;});
			var result = $filter('filter')(dataList, {categoryId: id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			//console.log(result);
			return result;
		}
	}
});


motorolaModule.run(function ($rootScope, $q, $http, $routeParams, $location, $route, $timeout, Products, DataProducts, LeadgenForm, URLHandler, Translation,PromotionAssets) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
		var temp = currentUrl.split('/');
		for (var i = 0; i < temp.length; i++) {
			if (temp[i] == 'i') {
				currentTick = '&i=' + temp[i + 1];
				break;
			}
		}
	}
	
	var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
	
	$rootScope.AllProducts = [];
	$rootScope.AllTabsAndNavigation = [];
	$rootScope.Dictionary = [];
	$rootScope.rootProductId = Products.mainproductid();
	$rootScope.CtrlMessage = '';
	$rootScope.PromotionAssets = [];
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.LeafCategories = [];
	$rootScope.ContactUsItem = [];
	$rootScope.ProductDeferLoaded = false;
	
	$rootScope.msieversion = function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        // If another browser, return 0
        return 0;
	}
	
	$rootScope.baseDomain = $rootScope.msieversion() > 0 && $rootScope.msieversion() < 9 ? $rootScope.baseDomain : '';	
	
	$rootScope.GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	}
	
	$rootScope.ParseObject = function (rawList, fieldSplit, ctId, fieldList, fieldDic) {
		var objectList = "[";
		
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var objectItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					property = property.trim();
					if (fieldList.indexOf(property) >= 0) {
						if (objectItem != '{') objectItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						objectItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			objectItem += "}";
			if (objectList != '[') objectList += ',';
			objectList += objectItem;
		}
		objectList += ']';
		return JSON.parse(objectList);
	}
	
	$rootScope.parseProductObj = function (rawData) {
		var ctId = '31000';
		var fieldSplit = 0;
		var fieldList = ['CSP_Unique_Key', 'Content_Title', 'Content_Title_Secondary', 'File_Image_Thumbnail_Url', 'File_Image_Url', 'Content_Description_Short', 'Content_Description_Long', 'Local_Type' , 'Local_Syndication_Type', 'Status_Sort_Order', 'Status_Promotion_Y_N', 'CSP_Category_ID', 'ContentCategoryParentID', 'ContentCategoryLineage', 'ContentCategoryDepth' , 'File_Presentation_1_Name', 'CSP_Reporting_Id', 'CSP_Level','Content_Description_Extended', 'Content_Information_Detail', 'Local_Target_Group', 'File_Video_Url'];
		var fieldDic = {
			"CSP_Unique_Key"			: "cspuniquekey",		
			"Content_Title"				: "title",	
			"Content_Title_Secondary"	: "secondarytitle",				
			"File_Image_Thumbnail_Url"	: "thumbnail",				
			"File_Image_Url"			: "image",
			"File_Video_Url"			: "filevideourl",
			"Content_Description_Short"	: "description",				
			"Content_Description_Long"	: "descriptionlong",				
			"Local_Type"				: "type",	
			"Local_Syndication_Type"	: "syndicationtype",				
			"Status_Sort_Order"			: "displayorder",		
			"Status_Promotion_Y_N"		: "feature",			
			"CSP_Category_ID"			: "categoryId",		
			"ContentCategoryParentID"	: "parentId",				
			"ContentCategoryLineage"	: "lineage",				
			"ContentCategoryDepth"		: "depth",			
			"File_Presentation_1_Name"	: "textorientation",				
			"CSP_Reporting_Id"			: "reportingId",		
			"CSP_Level"					: "cspLevel",
			"Content_Information_Detail": "Content_Information_Detail",
			"Content_Description_Extended" : "Content_Description_Extended",
			"Local_Target_Group"			: "availablecountry"
		}
		fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawData);
		return $rootScope.ParseObject(rawData, fieldSplit, ctId, fieldList, fieldDic);
	};
	
	$rootScope.initCspReportAndDeeplink = function () {
		if (typeof(CspReportLib) == 'undefined') {
			jQuery('#csp-report-lib').remove();
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.id = "csp-report-lib";
			script.src = "js/CspReportLib.js";
			// clear CspReportLib 
			if (typeof (CspReportLib) != "undefined") {
				CspReportLib.wt.DCSext["ConversionContent"] = null;
				CspReportLib.wt.DCSext["ConversionShown"] = null;
				CspReportLib.wt.DCSext["ConversionClick"] = null;
				CspReportLib.wt.DCSext["ConversionType"] = null;
				CspReportLib.wt.DCSext["csp_vname"] = null;
				CspReportLib.wt.DCSext["csp_vaction"] = null;
			}
			
			document.getElementsByTagName('head')[0].appendChild(script);
		}
		jQuery(document).attr('title', $rootScope.pageTitle);
		try{parent.postMessage(window.location.href,"*");} catch(ex){console.log(ex);};
	};
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ProductCtrl':
					$rootScope.pageTitle = 'Product';
					break;
				case 'ProductDetailCtrl':
					$rootScope.pageTitle = 'Product Detail';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:
					$rootScope.pageTitle = 'Motorola Solutions';
			}
		}		
		try {
			jQuery('#csp-report-lib').remove();
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.id = "csp-report-lib";
			script.src = "js/CspReportLib.js";
			// clear CspReportLib 
			if (typeof (CspReportLib) != "undefined") {
				CspReportLib.wt.DCSext["ConversionContent"] = null;
				CspReportLib.wt.DCSext["ConversionShown"] = null;
				CspReportLib.wt.DCSext["ConversionClick"] = null;
				CspReportLib.wt.DCSext["ConversionType"] = null;
				CspReportLib.wt.DCSext["csp_vname"] = null;
				CspReportLib.wt.DCSext["csp_vaction"] = null;
			}
			jQuery(document).attr('title', $rootScope.pageTitle);
			document.getElementsByTagName('head')[0].appendChild(script);
			window.setTimeout(function() {
				jQuery(document).click();
			}, 1000);
			try{parent.postMessage(window.location.href,"*");} catch(ex){console.log(ex);};
		} catch (Err) {};
	};
	
	$rootScope.GetCurrentLangCode = function () {
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var crrLangCode = '';
		
		if (baseDomain.indexOf('-') > 0) {
			var subBaseDomain = baseDomain.split('-');
			var langParamPatt = new RegExp(/^l\w{2}$/);
			var langParam = '';
			for (var i = 0; i < subBaseDomain.length; i++) {
				if (langParamPatt.test(subBaseDomain[i]))
					langParam = subBaseDomain[i];
			}
			if (langParam != '')
				crrLangCode = langParam.substring(1);
		}
		if (crrLangCode == '')
			crrLangCode = cspConsumerInfo.lng.toLowerCase();
		return crrLangCode;
	};
	
	$rootScope.currLangCode = $rootScope.GetCurrentLangCode();
	$rootScope.isRTLLang = LANG_RTL.indexOf($rootScope.currLangCode) > -1;
	$rootScope.langDir = $rootScope.isRTLLang ? 'rtl' : 'auto';
	
	var GetLeafNodes = function (treeNode, maxLevel) {
		if (treeNode.children == undefined) return;
		if (treeNode.value.depth == (maxLevel - 1)) {
			$rootScope.LeafCategories = $rootScope.LeafCategories.concat(treeNode.children);
			return;
		}
		for (var i = 0; i < treeNode.children.length; i++) {
			GetLeafNodes(treeNode.children[i], maxLevel);
		}
	}
	
	$rootScope.BuildTreeAndGetLeaves = function (rawData) {
		var productTree = Products.convertJSONtoTree(rawData);
		for (var i = 0; i < productTree.length; i++)
			GetLeafNodes(productTree[i], MAIN_MENU_MAX_LEVEL);
	}
	
	$rootScope.CallProductDefer = function () {
		$rootScope.ProductDeferLoaded = true;
		$http.get('d1.aspx?p2003(ct31000&cd' + $rootScope.rootProductId + 'i2)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			$rootScope.deferredProducts.resolve(responseData);
		});
	};
	
	if (true) {
		$rootScope.deferredProducts = $q.defer();
		Products.promise = $rootScope.deferredProducts.promise.then(function (productList) {
			Products.setData(productList);
			return productList;
		});
		
		// $timeout(function() {
			// $http.get('d1.aspx?p2003(ct31000&cd' + $rootScope.rootProductId + 'i2)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
				// $rootScope.deferredProducts.resolve(responseData);
			// });
		// }, 2000);
		
		// ltu 10/29/2013: added promotion assets
		/*
		var deferredPromotionAssets = $q.defer();
		PromotionAssets.promise = deferredPromotionAssets.promise.then(function(dataList){
			PromotionAssets.setData(dataList);
			return dataList;
		});		
		$http.get('d1.aspx?p2003(ct21000&cd' + $rootScope.rootProductId + 'i99&fStatus_Promotion_Y_N~1=yes!)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {						
			deferredPromotionAssets.resolve(responseData);
		});
		*/
		//end of promotion assets
		
		var translationDeferred = $q.defer();
		Translation.promise = translationDeferred.promise.then(function (dataList) {
			Translation.setData(dataList);
			return dataList;
		});
		
		$http.get('d1.aspx?p2005(ct35000)[]' + currentTick).success(function(responseData) {
			translationDeferred.resolve(responseData);
		});
		
		var leadgenFormDeferred = $q.defer();
		LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
			if (data.length <= 1) return null;
			LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
			LeadgenForm.setFields(data[1].contactusformfields);
			return data;
		});
		
		$http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			leadgenFormDeferred.resolve(responseData);
		});
	}
});