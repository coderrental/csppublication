var contentOriginalHeight;

function initApp() {
	AddExceptionMenuItems();
	categories.Render("#csp-nav");
	
	ExpandContentPane();
	
	initMenu();
}

function ExpandContentPane() {
	$('#csp-side-col').remove();
	$('#csp-right-col').css({
		padding: '0px',
		width: '100%'
	});
	
	$('#page-banner').css({
		width: '658px'
	});
	if ($('#page-banner').find('img').length > 0) {
		$('#page-banner').css({
			width: '660px'
		});
		$('#page-banner').find('img').attr('width', '660px');
	}
}

function vAlignNavText() {
    $('.root-item-link').each(function () {
        var thisHeight = $(this).height();
        var wrapperHeight = $(this).closest('li').height();
        var topPos = (wrapperHeight - thisHeight) / 2;
    	var newHeight = wrapperHeight - topPos;
        $(this).css({'padding-top': (topPos) + 'px', 'height': (newHeight) + 'px' });
        $(this).css('display','block');
    });
}

function hAlignNavItem() {
    //var navWidth = 510;
	var navWidth = $('#csp-nav').width();
    var menuItemCount = $('li.root-item').length;
	$('#main-nav').addClass('main-nav-' + menuItemCount);
    //var itemWidth = (navWidth / menuItemCount) - 1;
    //var crrStyle = $('li.root-item').attr('style');
    //$('li.root-item').css('width', itemWidth + 'px');
   // $('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
}


function initMenu() {
	$('#main-nav li.root-item:last').addClass('selected');
    menuEffect();
	
    hAlignNavItem();
    
    var subMenuItemCount;
    var widthPercent;
    $('ul.sub-menu').each(function () {
        subMenuItemCount = $(this).find('li.sub-item').length;
        /*widthPercent = (100 / subMenuItemCount) - 3;
        $(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '%; padding-right: 3%');*/
        $(this).find('li.sub-item').attr('style', 'width:30.33%; padding-right: 3%');
    });

    vAlignNavText();
	AddMobileTrigger();
}

function menuEffect() {
	if (window.location.href.indexOf('&home=true') < 0 && window.location.href.indexOf('&cat=') > 0)
		$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		if ($(window).width() <= 480) return;
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		if ($(window).width() <= 480) return;
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
	
	if (isLessThanIE8 == true)
		$('#main-nav').css('background-color', '#4b4b4b');
	//4b4b4b
}

function AddMobileTrigger() {
	$('li.root-item').each(function() {
		if ($(this).find('div.sub-menu-wrapper').length == 0) return;
		var rootMenuEl= $(this);		
		
		if (isLessThanIE8 == true) {
			$(this).html($(this).html() + '<a class="mm-trigger" src="javascript: void(0)"></a>');
			$('.mm-trigger').click(function() {
				$(this).parent('li.root-item').find('div.sub-menu-wrapper').toggle();
			});
		}
		else {
			var mTrigger = $('<a class="mm-trigger" src="javascript: void(0)">');
			mTrigger.click(function() {
				rootMenuEl.find('div.sub-menu-wrapper').toggle();
			});
			$(this).append(mTrigger);
		}
	});
	
	// handle the csp-mobile-button
	$("div#csp-mobile-button").click(function(ev){
		ev.preventDefault();
		$("div#csp-nav").toggle(0, function(){
			var t = $(this);
			t.attr("state",t.css("display"))
		});
	});
	$(window).on('resize', function() {
		var w = $(window)
		var t = $("div#csp-nav");
		if (w.width()>480 && $("div#csp-nav").css("display")=="none") {
			t.show();
			t.attr("z", "true");
		}
		else if (w.width()<=480 && t.attr("z")=="true") {			
			t.css("display", t.attr("state"));
			t.attr("z","false");
		}
	});
}

function AddExceptionMenuItems() {
	var firstItems = categories.FindByAttr('level', 1);
	if (firstItems.length > 2) {
		firstItems.sort(function(a,b){
			var r = 0;
			if (a.displayOrder>b.displayOrder) r = 1; 
			else if (a.displayOrder<b.displayOrder) r = -1;
			return r;
		});
		var homeUrl = firstItems[1].href + '&home=true';
		var contactUrl = 'd1.aspx?p3():c2(ct31000&cd1i99)[st(ct31000*Status_Sort_Order*)]:c4(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
		firstItems[0].href = homeUrl;
		firstItems[firstItems.length - 1].href = contactUrl;
		$("#csp-logo img").wrap('<a href="'+homeUrl+'"></a>');
	}
}

function RenderCustomBreadcrumb(elId, catId, level) {
	var el = $(elId);
	var categoryEl = categories.Find(catId);
	var theLineage = GetCategoryLineage(catId);
	var html = '';
	var theUrl;
	for (var i = 0; i < theLineage.length; i++) {
		if (theLineage[i] == '') continue;
		if (theLineage[i] == rootCatId) {
			categoryEl = categories.Find(catId);
			theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href + '&home=true' : "javascript:void(0);");
			html += '<a href="' + theUrl + '">Home</a>  &raquo;  ';
			continue;
		}
		else if (theLineage[i] == catId) {
			categoryEl = categories.Find(catId);
			html += categoryEl.text;
			continue;
		}
		categoryEl = categories.Find(parseInt(theLineage[i]));
		if (categoryEl == null) continue;
		theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href : "javascript:void(0);");
		html += '<a ccid="' + categoryEl.cnid + '" catid="' + categoryEl.id + '" href="' + theUrl + '">' + categoryEl.text + '</a>  &raquo;  ';
	}
	if (html != "")
		el.html(html);
};

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	if (categoryItem == null)
		return lineageArr;
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}