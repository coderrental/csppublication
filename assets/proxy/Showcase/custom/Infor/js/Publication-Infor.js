var contentOriginalHeight;
var hasBannerText = false;

function initApp() {
	AddExceptionMenuItems();
	categories.Render("#csp-nav");
	
	if (window.location.href.indexOf('home=true') < 0)
		RenderCustomBreadcrumb ("#csp-breadcrumb", crrCatId, (categoryLevel));
	ResetContentImages();
	if (categoryLevel > 1) {
		RenderSideItems();
	}
	else {
		RenderChildrenBlocks ();
		ExpandContentPane();
	}
	
	ContentPageLinksDetect();
	//window.setTimeout("initMenu()", 1500);
	initMenu();
	if (window.location.href.indexOf('home=true') >= 0 || window.location.href.indexOf('cat=') < 0) {
		$('.root-item').removeClass('selected');
		$('.root-item').first().addClass('selected');
	}
	initLightBox();
	ResizeColumn();
	$(window).resize(function(){ ResizeColumn();});
}

function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);		
}

function ResizeColumn() {
	if ($(window).width() > 480) {
		$('#side-col-list').show();
		$('.mm-trigger').hide();
	}
	else {
		if (isLessThanIE8 == false)
			$('#side-col-list').hide();
		$('.mm-trigger').show();
	}
	
	if ($(window).width() > 460) {
		$('.mm-trigger').hide();
	}
	else {
		$('.mm-trigger').show();
	}
}

function ResetContentImages() {
	var sectionWidth = $('#csp-right-col').width();
	$('#csp-right-col').find('img').each(function() {
		if ($(this).parents('#page-banner').length > 0)
			return;
		var newImg = new Image();
		var height = 0;
		var width = 0;
		var crrImgEl = $(this);
		
		newImg.onload = function() {
			height = newImg.height;
			width = newImg.width;
			if (width >= sectionWidth) {
				crrImgEl.attr('width', (sectionWidth - 30) + 'px');
			}
		}

		newImg.src = $(this).attr('src'); // this must be done AFTER setting onload
	});
}

function RenderSideItems() {
	// at any given category, 
	// first find children of the current category, if found them shows children	
	var secondLevelItems = categories.FindByAttr('pid', crrCatId);	
	var itself = categories.FindByAttr('id', crrCatId);	
	if (secondLevelItems.length > 0) {
		var newSideUL = $('<ul>');
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		$('#csp-side-col').append(newSideUL);
		RenderDetailSideList(newSideUL, 'aside-menu-item', secondLevelItems);
		$('#page-banner').find('img').attr('width', $('#csp-right-col').width() + 'px');
		var featureSection = $('#csp-right-col').find('.feature:first');
		featureSection.find('.figure').attr('style', 'width: 245px');
		featureSection.find('.figure img').attr('width', '245px');
		SetContentTableWidth (200);
		
		$('#mobile-left-col-trigger').unbind().click(function() {
			$('#side-col-list').toggle();
		});
	}
	// secondly if current category doesnt have any children, this is the last level, and we need to show siblings
	else if (itself.length > 0) {
		var newSideUL = $('<ul>');
		newSideUL.attr({
			id: 'side-col-list',
			'class': 'csp-sibling-items'
		});
		$('#csp-side-col').append(newSideUL);
		var siblingsItems = categories.FindByAttr('pid', itself[0].pid);
		RenderDetailSideList(newSideUL, 'aside-menu-item', siblingsItems);
		$('#page-banner').find('img').attr('width', $('#csp-right-col').width() + 'px');
		var featureSection = $('#csp-right-col').find('.feature:first');
		featureSection.find('.figure').attr('style', 'width: 245px');
		featureSection.find('.figure img').attr('width', '245px');
		SetContentTableWidth (200);
		
		$('#mobile-left-col-trigger').unbind().click(function() {
			$('#side-col-list').toggle();
		});
	}
	else {
		ExpandContentPane();
		$('#mobile-left-col-trigger').hide();
	}
}

function RenderChildrenBlocks () {
	var childrenList = categories.FindByAttr('pid', crrCatId);
	if (childrenList.length <= 0)
		return;
	$('#sub-category-blocks').remove();	
	var contentWidth = $('#csp-right-col').width();
	var blockWidth;

	if (childrenList.length <= 4) {
		blockWidth = 100 / childrenList.length;
		if (navigator.userAgent.match(/MSIE/i))
			blockWidth = blockWidth - 1;
	}
	else {
		blockWidth = 100 / 4;
	}

	var blocksWrapper = $('<div id="sub-category-blocks">');
	
	for (var i = 0; i < childrenList.length; i++) {
		var blockDetail = '<div class="sub-category-block" id="block-' + childrenList[i].id + '">';
		blockDetail += '<a class="sub-category-block-figure" cspenglishvalue="' + childrenList[i].text + '" csptype="LINKS" cspobj="REPORT" href="' + childrenList[i].href + '"><img src="' + childrenList[i].thumbnail + '"></a>';
		blockDetail += '<h2><a cspenglishvalue="' + childrenList[i].text + '" csptype="LINKS" cspobj="REPORT" href="' + childrenList[i].href + '">' + childrenList[i].text + '</a></h2>';
		blockDetail += '<p>' + childrenList[i].desc + '</p></div>';
		blocksWrapper.append(blockDetail);
	}
	blocksWrapper.append('<div class="clear"></div>');
	$('#csp-right-col').append(blocksWrapper);
	$('.sub-category-block').css('width', blockWidth + '%');
}

function ExpandContentPane() {
	$('#csp-side-col').remove();
	$('#csp-right-col').css({
		padding: '0px',
		width: '100%'
	});
	
	$('#page-banner').css({
		width: '658px'
	});
	if ($('#page-banner').find('img').length > 0) {
		$('#page-banner').css({
			width: '660px'
		});
		$('#page-banner').find('img').attr('width', '660px');
	}
}

function SetContentTableWidth (width) {
	$('.layoutColumn').find('table').each(function() {
		var styleValue = $(this).attr('style');
		if (styleValue.indexOf('float') < 0)
			return;
		$(this).width(width);
		$(this).find('img').width(width);
		$(this).find('td').attr('style', '');
	});
}

function RenderDetailSideList(ulParent, liClassName, categoryList) {
	for (var i = 0; i < categoryList.length; i++) {
		var newSiblingLI = $('<li>');
		newSiblingLI.attr({
			id: 'c-' + categoryList[i].id
		});
		if (categoryList[i].id == crrCatId)
			newSiblingLI.addClass('selected');
		var newSiblingA = $('<a>');
		newSiblingA.html(categoryList[i].text);
		newSiblingA.attr({
			'class': liClassName,
			cateid: categoryList[i].id,
			rel: 'CategoryTitleEditor-' + categoryList[i].id,
			href: categoryList[i].href,
			ccid: categoryList[i].cnid
		});
		var newSiblingInput = $('<input type="text">');
		newSiblingInput.val(categoryList[i].text);
		newSiblingInput.attr({
			'class': 'hide',
			id: 'CategoryTitleEditor-' + categoryList[i].id
		});
		newSiblingLI.append(newSiblingA);
		newSiblingLI.append(newSiblingInput);
		ulParent.append(newSiblingLI);
	}
}

function vAlignNavText() {
    $('.root-item-link').each(function () {
        var thisHeight = $(this).height();
        var wrapperHeight = $(this).closest('li').height();
        var topPos = (wrapperHeight - thisHeight) / 2;
    	var newHeight = wrapperHeight - topPos;
        $(this).css({'padding-top': (topPos) + 'px', 'height': (newHeight) + 'px' });
        $(this).css('display','block');
    });
}

function hAlignNavItem() {
    //var navWidth = 510;
	var navWidth = $('#csp-nav').width();
    var menuItemCount = $('li.root-item').length;
	$('#main-nav').addClass('main-nav-' + menuItemCount);
    //var itemWidth = (navWidth / menuItemCount) - 1;
    //var crrStyle = $('li.root-item').attr('style');
    //$('li.root-item').css('width', itemWidth + 'px');
   // $('a.root-item-link').attr('style', 'width: ' + parseInt(itemWidth - 10) + 'px !important');
}

function initMenu() {
    menuEffect();
	
    hAlignNavItem();
    
    var subMenuItemCount;
    var widthPercent;
    $('ul.sub-menu').each(function () {
        subMenuItemCount = $(this).find('li.sub-item').length;
        /*widthPercent = (100 / subMenuItemCount) - 3;
        $(this).find('li.sub-item').attr('style', 'width: ' + widthPercent + '%; padding-right: 3%');*/
       $(this).find('li.sub-item').attr('style', 'width:29.33%; padding-right: 3%');
    });

    vAlignNavText();
	
	AddMobileTrigger();
}

function menuEffect() {
	if (window.location.href.indexOf('&home=true') < 0 && window.location.href.indexOf('&cat=') > 0)
		$('a[cateid="' + crrCatId + '"]').parents('li.root-item').addClass('selected');
	$('li.root-item').unbind('hover').hover(function () {
		if ($(window).width() <= 480) return;
		$(this).addClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').show();
		if ($('#csp-content').height() < $(this).find('div.sub-menu-wrapper').height())
			$('#csp-content').height($(this).find('div.sub-menu-wrapper').height());
	}, function () {
		if ($(window).width() <= 480) return;
		$(this).removeClass('root-item-hover');
		$(this).find('div.sub-menu-wrapper').hide();
	});
	
	if (isLessThanIE8 == true)
		$('#main-nav').css('background-color', '#f1f1f1');
	
	// We need to hide breadcrumb on Products page and cat=2 for products 
	//console.log(crrCatId);
	if (window.location.href.indexOf('&home=true') < 0 && (crrCatId==2)) 
	$('#csp-breadcrumb').empty();
}

function AddMobileTrigger() {
	$('li.root-item').each(function() {
		if ($(this).find('div.sub-menu-wrapper').length == 0) return;
		var rootMenuEl= $(this);
		var mTrigger = $('<a class="mm-trigger" src="javascript: void(0)">');
		if (isLessThanIE8 == true) {
			$(this).html($(this).html() + '<a class="mm-trigger" src="javascript: void(0)"></a>');
			$('.mm-trigger').click(function() {
				$(this).parent('li.root-item').find('div.sub-menu-wrapper').toggle();
			});
		}
		else {
			$(this).append(mTrigger);			
			mTrigger.click(function() {
				rootMenuEl.find('div.sub-menu-wrapper').toggle();
			});
		}
	});

	// handle the csp-mobile-button
	$("div#csp-mobile-button").click(function(ev){
		ev.preventDefault();
		$("div#csp-nav").toggle(0, function(){
			var t = $(this);
			t.attr("state",t.css("display"))
		});
	});
	$(window).on('resize', function() {
		var w = $(window)
		var t = $("div#csp-nav");
		if (w.width()>480 && $("div#csp-nav").css("display")=="none") {
			t.show();
			t.attr("z", "true");
		}
		else if (w.width()<=480 && t.attr("z")=="true") {			
			t.css("display", t.attr("state"));
			t.attr("z","false");
		}
	});	
}

function AddExceptionMenuItems() {
	var firstItems = categories.FindByAttr('level', 1);	
	if (firstItems.length > 2) {
		firstItems.sort(function(a,b){
			var r = 0;
			if (a.displayOrder>b.displayOrder) r = 1; 
			else if (a.displayOrder<b.displayOrder) r = -1;
			return r;
		});
		var homeUrl = firstItems[1].href + '&home=true';
		var contactUrl = 'd1.aspx?p3():c2(ct31000&cd1i99)[st(ct31000*Status_Sort_Order*)]:c4(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
		firstItems[0].href = homeUrl;
		firstItems[firstItems.length - 1].href = contactUrl;
		$("#csp-logo img").wrap('<a href="'+homeUrl+'"></a>');
	}
	// var newUl;
	// if ($('#csp-nav').find('#main-nav').length == 0) {
		// newUl = $('<ul id="main-nav" class="the-main-menu">');
		// $('#csp-nav').append(newUl);
	// }
	// else
		// newUl = $('#csp-nav').find('#main-nav');
	// var firstCategories = categories.FindByAttr('level', 1);
	// var homeUrl = firstCategories[0].href + '&home=true';
	// newUl.prepend($('<li class="root-item" id="home-item">').html('<a class="root-item-link exception-item" href="' + homeUrl + '">Home</a>'));
	// var contactUrl = 'javascript: void(0)';
	// var arr = firstCategories[0].href.split("/");
	// contactUrl = 'd1.aspx?p3():c2(ct31000&cd1i99)[st(ct31000*Status_Sort_Order*)]:c4(ct15000)[st(ct15000*Status_Sort_Order*)][]&ticks=' + Math.round((new Date()).getTime() / 1000);
	// newUl.append($('<li class="root-item exception-item">').html('<a class="root-item-link exception-item" href="' + contactUrl + '">Contact</a>'));
}

function RenderCustomBreadcrumb(elId, catId, level) {
	var el = $(elId);
	var categoryEl = categories.Find(catId);
	var theLineage = GetCategoryLineage(catId);
	var html = '';
	var theUrl;
	for (var i = 0; i < theLineage.length; i++) {
		if (theLineage[i] == '') continue;
		if (theLineage[i] == rootCatId) {
			categoryEl = categories.Find(theLineage[i + 1]);
			if(categoryEl == null)
				theUrl="javascript:void(0);";
			else	
				theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href + '&home=true' : "javascript:void(0);");
			html += '<a href="' + theUrl + '">Home</a>  &gt;  ';
			continue;
		}
		else if (theLineage[i] == catId) {
			categoryEl = categories.Find(catId);
			if (categoryEl != null)
				html += categoryEl.text;
			continue;
		}
		categoryEl = categories.Find(parseInt(theLineage[i]));
		if (categoryEl == null) continue;
		theUrl = (typeof(categoryEl.href) != "undefined" ? categoryEl.href : "javascript:void(0);");
		// 2 is the category id for Products which we need to hide
		if(i != 1) 
		html += '<a ccid="' + categoryEl.cnid + '" catid="' + categoryEl.id + '" href="' + theUrl + '">' + categoryEl.text + '</a>  &gt;  ';
	}
	if (html != "")
		el.html(html);
};

function ContentPageLinksDetect() {
	$('#csp-right-col').find('a.content-page-links').each(function() {
		var idMixed = $(this).attr('id').split('-');
		var linkCategoryId = idMixed.pop();
		var crrCategoryNode = categories.Find(linkCategoryId);
		if (crrCategoryNode == null) {
			$(this).remove();
			return;
		}
		$(this).attr('href', crrCategoryNode.href);
	});
}

function GetCategoryLineage(catId) {
	var categoryItem = categories.Find(catId);
	var lineageArr = [catId];
	if (categoryItem == null)
		return lineageArr;
	p = categories.Find(categoryItem.pid);
	while (p != null) {
		lineageArr.unshift(p.id);
		p = categories.Find(p.pid);
	}
	lineageArr.unshift(rootCatId);
	return lineageArr;
}

function initLightBox () {
	$("[cspobj]").each(function(id, el){
		el = $(el);
		if (el.attr("cspobj").toLowerCase()!="report") return;
		switch(el.attr("csptype").toLowerCase()) {
			case "pdf":
				if(el.attr("isgated")&&el.attr("isgated").toLowerCase()=="true"){
					$(el).fancybox({
						type: "iframe",
						href: "/d1.aspx?p8(ct15000)[st(ct15000*Status_Sort_Order*)]&ticks="+(new Date()).getTime()+"&file="+encodeURIComponent(el.attr("href")),
						width: "450px",
						iframe: {preload: false}
					});
				}
				else {
					if (isMobile()) {
						el.click(function(ev){ev.preventDefault();window.open(el.attr("href"));})					
					}
					else {						
						$(el).fancybox({
							type: "iframe",
							iframe: {
								preload: false
							}		
						});
					}
				}
				break;
			case "video":
				if (isMobile()) {
					el.click(function(ev){ev.preventDefault();window.open(el.attr("video-url"),"_blank");})		
					//el.click(function(ev){ev.preventDefault();window.open("http://global.syndication.tiekinetix.net/ui/universalresources/cspvideoplayer/cspvideoplayer.html?video="+ el.attr("video-url"),"_blank");})				
				}
				else {
					$(el).fancybox({
						type: "iframe"
						, beforeLoad: function() {
							var el = $(this.group[0].element);
							var reportingOptions = '{"project":"' + CspReportLib.wt.DCSext.vendorName + '",' + 
							'"url":"' + CspReportLib.wt.DCS.dcsref + '",' +
							'"src":"",' +
							'"extra":"",' +
						    '"CSP_ShowHover":0,' +
						    '"protocol":"http",' +
						    '"SyndicationType":"showcase",' +
						    '"ElementType":"video",' +
						    '"ElementAction":"lightbox",' +
						    '"GlobalIDNumber":"",' +						    
						    '"cid":' + CspReportLib.wt.DCSext.csp_companyId + ',' +
						    '"cn":"' + CspReportLib.wt.DCSext.csp_companyname + '",' +
						    '"IntegratedType":"",' +
						    '"country":"' + CspReportLib.wt.DCSext.csp_country + '",' +
						    '"banner":"",' +
						    '"useConversionClick":true,' +
						    '"sId":' + CspReportLib.wt.DCSext.csp_vendorid + ',' +
						    '"sName":"' + CspReportLib.wt.DCSext.vendorName + '",' +
						    '"lngDesc":"' + CspReportLib.wt.DCSext.csp_language_descr + '"}';
							
							this.href = "http://entrypoints-production.herokuapp.com/videoplayer/?video=" + el.attr("video-url") + 
								"&width=535&autostart=true&dcsid=" + CspReportLib.wt.dcsid + 
								"&options=" + encodeURIComponent(reportingOptions);
							//this.height = 450;
							this.height = 400;
							this.width = 570;
						}
						, iframe: {
							preload: true
						}		
					});
				}
				break;
			default:
				break;
		}
	});
}