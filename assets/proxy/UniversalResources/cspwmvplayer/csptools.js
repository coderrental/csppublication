function cspGetQuerystring(key, default_) {
    if (default_ == null)
        default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function cspInit() {
    var vid = window.unescape(cspGetQuerystring('video'));
    var preview = window.unescape(cspGetQuerystring('preview'));
    var h = cspGetQuerystring('height');
    if (!h) {
        h = 360;
    }
    var w = cspGetQuerystring('width');
    if (!w) {
        w = 560;
    }
    var cnt = document.getElementById("mediaspace");
    var src = 'wmvplayer.xaml';
    var cfg = {
        file : vid,
        image : preview,
        height : h,
        width : w,
        autostart : "true",
        fallback: true,
    };
    var ply = new jeroenwijering.Player(cnt, src, cfg);
}
