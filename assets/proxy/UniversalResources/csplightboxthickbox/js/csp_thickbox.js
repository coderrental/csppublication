/*
* Thickbox 3.1 - One Box To Rule Them All.
* By Cody Lindley (http://www.codylindley.com)
* Copyright (c) 2007 cody lindley
* Licensed under the MIT License: http://www.opensource.org/licenses/mit-license.php
*/
(function($) {

    $.fn.thickbox = function(options) {
        return $(this)
			.css('cursor', 'pointer')
			.unbind('click.tb')
			.bind('click.tb', function(e) {
			    tb_show(options);
			    this.blur();
			    return false;
			});    };

    //lauches a thickbox on call
    $.thickbox_launch = function(options) {
        tb_show(options);
    };

    var tb_show = function(options) {

        try {
            //prepare
            if (typeof document.body.style.maxHeight === "undefined") {//if IE 6
                $("body", "html").css({ height: "100%", width: "100%" });
                $("html").css("overflow", "hidden");
                if (document.getElementById("tk_csp_TB_HideSelect") === null) {//iframe to hide select elements in ie6
                    $("body").append("<iframe id='tk_csp_TB_HideSelect'></iframe><div id='tk_csp_TB_overlay'></div><div id='tk_csp_TB_window'></div>");
                    $("#tk_csp_TB_overlay").click(tb_remove);
                }
            } else {//all others
                if (document.getElementById("tk_csp_TB_overlay") === null) {
                    $("body").append("<div id='tk_csp_TB_overlay'></div><div id='tk_csp_TB_window'></div>");
                    $("#tk_csp_TB_overlay").click(tb_remove);
                }
            }
            if (tb_detectMacXFF()) {
                $("#tk_csp_TB_overlay").addClass("tk_csp_TB_overlayMacFFBGHack"); //use png overlay so hide flash
            } else {
                $("#tk_csp_TB_overlay").addClass("tk_csp_TB_overlayBG"); //use background and opacity
            }

            $("body").append("<div id='tk_csp_TB_load'></div>"); //add loader to the page
            $('#tk_csp_TB_load').show(); //show loader

            var TB_WIDTH = (options.width * 1) + 30 || 630; //defaults to 630 if no paramaters were added to URL
            var TB_HEIGHT = (options.height * 1) + 40 || 440; //defaults to 440 if no paramaters were added to URL

            //height limiting code
            var MAX_HEIGHT = null;

            if (window.innerHeight)
                MAX_HEIGHT = window.innerHeight;
            else if (document.documentElement.clientHeight == 0)
                MAX_HEIGHT = document.body.clientHeight;
            else
                MAX_HEIGHT = document.documentElement.clientHeight;

            if (MAX_HEIGHT != null && TB_HEIGHT > MAX_HEIGHT) {
                TB_HEIGHT = MAX_HEIGHT;
            }
            //end limiting code

            //iframe no modal						
            var frame = $("<iframe frameborder='0' hspace='0' src='" + options.url + "' id='tk_csp_TB_iframeContent' name='tk_csp_TB_iframeContent" + options.InstanceId + "' style='width:" + TB_WIDTH + "px;height:" + TB_HEIGHT + "px;' > </iframe>");
            frame.bind("load", tb_showIframe);
            //set static table width = frame width + td.content margins + td.w width + td.e width
            var heightFix = 0, pngFix = "";
            if ($.browser.msie) {
                heightFix = 35;
                pngFix = "class='tk_csp_pngFix'";
            }            
            var table = $("<table border='0' cellspacing='0' width='" + (TB_WIDTH + 50) + "'>" +
							"<tr style='height:" + heightFix + "px;'><td class='tk_csp_nw' ></td><td class='tk_csp_n'></td><td class='tk_csp_ne'></td></tr>" +
							"<tr><td class='tk_csp_w' ></td><td class='content'></td><td class='tk_csp_e'></td></tr>" +
							"<tr><td class='tk_csp_sw' ></td><td class='tk_csp_s'></td><td class='tk_csp_se'></td></tr>" +
						"</table>");

            //$(".n",table).append("<img src='https://cc.syndicate.cnetcontent.com/css/images/close-gray.gif' style='float:right;margin:0;cursor:pointer' />");
            //$("td",table).bind("click",tb_remove);
            var tb_close = $("<span id='tk_csp_TB_close' " + pngFix + ">&nbsp;</span>").bind("click", tb_remove);
            $(".tk_csp_ne", table).append(tb_close);
            $("td.content", table).append(frame);
            $("#tk_csp_TB_window").append(table);

            tb_position(TB_HEIGHT, TB_WIDTH);
            if ($.browser.safari) {//safari needs help because it will not fire iframe onload
                $("#tk_csp_TB_load").remove();
                $("#tk_csp_TB_window").css({ display: "block" });
            }

            //no modal
            document.onkeyup = function(e) {
                if (e == null) { // ie
                    keycode = event.keyCode;
                } else { // mozilla
                    keycode = e.which;
                }
                if (keycode == 27) { // close
                    tb_remove();
                }
            };

        } catch (e) {
            //nothing here
            if (typeof (console) != "undefined") {
                if (typeof (console["log"]) != "undefined") console.log("Error: %o", e);
            }
        }
    };

    //helper functions below
    var tb_showIframe = function() {
        $("#tk_csp_TB_load").remove();
        $("#tk_csp_TB_window").css({ display: "block" });
    };
    var tb_remove = function() {
        $("#TB_imageOff").unbind("click");
        $("#TB_closeWindowButton").unbind("click");
        var iframe = $("#tk_csp_TB_iframeContent");
        if (iframe.length > 0) { 
            iframe.attr("src", "");
        }
        try {
            $('#tk_csp_TB_window,#tk_csp_TB_overlay,#tk_csp_TB_HideSelect').trigger("unload").unbind().remove();
        } catch (er) {
            if (typeof (console) != "undefined") {
                if (typeof (console["log"]) != "undefined") { 
                    console.log(er);
                }
            }
        } finally {
            $('#tk_csp_TB_window,#tk_csp_TB_overlay,#tk_csp_TB_HideSelect').remove();
        }
        $("#tk_csp_TB_load").remove();
        if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
            $("body", "html").css({ height: "auto", width: "auto" });
            $("html").css("overflow", "");
        }
        document.onkeydown = "";
        document.onkeyup = "";
        return false;
    };
    var tb_position = function(height, width) {
        $("#tk_csp_TB_window").css({ marginLeft: '-' + parseInt((width / 2), 10) + 'px' });
        if (!($.browser.msie && $.browser.version < 7)) { // take away IE6
            $("#tk_csp_TB_window").css({ marginTop: '-' + parseInt((height / 2), 10) + 'px' });
        }
    };
    var tb_detectMacXFF = function() {
        var userAgent = navigator.userAgent.toLowerCase();
        if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox') != -1) {
            return true;
        }
    };
})(CSP_GLOBAL.jQuery);
//End thickbox

if (typeof(CSP_GLOBAL) != "undefined"){
	(function($$){
		try{
			var scriptname = "csp_thickbox.js";
			$$.scriptMap[scriptname].ready = true;
			$$.processQueue(scriptname);
		}catch(ex){
			$$.log(ex);
		};
	})(CSP_GLOBAL);
}