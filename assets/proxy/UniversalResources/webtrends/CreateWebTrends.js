CSP_GLOBAL_ID.createWebtrends = function(options) {
	var $$ = CSP_GLOBAL_ID;
	var $ = CSP_GLOBAL_ID.$;
    var wttag = null;
    if ( typeof ($$["wt" + options.project]) != "undefined") {
        wttag = $$["wt" + options.project];
    } else {
        wttag = new CSP_GLOBAL_ID.WebTrends();
    }
    wttag.dcsGetId();
	        //wttag.DCSext.vendorName = "{server:cleanmframe}".toLowerCase(); EGO don't think we need this anymore
    wttag.DCSext.ConversionType = options.SyndicationType.toLowerCase();
    wttag.DCSext.embed_url = window.location.href;
    var elementType = options.ElementType.toLowerCase();
    if (elementType == "image")
        elementType = "banner";
    var elementAction = options.ElementAction.toLowerCase();
    var elementDesc = (elementAction == "none" ? elementType : elementType + "/" + elementAction);
    wttag.DCSext.ConversionClick = (elementAction == "none" ? elementDesc : null);
    wttag.DCSext.ConversionShown = elementDesc;

    if (options.SyndicationType == "campaign" && typeof (options.banner) != "undefined" && options.banner) {
        wttag.DCSext.ConversionContent = options.banner;
        wttag.DCSext.ConversionShown = options.banner;
    } else if (options.SyndicationType == "inline_showcase") {
        try {
            var mfrsku = $("#cspElement_" + options.GlobalIDNumber).attr("mfrsku");
            if (mfrsku) {
                wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = wttag.DCSext.ConversionClick = wttag.DCSext.sku_key = mfrsku;
            } else if (options.url.match(/mfrsku\=\d+/)[0].split("=")[1]) {
                mfrsku = options.url.match(/mfrsku\=\d+/)[0].split("=")[1];
                wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = wttag.DCSext.ConversionClick = wttag.DCSext.sku_key = mfrsku;
            }
        } catch (e) {
            $$.log(ex);
        }
    } else if (options.SyndicationType.indexOf("key_") == 0) {
        var mfrsku = options["mfrsku"];
        if (mfrsku)
            wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = mfrsku;
    } else {
        wttag.DCSext.ConversionContent = elementDesc;
        if (options["mfrsku"]) {
            wttag.DCSext.Entry_Launch = wttag.DCSext.ConversionContent;
            wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = options["mfrsku"];
            wttag.DCSext.sku_key = options["mfrsku"];
        }
    }

    if ( typeof (options.sId) != "undefined") {
        wttag.DCSext.vendorName = options.sName;
        wttag.DCSext.csp_vendorid = options.sId;
    } 
    wttag.DCSext.csp_stype = "embededscript";
    wttag.DCSext.csp_companyId = options.cid;
    wttag.DCSext.csp_pageTitle = document.title;
    if (wttag.DCSext.csp_pageTitle.length > 100) {
        wttag.DCSext.csp_pageTitle = wttag.DCSext.csp_pageTitle.substring(0, 75) + "...";
    }
    wttag.DCSext.csp_companyname = options.cn;
    //if (typeof (options.lngDesc) != "undefined") wttag.DCSext.csp_language_descr = options.lngDesc;
    if ( typeof (options.lngDesc) != "undefined")
        wttag.DCSext.language = options.lngDesc;
    if ( typeof (options.country) != "undefined")
        wttag.DCSext.csp_country = options.country;
    if ( typeof (options.campaignReportingId) != "undefined")
        wttag.DCSext.csp_sname = options.campaignReportingId;
    else
        wttag.DCSext.csp_sname = wttag.DCSext.ConversionType;

    if (elementAction != "none" && options.IntegratedType) {
        wttag.WT.si_n = options.IntegratedType;
        wttag.WT.si_x = "1";
    }

    wttag.dcsCollect();
    if ( typeof ($$["wt" + options.project]) == "undefined") {
        $$["wt" + options.project] = wttag;
    }
    //wttag.dcsMultiTrack();

    if (options.ElementAction != "none") { 
    	try {
	        $("#cspElement_" + options.GlobalIDNumber).on("click", function(ev) {
	            try {
	                if (wttag.WT.si_n) {
	                    wttag.WT.si_x = "2";
	                }
	                wttag.DCSext.ConversionShown = null;
	                wttag.DCSext.ConversionType = options.SyndicationType.toLowerCase();
	                if (options.SyndicationType == "campaign" && typeof (options.banner) != "undefined" && options.banner)
	                    wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = options.banner;
	                else {
	                    wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = elementDesc;
	                    if (options["mfrsku"]) {
	                        wttag.DCSext.Entry_Launch = elementDesc;
	                        wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = wttag.DCSext.sku_key = options["mfrsku"];
	                    }
	                }
	                wttag.dcsMultiTrack();
	            } catch (ex) {
	                $$.log(ex);
	            }
	        });
        } catch (ex) {
            $$.log(ex);
        }
    }
};