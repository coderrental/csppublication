﻿#light
namespace UrlParserToJson
open System
open System.Diagnostics
open System.Data.SqlClient

type LanguageObj(id:int, code:string, desc:string) = 
    member this.Id = id
    member this.Code = code
    member this.Desc = desc

type A = {Id: int; Code: string; Desc: string }    

    type public ActiveLanguagesToJson() =
        member public this.GetJson(url: Uri, connectionString:string) =
        use conn = new SqlConnection(connectionString)
        use cmd = new SqlCommand("select languages_Id, languagecode, description from languages where active=1 order by description", conn)
        conn.Open()
        use reader = cmd.ExecuteReader()
//        let languages = 
//                        [ while reader.Read() do            
//                            yield new LanguageObj(reader.GetInt32(0), reader.GetString(1),reader.GetString(2))]
    
        let languages = 
                [ while reader.Read() do
                    yield { A.Id = reader.GetInt32(0) ; Code = reader.GetString(1); Desc = reader.GetString(2) }
                ]
                            
        conn.Close()
        reader.Dispose
        Newtonsoft.Json.JsonConvert.SerializeObject(languages) 

