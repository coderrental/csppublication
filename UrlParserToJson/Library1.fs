﻿namespace UrlParserToJson
open System

type public ContentCollectionToJson() = 

    member this.Clean(text:string) = 
        let temp = text.Replace("\"","\\\"").Replace("\r","").Replace("\n","")        
        temp
    member public this.GetJson (cc: System.Collections.Generic.List<obj>) =        
        let a = cc 
                |> Seq.sortBy(fun b -> b.GetType().Name)
                |> Seq.map(fun b ->
                                let v = 
                                    b.GetType().GetProperties() 
                                    |> Seq.map(fun p -> (p.Name, if p.GetValue(b) = null then "" else p.GetValue(b).ToString() ))
                                (b.GetType().Name, v |> Map.ofSeq)
                           ) 
                |> Seq.groupBy(fun b -> fst b)

        let jsonText = 
                (a |> Seq.fold(fun c d -> 
                    c + 
                    if c = "" then "{\"" + fst d + "\": ["
                        else "]},"
                    + (snd d 
                        |> Seq.fold(
                            fun initial value ->                                 
                                let nameValueJson = (snd value)
                                                    |> Map.fold(fun state k v -> 
                                                        state + String.Format("{0}\"{1}\":\"{2}\"", (if state ="" then "{" else "},{"), k ,this.Clean(v))
                                                    ) "" // end of fold
                                (if initial <> "" then initial + "\r\n," else "") 
                                + nameValueJson 
                                + (if nameValueJson <> "" then "}" else "") // close the value
                    ) "") // end of fold
                    
                ) "") // end of fold
                + (if a|>Seq.length = 1 then "]}" else "")

        // test code
        "[" + jsonText + "]"       
