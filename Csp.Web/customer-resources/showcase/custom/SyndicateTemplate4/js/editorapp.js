var categoryTree = [];
var rawCategoryData = [];
var wdWidth = jQuery(window).width();
var crrCategoryItem;
var countMainNavLiLoad = 0;
var SYNDICATION_TYPE = 'showcase';
var VALID_SYNDICATION_TYPES = ['', SYNDICATION_TYPE, 'all', 'hidden'];
var FeaturedPages = {
	'home': '',
	'product': '',
	'contactus': ''
};

function ReAdjustMainNav(){
	if (countMainNavLiLoad >= 5) return;
	if (jQuery('#main-nav-left-wrapper li').length == 0) {
		countMainNavLiLoad++;
		setTimeout(function() { ReAdjustMainNav(); }, 1000);
		return;
	}
	
	startReAdjustMainNav();
}

function DoFilterSyndicationType(data) {
	var filteredList = [];
	for (var i = 0; i < data.length; i++) {
		var type = data[i].localsyndicationtype.toLowerCase();
		
		if (VALID_SYNDICATION_TYPES.indexOf(type) != -1)
			filteredList.push(data[i]);
	}
	return filteredList;
}

function startReAdjustMainNav() {
	var menuWidth = jQuery('#main-nav-left-wrapper').innerWidth();
	var totalLiWidth = 0;
	var isOverFlow = false;
	var windowWidth = jQuery(window).width();
	
	if (windowWidth <= 530) {
		jQuery('#main-nav-left-wrapper ul:first > li').not('.hidden-contactus').show();
		return;
	}
	
	jQuery('#mn-overflow-items').html('');
	jQuery('#main-nav-left-wrapper ul:first > li').not('.hidden-contactus').each(function() {
		totalLiWidth += jQuery(this).outerWidth();
		if (totalLiWidth > menuWidth) {
			jQuery(this).clone().appendTo('#mn-overflow-items').show();
            jQuery(this).hide();
			isOverFlow = true;
        }
		else {
			jQuery(this).show();
		}
	});
	if (isOverFlow)
		jQuery('#mn-overflow-toggle').show();
};

/*
function setWidth(){
	var width = 0;
	jQuery('#main-nav-left-wrapper > ul > li').each(function(){
		if(jQuery(this).width() > width ){
			width = jQuery(this).width();
		}
	});
	return width;
}

function countMenu(){
	var length = jQuery('#main-nav-left-wrapper ul:first > li').length;
	var navWidth = jQuery("#main-nav-left-wrapper").width();
	var width= setWidth();
	if( wdWidth > 550){
		jQuery('#sce-wrapper .navbar-collapse .nav:first > li').css("width", width);
	}
	
	if ((width * length) < navWidth)
		$('#custom-toggle-btn').css('visibility', 'hidden');
	var count = Math.floor(navWidth / width) - 1;
	toggleMenu();
	for(i = count + 2; i < length + 1 ;i ++){
		jQuery("#main-nav-left-wrapper ul:first > li:nth-child("+i+") > a").css("display","none");
		var text = jQuery("#main-nav-left-wrapper ul:first > li:nth-child("+i+") > a").text();
		var elementUrl = jQuery("#main-nav-left-wrapper ul:first > li:nth-child("+i+") > a").attr("href");
		jQuery("#popup-menu ul").append("<li><a href='"+ elementUrl + "'>"+ text + "</a></li>");
	}
	
}

function toggleMenu(){
	jQuery(".navbar-header-right .navbar-toggle").unbind('click').click(function(){
		console.log('navbar-toggle hover');
		jQuery("#popup-menu").toggle();
	});
	
}
*/

var initApp = function () {
	jQuery('#main-nav-wrapper').stickyfloat({ duration: 0, startOffset: 200, offsetY: 0 });
	
	jQuery('#mn-overflow-toggle').on('click', function(e) {
		e.preventDefault();
		jQuery('#mn-overflow-items').toggle();
	});
	
	try {
		//$('head').append('<link rel="stylesheet" href="' + ngViewFolderUrl + '/FlatStyle2/css/bootstrap.min.css" type="text/css" />');
		initCategoryList();
		initSupplierLogo();
		loadFooterImages();
		
		$('#copyright-year').text(new Date().getFullYear());
	} catch (exc) {
		console.log(exc);
	}
	
}
function GetInnerText(itemTitle) {
	var regex = /(<([^>]+)>)/ig;
	var title = jQuery('<div>').html(itemTitle);
	var htmlString = $('<input>').val(title.text());
	var innerText = htmlString.val().replace(regex, "");
	innerText = innerText.replace(/&nbsp;/g, ' ');
	return innerText;
	/*
	itemTitle = itemTitle.replace('&lt;br&gt;', '');
	var title = jQuery('<div/>').html(itemTitle).text();
	if(jQuery(title).text()!=''){
		return jQuery(title).text();
	}
	return title;
	*/
}
function GetHTMLString(itemTitle) {
	return jQuery('<div/>').html(itemTitle).text();
}
function initCategoryList() {
	switchLoadingPanel(true);
	$.ajax({
		url: categoryJSONUrl,
		type: 'POST',
		dataType: 'json',
		data: { ajaxaction: 'get_category' },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (data) {
			data = DoFilterSyndicationType(data);
			rawCategoryData = data;
			if (typeof (rootId) != 'undefined' && !isNaN(rootId)) {
				categoryTree = convertJSONtoTree(data, rootId);
			}
			else
				categoryTree = convertJSONtoTree(data);
			
			switchLoadingPanel(false);
			
			//Build main nav bar
			for (var i = 0; i < categoryTree.length; i++)
				buildMainNav(categoryTree[i], jQuery('#csp-main-nav > #main-nav-left-wrapper > ul:first'));
			
			if (crrCategoryItem == undefined || document.URL.indexOf('categoryid') == -1) {
				try {
					switchLoadingPanel(true);
				} catch(exc) {}
				if (typeof(FeaturedPages.home)!="undefined"&&FeaturedPages.home!="") {
					if(document.URL.indexOf('.aspx?') == -1)
						window.location = document.URL + '?categoryid='+FeaturedPages.home;
					else
						window.location = document.URL + '&categoryid='+FeaturedPages.home;
					return;
				}
				else {
					switchLoadingPanel(false);
				}
				return;
			}
			
			//Render breadcrumb
			if (crrCategoryItem.CSP_Level.toLowerCase() != 'home') {
				initBreadCrumb();
				initLeftCol();
			}
			else {
				$('#sce-breadcrumb').hide();
			}
			// if (crrCategoryItem.hideOnTopNav == "hidden"){
				// $('#sce-breadcrumb').hide();
				// $('#sce-left-col').hide();
				// $('#main-nav-left-wrapper').hide();
				// $('#main-nav-contactus').hide();
				// $('#workspace').css("width","100%");
			// }
			// else{
				// $('#sce-breadcrumb').show();
				// $('#sce-left-col').show();
				// $('#main-nav-left-wrapper').show();
				// $('#main-nav-contactus').show();
			// }
			ReAdjustMainNav();
		},
		error: function (err) {
		}
	});
}

function buildMainNav(crrNode, parentNode) {

    if (VALID_SYNDICATION_TYPES.indexOf(crrNode.value.localsyndicationtype) == -1) {
        return;
    }
    
	var itemTitle = jQuery('<div>').html(crrNode.value.title).text();
	itemTitle = itemTitle.replace(/<br>/g, '');
	if (crrNode.value.categoryId == FeaturedPages.contactus) {
		$('#btn-contactus').children('a').text(itemTitle);
		if ($('.contactleft').length > 0) $('.contactleft').children('a').text(itemTitle);
		return;
	}
	var templateLi = '<li><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a><div class="block-item"><div class="arrow-up"></div><div class="item-info"></div></div></li>';
	if (crrNode.children != undefined) {
        // check right here if they have children with the right type
		/*
        var hasChildren = false;
        for(var i=0;i<crrNode.children.length;i++) {
            var item = crrNode.children[i];
            hasChildren = hasChildren || (item.value.localsyndicationtype==""||item.value.localsyndicationtype.toLowerCase()=="showcase");                        
        }
        if (!hasChildren) {
            templateLi = '<li><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a><div class="block-item"><div class="arrow-up"></div></div></li>';
        }
        else
            templateLi = '<li><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a><div class="block-item"><div class="arrow-up"></div><ul class="item-info"></ul></div></li>';
		*/
		
		//Please see 'data = DoFilterSyndicationType(data);' around line 153, the filtering is already there
		templateLi = '<li><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a><div class="block-item"><div class="arrow-up"></div><ul class="item-info"></ul></div></li>';
    }
		
	// if (parentNode.hasClass('item-info'))
		// templateLi = '<li class="item"><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a></li>';
	// if (hostUrl.indexOf('nuance.tiekinetix.net') == -1){
		// templateLi = '<li class="item"><a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '">' + itemTitle + '</a></li>';
	// }
	
	parentNode.append(jQuery(templateLi));
	
	// if (crrNode.children != undefined && !parentNode.hasClass('item-info')) {
			// var nextParentNode = parentNode.find('li:last ul.item-info:first');
			// for (var i = 0; i < crrNode.children.length; i++) {
				// buildMainNav(crrNode.children[i], nextParentNode);
			// }
		// }
	// if (hostUrl.indexOf('nuance.tiekinetix.net') != -1){
		// if (crrNode.children != undefined) {
			// var nextParentNode = parentNode.find('li:last ul.item-info:first');
			// for (var i = 0; i < crrNode.children.length; i++) {
				// buildMainNav(crrNode.children[i], nextParentNode);
			// }
		// }
	// }
	// else{
		// if (crrNode.children != undefined && !parentNode.hasClass('item-info')) {
			// var nextParentNode = parentNode.find('li:last ul.item-info:first');
			// for (var i = 0; i < crrNode.children.length; i++) {
				// buildMainNav(crrNode.children[i], nextParentNode);
			// }
		// }
	// }
	if (crrNode.children != undefined) {
			var nextParentNode = parentNode.find('li:last ul.item-info:first');
			for (var i = 0; i < crrNode.children.length; i++) {
				buildMainNav(crrNode.children[i], nextParentNode);
			}
		}
}

function initBreadCrumb() {
	var listBreadcrumb = [];
	var cateItem = crrCategoryItem;
	while (true) {
		if (typeof(rootId) != 'undefined' && cateItem.categoryId == rootId) break;
		if (cateItem.parentId == 0 || cateItem.parentId == '') break;
		listBreadcrumb.push(cateItem);
		for (var i = 0; i < rawCategoryData.length; i++) {
			if (cateItem.parentId == rawCategoryData[i].categoryId) {
				cateItem = rawCategoryData[i];
				break;
			}
		}
	}
	
	if (listBreadcrumb.length == 0) return;
	
	listBreadcrumb.pop();
	
	for (var i = 0; i < rawCategoryData.length; i++) {
		if (rawCategoryData[i].CSP_Level.toLowerCase() == 'home') {
			listBreadcrumb.push(rawCategoryData[i]);
			break;
		}
	}
		
	listBreadcrumb.reverse();
	
	for (var j = 0; j < listBreadcrumb.length; j++) {
		var itemClassName = '';
		var breadcrumbTitle = listBreadcrumb[j].title;
		breadcrumbTitle = GetInnerText(breadcrumbTitle);
		if (j == listBreadcrumb.length - 1)
			itemClassName = 'current';
		var bcWrapper = $('<span>');
		var breadcrumbItem = $('<a class="' + itemClassName + '" href="' + categoryUrlFormat + '?categoryid=' + listBreadcrumb[j].categoryId + '">');
		breadcrumbItem.text(breadcrumbTitle);
		bcWrapper.append(breadcrumbItem);
		if (j < listBreadcrumb.length - 1)
			bcWrapper.append($('<span class="bc-decor">&gt;</span>'));
			
		$('#sce-breadcrumb').append(bcWrapper);
	}
}

function initLeftCol() {
	$('#sce-left-col').show();
	var leftItemList = [];
	var childrenList = [];
	var siblingList = [];
	
	for (var j = 0; j < rawCategoryData.length; j++) {
		if (rawCategoryData[j].parentId == crrCategoryItem.categoryId) {
			childrenList.push(rawCategoryData[j]);
		}
		else if (rawCategoryData[j].parentId == crrCategoryItem.parentId) {
			siblingList.push(rawCategoryData[j]);
		}
	}
	
	leftItemList = childrenList;
	if (leftItemList.length == 0)
		leftItemList = siblingList;
	
	if (leftItemList.length == 0) {
		$('#sce-left-col').hide();
	}
	else {
		$('#workspace').addClass('col-md-8 block-content');
	}
	for (var i = 0; i < leftItemList.length; i++) {
        // if (leftItemList[i].localsyndicationtype!=""&&leftItemList[i].localsyndicationtype.toLowerCase()!="all"&&leftItemList[i].localsyndicationtype.toLowerCase()!="showcase") {
			// continue;
        // }
        
		
		var categoryTitle = leftItemList[i].title;
		var hasSubmenu = false;
		categoryTitle = GetInnerText(categoryTitle);
		var leftItem = $('<li class="up-menu">');
		if (leftItemList[i].categoryId == crrCategoryItem.categoryId)
			leftItem.addClass('active');
		var leftItemLink = $('<a href="' + categoryUrlFormat + '?categoryid=' + leftItemList[i].categoryId + '">').html('<h1>' + categoryTitle + '</h1>');
		leftItem.append(leftItemLink);
		
		for (var j = 0; j < rawCategoryData.length; j++) {
			if (rawCategoryData[j].parentId == leftItemList[i].categoryId) {
				var subMenu = leftItem.find('ul.menu-child');
				if (leftItem.find('ul.menu-child').length == 0) {
					subMenu = $('<ul class="menu-child">');
					leftItem.append(subMenu);
				}
				var subItem = $('<li>').html('<a href="' + categoryUrlFormat +'?categoryid='+ rawCategoryData[j].categoryId + '">' + GetInnerText(rawCategoryData[j].title) + '<a>');
				subMenu.append(subItem);
				hasSubmenu = true;
			}
		}
		if (hasSubmenu) {
			var btnToggle = $('<a class="submenu-toggle">');
			btnToggle.click(toggleSubmenu);
			leftItem.prepend(btnToggle);
		}
		$('#sce-left-col > ul').append(leftItem);
	}

	//var hostUrl = window.location.hostname;
	if (hostUrl.indexOf('nuance.tiekinetix.net') != -1){
		jQuery('#sce-left-col').hide();
		jQuery('#workspace').css('width','100%');
	}
}

function toggleSubmenu (e) {
	e.preventDefault();
	var thisEl = $(e.currentTarget);
	var parentLi = thisEl.parent('li.up-menu');
	if (parentLi.length == 0) return;
	parentLi.find('ul.menu-child').toggle();
	if (parentLi.find('ul.menu-child').is(':visible'))
		thisEl.addClass('submenu-up');
	else
		thisEl.removeClass('submenu-up');
}

function initPageInfo() {
	$('#sce-prod-title').text(crrCategoryItem.title);
	$('#sce-prod-desc').text(crrCategoryItem.description);
}

function initHomeBlocks(categoryArr) {
	var prodItem;
	var itemCount = 0;
	//console.log(crrCategoryItem, categoryArr);
	if (crrCategoryItem.CSP_Level.toLowerCase() == 'home' || crrCategoryItem.CSP_Level.toLowerCase() == 'product') {
		for (var i = 0; i < categoryArr.length; i++) {
			if (categoryArr[i].CSP_Level == undefined || categoryArr[i].CSP_Level == '') continue;
			if (categoryArr[i].CSP_Level.toLowerCase() == 'product') {
				prodItem = categoryArr[i];
				break;
			}
		}
		/*
		<div class="sub-category-block ng-scope" ng-include="'homeblock-tmpl'" ng-repeat="block in homeBlocks | orderBy:'displayorder'">
			<div class="block-wrapper ng-scope">
				<a class="sub-category-block-figure" ng-href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137" href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137">
					<img ng-src="http://localhost/dnn/0602/customer-resources/showcase/custom/ShowcaseMedia/Portal_0/User_1/banner-1.png" src="http://localhost/dnn/0602/customer-resources/showcase/custom/ShowcaseMedia/Portal_0/User_1/banner-1.png">
				</a>
				<h2>
					<a class="ng-binding" ng-href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137" href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137">About Epicor EDI Partner TIE Kinetix and Epicor Partnership</a>
				</h2>
				<p class="ng-binding" ng-bind-html-unsafe="block.description">this is a feature</p>
			</div>
		</div>
		*/
		for (var j = 0; j < categoryArr.length; j++) {
			if (categoryArr[j].parentId == prodItem.categoryId) {
				var categoryDesc = GetHTMLString(categoryArr[j].description);
				//categoryDesc = GetInnerText(categoryDesc);
				var categoryTitle = GetHTMLString(categoryArr[j].title);
				//categoryTitle = GetInnerText(categoryArr[j].title);
				var newSubBlock = $('<div class="sub-category-block">');
				var newSubBlockWrapper = $('<div class="block-wrapper ng-scope">');
				var newBlockFigureLink = $('<a class="sub-category-block-figure" href="' + categoryUrlFormat + '?categoryid=' + categoryArr[j].categoryId + '">');
				var newFigureImage = $('<img src="' + categoryArr[j].thumbnail + '" />');
				//var newBlockTitle = $('<h2>');
				var newBlockTitleTextLink = $('<a class="block-title" href="' + categoryUrlFormat + '?categoryid=' + categoryArr[j].categoryId + '">').html(categoryTitle);
				var newBlockDesc = $('<p>').html(categoryDesc);
				
				newBlockFigureLink.append(newFigureImage);
				//newBlockTitle.append(newBlockTitleTextLink);
				newSubBlockWrapper.append(newBlockFigureLink);
				newSubBlockWrapper.append(newBlockTitleTextLink);
				newSubBlockWrapper.append(newBlockDesc);
				newSubBlock.append(newSubBlockWrapper);
				$('#sub-category-blocks').append(newSubBlock);
				itemCount++;
			}
		}
		
		$('#sub-category-blocks').addClass('home-blocks-' + itemCount);
	}
	else
		$('#sub-category-blocks').hide();
}

function initSupplierLogo() {
	if (supplierLogo == '') {
		$('#sce-logo-img').hide();
		return;
	}
	$('#sce-logo-img').attr('src', supplierLogo);
	$('#sce-logo-img').parent('a').attr('href', categoryUrlFormat);
}

function loadFooterImages() {
	themeFolder = themeFolder.replace('~/', '/');
	$('#footer').find('img').each(function() {
		$(this).attr('src', themeFolder + $(this).attr('src'));
	});
}

var convertJSONtoTree = function (inputArr, rootId) {
	var fullArray = inputArr;
	var rootFound= false;
	if (rootId != undefined && !isNaN(rootId)) {
		for (var w = inputArr.length - 1; w >= 0; w--) {
			if (inputArr[w].categoryId == rootId)
				rootFound = true;
			if (inputArr[w].parentId == 0 && inputArr[w].categoryId != rootId)
				inputArr.splice(w, 1);
		}
		
		if (!rootFound) {
			var rootNode = [
					{
						"title" : "Root",
						"categoryId" : rootId,
						"parentId" : "",
						"CSP_Level" : ""
					}
				]
			fullArray = rootNode.concat(inputArr);
			rawCategoryData = fullArray;
		}
	}
	var roots = [], children = {};

	// find the top level nodes and hash the children based on parent
	for (var i = 0, len = fullArray.length; i < len; ++i) {
		var item = fullArray[i],
			p = item.parentId,
			target = !p ? roots : (children[p] || (children[p] = []));

		target.push({ value: item });
		
		if (fullArray[i].categoryId == parseInt(categoryId))
			crrCategoryItem = fullArray[i];
		if (fullArray[i].CSP_Level.toLowerCase() == 'home' || fullArray[i].CSP_Level.toLowerCase() == 'product' || fullArray[i].CSP_Level.toLowerCase() == 'contactus')
			FeaturedPages[fullArray[i].CSP_Level.toLowerCase()] = fullArray[i].categoryId;
	}
	
	// function to recursively build the tree
	var findChildren = function(parent) {
		if (children[parent.value.categoryId]) {
			parent.children = children[parent.value.categoryId];
			for (var i = 0, len = parent.children.length; i < len; ++i) {
				findChildren(parent.children[i]);
			}
		}
	};

	// enumerate through to handle the case where there are multiple roots
	for (var i = 0, len = roots.length; i < len; ++i) {
		findChildren(roots[i]);
	}
	
	return roots[0].children;
}


