var categoryTree = [];
var rawCategoryData = [];
var crrCategoryItem;

var initApp = function () {
	try {
		//$('head').append('<link rel="stylesheet" href="' + ngViewFolderUrl + '/FlatStyle2/css/bootstrap.min.css" type="text/css" />');
		initCategoryList();
	} catch (exc) {
		console.log(exc);
	}
}

function initCategoryList() {
	$.ajax({
		url: categoryJSONUrl,
		type: 'POST',
		dataType: 'json',
		data: { ajaxaction: 'get_category' },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (data) {
			rawCategoryData = data;
			categoryTree = convertJSONtoTree(data);
			
			//console.log(crrCategoryItem);		
			//Render breadcrumb
			if (crrCategoryItem.CSP_Level.toLowerCase() != 'home')
				initBreadCrumb();
			
			//Render page info
			//initPageInfo();
			
			initHomeBlocks(data);
		},
		error: function (err) {
		}
	});
}

function initBreadCrumb() {
	var listBreadcrumb = [];
	var cateItem = crrCategoryItem;
	while (true) {
		if (cateItem.parentId == 0) break;
		listBreadcrumb.push(cateItem);
		for (var i = 0; i < rawCategoryData.length; i++) {
			if (cateItem.parentId == rawCategoryData[i].categoryId) {
				cateItem = rawCategoryData[i];
				break;
			}
		}
	}
	
	if (listBreadcrumb.length == 0) return;
	
	listBreadcrumb.pop();
	
	for (var i = 0; i < rawCategoryData.length; i++) {
		if (rawCategoryData[i].CSP_Level.toLowerCase() == 'home') {
			listBreadcrumb.push(rawCategoryData[i]);
			break;
		}
	}
		
	listBreadcrumb.reverse();
	
	for (var j = 0; j < listBreadcrumb.length; j++) {
		var itemClassName = '';
		if (j == listBreadcrumb.length - 1)
			itemClassName = 'current';
		var bcWrapper = $('<span>');
		var breadcrumbItem = $('<a class="' + itemClassName + '" href="' + categoryUrlFormat + '?categoryid=' + listBreadcrumb[j].categoryId + '">');
		breadcrumbItem.text(listBreadcrumb[j].title);
		bcWrapper.append(breadcrumbItem);
		if (j < listBreadcrumb.length - 1)
			bcWrapper.append($('<span class="bc-decor">&gt;</span>'));
			
		$('#sce-breadcrumb').append(bcWrapper);
	}
}

function initPageInfo() {
	$('#sce-prod-title').text(crrCategoryItem.title);
	$('#sce-prod-desc').text(crrCategoryItem.description);
}

function initHomeBlocks(categoryArr) {
	var prodItem;
	var itemCount = 0;
	//console.log(crrCategoryItem, categoryArr);
	if (crrCategoryItem.CSP_Level.toLowerCase() == 'home' || crrCategoryItem.CSP_Level.toLowerCase() == 'product') {
		for (var i = 0; i < categoryArr.length; i++) {
			if (categoryArr[i].CSP_Level == undefined || categoryArr[i].CSP_Level == '') continue;
			if (categoryArr[i].CSP_Level.toLowerCase() == 'product') {
				prodItem = categoryArr[i];
				break;
			}
		}
		for (var j = 0; j < categoryArr.length; j++) {
			if (categoryArr[j].parentId == prodItem.categoryId) {
				var newLi = $('<li>');
				var newA = $('<a href="' + categoryUrlFormat + '?categoryid=' + categoryArr[j].categoryId + '">');
				var newDiv = $('<div>').text(categoryArr[j].title);
				newA.append(newDiv);
				newLi.append(newA);
				$('#home-blocks ul').append(newLi);
			}
		}
	}
	else
		$('#home-blocks ul').hide();
}

var convertJSONtoTree = function (inputArr, rootId) {
	var fullArray = inputArr;
	var roots = [], children = {};

	// find the top level nodes and hash the children based on parent
	for (var i = 0, len = fullArray.length; i < len; ++i) {
		var item = fullArray[i],
			p = item.parentId,
			target = !p ? roots : (children[p] || (children[p] = []));

		target.push({ value: item });
		
		if (fullArray[i].categoryId == parseInt(categoryId))
			crrCategoryItem = fullArray[i];
	}
	
	// function to recursively build the tree
	var findChildren = function(parent) {
		if (children[parent.value.categoryId]) {
			parent.children = children[parent.value.categoryId];
			for (var i = 0, len = parent.children.length; i < len; ++i) {
				findChildren(parent.children[i]);
			}
		}
	};

	// enumerate through to handle the case where there are multiple roots
	for (var i = 0, len = roots.length; i < len; ++i) {
		findChildren(roots[i]);
	}
	
	return roots[0].children;
}