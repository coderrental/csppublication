var categoryTree = [];
var rawCategoryData = [];
var crrCategoryItem;
var CrrLangId = 0;
var SYNDICATION_TYPE = 'showcase';
var TemplateVersion = 1;
var FirstImageBlockDetectInterval;
var VALID_SYNDICATION_TYPES = ['', SYNDICATION_TYPE, 'all', 'hidden'];
var FeaturedPages = {
	'home': '',
	'product': '',
	'contactus': ''
}
var CONTACT_US_TRANSLATION = {
	'11': 'Vraag hier uw advies aan',
	'1': 'Contact us'
}
var HOME_TEXT_TRANSLATION = {
	'11': '<h1>Ontdek onze beste producten</h1><p>Luxaflex® maakt al meer dan 60 jaar raambekledingen en biedt u een uitgebreid aanbod van exclusieve, op maat gemaakte raambekledingen. Hier is een kleine selectie van onze beste producten.</p>'
}
var initShowhide = function(){
	$( "#lux-wrapper #content .row .col-sm-6 .block-shadow .infor" ).hide();
	$( "#lux-wrapper #content .row .col-sm-6 .block-shadow " ).hover(function() {
		$( this).find(".infor").fadeIn(250);
		}, function() {
		$( this).find(".infor").hide();
		}
	);
	AppendClear();
	function AppendClear(){
		var i =0;
		$('#lux-wrapper #content .row .col-sm-6').each(function(j){
               i = i + 1;
               if (i == 3) {
                      // add class 
                      $(this).after('<div style="clear:both"></div>'); 
                      i = 0;
               }
          });
		var length= $('#lux-wrapper #content .row .col-sm-6').length;
		if(length%3!=0){
			$('#home-blocks').append('<div class="fix-height"></div>');
		}
	}
}

function TemplateVersionDetect() {
	TemplateVersion = 2; return;
	var theUrlEl = categoryJSONUrl.split('langid/')[1];
	var langId = theUrlEl.split('/').shift();
	CrrLangId = parseInt(langId);
	if (CrrLangId == 11)
		TemplateVersion = 2;
}

var initApp = function () {
	try {
		TemplateVersionDetect();
		if (TemplateVersion == 1) {
			$('#header2').attr('style', 'display: hidden !important');
			$('#header2').remove();
			initCategoryList();
		}
		else
			initNewTemplateVersion();
	} catch (exc) {
		console.log(exc);
	}
}

function DoFilterSyndicationType(data) {
	var filteredList = [];
	for (var i = 0; i < data.length; i++) {
		var type = data[i].localsyndicationtype.toLowerCase();
		
		if (VALID_SYNDICATION_TYPES.indexOf(type) != -1)
			filteredList.push(data[i]);
	}
	return filteredList;
}

function hideOldVersionSections() {
	jQuery('#csp-wrapper #header').hide();
	jQuery('#content #template-1-row').hide();
	jQuery('#footer').hide();
}

function AdjustHomeButtonsPosition() {
	var firstContentBlock = $('#workspace .cr-inline-column-content:first');
	if (firstContentBlock.length > 0 && (firstContentBlock.attr('rel') == 'wgimagetext' || firstContentBlock.attr('rel') == 'wgimage')) {
		var pluginWrapper = firstContentBlock.parents('div.columnwraper:first');
		if (typeof(pluginWrapper.next().attr('id')) == 'undefined' || pluginWrapper.next().attr('id') != 'new-homeblock-buttons')
			jQuery('#new-homeblock-buttons').insertAfter(firstContentBlock.parents('div.columnwraper:first'));
	}
}

function showNewSections() {
	initSupplierLogo();	
	jQuery('#header2').removeClass('hide').show();
	jQuery('#workspace').addClass('template-2');
	jQuery('#new-homeblock-buttons').removeClass('hide').show();
	jQuery('.editor-work-space #content').addClass('content-template-2');
	FirstImageBlockDetectInterval = setInterval(AdjustHomeButtonsPosition, 1500);
}

function initHomeButtons(tree) {
	if (typeof (crrCategoryItem) != 'undefined' && crrCategoryItem.CSP_Level.toLowerCase() != 'home') return;
	for(var i = 0, len = tree.length; i < len; i++){
		if(tree[i].value.CSP_Level.toLowerCase() == 'product'){
			for (var j = 0; j < tree[i].children.length; j++) {
				jQuery('#new-homeblock-buttons ul').append('<li class="home-link"><a class="home-link-button" href="' + categoryUrlFormat + '?categoryid=' + tree[i].children[j].value.categoryId + '">' + tree[i].children[j].value.title + '</a></li>');
			}
			break;
		}
	}
	var textContactUs = typeof (CONTACT_US_TRANSLATION[CrrLangId]) != 'undefined' ? CONTACT_US_TRANSLATION[CrrLangId] : CONTACT_US_TRANSLATION[1];
	jQuery('#new-homeblock-buttons ul').append('<li class="home-link"><a class="home-link-button" href="javascript: void(0)">' + textContactUs + '</a></li>');
	
	var homeText = typeof(HOME_TEXT_TRANSLATION[CrrLangId]) != 'undefined' ? HOME_TEXT_TRANSLATION[CrrLangId] : '';
	jQuery('#home-text').html(homeText);
}

function initNewTemplateVersion() {
	hideOldVersionSections();
	showNewSections();
	
	$.ajax({
		url: categoryJSONUrl,
		type: 'POST',
		dataType: 'json',
		data: { ajaxaction: 'get_category' },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (data) {
			data = DoFilterSyndicationType(data);
			rawCategoryData = data;
			categoryTree = convertJSONtoTree(data,rootId);
			/*
			if (crrCategoryItem == undefined || document.URL.indexOf('categoryid') == -1) {
				try {
					switchLoadingPanel(true);
				} catch(exc) {}
				if (typeof(FeaturedPages.home)!="undefined"&&FeaturedPages.home!="") {
					if(document.URL.indexOf('.aspx?') == -1)
						window.location = document.URL + '?categoryid='+FeaturedPages.home;
					else
						window.location = document.URL + '&categoryid='+FeaturedPages.home;
					return;
				}
				else {
					switchLoadingPanel(false);
				}
				return;
			}
			*/
			initHomeButtons(categoryTree);
		},
		error: function (err) {
		}
	});
}

function initCategoryList() {
	$.ajax({
		url: categoryJSONUrl,
		type: 'POST',
		dataType: 'json',
		data: { ajaxaction: 'get_category' },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (data) {
			data = DoFilterSyndicationType(data);
			rawCategoryData = data;
			categoryTree = convertJSONtoTree(data,rootId);
			/*
			if (crrCategoryItem == undefined || document.URL.indexOf('categoryid') == -1) {
				try {
					switchLoadingPanel(true);
				} catch(exc) {}
				if (typeof(FeaturedPages.home)!="undefined"&&FeaturedPages.home!="") {
					if(document.URL.indexOf('.aspx?') == -1)
						window.location = document.URL + '?categoryid='+FeaturedPages.home;
					else
						window.location = document.URL + '&categoryid='+FeaturedPages.home;
					return;
				}
				else {
					switchLoadingPanel(false);
				}
				return;
			}
			*/
			try {
				InitHomeBlocks(categoryTree);
			} catch(exc) {}
			initSupplierLogo();	
			/*if (crrCategoryItem.CSP_Level.toLowerCase() != 'home')
				initBreadCrumb();*/
						
		},
		error: function (err) {
		}
	});
}

function initSupplierLogo(){
	 if (supplierLogo == '') {
		  $('#header-logo').append('<a href="'+categoryUrlFormat+'"><img src="http://placehold.it/105x45" /></a>');
		  $('#header2-logo').append('<a href="'+categoryUrlFormat+'"><img src="http://placehold.it/105x45" /></a>');
		  return;
	 }
	 $('#header-logo').append('<a href="'+categoryUrlFormat+'"><img src="'+supplierLogo+'" /></a>');
	 $('#header2-logo').append('<a href="'+categoryUrlFormat+'"><img src="'+supplierLogo+'" /></a>');
}

function initBreadCrumb() {
	var listBreadcrumb = [];
	var cateItem = crrCategoryItem;
	while (true) {
		if (cateItem.parentId == 0) break;
		listBreadcrumb.push(cateItem);
		for (var i = 0; i < rawCategoryData.length; i++) {
			if (cateItem.parentId == rawCategoryData[i].categoryId) {
				cateItem = rawCategoryData[i];
				break;
			}
		}
	}
	
	if (listBreadcrumb.length == 0) return;
	
	listBreadcrumb.pop();
	
	for (var i = 0; i < rawCategoryData.length; i++) {
		if (rawCategoryData[i].CSP_Level.toLowerCase() == 'home') {
			listBreadcrumb.push(rawCategoryData[i]);
			break;
		}
	}
		
	listBreadcrumb.reverse();
	
	for (var j = 0; j < listBreadcrumb.length; j++) {
		var itemClassName = '';
		if (j == listBreadcrumb.length - 1)
			itemClassName = 'current';
		var bcWrapper = $('<span>');
		var breadcrumbItem = $('<a class="' + itemClassName + '" href="' + categoryUrlFormat + '?categoryid=' + listBreadcrumb[j].categoryId + '">');
		breadcrumbItem.text(listBreadcrumb[j].title);
		bcWrapper.append(breadcrumbItem);
		if (j < listBreadcrumb.length - 1)
			bcWrapper.append($('<span class="bc-decor">&gt;</span>'));
			
		$('#sce-breadcrumb').append(bcWrapper);
	}
}

var InitHomeBlocks = function(tree){
	if (typeof (crrCategoryItem) != 'undefined' && crrCategoryItem.CSP_Level.toLowerCase() != 'home') return;
	var listcategories=[];
	if(listcategories.length < 1){
		$('#sce-page-navigation').hide();
	}
	for(var i = 0, len = tree.length; i < len; i++){
		if(tree[i].value.CSP_Level.toLowerCase() == 'product'){
			var treeProduct=tree[i].children;
			for(var j = 0; j < treeProduct.length; j++){
				var lastLevelCategory = treeProduct[j].children;
				if(lastLevelCategory.length < 1){
					$('#sce-page-navigation').hide();
				}
				for(var k =0; k < lastLevelCategory.length; k++){
					var type=lastLevelCategory[k].value.localsyndicationtype.toLowerCase();
					if(type==''||type=='all'||type=="showcase"){
						var newColumn = $('<div class="col-sm-6 col-md-3"></div>');
						var newBlockShadow=$('<div class="block-shadow"></div>');
						var newDivImg=$('<div class="img"></div>');
						var thumbnail='http://placehold.it/200x130';
						if(lastLevelCategory[k].value.thumbnail!=''){
							thumbnail=lastLevelCategory[k].value.thumbnail;
						}
						var newA=$('<a href="' + categoryUrlFormat + '?categoryid=' + lastLevelCategory[k].value.categoryId + '"><img src="'+thumbnail+'" /></a>');
						var newDescription=$('<div class="description"></div>');
						var newTitle=$('<div class="title">'+lastLevelCategory[k].value.title+'</div>');
						var newInfo=$('<div class="infor"><p>'+jQuery('<div/>').html(lastLevelCategory[k].value.description).text()+'</p></div>');
						
						newDescription.append(newTitle).append(newInfo);
						newDivImg.append(newA);
						newBlockShadow.append(newDivImg).append(newDescription);					
						newColumn.append(newBlockShadow);
						$('#home-blocks').append(newColumn);
						listcategories.push(lastLevelCategory[k].value);
					}
				}
			}
		}
	}

	var GetPrevCategory = function(){
		for(var i=0;i<listcategories.length;i++){
			if(listcategories[i].categoryId==crrCategoryItem.categoryId){
				if(i==0){
					$('#sce-page-navigation .prev').hide();
					return;
				}
				else{
					$('#sce-page-navigation .prev').show();
					var linkUrl= categoryUrlFormat + '?categoryid=' + listcategories[i-1].categoryId;
					$('#sce-page-navigation .prev a').attr('href',linkUrl);
				}
			}			
		}
	}
	
	var GetNextCategory = function(){
		for(var i=0;i<listcategories.length;i++){
			if(listcategories[i].categoryId==crrCategoryItem.categoryId){
				if(i==listcategories.length-1){
					$('#sce-page-navigation .next').hide();
					return;
				}
				else{
					var linkUrl= categoryUrlFormat + '?categoryid=' + listcategories[i+1].categoryId;
					$('#sce-page-navigation .next a').attr('href',linkUrl);
				}
			}			
		}
	}
	if(crrCategoryItem.CSP_Level.toLowerCase() == 'home'){
		$('#home-blocks').show();
		$('#sce-page-navigation').hide();
		initShowhide();
	}
	else{
		GetNextCategory();
		GetPrevCategory();
		$('#sce-page-navigation').show();
		$('#home-blocks').hide();
		$('#introduction').hide();
	}
	
}

var convertJSONtoTree = function (inputArr, rootId) {
	var fullArray = inputArr;
	var roots = [], children = {};

	// find the top level nodes and hash the children based on parent
	for (var i = 0, len = fullArray.length; i < len; ++i) {
		var item = fullArray[i],
			p = item.parentId,
			target = !p ? roots : (children[p] || (children[p] = []));

		target.push({ value: item });
		
		if (fullArray[i].categoryId == parseInt(categoryId))
			crrCategoryItem = fullArray[i];
		if (fullArray[i].CSP_Level.toLowerCase() == 'home' || fullArray[i].CSP_Level.toLowerCase() == 'product' || fullArray[i].CSP_Level.toLowerCase() == 'contactus')
			FeaturedPages[fullArray[i].CSP_Level.toLowerCase()] = fullArray[i].categoryId;
	}
	// function to recursively build the tree
	var findChildren = function(parent) {
		if (children[parent.value.categoryId]) {
			parent.children = children[parent.value.categoryId];
			for (var i = 0, len = parent.children.length; i < len; ++i) {
				findChildren(parent.children[i]);
			}
		}
	};

	// enumerate through to handle the case where there are multiple roots
	for (var i = 0, len = roots.length; i < len; ++i) {
		findChildren(roots[i]);
		if(roots[i].value.categoryId==rootId){
			return roots[i].children;
		}
	}	
}

