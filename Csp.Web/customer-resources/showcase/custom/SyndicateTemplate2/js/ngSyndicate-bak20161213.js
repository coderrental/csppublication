var SYNDICATION_TYPE = 'showcase';
var VALID_SYNDICATION_TYPES = ['', SYNDICATION_TYPE, 'all', 'hidden'];
var NO_PARTNER_ID_INJECTION = ['3001044'];
var OUR_PRODUCTS_TRANSLATION = {
	'NL': 'Onze Producten',
	'NLB': 'Onze Producten',
	'FRB': 'Nos produits'
};

var PREV_PRODUCT_TRANSLATION = {
	'NL': 'vorig product',
	'NLB': 'vorig product',
	'FRB': 'Précédent'
};

var NEXT_PRODUCT_TRANSLATION = {
	'NL': 'volgend product',
	'NLB': 'volgend product',
	'FRB': 'Prochain'
};

var CONTACT_US_TRANSLATION = {
	'NL': 'Vraag hier uw advies aan',
	'NLB': 'Vraag hier uw advies aan',
	'FRB': 'Cliquez ici pour demander un conseil',
	'UK': 'Contact us for advice',
	'EN': 'Contact us'
}

var HOME_TEXT_TRANSLATION = {
	'NL': '<h1>Ontdek onze beste producten</h1><p>Luxaflex® maakt al meer dan 60 jaar raambekledingen en biedt u een uitgebreid aanbod van exclusieve, op maat gemaakte raambekledingen. Hier is een kleine selectie van onze beste producten.</p>'
}

function initSupplierLogo(){
		var imageUrl=$('#lux-wrapper #header #header-logo a img').attr('src');
		if(imageUrl==''){
			$('#lux-wrapper #header #header-logo a img').attr('src','http://placehold.it/105x45');
		}
					

}

var FixImageThumbnail = function(){
	$('#lux-wrapper #content .row #home-blocks .col-sm-6 .block-shadow .img a img').each(function(){
		var imageUrl=$(this).attr('src');
		if(imageUrl==null){
			$(this).attr('src','http://placehold.it/200x130');
		}
					
	});
}
var initShowhide = function(){
	$( "#lux-wrapper #content .row #home-blocks .col-sm-6 .block-shadow .infor" ).hide();
	$( "#lux-wrapper #content .row #home-blocks .col-sm-6 .block-shadow " ).hover(function() {
		$( this).find(".infor").fadeIn(250);
		}, function() {
		$( this).find(".infor").hide();
		}
	);
	AppendClear();
	function AppendClear(){
		var i =0;
		
		$('#lux-wrapper #content .row #home-blocks .col-sm-6').each(function(j){
               i = i + 1;
               if (i == 3) {
                      // add class 
                      $(this).after('<div style="clear:both"></div>'); 
                      i = 0;
               }
        });
		
		var length= $('#lux-wrapper #content .row #home-blocks .col-sm-6').length;
		if(length%3!=0){
			$('#home-blocks').append('<div class="fix-height"></div>');
		}
	}
}


var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;

var moduleSyndicate = angular.module('ngSyndicate', []).config(function($routeProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contentpageview.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contentpageview.html'}).
		when('/category/:id', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contentpageview.html'}).
		when('/category/:id/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contentpageview.html'}).
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contactusview.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate2/contactusview.html'}).
		otherwise({ redirectTo: '/' });
});

moduleSyndicate.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

moduleSyndicate.factory('Categories', function ($http) {
	var rootId = getRootCategoryId();
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (listCategory) {
			data = listCategory;
		},
		convertJSONtoTree: function (rawArray) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(rawArray);
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			
			return roots[0].children;
		}
	}
});

moduleSyndicate.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						if (formFields[i].fieldlabel.indexOf('*') != -1)
							formFields[i].fieldvalidationtype = 'mandatoryField';
						if (formFields[i].fieldid.indexOf('email') != -1)
							formFields[i].fieldvalidationtype = 'mandatoryEmail';
						if (formFields[i].fieldid.indexOf('phone') != -1)
							formFields[i].fieldvalidationtype = 'mandatoryNumber';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();
				if (jQuery(this).prop("tagName") == 'SELECT')
					vValue = jQuery(this).find('option:selected').val();
				
				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}
				
			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			}
			alert(errorMessage);
			return false;
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

function MainNavCtrl($scope, $http, $rootScope, $routeParams, Categories, URLHandler) {
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.GetCategoryUrl = function (navItem) {
		var currentUrl = document.URL;
		if (navItem.value.cspLevel.toLowerCase() == 'contactus') return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		if (navItem.value.cspLevel.toLowerCase() == 'home') return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + navItem.value.categoryId;
	}
	
	$scope.GetDataToggle = function (navItem) {
		return 'dropdown';
	}
	
	Categories.promise.then(function(resultData){
		resultData.pop();
		$rootScope.AllProducts = Categories.convertJSONtoTree(resultData);
		$scope.navitems = $rootScope.AllProducts;
	});
}

function MainCtrl($scope, $rootScope, $http, $routeParams, $filter, $timeout, $location, $anchorScroll, Categories, URLHandler) {
	if (!$rootScope.validFeaturedPageList) return;
	
	tickValue = URLHandler.getTick();
	var crrCatId;
	var crrCatItem;
	var productCategories;
	$scope.ContentPage = '';
	$rootScope.CategoryItem = [];
	$rootScope.homeBlocks = [];
	$rootScope.homeBlocksRows = [];
	$rootScope.breadcrumbBlocks = [];
	$scope.ajaxLoading = true;
	$rootScope.nextCategory = [];
	$rootScope.prevCategory = [];
	$scope.ProdItems = [];
	$rootScope.IsHomePage = false;
	
	$scope.ScrollTo = function (sectionId) {
		console.log('scroll started', sectionId);
		$location.hash(sectionId);
		$anchorScroll();
	};
	var GetPrevCategory = function(){
		for(var i=0;i<productCategories.length;i++){
			if(productCategories[i].categoryId==crrCatId){
				if(i==0){
					$('#sce-page-navigation .prev').hide();
					return;
				}
				else{
					$('#sce-page-navigation .prev').show();
					$rootScope.prevCategory= productCategories[i-1];
				}
			}			
		}
	}
	
	var GetNextCategory = function(){
		for(var i=0;i<productCategories.length;i++){
			if(productCategories[i].categoryId==crrCatId){
				if(i==productCategories.length-1){
					$('#sce-page-navigation .next').hide();
					return;
				}
				else{
					$('#sce-page-navigation .next').show();
					$rootScope.nextCategory= productCategories[i+1];
				}
			}			
		}
	}
	
	var GetBreadCrumb = function (categoryList, crrId, bcArray) {
		if (crrId == 0) return;
		var crrItem = $filter('filter')(categoryList, {categoryId: crrId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if (crrItem[0] == undefined || crrItem[0].parentId == 0) return;
		
		bcArray.push(crrItem[0]);
		GetBreadCrumb(categoryList, crrItem[0].parentId, bcArray);
	}
	
	var InitNewVersionContent = function (categoryList) {
		if (crrCatId != $rootScope.FeaturedPageList['home'] && crrCatId != $rootScope.FeaturedPageList['product']) {
			$rootScope.ProdItems = [];
			return;
		}
		$rootScope.ProdItems = $filter('filter')(categoryList, {parentId: $rootScope.FeaturedPageList['product']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
	}
	
	$scope.GetDeptorParam = function () {
		if (typeof(cspDeptorId) == 'undefined' || cspDeptorId == '') return '';
		return '&partner_id=' + cspDeptorId;
	};
	$scope.initPageContent = function (resultData) {
		var htmlContent = jQuery('<div>').html($scope.ContentPage);
		
		try {
		//JavaScript Plugin
			htmlContent.find('.wgscript-wrapper').each(function() {
				var parentRow = jQuery(this).parents('.columnwraper');
				var totalPlugin = parentRow.find('.cr-inline-column-content').length;
				var jsPlugin = parentRow.find('.cr-inline-column-content[rel="wgscript"]');
				jQuery(this).parents('.cr-inline-column-content[rel="wgscript"]').hide();
				if (jsPlugin.length == totalPlugin)
					parentRow.hide();
				var scriptCode = jQuery(this).find('pre').text();
				try {
					eval(scriptCode);
				} catch (exc) {
					console.log(exc);
				}
				
			});
		}
		catch (err) {}			
	}
	Categories.promise.then(function(resultData){
		resultData = $filter('filter')(resultData, {syndicationtype: VALID_SYNDICATION_TYPES}, function (actual, expected) {
			if (expected.indexOf(actual) != -1) return true;
			return false;
		});
		if (typeof($routeParams.id) == "undefined") {
			crrCatId = $rootScope.FeaturedPageList['home'];
			crrCatItem = $filter('filter')(resultData, {categoryId: crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$rootScope.IsHomePage = true;
		}
		else {
			crrCatItem = $filter('filter')(resultData, {categoryId: $routeParams.id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			crrCatId = $routeParams.id;
		}
		
		if (crrCatItem.length == 0) {
			window.location = $rootScope.GetHomeUrl();
			return;
		}
		
		$rootScope.IsHomePage = crrCatId == $rootScope.FeaturedPageList['home'];
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1)
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
				
		$http.get('d1.aspx?p2013(ct66000&cd' + crrCatId + ')[]' + URLHandler.getUrlTick()).success(function(responseData) {
			$scope.ContentPage = responseData[0].contentpage;
			
			var htmlContent = jQuery('<div>').html(responseData[0].contentpage);
			try {
				/*
				htmlContent.find('a:not(.csp-category-deeplink)').each(function() {
					jQuery(this).attr('target', '_blank');
				});
				*/
				htmlContent.find('a[href$="pdf"]').each(function(){
					var url=jQuery(this).attr('href');
					var fileName= url.split("/").pop().split('.').shift();
					jQuery(this).attr({cspobj:"REPORT", csptype:"DOWNLOADS", cspenglishvalue:fileName});
				});
				
				if (NO_PARTNER_ID_INJECTION.indexOf(crrCatId) == -1) {
					
					htmlContent.find('iframe').each(function() {
						var deviceWidth = jQuery(window).width();
						jQuery(this).attr('src', jQuery(this).attr('src') + $scope.GetDeptorParam());
						if (deviceWidth < 660)
							jQuery(this).css('width', deviceWidth);
						else
							jQuery(this).css('max-width', '660');
						//jQuery(this).attr('src', '');
						
					});
				}
				
				htmlContent.find('.wgscript-wrapper').each(function() {
					var parentRow = jQuery(this).parents('.columnwraper');
					var totalPlugin = parentRow.find('.cr-inline-column-content').length;
					var jsPlugin = parentRow.find('.cr-inline-column-content[rel="wgscript"]');
					jQuery(this).parents('.cr-inline-column-content[rel="wgscript"]').hide();
					if (jsPlugin.length == totalPlugin)
						parentRow.hide();
					var scriptCode = jQuery(this).find('pre').text();
					try {
						eval(scriptCode);
					} catch (exc) {
						console.log(exc);
					}
					
				});
				
				$scope.ContentPage = htmlContent.html();
				//$scope.initPageContent(resultData);
			}
			catch (err) {}
			
			if (parseInt(cspEntryPoint) == 4) {
				//var htmlContent = jQuery('<div>').html(responseData[0].contentpage);
				var bannerTextBlocks = htmlContent.find('.imgtext-plugin-block');
				if (bannerTextBlocks.length > 0) {
					try {
						htmlContent.find('.imgtext-plugin-block').first().find('ul').remove();
						htmlContent.find('a[href="scrollto=header"]').remove();
					}
					catch (err) {}
				}
				$scope.ContentPage = htmlContent.html();
			}
			
			$scope.ContentPage = htmlContent.html() + '<div class="clear"></div>';
			
			setTimeout(function(){
				//Place the Home Buttons
				if ($rootScope.IsHomePage && $rootScope.TemplateVersion == 2) {
					var firstContentBlock = jQuery('#workspace .cr-inline-column-content:first');
					if (firstContentBlock.length > 0 && (firstContentBlock.attr('rel') == 'wgimagetext' || firstContentBlock.attr('rel') == 'wgimage')) {
						var homeButtons = jQuery('#new-homeblock-buttons');
						firstContentBlock.parents('div.columnwraper:first').css('margin', '0 !important');
						firstContentBlock.parents('div.columnwraper:first').removeAttr('style');
						homeButtons.clone(true).appendTo(firstContentBlock.parents('div.columnwraper:first')).show();
						homeButtons.hide();
						//jQuery('#new-homeblock-buttons').insertAfter(firstContentBlock.parents('div.columnwraper:first'));
					}
				}
				
				jQuery('.full-size').each(function() {
					jQuery(this).find('*').each(function() {
						if (!jQuery(this).hasClass('overlaywrap') && !jQuery(this).hasClass('player'))
							jQuery(this).width('100%');
					});
				});
				
				/*jQuery('a:not(.csp-category-deeplink)').each(function() {
					if (jQuery(this).attr('href').indexOf('scrollTo') >= 0)
						jQuery(this).attr('href', document.URL + jQuery(this).attr('href'));
				});*/
				
				jQuery('.csp-category-deeplink').each(function() {
					var cId = jQuery(this).attr('rel');
					if (cId == $rootScope.FeaturedPageList['contactus']) {
						jQuery(this).attr('href', '#/contactus');
						return;
					}
					var cItem = $filter('filter')(resultData, {categoryId: cId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
					if (cItem.length == 0) {
						jQuery(this).remove();
						return;
					}
					jQuery(this).attr('href', document.URL.substring(0, document.URL.indexOf('#/')) + '#/category/' + jQuery(this).attr('rel'));
				});
				
				$rootScope.updatePageInfo(crrCatItem[0].title);
			},500);
			
			$timeout(function(){
				$scope.ajaxLoading = false;
			},1500);
			
			/*
			setTimeout(function() {
				jQuery(document).click();							
			}, 1000);			
			*/
		});
		
		if ($rootScope.TemplateVersion >= 2) {
			InitNewVersionContent(resultData);
		}
		else {
			var productChildren = $filter('filter')(resultData, {parentId: $rootScope.FeaturedPageList['product']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			productCategories = [];
			for (var i = 0; i < productChildren.length; i++) {
				var content=$filter('filter')(resultData, {parentId: productChildren[i].categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual)});
				for(var k = 0; k < content.length;k++){
					var type = content[k].syndicationtype.toLowerCase();
					if(type==''||type=='all'||type=="showcase"){
						productCategories.push(content[k]);
					}
					
				}
			}
			
			if (crrCatId == $rootScope.FeaturedPageList['home'] || crrCatId == $rootScope.FeaturedPageList['product']) {
				$rootScope.homeBlocks = productCategories;
				
				setTimeout(function(){
					initShowhide();
					FixImageThumbnail();
					initSupplierLogo();
					$('#introduction').show();
					$('#sce-page-navigation').hide();
				},1);
			}
			else {
				$rootScope.breadcrumbBlocks.push(crrCatItem[0]);
				GetBreadCrumb(resultData, crrCatItem[0].parentId, $rootScope.breadcrumbBlocks);
				$rootScope.breadcrumbBlocks.pop();
				var homeItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				$rootScope.breadcrumbBlocks.push(homeItem[0]);
				$rootScope.breadcrumbBlocks.reverse();
				GetPrevCategory();
				GetNextCategory();
				$('#introduction').hide();
				$('#sce-page-navigation').show();
				initSupplierLogo();
			}
		}
	});
	
}

function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    if (arr[i] !== undefined) rv[i] = arr[i];
  return rv;
}

function ContactUsCtrl ($rootScope, $scope, $routeParams, $timeout, $http, $filter, LeadgenForm, URLHandler, Categories) {
	tickValue = URLHandler.getTick();
	$rootScope.homeBlocks = [];
	$rootScope.homeBlocksRows = [];
	$rootScope.ProdItems = [];
	$rootScope.IsHomePage = false;
	Categories.promise.then(function(resultData){
		$rootScope.breadcrumbBlocks = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1)
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
	});
	
	//$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	$scope.ajaxLoading = false;
	//$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		if (LeadgenForm.submit('form1', $scope.submitted) == 1)
			$scope.ajaxLoading = true;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
	
}

moduleSyndicate.run(function ($rootScope, $q, $http, $location, $anchorScroll, $routeParams, Categories, LeadgenForm) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
					if (temp[i] == 'i') {
							currentTick = '&i=' + temp[i + 1];
							break;
					}
			}
	}
	$rootScope.TemplateVersion = 2;//typeof(cspConsumerInfo.lng) != 'undefined' && cspConsumerInfo.lng.toLowerCase() == 'nl' ? 2 : 1;
	$rootScope.IsHomePage = false;
	$rootScope.CategoryItem = [];
	$rootScope.pageTitle = 'Home';
	$rootScope.pageTypes = new Array ('home', 'product', 'contactus');
	$rootScope.AllCategories = [];
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.validFeaturedPageList = true;
	$rootScope.homeBlocks = [];
	$rootScope.homeBlocksRows = [];
	$rootScope.ProdItems = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.nextCategory = [];
	$rootScope.prevCategory = [];
	$rootScope.label = {
		contactus: 'Contact Us',
		ourproducts: 'Onze Producten'
	};
	
	if (typeof(cspConsumerInfo) != 'undefined' && typeof(cspConsumerInfo.lng) != 'undefined' && typeof(OUR_PRODUCTS_TRANSLATION[cspConsumerInfo.lng]) != 'undefined') {
		$rootScope.label.ourproducts = OUR_PRODUCTS_TRANSLATION[cspConsumerInfo.lng];
		jQuery('#csp-wrapper #introduction').text($rootScope.label.ourproducts);
	}
	
	//Translate Next/Prev Product links text
	var prevText = PREV_PRODUCT_TRANSLATION[cspConsumerInfo.lng];
	var nextText = NEXT_PRODUCT_TRANSLATION[cspConsumerInfo.lng];
	if (typeof (PREV_PRODUCT_TRANSLATION[cspConsumerInfo.lng]) == 'undefined') {
		prevText = PREV_PRODUCT_TRANSLATION['NL'];
		nextText = NEXT_PRODUCT_TRANSLATION['NL'];
	}
	$('#sce-page-navigation .prev a').text(prevText);
	$('#sce-page-navigation .next a').text(nextText);
	
	$rootScope.ContactUsItem = [];
	
	$rootScope.FeaturedPageList = GetFeaturedPageList($rootScope.pageTypes);
	
	for (var i = 0; i < $rootScope.pageTypes.length; i++) {
		if ($rootScope.FeaturedPageList[$rootScope.pageTypes[i]] == undefined) {
			$rootScope.validFeaturedPageList = false;
			return;
		}
	}
	
	$rootScope.GetHomeText = function() {
		return typeof(HOME_TEXT_TRANSLATION[cspConsumerInfo.lng]) != 'undefined' ? HOME_TEXT_TRANSLATION[cspConsumerInfo.lng] : '';
	}
	
	$rootScope.GetContactUsText = function () {
		return typeof(CONTACT_US_TRANSLATION[cspConsumerInfo.lng]) != 'undefined' ? CONTACT_US_TRANSLATION[cspConsumerInfo.lng] : CONTACT_US_TRANSLATION['EN'];
	}
	
	$rootScope.GetCategoryUrl = function (block) {
		if (block.categoryId == undefined) return '';
		var currentUrl = document.URL;
		if (block.categoryId == $rootScope.FeaturedPageList['contactus']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		if (block.categoryId == $rootScope.FeaturedPageList['home']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	};
	// reporting
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
			}
		}
		try {
			jQuery(document).attr('title', $rootScope.pageTitle);
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined" && typeof (CspReportLib.wt) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				document.getElementsByTagName('head')[0].appendChild(script);
			}, 1000);
			
			window.setTimeout(function() {
				jQuery(document).click();
			}, 2000);
			
			try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
		} catch (Err) {};
	};	
	
	$rootScope.GetHtmlText = function (rawText) {
		return jQuery('<textarea/>').html(rawText).val();
	}
	
	$rootScope.GetHomeUrl = function () {
	  var currentUrl = document.URL;
	  return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
	}
	
	//Get all categories
	var deferred = $q.defer();
	Categories.promise = deferred.promise.then(function (listCategory) {
		Categories.setData(listCategory);
		try {
			for(var i=0;i<listCategory.length;i++) {
				listCategory[i].description=jQuery("<div />").html(listCategory[i].description).text();
			}
		}
		catch(ex){}
		return listCategory;
	});
	
	var queryCategoryRootId = getRootCategoryId();
	if (queryCategoryRootId == -1) {
		alert ('Invalid root category id');
		return;
	}
	$http.get('d1.aspx?p2003(ct31000&cd' + queryCategoryRootId + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		deferred.resolve(responseData);
	});
	
	var leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
		
		if (data.length <= 1) return null;
		LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
		LeadgenForm.setFields(data[1].contactusformfields);
		
		return data;
	});
	
	$http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		leadgenFormDeferred.resolve(responseData);
	});
	
	$rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
		$location.hash($routeParams.scrollTo);
		setTimeout(function() {
			$anchorScroll(); 
		}, 1500);
	});
});

function GetCurrentLangCode() {
	var crrUrlInfo = location.href.split('/');
	var baseDomain = crrUrlInfo[2];
	var crrLangCode = '';
	var tmp = baseDomain.split('-');
	
	for (var i = 0; i < tmp.length; i++) {
		if (tmp[i].indexOf('l') == 0) {
			crrLangCode = tmp[i].substr(1);
			break;
		}
	}
	
	return crrLangCode != '' ? crrLangCode : cspConsumerInfo.lng.toLowerCase();
};

function GetFeaturedPageList(pageTypes) {
	var pageList = [];
	try {
		cspPageIndicator = cspPageIndicator.replace(new RegExp('\\"', 'g'),'"');
		var featuredPagesList = JSON.parse(cspPageIndicator);
		if (typeof (cspConsumerInfo) == 'undefined') return pageList;
		var crrLangCode = GetCurrentLangCode();
		for (var i = 0; i < featuredPagesList.length; i++) {
			if ( (isNaN(crrLangCode) && featuredPagesList[i].LangCode.toLowerCase() == crrLangCode)
			|| (!isNaN(crrLangCode) && featuredPagesList[i].LangId == crrLangCode)
				) {
				pageList[pageTypes[0]] = featuredPagesList[i].HomeId;
				pageList[pageTypes[1]] = featuredPagesList[i].ProductId;
				pageList[pageTypes[2]] = featuredPagesList[i].ContactUsId;
			}
		}
	}
	catch (exc) {
		var splitPattern = '_' + cspRootCategoryId + '$';
		if (cspPageIndicator.indexOf('$') < 0)
			cspPageIndicator += splitPattern;
		if (cspPageIndicator.indexOf(splitPattern) < 0) return null;
				
		var mixedInfo = cspPageIndicator.split(splitPattern)[0];
		if (mixedInfo.indexOf('$')) {
			var tmp = mixedInfo.split('$');
			mixedInfo = tmp.pop();
		}
		mixedInfo = mixedInfo.split(';');
		
		for (var i = 0; i < mixedInfo.length; i++) {
			var temp = mixedInfo[i].split('-');
			try {
				pageList[pageTypes[temp[0]]] = temp[1];
			}
			catch (exc) {
				return null;
			}
		}
	}
	return pageList;
}

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function getRootCategoryId() {
	if (cspRootCategoryId != '') {
	  return parseInt(cspRootCategoryId);
	 }
	 for (var qKey in QueryString) {
			if (QueryString[qKey] == undefined) continue;
	  if (QueryString[qKey].indexOf('rcategoryid'))
	   return QueryString[qKey];
		}
	 return -1;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}