var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;
var SYNDICATION_TYPE = 'showcase';
var VALID_SYNDICATION_TYPES = ['', SYNDICATION_TYPE, 'all', 'hidden'];
var FIRST_LOAD = false;
var countMainNavLiLoad = 1;
var CT_INJECTED_BLOCK = 72000;
var countryList = {
	'en': 'United States#$#Canada#$#Afghanistan#$#Aland Islands#$#Albania#$#Algeria#$#American Samoa#$#Andorra#$#Angola#$#Anguilla#$#Antarctica#$#Antigua and Barbuda#$#Argentina#$#Armenia#$#Aruba#$#Australia#$#Austria#$#Azerbaijan#$#Bahamas#$#Bahrain#$#Bangladesh#$#Barbados#$#Belarus#$#Belgium#$#Belize#$#Benin#$#Bermuda#$#Bhutan#$#Bolivia, Plurinational State of#$#Bosnia and Herzegovina#$#Botswana#$#Bouvet Island#$#Brazil#$#British Indian Ocean Territory#$#Brunei Darussalam#$#Bulgaria#$#Burkina Faso#$#Burundi#$#Cambodia#$#Cameroon#$#Cape Verde#$#Cayman Islands#$#Central African Republic#$#Chad#$#Chile#$#China#$#Christmas Island#$#Cocos (Keeling) Islands#$#Colombia#$#Comoros#$#Congo#$#The Democratic Republic of Congo#$#Cook Islands#$#Costa Rica#$#Côte D&amp;#039;Ivoire#$#Croatia#$#Cuba#$#Cyprus#$#Czech Republic#$#Denmark#$#Djibouti#$#Dominica#$#Dominican Republic#$#Ecuador#$#Egypt#$#El Salvador#$#Equatorial Guinea#$#Eritrea#$#Estonia#$#Ethiopia#$#Falkland Islands (Malvinas)#$#Faroe Islands#$#Fiji#$#Finland#$#France#$#French Guiana#$#French Polynesia#$#French Southern Territories#$#Gabon#$#Gambia#$#Georgia#$#Germany#$#Ghana#$#Gibraltar#$#Greece#$#Greenland#$#Grenada#$#Guadeloupe#$#Guam#$#Guatemala#$#Guernsey#$#Guinea#$#Guinea-Bissau#$#Guyana#$#Haiti#$#Heard Island and McDonald Islands#$#Honduras#$#Hong Kong#$#Hungary#$#Iceland#$#India#$#Indonesia#$#Iran Islamic Republic of#$#Iraq#$#Ireland#$#Isle of Man#$#Israel#$#Italy#$#Jamaica#$#Japan#$#Jersey#$#Jordan#$#Kazakhstan#$#Kenya#$#Kiribati#$#Korea Democratic People&amp;#039;s Republic of#$#Korea Republic of#$#Kuwait#$#Kyrgyzstan#$#Lao People&amp;#039;s Democratic Republic#$#Latvia#$#Lebanon#$#Lesotho#$#Liberia#$#Libyan Arab Jamahiriya#$#Liechtenstein#$#Lithuania#$#Luxembourg#$#Macao#$#Macedonia The Former Yugoslav Republic of#$#Madagascar#$#Malawi#$#Malaysia#$#Maldives#$#Mali#$#Malta#$#Marshall Islands#$#Martinique#$#Mauritania#$#Mauritius#$#Mayotte#$#Mexico#$#Micronesia Federated States of#$#Moldova Republic of#$#Monaco#$#Mongolia#$#Montenegro#$#Montserrat#$#Morocco#$#Mozambique#$#Myanmar#$#Namibia#$#Nauru#$#Nepal#$#Netherlands#$#Netherlands Antilles#$#New Caledonia#$#New Zealand#$#Nicaragua#$#Niger#$#Nigeria#$#Niue#$#Norfolk Island#$#Northern Mariana Islands#$#Norway#$#Oman#$#Pakistan#$#Palau#$#Palestinian Territory Occupied#$#Panama#$#Papua New Guinea#$#Paraguay#$#Peru#$#Philippines#$#Pitcairn#$#Poland#$#Portugal#$#Puerto Rico#$#Qatar#$#Reunion#$#Romania#$#Russian Federation#$#Rwanda#$#Saint Barthelemy#$#Saint Helena#$#Saint Kitts and Nevis#$#Saint Lucia#$#Saint Martin#$#Saint Pierre and Miquelon#$#Saint Vincent and The Grenadines#$#Samoa#$#San Marino#$#Sao Tome and Principe#$#Saudi Arabia#$#Senegal#$#Serbia#$#Seychelles#$#Sierra Leone#$#Singapore#$#Slovakia#$#Slovenia#$#Solomon Islands#$#Somalia#$#South Africa#$#South Georgia and the South Sandwich Islands#$#Spain#$#Sri Lanka#$#Sudan#$#Suriname#$#Svalbard and Jan Mayen#$#Swaziland#$#Sweden#$#Switzerland#$#Syrian Arab Republic#$#Taiwan, Province of China#$#Tajikistan#$#Tanzania United Republic of#$#Thailand#$#Timor-Leste#$#Togo#$#Tokelau#$#Tonga#$#Trinidad and Tobago#$#Tunisia#$#Turkey#$#Turkmenistan#$#Turks and Caicos Islands#$#Tuvalu#$#Uganda#$#Ukraine#$#United Arab Emirates#$#United Kingdom#$#United States Minor Outlying Islands#$#Uruguay#$#Uzbekistan#$#Vanuatu#$#Vatican City State (Holy See)#$#Venezuela#$#VietNam#$#Virgin Islands British#$#Virgin Islands US#$#Wallis and Futuna#$#Western Sahara#$#Yemen#$#Zambia#$#Zimbabwe',
	'es': 'Estados Unidos#$#Canadá#$#Afganistán#$#Islas Aland#$#Albania#$#Argelia#$#Samoa Americana#$#Andorra#$#Angola#$#Anguila#$#Antártida#$#Antigua y Barbuda#$#Argentina#$#Armenia#$#Aruba#$#Australia#$#Austria#$#Azerbaiyán#$#Bahamas#$#Baréin#$#Bangladesh#$#Barbados#$#Bielorrusia#$#Bélgica#$#Belice#$#Benín#$#Bermuda#$#Bután#$#Bolivia, Estado Plurinacional de#$#Bosnia y Herzegovina#$#Botsuana#$#Isla Bouvet#$#Brasil#$#Territorio Británico del Océano Índico#$#Brunei Darussalam#$#Bulgaria#$#Burkina Faso#$#Burundi#$#Camboya#$#Camerún#$#Cabo Verde#$#Islas Caimán#$#República Centroafricana#$#Chad#$#Chile#$#China#$#Isla de Navidad#$#Islas Cocos (Keeling)#$#Colombia#$#Comoras#$#Congo#$#República Democrática del Congo#$#Islas Cook#$#Costa Rica#$#Costa de Marfil#$#Croacia#$#Cuba#$#Chipre#$#República Checa#$#Dinamarca#$#Yibuti#$#Dominica#$#República Dominicana#$#Ecuador#$#Egipto#$#El Salvador#$#Guinea Ecuatorial#$#Eritrea#$#Estonia#$#Etiopía#$#Islas Falkland (Malvinas)#$#Islas Faroe#$#Fiji#$#Finlandia#$#Francia#$#Guayana Francesa#$#Polinesia Francesa#$#Territorios Franceses del Sur#$#Gabón#$#Gambia#$#Georgia#$#Alemania#$#Gana#$#Gibraltar#$#Grecia#$#Groenlandia#$#Granada#$#Guadalupe#$#Guam#$#Guatemala#$#Guernsey#$#Guinea#$#Guinea Bisáu#$#Guyana#$#Haití#$#Islas Heard y McDonald#$#Honduras#$#Hong Kong#$#Hungría#$#Islandia#$#India#$#Indonesia#$#República Islámica de Irán#$#Irak#$#Irlanda#$#Isla de Man#$#Israel#$#Italia#$#Jamaica#$#Japón#$#Jersey#$#Jordania#$#Kazajistán#$#Kenia#$#Kiribati#$#República Popular Democrática de Corea#$#República de Corea#$#Kuwait#$#Kirguistán#$#República Democrática Popular Lao#$#Letonia#$#Líbano#$#Lesoto#$#Liberia#$#Jamahiriya Árabe Libia#$#Liechtenstein#$#Lituania#$#Luxemburgo#$#Macao#$#Macedonia, Antigua República Yugoslava de#$#Madagascar#$#Malaui#$#Malasia#$#Maldivas#$#Malí#$#Malta#$#Islas Marshall#$#Martinica#$#Mauritania#$#Mauricio#$#Mayotte#$#México#$#Estados Federados de Micronesia#$#República de Moldavia#$#Mónaco#$#Mongolia#$#Montenegro#$#Montserrat#$#Marruecos#$#Mozambique#$#Birmania#$#Namibia#$#Nauru#$#Nepal#$#Países Bajos#$#Antillas Neerlandesas#$#Nueva Caledonia#$#Nueva Zelanda#$#Nicaragua#$#Níger#$#Nigeria#$#Niue#$#Isla Norfolk#$#Islas Marianas del Norte#$#Noruega#$#Omán#$#Pakistán#$#Palaos#$#Territorios Palestinos Ocupados#$#Panamá#$#Papúa Nueva Guinea#$#Paraguay#$#Perú#$#Filipinas#$#Pitcairn#$#Polonia#$#Portugal#$#Puerto Rico#$#Qatar#$#Reunión#$#Rumania#$#Federación Rusa#$#Ruanda#$#San Bartolomé#$#Santa Helena#$#San Cristóbal y Nieves#$#Santa Lucía#$#San Martín#$#San Pedro y Miquelón#$#San Vicente y las Granadinas#$#Samoa#$#San Marino#$#Santo Tomé y Príncipe#$#Arabia Saudita#$#Senegal#$#Serbia#$#Seychelles#$#Sierra Leona#$#Singapur#$#Eslovaquia#$#Eslovenia#$#Islas Salomón#$#Somalia#$#Sudáfrica#$#Islas Georgias del Sur y Sándwich del Sur#$#España#$#Sri Lanka#$#Sudán#$#Surinam#$#Islas Svalbard y Jan Mayen#$#Suazilandia#$#Suecia#$#Suiza#$#República Árabe Siria#$#Taiwán, Provincia de China#$#Tayikistán#$#República Unida de Tanzania#$#Tailandia#$#Timor Oriental#$#Togo#$#Tokelau#$#Tonga#$#Trinidad y Tobago#$#Túnez#$#Turquía#$#Turkmenistán#$#Islas Turcas y Caicos#$#Tuvalu#$#Uganda#$#Ucrania#$#Emiratos Árabes Unidos#$#Reino Unido#$#Islas Ultramarinas Menores de los Estados Unidos#$#Uruguay#$#Uzbekistán#$#Vanuatu#$#Ciudad del Vaticano (Santa Sede)#$#Venezuela#$#Vietnam#$#Islas Vírgenes Británicas#$#Islas Vírgenes de los EE. UU.#$#Wallis y Futuna#$#Sahara Occidental#$#Yemen#$#Zambia#$#Zimbabue'
}
var jobRoleList = 'Security Analyst#$#Security Architect#$#Security Manager/Director#$#CISO/CSO#$#Compliance Auditor#$#Consultant#$#System Administrator#$#SOC Operator#$#Applications Manager/Director#$#CEO/CFO/COO/CMO#$#CIO/CTO#$#Human Resources#$#IT Manager/Director#$#IT Risk Manager#$#Network Manager/Director#$#Operations Manager/Director#$#Student#$#VP/EVP#$#Other'
var stateUS = 'AK#$#AL#$#AR#$#AZ#$#CA#$#CO#$#CT#$#DC#$#DE#$#FL#$#GA#$#HI#$#IA#$#ID#$#IL#$#IN#$#KS#$#KY#$#LA#$#MA#$#MD#$#ME#$#MI#$#MN#$#MO#$#MS#$#MT#$#NC#$#ND#$#NE#$#NH#$#NJ#$#NM#$#NV#$#NY#$#OH#$#OK#$#OR#$#PA#$#RI#$#SC#$#SD#$#TN#$#TX#$#UT#$#VA#$#VT#$#WA#$#WI#$#WV#$#WY';
var provinceCanada = 'ON#$#QC#$#NS#$#NB#$#MB#$#BC#$#PE#$#SK#$#AB#$#NL';
var interestAreaList = 'Back office efficiency#$#Call center modernization#$#Cloud contact center#$#Collections#$#Customer experience#$#Mobile customer care#$#Mobile marketing#$#Mobility#$#Multi-channel contact center#$#Self-service IVR#$#Social customer service#$#Speech / text analytics#$#Web customer service#$#Workforce optimization & analytics';
var PageWidth = 660;
var frmLeadgenTitleTemplate = '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>'; 
var frmLeadgenTitle = '';
var frmLeadgenTitleSection = '';
var isSvedex = false;
var YouTubePlayers = [];
var YouTubeAPILoaded = false;
if(cspProjectId == "svedex")
	isSvedex = true;
var pageLoadedInterval;
var DEFAULT_HEADER_FIELDS = '';

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get the hostname
    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];

    return hostname;
}

function CspListener (event) {
	try {
		window.removeEventListener("message", listener, false);
		window.removeEventListener("message", listener, true);
	} catch (exc) {}
	try {
		var iFrameNewHeight = 0;
		if (event.data.indexOf('&') == -1 && !isNaN(event.data)) iFrameNewHeight = parseInt(event.data);
		else {
			var queryIframeHeight = getQueryVariable(event.data, 'y');
			if (!isNaN(queryIframeHeight))
				iFrameNewHeight = parseInt(queryIframeHeight);
		}
		if (iFrameNewHeight > 0) {
			jQuery('#workspace').find('iframe').height(iFrameNewHeight);
			jQuery('#workspace').find('iframe').closest('.wgiframe-wrapper').height(iFrameNewHeight);
	}
	} catch (exc) {}
}

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function UpdateUserCookies() {
	jQuery('form .tieValidate').each(function() {
		if (jQuery(this).attr('id') != 'redirect')
			setCookie(jQuery(this).attr('id'), jQuery(this).val());
	});
}

function GetUserInfo() {
	return { firstname: getCookie('firstname'), lastname: getCookie('lastname'), companyname: getCookie('companyname'), email: getCookie('email'),phone: getCookie('phone'), jobtitle: getCookie('jobtitle')};
}

function AutoPopulateUserInfo(formId) {
	if ((formId != undefined && jQuery(formId).length == 0)) return;
	jQuery(formId).find('input.form-control').each(function() {
		var cookieValue = typeof (jQuery(this).attr('id')) != 'undefined' ? getCookie(jQuery(this).attr('id')) : '';
		if (cookieValue != undefined && cookieValue != '') {
			jQuery(formId).find('#' + jQuery(this).attr('id')).val(cookieValue);
		}
	});
}

function ReAdjustMainNav(){
	if (countMainNavLiLoad >= 5) return;
	if (jQuery('#main-nav-left-wrapper li').length == 0) {
		countMainNavLiLoad++;
		setTimeout(function() { ReAdjustMainNav(); }, 1000);
		return;
	}
	
	startReAdjustMainNav();
}

function startReAdjustMainNav() {
	var menuWidth = jQuery('#main-nav-left-wrapper').innerWidth();
	var totalLiWidth = 0;
	var isOverFlow = false;
	var windowWidth = jQuery(window).width();
	
	if (windowWidth <= 530) {
		jQuery('#main-nav-left-wrapper ul:first > li').not('.hidden-contactus').show();
		return;
	}
	
	jQuery('#mn-overflow-items').html('');
	jQuery('#main-nav-left-wrapper ul:first > li').not('.hidden-contactus').each(function() {
		totalLiWidth += jQuery(this).outerWidth();
		if (totalLiWidth > menuWidth) {
			jQuery(this).clone().appendTo('#mn-overflow-items').show();
            jQuery(this).hide();
			isOverFlow = true;
        }
		else {
			jQuery(this).show();
		}
	});
	if (isOverFlow)
		jQuery('#mn-overflow-toggle').show();
};

function onYouTubeIframeAPIReady() { 
	if (!YouTubeAPILoaded) {
		YouTubeAPILoaded = true;
		return;
	}
	jQuery('.ytiframe').each(function(){
		var divId = jQuery(this).attr('id');
		var ytId = jQuery(this).attr('ytid');
		YouTubePlayers[divId] = new YT.Player(divId, {
			height: '352',
			width: '640',
			videoId: ytId,
			playerVars: { 'autoplay': 0, 'controls': 1 },
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
		YouTubePlayers[divId]['firstclick'] = false;
	});
}

function onPlayerReady(event) {
}


function onPlayerStateChange(event) {
	if (event.data < 0 || event.data > 2) return;
	var videoData = event.target.getVideoData();
	if (typeof(CspReportLib) != 'undefined') {
		var olDCSext = CspReportLib.wt.DCSext;
		CspReportLib.wt.DCSext["ConversionShown"] = null;
		CspReportLib.wt.DCSext["ConversionClick"] = 'YouTube';
		CspReportLib.wt.DCSext["ConversionType"] = 'VIDEO';
		CspReportLib.wt.DCSext["ConversionContent"] = videoData.title;
		//CspReportLib.wt.dcsMultiTrack();
		
		switch (event.data) {
			case 0:
				CspReportLib.trackVideoStatus.finish(videoData.title);
				break;
			case 1:
				CspReportLib.trackVideoStatus.start(videoData.title);
				break;
			case 2:
				CspReportLib.trackVideoStatus.stop(videoData.title);
				break;
		}
		CspReportLib.wt.DCSext = olDCSext;
	}
}

var moduleSyndicate = angular.module('ngSyndicate', []).config(function($routeProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contentpageview.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contentpageview.html'}).
		when('/category/:id', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contentpageview.html'}).
		when('/category/:id/pl', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contentpageview.html'}).
		when('/category/:id/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contentpageview.html'}).
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/contactus/title/:titlevalue', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/contactus/title/:titlevalue/formID/:fid', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/contactus/title/:titlevalue/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/contactus/title/:titlevalue/formID/:fid/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/contactusview.html'}).
		when('/categoryform/:id', {controller:CategoryFormCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/categoryformview.html'}).
		when('/categoryform/:id/asseturl/:aurl', {controller:CategoryFormCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate3/categoryformview.html'}).
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i>=0)
					return path.substring(i+9);
			} catch(e) {}
			return '/';
		} });
});

moduleSyndicate.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

moduleSyndicate.directive('myContentpageDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			console.log('loaded');
		}
	};
});

moduleSyndicate.factory('Categories', function ($http) {
	var rootId = getRootCategoryId();
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (listCategory) {
			data = listCategory;
		},
		convertJSONtoTree: function (rawArray) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(rawArray);
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			return roots[0].children;
		}
	}
});

moduleSyndicate.factory('InjectedBlock', function ($http) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (injectedBlockList) {
			data = injectedBlockList;
		}
	}
});

moduleSyndicate.factory('LeadgenForm', function ($rootScope, $http,$routeParams) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var portalUrl = '';
	var supplierInfo = [];
	var exceptionFieldState;
	var exceptionFieldProvince;
	var formHeaderFields = '';
	
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			if (typeof(partnerId)!="undefined"&&partnerId!="") {
				this.fieldsList += '<input value="' + partnerId + '" type="hidden" name="partnerId" />';
			}
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="CSP" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="{{formSubject}}" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			try {
				if (cspProjectId=="salesdemo") {
					this.fieldsList += '<input value="http://dg-demo.tiekinetix.net/portal/en-us/leadmanagement.aspx" type="hidden" name="portalUrl" />';
				}
			}
			catch(exc){}
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			try {
				var wtInstance = new WebTrends();
				wtInstance.dcsFPC();
				if (typeof(wtInstance.WT.vtvs) != 'undefined')
					this.fieldsList += '<input value="' + wtInstance.WT.vtvs + '" type="hidden" name="SessionID" />';
				if (typeof(wtInstance.WT.vtid) != 'undefined')
					this.fieldsList += '<input value="' + wtInstance.WT.vtid + '" type="hidden" name="visitorID" />';
				console.log('session id: ' + wtInstance.WT.vtvs);
				console.log('visitor id: ' + wtInstance.WT.vtid);
			}
			catch(exc){}
			if(typeof(cspPartnerId) != "undefined" && cspPartnerId != "" ){
				this.fieldsList += '<input value="' + cspPartnerId + '" type="hidden" name="CID" />';
			}
			
			if(typeof($rootScope.topicFormID) != "undefined" && $rootScope.topicFormID != "" ){
				this.fieldsList += '<input value="' + $rootScope.topicFormID + '" type="hidden" name="formID" />';
			}
			
			if (typeof(cspProjectId) != 'undefined')
				this.fieldsList += '<input value="' + cspProjectId + '" type="hidden" name="_nosaveprojectname" />';
			
			this.formHeaderFields = this.fieldsList;
			
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">({{formHeaderDescriptionShort}})</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			this.downloadFormHeader = '';
			this.downloadFormDescription = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = typeof(formFields[i].contenttitle) != 'undefined' && formFields[i].contenttitle != undefined ? formFields[i].contenttitle : '';
						this.formHeaderDescriptionShort = typeof(formFields[i].description) != 'undefined' && formFields[i].description != undefined ? formFields[i].description : '';
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = typeof(formFields[i].contenttitle) != 'undefined' && formFields[i].contenttitle != undefined ? formFields[i].contenttitle : '';
						this.downloadFormDescription = typeof(formFields[i].description) != 'undefined' && formFields[i].description != undefined ? formFields[i].description : '';
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].fieldid == 'country') {
								var countryListArr;
								var crLangCode = $rootScope.GetCurrentLangCode();
								if (typeof (countryList[crLangCode]) == 'undefined')
									crLangCode = 'en';
								countryListArr = countryList[crLangCode].split('#$#');
																	
								var selectTag = '<div class="tieContactUsFormField"><select id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50">';
								selectTag += '<option value=""></option>';
								for (var z = 0; z < countryListArr.length; z++) {
									selectTag += '<option value="' + countryListArr[z] + '">' + countryListArr[z] + '</option>';
								}
								selectTag += '</select></div>';
								inputField = fieldLabel + selectTag;
							}
							
							if (formFields[i].fieldid == 'jobrole') {
								var jobRoleListArr = jobRoleList.split('#$#');
								var selectTag = '<select id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50">';
								selectTag += '<option value=""></option>';
								for (var w = 0; w < jobRoleListArr.length; w++) {
									selectTag += '<option value="' + jobRoleListArr[w] + '">' + jobRoleListArr[w] + '</option>';
								}
								selectTag += '</select>';
								inputField = fieldLabel + selectTag;
							}
							
							if (formFields[i].fieldid == 'interestarea') {
								var interestAreaListArr = interestAreaList.split('#$#');
								var selectTag = '<select id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50">';
								selectTag += '<option value=""></option>';
								for (var z = 0; z < interestAreaListArr.length; z++) {
									selectTag += '<option value="' + interestAreaListArr[z] + '">' + interestAreaListArr[z] + '</option>';
								}
								selectTag += '</select>';
								inputField = fieldLabel + selectTag;
							}
							
							if (formFields[i].fieldid == 'state') {
								this.exceptionFieldState = formFields[i];
								continue;
							}
							
							if (formFields[i].fieldid == 'province') {
								this.exceptionFieldProvince = formFields[i];
								continue;
							}
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			//this.fieldsList = this.fieldsList.replace(/{{formSubject}}/g, 'ContactUs');
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			
			//this.downloadFields = this.downloadFields.replace(/{{formSubject}}/g, this.downloadFormHeader);
			//CSP-5260: @ducuytran
			try {
				this.fieldsList = this.fieldsList.replace(/{{formSubject}}/g, 'Contact Us from Showcase');
				this.downloadFields = this.downloadFields.replace(/{{formSubject}}/g, 'Gated Asset from Showcase');
			} catch (exc) {
				this.fieldsList = this.fieldsList.replace(/{{formSubject}}/g, 'ContactUs');
				this.downloadFields = this.downloadFields.replace(/{{formSubject}}/g, this.downloadFormHeader);
			}
			
			//issue CSP-3941 - clear asset form header title to prevent page title overwritten.
			if (typeof(frmLeadgenTitle) != 'undefined' && typeof(frmLeadgenTitleSection) != 'undefined') {
				frmLeadgenTitle = this.downloadFormHeader;
				frmLeadgenTitleSection = frmLeadgenTitleTemplate.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
				
			}
			this.downloadFields = this.downloadFields.replace('<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>', '');
			
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			if (this.downloadFormDescription == '')
				this.downloadFields = this.downloadFields.replace(/\({{formHeaderDescriptionShort}}\)/g, this.downloadFormDescription);
			else
				this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getExceptionField: function (fieldName) {
			var excField = [];
			var excFieldValue = [];
			if (fieldName == 'state') {
				excField = this.exceptionFieldState;
				excFieldValue = stateUS.split('#$#');
			}
			else if (fieldName == 'province') {
				excField = this.exceptionFieldProvince;
				excFieldValue = provinceCanada.split('#$#');
			}
			
			var strField = '';
			var fieldLabel = '';
			var tieValidate = 'tieValidate';
			if (typeof(excField) != 'undefined' && typeof(excField.fieldid) != 'undefined') {
				if (excField.statusvalidation.toLowerCase() != 'yes')
					tieValidate = '';
				fieldLabel = '<div class="tieContactUsFormFieldLabel exceptionFieldLabel"><label for="' + excField.fieldid + '">' + excField.fieldlabel + '</label></div>';
				
				var selectTag = '<select id="' + excField.fieldid + '" name="' + excField.fieldname + '" error="' + excField.fielderrormessage + '" valType="' + excField.fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control exceptionFieldControl" >';
				selectTag += '<option value=""></option>';
				for (var w = 0; w < excFieldValue.length; w++) {
					selectTag += '<option value="' + excFieldValue[w] + '">' + excFieldValue[w] + '</option>';
				}
				selectTag += '</select>';
				strField = fieldLabel + selectTag;
			}
			return strField;
		},
		getFormHeaderFields: function () {
			return this.formHeaderFields;
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();
				if (jQuery(this).prop("tagName") == 'SELECT')
					vValue = jQuery(this).find('option:selected').val();
				
				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}
				
			});
			if (errorMessage == "") {
				if (form.find('#redirect').length > 0 && form.find('#redirect').val() != '') {
					window.open(form.find('#redirect').val());
					jQuery('#redirect').each(function() {
						jQuery(this).val('');
					});
				}
				return true;
			}
			alert(errorMessage);
			return false;
		},
		submit: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			var submitUrl = 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick;
			if ($rootScope.GenesysException && formId == 'frmleadgen')
				submitUrl = 'd1.aspx?p2016(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick;
			if (cspProjectId == 'digitalchannel' && formId == 'frmleadgen')
				submitUrl = 'd1.aspx?p2021(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: submitUrl,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitCategoryForm: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			var submitUrl = 'emailForm?pid=9999';
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: submitUrl,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Categories, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.contactUsItem = [];
	
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.GetNavItemTitle = function (navItem) {
		var itemTitle = jQuery('<div>').html(navItem.value.title).text();
		itemTitle = itemTitle.replace(/<br>/g, '');
		return itemTitle;
	}
	
	/*
	$scope.GetContactUsTitle = function () {
		if ($scope.contactUsItem == undefined || $scope.contactUsItem.length == 0)
			return '';
		return $scope.GetNavItemTitle($scope.contactUsItem);
	}
	*/
	
	$scope.GetCategoryUrl = function (navItem, isRoot) {
		if (isRoot) return '';
		var currentUrl = document.URL;
		if (navItem.value.categoryId == $rootScope.FeaturedPageList['contactus']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		if (navItem.value.categoryId == $rootScope.FeaturedPageList['home']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + navItem.value.categoryId;
	}
	
	$scope.GetToggleValue = function (navItem) {
		if (navItem.children != undefined)
			return 'dropdown';
		return '';
	}
	
	$scope.GetOrderField = function (subitem) {
		if ($rootScope.GenesysException) return subitem.value.title;
		return subitem.value.displayorder;
	}
	
	Categories.promise.then(function(resultData){
		resultData.pop();
		resultData = $filter('filter')(resultData, {syndicationtype: SYNDICATION_TYPE}, function (actual, expected) {
			if (actual == '' || actual.toLowerCase() == 'all' || actual.toLowerCase() == expected.toLowerCase()) return true;
			return false;
		});
		$scope.navitems = Categories.convertJSONtoTree(resultData);
		if ($scope.navitems == undefined) $scope.navitems = [];
		for (var i = 0; i < $scope.navitems.length; i++) {
			if ($scope.navitems[i].value.categoryId == $rootScope.FeaturedPageList.contactus) {
				$scope.contactUsItem = $scope.navitems[i];
				break;
			}
		}
	});
}

function MainCtrl($scope, $rootScope, $http, $routeParams, $filter, $timeout, Categories, URLHandler, LeadgenForm) {
	if (!$rootScope.validFeaturedPageList) return;
	
	tickValue = URLHandler.getTick();
	$scope.ContentPage = '';
	$scope.crrCatId;
	$scope.crrCatItem = [];
	$rootScope.CategoryItem = [];
	$rootScope.homeBlocks = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.leftItem = [];
	$scope.ajaxLoading = true;
	$scope.formHeaderTitle = '';
	$scope.formHeaderDescriptionShort = '';
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.injectedBlock = '';
	$rootScope.fid = '';
	$rootScope.pageLoadedIntervalCount = 1;
	$scope.formCategorySubmitted = false;
	$scope.contactusMessage = '';
	$scope.IsCategoryForm = false;
	$scope.categoryFormContent = '';
	//31-10
	/*
	$scope.$on('$viewContentLoaded', function(){
		pageLoadedInterval = setInterval(function(){ 
			if ($rootScope.pageLoadedIntervalCount >= 5) {
				clearInterval(pageLoadedInterval);
				//jQuery('body').click();
				return;
			}
			$rootScope.pageLoadedIntervalCount++;
			
			var Instanceid = 1;
            var iframeName = window.name;
            if (!iframeName) iframeName = "_opt" + Instanceid;
			window.top.postMessage("x=665&y=" + (jQuery('#sce-wrapper').outerHeight() + 75) + "&id=" + iframeName, "*");
			parent.postMessage("x=665&y=" + (jQuery('#sce-wrapper').outerHeight() + 75) + "&id=" + iframeName, "*");
		}, 4000);
	});
	*/
	var GetBreadCrumb = function (categoryList, crrId, bcArray) {
		if (crrId == 0 || crrId == $rootScope.FeaturedPageList['home']) return;
		var crrItem = $filter('filter')(categoryList, {categoryId: crrId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if (crrItem[0] == undefined || crrItem[0].parentId == 0) return;
		
		bcArray.push(crrItem[0]);
		GetBreadCrumb(categoryList, crrItem[0].parentId, bcArray);
	};
	
	/*
	$scope.GetLeftItemUrl = function (block) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	}
	*/
	
	$scope.submittedCateForm = function (resultMsg) {
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formCategorySubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submitCateForm = function() {
		$scope.formCategorySubmitted = true;
		if ($scope.topicTitle != '' && jQuery('#frm-category-form #topic').length > 0 && jQuery('#frm-category-form #topic').val().indexOf("Topics I'd like to discuss:") != 0)
			jQuery('#frm-category-form #topic').val("Topics I'd like to discuss: " + jQuery('#frm-category-form #topic').val());
		if (LeadgenForm.submitCategoryForm('frm-category-form', $scope.submittedCateForm) == 1) {
			$scope.ajaxLoading = true;
		}
		else
			$scope.formCategorySubmitted = false;
	};
	
	$scope.renderCategoryForm = function (htmlContent) {
		$scope.IsCategoryForm = true;
		$scope.ContentPage = '';
		LeadgenForm.promise.then(function (resultData) {
			var formTitle = '<div class="tieContentFormHeaderTitle">' + $rootScope.CategoryItem.title + '</div>';
			var toEmail = resultData.length == 0 ? '' : resultData[0].supplierinfo[0].emailaddress;
			var hiddenFields = '<input type="hidden" name="subject" value="' + $rootScope.CategoryItem.title + '"/>';
			hiddenFields += '<input type="hidden" name="from" value="noreply@tiekinetix.com" />';
			hiddenFields += '<input type="hidden" name="to" value="' + toEmail + '" />';
			/* if ($scope.assetUrl != '')
				hiddenFields += '<input type="hidden" name="redirect" id="redirect" value="' + $scope.assetUrl + '" />';
			if (responseData.length > 0)
				$scope.fieldList = formTitle + hiddenFields + responseData[0].contentpage; */
				
			$scope.categoryFormContent = formTitle + hiddenFields + htmlContent.html();
		});
		
	}
	
	$scope.initPageContent = function (resultData) {
		var htmlContent = jQuery('<div>').html($scope.ContentPage);
		if (htmlContent.find('div[fieldid]').length > 0 && htmlContent.find('.render-contact-form').length == 0) {
			$scope.ContentPage = '<iframe frameBorder="0" width="660" height="700" src="' + document.URL.substring(0, document.URL.indexOf('#/')) + '#/categoryform/' + $scope.crrCatId + '" />';
			return;
		}
		try {
			htmlContent.find('.column-control-bar').remove();
			htmlContent.find('.render-contact-form').each(function() {
				var formId = jQuery(this).children('div:first').attr('class');
				console.log(formId);
				var formHtml = jQuery(this).html();
				jQuery(this).html('<form class="csp-inline-form" id="' + formId + '">' + formHtml + '</form>');
			});
			htmlContent.find('a:not(.csp-category-deeplink)').each(function() {
				jQuery(this).attr('target', '_blank');
				thisHref = jQuery(this).attr("href");
				if(thisHref != undefined){
					if ($rootScope.CspSSLMode){
						thisHref = thisHref.replace("{base_domain}",document.location.host);
						jQuery(this).attr("href", thisHref);
					}
					else{
						thisHref = thisHref.replace("{base_domain}","");
						jQuery(this).attr("href", thisHref);
					}
				}
			});
			
			
			htmlContent.find('div#main .page-width img').each(function(){ 
				jQuery(this).css('height', '');
			});
			
			
			htmlContent.find('div.bg-dark-blue').each(function() {
				if (!jQuery(this).parent('.columnwraper').hasClass('bg-dark-blue'))
					jQuery(this).parent('.columnwraper').addClass('bg-dark-blue');
			});
			
			htmlContent.find('div.image-text-bottom').each(function() {
				if (jQuery(this).attr('style').indexOf('display: none') == -1) {
					jQuery(this).parent('div.img-plugin-block').addClass('has-text-section');
					jQuery(this).prev().addClass('mobile-hidden');
				}
			});
			
			//margin-right-block
			htmlContent.find('div.margin-right-block').each(function() {
				if (!jQuery(this).parent('.columnwraper').hasClass('full-mobile-width'))
					jQuery(this).parent('.columnwraper').addClass('full-mobile-width')
			});
			
			htmlContent.find('a[href$="pdf"]').each(function(){
				var url=jQuery(this).attr('href');
				var fileName= url.split("/").pop().split('.').shift();
				jQuery(this).attr({cspobj:"REPORT", csptype:"DOWNLOADS", cspenglishvalue:fileName});
			});
			
			//Image Plugin patch
			htmlContent.find('.img-plugin-wrap').each(function() {
				if (jQuery(this).get(0).style.width.indexOf('px') >= 0 ) {
					jQuery(this).css({ 'width': '100%', 'height': 'auto' });
				}
			});
			
			htmlContent.find('.cr-inline-column-content').each(function() {
				jQuery(this).css('opacity', 1);
			});
			
			//JavaScript Plugin
			htmlContent.find('.wgscript-wrapper').each(function() {
				var parentRow = jQuery(this).parents('.columnwraper');
				var totalPlugin = parentRow.find('.cr-inline-column-content').length;
				var jsPlugin = parentRow.find('.cr-inline-column-content[rel="wgscript"]');
				jQuery(this).parents('.cr-inline-column-content[rel="wgscript"]').hide();
				if (jsPlugin.length == totalPlugin)
					parentRow.hide();
				var scriptCode = jQuery(this).find('pre').text();
				try {
					eval(scriptCode);
				} catch (exc) {
					console.log(exc);
				}
			});
			
			htmlContent.find('.youtube-plugin-block iframe').each(function(i) {
				var targetUrl = jQuery(this).attr('src');
				var youtubeId = targetUrl.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
				if (targetUrl.indexOf('&v=') > -1 || targetUrl.indexOf('?v=') > -1) {
					youtubeId = targetUrl.match(/v=([^&#]{5,})/);
				}
				else if (targetUrl.indexOf('/embed/') > -1) {
					youtubeId = targetUrl.split('embed/')[1],
					youtubeId = youtubeId.split('?')[0]; 
					if (targetUrl.indexOf('?') == -1)
						jQuery(this).attr('src', targetUrl + '?enablejsapi=1');
					else
						jQuery(this).attr('src', targetUrl + '&enablejsapi=1');
				}
				jQuery(this).addClass('ytiframe');
				jQuery(this).attr('id', 'ytvid' + i);
				jQuery(this).attr('ytid', youtubeId);
			});
			
			htmlContent.find('iframe.wgiframe-main').each(function(i) {
				var projectDomain = "p" + cspProjectId + "-";
				var projectname = "p" + cspProjectId + "-";
				if ($rootScope.CspSSLMode){
					projectDomain = "https://" + projectDomain;
				}
				else{
					projectDomain = "http://" + projectDomain;
				}
				var targetUrl = jQuery(this).attr('src');
				if(targetUrl.indexOf(projectname) == -1)
					targetUrl = targetUrl.replace("{base_domain}",projectDomain + "{base_domain}");
				if (targetUrl == undefined) return '';
				if (distiResellerID != '')
					targetUrl = targetUrl.replace(/\{Consumer:fdisti_reseller_ID\}/g, distiResellerID);
				targetUrl = targetUrl.replace(/\{Consumer:fCircuit_Terms_and_Conditions\}/g, encodeURIComponent(CircuitTermsConditions));
				targetUrl = targetUrl.replace(/\{Consumer:fcircuit_country_code\}/g, $rootScope.CircuitCountryCode);
				if (targetUrl != undefined && targetUrl.indexOf('{base_domain}')) {
					$rootScope.cspCodeBaseDomainValue = $rootScope.cspCodeBaseDomainValue.replace(/^https?:\/\//,'');
					$rootScope.cspCodeBaseDomainValue = $rootScope.cspCodeBaseDomainValue.replace(/^http?:\/\//,'');
					targetUrl = targetUrl.replace('{base_domain}', $rootScope.cspCodeBaseDomainValue)
					jQuery(this).attr('src', targetUrl);
				}
			});

			
			//Exceptions for Genesys & GenesysDevInt
			if ($rootScope.GenesysException) {
				if ($scope.crrCatId != $rootScope.FeaturedPageList['home'] && $scope.crrCatId != 183) {
					htmlContent.find('a').each(function() {
						/*
						if ($scope.crrCatId == $rootScope.FeaturedPageList['home'] && jQuery(this).parent('div.resource-item').length > 0) {
							var linkWrapper = jQuery(this).parent('div.resource-item');
							var parentEl = linkWrapper.parent('.resources');
							if (parentEl.children('.resource-item').index(linkWrapper) == 1) {
								return;
							}
						}
						*/
						if (!jQuery(this).hasClass('injected-link') && !jQuery(this).hasClass('csp-category-deeplink')) {
							jQuery(this).attr('href', 'javascript: void(0)');
							jQuery(this).removeAttr('target');
							jQuery(this).addClass('killed-link');
						}
					});
				}
				
				htmlContent.find('object').each(function() {
					jQuery(this).attr('width', '460');
					jQuery(this).width(460);
					jQuery(this).find('[name="width"]').attr('value', 460);
				});
				
				htmlContent.find('img').each(function() {
					jQuery(this).addClass('img-responsive');	
				});
				
				if ($scope.crrCatId == $rootScope.FeaturedPageList['home']) {
					htmlContent.find('.resources > .resource-item').each(function(eIndex) {
						if (eIndex == 1) {
							var vidLink = jQuery(this).find('a:first').attr('href');
							jQuery(this).find('a').attr('href', vidLink);
							jQuery(this).find('a').addClass('csp-lightbox');
						}
					});
				}
				
				if (htmlContent.find('#sel-type').length > 0 || htmlContent.find("#sel-topic").length > 0) {
					htmlContent.find('.resource-item').show();
					
					htmlContent.find("#showall").click(function() {
						htmlContent.find('.resource-item').show();
					});
					
					jQuery("body").undelegate("#sel-type", "change").delegate("#sel-type", "change", function(event) {
					//htmlContent.find("#sel-type").change(function() {
						var a = jQuery("#sel-type").val();
						var b = jQuery("#sel-topic").val();
						
						jQuery('.resource-item').hide();
						jQuery('.' + a + '.' + b ).show();
					});
					
					jQuery("body").undelegate("#sel-topic", "change").delegate("#sel-topic", "change", function(event) {
					//htmlContent.find("#sel-topic").change(function() {
						var a = jQuery("#sel-type").val();
						var b = jQuery("#sel-topic").val();
						
						jQuery('.resource-item').hide();
						jQuery('.' + a + '.' + b ).show();
					});
					
					jQuery("body").undelegate("#sel-prod", "change").delegate("#sel-prod", "change", function(event) {
					//htmlContent.find("#sel-prod").change(function() {
						var a = jQuery("#sel-type").val();
						var b = jQuery("#sel-topic").val();
						
						jQuery('.resource-item').hide();
						jQuery('.' + a + '.' + b ).show();
					});
				}
			}
		}
		catch (err) {}
		$scope.ContentPage = htmlContent.html();
		
		setTimeout(function(){
			PageWidth = jQuery('#workspace').width();
			
			jQuery('.full-size').each(function() {
				jQuery(this).find('*').each(function() {
					if (!jQuery(this).hasClass('overlaywrap') && !jQuery(this).hasClass('player'))
						jQuery(this).width('100%');
				});
			});
					 
			jQuery('.csp-category-deeplink').each(function() {
				var cId = jQuery(this).attr('rel');
				if (cId == $rootScope.FeaturedPageList['contactus']) {
					jQuery(this).attr('href', '#/contactus');
					return;
				}
				var cItem = $filter('filter')(resultData, {categoryId: cId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				if (cItem.length == 0) {
					jQuery(this).remove();
					return;
				}
				
				jQuery(this).attr('href', document.URL.substring(0, document.URL.indexOf('#/')) + '#/category/' + jQuery(this).attr('rel'));
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'LINKS');
				jQuery(this).attr('cspenglishvalue', cItem[0].title);	
			});
	
			//youtube track id
			if (YouTubeAPILoaded)
				onYouTubeIframeAPIReady();
			//end youtube
			
			jQuery('.gated-assets').each(function() {
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'DOWNLOADS');
				var url=jQuery(this).attr('gated-asset-link');
				if (url == undefined) return;
				var fileName= url.split("/").pop();
				jQuery(this).attr('cspenglishvalue', fileName);
			});			
			
			jQuery('.imagelink').each(function() {
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'LINKS');
				//jQuery(this).attr('cspenglishvalue', 'imagelink');
			});	
			jQuery('.video-plugin-block .player a').each(function() {
				var video_url = this.href;
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'LINKS');
				jQuery(this).attr('cspenglishvalue', video_url);
			});
			
			jQuery('.video-plugin-block .block-video-thumbnail img').each(function() {
				var img_url = this.href;
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'VIDEO');
				jQuery(this).attr('cspenglishvalue', img_url);
			});
			
			jQuery('#workspace .youtube-plugin-block').each(function() {
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'LINKS');
				jQuery(this).attr('cspenglishvalue', 'youtube_url');
				
			});
			
			jQuery('.youtube-plugin-block iframe').each(function() {
				var youtube_url = this.src;
				jQuery(this).attr('cspobj', 'REPORT');
				jQuery(this).attr('csptype', 'VIDEO');
				jQuery(this).attr('cspenglishvalue', youtube_url);
				
			});
			
			jQuery('.columnwraper').each(function() {
				var rowCols = jQuery(this).children('.cr-inline-column');
				var crrRowWidth = jQuery(this).width();
				var totalColInPixel = 0;
				var totalPixel = 0;
				//crrRowWidth = PageWidth / crrRowWidth * 100;
				if (rowCols.length > 1) {
					rowCols.each(function() {
						if (jQuery(this).get(0).style.width.indexOf('px') >= 0 ) {
							totalColInPixel++;
							totalPixel += parseFloat(jQuery(this).get(0).style.width);
						}
					});
					if (totalPixel > 0) {
						var remainPercentage = 100 - ((totalPixel / crrRowWidth) * 100);
						var newColPercentage = (remainPercentage / (rowCols.length - totalColInPixel));
						rowCols.each(function() {
							if (jQuery(this).get(0).style.width.indexOf('px') == -1 ) {
								jQuery(this).width(newColPercentage + '%');
							}
						});
					}
				}
			});
			
			if (typeof (CircuitAuthorized) == 'undefined' || CircuitAuthorized.toLowerCase() != 'yes'
				|| typeof(distiResellerID) == 'undefined' || distiResellerID == "") {
				jQuery('a.disti-reseller').each(function() {
					jQuery(this).attr('href', 'javascript: void(0)');
				});
			}
			
		},500);
		
		$timeout(function(){
			$scope.ajaxLoading = false;
		},1500);
		
		setTimeout(function() {
			$rootScope.updatePageInfo(null, $scope.crrCatItem[0].title);
			jQuery(document).click();							
		}, 1000);
		
		
	};
	
	Categories.promise.then(function(resultData){
		resultData = $filter('filter')(resultData, {syndicationtype: VALID_SYNDICATION_TYPES}, function (actual, expected) {
			if (expected.indexOf(actual) != -1) return true;
			return false;
		});
		if (typeof($routeParams.id) == "undefined") {
			$scope.crrCatId = $rootScope.FeaturedPageList['home'];
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $scope.crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			jQuery('#sce-left-col').hide();
		}
		else {
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $routeParams.id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$scope.crrCatId = $routeParams.id;
		};
		
		if ($scope.crrCatItem.length == 0) {
			window.location = $rootScope.GetHomeUrl();
			return;
		}
		
		$rootScope.CategoryItem = $scope.crrCatItem[0];
		
		$rootScope.CategoryItem.title = $rootScope.GetInnerText($rootScope.CategoryItem.title);
		
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1)
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
			
		$http.get('d1.aspx?p2013(ct66000&cd' + $scope.crrCatId + ')[]' + URLHandler.getUrlTick()).success(function(responseData) {
			if (responseData.length == 0) return;
			
			var htmlContent = '';
			if (responseData.length > 0)
				htmlContent = jQuery('<div>').html(responseData[0].contentpage);
			if (htmlContent.find('div[fieldid]').length > 0 && htmlContent.find('.render-contact-form').length == 0) {
				$scope.renderCategoryForm(htmlContent);
				//$scope.ContentPage = '<iframe frameBorder="0" width="660" height="700" src="' + document.URL.substring(0, document.URL.indexOf('#/')) + '#/categoryform/' + $scope.crrCatId + '" />';
				//return;
			}
			else {
				if (responseData.length > 0)
					$scope.ContentPage = responseData[0].contentpage;
				if ($rootScope.CspSSLMode)
					$scope.ContentPage = $scope.ContentPage.replace(/http:\/\//ig, 'https://');
				$http.get('d2.aspx?p2013(ct' + CT_INJECTED_BLOCK + '&cd' + $scope.crrCatId + ')[st(ct' + CT_INJECTED_BLOCK + '*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(rawInjectedBlock) {
					injectedBlock = JSON.parse($rootScope.ParseInjectedData(rawInjectedBlock));
					if (injectedBlock.length > 0 && injectedBlock[0].Content_HTML != undefined) {
						for (var j = 0; j < injectedBlock.length; j++) {
							$scope.injectedBlock += injectedBlock[j].Content_HTML != undefined ? '<span class="asset-related" asset-related-id="' + injectedBlock[j].Asset_Related_ID + '">' + injectedBlock[j].Content_HTML + '</span>' : '';
						}
						if ($rootScope.CspSSLMode)
							$scope.injectedBlock = $scope.injectedBlock.replace(/http:\/\//ig, 'https://');
						if ($scope.injectedBlock != '') {
							var htmlContent = jQuery('<div>').html($scope.injectedBlock);
							htmlContent.find('a').each(function() {
								jQuery(this).addClass('injected-link');
								var closestSpan = jQuery(this).closest("span").attr("asset-related-id");
								if(jQuery(this).hasClass('gated-assets')){
									jQuery(this).parent("div").attr("asset-related-id", closestSpan);
								}
							});
							
							$scope.injectedBlock = htmlContent.html();
						}
					}
					$scope.ContentPage = $scope.injectedBlock + $scope.ContentPage;
					$scope.initPageContent(resultData);
				});
			}
		});
		
		$rootScope.breadcrumbBlocks.push($scope.crrCatItem[0]);
		GetBreadCrumb(resultData, $scope.crrCatItem[0].parentId, $rootScope.breadcrumbBlocks);
		var homeItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if (homeItem.length > 0)
			$rootScope.breadcrumbBlocks.push(homeItem[0]);
		$rootScope.breadcrumbBlocks.reverse();
		
		$rootScope.leftItems = $filter('filter')(resultData, {parentId: $scope.crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		
		if ($rootScope.leftItems.length == 0 && $scope.crrCatId != $rootScope.FeaturedPageList['home'] && $scope.crrCatId != $rootScope.StagingHomeId)
			$rootScope.leftItems = $filter('filter')(resultData, {parentId: $scope.crrCatItem[0].parentId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		
		if (($scope.crrCatId == $rootScope.FeaturedPageList['home'] || $scope.crrCatId == $rootScope.StagingHomeId) && cspProjectId.indexOf('digium') == -1) {
			$rootScope.breadcrumbBlocks = [];
			$rootScope.leftItems = [];
			jQuery('#sce-left-col').hide();
		}
		
		if (cspProjectId.indexOf('digium') > -1 && $scope.crrCatId == $rootScope.FeaturedPageList['home']) {
			$rootScope.breadcrumbBlocks = [];
			jQuery('#sce-left-col').show();
		}
		
		$rootScope.crrCatId = $scope.crrCatId;
		
		if ($rootScope.CategoryItem.syndicationtype != 'hidden') {
			$rootScope.leftItems = $filter('filter')($rootScope.leftItems, {syndicationtype: 'hidden'}, function (expected, actual) {
				return expected != actual;
			});
		}
		else
			$rootScope.leftItems = [];
		
		for (var i = 0; i < $rootScope.leftItems.length; i++) {
			var subItems = $filter('filter')(resultData, {parentId: $rootScope.leftItems[i].categoryId}, function (expected, actual) {
				return parseInt(expected) == parseInt(actual);
			});
			
			subItems = $filter('filter')(subItems, {syndicationtype: 'hidden'}, function (expected, actual) {
				return expected != actual;
			});
			
			if (subItems.length > 0) {
				$rootScope.leftItems[i].children = subItems;
				$rootScope.leftItems[i].showchildren = false;
			}
		}
		//console.log($rootScope.CategoryItem, $rootScope.FeaturedPageList);
		if ($scope.crrCatId != $rootScope.FeaturedPageList['home'] && $scope.crrCatId != $rootScope.StagingHomeId && $rootScope.leftItems.length > 0) {
			jQuery('#sce-left-col').show();
			jQuery('#workspace').addClass('col-md-8');
			jQuery('#workspace').addClass('block-content');
		}
		if(cspProjectId == "digitalchannel" || cspProjectId == "unify"){
			jQuery('#sce-left-col').hide();
			jQuery('#workspace').css('width','100%');
		}
	});
	
	$scope.submitted = function (resultMsg) {
		try {
			if (typeof (cspConsumerInfo) != "undefined" && cspConsumerInfo.companyname.match(/^test_/i)) {
				if (typeof (csp_report_variables) != "undefined" && csp_report_variables == cspConsumerInfo.companies_Id) {
					//return;
				}
				else {
					jQuery.colorbox.close();
					return;
				}
			}
			if (typeof(CspReportLib) != 'undefined') {
				var cContent = jQuery('#submitbutton').attr('cspenglishvalue');
				if (jQuery('#frmleadgen #redirect').length > 0) {
					cContent = jQuery('#frmleadgen #redirect').val().split('/').pop();
				}
				var olDCSext = CspReportLib.wt.DCSext;
				CspReportLib.wt.DCSext["ConversionShown"] = null;
				CspReportLib.wt.DCSext["ConversionClick"] = cContent;
				CspReportLib.wt.DCSext["ConversionType"] = jQuery('#field-list').find('input[name="subject"]').length > 0 ? jQuery('#field-list').find('input[name="subject"]').val() : "View Asset Authorization Form";
				CspReportLib.wt.DCSext["ConversionContent"] = cContent;//jQuery('#submitbutton').attr('cspenglishvalue');
				CspReportLib.wt.dcsMultiTrack();
				CspReportLib.wt.DCSext = olDCSext;
			}
			jQuery.colorbox.close();
		} catch(err) {}
		
	};
	
	$scope.circuitSubmitted = function (resultMsg) {
		try {
			if (typeof (cspConsumerInfo) != "undefined" && cspConsumerInfo.companyname.match(/^test_/i)) {
				if (typeof (csp_report_variables) != "undefined" && csp_report_variables == cspConsumerInfo.companies_Id) {
					//return;
				}
				return;
			}
			if (typeof(CspReportLib) != 'undefined') {
				var olDCSext = CspReportLib.wt.DCSext;
				CspReportLib.wt.DCSext["ConversionShown"] = null;
				CspReportLib.wt.DCSext["ConversionClick"] = cContent;
				CspReportLib.wt.DCSext["ConversionType"] = jQuery('#field-list').find('input[name="subject"]').length > 0 ? jQuery('#field-list').find('input[name="subject"]').val() : "View Circuit Authorization Form";
				CspReportLib.wt.DCSext["ConversionContent"] = cContent;//jQuery('#submitbutton').attr('cspenglishvalue');
				CspReportLib.wt.dcsMultiTrack();
				CspReportLib.wt.DCSext = olDCSext;
			}
			
		} catch(err) {} finally {
			jQuery.colorbox.close();
		}
		
	};

	$scope.submit = function() {
		LeadgenForm.submit('frmleadgen', $scope.submitted);
	};
	
	$scope.submitCircuit = function() {
		LeadgenForm.submit('frmCircuit', $scope.circuitSubmitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getDownloadFormFields();
		if ($scope.fieldList.indexOf('id="downloadedasset"') == -1) {
			$scope.fieldList += '<input value="" type="hidden" name="downloadedasset" id="downloadedasset" />';
		}
		$scope.fieldListCircuit = LeadgenForm.getAllFields();
		
		var toEmail = resultData.length == 0 ? '' : resultData[0].supplierinfo[0].emailaddress;
		var hiddenFields = '';
		hiddenFields += '<input type="hidden" name="from" value="no-reply@' + cspProjectId + '.tiekinetix.com" />';
		hiddenFields += '<input type="hidden" name="to" value="' + toEmail + '" />';
		DEFAULT_HEADER_FIELDS = LeadgenForm.getFormHeaderFields().replace('{{formSubject}}', 'Gated Asset from Showcase');
		DEFAULT_HEADER_FIELDS = DEFAULT_HEADER_FIELDS.replace(/name="/g, 'name="csphidden_');
		DEFAULT_HEADER_FIELDS = DEFAULT_HEADER_FIELDS.replace(/csphidden__nosave/g, '_nosave');	
		DEFAULT_HEADER_FIELDS = DEFAULT_HEADER_FIELDS.replace(/csphidden_subject/g, 'subject');
		DEFAULT_HEADER_FIELDS += hiddenFields;
		
		setTimeout(function() {
			
			//Tenable exception form fields
			jQuery(document).on('change', 'select#country', function(e) {
				var selectedCountry = jQuery(this).find('option:selected').val(); 
				var exceptionField = '';
				if (selectedCountry == 'United States' || selectedCountry == 'Estados Unidos')
					exceptionField = LeadgenForm.getExceptionField('state');
				else if (selectedCountry == 'Canada' || selectedCountry == 'Canadá' )
					exceptionField = LeadgenForm.getExceptionField('province');
				jQuery('.exceptionFieldLabel').remove();
				jQuery('.exceptionFieldControl').remove();
				if (exceptionField != '') {
					jQuery(this).after(exceptionField);
					jQuery.colorbox.resize();
				}
			});
		}, 1000);
	});
}

function CategoryFormCtrl($scope, $rootScope, $http, $routeParams, $filter, $timeout, Categories, URLHandler, LeadgenForm) {
	if (!$rootScope.validFeaturedPageList) return;
	
	jQuery('#main-nav-wrapper').hide();
	jQuery('#footer').hide();
	jQuery('#sce-left-col').hide();
	
	tickValue = URLHandler.getTick();
	$scope.crrCatId;
	$scope.crrCatItem = [];
	$rootScope.CategoryItem = [];
	$rootScope.homeBlocks = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.leftItem = [];
	$scope.ajaxLoading = true;
	$scope.formHeaderTitle = '';
	$scope.formHeaderDescriptionShort = '';
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.injectedBlock = '';
	$rootScope.fid = '';
	$rootScope.pageLoadedIntervalCount = 1;
	$scope.assetUrl = typeof(getCookie('asseturl')) == "undefined" ? '' : getCookie('asseturl');
	
	jQuery('#main-nav-wrapper').hide();
	jQuery('#footer').hide();
	jQuery('#sce-left-col').hide();
	jQuery('#csp-view').css('padding-top', '0px');
		
	Categories.promise.then(function(resultData){
		jQuery('#main-nav-wrapper').hide();
		jQuery('#footer').hide();
		jQuery('#sce-left-col').hide();
		jQuery('#csp-view').css('padding-top', '0px');
		resultData = $filter('filter')(resultData, {syndicationtype: VALID_SYNDICATION_TYPES}, function (actual, expected) {
			if (expected.indexOf(actual) != -1) return true;
			return false;
		});
		if (typeof($routeParams.id) == "undefined") {
			$scope.crrCatId = $rootScope.FeaturedPageList['home'];
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $scope.crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			jQuery('#sce-left-col').hide();
		}
		else {
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $routeParams.id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$scope.crrCatId = $routeParams.id;
		};
		
		if ($scope.crrCatItem.length == 0) {
			window.location = $rootScope.GetHomeUrl();
			return;
		}
		
		$rootScope.CategoryItem = $scope.crrCatItem[0];
		
		$rootScope.CategoryItem.title = $rootScope.GetInnerText($rootScope.CategoryItem.title);
		
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1)
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
		
		LeadgenForm.promise.then(function (resultData) {
			//if (resultData.length == 0) return;
			$http.get('d1.aspx?p2013(ct66000&cd' + $scope.crrCatId + ')[]' + URLHandler.getUrlTick()).success(function(responseData) {
				var formTitle = '<div class="tieContentFormHeaderTitle">' + $rootScope.CategoryItem.title + '</div>';
				var toEmail = resultData.length == 0 ? '' : resultData[0].supplierinfo[0].emailaddress;
				var hiddenFields = '<input type="hidden" name="subject" value="' + $rootScope.CategoryItem.title + '"/>';
				hiddenFields += '<input type="hidden" name="from" value="noreply@tiekinetix.com" />';
				hiddenFields += '<input type="hidden" name="to" value="' + toEmail + '" />';
				if ($scope.assetUrl != '')
					hiddenFields += '<input type="hidden" name="redirect" id="redirect" value="' + $scope.assetUrl + '" />';
				if (responseData.length > 0)
					$scope.fieldList = formTitle + hiddenFields + responseData[0].contentpage;
				
			});
		});
	});
	
	$scope.submitted = function (resultMsg) {
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		$scope.formSubmitted = true;
		if ($scope.topicTitle != '' && jQuery('#form1 #topic').length > 0 && jQuery('#form1 #topic').val().indexOf("Topics I'd like to discuss:") != 0)
			jQuery('#form1 #topic').val("Topics I'd like to discuss: " + jQuery('#form1 #topic').val());
		if (LeadgenForm.submitCategoryForm('form1', $scope.submitted) == 1) {
			$scope.ajaxLoading = true;
		}
		else
			$scope.formSubmitted = false;
	};
}

function ContactUsCtrl ($rootScope, $scope, $routeParams, $timeout, $http, $filter, LeadgenForm, URLHandler, Categories) {
	tickValue = URLHandler.getTick();
	//$rootScope.updatePageInfo(this);
	$rootScope.homeBlocks = [];
	$rootScope.leftItems = [];
	jQuery('#sce-left-col').hide();
	$rootScope.breadcrumbBlocks = [];
	$rootScope.topicFormID = typeof($routeParams.fid) == "undefined" ? '' : $routeParams.fid;
	$rootScope.IsSvedex = false;
	if(cspProjectId == "svedex")
		$rootScope.IsSvedex = true;
	//get googleTagId
	$scope.googleUrl = function(){
		 if (googleTagId == '' ) return '';
		 else 
			 return '//www.googletagmanager.com/ns.html?id=' + googleTagId;
	};
	Categories.promise.then(function(resultData){
		$rootScope.breadcrumbBlocks = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1) {
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
			$rootScope.breadcrumbBlocks.push($rootScope.ContactUsItem);
		}
	});
	
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	
	$scope.ajaxLoading = false;
	//$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		$scope.formSubmitted = true;
		if ($scope.topicTitle != '' && jQuery('#form1 #topic').length > 0 && jQuery('#form1 #topic').val().indexOf("Topics I'd like to discuss:") != 0)
			jQuery('#form1 #topic').val("Topics I'd like to discuss: " + jQuery('#form1 #topic').val());
		if (LeadgenForm.submit('form1', $scope.submitted) == 1) {
			$scope.ajaxLoading = true;
		}
		else
			$scope.formSubmitted = false;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		if (resultData.length == 0) return;
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		$rootScope.updatePageInfo(this);
		
		setTimeout(function() {
			jQuery('#field-list').before(jQuery('.tieContentFormHeader'));
		}, 100);
		
		setTimeout(function() {
			AutoPopulateUserInfo('#form1');
		}, 1000);
		
		
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();
			
			//Tenable exception form fields
			jQuery(document).on('change', 'select#country', function(e) {
				var selectedCountry = jQuery(this).find('option:selected').val(); 
				var exceptionField = '';
				if (selectedCountry == 'United States' || selectedCountry == 'Estados Unidos')
					exceptionField = LeadgenForm.getExceptionField('state');
				else if (selectedCountry == 'Canada' || selectedCountry == 'Canadá' )
					exceptionField = LeadgenForm.getExceptionField('province');
				jQuery('.exceptionFieldLabel').remove();
				jQuery('.exceptionFieldControl').remove();
				if (exceptionField != '')
					jQuery(this).parent('div').after(exceptionField);
			});
		}, 1000);
	});
	
}

moduleSyndicate.run(function ($rootScope, $q, $http, Categories, InjectedBlock, LeadgenForm) {
	var currentUrl = document.URL;

	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
					if (temp[i] == 'i') {
							currentTick = '&i=' + temp[i + 1];
							break;
					}
			}
	}

	//Init YouTube API
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	
	if (window.addEventListener) {
		console.log('run first');
		window.addEventListener("message", CspListener, false);
	} else {
		detachEvent("onmessage", CspListener);
	}
	$rootScope.pageLoadedIntervalCount = 1;			
	$rootScope.RootCategoryId = getRootCategoryId();
	$rootScope.crrCatId;
	$rootScope.CategoryItem = [];
	$rootScope.pageTitle = 'Home';
	$rootScope.pageTypes = new Array ('home', 'product', 'contactus');
	$rootScope.AllCategories = [];
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? window.location.protocol + '//' + document.domain : document.domain;
	$rootScope.cspCodeBaseDomainValue = $rootScope.baseDomain.split('-').pop();
	$rootScope.validFeaturedPageList = true;
	$rootScope.homeBlocks = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.label = {
		contactus: 'Contact Us',
		requestdemo: 'Request a Demo'
	};
	$rootScope.ContactUsItem = [];
	$rootScope.SupplierLogo = cspSupplierLogo;
	$rootScope.homepageContent = '';
	$rootScope.CopyrightYear = new Date().getFullYear();
	$rootScope.InjectedBlockEnable = false;
	$rootScope.CircuitCountryCode = 'US';
	if (typeof(CircuitTermsConditions) == 'undefined' || CircuitTermsConditions == '')
		CircuitTermsConditions = 'https://www.unify.com/us/Home/Internet/web/us/misc/tac/terms_of_use.aspx?tac=1';
	if (typeof(CircuitCountryCode) != 'undefined' && CircuitCountryCode != '')
		$rootScope.CircuitCountryCode = CircuitCountryCode;
	//Detect Genesys prj
	$rootScope.GenesysException = false;
	if (typeof(cspProjectId) != "undefined" && cspProjectId!=null && cspProjectId!="")
		$rootScope.GenesysException = cspProjectId.toLowerCase() == 'genesys' || cspProjectId.toLowerCase() == 'genesysdevint' || cspProjectId.toLowerCase() == 'dcs222c8bn4jv4l3so45kwjnh_3n5j';
	$rootScope.CspSSLMode = false;
	try {
		$rootScope.CspSSLMode = typeof(cspSSLMode) == 'undefined' ? false : cspSSLMode.toLowerCase() == 'yes';
		$rootScope.FaceBookUrl = typeof cspFaceBook != 'undefined' ? cspFaceBook : '';
		$rootScope.TwitterUrl = typeof cspTwitter != 'undefined' ? cspTwitter : '';
		$rootScope.LinkedInUrl = typeof cspLinkedIn != 'undefined' ? cspLinkedIn : '';
		
		if ($rootScope.CspSSLMode) {
			$rootScope.FaceBookUrl = $rootScope.FaceBookUrl.replace('http://', 'https://');
			$rootScope.TwitterUrl = $rootScope.TwitterUrl.replace('http://', 'https://');
			$rootScope.LinkedInUrl = $rootScope.LinkedInUrl.replace('http://', 'https://');
			if (cspSupplierLogo.indexOf('http') != 0)
				cspSupplierLogo = 'https://' + cspSupplierLogo;
			else
				cspSupplierLogo = cspSupplierLogo.replace('http://', 'https://');
		}
		jQuery('#sce-logo-img').attr('src', cspSupplierLogo);
	} catch(err) {
		$rootScope.FaceBookUrl = '';
		$rootScope.TwitterUrl = '';
		$rootScope.LinkedInUrl = '';
	}
	
	$rootScope.FeaturedPageList = GetFeaturedPageList($rootScope.pageTypes);
	$rootScope.StagingHomeId = 0;
	if (typeof(cspStagingHomeId) != 'undefined')
		$rootScope.StagingHomeId = parseInt(cspStagingHomeId)|| 0;
	
	$rootScope.GetHomeUrl = function () {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
	}
	
	$rootScope.GetContactUsUrl = function () {
		var currentUrl = document.URL;
		
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
	}
	
	$rootScope.GetFormID= function() {
		var formID = jQuery(".asset-related").attr("asset-related-id");
		if(formID != '')
			return '/formID/' + formID;
		return '';
	}
	
	$rootScope.GetContactUsTitle = function () {
		if ($rootScope.ContactUsItem == undefined || $rootScope.ContactUsItem == null || $rootScope.ContactUsItem.length == 0)
			return $rootScope.label.contactus;
		return $rootScope.GetInnerText($rootScope.ContactUsItem.title);
	}
	
	$rootScope.GetLeftColContactUsTitle = function () {
		if ($rootScope.GenesysException)
			return $rootScope.label.requestdemo;
		if ($rootScope.ContactUsItem == undefined || $rootScope.ContactUsItem == null || $rootScope.ContactUsItem.length == 0)
			return $rootScope.label.contactus;
		return $rootScope.GetInnerText($rootScope.ContactUsItem.title);
	}
	
	$rootScope.GetCategoryCspUniqueKey = function (catItem) {
		if (catItem.cspuniquekey == undefined) return '';
		var cspUKey = catItem.cspuniquekey.toLowerCase();
		cspUKey = cspUKey.split(' ').join('');
		return cspUKey;
	}
	
	$rootScope.GetCategoryId = function (catItem) {
		if (catItem.categoryId == undefined) return '';
		return catItem.categoryId;
	}
	
	$rootScope.LeftMenuOrderBy = function (leftitem) {
		if ($rootScope.GenesysException) return leftitem.title;
		return leftitem.displayorder;
	}
	
	var GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	}
	
	$rootScope.ParseInjectedData = function(rawList) {
		var ctId = CT_INJECTED_BLOCK;
		var fieldSplit = GetFieldSplitNo(ctId, rawList);
		if (fieldSplit == 0) return '[]';
		var fieldList = ['CSP_Unique_Key', 'Content_HTML', 'Status_Sort_Order', 'Asset_Related_ID'];
		var fieldDic = {
			"CSP_Unique_Key"			: "CSP_Unique_Key",		
			"Content_HTML"				: "Content_HTML",	
			"Status_Sort_Order"			: "Status_Sort_Order",
			"Asset_Related_ID"			: "Asset_Related_ID"
		}
		var jsonInjectedList = "[";
		
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var injectedItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					if (fieldList.indexOf(property) >= 0) {
						if (injectedItem != '{') injectedItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						injectedItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			injectedItem += "}";
			if (jsonInjectedList != '[') jsonInjectedList += ',';
			jsonInjectedList += injectedItem;
		}
		jsonInjectedList += ']';
		return jsonInjectedList;
	};
	
	$rootScope.GetCurrentLangCode = function () {
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var crrLangCode = '';
		
		if (baseDomain.indexOf('-') > 0) {
			var subBaseDomain = baseDomain.split('-');
			var langParamPatt = new RegExp(/^l\w{2}$/);
			var langParam = '';
			for (var i = 0; i < subBaseDomain.length; i++) {
				if (langParamPatt.test(subBaseDomain[i]))
					langParam = subBaseDomain[i];
			}
			if (langParam != '')
				crrLangCode = langParam.substring(1);
		}
		if (crrLangCode == '')
			crrLangCode = cspConsumerInfo.lng;
		
		return crrLangCode == undefined || crrLangCode == '' ? 'en' : crrLangCode.toLowerCase();
	};
	
	for (var i = 0; i < $rootScope.pageTypes.length; i++) {
		if ($rootScope.FeaturedPageList[$rootScope.pageTypes[i]] == undefined) {
			$rootScope.validFeaturedPageList = false;
			return;
		}
	}
	
	//Get all categories
	var deferred = $q.defer();
	Categories.promise = deferred.promise.then(function (listCategory) {
		Categories.setData(listCategory);
		return listCategory;
	});
	
	var queryCategoryRootId = getRootCategoryId();
	if (queryCategoryRootId == -1) {
		alert ('Invalid root category id');
		return;
	}
	$http.get('d1.aspx?p2003(ct31000&cd' + queryCategoryRootId + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		//console.log(responseData);
		deferred.resolve(responseData);
	});
	
	var leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
		
		if (data.length <= 1) return null;
		LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
		LeadgenForm.setFields(data[1].contactusformfields);
		return data;
	});
	/*
	$http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		leadgenFormDeferred.resolve(responseData);
	});
	*/
	jQuery.ajax({
		url: 'd1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]',
		dataType: 'text',
		success: function(e) {
			var tmp = e.replace(/\t+/g, '');
			try {
				var tmpJson = JSON.parse(tmp);
				leadgenFormDeferred.resolve(tmpJson);
			} catch (exc) { console.log(exc); }
		}
	});
	
	$rootScope.GetCategoryUrl = function (block) {
		if (block.categoryId == undefined) return '';
		var currentUrl = document.URL;
		if (block.categoryId == $rootScope.FeaturedPageList['contactus']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		if (block.categoryId == $rootScope.FeaturedPageList['home']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	};
	
	$rootScope.GetCategoryTitle = function (navItem) {
		var itemTitle = jQuery('<div>').html(navItem.title).text();
		itemTitle = itemTitle.replace(/<br>/g, '');
		return itemTitle;
	};
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = $rootScope.GetInnerText(value);
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
			}
		}
		try {
			jQuery(document).attr('title', $rootScope.pageTitle);
			//4639: reporting for buttons
			jQuery('[cspenglishvalue="imagelink"]').each(function() {
				if (jQuery(this).hasClass('csp-category-deeplink')) {
					var cspEnglishValue = typeof(jQuery(this).attr('categoryname')) != 'undefined' ? jQuery(this).attr('categoryname') : jQuery(this).attr('cspenglishvalue');
					jQuery(this).attr('cspenglishvalue', cspEnglishValue);
					return;
				}
				var clonedElement = jQuery(this).clone();
				var attr = typeof($(this).attr('reportidvalue')) != 'undefined' ? jQuery(this).attr('reportidvalue') : jQuery(this).attr('href');
				clonedElement.click(function(e) {
					var olDCSext = CspReportLib.wt.DCSext;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = attr;
					CspReportLib.wt.DCSext["ConversionType"] = "image-link";
					CspReportLib.wt.DCSext["ConversionContent"] = attr;
					CspReportLib.wt.dcsMultiTrack();
					CspReportLib.wt.DCSext = olDCSext;
				});
				clonedElement.insertAfter(jQuery(this));
				jQuery(this).remove();
			});
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined" && typeof (CspReportLib.wt) != 'undefined') {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				document.getElementsByTagName('head')[0].appendChild(script);
				
			}, 1000);
			try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
		} catch (Err) {};
	};
	
	$rootScope.GetBlockUrl = function (block) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	}
	$rootScope.GetHTMLString = function (itemTitle) {
		return jQuery('<div/>').html(itemTitle).text();
	}
	
	var escapeHtml = function(unsafe) {
		return unsafe
			 .replace(/&/g, "&amp;")
			 .replace(/</g, "&lt;")
			 .replace(/>/g, "&gt;")
			 .replace(/"/g, "&quot;")
			 .replace(/'/g, "&#039;");
	}

	$rootScope.GetInnerText = function (itemTitle) {
		var regex = /(<([^>]+)>)/ig;
		var title = jQuery('<div>').html(itemTitle);
		var htmlString = $('<input>').val(title.text());
		var innerText = htmlString.val().replace(regex, "");
		innerText = innerText.replace(/&nbsp;/g, ' ');
		return innerText;
		
		/*
		itemTitle = itemTitle.replace('&lt;br&gt;', '');
		itemTitle = escapeHtml(itemTitle);
		var title;
		title = jQuery('<div/>').html(itemTitle).text();
		try {
			if(jQuery(title).text()!=''){
				return jQuery(title).text();
			}
		} catch(e) {
			return title;
		}
		return title;
		*/
	}
});

function GetCurrentLangCode() {
	var crrUrlInfo = location.href.split('/');
	var baseDomain = crrUrlInfo[2];
	var crrLangCode = '';
	var tmp = baseDomain.split('-');
	
	for (var i = 0; i < tmp.length; i++) {
		if (tmp[i].indexOf('l') == 0) {
			crrLangCode = tmp[i].substr(1);
			break;
		}
	}
	
	return crrLangCode != '' ? crrLangCode : cspConsumerInfo.lng.toLowerCase();
};

function GetFeaturedPageList(pageTypes) {
	var pageList = [];
	try {
		cspPageIndicator = cspPageIndicator.replace(new RegExp('\\"', 'g'),'"');
		var featuredPagesList = JSON.parse(cspPageIndicator);
		if (typeof (cspConsumerInfo) == 'undefined') return pageList;
		var crrLangCode = GetCurrentLangCode();		
		
		pageList[pageTypes[0]] = featuredPagesList[0].HomeId;
		pageList[pageTypes[1]] = featuredPagesList[0].ProductId;
		pageList[pageTypes[2]] = featuredPagesList[0].ContactUsId;
		
		for (var i = 0; i < featuredPagesList.length; i++) {
			if ( (isNaN(crrLangCode) && featuredPagesList[i].LangCode.toLowerCase() == crrLangCode)
			|| (!isNaN(crrLangCode) && featuredPagesList[i].LangId == crrLangCode)
				) {
				pageList[pageTypes[0]] = featuredPagesList[i].HomeId;
				pageList[pageTypes[1]] = featuredPagesList[i].ProductId;
				pageList[pageTypes[2]] = featuredPagesList[i].ContactUsId;
			}
		}
	}
	catch (exc) {
		var splitPattern = '_' + cspRootCategoryId + '$';
		if (cspPageIndicator.indexOf('$') < 0)
			cspPageIndicator += splitPattern;
		if (cspPageIndicator.indexOf(splitPattern) < 0) return null;
				
		var mixedInfo = cspPageIndicator.split(splitPattern)[0];
		if (mixedInfo.indexOf('$')) {
			var tmp = mixedInfo.split('$');
			mixedInfo = tmp.pop();
		}
		mixedInfo = mixedInfo.split(';');
		
		for (var i = 0; i < mixedInfo.length; i++) {
			var temp = mixedInfo[i].split('-');
			try {
				pageList[pageTypes[temp[0]]] = temp[1];
			}
			catch (exc) {
				return null;
			}
		}
	}
	return pageList;
}

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = jQuery(window).height() - 100;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		wdHeight = jQuery(window).height() - 100;
		return false;
	}
	return true;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function getRootCategoryId() {
	if (cspRootCategoryId != '') {
		return parseInt(cspRootCategoryId);
	}
	for (var qKey in QueryString) {
        if (QueryString[qKey] == undefined) continue;
		if (QueryString[qKey].indexOf('rcategoryid'))
			return QueryString[qKey];
    }
	return -1;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function isExternal(url) {
    // var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
    // if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) return true;
    // if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "") !== location.host) return true;
    // return false;
	var isExternal = false;
	var linkTarget = jQuery('[href="' + url + '"]');
	if (linkTarget.parents('#workspace').length && !linkTarget.hasClass('csp-category-deeplink'))
		isExternal = true;
	return isExternal;
}

function circuitTrigger (thisEl, event) {
	event.preventDefault();
	var a_link = jQuery(thisEl).attr('href');
	
	if (typeof (CircuitAuthorized) != 'undefined' && CircuitAuthorized.toLowerCase() == 'yes'
		&& typeof(distiResellerID) != 'undefined' && distiResellerID != ""
		&& typeof(a_link) != 'undefined' && a_link != "") {
		//console.log(a_link + "?" + distiResellerID);
		window.location.href = a_link + "?" + distiResellerID;
	}
	else {
		try {
			jQuery('input[name="redirect"]').each(function() {
				jQuery(this).val('');
			});
			jQuery("#frmCircuit").find('#redirect').remove();
			reAdjustColorBoxSize(350);
			AutoPopulateUserInfo("#frmCircuit");
			jQuery.colorbox({inline:true, href:"#frmCircuit", width: wdWidth + "px", height: wdHeight + "px"});
			setTimeout(jQuery.colorbox.resize(), 2000);
			
		} catch (Err) {
			console.log('#2: ' + Err);
		}
	}
}

function addTick(linkEl, event) {
	if (linkEl.hasClass('disti-reseller')) {
		circuitTrigger(linkEl, event);
		return;
	}
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
		return;
	}
	
	if (isExternal(newHref)) return;
	
	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

var validateInlineForm = function (form) {
	var errorMessage = "";
	form.find(".tieValidate").each(function() {
		var isMandatory = jQuery(this).parent().parent().attr("mandatory");
		var isCategoryForm = jQuery('#contactus-page').hasClass('categoryForm');
		var vErrorMsg = jQuery(this).attr("error");
		var vValType = jQuery(this).attr("valType");
		var vValue = jQuery(this).val() || jQuery(this).text();
		var emailpattern = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (jQuery(this).prop("tagName") == 'SELECT')
			vValue = jQuery(this).find('option:selected').val();
		
		if ( isMandatory == "True" || !isCategoryForm){
			if (vValType == 'mandatoryField') {
				vErrorMsg = vErrorMsg.trim() != '' ? vErrorMsg : jQuery(this).attr('name') + ' is required';
				// if mandatory
				// if is mandatoryEmail
				if (jQuery(this).attr('name').toLowerCase().indexOf('email') !== -1){
					vValType = 'mandatoryEmail';
				}
				else if (vValue.length > 1) {
					// do nothing
				} else
					errorMessage += vErrorMsg + "<br>";
			}

			if (vValType == 'mandatoryNumber') {
				// if mandatory
				if (vValue.length > 0) {
					// do nothing
					if (isNaN(vValue))
						{// do nothing
							}
				} else
					errorMessage += vErrorMsg + "<br>";
			}			
			
			if (vValType == 'mandatoryEmail') {
				//var emailpattern = /.+@.+\./;
				if (emailpattern.test(vValue)) {
					//do nothing
				} else
					errorMessage += vErrorMsg + "<br>";
			}
		}
		else {
			//if has value & is email
			if ((vValue.length > 0) && (jQuery(this).attr('name').toLowerCase().indexOf('email') !== -1)){
				if (emailpattern.test(vValue)) {
					//do nothing
				} else {
					vErrorMsg = vErrorMsg.trim() != '' ? vErrorMsg : jQuery(this).attr('name') + ' is not valid';
					errorMessage += vErrorMsg + "\r\n";
				}
			}
		}
	});
	//errorMessage="";
	if (errorMessage == "") {
		
		return true;
	}
	errorMessage = errorMessage.replace(/<br>/g, '\r\n');
	alert(errorMessage);
	return false;
}

function cspInlineFormSubmit(formId) {
	if (DEFAULT_HEADER_FIELDS == '') {
		setTimeout(function() {
			cspInlineFormSubmit(formId)
		}, 500);
		return;
	}
	
	var form = jQuery('#' + formId);
	if (!validateInlineForm(form)) return;
	
	UpdateUserCookies();

	var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
	var submitMsg = 0;
	var submitUrl = 'emailForm?pid=9999';
	$.ajax({
		type: 'POST',
		url: '/saveform?tick=' + tick,
		data: form.serialize(),
		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	});
	
	$.ajax({
		type: "POST",
		url: submitUrl,
		data: form.serialize(),
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		success: function(e) {
			alert('Inline form submitted');
			form[0].reset();
		}
	});
}

jQuery(function() {
	jQuery("#sce-wrapper").on('submit', '.csp-inline-form', function(e) {
		e.preventDefault();
	});
	
	jQuery("#sce-wrapper").on('click', '.csp-inline-form :submit', function(e) {
		e.preventDefault();
		var inlineForm = jQuery(this).parents('form');
		if (jQuery(inlineForm).find('input[name="subject"]').length == 0) {
			jQuery(inlineForm).append(DEFAULT_HEADER_FIELDS);
		}
		
		if (jQuery(inlineForm).find('input[name="subject"]').length > 0) {
			jQuery(inlineForm).find('input[name="subject"]').val('Contact Us from Showcase');
		}
		cspInlineFormSubmit(jQuery(inlineForm).attr('id'));
	});
});