var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;

var moduleSyndicate = angular.module('ngSyndicate', []).config(function($routeProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contentpageview.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contentpageview.html'}).
		when('/category/:id', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contentpageview.html'}).
		when('/category/:id/i/:tick', {controller:MainCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contentpageview.html'}).
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contactusview.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contactusview.html'}).
		when('/contactus/title/:titlevalue', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contactusview.html'}).
		when('/contactus/title/:titlevalue/i/:tick', {controller:ContactUsCtrl, templateUrl:'customer-resources/showcase/Custom/SyndicateTemplate1/contactusview.html'}).
		otherwise({ redirectTo: '/' });
});

moduleSyndicate.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

moduleSyndicate.factory('Categories', function ($http) {
	var rootId = getRootCategoryId();
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (listCategory) {
			data = listCategory;
		},
		convertJSONtoTree: function (rawArray) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(rawArray);
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			
			return roots[0].children;
		}
	}
});

moduleSyndicate.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

function MainNavCtrl($scope, $http, $rootScope, $routeParams, Categories, URLHandler) {
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.GetCategoryUrl = function (navItem, isRoot) {
		if (navItem.children != undefined && isRoot) return '';
		var currentUrl = document.URL;
		if (navItem.value.categoryId == $rootScope.FeaturedPageList['contactus']) {
			$rootScope.label.contactus = navItem.value.title;
			return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		}
		if (navItem.value.categoryId == $rootScope.FeaturedPageList['home']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + navItem.value.categoryId;
	}
	
	$scope.GetToggleValue = function (navItem) {
		if (navItem.children != undefined)
			return 'dropdown';
		return '';
	}
	
	Categories.promise.then(function(resultData){
		resultData.pop();
		//$rootScope.AllProducts = Categories.convertJSONtoTree(resultData);
		if (resultData.length ==0)
			return;
		$scope.navitems = Categories.convertJSONtoTree(resultData);
	});
}

function MainCtrl($scope, $rootScope, $http, $routeParams, $filter, $timeout, Categories, URLHandler, LeadgenForm) {
	if (!$rootScope.validFeaturedPageList) return;
	
	tickValue = URLHandler.getTick();
	$scope.crrCatId;
	$scope.crrCatItem = [];
	$rootScope.CategoryItem = [];
	$rootScope.homeBlocks = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.leftItem = [];
	$scope.ajaxLoading = true;
	$scope.formHeaderTitle = '';
	$scope.formHeaderDescriptionShort = '';
	$scope.supplier = [];
	$scope.fieldList = '';
	
	var GetBreadCrumb = function (categoryList, crrId, bcArray) {
		if (crrId == 0) return;
		var crrItem = $filter('filter')(categoryList, {categoryId: crrId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if (crrItem[0] == undefined || crrItem[0].parentId == 0) return;
		
		bcArray.push(crrItem[0]);
		GetBreadCrumb(categoryList, crrItem[0].parentId, bcArray);
	};
	
	/*
	$scope.GetLeftItemUrl = function (block) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	}
	*/
	
	Categories.promise.then(function(resultData){
		if (typeof($routeParams.id) == "undefined") {
			$scope.crrCatId = $rootScope.FeaturedPageList['home'];
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $scope.crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		}
		else {
			$scope.crrCatItem = $filter('filter')(resultData, {categoryId: $routeParams.id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$scope.crrCatId = $routeParams.id;
		}
		
		if ($scope.crrCatItem.length == 0) {
			return;
		}
		$rootScope.updatePageInfo(null, $scope.crrCatItem[0].title);
		$rootScope.CategoryItem = $scope.crrCatItem[0];
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1) {
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
			console.log($rootScope.ContactUsItem);
		}
			
		$http.get('d1.aspx?p2013(ct66000&cd' + $scope.crrCatId + ')[]' + URLHandler.getUrlTick()).success(function(responseData) {
			$scope.ContentPage = responseData[0].contentpage;
			var htmlContent = jQuery('<div>').html(responseData[0].contentpage);
			try {
				htmlContent.find('a:not(.csp-category-deeplink)').each(function() {
					jQuery(this).attr('target', '_blank');
				});
				
				htmlContent.find('a[href$="pdf"]').each(function(){
					var url=jQuery(this).attr('href');
					var fileName= url.split("/").pop().split('.').shift();
					jQuery(this).attr({cspobj:"REPORT", csptype:"DOWNLOADS", cspenglishvalue:fileName});
				});
				
				$scope.ContentPage = htmlContent.html();
			}
			catch (err) {}
			
			setTimeout(function(){
				$('.full-size').each(function() {
					$(this).find('*').each(function() {
						if (!$(this).hasClass('overlaywrap') && !$(this).hasClass('player'))
							$(this).width('100%');
					});
				});
				
				$('.csp-category-deeplink').each(function() {
					var cId = $(this).attr('rel');
					var cItem = $filter('filter')(resultData, {categoryId: cId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
					if (cItem.length == 0) {
						$(this).remove();
						return;
					}
					$(this).attr('href', document.URL.substring(0, document.URL.indexOf('#/')) + '#/category/' + $(this).attr('rel'));
				});
			},500);
			
			$timeout(function(){
				$scope.ajaxLoading = false;
			},1500);
			
			setTimeout(function() {
				//console.log("*** Product Promised CLICKED");
				jQuery(document).click();							
			}, 1000);
		});
		
		
		if ($scope.crrCatId == $rootScope.FeaturedPageList['home'] || $scope.crrCatId == $rootScope.FeaturedPageList['product']) {
			$rootScope.leftItems = [];
			$rootScope.homeBlocks = $filter('filter')(resultData, {parentId: $rootScope.FeaturedPageList['product']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		}
		else {
			$rootScope.breadcrumbBlocks.push($scope.crrCatItem[0]);
			GetBreadCrumb(resultData, $scope.crrCatItem[0].parentId, $rootScope.breadcrumbBlocks);
			$rootScope.breadcrumbBlocks.pop();
			var homeItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$rootScope.breadcrumbBlocks.push(homeItem[0]);
			$rootScope.breadcrumbBlocks.reverse();
			
			$rootScope.leftItems = $filter('filter')(resultData, {parentId: $scope.crrCatId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			if ($rootScope.leftItems.length == 0)
				$rootScope.leftItems = $filter('filter')(resultData, {parentId: $scope.crrCatItem[0].parentId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			$rootScope.crrCatId = $scope.crrCatId;
		}
		
	});
	
	$scope.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		
	};

	$scope.submit = function() {
		LeadgenForm.submit('frmleadgen', $scope.submitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getDownloadFormFields();
		jQuery(document).click();
	});
}

function ContactUsCtrl ($rootScope, $scope, $routeParams, $timeout, $http, $filter, LeadgenForm, URLHandler, Categories) {
	tickValue = URLHandler.getTick();
	$rootScope.updatePageInfo(this);
	$rootScope.homeBlocks = [];
	$rootScope.leftItems = [];
	Categories.promise.then(function(resultData){
		$rootScope.breadcrumbBlocks = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['home']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		$rootScope.ContactUsItem = $filter('filter')(resultData, {categoryId: $rootScope.FeaturedPageList['contactus']}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if ($rootScope.ContactUsItem.length == 1)
			$rootScope.ContactUsItem = $rootScope.ContactUsItem[0];
	});
	
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	$scope.ajaxLoading = false;
	//$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		if (LeadgenForm.submit('form1', $scope.submitted) == 1)
			$scope.ajaxLoading = true;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
	
}

moduleSyndicate.run(function ($rootScope, $q, $http, Categories, LeadgenForm) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
					if (temp[i] == 'i') {
							currentTick = '&i=' + temp[i + 1];
							break;
					}
			}
	}
	
	$rootScope.crrCatId;
	$rootScope.CategoryItem = [];
	$rootScope.pageTitle = 'Home';
	$rootScope.pageTypes = new Array ('home', 'product', 'contactus');
	$rootScope.AllCategories = [];
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.validFeaturedPageList = true;
	$rootScope.homeBlocks = [];
	$rootScope.breadcrumbBlocks = [];
	$rootScope.label = {
		contactus: 'Contact Us'
	};
	$rootScope.ContactUsItem = [];
	$rootScope.SupplierLogo = cspSupplierLogo;
	
	$rootScope.FeaturedPageList = GetFeaturedPageList($rootScope.pageTypes);
	
	$rootScope.GetHomeUrl = function () {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
	}
	
	for (var i = 0; i < $rootScope.pageTypes.length; i++) {
		if ($rootScope.FeaturedPageList[$rootScope.pageTypes[i]] == undefined) {
			$rootScope.validFeaturedPageList = false;
			return;
		}
	}
	
	//Get all categories
	var deferred = $q.defer();
	Categories.promise = deferred.promise.then(function (listCategory) {
		Categories.setData(listCategory);
		return listCategory;
	});
	
	var queryCategoryRootId = getRootCategoryId();
	if (queryCategoryRootId == -1) {
		alert ('Invalid root category id');
		return;
	}
	$http.get('d1.aspx?p2003(ct31000&cd' + queryCategoryRootId + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		deferred.resolve(responseData);
	});
	
	var leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
		
		if (data.length <= 1) return null;
		LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
		LeadgenForm.setFields(data[1].contactusformfields);
		
		return data;
	});
	
	$http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		leadgenFormDeferred.resolve(responseData);
	});
	
	$rootScope.GetCategoryUrl = function (block) {
		if (block.categoryId == undefined) return '';
		var currentUrl = document.URL;
		if (block.categoryId == $rootScope.FeaturedPageList['contactus']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/contactus';
		if (block.categoryId == $rootScope.FeaturedPageList['home']) return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/';
		//if (navItem.children != undefined) return '';
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	};
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = $rootScope.GetInnerText(value);
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
			}
		}
		//console.log('#webtrends:' + $rootScope.pageTitle);
		try {
			jQuery(document).attr('title', $rootScope.pageTitle);
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				document.getElementsByTagName('head')[0].appendChild(script);

			}, 1000);
			try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
		} catch (Err) {};
	};
	
	$rootScope.GetBlockUrl = function (block) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + block.categoryId;
	}
	$rootScope.GetHTMLString = function (itemTitle) {
		return jQuery('<div/>').html(itemTitle).text();
	}

	$rootScope.GetInnerText = function (itemTitle) {
		
		itemTitle = itemTitle.replace('&lt;br&gt;', '');
		try {
			var title = jQuery('<div/>').html(itemTitle).text();
			if(jQuery(title).text()!=''){
				return jQuery(title).text();
			}
		} catch(e) {
			title = jQuery('<div/>').html(title).text();
		}
		finally {			
			try {
			if(jQuery(title).text()!=''){
				return jQuery(title).text();
			}
			} catch(e1) {
				return title;
			}
		}
		return title;
	}
	
	$rootScope.GetEncodedInnerText = function (title) {
		var innerText = $rootScope.GetInnerText(title);
		return encodeURIComponent(innerText);
	}
});

function GetFeaturedPageList(pageTypes) {
	var splitPattern = '_' + cspRootCategoryId + '$';
	if (cspPageIndicator.indexOf('$') < 0)
		cspPageIndicator += splitPattern;
	if (cspPageIndicator.indexOf(splitPattern) < 0) return null;
			
	var mixedInfo = cspPageIndicator.split(splitPattern)[0];
	if (mixedInfo.indexOf('$')) {
		var tmp = mixedInfo.split('$');
		mixedInfo = tmp.pop();
	}
	mixedInfo = mixedInfo.split(';');
	var pageList = [];
	for (var i = 0; i < mixedInfo.length; i++) {
		var temp = mixedInfo[i].split('-');
		try {
			pageList[pageTypes[temp[0]]] = temp[1];
		}
		catch (exc) {
			return null;
		}
	}
	return pageList;
	// var mixedInfo = cspPageIndicator.split(';');
	// var pageList = [];
	// for (var i = 0; i < mixedInfo.length; i++) {
		// var temp = mixedInfo[i].split('-');
		// try {
			// pageList[pageTypes[temp[0]]] = temp[1];
		// }
		// catch (exc) {
			// return null;
		// }
	// }
	// return pageList;
}

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function getRootCategoryId() {
	if (cspRootCategoryId != '') {
		return parseInt(cspRootCategoryId);
	}
	for (var qKey in QueryString) {
        if (QueryString[qKey] == undefined) continue;
		if (QueryString[qKey].indexOf('rcategoryid'))
			return QueryString[qKey];
    }
	return -1;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}