var categoryTree = [];
var rawCategoryData = [];
var crrCategoryItem;
var SYNDICATION_TYPE = 'showcase';
var VALID_SYNDICATION_TYPES = ['', SYNDICATION_TYPE, 'all', 'hidden'];
var FeaturedPages = {
	'home': '',
	'product': '',
	'contactus': ''
};

var initApp = function () {
	try {
		//$('head').append('<link rel="stylesheet" href="' + ngViewFolderUrl + '/FlatStyle2/css/bootstrap.min.css" type="text/css" />');
		initCategoryList();
		initSupplierLogo();
	} catch (exc) {
		console.log(exc);
	}
}
function GetHTMLString(itemTitle) {
		return jQuery('<div/>').html(itemTitle).text();
	}

function GetInnerText(itemTitle) {
		itemTitle = itemTitle.replace('&lt;br&gt;', '');
		try {
			var title = jQuery('<div/>').html(itemTitle).text();
			if(jQuery(title).text()!=''){
				return jQuery(title).text();
			}
		} catch(e) {
			title = jQuery('<div/>').html(title).text();
		}
		finally {			
			if(jQuery(title).text()!=''){
				return jQuery(title).text();
			}
		}
		return title;
	}
	
function DoFilterSyndicationType(data) {
	var filteredList = [];
	for (var i = 0; i < data.length; i++) {
		var type = data[i].localsyndicationtype.toLowerCase();
		
		if (VALID_SYNDICATION_TYPES.indexOf(type) != -1)
			filteredList.push(data[i]);
	}
	return filteredList;
}

function initCategoryList() {
	$.ajax({
		url: categoryJSONUrl,
		type: 'POST',
		dataType: 'json',
		data: { ajaxaction: 'get_category' },
		beforeSend: function (xhr) {
			xhr.setRequestHeader("X-OFFICIAL-REQUEST", "TRUE"); //Used to ID as a AJAX Request
		},
		success: function (data) {
			data = DoFilterSyndicationType(data);
			rawCategoryData = data;
			var rootNode = getRootNode(data);
			categoryTree = convertJSONtoTree(data, rootId);
			
			if (crrCategoryItem == undefined || document.URL.indexOf('categoryid') == -1) {
				try {
					switchLoadingPanel(true);
				} catch(exc) {}
				if (typeof(FeaturedPages.home)!="undefined"&&FeaturedPages.home!="") {
					if(document.URL.indexOf('.aspx?') == -1)
						window.location = document.URL + '?categoryid='+FeaturedPages.home;
					else
						window.location = document.URL + '&categoryid='+FeaturedPages.home;
					return;
				}
				else {
					switchLoadingPanel(false);
				}
				return;
			}
			
			//Build main nav bar
			for (var i = 0; i < categoryTree.length; i++)
				buildMainNav(categoryTree[i], null);
			
			//console.log(crrCategoryItem);		
			//Render breadcrumb
			if (crrCategoryItem.CSP_Level.toLowerCase() != 'home') {
				initBreadCrumb();
				initLeftCol();
			}
			else {
				$('#sce-breadcrumb').hide();
			}
			
			//Render page info
			//initPageInfo();
			
			initHomeBlocks(data);
		},
		error: function (err) {
		}
	});
}

function buildMainNav(crrNode, parentNode) {
	var parentDOMNode;// = $('li#nav-item-' + crrNode.value.parentId);
	var activeClass = '';
	if (crrNode.value.categoryId == crrCategoryItem.categoryId)
		activeClass = 'active';
	var templateLi = $('<li id="#nav-item-' + crrNode.value.categoryId + '" class="' + activeClass + '">');
	var templateA = $('<a href="' + categoryUrlFormat + '?categoryid=' + crrNode.value.categoryId + '" >');
	var subMenuCol;
	
	var data = GetInnerText(crrNode.value.title);
	try {
		templateA.html(data);
	}
	catch(ex) {
		templateA.text(data);
	}
	
	if (parentNode == null) {
		parentDOMNode = $('ul#sce-mainnav-ul');
		templateLi.append(templateA);
		templateLi.addClass('main-menu-item');
		templateA.addClass('root-item');
		if (crrNode.children != undefined) {
			templateLi.addClass('dropdown');
			templateA.append('<b class="caret">');
			templateA.addClass('dropdown-toggle');
			templateA.attr('href', '#');
			templateA.attr('data-toggle', 'dropdown');
		}
	}
	else {
		parentDOMNode = parentNode;
		if (parentDOMNode.parents('ul').length == 1) {
			templateLi.addClass('sub-menu-item');
			templateA.addClass('sub-header-item');
			subMenuCol = $('<div class="sub-menu-col">');
			templateLi.append(subMenuCol);
			subMenuCol.append(templateA);
		}
		else {
			templateLi.addClass('last-level-item');
			templateLi.append(templateA);
		}
	}

	parentDOMNode.append(templateLi);
	
	if (crrNode.children != undefined && parentDOMNode.parents('ul').length <= 1) {
		var nextParentNode = $('<ul>');
		if (parentNode == null) {
			nextParentNode.addClass('sub-menu dropdown-menu sub-menu-' + crrNode.children.length);
			templateLi.append(nextParentNode);
		}
		else
			subMenuCol.append(nextParentNode);
		for (var i = 0; i < crrNode.children.length; i++)
			buildMainNav(crrNode.children[i], nextParentNode)
	}
}

function initBreadCrumb() {
	var listBreadcrumb = [];
	var cateItem = crrCategoryItem;
	while (true) {
		if (cateItem.parentId == 0||cateItem.parentId == 1) break;
		listBreadcrumb.push(cateItem);
		for (var i = 0; i < rawCategoryData.length; i++) {
			if (cateItem.parentId == rawCategoryData[i].categoryId) {
				cateItem = rawCategoryData[i];
				break;
			}
		}
	}
	
	if (listBreadcrumb.length == 0) return;
	
	listBreadcrumb.pop();
	
	for (var i = 0; i < rawCategoryData.length; i++) {
		if (rawCategoryData[i].CSP_Level.toLowerCase() == 'home') {
			listBreadcrumb.push(rawCategoryData[i]);
			break;
		}
	}
		
	listBreadcrumb.reverse();
	
	for (var j = 0; j < listBreadcrumb.length; j++) {
		var itemClassName = '';
		if (j == listBreadcrumb.length - 1)
			itemClassName = 'current';
		var bcWrapper = $('<span>');
		var breadcrumbItem = $('<a class="' + itemClassName + '" href="' + categoryUrlFormat + '?categoryid=' + listBreadcrumb[j].categoryId + '">');
		breadcrumbItem.text(GetInnerText(listBreadcrumb[j].title));
		bcWrapper.append(breadcrumbItem);
		if (j < listBreadcrumb.length - 1)
			bcWrapper.append($('<span class="bc-decor">&gt;</span>'));
			
		$('#sce-breadcrumb').append(bcWrapper);
	}
}

function initLeftCol() {
	$('#sce-left-col').show();
	var leftItemList = [];
	var childrenList = [];
	var siblingList = [];
	for (var j = 0; j < rawCategoryData.length; j++) {
		if (rawCategoryData[j].parentId == crrCategoryItem.categoryId) {
			childrenList.push(rawCategoryData[j]);
		}
		else if (rawCategoryData[j].parentId == crrCategoryItem.parentId) {
			siblingList.push(rawCategoryData[j]);
		}
	}
	
	leftItemList = childrenList;
	if (leftItemList.length == 0)
		leftItemList = siblingList;
	
	if (leftItemList.length == 0) {
		$('#sce-left-col').hide();
	}
	else {
		$('#workspace').addClass('sce-right-col src-builder-mode');
	}
	
	for (var i = 0; i < leftItemList.length; i++) {
		var leftItem = $('<li>');
		if (leftItemList[i].categoryId == crrCategoryItem.categoryId)
			leftItem.addClass('active');
		var leftItemLink = $('<a href="' + categoryUrlFormat + '?categoryid=' + leftItemList[i].categoryId + '">').text(GetInnerText(leftItemList[i].title));
		leftItem.append(leftItemLink);
		$('#sce-left-col').find('ul').append(leftItem);
	}
}

function initPageInfo() {
	$('#sce-prod-title').text(crrCategoryItem.title);
	$('#sce-prod-desc').text(crrCategoryItem.description);
}

function initHomeBlocks(categoryArr) {
	var prodItem;
	var itemCount = 0;
	//console.log(crrCategoryItem, categoryArr);
	if (crrCategoryItem.CSP_Level.toLowerCase() == 'home' || crrCategoryItem.CSP_Level.toLowerCase() == 'product') {
		for (var i = 0; i < categoryArr.length; i++) {
			if (categoryArr[i].CSP_Level == undefined || categoryArr[i].CSP_Level == '') continue;
			if (categoryArr[i].CSP_Level.toLowerCase() == 'product') {
				prodItem = categoryArr[i];
				break;
			}
		}
		/*
		<div class="sub-category-block ng-scope" ng-include="'homeblock-tmpl'" ng-repeat="block in homeBlocks | orderBy:'displayorder'">
			<div class="block-wrapper ng-scope">
				<a class="sub-category-block-figure" ng-href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137" href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137">
					<img ng-src="http://localhost/dnn/0602/customer-resources/showcase/custom/ShowcaseMedia/Portal_0/User_1/banner-1.png" src="http://localhost/dnn/0602/customer-resources/showcase/custom/ShowcaseMedia/Portal_0/User_1/banner-1.png">
				</a>
				<h2>
					<a class="ng-binding" ng-href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137" href="http://pepicor-c635249398645816142.epicor.local.coderrental.com/d1.aspx?p2000()[]rcategoryid=133#/category/137">About Epicor EDI Partner TIE Kinetix and Epicor Partnership</a>
				</h2>
				<p class="ng-binding" ng-bind-html-unsafe="block.description">this is a feature</p>
			</div>
		</div>
		*/
		for (var j = 0; j < categoryArr.length; j++) {
			if (categoryArr[j].parentId == prodItem.categoryId) {
				var categoryDesc = GetHTMLString(categoryArr[j].description);
				var categoryTitle = GetHTMLString(categoryArr[j].title);
				var newSubBlock = $('<div class="sub-category-block">');
				var newSubBlockWrapper = $('<div class="block-wrapper ng-scope">');
				var newBlockFigureLink = $('<a class="sub-category-block-figure" href="' + categoryUrlFormat + '?categoryid=' + categoryArr[j].categoryId + '">');
				var newFigureImage = $('<img src="' + categoryArr[j].thumbnail + '" />');
				//var newBlockTitle = $('<h2>');
				var newBlockTitleTextLink = $('<a class="block-title" href="' + categoryUrlFormat + '?categoryid=' + categoryArr[j].categoryId + '">').html(categoryTitle);
				var newBlockDesc = $('<p>').html(categoryDesc);
				
				newBlockFigureLink.append(newFigureImage);
				//newBlockTitle.append(newBlockTitleTextLink);
				newSubBlockWrapper.append(newBlockFigureLink);
				newSubBlockWrapper.append(newBlockTitleTextLink);
				newSubBlockWrapper.append(newBlockDesc);
				newSubBlock.append(newSubBlockWrapper);
				$('#sub-category-blocks').append(newSubBlock);
				itemCount++;
			}
		}
		
		$('#sub-category-blocks').addClass('home-blocks-' + itemCount);
	}
	else
		$('#sub-category-blocks').hide();
}

function initSupplierLogo() {
	if (supplierLogo == '') {
		$('#sce-logo-img').hide();
		return;
	}
	$('#sce-logo-img').attr('src', supplierLogo);
	$('#sce-logo-img').parent('a').attr('href', categoryUrlFormat);
}

function initContactUsBtn(cuItem) {
	$('#left-btn-contactus').text(cuItem.title);
}

var getRootNode = function (inputArr) {
	for (var i = 0; i < inputArr.length; i++) {
		if (inputArr[i].categoryId == rootId)
			return inputArr[i];
	}
}

var convertJSONtoTree = function (inputArr, crootId) {
	var fullArray = inputArr;
	var roots = [], children = {};

	// find the top level nodes and hash the children based on parent
	for (var i = 0, len = fullArray.length; i < len; ++i) {
		var item = fullArray[i],
			p = item.parentId,
			target = !p ? roots : (children[p] || (children[p] = []));
		
		//Get root node by arg crootId
		if (crootId != undefined && item.categoryId == parseInt(crootId))
			target = roots;
		target.push({ value: item });
		
		if (fullArray[i].categoryId == parseInt(categoryId))
			crrCategoryItem = fullArray[i];
		if (fullArray[i].CSP_Level.toLowerCase() == 'home' || fullArray[i].CSP_Level.toLowerCase() == 'product' || fullArray[i].CSP_Level.toLowerCase() == 'contactus') {
			FeaturedPages[fullArray[i].CSP_Level.toLowerCase()] = fullArray[i].categoryId;
			if (fullArray[i].CSP_Level.toLowerCase() == 'contactus') {
				initContactUsBtn(fullArray[i]);
			}
		}
	}
	
	// function to recursively build the tree
	var findChildren = function(parent) {
		if (children[parent.value.categoryId]) {
			parent.children = children[parent.value.categoryId];
			for (var i = 0, len = parent.children.length; i < len; ++i) {
				findChildren(parent.children[i]);
			}
		}
	};

	// enumerate through to handle the case where there are multiple roots
	for (var i = 0, len = roots.length; i < len; ++i) {
		findChildren(roots[i]);
	}
	return roots[0].children;
}