﻿using System.Web;
using System.Web.Routing;

namespace Csp.Web.Infrastructure
{
    public class CspRoute : Route
    {
        public CspRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler) : base(url, defaults, routeHandler)
        {
        }

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var routeData = base.GetRouteData(httpContext);

            return routeData;
        }

    }
}