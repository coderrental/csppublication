﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Csp.Core.Infrastructure.Publication;

namespace Csp.Web.Infrastructure
{
    public class CspRouteHandler : MvcRouteHandler
    {
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            base.GetHttpHandler(requestContext);
            return (IHttpHandler)new CspHandler(requestContext);
        }
    }

    public class CspHandler : MvcHandler
    {
        public CspHandler(RequestContext requestContext) : base(requestContext)
        {
        }

        protected override IAsyncResult BeginProcessRequest(HttpContext httpContext, AsyncCallback callback, object state)
        {
            PublicationEngine.CspHandlerInit();
            PublicationEngine.CspHandlerProcessRequest(httpContext);
            return base.BeginProcessRequest(httpContext, callback, state);
        }
    }
}