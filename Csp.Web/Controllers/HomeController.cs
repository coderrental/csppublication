﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Csp.Core.Infrastructure.DAO.Models;
using Csp.Core.Infrastructure.Publication;
using Csp.Core.Infrastructure.Publication.Models;
using Csp.Core.Infrastructure.Publication.Services;

namespace Csp.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISynService _service;

        public HomeController(ISynService service)
        {
            _service = service;
        }

        public ActionResult Index(string page)
        {
            var result = new ResponseResult();
            var proxyModel = new ProxyModel();

            InitData(page, proxyModel, result);

            if (!string.IsNullOrEmpty(result.Content))
            {
                return Content(result.Content, result.Type);
            }

            switch (page)
            {
                case "d1":
                    result = PublicationEngine.D1Function(new HttpContextWrapper(System.Web.HttpContext.Current), proxyModel, _service);
                    break;
                case "d2":
                    result = PublicationEngine.D2Function(new HttpContextWrapper(System.Web.HttpContext.Current), proxyModel, _service);
                    break;
            }

            return Content(result?.Content, result?.Type);
        }

        public void InitData(string page, ProxyModel proxyModel, ResponseResult result)
        {
            var context = System.Web.HttpContext.Current;
            if (context.Items["PROXY_CONN_STRING"] != null)
            {
                proxyModel.ConnectionString = context.Items["PROXY_CONN_STRING"].ToString();
                proxyModel.RequestUrl = (Uri)context.Items["PROXY_URL"];
                proxyModel.ReqSerVarQryStr = (string)context.Items["PROXY_SERVER_VAR_QUERY"];
                proxyModel.InlineRequestUri = new Uri(Request.Url?.Scheme + "://" + proxyModel.RequestUrl.Host + ":" + Request.Url?.Port + "/?" + proxyModel.ReqSerVarQryStr);
                proxyModel.ProjectName = context.Items["PROJECT_NAME"] as string;
                if (context.Session["NOCACHE"] != null)
                {
                    proxyModel.IsNoCache = (bool)context.Session["NOCACHE"];
                }

                if (context.Items["PROXY_SERVER_ORIGINAL_QUERY"] != null)
                {
                    proxyModel.ProxyQuery = (string)context.Items["PROXY_SERVER_ORIGINAL_QUERY"];
                }

                if (context.Items["REQUESTED_URL"] != null)
                {
                    proxyModel.OriginalRequestedUrl = (Uri)context.Items["REQUESTED_URL"];
                }
                else
                {
                    result.Content = "Connection not found";
                    return;
                }
            }

            try
            {
                proxyModel.LngId = (int?)Session["language_Id"];
            }
            catch (Exception)
            {
                var lngStr = System.Web.HttpContext.Current.Request.QueryString["lng"];
                if (!string.IsNullOrEmpty(lngStr))
                {
                    proxyModel.LngId = _service.GetLngId(lngStr);
                }
            }

            // d1 cache recall if possible
            try
            {
                proxyModel.DaysToFetchCache = Convert.ToInt32(ConfigurationManager.AppSettings["DaysToUsePubCache"]);
                // check for inline content request
                if ((from string q in HttpUtility.ParseQueryString(proxyModel.RequestUrl.Query) where !string.IsNullOrEmpty(q) select q).Any(q => q.Equals("inlinecontent", StringComparison.CurrentCultureIgnoreCase)))
                {
                    proxyModel.IsInlineRequest = true;
                }
            }
            catch (Exception)
            {
                proxyModel.DaysToFetchCache = -1;
                proxyModel.IsInlineRequest = false;
            }

            // Load parameters
            var isHasParam = proxyModel.RequestUrl.OriginalString.Contains("?") && !string.IsNullOrEmpty(proxyModel.ReqSerVarQryStr);
            if (isHasParam)
            {
                //  There are parameters in this URL, so it's not the homepage being requested => use the pageID from the URL
                var strWorkURL = proxyModel.ReqSerVarQryStr.Replace("%20", " ");
                var strQueryString = proxyModel.ReqSerVarQryStr;

                proxyModel.StrRawUrl = strWorkURL;//  We need the rawURL later on for passing it on to HTML.SourceURL for paging
                strWorkURL = PublicationEngine.ReplaceCharacters(strWorkURL);

                if (page.Equals("d1"))
                {
                    //  Replace any dangerous/strange characters in the URL
                    // 4-13
                    // strQueryString = strQueryString.Replace("%28", "(").Replace("%29", ")").Replace("*C*", ":")
                    strQueryString = strQueryString.Replace("=%", "EQPERCENT123");
                    strQueryString = strQueryString.Replace("%28", "(").Replace("%29", ")").Replace("*C*", ":");
                    strQueryString = strQueryString.Replace("EQPERCENT123", "=%");
                    // ltu 08/31
                    // %5B%5D
                    strWorkURL = strWorkURL.Replace("%5B", "[").Replace("%5D", "]");
                    strWorkURL = strWorkURL.Replace("%7C", "|");
                    // ltu 10/25
                    strWorkURL = strWorkURL.Replace("%25", "%");
                    if (strQueryString.Contains("cs=0"))
                    {
                        //  Redirect from ChannelBuilder. Remove the cs=0 parameter. 
                        //  No extra parameters, so just remove cs=0
                        strQueryString = Request.RawUrl == "/d1.aspx?cs=0"
                            ? strQueryString.Remove(strQueryString.IndexOf("cs=0"), 4)
                            : strQueryString.Remove(strQueryString.IndexOf("cs=0") - 1, 5);
                    }

                    if (strWorkURL.Contains("cs=0"))
                    {
                        strWorkURL = Request.RawUrl.Equals("/d1.aspx?cs=0")
                            ? strWorkURL.Remove(strWorkURL.IndexOf("cs=0"), 4)
                            : strWorkURL.Remove(strWorkURL.IndexOf("cs=0") - 1, 5);
                    }

                    if (strWorkURL.Equals("") && strQueryString.Equals(""))
                    {
                        //  We have an empty WorkURL. We only get here if this is a dealer integrated site. 
                        //  For now we're getting the base_parameters to get the content. In the future in should be possible to use parameters
                        //  in the URL as well. Redirect to where we get the base parameters from the database.
                        isHasParam = false;
                    }
                }

                if (isHasParam)
                {
                    // If Request.QueryString("lng") Is Nothing Then ' Get the languageCode and set a Session.
                    // If Session("language_Id") Is Nothing Then
                    // Session("language_Id") = ""
                    // End If
                    // Else
                    //     Session("language_Id") = Request.QueryString("lng")
                    //  End If
                    if (strWorkURL.EndsWith(")"))
                    {
                        //  strWorkURL has to end with () for the URLParser.
                    }
                    else
                    {
                        strWorkURL = strWorkURL + "()";
                    }

                    if (strQueryString.Contains("("))
                    {
                        //  The pageID can't have (), so remove it.
                        //  Check if we have a valid page ID, meaning only numbers (integer) and not any characters (string)
                        if (int.TryParse(strQueryString.Substring(1, strQueryString.IndexOf("(") - 1), out var rslt))
                        {
                            proxyModel.IntPageId = rslt;
                        }
                        else
                        {
                            result.Content = "PageID is not valid (ErrorCode 10).<br><br><a href=http://" + proxyModel.RequestUrl.Host + ">Go back to the homepage</a> ";
                            return;
                        }
                    }
                    else
                    {
                        //  Check if we have a valid page ID, meaning only numbers (integer) and not any characters (string)
                        if (int.TryParse(strQueryString.Remove(0, 1), out var rslt))
                        {
                            proxyModel.IntPageId = rslt;
                        }
                        else
                        {
                            result.Content = "PageID is not valid (ErrorCode 15).<br><br><a href=http://" + proxyModel.RequestUrl.Host + ">Go back to the homepage</a> ";
                            return;
                        }
                    }

                    //  Since the user could have manipulated the pageID in the URL, we check if this domain is allowed to get this page/component/javascript.
                    proxyModel.IntPublicationId = _service.GetIntPublicationID(proxyModel.IntPageId, proxyModel.RequestUrl.Host, out var errMsg);
                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        //  No rows found, so this domain is not allowed to request this page/component/javascript.
                        result.Content = "You are not allowed to request this publication (ErrorCode 70).<br><br><a href=http://" + proxyModel.RequestUrl.Host + ">Go back to the homepage</a> ";
                        return;
                    }
                }

                proxyModel.StrWorkUrl = strWorkURL;
            }

            if (!isHasParam)
            {
                //  No parameters in the URL, get parameters, publicationID and index pageID for this domain from the database.
                var errorMsg = "";
                if (page.Equals("d1"))
                {
                    _service.GetParameters(proxyModel.RequestUrl.Host, proxyModel, out errorMsg);
                }
                else if (page.Equals("d1"))
                {
                    _service.GetParameters_D2(proxyModel.RequestUrl.Host, proxyModel, out errorMsg);
                }

                if (!string.IsNullOrEmpty(errorMsg))
                {
                    result.Content = errorMsg;
                }

                //  T.M. 24-01-2008 Quantore demo-day
                //  Check if the index pageID matches the pageID in the base parameters, if there are any parameters set for this domain. If they don't
                //  match, the URLParser will show nothing. Makes sence, since the URLParser will then get 2 different pageID's to process.
                // If checkPageIDs(strWorkURL, intPageID) = False Then ' The pageID's don't match, so we stop here.
                // Response.Write("The index pageID does not match the pageID in the Base Parameters.")
                // Exit Sub
                // Else ' The pageID's match, so we continue.
                // End If
            }
        }
    }
}
