﻿using Csp.Web;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace Csp.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            PackageConfig.Register();

            DependencyConfig.Register(app);
        }
    }
}
