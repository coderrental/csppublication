if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(elt /*, from*/) {
		var len = this.length >>> 0;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
		: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++) {
			if (from in this &&
			this[from] === elt)
				return from;
		}
		return -1;
	};
}

(function($) {
	var gThisEl;
	var gSettings = {};
	var gBlockOrders = ["product-detail-panel", "block-detail-panel", "asset-detail-panel"];//Define blocks' display orders
	var gEmbededVids = [];

    $.fn.cspinline = function(options, finishCallback) {
		// Establish default settings
        var settings = $.extend({
            language: null,
            partnumber: null,
            cspid: null,
			//targeturl: 'http://selfserviceapi.tiekinetix.com/csp/api/json/inline/[projectId]/[cspid]/[language]/Product_Number_SKU/[partnumber]',
			targeturl: 'https://cspwebapi.tiekinetix.com/cspwebapi/api/json/inline/[projectId]/[cspid]/[language]/Local_Keywords/[partnumber]',
			templatefolder: "templates/",
			//resourcepath: "http://global.csp.syndicationtest.com/",
			resourcepath: "https://cspwebapi.tiekinetix.com/",
			projectid: ''
        }, options);
		
		//Load head resources
		(function($$) {
			try {
				var cspInlineCounter = $('.csp-inline-showcase').length;
				$$.loadCss(settings.resourcepath + "ui/cspinlinev2/css/style.css");
				// ltu 8/26/14 load custom css per project
				$$.loadCss(settings.resourcepath + "ui/cspinlinev2/css/custom_"+settings.projectid+".css");
				$$.loadCss(settings.resourcepath + "ui/cspinlinev2/css/custom_"+settings.cspid+".css");
				if (cspInlineCounter == 0) {
					$$.loadCss(settings.resourcepath + "ui/universalresources/cspfancybox/source/jquery.fancybox.css");
					$$.loadCss(settings.resourcepath + "ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-thumbs.css");
					$$.loadAndExecute(settings.resourcepath + "ui/universalresources/cspfancybox/source/jquery.fancybox.pack.js", function(f) {
						$$.loadAndExecute(settings.resourcepath + "ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-media.js", function(f) {
							$$.loadAndExecute(settings.resourcepath + "ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-thumbs.js", function(f) {
							})
						})
					});
				}
			} catch (exc) {}
		})(CSP_GLOBAL);
		
		var thisEl = null;
		var jsonObj = null;
		var prodTimer, assetTimer, blockTimer, callbackTimer;
		var kalturaPId = '';
		var kalturaUId = '';
		
		//Main plugin script
		return this.each( function() {
            if (settings.language == null || settings.partnumber == null || settings.cspid == null) {
				$(this).text('Invalid settings!');
				return;
			}
			
			if (settings.targeturl.toLowerCase().indexOf('[cspid]') == -1 || settings.targeturl.toLowerCase().indexOf('[language]') == -1 || settings.targeturl.toLowerCase().indexOf('[partnumber]') == -1) {
				$(this).text('Invalid targeturl!');
				return;
			}
			
			thisEl = $(this);
			if ($('.inline_showcase').length > 0 && !$(this).hasClass('inline_showcase')) {
				return;
			}
			
			if (!thisEl.hasClass('csp-inline-showcase')) {
				thisEl.addClass('csp-inline-showcase');
			}
			
			thisEl.empty();
			
			gThisEl = thisEl;
			gSettings = settings;
			ReGenerateUrl();
			DoRequest();
        });
		
		function ReGenerateUrl() {
			settings.targeturl = settings.targeturl.replace(/\[cspid\]/g, settings.cspid);
			settings.targeturl = settings.targeturl.replace(/\[language\]/g, settings.language);
			settings.targeturl = settings.targeturl.replace(/\[partnumber\]/g, settings.partnumber);
			settings.targeturl = settings.targeturl.replace(/\[projectId\]/g, settings.projectid);
		}
		
		function GetTemplate(fileName) {
			var elm = document.createElement("script");
			elm.setAttribute("type", "text/javascript");
			elm.src = settings.templatefolder + fileName;
			document.body.appendChild(elm);
		}
		
		function DoRequest() {
			$.ajax({
				type:"GET",
				url: settings.targeturl,
				data: {}, 
				dataType: "jsonp", 
				timeout: 200000,
				jsonp:"jsonp",
				done: function(data) { 
					debugger;
				},
				success: function(result) {
					jsonObj = result;
					//console.log(jsonObj);
					RenderContent();
				}, 
				error: function(result, s, e) {
					console.log(e);
				}
			});
		}
		
		function RenderContent() {
			var productObj = jsonObj.product;
			var assetObj = jsonObj.assets;
			var blockObj = jsonObj.blocks;

			//Render Product first
			if (productObj) {
				GetTemplate("product_template.html");
				prodTimer = setInterval(function(){RenderProd(productObj)},1000);
			}
			
			//Render blocks
			if (blockObj) {
				GetTemplate("block_template.html");
				blockTimer = setInterval(function(){RenderBlock(blockObj)},1000);
			}
			
			//Render Assets
			if (assetObj) {
				GetTemplate("asset_template.html");
				assetTimer = setInterval(function(){RenderAsset(assetObj)},1000);
			}
			
			callbackTimer = setInterval(function(){CallBackTrigger()},1000);
			/**
			setTimeout(function(){
				console.log("**** timeout call back");
				finishCallback();
			}, 10000);
			*/
		}
		
		function CallBackTrigger() {
			var isAllLoaded = false;
			for (var i = 0; i < gBlockOrders.length; i++) {
				if (gBlockOrders[gBlockOrders[i]] != undefined)
				isAllLoaded = gBlockOrders[gBlockOrders[i]].loaded;
			}
			if (!isAllLoaded) {return;}
			console.log('+++ Start CallBack +++');
			window.clearInterval(callbackTimer);
			finishCallback();
		}
		
		function IsTime(blockId) {
			var blockIndex = gBlockOrders.indexOf(blockId);
			if (blockIndex < 0) return false;
			if (!$.isArray(gBlockOrders[blockId])) return false;
			if (blockIndex == 0) {
				thisEl.append(gBlockOrders[blockId][0]);
				//gBlockOrders[blockId].loaded = true;
				return true;
			}
			else {
				var preBlock = gBlockOrders[blockIndex - 1];
				if (thisEl.find('#' + preBlock).length > 0) {
					thisEl.append(gBlockOrders[blockId][0]);
					//gBlockOrders[blockId].loaded = true;
					return true;
				}
			}
			return false;
		}
		
		function RenderProd(productObj) {
			if (!IsTime("product-detail-panel")) return;
			window.clearInterval(prodTimer);
			thisEl.find('#csp-detail-title').html(productObj.content_Title);
			thisEl.find('#csp-detail-title-description').html(productObj.content_Title_Secondary);
			
			//Product Image
			thisEl.find('#zoom-trigger').attr('href', productObj.file_Image_Url);
			thisEl.find('#csp-first-img').find('img').attr('src', productObj.file_Image_Thumbnail_Url);
			
			//Product desc & long desc
			thisEl.find('#csp-first-description-title').html(productObj.content_Description_Short);
			thisEl.find('#csp-first-description-detail').html(productObj.content_Description_Long);
			
			InitProductBlockEvents();
			gBlockOrders["product-detail-panel"].loaded = true;
		}
		
		function RenderAsset(assetObj) {
			if (!IsTime("asset-detail-panel")) return;
			window.clearInterval(assetTimer);
			//console.log(assetObj);
			for (var i = 0; i < assetObj.length; i++) {
				var assetTemplate = $('#asset-block-tpl').clone(true);
				var assetLink = assetObj[i].content_Link_Url;
				assetTemplate.attr('id', '');
				
				if (i % 2 == 1) assetTemplate.addClass('odd-block');
				else assetTemplate.addClass('even-block');
				
				if (assetObj[i].local_File_Type.toLowerCase() != 'video') {
					assetTemplate.find('.csp-asset-thumbnail').hide();
					assetTemplate.find('.csp-asset-description').addClass('full-size');
				}
				else {
					var vidId = InitVideoObj(assetObj[i]);
					var vidSize = GetVidURLParameter('width', assetObj[i].content_Link_Url) + 'x' + GetVidURLParameter('height', assetObj[i].content_Link_Url);
					assetTemplate.find('.csp-asset-thumbnail a').addClass(assetObj[i].local_File_LaunchType);
					assetTemplate.find('.csp-asset-thumbnail a').addClass('video');
					assetTemplate.find('.csp-asset-thumbnail a').attr({'href': assetObj[i].content_Link_Url, 'vidid': vidId, 'vidsize': vidSize, cspObj: "REPORT", cspType: "VIDEO", cspEnglishValue: assetObj[i].csP_Reporting_Id});
					assetTemplate.find('.csp-asset-thumbnail img').attr('src', assetObj[i].content_Image_Url);
					assetTemplate.find('.csp-asset-title a').addClass('video');
					assetTemplate.find('.csp-asset-title a').attr({'href': assetObj[i].content_Link_Url, 'vidid': vidId, 'vidsize': vidSize, cspObj: "REPORT", cspType: "VIDEO", cspEnglishValue: assetObj[i].csP_Reporting_Id});
				}
				
				if (assetObj[i].local_Featured.toLowerCase() == 'cobrand_html')
					assetLink = "http://209.134.51.131:9995/CobrandHTML2/" + settings.projectid + "/" + settings.cspid + "/" + encodeURIComponent(asset.linkurl);
				
				if (assetObj[i].local_File_Type.toLowerCase() != 'video')
					assetTemplate.find('.csp-asset-title a').attr({'href': assetLink, cspObj: "REPORT", cspType: "DOWNLOADS", cspEnglishValue: assetObj[i].csP_Reporting_Id});
					
				assetTemplate.find('.csp-asset-date').html(assetObj[i].content_Asset_Date);
				assetTemplate.find('.csp-asset-title a').addClass(assetObj[i].local_File_LaunchType + ' ' + assetObj[i].local_Featured.toLowerCase());
				assetTemplate.find('.csp-asset-title a').html(assetObj[i].content_Description_Short);
				assetTemplate.find('.csp-asset-long-desc').html(assetObj[i].content_Description_Long);
				thisEl.find('#asset-detail-panel').append(assetTemplate);
			}
			
			InitAssetBlockEvents();
			gBlockOrders["asset-detail-panel"].loaded = true;
		}
		
		function RenderBlock(blockObj) {
			if (!IsTime("block-detail-panel")) return;
			window.clearInterval(blockTimer);
			var videoCount = 0;
			for (var i = 0; i < blockObj.length; i++) {
				if (blockObj[i].status_Block_ID.toLowerCase() == 'video') {
					var demoVidItemTemplate = $('#demo-vid-tmp').clone(true);
					var vidId = InitVideoObj(blockObj[i]);
					var vidSize = GetVidURLParameter('width', blockObj[i].file_Video_Url) + 'x' + GetVidURLParameter('height', blockObj[i].file_Video_Url);
					demoVidItemTemplate.attr('id', '');
					demoVidItemTemplate.find('.video-demo-item').attr('src', blockObj[i].file_Image_Thumbnail_Url);
					//cspObj="REPORT" cspType="VIDEO" cspEnglishValue="{{firstVideoBlock.identifier}}"
					demoVidItemTemplate.find('.video-lightbox').attr({href: blockObj[i].file_Video_Url, vidid: vidId, vidsize: vidSize, cspObj: "REPORT", cspType: "VIDEO", cspEnglishValue: blockObj[i].csP_Reporting_Id});
					$('#csp-video-blocks ul.thumbnails').append(demoVidItemTemplate);
					videoCount++;
					continue;
				}
				var indexWithoutVideo = i - videoCount;
				var blockTemplate = $('#overview-block-tpl').clone(true);
				blockTemplate.attr('id', '');
				if (i == 0) blockTemplate.addClass('overview-first-block');
				else if (i == blockObj.length - 1) blockTemplate.addClass('overview-last-block');
				if (indexWithoutVideo % 2 == 1) blockTemplate.addClass('odd-block');
				else blockTemplate.addClass('even-block');
				blockTemplate.find('.csp-short-desc').html(blockObj[i].content_Description_Short);
				blockTemplate.find('.csp-long-desc').html(blockObj[i].content_Description_Long);
				blockTemplate.find('.overview-block-2 img').attr('src', blockObj[i].file_Image_Url);
				thisEl.find('#block-detail-panel').append(blockTemplate);
			}
			
			InitBlockEvents();
			gBlockOrders["block-detail-panel"].loaded = true;
		}
		
		function InitBlockEvents() {
			thisEl.find('#csp-video-blocks a.video-lightbox').one('click', function (e) {
				CallFancyBoxVideo(this);
				e.preventDefault();
			});
		};
		
		function InitAssetBlockEvents() {
			thisEl.find('#asset-detail-panel a.lightbox').one('click', function (e) {
				if ($(this).hasClass('video')) {
					CallFancyBoxVideo(this);
				}
				else
					CallFancyBoxPDF(this);
				e.preventDefault();
			});
		}
		
		function InitProductBlockEvents() {
			thisEl.find('#zoom-trigger').one('click', function (e) {
				e.preventDefault();
				CallFancyBoxImage(this);
			});
		}
		
		function CallFancyBoxImage(me) {
			$(me).fancybox({
				'type': 'image',
				'href': $(me).attr('href')
			}).click();
		}
		
		function CallFancyBoxPDF(me) {
			//console.log($(me).attr('href'));
			$(me).fancybox({
				type: 'iframe',
				openEffect  : 'none',
				closeEffect : 'none',
				iframe : {
					preload: false
				},
				'href': $(me).attr('href')
			}).click();
		}
		
		function CallFancyBoxVideo(me) {
			var videoSize = $(me).attr('vidsize').split('x');
			//console.log(videoSize, kalturaPId, kalturaUId);
			var videoId = $(me).attr('vidid');
			var videoWrapper = '<div id="all-video-wrapper"><div id="video_' + $(me).attr('vidid') + '"></div></div>';
			//return;
			$(me).fancybox({
				type: 'inline',
				autoSize: false,
				width: videoSize[0],
				height: videoSize[1],
				openEffect  : 'none',
				closeEffect : 'none',
				content: videoWrapper,
				afterShow: function(){
					setTimeout(function() {
						kWidget.embed({
							'targetId': 'video_' + videoId,
							'wid': '_' + kalturaPId,
							'uiconf_id' : kalturaUId,
							'entry_id' : videoId,
							'flashvars':{
								//'externalInterfaceDisabled' : false,
								'autoPlay' : false
							},
							'readyCallback': function( playerId ){
								window.kdp = $('#' + playerId).get(0);
							}
						});
					}, 1000);
				},
				afterClose: function () {
					try {
						kWidget.destroy(window.kdp);
						delete(window.kdp);
					} catch(exc) {}
				}
			}).click();
		}
		
		function InitVideoObj(vidObj) {
			var videoIdParamName = 'entry_id';
			var videoUrl = vidObj.content_Link_Url == undefined ? vidObj.file_Video_Url : vidObj.content_Link_Url;
			var videoId = GetVidURLParameter(videoIdParamName, videoUrl);
			var friendlyUrlPart = videoUrl.split('?')[0].split('/');
			var placeHolder = [];
			//console.log(videoUrl, videoId, friendlyUrlPart);
			if (gEmbededVids.indexOf(vidObj) == -1 && videoUrl.indexOf('embed') > 0) {
				gEmbededVids.push(vidObj);
				
				if ($('#kaltura-embeded-lib').length == 0) {
					$.getScript(videoUrl.split('?')[0], function() {
						//console.log('#Kaltura loaded');
					});
					/*
					var scriptElement = $('<script type="text/javascript" id="kaltura-embeded-lib">');
					scriptElement.attr('src', videoUrl.split('?')[0]);
					$('head').append(scriptElement);
					*/
					
					var script = document.createElement("script");
					script.type = "text/javascript";
					script.id = "kaltura-embeded-lib";
					script.src = videoUrl.split('?')[0];
					document.getElementsByTagName('head')[0].appendChild(script);
					for (var i = 0; i < friendlyUrlPart.length; i++) {
						if (friendlyUrlPart[i] == 'partner_id')
							kalturaPId = friendlyUrlPart[i + 1];
						if (friendlyUrlPart[i] == 'uiconf_id')
							kalturaUId = friendlyUrlPart[i + 1];
					}
				}
			}
			return videoId;
		}
		
		function GetVidURLParameter(name, givenstring) {
			return decodeURI(
				(RegExp('(^|&)' + name + '=(.+?)(&|$)').exec(givenstring)||[,,null])[2]
			);
		}
    }
	
	//$.cspinline = {};
	
	$.fn.cspinline.parsetemplate = function ($obj) {
		var htmlTags = $($obj.template);
		var firstDivId = htmlTags.attr('id');

		if (gBlockOrders.indexOf(firstDivId) >= 0) {
			gBlockOrders[firstDivId] = [];
			gBlockOrders[firstDivId].push($obj.template);
			gBlockOrders[firstDivId].loaded = false;
		}
	}
//}(jQuery));

}(CSP_GLOBAL.jQuery));


if (typeof (CSP_GLOBAL) != "undefined") {
    (function($$) {
        try {
            var scriptname = "mainApp.js";            
            $$.scriptMap[scriptname].retObj = CSP_GLOBAL.$.fn.cspinline;
            $$.scriptMap[scriptname].ready = true;
            $$.processQueue(scriptname);
        } catch (ex) {
            $$.log(ex);
        };
    })(CSP_GLOBAL);
}