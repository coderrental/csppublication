var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;
var ROOT_CATEGORY_ID = 134;
if(typeof(CSP_Root_Category)!="undefined"&&CSP_Root_Category!=""&&!isNaN(CSP_Root_Category)) 
	ROOT_CATEGORY_ID = CSP_Root_Category;
var PRODUCT_CATEGORY_ID = 134;
var WHAT_NEW_ITEM_LIMIT = 30;
var SYNDICATION_TYPE = 'intranet';
var COOKIES_INFO = {
	username: 'username',
	password: 'pwd'
}
var EMBEDED_IMG_COUNT = 6;
var MISSED_EMBEDED_IMG = [];
var STANDARD_PARTNER_TYPES = new Array("Reseller", "MSP", "Distributor");
var MAX_LEFT_MENU_ITEM = 4;
var PreviewVidWindowInterval;
var JWPLAYER_KEY = "NuEgzm5lRNspzDwGHcoqou3OgypbG5F0FkLvLA==";

/*
var API_URL = {
	General: 'http://cspwebapi.tiekinetix.com/cspwebapi_v3/api/json/src/v2/{prj}/{cid}/{ptype}/{langid}?jsonp=JSON_CALLBACK',
	Asset: 'http://cspwebapi.tiekinetix.com/cspwebapi_v3/api/json/src/{prj}/{cid}/{categoryid}/{ptype}/{langid}?jsonp=JSON_CALLBACK',
	Search: 'http://cspwebapi.tiekinetix.com/cspwebapi_v3/api/json/src/{prj}/{cid}/{ptype}/{langid}/search?query={keyword}&jsonp=JSON_CALLBACK'
};
*/
var API_URL = {
	General: 'http://cspwebapi.tiekinetix.com/cspwebapi_v5/api/json/src/v2/{prj}/{cid}/{ptype}/{langid}?jsonp=JSON_CALLBACK',
	Asset: 'http://cspwebapi.tiekinetix.com/cspwebapi_v5/api/json/src/{prj}/{cid}/{categoryid}/{ptype}/{langid}/{brandid}?jsonp=JSON_CALLBACK',
	Search: 'http://cspwebapi.tiekinetix.com/cspwebapi_v5/api/json/src/{prj}/{cid}/{ptype}/{langid}/search?query={keyword}&jsonp=JSON_CALLBACK'
};

String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	var regex; 
	for (var i = 0; i < find.length; i++) {
		regex = new RegExp(find[i], "g");
		replaceString = replaceString.replace(regex, replace[i]);
	}
	return replaceString;
};

jQuery.fn.preload = function() {
    this.each(function(){
        jQuery('<img/>')[0].src = this;
    });
}

function reAdjustColorBoxSize(minWidth, maxWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 970)
		wdWidth = 700;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if (maxWidth == undefined) maxWidth=0;
	if (maxWidth > 0 && wdWidth > maxWidth) {
		wdWidth = maxWidth;
	}
	
	if (wdWidth > jQuery(window).width())
		wdWidth = jQuery(window).width();
	if (wdHeight > jQuery(window).height())
		wdHeight = jQuery(window).height();
		
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

function ResetAssetVideoHolder() {
	var playerHolder = jQuery('#asset-preview-player');
	playerHolder.find('#asset-youtube-player').attr('src', '');
	playerHolder.find('#asset-youtube-player').hide();
	playerHolder.find('#asset-jwplayer').hide();
	playerHolder.find('#asset-jwplayer_wrapper').hide();
	try {
		jwplayer('asset-jwplayer').remove();
	} catch (exc) {}
	playerHolder.find('video').remove();
	playerHolder.find('iframe').remove();
}

function InitPreviewVideo (item) {
	ResetAssetVideoHolder();
	PreviewVidWindowInterval = setInterval(function(){ jQuery.colorbox.resize(); }, 1000);
	var playerHolder = jQuery('#asset-preview-player');
	var assetVidUrl = item.attr('href');
	var defaultWidth = 650;
	var defaultHeight = 340;
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || jQuery(window).width() < 600)
		defaultWidth = '100%';
	
	var innerWidth = defaultWidth - 20;
	var innerHeight = defaultHeight - 20;
	//http://entrypoints-production.herokuapp.com/videoplayer/?video=https://www.youtube.com/watch?v=AYWT_Goel5E&width=560&height=360&autostart=false&preview=seek&dcsid=dcs2221s4mdhdbskjujkvw9vp_1j3c&options=%7B%22project%22:%22groupemichelin%22,%22url%22:%22%23%22,%22src%22:%22%22,%22extra%22:%22%7B%5C%22width%5C%22:%20%5C%22560%5C%22,%20%5C%22height%5C%22:%20%5C%22360%5C%22%7D%22,%22CSP_ShowHover%22:0,%22protocol%22:%22http%22,%22SyndicationType%22:%22AssetSyndication%22,%22ElementType%22:%22video%22,%22ElementAction%22:%22inlineVideo%22,%22GlobalIDNumber%22:%2263556740045244634099885657%22,%22cid%22:13,%22cn%22:%22testCo%22,%22IntegratedType%22:%22%22,%22country%22:%22United%20States%22,%22banner%22:%22YT_MICHELIN_CrossClimate__a_brand_new_product__Live_in_Divonne%22,%22useConversionClick%22:true,%22sId%22:12,%22sName%22:%22Michelin%22,%22lngDesc%22:%22English%20Global%22%7D
	playerHolder.append('<iframe width="' + defaultWidth + '" height="' + defaultHeight + '" id="cspElement_63556740045244634089573939" src="http://entrypoints-production.herokuapp.com/videoplayer/?video=' + assetVidUrl + '&amp;width=' + innerWidth + '&amp;height=' + innerHeight + '&amp;autostart=false&amp;preview=seek" frameborder="0" allowfullscreen=""></iframe>');
	return;
}

function initJCarousel(targetLink, wrapperNode) {
	if (jQuery(window).width() > 480) {
		if (targetLink.hasClass('jcarouseled')) {
			wrapperNode.find('.jcarousel').jcarousel('destroy');
			wrapperNode.find('li').attr('style', '');
			if (wrapperNode.find('.fake-asset-item').length == 0 && wrapperNode.find('li').length % 2 == 1)
				jQuery('#trunk-wrapper .fake-asset-item').insertAfter(wrapperNode.find('li:last'));
			targetLink.removeClass('jcarouseled');
		}
		wrapperNode.find('div.csp-item-asset').removeClass('jcarousel');
		return;
	}
	
	wrapperNode.find('div.csp-item-asset').addClass('jcarousel');
	wrapperNode.find('.fake-asset-item').remove();
	var jcarousel = wrapperNode.find('.jcarousel');
	if (!targetLink.hasClass('jcarouseled')) {
		jcarousel
		.on('jcarousel:reload jcarousel:create', function () {
			var width = jQuery(window).width();//jcarousel.innerWidth();

			if (width >= 450) {
				width = width / 3;
			} else {
				width = width / 2;
			}
			width = width - 36;
			jcarousel.jcarousel('items').css('width', width + 'px');
		})
		.jcarousel({
			wrap: 'circular'
		});

		wrapperNode.find('.jcarousel-pagination')
		.on('jcarouselpagination:active', 'a', function() {
			jQuery(this).addClass('active');
		})
		.on('jcarouselpagination:inactive', 'a', function() {
			jQuery(this).removeClass('active');
		})
		.on('click', function(e) {
			e.preventDefault();
		})
		.jcarouselPagination({
			perPage: 1,
			item: function(page) {
				return '<a href="#' + page + '">' + page + '</a>';
			}
		});
		jcarousel.on( "swipeleft", function() { jcarousel.jcarousel('scroll', '+=1'); } );
		jcarousel.on( "swiperight", function() { jcarousel.jcarousel('scroll', '-=1'); } );
		targetLink.addClass('jcarouseled');
	}
	else {
		jcarousel.jcarousel('reload');
	}
}

function InitEmbededBanners(thisEl) {
	CURRENT_CSP_UNIQUE_KEY = thisEl.attr('cspuniquekey');
	reAdjustColorBoxSize(550);
	var thumbnailList = ApplyEmbededThumbnails(thisEl.siblings('.thumbnail-holder').text().split(','));
	
	var embededScriptSrc = thumbnailList.length > 0 ? jQuery('#embedscript-placeholder').text() : jQuery('#embededscript-withoutbanner').text();
	embededScriptSrc = embededScriptSrc.replace('[keyword]', CURRENT_CSP_UNIQUE_KEY);
	if (thumbnailList.length > 0) {
		var defaultThumbnail = thisEl.siblings('.default-thumbnail-holder').text()
		var liTemplate = '<li><div class="center-align"><img class="embeded-thumb selected" rel="Content_Image_Thumbnail" src="' + defaultThumbnail + '" /></div></li>';

		liTemplate = '<li><div class="center-align"><img class="embeded-thumb" rel="Content_Image_Thumbnail" src="' + defaultThumbnail + '" /></div></li>';
		jQuery('#banner-carousel').find('ul').append(liTemplate);
	}
	var scriptText = jQuery('#csp-generated-code-placeholder').val();
	scriptText = scriptText.replace('[embeded_script]', embededScriptSrc);
	jQuery('#csp-generated-code').val(scriptText);
	
	jQuery.colorbox({
		className: 'csp-asset-embed-detail', 
		width: wdWidth + "px",
		inline: true, 
		href: '#csp-asset-embed-holder', 
		onComplete: function() {
			var bannerCarousel = jQuery('#banner-carousel');
			bannerCarousel.jcarousel('destroy');
			bannerCarousel
			.on('jcarousel:reload jcarousel:create', function () {
				var width = jQuery('#csp-asset-embed-holder').width();
				bannerCarousel.jcarousel('items').css('width', width + 'px');
			})
			.jcarousel({
				wrap: 'circular'
			});
			bannerCarousel.jcarousel('scroll', 0);
			bannerCarousel.find('.jcarousel-pagination')
			.on('jcarouselpagination:active', 'a', function() {
				var currentIndex = bannerCarousel.find('.jcarousel-pagination a').index(jQuery(this));
				var activeItem = bannerCarousel.find(' > ul > li:eq(' + currentIndex + ')');
				var crrHeight = activeItem.find(' > div.center-align').outerHeight();
				activeItem.find('img.embeded-thumb').click();
				bannerCarousel.height(crrHeight);
				jQuery(this).addClass('active');
			})
			.on('jcarouselpagination:inactive', 'a', function() {
				jQuery(this).removeClass('active');
			})
			.on('click', function(e) {
				e.preventDefault();
			})
			.jcarouselPagination({
				perPage: 1,
				item: function(page) {
					return '<a href="#' + page + '">' + page + '</a>';
				}
			});
			ColorboxResizeInterval = setInterval(function(){ 
				jQuery.colorbox.resize(); 
				var width = jQuery('#csp-asset-embed-holder').width();
				jQuery('#banner-carousel').jcarousel('items').css('width', width + 'px');
			}, 1000);
		} ,
		onClose: function() {
			clearInterval(ColorboxResizeInterval);
		}
	});
}

function ApplyEmbededThumbnails(tList) {
	jQuery('#banner-carousel').find('ul li').remove();
	var isEmpty = true;
	for (var i = 0; i < tList.length; i++) {
		if (tList[i] != '') {
			isEmpty = false;
			var liTemplate = '<li><div class="center-align"><img class="embeded-thumb" rel="Embeded_Thumbnail_Image_' + (i + 1) + '" src="' + tList[i] + '" /></div></li>';
			jQuery('#banner-carousel').find('ul').append(liTemplate);
		}
	}
	if (isEmpty)
		tList = new Array();
	return tList;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

function sortNumber(a, b) {
    return a - b;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function AssetSecureCheck(cid, cbtype) {
	//Check if cookies disabled
	/*
	var cookieEnabled=(navigator.cookieEnabled)? true : false   
    if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){ 
        document.cookie="testcookie";
        cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false;
    }
	if (!cookieEnabled) {
		alert('Please enable your browser cookies');
		return;
	}
	//DNN_Portal_Id = '0';
	if (DNN_Portal_Id == '' || isNaN(DNN_Portal_Id)) {
		alert('Invalid DNN Portal Id');
		return;
	}
	
	if (getCookie(COOKIES_INFO.username) == '') {
		reAdjustColorBoxSize(500, 580);
		jQuery('#csp-auth-first-cid').val(cid);
		jQuery('#csp-auth-first-cbtype').val(cbtype);
		jQuery.colorbox({ className: 'asset-auth-form', width: wdWidth + "px", height: "300px", inline: true, href: jQuery('#csp-login-form-holder-inner')});
	}
	else 
	{
		jQuery.post(("dnnservices/api/user/UserAuthenticate"), { username: getCookie(COOKIES_INFO.username), password: window.btoa(getCookie(COOKIES_INFO.password)), portalid: DNN_Portal_Id, instance: ProjectId, contentid: cid, fieldname: "Content_Link_Url" }, function (response) {
			TreatAssetLink(response.ContentValue, cbtype);
        });
	}
	*/
	reAdjustColorBoxSize(500, 580);
	jQuery('#csp-auth-first-cid').val(cid);
	jQuery('#csp-auth-first-cbtype').val(cbtype);
	jQuery.colorbox({ className: 'asset-auth-form', width: wdWidth + "px", height: "300px", inline: true, href: jQuery('#csp-login-form-holder-inner')});
}

function CspAuthSubmit() {
	jQuery.post(("dnnservices/api/user/UserAuthenticate"), { username: jQuery('#csp-user').val(), password: jQuery('#csp-password').val(), portalid: DNN_Portal_Id, instance: ProjectId, contentid: jQuery('#csp-auth-first-cid').val(), fieldname: "Content_Link_Url" }, function (response) {
		if (response.User == null) {
			alert(response.StatusMessage);
			return;
		}
		var exdays = 1;
		//setCookie(COOKIES_INFO.username, jQuery('#csp-user').val(), exdays);
		//setCookie(COOKIES_INFO.password, window.atob(jQuery('#csp-password').val()), exdays);
		//TreatAssetLink(response.ContentValue, jQuery('#csp-auth-first-cbtype').val());
		TriggerLinkByLoadType(response.ContentValue, jQuery('#csp-auth-first-cid').val());
		jQuery.colorbox.close();
	});
}

function TreatAssetLink (assetLink, cbtype) {
	if (assetLink == '') return;
	if (cbtype.toLowerCase()=='cobrand_html')
		assetLink = "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(assetLink);
		//assetLink = "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(assetLink);
	//window.location = assetLink;
	TriggerLinkByLoadType(assetLink, jQuery('#csp-auth-first-cid').val());
}

function TriggerLinkByLoadType(targetLink, cid) {
	var targetElement = jQuery('[cid="' + cid + '"]:first');
	if (targetElement.length == 0) return;
	
	var launchTypeClass = targetElement.attr('class') != undefined ? targetElement.attr('class') : '';
	//var launchTypeClass = targetElement.attr('ng-class') != undefined ? targetElement.attr('ng-class') : '';
	
	if (targetElement.attr('cobrandtype').toLowerCase() == 'cobrand_html')
		targetLink = "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(targetLink);
		//targetLink = "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(targetLink);
	if (launchTypeClass.indexOf('newwindow') != -1)
		window.open(targetLink);
	else if (launchTypeClass.indexOf('lightbox') != -1) {
		reAdjustColorBoxSize(650);
		var videoid = targetLink.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
		if(videoid != null) {
			jQuery.colorbox({width: wdWidth + "px", height: wdHeight + "px", iframe:true, href: 'http://www.youtube.com/embed/' + videoid[1]});
		} else { 
			jQuery.colorbox({width: wdWidth + "px", height: wdHeight + "px", iframe:true, href: targetLink});
		}
	}
	else
		window.location = targetLink;
}

var intranetAssetModule = angular.module('ngIntranetAssets', ['ngSanitize']).config(function($routeProvider, $locationProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/brand/:brandid', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/brand/:brandid/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/category/:categorykey', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/category/:categorykey/brand/:brandid', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/category/:categorykey/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/category/:categorykey/brand/:brandid/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/brand/:brandid', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/brand/:brandid/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey/brand/:brandid', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/partner-type/:partnertype/category/:categorykey/brand/:brandid/i/:tick', {controller:MainCtrl, templateUrl:'ui/assetsv2/custom/Shared/v3/main.html'}).
		when('/search/:searchkey', {controller:SearchCtrl, templateUrl:'ui/assetsv2/custom/Shared/search.html'}).
		when('/search/:searchkey/brand/:brandid', {controller:SearchCtrl, templateUrl:'ui/assetsv2/custom/Shared/search.html'}).
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i >= 0)
					return path.substring(i + 9);
			} catch(e) {}
			return '/';
		} });
});
/*
intranetAssetModule.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$first && scope.$last){
		console.log(angular.element(element).parent('ul').siblings('span.sub-menu-toggle'));
		angular.element(element).parent('ul').siblings('span.sub-menu-toggle').remove();
		//if (jQuery(element).parent('ul').
    }
  };
})
*/
intranetAssetModule.directive('myBlockResize', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			var maxHeight = 0;
			var blocksWrapper = jQuery(angular.element(element)).parent('ul');
			setTimeout(function() {
				blocksWrapper.find('li').each(function() {
					if (jQuery(this).outerHeight() > maxHeight)
						maxHeight = jQuery(this).outerHeight();
				});
				blocksWrapper.find('li').each(function() {
					jQuery(this).outerHeight(maxHeight);
				});
			}, 1500);
		}
	};
});

intranetAssetModule.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

intranetAssetModule.factory('Searcher', function ($rootScope, $http) {
	var dataCategories = [];
	var dataAssets = [];
	var resultCategories = [];
	var resultAssets =[];
	
	return {
		allCategories: function () {
			return dataCategories;
		},
		allAssets: function () {
			return dataAssets;
		},
		setCategories: function (dataList) {
			dataCategories = dataList;
		},
		setAssets: function (dataList) {
			dataAssets = dataList;
		},
		getAssetResult: function () {
			return resultAssets;
		},
		getCategoryResult: function () {
			return resultCategories;
		},
		doSearch: function (keyWord) {
			resultCategories = [];
			resultAssets = [];
			var lowerKeyWord = keyWord.toLowerCase();
			var searchAPI = API_URL.Search.replace('{keyword}', lowerKeyWord);
			
			$http.jsonp(searchAPI).success(function(responseData) {
				resultAssets = responseData.searchResults;
				$rootScope.SearchResultAsset = resultAssets;
			});
			/*
			for (var k = 0; k < dataAssets.length; k++) {
				if (dataAssets[k].content_Title.toLowerCase().indexOf(lowerKeyWord) >= 0 || dataAssets[k].content_Description_Long.toLowerCase().indexOf(lowerKeyWord) >= 0)
					resultAssets.push(dataAssets[k]);
			}
			*/
			/*
			for (var i = 0; i < dataCategories.length; i++) {
				var isFound = false;
				if (dataCategories[i].content_Title.toLowerCase().indexOf(lowerKeyWord) >= 0 || dataCategories[i].content_Description_Long.toLowerCase().indexOf(lowerKeyWord) >= 0)
					isFound = true;
				if (isFound)
					resultCategories.push(dataCategories[i]);
			};
			*/
		}
	}
});

intranetAssetModule.factory('TranslationData', function ($http) {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		}
	}
});

intranetAssetModule.factory('Translation', function () {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
});

intranetAssetModule.factory('Products', function ($rootScope, $filter, $http, $routeParams) {
	
	var rootId = ROOT_CATEGORY_ID;
	var categoryProductId = PRODUCT_CATEGORY_ID;
	var data = [];
	var partnerTypes = [];
	var checkedList = [];
	var brandData = [];
	var currentBrandCategories = [];
	return {
		promise: {},
		rootid: function () {
			return rootId;
		},
		setrootid: function (newrootid) {
			rootId = newrootid;
		},
		mainproductid: function () {
			return categoryProductId;
		},
		branddata: function () {
			return brandData;
		},
		getbrandcategories: function () {
			return $rootScope.GetByValue(brandData,$rootScope.CurrentBrandId).categories;
		},
		setbrandcategories: function (data) {
			if (typeof (data.brandsWithCategories) != 'undefined')
				currentBrandCategories = $rootScope.GetByValue(data.brandsWithCategories, $rootScope.CurrentBrandId).categories;
		},
		all: function () {
			return {categories: data, partnerTypes: partnerTypes};
		},
		setData: function (productList) {
			if (typeof ($routeParams.partnertype) != "undefined") {
				$rootScope.CurrentPartnerType = $routeParams.partnertype;
			}
			else if (typeof ($rootScope.CurrentPartnerType) != "undefined" || $rootScope.CurrentPartnerType == undefined || $rootScope.CurrentPartnerType == '') {				
				$rootScope.CurrentPartnerType = productList.partnerTypes[0].localPartnerType;
			}
			
			if ($rootScope.DisablePartnerTypesFilter)
				$rootScope.CurrentPartnerType = 'All';
			
			for (var i = 0; i < productList.partnerTypes.length; i++) {
				if (productList.partnerTypes[i].localPartnerType == $rootScope.CurrentPartnerType) {
					$rootScope.AllAssets = productList.partnerTypes[i].assets;
					break;
				}
			}
			
			var tmpData = $rootScope.GetByValue(productList.brandsWithCategories,$rootScope.CurrentBrandId).categories;
			//currentBrandCategories = tmpData;
			rootId = parseInt(tmpData[0].rootCategoryId);
			
			brandData = productList.brandsWithCategories;
			
			data = productList.categories;
			partnerTypes = productList.partnerTypes;
		},
		convertJSONtoTree: function (arry) {			
			var foundRootNode = false;
			var rootNode = [
				{
					"content_Title" : "Root",
					"categoryId" : rootId,
					"parentCategoryId" : ""
				}
			]
			
			for (var r = 0; r < arry.length; r++) {
				if (arry[r].categoryId == rootId) {
					foundRootNode = true;
					rootNode = arry[r];
					arry[r].parentCategoryId = '';
					break;
				}
			}
			
			var fullArray = foundRootNode == false ? rootNode.concat(arry) : arry;
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentCategoryId,
					target = !p ? roots : (children[p] || (children[p] = []));
				target.push({ value: item });
			}
			
			// function to recursively build the tree
			var findChildren = function(parent, depth) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i], depth + 1);
						parent.children[i].value.depth = parseInt(depth);
						$rootScope.MenuItemStat['c_' + parent.children[i].value.categoryId] = [];
						$rootScope.MenuItemStat['c_' + parent.children[i].value.categoryId] = $rootScope.CurrentCategoryKey == parent.children[i].value.categoryId;
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var j = 0, len = roots.length; j < len; ++j) {
				findChildren(roots[j], 2);
				roots[j].value.depth = 1;
			}
			
			if (roots.length == 1)
				return roots[0].children;
			
			for (var k = 0; k < roots.length; k++) {
				if (roots[k].value.categoryId == rootId)
					return roots[k].children;
			}
		},
		filterByAsset: function (entry) {
			var isValid = false;
			var totalCount = parseInt(entry.value.assetCount);
			var isBelong = false;
			for (var i = 0; i < currentBrandCategories.length ; i++){
				if(entry.value.categoryid == currentBrandCategories.categoryid)
				{
					isBelong = true;
					break;
				}
			}
			if (totalCount > 0 && isBelong == true)
				return true;
			
			
			if (entry.children != undefined) {
				for (var i = 0; i < entry.children.length; i++) {
					isValid = this.filterByAsset(entry.children[i]);
					if (isValid) {
						return true;
					}
				}
			}
			
			return isValid;
		},
		filterByPartner: function (entry) {

			var isValid = false;
			var entryId = 0;
			
			if (typeof(entry.value) == "undefined")
				entryId = parseInt(entry.categoryId);
			else
				entryId = parseInt(entry.value.categoryId);

			if ($rootScope.CurrentPartnerType == '') {
				for (var j = 0; j < $rootScope.LocalPartnerTypes.length; j++) {
					if ($rootScope.LocalPartnerTypesCategories[$rootScope.LocalPartnerTypes[j]].indexOf(entryId) >= 0) {
						return true;
					}
				}
			}
			else {
				var totalCount = $filter('filter')($rootScope.AllAssets, {categoryId: entryId}, function (expected, actual) {
					var expectedIds = expected.split(',');
					for (var i = 0; i < expectedIds.length; i++) {
						if (parseInt(expectedIds[i]) == parseInt(actual))
							return true;
					}
					return false;
				}).length;
				if (totalCount > 0)
					return true;
			}
			
			if (entry.children != undefined) {
				for (var i = 0; i < entry.children.length; i++) {
					isValid = this.filterByPartner(entry.children[i]);
					if (isValid) {
						return true;
					}
				}
			}
			
			return isValid;
		}
	}
});

intranetAssetModule.factory('Asset', function ($http, $rootScope) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (assetList) {
			//data = assetList;
			var resultData = $rootScope.ParseAsset(assetList);
			data = JSON.parse(resultData);
		}
	};
});

intranetAssetModule.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

intranetAssetModule.filter('filterByCategory', function () {
    return function (items, categoryId) {
        var newItems = [];		
        for (var i = 0; i < items.length; i++) {
			var cspCatIds = items[i].categoryId.split(',');
			for (var key = 0; key < cspCatIds.length; key++) {
				if (parseInt(cspCatIds[key]) == parseInt(categoryId)) {
					newItems.push(items[i]);
				}
			}
        };
        return newItems;
    }
});

intranetAssetModule.factory('CSPLanguage', function () {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		}
	}
});

intranetAssetModule.directive('eatClick', function() {
	return function(scope, element, attrs) {
		jQuery(element).click(function(event) {
			event.preventDefault();
		});
	}
});




intranetAssetModule.run(function ($rootScope, $q, $http, $routeParams, $location, $filter, Translation, Products, Asset, LeadgenForm, Searcher, CSPLanguage) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
		var temp = currentUrl.split('/');
		for (var i = 0; i < temp.length; i++) {
			if (temp[i] == 'i') {
				currentTick = '&i=' + temp[i + 1];
				break;
			}
		}
	}
    
    // handle the customer parnter type
    if (ConsumerPartnerType == "") 
        ConsumerPartnerType = "all";
    else
        ConsumerPartnerType = ConsumerPartnerType.replace(",","||");
	
	$rootScope.MaxLeftMenuLevel = MAX_LEFT_MENU_ITEM;
	$rootScope.pageTitle = 'Home';
	$rootScope.AllProducts = [];
	$rootScope.ProductUrl = '';
	$rootScope.MainMenuItems = [];
	$rootScope.AllAssets = [];
	$rootScope.NotAssetProducts = [];
	$rootScope.LocalPartnerTypes = [];
	$rootScope.LocalPartnerTypeStat = [];
	$rootScope.MenuItemStat = [];
	$rootScope.LocalPartnerTypesCategories = new Array();
	$rootScope.LocalPartnerTypesAssets = [];
	
	$rootScope.AllTabsAndNavigation = [];
	$rootScope.Dictionary = [];
	$rootScope.rootProductId = Products.mainproductid();
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.AjaxLoading = true;
	$rootScope.CurrentPartnerType = '';
	$rootScope.CurrentCategoryKey = '';
	$rootScope.CurrentCategoryItem = [];
	$rootScope.FeaturedAssets = [];
	$rootScope.ConsumerPartnerId = ConsumerPartnerId;
	$rootScope.ProjectId = ProjectId;
	$rootScope.IsSearch = false;
	$rootScope.SearchResultCategory = [];
	$rootScope.SearchResultAsset = [];
	$rootScope.SearchResultShowLimit = 3;
	$rootScope.DisableAssetDownload = false; 
	if (typeof(CSPDisableAssetDownload) != 'undefined')
		$rootScope.DisableAssetDownload = CSPDisableAssetDownload.toLowerCase() == 'yes';
	$rootScope.EnableAssetSyndication = EnableAssetSyndication.toLowerCase() == 'yes';
	$rootScope.EnableLanguageSelection = CSPEnableLanguageSelection.toLowerCase() == 'yes';
	$rootScope.LanguageList = [];
	$rootScope.BrandList = [];
	$rootScope.CurrentCspLang = {
		Id: '',
		Code: '',
		Desc: ''
	};
	$rootScope.CurrentBrandId= '';
	$rootScope.CurrentBrandName = '';
	$rootScope.GlobalLabel = {
		privacypolicy: 'privacypolicy',
		termsofuse: 'termsofuse',
		privacypolicylink: 'privacypolicylink',
		termsofuselink: 'termsofuselink',
		LearnMore: 'learnmore',
		EmbedThisAsset: 'Embed Asset',
		Back: 'Back',
		CopyCodeToClipboard: 'Copy Code',
		CodeCopied: 'Code copied',
		BannerSelect: 'pleaseselectabanner',
		Watch: 'Watch'
	};
	$rootScope.AssetSyndicationUrl = document.location.protocol + "//" + document.location.host + "/Csp/?mfrname="+ProjectId+"&t=AssetSyndication&keyword=[keyword]&banner=[embeded_thumbnail]";
	$rootScope.AssetSyndicationUrlWithoutBanner = document.location.protocol + "//" + document.location.host + "/Csp/?mfrname="+ProjectId+"&t=AssetSyndication&keyword=[keyword]";
	$rootScope.CurrentCategoryKey = (typeof ($routeParams.categorykey) != "undefined") ? $routeParams.categorykey : '';
	$rootScope.DisablePartnerTypesFilter = false;
	
	//Due to CSP-3405
	/*
	if (typeof (CompanyName) != 'undefined' && CompanyName.toLowerCase().indexOf('michelin') != -1)
		$rootScope.DisablePartnerTypesFilter = true;
	*/
	
	$rootScope.byPartnerType = function (entry) {
		var isInPartnerList = Products.filterByAsset(entry);
		return isInPartnerList;	
	};
	
	$rootScope.DoSearch = function (eventTarget) {
		var minLength = 3;
		var searchKey = eventTarget.currentTarget.value;
		$rootScope.SearchResultCategory = [];
		$rootScope.SearchResultAsset = [];
		if (searchKey.length < minLength) return;
		
		Searcher.doSearch(searchKey);
		
		setTimeout(function() {
			jQuery("#csp-search-result-list a.result-title").highlight(searchKey);
		}, 700);
	};
	
	$rootScope.GetTranslation = function (text) {
		return Translation.translate (Translation.all(), text);
		//return "test";
	};
	
	$rootScope.GetPartnerTypeLink = function (pType) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/partner-type/' + pType + '/brand/' + $rootScope.CurrentBrandId;
	};
	
	$rootScope.ValidAssetKeys = function (assetItem) {	
		if (assetItem.local_File_LaunchType.toLowerCase() == 'noembedded') return false;
		var assetUniqueKey = typeof(assetItem.csP_Unique_Key) == 'undefined' ? '' : assetItem.csP_Unique_Key;
		var assetKeyword = typeof(assetItem.local_Keywords) == 'undefined' ? '' : assetItem.local_Keywords;
		var tmpKeys = assetUniqueKey + assetKeyword;
		
		tmpKeys = tmpKeys.trim();
		return tmpKeys != '';
	}

	$rootScope.GetAssetUrl = function(asset) {
		if (asset.status_Secure_Y_N != undefined && asset.status_Secure_Y_N.toLowerCase() == 'yes')
			return '#';
		try {
			if (asset.local_Featured && asset.local_Featured.toLowerCase()=='cobrand_html')
				return "http://cspwebapi.tiekinetix.com/cspwebapi_v2/api/html/cobrand/" + ProjectId + "/"+ConsumerPartnerId+"/?url=" + encodeURIComponent(asset.content_Link_Url);
				//return "http://209.134.51.131:9995/CobrandHTML2/" + ProjectId + "/" + ConsumerPartnerId + "/" + encodeURIComponent(asset.linkurl);
			else
				return encodeURI(asset.content_Link_Url);
		} catch(ex) {
			return asset.content_Link_Url;
		}
	};
	
	$rootScope.GetSearchCategoryUrl = function (searchItem) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + searchItem.categoryId;
	}
	
	$rootScope.GetAssetThumbnail = function (itemThumbnail) {
		return (itemThumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(itemThumbnail);
	};

	$rootScope.DecodeHtml = function(items) {
		for(var i=0;i<items.length;i++) {
			items[i].title = jQuery("<div />").html(items[i].title).text();
			items[i].description = jQuery("<div />").html(items[i].description).text();
		}
		return items;
	};
	
	$rootScope.GetDecodedHTML = function(htmlString) {
		var txt = document.createElement("textarea");
		txt.innerHTML = htmlString;
		return txt.value;
	}
	
	$rootScope.RemoveHtml = function(item) {
		var temp = jQuery("<div />").html(item).text();
		var r = temp;
		try {
			r = (jQuery(temp).text()!=''?jQuery(temp).text():temp);
		}
		catch(e) {
			r = temp;
		}		
		return  r;		
	};
	
	$rootScope.GetAssetEmbededThumbnails = function (assetItem) {
		var imgList = '';
		for (var i = 1; i <= EMBEDED_IMG_COUNT; i++) {
			if (typeof(assetItem['embeded_Thumbnail_Image_' + i]) == 'undefined') continue;
			if (imgList != '') imgList += ',';
			imgList += assetItem['embeded_Thumbnail_Image_' + i];
		}
		return imgList;
	}
	
	var GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	}
	
	$rootScope.GoCspLang = function (langItem) {
		$rootScope.CurrentCspLang = langItem;
		jQuery('#csp-mobile-lang-list').hide();
		
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var reDirectUrl = '';
		var tmp = baseDomain.split('-');
		baseDomain = tmp[tmp.length - 1];
		
		if (baseDomain.indexOf('c') != 0)
			baseDomain = 'c' + baseDomain;
		
		baseDomain = 'l' + langItem.Code.toLowerCase() + '-p' + ProjectId.toLowerCase() + '-' + baseDomain;
		
		reDirectUrl = crrUrlInfo[0] + '//' + baseDomain;
		
		for (var i = 3; i < crrUrlInfo.length; i++)
			reDirectUrl += '/' + crrUrlInfo[i];
		
		window.location = reDirectUrl;
	};
	
	$rootScope.GoBrand = function (brand) {
		
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var reDirectUrl = '';
		var isBrandExist = false;
		
		reDirectUrl = crrUrlInfo[0] + '//' + baseDomain;
		
		for (var i = 3; i < crrUrlInfo.length; i++) {
			if (crrUrlInfo[i] == 'category' || crrUrlInfo[i - 1] == 'category')
				continue;
			
			if (crrUrlInfo[i - 1] == 'brand') {
				reDirectUrl += '/' + brand.id;
				isBrandExist = true;
			}
			else
				reDirectUrl += '/' + crrUrlInfo[i];
		}
		
		if (!isBrandExist)
			reDirectUrl += 'brand/' + brand.id;
		//console.log(reDirectUrl);
		window.location.href = reDirectUrl;
		//window.location.reload();
		
		/*
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var reDirectUrl = '';
		
		reDirectUrl = crrUrlInfo[0] + '//' + baseDomain + '/brand/' + brand.id;
		
		window.location = reDirectUrl;
		*/
	}
	
	$rootScope.getBrandName = function (brandID){
		for(var i = 0; i < $rootScope.BrandList.length; i ++){
			if($rootScope.BrandList[i].id == brandID)
				return $rootScope.BrandList[i].name;
		}
	};
	
	$rootScope.checkBandId = function (brandID){
		for(var i = 0; i < $rootScope.BrandList.length; i ++){
			if($rootScope.BrandList[i].id == brandID)
				return true;
		}
		return false;
	}
	
	$rootScope.GetBrandUrl = function (brand) {
		var currentUrl = document.URL;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/brand/' + brand.id;
	};
	
	$rootScope.GetCurrentBrand = function () {
		//Get brand list & current brand if exist
		$rootScope.CurrentBrandId = $routeParams.brandid;
		if (typeof($routeParams.brandid) == 'undefined')
		{
			$rootScope.CurrentBrandId = $rootScope.BrandList[0].id;
		}
		
		if($rootScope.BrandList.length > 0 && !($rootScope.checkBandId($rootScope.CurrentBrandId)))
			$rootScope.CurrentBrandId = $rootScope.BrandList[0].id;

		$rootScope.CurrentBrandName = ($rootScope.getBrandName($rootScope.CurrentBrandId));
	}
	$rootScope.GetByValue = function(arr, value) {
	  var result = [];

	  arr.forEach(function(o){if (o.supplier.id == value) result.push(o);} );

	  return result? result[0] : null; // or undefined
		
	}
	
	var GetCurrentLangCode = function () {
		var crrUrlInfo = location.href.split('/');
		var baseDomain = crrUrlInfo[2];
		var crrLangCode = '';
		var regexLang = /^l\w{2}/g;
		var tmp = baseDomain.split('-');
		
		for (var i = 0; i < tmp.length; i++) {
			if (tmp[i].match(regexLang)) {
				crrLangCode = tmp[i].substr(1);
				break;
			}
		}
		
		return crrLangCode != '' ? crrLangCode : cspConsumerInfo.lng.toLowerCase();
	};
	
	var StartTranslateGlobalLabel = function () {
		for (label in $rootScope.GlobalLabel)
			$rootScope.GlobalLabel[label] = $rootScope.GetTranslation($rootScope.GlobalLabel[label]);
	};
	
	$http.get('d10.aspx').success(function(responseData) {
		cspLangDeferred.resolve(responseData);
	});

	var deferred = $q.defer();

	Products.promise = deferred.promise.then(function (productList) {
		Products.setData(productList);
		return Products.all();
	});
	
	//Get parameters on first load
	$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
		API_URL.General = API_URL.General.replaceArray(["{prj}", "{cid}", "{langid}"], [ProjectId, cspConsumerInfo.companies_Id, cspConsumerInfo.lId]);
		API_URL.Search = API_URL.Search.replaceArray(["{prj}", "{cid}","{ptype}", "{langid}"], [ProjectId, cspConsumerInfo.companies_Id, ConsumerPartnerType, cspConsumerInfo.lId]);
		API_URL.Asset = API_URL.Asset.replaceArray(["{prj}", "{cid}", "{langid}"], [ProjectId, cspConsumerInfo.companies_Id, cspConsumerInfo.lId]);
		// getting categories and assets
		$rootScope.ProductUrl = API_URL.General.replace('{ptype}', ConsumerPartnerTypeList[0]);
		if ($rootScope.DisablePartnerTypesFilter)
			$rootScope.ProductUrl = API_URL.General.replace('{ptype}', 'All');
		
		$http.jsonp($rootScope.ProductUrl).success(function(responseData) {
			$rootScope.BrandList = responseData.brands;
			$rootScope.GetCurrentBrand();
			Products.setbrandcategories(responseData);
			if (ConsumerPartnerType == 'all') {
				ConsumerPartnerTypeList = [];
				for (var dt = 0; dt < responseData.partnerTypes.length; dt++) {
					ConsumerPartnerTypeList.push(responseData.partnerTypes[dt].localPartnerType);
				}
			}
			
			if (typeof(ConsumerPartnerTypeList[0]) != 'undefined' && !isNaN(ConsumerPartnerTypeList[0]))
				ConsumerPartnerTypeList.sort(function(a, b){return parseInt(a) - parseInt(b)});
			
			$rootScope.LocalPartnerTypes = ConsumerPartnerTypeList;
			if ($rootScope.DisablePartnerTypesFilter) {
				$rootScope.LocalPartnerTypes = ["All"];
			}
			
			if (typeof ($routeParams.partnertype) == "undefined") {
				$rootScope.LocalPartnerTypeStat[$rootScope.LocalPartnerTypes[0]] = true;
			}
			if (ConsumerPartnerType != 'all')
				deferred.resolve(responseData);
			else {
				if (document.URL.indexOf('partner-type/') == -1)
				{
					$rootScope.ProductUrl = API_URL.General.replace('{ptype}', ConsumerPartnerTypeList[0]);
					$http.jsonp($rootScope.ProductUrl).success(function(responseDataByPartner) {
						deferred.resolve(responseDataByPartner);
					});
				}
			}
		});
	});
	
	//CSPLanguage
	var cspLangDeferred = $q.defer();
	CSPLanguage.promise = cspLangDeferred.promise.then(function (dataList) {
		CSPLanguage.setData(dataList);
		$rootScope.LanguageList = dataList;
		if (dataList.length > 0) {
			var crrLangCode = GetCurrentLangCode();
			if (crrLangCode == '')
				$rootScope.CurrentCspLang = dataList[0];
			else {
				$rootScope.CurrentCspLang = $filter('filter')(dataList, {Code: crrLangCode}, function(actual, expected) {
					return actual.toLowerCase() == expected.toLowerCase();
				});
				if ($rootScope.CurrentCspLang.length == 0)
					$rootScope.CurrentCspLang = dataList[0];
				else
					$rootScope.CurrentCspLang = $rootScope.CurrentCspLang[0];
			}
		}
		
		return dataList;
	});
	
	/*
	var assetDeferred = $q.defer();
	Asset.promise = assetDeferred.promise.then(function (assetList) {
		Asset.setData(assetList);
		return Asset.all();
	});
	
	$http.get('d2.aspx?p2004(ct21000&cd' + Products.rootid() + 'i99)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
	//$http.get('d2.aspx?p2004(ct21000&cd189i1)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		assetDeferred.resolve(responseData);
	});
	*/
	
	var translationDeferred = $q.defer();
	Translation.promise = translationDeferred.promise.then(function (dataList) {
		Translation.setData(dataList);
		StartTranslateGlobalLabel();
		return dataList;
	});
	
	$http.get('d1.aspx?p2005(ct35000)[]' + currentTick).success(function(responseData) {
		translationDeferred.resolve(responseData);
	});
	
	var leadgenFormDeferred = $q.defer();
	LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
		if (data.length <= 1) return null;
		LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
		LeadgenForm.setFields(data[1].contactusformfields);
		return data;
	});
	// $http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		// leadgenFormDeferred.resolve(responseData);
	// });

	$rootScope.getClass = function (obj) {
		var className = "";
		for (var i = obj.length - 1; i >= 0; i--) {
			if (obj[i].value.categoryId==$rootScope.CurrentCategoryKey) {
				className = "sub-menu-show";
				break;
			}
		};
		return className;
	};
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:

					if ($rootScope.CurrentCategoryItem!=null&&typeof($rootScope.CurrentCategoryItem.value)!="undefined") 
						$rootScope.pageTitle = $rootScope.RemoveHtml($rootScope.CurrentCategoryItem.value.title);
					else 
						$rootScope.pageTitle = defaultTitle;
			}
		}

		try {
			//handle deep links
			if (window.location.hash&&window.location.hash.indexOf('#/#')==0) {						
				try {
					var url = decodeURIComponent(window.location.hash.substring(2));
					window.location = url;
				} catch(e) {}
			}			
			jQuery('#csp-report-lib').remove();
			
			window.setTimeout(function() {
				try {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				jQuery(document).attr('title', $rootScope.pageTitle);
				document.getElementsByTagName('head')[0].appendChild(script);
				} catch (err) {}
			}, 1000);
			
			try{
				parent.postMessage(window.location.href,"*");
			}catch(ex){console.log(ex);};
		} catch (Err) {
			console.log(Err);
		}
	};
});

function MainCtrl($rootScope, $scope, $q, $http, $filter, $routeParams, $window, URLHandler, Asset, Products, Translation, Searcher) {
	tickValue = URLHandler.getTick();
	$rootScope.CurrentCategoryKey = (typeof ($routeParams.categorykey) != "undefined") ? $routeParams.categorykey : '';
	$scope.PartnerSwitched = false;
	$scope.BrandSwitched = false;
	
	for (var ptStat in $rootScope.LocalPartnerTypeStat)
		$rootScope.LocalPartnerTypeStat[ptStat] = false;
	
	//Switch partner type
	if (typeof ($routeParams.partnertype) != "undefined" && $routeParams.partnertype != $rootScope.CurrentPartnerType) {
		$scope.PartnerSwitched = true;
		$rootScope.CurrentPartnerType = $routeParams.partnertype;
		$rootScope.ProductUrl = API_URL.General.replace('{ptype}', $rootScope.CurrentPartnerType);
			
		var deferred = $q.defer();

		Products.promise = deferred.promise.then(function (productList) {
			Products.setData(productList);
			return Products.all();
		});
		
		$http.jsonp($rootScope.ProductUrl).success(function(responseData) {
			deferred.resolve(responseData);
		});
	}
	
	if (typeof ($routeParams.brandid) != "undefined" && $routeParams.brandid != $rootScope.CurrentBrandId) {
		$scope.BrandSwitched = true;
		$rootScope.GetCurrentBrand();
		// Products.setbrandcategories(data);
	}
	
	if ($rootScope.DisablePartnerTypesFilter)
		$rootScope.CurrentPartnerType = "All";
	
	$rootScope.LocalPartnerTypeStat[$rootScope.CurrentPartnerType] = true;
	$rootScope.AjaxLoading = true;
	$rootScope.IsSearch = false;
	$rootScope.SearchResultCategory = [];
	$rootScope.SearchResultAsset = [];
	$scope.currentCategoryTitle = '';
	$scope.assetCount = 0;
	$scope.crrAssetList = [];
	$scope.whatNewList = [];
	$scope.categoryBlocks = [];
	$scope.whatNewLimitTo = WHAT_NEW_ITEM_LIMIT;
	$scope.whatNewStartDate = new Date();
	$scope.CategoryAssets = [];
	$scope.ResetMenuStat = false;
	$scope.ShowSubMenuInterval;
	$scope.isResponsive = false;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || jQuery(window).width() < 460) {
		$scope.isResponsive = true;
	}
	var today = new Date();
	$scope.whatNewStartDate.setDate(today.getDate() - $scope.whatNewLimitTo);
	
	$scope.label = {
		WhatNew: 'whatsnew',
		WhatNewDescription: 'whatsnewdesc',
		categorypageinfo: 'categorypageinfo',
		firstcategorypageinfo: '',
		lastcategorypageinfo: '',
		LearnMore: 'learnmore'
	};
	
	$scope.GetCategoryDescription = function (categoryblock) {
		var cateDesc = categoryblock.value.content_Description_Long;
		if (cateDesc == undefined)
			cateDesc = categoryblock.value.content_Description_Short;
		
		return cateDesc == undefined ? '' : $rootScope.GetDecodedHTML(cateDesc);
	}
	
	$scope.myCategoryBlockOrder = function (categoryBlock) {
		var val = parseInt(categoryBlock.value.status_Sort_Order);
		return val;
	}
	
	$scope.assetListOrder = function(assetitem){
		var val = parseInt(assetitem.status_Sort_Order);
		return val;
	}
	$scope.GetInlineAsset = function(assetitem) {
		if(assetitem.local_File_LaunchType == "inline" && assetitem.local_File_Type == "text"){
			return assetitem.content_Link_Url;
		}
		else{
			return assetitem.content_Description_Long;
		}
	}
	
	
	$scope.customFilter = function (categoryId) {
		return function (item) {
			var realResult = $filter('filterByCategory')($scope.crrAssetList, categoryId);			
			if ($scope.CategoryAssets[categoryId] == undefined) {
				$scope.CategoryAssets[categoryId] = realResult.length;
				$scope.assetCount += realResult.length;
			}
			return realResult.indexOf(item) >= 0;
		};
	};
	
	// $scope.GetAssetThumbnail = function (itemThumbnail) {
		// return (itemThumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(itemThumbnail);
	// };
	
	$scope.GetAssetThumbnail = function (assetitem) {
		if(assetitem.local_File_LaunchType == "inline" && assetitem.local_File_Type == "text"){
			return encodeURI("http://209.134.51.144/test/textIcon.png");
		}
		else
			return (assetitem.content_Image_Thumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(assetitem.content_Image_Thumbnail);
	};
	
	$scope.GetButtonLabel = function (assetItem) {
		return $rootScope.GetTranslation('download');
	};
	
	$scope.SetFakeItemStatus = function (categoryId, itemCount) {
		var totalCount = 0;
		if (itemCount > 0)
			totalCount = itemCount;
		else {
			var assetByPartner = $scope.crrAssetList;//$filter('filter')($scope.crrAssetList, {localpartnertype: $rootScope.CurrentPartnerType}, function (expected, actual) {return expected.indexOf(actual) >= 0;});
			totalCount = $filter('filter')(assetByPartner, {cspcategoryid: categoryId}, function (expected, actual) {
				var expectedIds = expected.split(',');
				for (var i = 0; i < expectedIds.length; i++) {
					if (parseInt(expectedIds[i]) == parseInt(actual))
						return true;
				}
				return false;
			}).length;
		}
		if (totalCount % 2 == 1)
			return true;
		return false;
	};
	
	var StartTranslating = function () {
		for (label in $scope.label) {
			$scope.label[label] = $rootScope.GetTranslation($scope.label[label]);
			if ($scope.label[label].indexOf('[assets_count]') >= 0) {
				$scope.label.firstcategorypageinfo = $scope.label[label].split('[assets_count]')[0];
				$scope.label.lastcategorypageinfo = $scope.label[label].split('[assets_count]')[1];
			}
		}
	};
	
	var findInChildrenList = function (catItem, childrenList) {
		var isChild = false;
		if (childrenList == undefined || childrenList.length == 0)
			return isChild;
		if (childrenList.indexOf(catItem) >= 0)
			isChild = true;
		else {
			for (var i = 0; i < childrenList.length; i++) {
				isChild = findInChildrenList(catItem, childrenList[i].children);
				if (isChild) break;
			}
		}
		return isChild;
	};
	
	var GetCategoryInTree = function (prodTree) {
		for (var i = 0; i < prodTree.length; i++) {
			if (parseInt(prodTree[i].value.categoryId) == parseInt($rootScope.CurrentCategoryKey)) {
				$rootScope.CurrentCategoryItem = prodTree[i];
				return;
			}
			if (prodTree[i].children != undefined)
				GetCategoryInTree(prodTree[i].children);
		}
	};
	
	var isNewCategoryAsset = function (assetItem, categoryNode) {
		var isNewAsset = false;
		if (categoryNode == undefined)
			return false;

		// ltu 01/27/14: categoryNode is an empty array
		if(typeof(categoryNode.value)=="undefined") 
			return false;

		if (parseInt(assetItem.cspcategoryid) == parseInt(categoryNode.value.categoryId))
			isNewAsset = true;
		else if (categoryNode.children != undefined) {
			for (var i = 0; i < categoryNode.children.length; i++) {
				isNewAsset = isNewCategoryAsset(assetItem, categoryNode.children[i]);
				if (isNewAsset) {
					break;
				}
			}
		}
		
		return isNewAsset;
	}
	
	$scope.RunResetMenuItemStat = function(crrId) {
		for (var k in $rootScope.MenuItemStat) {
			$rootScope.MenuItemStat[k] = false;
		}
		$rootScope.MenuItemStat['c_' + crrId] = true;
	}
	
	if ($rootScope.MenuItemStat.length > 0) {
		$scope.RunResetMenuItemStat($rootScope.CurrentCategoryKey);
		$scope.ResetMenuStat = true;
	}
	
	Translation.promise.then(function (translationData) {
		StartTranslating();
		
		Products.promise.then(function(data){
			for (var i = 0; i < data.partnerTypes.length; i++) {
				if (data.partnerTypes[i].localPartnerType == $rootScope.CurrentPartnerType) {
					$rootScope.AllAssets = data.partnerTypes[i].assets;
					$scope.crrAssetList = $rootScope.AllAssets;
					break;
				}
			}
			
			
			if ($scope.PartnerSwitched || $scope.BrandSwitched) {
				if ($scope.BrandSwitched) {
					Products.setbrandcategories(data);
					if (typeof (Products.getbrandcategories()[0]) != 'undefined')
						Products.setrootid(Products.getbrandcategories()[0].rootCategoryId);
				}
				
				$rootScope.AllProducts = Products.convertJSONtoTree(data.categories);
				//console.log(data.categories, $rootScope.AllProducts);
				if (typeof $rootScope.AllProducts != 'undefined') {
					$rootScope.MainMenuItems = $rootScope.AllProducts.slice(0);
				}
			}
			
			//Get featured assets by partner type
			$rootScope.FeaturedAssets = $filter('filter')($rootScope.AllAssets, {status_Featured_Y_N: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual.toLowerCase();});
			
			if ($rootScope.CurrentCategoryKey != '') {
				//$rootScope.GetCurrentBrand();
				var assetUrl = API_URL.Asset.replaceArray(["{categoryid}", "{ptype}", "{brandid}"], [$rootScope.CurrentCategoryKey, $rootScope.CurrentPartnerType, $rootScope.CurrentBrandId]);
				$http.jsonp(assetUrl).success(function(responseData) {
					$rootScope.AllAssets = typeof(responseData.partnerTypes[0]) != 'undefined' ? responseData.partnerTypes[0].assets : [];
					$scope.crrAssetList = $rootScope.AllAssets;

					
					var categoryTree = $rootScope.MainMenuItems;//Products.convertJSONtoTree(data.categories)
					GetCategoryInTree(categoryTree);
					if ($rootScope.CurrentCategoryItem.length > 0) {
						$scope.currentCategoryTitle = $rootScope.CurrentCategoryItem.value.content_Title;
						$rootScope.updatePageInfo(null, $scope.currentCategoryTitle);
					}
					
					$scope.categoryBlocks.push($rootScope.CurrentCategoryItem);
					if (!$scope.ResetMenuStat)
						$scope.RunResetMenuItemStat($rootScope.CurrentCategoryKey);
					
					if (jQuery('ul.sub-menu-show').length == 0) {
						$scope.ShowSubMenuInterval = setInterval(function(){ 
							if (jQuery('#csp-left-section-inner .active-menu-item').length > 0) {
								var closestUl = jQuery('#csp-left-section-inner .active-menu-item').parents('ul.sub-menu:first');
								var closestToggleSpan = closestUl.siblings('span.sub-menu-toggle');
								jQuery('#csp-left-section-inner .active-menu-item').parents('ul.sub-menu').addClass('sub-menu-show');
								if (closestToggleSpan.length > 0)
									closestToggleSpan.addClass('arrow-up');
								clearInterval($scope.ShowSubMenuInterval);
							}
						}, 1000);
					}
				
					$rootScope.AjaxLoading = false;
				});
			}
			else {
				
				// load what's news section here
				//Get Whats New List
				var whatNewData = $scope.crrAssetList.slice(0);
				var newByDate = $filter('filter')(whatNewData, {content_Asset_Date: $scope.whatNewStartDate}, function (expected, actual) {
					return new Date(expected) >= actual;
				});
				
				$scope.whatNewList = newByDate;
				jQuery('#csp-left-section-inner .active-menu-item').removeClass('active-menu-item');
				jQuery('#csp-left-section-inner ul.sub-menu-show').removeClass('sub-menu-show');
				jQuery('#csp-left-section-mobile-inner').css('min-height', jQuery('#csp-body').outerHeight() + jQuery('#csp-header').outerHeight());
				
				$rootScope.updatePageInfo(this);
				$rootScope.AjaxLoading = false;
			}
		});
		
	});
	
	jQuery('#csp-search-key').val('');
}

function SearchCtrl($rootScope, $scope, $http, $filter, $routeParams, URLHandler, Asset, Products, Translation, Searcher) {
	$rootScope.IsSearch = true;
	$scope.searchKey = typeof($routeParams.searchkey)=="undefined"?'':$routeParams.searchkey;
	if ($scope.searchKey == '') {
		var currentUrl = document.URL;
		window.location = currentUrl.substring(0, currentUrl.indexOf('#/'));
		return;
	}
	$rootScope.AjaxLoading = true;
	
	$scope.lowerSearchKey = $scope.searchKey.toLowerCase();
	$scope.searchResult = [];
	$scope.crrAssetList = [];
	$scope.label = {
		WhatNew: 'whatsnew',
		WhatNewDescription: 'whatsnewdesc',
		categorypageinfo: 'categorypageinfo',
		firstcategorypageinfo: '',
		lastcategorypageinfo: '',
		LearnMore: 'learnmore'
	};
	$scope.productReadyCheck;
	
	$scope.GetInlineAsset = function(assetitem) {
		if(assetitem.local_File_LaunchType == "inline" && assetitem.local_File_Type == "text"){
			return assetitem.content_Link_Url;
		}
		else{
			return assetitem.content_Description_Long;
		}
	}
	$scope.GetAssetThumbnail = function (assetitem) {
		if(assetitem.local_File_LaunchType == "inline" && assetitem.local_File_Type == "text"){
			return encodeURI("http://209.134.51.144/test/textIcon.png");
		}
		else
			return (assetitem.content_Image_Thumbnail == '') ? 'http://placehold.it/100x140' : encodeURI(assetitem.content_Image_Thumbnail);
	};
	var StartTranslating = function () {
		for (label in $scope.label) {
			$scope.label[label] = $rootScope.GetTranslation($scope.label[label]);
			if ($scope.label[label].indexOf('[assets_count]') >= 0) {
				$scope.label.firstcategorypageinfo = $scope.label[label].split('[assets_count]')[0];
				$scope.label.lastcategorypageinfo = $scope.label[label].split('[assets_count]')[1];
			}
		}
	};
	
	//Reset menu stat
	for (var k in $rootScope.MenuItemStat) {
		$rootScope.MenuItemStat[k] = false;
	}
	jQuery('#csp-left-section-inner .active-menu-item').removeClass('active-menu-item');
	jQuery('#csp-left-section-inner ul.sub-menu-show').removeClass('sub-menu-show');
	
	$scope.LoadSearchResult = function () {
		$scope.searchResult = $rootScope.SearchResultCategory;
		$scope.crrAssetList = $rootScope.SearchResultAsset;
		setTimeout(function() {
			jQuery("body p").highlight($scope.searchKey);
			jQuery(document).click();
			resizeAssetBlocks(jQuery('.csp-item-asset'));
			jQuery('#csp-search-key').val($scope.searchKey);
			jQuery('#mobile-search-key').val($scope.searchKey);
		}, 1000);
		$rootScope.AjaxLoading = false;
	}
	
	Translation.promise.then(function (translationData) {
		StartTranslating();
		if ($rootScope.SearchResultAsset.length == 0) {
			var searchAPI = API_URL.Search.replace('{keyword}', $scope.searchKey);
			$http.jsonp(searchAPI).success(function(responseData) {
				$rootScope.SearchResultAsset = responseData.searchResults;
				$scope.LoadSearchResult();
			});
		}
		else {
			$scope.LoadSearchResult();
		}
	});
}

function ContactUsCtrl($rootScope, $scope, $routeParams, $timeout, $http, $location, LeadgenForm, Translation, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	$scope.ajaxLoading = false;
	$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		if (LeadgenForm.submit('form1', $scope.submitted) == 1)
			$scope.ajaxLoading = true;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
}

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Products, Asset, Translation, URLHandler) {
	if (typeof($routeParams.searchkey) != "undefined") return;
	$rootScope.CurrentCategoryKey = (typeof ($routeParams.categorykey) != "undefined") ? $routeParams.categorykey : '';
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.ValidChildren = function (menuItem) {
		if (menuItem.children == undefined || menuItem.value.depth != 3) return false;
		for (var i = 0; i < menuItem.children.length; i++) {
			if (parseInt(menuItem.children[i].value.assetCount) > 0) return true;
		}
		return false;
	}
	
	$scope.LeftMenuItemClicked = function (item) {
		jQuery('ul.sub-menu').removeClass('sub-menu-show');
		jQuery('.menu-item-' + item.value.categoryId).parents('ul.sub-menu').addClass('sub-menu-show');
	}
	
	$scope.myDisplayOrder = function (menuItem) {
		var val = parseInt(menuItem.value.status_Sort_Order);
		return val;
	}
	
	$scope.GetNavItemLink = function (navItem) {
		//TODO: need a better solution for this exception
		if (ProjectId.indexOf('michelin') != -1 || ProjectId == 'assettest') {
			if (navItem.value.depth == 2 && navItem.children != undefined) return '';
		}
		var currentUrl = document.URL;
		if ($rootScope.CurrentPartnerType != '')
			return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/partner-type/' + $rootScope.CurrentPartnerType + '/category/' + navItem.value.categoryId + '/brand/' + $rootScope.CurrentBrandId;
		return currentUrl.substring(0, currentUrl.indexOf('#/')) + '#/category/' + navItem.value.categoryId + '/brand/' + $rootScope.CurrentBrandId;
	};
	
	Products.promise.then(function (resultData) {
		$rootScope.AllProducts = Products.convertJSONtoTree(resultData.categories);
		if (typeof $rootScope.AllProducts != 'undefined') {
			$rootScope.MainMenuItems = $rootScope.AllProducts.slice(0);
			if ($rootScope.CurrentCategoryKey != '')
				$rootScope.MenuItemStat['c_' + $rootScope.CurrentCategoryKey] = true;
		}
		$rootScope.AjaxLoading = false;
	});
}
jQuery(function(){
	jQuery('body').on('click', 'span.sub-menu-toggle', function (e) {
		jQuery(this).siblings('ul').toggle();
		jQuery(this).bind('selectstart', function(){ return false; });
		e.stopPropagation();
		if(jQuery(this).hasClass('arrow-up')){
			jQuery(this).removeClass('arrow-up');
		}
		else{
			jQuery(this).addClass('arrow-up');
		}
	});
	
});