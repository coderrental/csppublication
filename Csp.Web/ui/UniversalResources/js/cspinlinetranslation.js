if(typeof(CSP_GLOBAL)!="undefined") {
	(function(cspObj){
		if (typeof(cspObj.CspTranslation)=="undefined")
			cspObj.CspTranslation = {"en":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"de":{"video":"Video","flash":"Flash Showcase","imagegallery":"Galerie","features":"Eigenschaften","pdf":"PDF"} ,"fr":{"video":"Vidéo","flash":"Présentation Flash","imagegallery":"Galerie","features":"Caractéristiques","pdf":"PDF"} ,"es":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"it":{"video":"Video","flash":"Vetrina Flash","imagegallery":"Galleria","features":"Caratteristiche","pdf":"PDF"} ,"p99":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"pt":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ja":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"zh":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"tr":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"nl":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Kenmerken","pdf":"PDF"} ,"da":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"no":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"sv":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ru":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ko":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eza":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"uk":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ca":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eie":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eau":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ptb":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"esl":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"cs":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"zht":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"enu":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"esc":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"}}; 
	})(CSP_GLOBAL);

	(function($$) {
	    try {
	        var scriptname = "cspinlinetranslation.js";
	        $$.scriptMap[scriptname].retObj = {};
	        $$.scriptMap[scriptname].ready = true;
	        $$.processQueue(scriptname);
	    } catch (ex) {
	        $$.log(ex);
	    };
	})(CSP_GLOBAL);    
}

