//
//  cspFlyout.js
//
//  Created by vroman on 2012-03-16.
//  Last update 2014-02-05
//  Copyright 2012-2014 vroman. All rights reserved.
//
var cspFo = null;
(function($) {
    $.fn.flyout = function(options) {
        (function() {
            // remove layerX and layerY
            // NOTE: remove this function patch after upgrading to jquery 1.7
            var all = $.event.props, len = all.length, res = [];
            while (len--) {
                var el = all[len];
                if (el != 'layerX' && el != 'layerY')
                    res.push(el);
            }
            $.event.props = res;
        } ());
        
        if (!cspFo && options && options.url && options.url != "") {
            cspFo = new cspFlyout();
            setTimeout(function(){cspFo.preloadContent($, options.url);}, 1);
        }

        if (typeof options.closeOnClick != "undefined" && options.closeOnClick) {
            return $(this)
                .css('cursor', 'pointer')
                .unbind('mouseover.tb')
                .bind('mouseover.tb', function(e) {
                    showFlyout(e, options);
                    // report(options);
                    return false;
                });
        } else {
            var patt1 = /SPAN\_\d+/;
            var patt2 = /\_opt\d+/;
            return $(this)
                .css('cursor', 'pointer')
                .unbind('mouseover.tb')
                .bind('mouseover.tb', function(e) {
                    var currentTarget = (window.event) ? e.srcElement : e.target;
                    var relatedTarget = (e.relatedTarget) ? e.relatedTarget : e.toElement;
                    if (typeof relatedTarget == 'undefined' 
                    && currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    || relatedTarget.tagName == "HTML" 
                    && currentTarget.tagName == "SPAN" &&  patt1.test(currentTarget.id)
                    || relatedTarget.tagName == "DIV" &&  relatedTarget.id != "mask" 
                    && currentTarget.tagName == "SPAN" &&  patt1.test(currentTarget.id)
                    || currentTarget.tagName == "SPAN" &&  patt1.test(currentTarget.id)
                    && relatedTarget.id == ""
                    || relatedTarget.tagName == "HTML" 
                    && currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    || relatedTarget.tagName == "DIV" &&  relatedTarget.id == "mask" 
                    && currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    || currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    && relatedTarget.tagName == "DIV" &&  relatedTarget.id != "mask" 
                    && relatedTarget.className != "showcase"
                    || currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    && relatedTarget.id == ""
                    || currentTarget.tagName == "A" &&  patt2.test(currentTarget.id)
                    || currentTarget.tagName == "IMG" &&  patt2.test(currentTarget.id)
                    && relatedTarget.tagName == "IMG" &&  patt2.test(relatedTarget.id)) {
                        showFlyout(e, options);
                    }
                    disabledEventPropagation(e);
                    return false;
                })
                .unbind('mouseout.tb')
                .bind('mouseout.tb', function(e) {
                    var currentTarget = (window.event) ? e.srcElement : e.target;
                    var relatedTarget = (e.relatedTarget) ? e.relatedTarget : e.toElement;
                    if (typeof relatedTarget == 'undefined' || !relatedTarget) {
                        hideFlyout(e);
                        return false;
                    }
                    if (currentTarget.nodeName == 'SPAN' && patt1.test(currentTarget.id)
                    && relatedTarget.nodeName == 'IMG' && patt2.test(relatedTarget.id)
                    || currentTarget.nodeName == 'IMG' && patt2.test(currentTarget.id)
                    && relatedTarget.nodeName == 'SPAN' && patt1.test(relatedTarget.id)
                    ||  currentTarget.nodeName != 'IMG' && !patt2.test(currentTarget.id)  
                    && relatedTarget.nodeName == 'DIV' && relatedTarget.id == "showcase"
                    || currentTarget.nodeName == 'IMG' && patt2.test(currentTarget.id) 
                    && relatedTarget.nodeName == 'DIV' && relatedTarget.id == "mask"
                    || currentTarget.nodeName != 'DIV' && currentTarget.id != "showcase" 
                    && relatedTarget.nodeName == 'DIV' && relatedTarget.id == "mask"
                    || currentTarget.nodeName == 'DIV' && currentTarget.id == "mask"
                    && relatedTarget.nodeName == 'SPAN' && patt1.test(relatedTarget.id)) {
                        return false;
                    }
                    hideFlyout(e);
                    return false;
                });
        }
    };
    
    var showFlyout = function(evt, options) {
        var evt = evt ? evt : window.event ? event : null;
        var x = null, y = null;
        if (evt) {
            x = evt.clientX ? evt.clientX : 1;
            y = evt.clientY ? evt.clientY : 1;
        }
        if (x && y && options
            && options.url && options.url != ""
            && options.height && options.height != "" && parseInt(options.height)
            && options.width && options.width != "" && parseInt(options.width) > 0) {
            if (!cspFo) {
                cspFo = new cspFlyout();
            }
            cspFo.setFlyout(x, y, options, evt.currentTarget);
            disabledEventPropagation(evt);
        }
    };

    var hideFlyout = function() {
        if (cspFo) {
            cspFo.closeFlyout();
        }
    };
    

    var disabledEventPropagation = function (event) {
        if (event.stopPropagation) {
            event.stopPropagation();
        } else if (window.event) {
            window.event.cancelBubble = true;
        }
    };
})(CSP_GLOBAL.jQuery);

if (typeof (CSP_GLOBAL) != "undefined") {
    (function($$) {
        try {
            var scriptname = "csp_flyout.js";
            $$.scriptMap[scriptname].ready = true;
            $$.processQueue(scriptname);
        } catch (ex) {
            $$.log(ex);
        };
    })(CSP_GLOBAL);
}

function cspFlyout() {
    var a = this;
    a.mainObj = null;
    a.contentObj = null;
    a.preloadedContentData = null;
    a.beakObj = null;
    a.closeObj = null;
    a.maskObj = null;
    a.baseUrl = null;
    a.link = null;
    a.reportingOptions = null;
    a.init = function() {
        a.mainObj = document.createElement("div");
        a.mainObj.className = "mainFlyout";
        a.mainObj.id = "mainFlyout";
        a.contentObj = document.createElement("div");
        a.contentObj.className = "mainFlyoutIFrame";
        a.contentObj.scrolling = "no";
        a.contentObj.border = '0';
        a.contentObj.id = "flyoutIFrame";
        a.maskObj = document.createElement("div");
        a.maskObj.id = "mask";
        a.maskObj.innerHTML = "&nbsp;";
        a.maskObj.onmouseout = function() {
            if (cspFo) {
                cspFo.closeFlyout();
            }
        };
        a.maskObj.onclick = function() {
            var extra = CSP_GLOBAL.tryEval(cspFo.reportingOptions.options.extra);
            if (cspFo.reportingOptions.options.ElementAction == "lightbox") {
                CSP_GLOBAL.$.fancybox({
                    width : (CSP_GLOBAL.Environment.isMobile() ? window.innerWidth - 50 : parseInt(extra.width)+10),
                    height : (CSP_GLOBAL.Environment.isMobile() ? window.innerHeight - 50 : extra.height),
                    href: cspFo.link,
                    type: 'iframe'
                });
            } else if (cspFo.reportingOptions.options.ElementAction == "newwindow") {
                window.open("http://" + cspFo.reportingOptions.options.url, "_blank", "menubar=0,location=0,status=0,scrollbars=1,toolbar=0,width=" + (parseInt(extra.width) + 20) + ",height=" + extra.height);   
            }
            cspFo.WTReport(cspFo.reportingOptions);
            cspFo.closeFlyout();
        };

        document.body.appendChild(a.maskObj);
        a.closeObj = document.createElement("div");
        a.closeObj.id = "flyoutCloseBtn";
        a.maskObj.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        a.closeObj.onclick = function() {
            if (cspFo) {
                cspFo.closeFlyout();
            }
        };
        a.mainObj.appendChild(a.closeObj);
        a.mainObj.appendChild(a.contentObj);
        a.beakObj = document.createElement("div");
        a.beakObj.id = "flyoutBeak";
        a.beakObj.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        // a.mainObj.appendChild(a.beakObj);
        document.body.appendChild(a.beakObj);
        document.body.appendChild(a.mainObj);
    };
    a.setFlyout = function(x, y, options, t) {
        if (!this.mainObj) {
            this.init();
        }
        this.reportingOptions = options;
        var patt1 = /SPAN\_\d+/;
        if (t.tagName.toLowerCase() == "span" && patt1.test(t.id) 
        || t.tagName.toLowerCase() == "div" && t.className == "showcase") {
            if (t.getElementsByTagName("IMG").length > 0){
                t = t.getElementsByTagName("IMG")[0];
            } else {
                t = t.getElementsByTagName("A")[0];
            }
        }
        var td = this.getTargetDimmensions(t);
        var w = parseInt(options.width);
        var h = parseInt(options.height);
        this.setFlyoutPosition(x, y, h, w, td);
        this.mainObj.style.width = w + "px";
        this.mainObj.style.height = h + "px";
        if (this.preloadedContentData) {
            this.setFlyoutContent(this.preloadedContentData);
        } else {
            this.setFlyoutContent(options.url);
        }
        this.contentObj.style.width = w + "px";
        this.contentObj.style.height = h + "px";
        this.beakObj.style.display = this.mainObj.style.display = "block";
        if (this.beakObj.className == "left") {
            this.maskObj.style.width = (w + 30) + "px";
            this.maskObj.style.height = h + "px";
            this.maskObj.style.left = (parseInt(this.mainObj.style.left) - 30) + "px";
        } else if (this.beakObj.className == "right") {
            this.maskObj.style.width = (w + 30) + "px";
            this.maskObj.style.height = h + "px";
            this.maskObj.style.right = (parseInt(this.mainObj.style.right) - 30) + "px";
        } else if (this.beakObj.className == "top") {
            this.maskObj.style.width = w + "px";
            this.maskObj.style.height = (h + 30) + "px";
            this.maskObj.style.top = (parseInt(this.mainObj.style.top) - 30) + "px";
        } else if (this.beakObj.className == "down") {
            this.maskObj.style.width = w + "px";
            this.maskObj.style.height = (h + 30) + "px";
            this.maskObj.style.down = (parseInt(this.mainObj.style.down) - 30) + "px";
        }
        this.maskObj.style.display = "block";
        var cspWT = null;
        for(var key in CSP_GLOBAL) {
            if (key == "wt" + options.options.project) {
                cspWT = key;    
            }
        }
        if (cspWT) {
            var original = CSP_GLOBAL[cspWT].DCSext.ConversionShown;
            CSP_GLOBAL[cspWT].DCSext.ConversionShown += "/flyout"; 
            CSP_GLOBAL[cspWT].DCSext.ConversionContent += "/flyout";
            CSP_GLOBAL[cspWT].dcsCollect();
            CSP_GLOBAL[cspWT].DCSext.ConversionShown = original; 
            CSP_GLOBAL[cspWT].DCSext.ConversionContent = original;
        }
    };
    a.getTargetDimmensions = function(t) {
        var r = {};
        r.width = t.clientWidth ? t.clientWidth : t.offsetWidth;
        r.height = t.clientHeight ? t.clientHeight : t.offsetHeight;
        var cp = this.getTargetPosition(t); 
        r.center = {};
        r.center.x = cp[0] + (r.width / 2);
        r.center.y = cp[1] + (r.height / 2);
        r.left = cp[0];
        r.top = cp[1];
        return r;
    };
    a.getTargetPosition = function(obj) {
        var curleft = curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return [curleft, curtop];
        }
    };
    a.setFlyoutContent = function(content) {
        if (content.indexOf("http://") == 0) {
            if (this.contentObj.src != content) {
                this.contentObj.src = content;
            }
        } else {
            this.contentObj.innerHTML = content;
        }
    };
    a.setFlyoutPosition = function(x, y, h, w, td) {
        var horizontalBeakWidth = 20, verticalBeakHeight = 15, verticalBeakCompensation = 2, browserScrollBarWidth = 20;
        var verticalCompensation = 9;
        var loc = this.calculateLeftPosition(x, y, h, w, td);
        var elementVerticalCenter = td.center.y;
        var visibleArea = this.getBrowserVisibleArea();
        if (loc != "top") {
            if (elementVerticalCenter > visibleArea.top) {
                if (elementVerticalCenter + h/2 < visibleArea.top + visibleArea.height) {
                    this.maskObj.style.top = this.mainObj.style.top = Math.max(visibleArea.top, elementVerticalCenter - (h / 2)) + "px";
                    this.setBeakTop(elementVerticalCenter);
                } else {
                    if (td.top + verticalBeakHeight < visibleArea.top + visibleArea.height - verticalCompensation - browserScrollBarWidth) {
                        this.maskObj.style.top = this.mainObj.style.top = (visibleArea.top + visibleArea.height - h - verticalCompensation - browserScrollBarWidth) + "px";
                        this.setBeakTop(td.top);
                    } else {
                        this.calculateLeftPosition(x, y, h, w, td, "down");
                        this.maskObj.style.top = this.mainObj.style.top = (elementVerticalCenter - (td.height/2 + h + horizontalBeakWidth)) + "px";
                        this.setBeakTop(td.top - verticalBeakHeight - verticalBeakCompensation); 
                    }
                }
            } else {
                this.maskObj.style.top = this.mainObj.style.top = (visibleArea.top + verticalBeakCompensation) + "px";
                this.setBeakTop(td.top + td.height - verticalBeakHeight);
            }
        } else {
            if (visibleArea.top > td.top 
            || visibleArea.top + h + verticalBeakHeight > td.top) {
                this.maskObj.style.top = this.mainObj.style.top = (elementVerticalCenter + verticalBeakHeight + td.height/2 - verticalBeakCompensation) + "px";
                this.setBeakTop(elementVerticalCenter + verticalBeakCompensation + td.height/2);
            } else {
                this.beakObj.className = "down";
                this.maskObj.style.top = this.mainObj.style.top = (elementVerticalCenter - (td.height/2 + h + horizontalBeakWidth)) + "px";
                this.setBeakTop(td.top - verticalBeakHeight - verticalBeakCompensation);
            }
        }
    };
    a.calculateLeftPosition = function(x, y, h, w, td, move) {
        var loc = "left", move = typeof move == 'undefined' ? null : move;
        var horizontalBeakWidth = 11, horizontalBeakCompensation = 9, browserScrollBarWidth = 20;
        var visibleArea = this.getBrowserVisibleArea();
        if (this.closeObj) {
            this.closeObj.style.left = (w - 27) + "px";
        }
        if (move && move == "left" 
        || !move && td.left + td.width + w + horizontalBeakWidth + horizontalBeakCompensation + browserScrollBarWidth < visibleArea.left + visibleArea.width) {
            this.beakObj.className = "left";
            this.maskObj.style.left = this.mainObj.style.left = (horizontalBeakWidth + td.width + td.left) + "px";
            this.beakObj.style.left = (td.left + td.width) + "px";
        } else if (move && move == "right" 
        || !move && td.left - visibleArea.left > w + horizontalBeakWidth + horizontalBeakCompensation) {
            loc = this.beakObj.className = "right";
            if (this.closeObj) {
                this.closeObj.style.left = "-17px";
            }
            this.beakObj.style.left = (td.left - horizontalBeakWidth - horizontalBeakCompensation + 3) + "px";
            this.maskObj.style.left = this.mainObj.style.left = (td.left - w - horizontalBeakWidth - horizontalBeakCompensation) + "px";
        } else {
            loc = this.beakObj.className = move ? move : "top";
            this.beakObj.style.left = (
                td.center.x > visibleArea.left + visibleArea.width - browserScrollBarWidth - horizontalBeakWidth ?  
                td.left + horizontalBeakWidth : 
                td.center.x < visibleArea.left ?
                td.left + td.width - horizontalBeakWidth :
                td.center.x - (horizontalBeakWidth / 2)) + "px";
            this.maskObj.style.left = this.mainObj.style.left = (
                (td.center.x - w / 2) + w > visibleArea.left + visibleArea.width - browserScrollBarWidth ? 
                visibleArea.left + visibleArea.width - browserScrollBarWidth - w :
                (td.center.x - w / 2) < visibleArea.left ?
                visibleArea.left : 
                (td.center.x - w / 2)) + "px";
        }
        return loc;
    };
    a.getBrowserVisibleArea = function () {
        return {
            top: window.pageYOffset, 
            left: window.pageXOffset, 
            height: window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight||0, 
            width: window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth||0
        };
    };
    a.setBeakTop = function(t) {
        this.beakObj.style.top = t + "px";
    };
    a.closeFlyout = function() {
        this.beakObj.style.display = this.mainObj.style.display = "none";
        this.setFlyoutContent("");
        if (this.maskObj) {
            this.closeObj.style.display = "none";
            this.maskObj.style.display = "none";
        }
        // a.mainObj = null;
    };
    a.getBaseUrl = function () {
       return this.baseUrl;
    };
    a.setBaseUrl = function (url) {
        this.baseUrl = url;
    };
    a.preloadContent = function(jq, url) {
        if (!a.mainObj) {
            a.init();
        }
        var urlTokenized = url.split("/d1.");
        a.setBaseUrl(urlTokenized[0] + "/");
        jq.ajax({
            url : url + "&callback=?",
            dataType : "jsonp",
            success : function(response) {
                a.setFlyoutContent(a.buildContentHTML(a.findContent(response)));
            }
        });
    };
    a.findContent = function(response) {
        var r = "";
        for ( a = 0; a < response.length; a++) {
            if (typeof response[a].title != 'undefined' && response[a].title && response[a].title != "" 
                && typeof response[a].thumbnail != 'undefined' && response[a].thumbnail && response[a].thumbnail != ""
                && typeof response[a].description != 'undefined' && response[a].description && response[a].description != "") {
                    response[a].title = response[a].title.replace("&nbsp;", " ");
                    response[a].description = response[a].description.replace("&nbsp;", " ");
                    r = response[a];
                    break;
            } else {
                continue;    
            }
        }
        if (r == "") {
            r = this.getFallBackContent();
        }
        return r;
    }; 
    a.getFallBackContent = function () {
        return CSP_GLOBAL.tryEval("{link: \"\", thumbnail:\"\", title:\"No Fly-out Content Found\", description:\"Click here to launch your syndication.\"}");  
    };
    a.buildContentHTML= function (entry) {
        var mainDiv = "<div class = \"csp-box\">"
            + "<a href=\"" + this.getBaseUrl() + entry.link + "\">" 
            + (entry.thumbnail == "" ? entry.thumbnail : "<img src=\"" + this.getBaseUrl() + entry.thumbnail + "\">")
            + "<div class=\"csp-box-description\">"
            + "<div class=\"csp-box-title\">"
            + entry.title
            + "</div>"
            + "<div class=\"csp-box-desc\">" + entry.description + "</div></div></a></div>";
        this.preloadedContentData = mainDiv;
        this.link = this.getBaseUrl() + entry.link;
       return mainDiv; 
    };
    a.WTReport = function(options) {
        if (options.wt) {
            var OConversionType = options.wt.DCSext.ConversionType;
            var OConversionShown = options.wt.DCSext.ConversionShown;
            var OConversionContent = options.wt.DCSext.ConversionContent; 
            var OConversionClick = options.wt.DCSext.ConversionClick;

            options.wt.DCSext.ConversionType = options.options.SyndicationType;
            options.wt.DCSext.ConversionShown = null;
            options.wt.DCSext.ConversionContent = options.wt.DCSext.ConversionClick = options.options.ElementAction + "/fyout";
            options.wt.dcsMultiTrack();
            
            options.wt.DCSext.ConversionType = OConversionType;
            options.wt.DCSext.ConversionShown = OConversionShown;
            options.wt.DCSext.ConversionContent = OConversionContent; 
            options.wt.DCSext.ConversionClick = OConversionClick;
        }
    };
}


