Add resources used across all projects and features, such as images, JS, CSS, etc.

If the resources can potentially be used across more than one project without ANY code changes, then it belongs here.

If an specific version of a feature is required and it's only used in one project, it should be moved into that project.