//instantiate CSP_GLOBAL if undefined
//{server:cleanmframe}
// Unified UI JS
// Modified ltu 11/25/2013

if ( typeof (CSP_GLOBAL) == "undefined") {
    CSP_GLOBAL = {};
    (function($$) {
        (function() {
            var url = window.location.href;
            var key = "csp_page:";
            var keyIndex = url.indexOf(key);
            var hashIndex = url.indexOf("#");

            if (hashIndex != -1) {
                $$.parentUrl = url.substring(0, hashIndex);
            } else {
                $$.parentUrl = url;
            }
            if (keyIndex != -1) {
                if (keyIndex > hashIndex)
                    $$.initialFramePage = url.substring(keyIndex + key.length).replace('?','');
                else
                    $$.initialFramePage = url.substring(keyIndex + key.length, hashIndex);
            } else {
                $$.initialFramePage = "";
            }
        })();

        $$.scriptMap = {};
        $$.validateSku = {};

        //element tracking
        $$.elementCount = 0;
        $$.getNextElementNum = function() {
            $$.elementCount++;
            return $$.elementCount;
        };
        $$.inlineElementCount = 0;
        $$.getNextInlineElementNum = function() {
            $$.inlineElementCount++;
            return $$.inlineElementCount;
        };

        $$.loadScript = function(src) {
            try {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = src;
                document.getElementsByTagName('head')[0].appendChild(script);
            } catch (ex) {
                //document.write("<script type='text/javascript' src='"+src+"'></script>");
            }
        };

        $$.loadCss = function(href) {
            var index = href.lastIndexOf("/");
            var filename = (index != -1 ? href.substring(index + 1, href.length) : href);
            if ( typeof ($$.scriptMap[filename]) == "undefined") {
                try {
                    var link = document.createElement("link");
                    link.type = "text/css";
                    link.rel = "stylesheet";
                    link.href = href;
                    document.getElementsByTagName('head')[0].appendChild(link);
                    $$.scriptMap[filename] = {};
                } catch (ex) {
                    //document.write("<script type='text/javascript' src='"+src+"'></script>");
                }
            }
        };

        $$.loadAndExecute = function(src, func) {
            $$.log("+++loadAndExecute: ");
            var index1 = src.indexOf("?");
            var index;
            if (index1 == -1)
                index = src.lastIndexOf("/");
            else
                index = src.substring(0, index1).lastIndexOf("/");
            var scriptname = (index != -1 ? src.substring(index + 1, src.length) : src);
            if ( typeof ($$.scriptMap[scriptname]) == "undefined") {
                $$.scriptMap[scriptname] = {};
                $$.scriptMap[scriptname].ready = false;
                $$.scriptMap[scriptname].queue = new Array();
                $$.scriptMap[scriptname].queue.push(func);
                $$.loadScript(src);
            } else if ($$.scriptMap[scriptname].ready) {
                var retObj = $$.scriptMap[scriptname].retObj;
                func(retObj);
            } else {
                $$.scriptMap[scriptname].queue.push(func);
            }
            $$.log("---loadAndExecute: " + scriptname);
        };
        //to be called at end of loaded script
        $$.processQueue = function(scriptname) {
            $$.log("+++ processQueue: " + scriptname);
            var queue = $$.scriptMap[scriptname].queue;
            var retObj = $$.scriptMap[scriptname].retObj;
            for (var i = 0; i < queue.length; i++) {
                (function(func, retObj) {
                    try {
                        func(retObj);
                    } catch (error) {
                        $$.log(error);
                    }
                })(queue[i], retObj);
            }
            $$.log("--- processQueue");
        };

        $$.tryEval = function(str1) {
            try {
                return eval("(" + str1 + ")");
            } catch (error) {
                return null;
            }
        };

        $$.log = function(obj) {
            try {
                if ( typeof (console) != "undefined")
                    console.log(obj);
                else {
                    var logdiv = document.getElementById("logdiv");
                    if ( typeof (obj) == "string" && logdiv) {
                        var ndiv = document.createElement("div");
                        ndiv.innerHTML = obj;
                        logdiv.appendChild(ndiv);
                        logdiv.scrollTop = logdiv.scrollHeight;
                    }
                }
            } catch (ex) {
            };
        };
    })(CSP_GLOBAL);
};

//start
(function($$) {
    $$.log("start csp");

    var protocol = "{server:protocol}";
    var domain = "{server:domain}";
    var locationId = "{server:locationId}";
    var requestedPageId = "{server:requestedPageId}";
    var spanId = Math.floor(Math.random() * 100000000);

    //place anchor incase of inline element
    if (!locationId) {
        document.write("<span id='SPAN_" + spanId + "' style='border:none;border-width:0;display:none'></span>");
    }
    //define consumer properties
    var consumer = {
        "baseDomain" : "{consumer:FBaseDomain}",
        "cid" : "{consumer:FConsumerID}",
        "cn" : "{consumer:FCompanyName}",
        "IntegratedType" : "{consumer:FIntegratedType}",
        "country" : "{consumer:Fcountry}",
        "lngDesc" : "{consumer:Fdescription}"
    };

    //replacement text
    //callbackObj is from domain utility 
    var callbackObj = {server:callbackObj};

    $$.loadAndExecute(protocol + "://" + domain + "/js/jquery.min.js", function($) {
        var actions = {
            validate : function(options, extra) {
                var el = $(this);
                var appendpoint = el;
                var instanceId = $$.getNextInlineElementNum();
                var _callbackObj = $.extend({}, callbackObj);
                _callbackObj.q = "?csp_request_type=validate&legacy=true&spanId=" + el.attr("id") + "&t=" + options.ElementType;
                //if (options.audience) _callbackObj.q += "&audience=" + options.audience;
                if (options.banner)
                    _callbackObj.q += "&banner=" + options.banner;
                if (options.category) {
                    _callbackObj.q += "&category=" + options.category;
                }
                _callbackObj.i = instanceId;
                _callbackObj.q += "&i=" + instanceId;
                var validateUrl = protocol + "://" + _callbackObj.getUrl() + "/CSP" + _callbackObj.q;
                $$.loadAndExecute(validateUrl, function(retObj) {
                    var options = $.extend({
                        "elementId" : "_opt" + instanceId
                    }, retObj);
                    if (retObj.show && retObj.show == "true")
                        render.call(el[0], options);
                });
            }
        };
        var append_methods = {
            dynamicbox : function(options) {
                var tag = $(this);
                if (!tag.hasClass("_csp_inline_showcase" + +options.elementId)) {
                    tag.addClass("_csp_inline_showcase" + options.elementId);
                    tag.addClass("dcmscript");
                }
                $$.loadAndExecute(protocol + "://" + options.url, function(content) {});
            },
            iframe : function(options) {
                var e = $(this);
                if (!e.attr("csp_state")) {
                    var path = protocol + "://" + options.url;
                        //initialFramePage: this is to setup a variable for deep linking
                        if ($$.initialFramePage) {
                            path = path.split("?")[0] + "?" + $$.initialFramePage;
                            $$.initialFramePage = "";
                        }
                        var extra = $$.tryEval(options.extra);
                        var height = extra.height;
                        var width = extra.width;
                        e.append("<iframe id='" + options.elementId + "' src='" + path + "' height='" + height + "' width='" + width + "' scrolling='auto' frameborder='0' hspace='0' vspace='0' marginheight='0' marginwidth='0' border='0'  ></iframe>");
                        e.attr("csp_state", options.elementId);
                    }
            },
            //dynamic iframe --> iframed
            iframed : function(options) {
                var e = $(this);
                if (!e.attr("csp_state")) {
                    var path = protocol + "://" + options.url;
                        if ($$.initialFramePage) {
                            path = path.split("?")[0] + "?" + $$.initialFramePage;
                            $$.initialFramePage = "";
                        }
                        var extra = $$.tryEval(options.extra);
                        var height = extra.height;
                        var width = extra.width;
                        e.append("<iframe id='" + options.elementId + "' src='" + path + "' height='" + height + "' width='" + width + "' scrolling='no' frameborder='0' hspace='0' vspace='0' marginheight='0' marginwidth='0' border='0'  ></iframe>");
                        $$.loadAndExecute(protocol + "://" + domain + "/js/iframed.js", function() {
                            $().iframed("startHost");
                            //$("#" + options.elementId).iframed(); PreDynamicIframeFix
                            $("iframe[id=" + options.elementId + "]").iframed(); //updated for IE 7, ensures proper resizing of iframe on a page where a campaign is also embedded. 
                        });
                        e.attr("csp_state", options.elementId);
                    }
            },
            link : function(options) {
                var e = $(this);
                if (!e.attr("csp_state")) {
                    e.append("<a id='" + options.elementId + "' href='javascript:;'>" + options.text + "</a>");
                    e.attr("csp_state", options.elementId);
                }
            },
            image : function(options) {
                var e = $(this);
                if (!e.attr("csp_state")) {
                    //e.append("<img id='" + options.elementId + "' src='" + options.src + "' alt='" + options.text + "'/>");
					          //Alt tag was removed becuase it caused some issue. 
                    e.append("<img id='" + options.elementId + "' src='" + options.src + "' />");
                    e.attr("csp_state", options.elementId);
                }
            },
            search_text : function(options) {
                $$.loadAndExecute(protocol + "://" + domain + "/js/findAndReplace.js", function() {
                    $$.findAndReplace("(" + options.search + ")", "<span class='" + options.elementId + "'>$1</span>");
                    if (options.tooltip) {
                        $("." + options.elementId).each(function(i) {
                            action_methods.tooltip.call(this, options);
                        });
                    }
                });
            },
            inline : function(options) {
                $(this).append(options.text);
            },
            campaign : function(options) {
                var _this = $(this);
                if (!_this.attr("csp_state")) {
                    $$.loadAndExecute(protocol + "://" + options.url, function(retObj) {
                        if ( typeof (CspCampaign) != "undefined") {
                            if (!retObj.fb) {
                                var c = CspCampaigns.Get(retObj.id);
                                var num = Math.floor(Math.random() * c.length);
                                _this.append("<img id='" + options.elementId + "' src='" + c[num]._banner + "' />");
                                //options.url = domain + "/ftp/campaign/{server:project}/" + c[num]._url + "/" + retObj.l;
                                options.url = options.domain + "/ftp/campaign/{server:project}/" + c[num]._url + "/" + retObj.l;
                                try {
                                    if (c[num]._id == "Micr_AtlasSol_Dis_Part_Bus")
                                        options.extra = "{width:'974',height:'600'}";
                                    else if (c[num]._id == "Micr_AtlasSol_Res_Cus_Bus")
                                        options.extra = "{width:'974',height:'600'}";
                                    else if (c[num]._id == "Micr_ServerSelector_Scenario10_Res_Bus")
                                        options.extra = "{width:'770',height:'630'}";
                                    else if (c[num]._id == "hppartner")
									                    options.extra = "{width:'620',height:'800'}";									
                                } catch (e) {
                                }
                                if (options.project == "lenovo" || options.project == "lenovodevint") {
                                    //Lenovo Server Selector Required New window only, instead of lightbox. 
                                    //Right now ALL lenovo campaigns would launch in a new window. 
                                    action_methods.newwindow.call(_this, options);
                                } else {
                                    //action_methods.lightbox.call(_this, options);// was in lenovo like this, but seems to be out of date
                                    action_methods[options.ElementAction].call(_this, options);
                                }
                                _this.attr("csp_state", options.elementId);
                                options.campaignReportingId = c[num]._id;
                                options.campaignBannerId = c[num]._banner;
                                options.sId = c[num]._sid;
                                options.sName = c[num]._sname;

                            } else {
                                //_this.append("use fallback category");
                                var n = _this.attr("fbc");
                                if (n == undefined)
                                    n = 0;
                                if (n == 0) {
                                    if (options.category)
                                        options.category = retObj.fbId;
                                    actions.validate.call(_this, options, retObj);
                                    _this.attr("fbc", ++n);
                                }
                            }
                            createWebtrends.call(_this, options);
                        }
                    });
                }
            },
            inline_showcase : function(options) {
                var tag = $(this);
                if (!tag.hasClass("_csp_inline_showcase" + +options.elementId))
                    tag.addClass("_csp_inline_showcase" + options.elementId);
                $$.loadAndExecute(protocol + "://" + options.url, function(content) {
					if (content.injectLocation == "" || content.injectLocation.indexOf("_opt") != -1) {
                        content.injectLocation = "_csp_inline_showcase" + options.elementId;
                    }
                    var ar = content.inlineContent.substr(content.inlineContent.indexOf("START_TK_CSPVENDORINFO") + 22, content.inlineContent.indexOf("END_TK_CSPVENDORINFO") - (content.inlineContent.indexOf("START_TK_CSPVENDORINFO") + 22)).replace(/\"/g, "").split(",$,");
					options.sId = ar[0];
                    options.sName = ar[1];
                    var l = $(content.injectLocation);
                    if (l.length == 0)
                        l = $("#" + content.injectLocation);
                    if (l.length == 0)
                        l = $("." + content.injectLocation);
                    if (content.inlineContent != "" && l.length > 0) {
                        createWebtrends.call(l[0], options);
                        l.html(content.inlineContent);
                        $$.handleRequest(l);
                        // translation
                        $$.loadAndExecute(content.path + "/ui/universalresources/js/cspinlinetranslation.js",function(){
                            $("[cspObj='translation']","div.cspisicontainer").each(function(){
                                try {
                                    var el = $(this);
                                    var value = CSP_GLOBAL.CspTranslation[content.lngCode.toLowerCase()][el.attr("cspValue")];
                                    if(value&&value!="") el.html(value);
                                } catch(ex) {/*keep existing text*/}
                            }); 
                        });                        $$.loadCss(content.path + "/ui/inline/css/template.css");
                        if (content.custom_parameters.Personalize_CSS_File)
                            $$.loadCss(content.custom_parameters.Personalize_CSS_File);
                        $$.loadCss(content.path + "/ui/universalresources/cspfancybox/source/jquery.fancybox.css");
                        $$.loadCss(content.path + "/ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-thumbs.css");
                        $$.loadAndExecute(content.path + "/ui/universalresources/cspfancybox/source/jquery.fancybox.pack.js", function(f) {
                            $$.loadAndExecute(content.path + "/ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-media.js", function(f) {
                                $$.loadAndExecute(content.path + "/ui/universalresources/cspfancybox/source/helpers/jquery.fancybox-thumbs.js", function(f) {
                                    var types, shows, values, id;
                                    types = shows = values = "";
                                    $$.log("l.hasClass(_csp_inline_showcase + options.elementId) :: " + l.hasClass("_csp_inline_showcase" + options.elementId));
                                    $$.log("l.attr(csp_state) :: " + l.attr("csp_state"));
                                    $$.log((l.hasClass("_csp_inline_showcase" + options.elementId) && !l.attr("csp_state")));
                                    if (l.hasClass("_csp_inline_showcase" + options.elementId) && !l.attr("csp_state")) {
                                        $(".csp_image_gallery", l).each(function(index) {
                                            var tag = $(this), imgUrl = "", iecss = "";
                                            if (navigator.userAgent.indexOf("IE") != -1 && tag.parent().parent().is("td")) {
                                                iecss = "ie";
                                            }
                                            tag.append("<span class='zoom-icon " + iecss + "'><img src='" + content.path + "/ui/universalresources/images/zoom.png' /></span>");
                                            imgUrl = (tag.attr("href").lastIndexOf("/") > 0 ? tag.attr("href").substring(tag.attr("href").lastIndexOf("/") + 1) : tag.attr("href"));
                                            inlineReport.call(tag, options, "ImageGallery", imgUrl);
                                            types = types + options.SyndicationType.toLowerCase() + ";";
                                            shows = shows + "ImageGallery;";
                                            values = values + imgUrl + ";";
                                        });
                                        $('.csp_image_gallery').fancybox({
                                            openEffect : 'none',
                                            closeEffect : 'none',
                                            nextEffect : 'none',
                                            prevEffect : 'none',
                                            scrolling : 'no'
                                        });

                                        $(".csp_video", l).each(function() {
                                            var tag = $(this);
                                            if (tag.attr('href').indexOf('.wmv') != -1) {
                                                var vurl = function(href) {
                                                    var key = "video";
                                                    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                                                    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
                                                    var qs = regex.exec(href);
                                                    if (qs == null)
                                                        return "";
                                                    else
                                                        return qs[1];
                                                };
                                                tag.attr("class", tag.attr("class").replace("swf", "iframe"));
                                                var vid = window.escape(vurl(tag.attr("href")));
                                                var preview = tag.children(0).attr('src') != "" ? "&preview=" + window.escape(tag.children(0).attr('src')) : "";
                                                tag.attr("href", content.path + "/ui/universalresources/cspwmvplayer/cspwmvplayer.html?video=" + vid + preview);
                                            } else if (tag.attr('href').indexOf('youtube.com/embed') != -1) {
                                                tag.attr("class", tag.attr("class").replace("swf", "iframe"));
                                            } else if (tag.attr('href').indexOf('youtube.com/watch') != -1) {
                                                tag.attr("class", tag.attr("class").replace("swf", "iframe"));
                                                tag.attr("href", tag.attr("href").substring(tag.attr("href").lastIndexOf("http")));
                                            }
                                            tag.append("<span class='play-icon'><img src='" + content.path + "/ui/universalresources/images/video_overlay.png' /></span>");
                                            if (tag.attr("csp_video")) {
                                                var videoUrl = (tag.attr("csp_video").lastIndexOf("/") > 0 ? tag.attr("csp_video").substring(tag.attr("csp_video").lastIndexOf("/") + 1) : tag.attr("csp_video"));
                                                inlineReport.call(tag, options, "Video", videoUrl);
                                                types = types + options.SyndicationType.toLowerCase() + ";";
                                                shows = shows + "Video;";
                                                values = values + videoUrl + ";";
                                            }
                                        });
                                        $(".csp_video").fancybox({
                                            maxWidth : 600,
                                            maxHeight : 400,
                                            fitToView : false,
                                            width : 600,
                                            height : 400,
                                            autoSize : false,
                                            closeClick : false,
                                            openEffect : 'none',
                                            closeEffect : 'none',
                                            scrolling : 'no',
                                            helpers : {
                                                media : {}
                                            }
                                        });
                                        
                                        var $allShowcases = $("embed[cspType='FLASHSHOWCASE']");
                                        $allShowcases.each(function() {
                                            var fsdima = $(this).attr('src').match(/\d+x\d+/);
                                            if (fsdima) {
                                                var fsdim = fsdima[0].split("x");
                                                if (parseFloat($('.cspisicontainer').outerWidth()) >= parseFloat(fsdim[0])) {  
                                                    $(this).width(parseFloat(fsdim[0])).height(parseFloat(fsdim[1]));
                                                } else {
                                                    $(this).width(parseFloat($('.cspisicontainer').outerWidth())).height(parseFloat($('.cspisicontainer').outerWidth()) * parseFloat(fsdim[1]) / parseFloat(fsdim[0]));
                                                }
                                            }
                                        }); 

                                        $(".csp_espec", l).each(function() {
                                            var tag = $(this);
                                            var target = $("#" + tag.attr("csp_espec"));
                                            if (target.length > 0) {
                                                try {
                                                    var espec = eval("(" + tag.text() + ")");

                                                    if (espec.Espec) {
                                                        var html = "<table cellpadding='0' cellspacing='0'>";

                                                        var className = "";
                                                        for (var i = 0; i < espec.Espec.length; i++) {
                                                            html += "<tr><th colspan=2>" + espec.Espec[i].SN + "</th></tr>";
                                                            for (var j = 0; j < espec.Espec[i].R.length; j++) {


                                                                if (j % 2 == 0)
                                                                    className = "even";
                                                                else
                                                                    className = "odd";
                                                                html += "<tr class='" + className + "'><td>" + espec.Espec[i].R[j].HT + "</td><td>" + espec.Espec[i].R[j].BT + "</td></tr>";
                                                            }
                                                        }
                                                        html += "</table>";
                                                        target.html(html);
                                                    }
                                                } catch (e) {
                                                }
                                            }
                                        });
                                        // end of tech spec
                                        $(".feature-box").fancybox({
                                            helpers : {
                                                media : {}
                                            }
                                        });
                                        $(".csp_pdf_container", l).each(function() {
                                            var tag = $(this);
                                            var PDFUrl = (tag.attr("csp_pdf").lastIndexOf("/") > 0 ? tag.attr("csp_pdf").substring(tag.attr("csp_pdf").lastIndexOf("/") + 1) : tag.attr("csp_pdf"));
                                            inlineReport.call(tag, options, "PDF", PDFUrl);
                                            types = types + options.SyndicationType.toLowerCase() + ";";
                                            shows = shows + "PDF;";
                                            values = values + PDFUrl + ";";
                                        });

                                        inlineReportOnLoad(options, types, shows, values);
                                        l.removeClass(("_csp_inline_showcase" + options.elementId));
                                        l.attr("csp_state", options.elementId);
                                        //l.attr("id", l.attr("id") + "_ready");
                                    } // check to prevent reload
                                });
                            });
                        });
                        // load lib
                    }
                });
            }
        };
        var inlineReportOnLoad = function(options, types, shows, values) {
            if ( typeof ($$["wt" + options.project]) != "undefined") {
                //remove extra ;
                if (types && types[types.length - 1] == ";")
                    types = types.substring(0, types.length - 1);
                if (shows && shows[shows.length - 1] == ";")
                    shows = shows.substring(0, shows.length - 1);
                if (values && values[values.length - 1] == ";")
                    values = values.substring(0, values.length - 1);
                var wt = $$["wt" + options.project];
                var savedData = new Object();
                savedData["DCSext"] = wt.DCSext;
                wt.DCSext.ConversionClick = null;
                wt.DCSext.ConversionType = types;
                wt.DCSext.ConversionShown = shows;
                wt.DCSext.ConversionContent = values;
                wt.dcsMultiTrack();
                wt.DCSext = savedData["DCSext"];
                savedData = null;
            }
        };
        var inlineReport = function(options, clickValue, contentValue) {
            var tag = this;
            if ( typeof ($$["wt" + options.project]) != "undefined") {
                tag.bind("click", function() {
                    var wt = $$["wt" + options.project];
                    var savedData = new Object();
                    savedData["DCSext"] = wt.DCSext;
                    wt.DCSext.ConversionShown = null;
                    wt.DCSext.ConversionType = options.SyndicationType.toLowerCase();
                    wt.DCSext.ConversionClick = clickValue;
                    wt.DCSext.ConversionContent = contentValue;
                    wt.dcsMultiTrack();
                    //restore
                    wt.DCSext = savedData["DCSext"];
                    savedData = null;
                });
            }
        };
        var action_methods = {
            lightbox : function(options) {
                $$.log("+++lightbox: " + options.elementId);
                var path = protocol + "://" + options.url;
                var extra = $$.tryEval(options.extra);
                var parent = $(this);
                //$$.loadCss(protocol + "://" + domain + "/css/csp_lightbox.css"); //lenovo system was running this version
                //$$.loadAndExecute(protocol + "://" + domain + "/js/csp_thickbox.js", function() { //lenovo system was running this version
                $$.loadCss(protocol + "://" + domain + "/ui/universalresources/csplightboxthickbox/css/csp_lightbox.css");
                $$.loadAndExecute(protocol + "://" + domain + "/ui/universalresources/csplightboxthickbox/js/csp_thickbox.js", function() {
                    parent.thickbox({
                        url : path,
                        width : extra.width,
                        height : extra.height
                    });

                    if ($$.initialFramePage) {
                        $.thickbox_launch({
                            url : path.split("?")[0] + "?" + $$.initialFramePage,
                            width : extra.width,
                            height : extra.height
                        });
                        $$.initialFramePage = "";
                    }
                });
            },
            tooltip : function(options) {
                var parent = $(this);
                $$.loadCss(protocol + "://" + domain + "/css/tipTip.css");
                $$.loadAndExecute(protocol + "://" + domain + "/js/jquery.tipTip.min.js", function() {
                    parent.css("color", "blue").tipTip({
                        content : options.text,
                        defaultPosition : "top",
                        fadeIn : 0,
                        fadeOut : 0,
                        delay : 0,
                        maxWidth : "auto",
                        keepAlive : true
                    });
                });
            },
            flyout : function(options) {
                $$.log("+++flyout: " + options.elementId);
                // var path = protocol + "://" + options.url;
                var hoverURL = protocol + "://" + options.hover_url;
                var extra = $$.tryEval(options.extra);
                var parent = $(this);
                // $$.loadCss(protocol + "://" + domain + "/css/csp_flyout.css");
                $$.loadCss(protocol + "://" + domain + "/ui/universalresources/cspflyout/csp_flyout.css");

                $$.loadAndExecute(protocol + "://" + domain + "/global/files/" + options.project + "/script/WebTrends.js?" + options.project, function() {
                    createWebtrends.call(parent, options);

                    // $$.loadAndExecute(protocol + "://" + domain + "/js/csp_flyout.js", function() {
                    $$.loadAndExecute(protocol + "://" + domain + "/ui/universalresources/cspflyout/csp_flyout.js", function() {
                        parent.flyout({
                            url : hoverURL,
                            width : extra.hover.width,
                            height : extra.hover.height,
                            closeOnClick : extra.hover.closeOnClick,
                            wt : $$["wt" + options.project],
                            options : options,
                            el : parent
                        });
                    });
                    $$.loadAndExecute(protocol + "://" + domain + "/js/iframed.js", function() {
                        $().iframed("startHost");
                        $("#flyoutIFrame").iframed();
                    });
                });
            },
            newwindow : function(options) {
                var extra = $$.tryEval(options.extra);
                //add 20px for scroll bar
                var width = parseInt(extra.width) + 20;
                var height = extra.height;
                var target = extra.target || "_blank";
                var path = protocol + "://" + options.url;

                $(this).click(function(event) {
                    window.open(path, target, "menubar=0,location=0,status=0,scrollbars=1,toolbar=0,width=" + width + ",height=" + height);
                }).css("cursor", "pointer");

                if ($$.initialFramePage) {
                    window.open(path.split("?")[0] + "?" + $$.initialFramePage, target, "menubar=0,location=0,status=0,scrollbars=1,toolbar=0,width=" + width + ",height=" + height);
                    $$.initialFramePage = "";
                }
            }
        };

        var createWebtrends = function(options) {
            var appendpoint = this;
			//if(options.project=="microsoft"&&(consumer.cid=="1273"||consumer.cid=="1554"))options.project="mgb";//outdated msft exception
            $$.loadAndExecute(protocol + "://" + domain + "/global/files/" + options.project + "/script/WebTrends.js?" + options.project, function() {
                try {
                    if (consumer.cn.match(/^test_/i)) //&&!(options.project=="mgb"&&(consumer.cid=="1273"||consumer.cid=="1554"))) //Oudated msft exception
                        return;
                } catch (e) {
                }
                //var wttag = new WebTrends();

                var wttag = null;
                if ( typeof ($$["wt" + options.project]) != "undefined") {
                    wttag = $$["wt" + options.project];
                } else {
                    wttag = new WebTrends();
                }
                wttag.dcsGetId();
				        //wttag.DCSext.vendorName = "{server:cleanmframe}".toLowerCase(); EGO don't think we need this anymore
                wttag.DCSext.ConversionType = options.SyndicationType.toLowerCase();
                wttag.DCSext.embed_url = window.location.href;
                var elementType = options.ElementType.toLowerCase();
                if (elementType == "image")
                    elementType = "banner";
                var elementAction = options.ElementAction.toLowerCase();
                var elementDesc = (elementAction == "none" ? elementType : elementType + "/" + elementAction);
                wttag.DCSext.ConversionClick = (elementAction == "none" ? elementDesc : null);
                wttag.DCSext.ConversionShown = elementDesc;

                if (options.SyndicationType == "campaign" && typeof (options.banner) != "undefined" && options.banner) {
                    wttag.DCSext.ConversionContent = options.banner;
                    wttag.DCSext.ConversionShown = options.banner;
                } else if (options.SyndicationType == "inline_showcase") {
                    try {
                        var mfrsku = $(appendpoint).attr("mfrsku");
                        if (mfrsku) {
                            wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = wttag.DCSext.ConversionClick = wttag.DCSext.sku_key = mfrsku;
                        } else if (options.url.match(/mfrsku\=\d+/)[0].split("=")[1]) {
                            mfrsku = options.url.match(/mfrsku\=\d+/)[0].split("=")[1];
                            wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = wttag.DCSext.ConversionClick = wttag.DCSext.sku_key = mfrsku;
                        }
                    } catch (e) {
                        $$.log(ex);
                    }
                } else if (options.SyndicationType.indexOf("key_") == 0) {
                    var mfrsku = options["mfrsku"];
                    if (mfrsku)
                        wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = mfrsku;
                } else {
                    wttag.DCSext.ConversionContent = elementDesc;
                    if (options["mfrsku"]) {
                        wttag.DCSext.Entry_Launch = wttag.DCSext.ConversionContent;
                        wttag.DCSext.ConversionContent = wttag.DCSext.ConversionShown = options["mfrsku"];
                        wttag.DCSext.sku_key = options["mfrsku"];
                    }
                }

                if ( typeof (options.sId) != "undefined") {
                    wttag.DCSext.vendorName = options.sName;
                    wttag.DCSext.csp_vendorid = options.sId;
                } //else {
                    // The above will probably always execute, the below ...
                    //wttag.DCSext.vendorName = "{server:cleanmframe}".toLowerCase();   
                //} I think the whole branch is now un needed

                wttag.DCSext.csp_stype = "embededscript";
                wttag.DCSext.csp_companyId = consumer.cid;
                wttag.DCSext.csp_pageTitle = document.title;
                if (wttag.DCSext.csp_pageTitle.length > 100) {
                    wttag.DCSext.csp_pageTitle = wttag.DCSext.csp_pageTitle.substring(0, 75) + "...";
                }
                wttag.DCSext.csp_companyname = consumer.cn;
                //if (typeof (consumer.lngDesc) != "undefined") wttag.DCSext.csp_language_descr = consumer.lngDesc;
                if ( typeof (consumer.lngDesc) != "undefined")
                    wttag.DCSext.language = consumer.lngDesc;
                if ( typeof (consumer.country) != "undefined")
                    wttag.DCSext.csp_country = consumer.country;
                if ( typeof (options.campaignReportingId) != "undefined")
                    wttag.DCSext.csp_sname = options.campaignReportingId;
                else
                    wttag.DCSext.csp_sname = wttag.DCSext.ConversionType;

                if (elementAction != "none" && consumer.IntegratedType) {
                    wttag.WT.si_n = consumer.IntegratedType;
                    wttag.WT.si_x = "1";
                }

                wttag.dcsCollect();
                if ( typeof ($$["wt" + options.project]) == "undefined") {
                    $$["wt" + options.project] = wttag;
                }
                //wttag.dcsMultiTrack();

                if (options.ElementAction != "none") {
                    $(appendpoint).bind("click", function(ev) {
                        try {
                            if (wttag.WT.si_n) {
                                wttag.WT.si_x = "2";
                            }
                            wttag.DCSext.ConversionShown = null;
                            wttag.DCSext.ConversionType = options.SyndicationType.toLowerCase();
                            if (options.SyndicationType == "campaign" && typeof (options.banner) != "undefined" && options.banner)
                                wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = options.banner;
                            else {
                                wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = elementDesc;
                                if (options["mfrsku"]) {
                                    wttag.DCSext.Entry_Launch = elementDesc;
                                    wttag.DCSext.ConversionClick = wttag.DCSext.ConversionContent = wttag.DCSext.sku_key = options["mfrsku"];
                                }
                            }
                            wttag.dcsMultiTrack();
                        } catch (ex) {
                            $$.log(ex);
                        }
                    });
                }
            });
        };
        var render = function(options) {
            var appendpoint = this;
            if ( typeof (options.ElementType) == "undefined")
                return;

            if (options.ElementType != "none")
                append_methods[options.ElementType].call(appendpoint, options);

            //set no element action when iframe is choosen
            if (options.ElementType == "iframe" || options.ElementType == "iframed")
                options.ElementAction = "none";

            if (options.ElementType != "campaign" && options.ElementAction != "none")
                action_methods[options.ElementAction].call(appendpoint, options);

            try {
                var opt = $$.tryEval(options.options);
                if (typeof opt != "undefined" && typeof (opt.hover) != "undefined" && opt.hover)
                    action_methods["flyout"].call(appendpoint, options);
            } catch (e) {
                $$.log(e);
            }


            if (options.ElementType != "campaign" && options.SyndicationType.indexOf("key_") != 0 && options.ElementType != "inline_showcase")
                createWebtrends.call(appendpoint, options);
            try {
                if (options.project == "digitalchannel")
                    $("a.SEOLink").hide();
            } catch (e) {
            }
        };
        var validate = function(url, elementId) {
            var appendpoint = this;
            $$.loadAndExecute(url, function(retObj) {
                $$.log("validated: " + url);
                var options = $.extend({
                    "elementId" : "_opt" + elementId
                }, retObj);
                if (retObj.show && retObj.show == "true") {
                    render.call(appendpoint, options);
                }
            });
        };
        if ( typeof ($$.handleRequest) == "undefined") {
            $$.handleRequest = function(el) {
                if ( typeof (el) == "undefined")
                    el = $(document);
                var components = ["showcase", "exploreproduct", "csp_tooltip", "search_text_tooltip", "inline_showcase", "key_news", "key_promotion", "key_video", "key_supplierinfo"];
                //fill out the mfrname and sku if there isnt any
                $.each(components, function(i, component) {
                    $("." + component, el).each(function(j) {
                        var mf = $(this).attr("mfrname");
                        var pn = $(this).attr("mfrsku");
                        var lng = $(this).attr("lng");
                        if (!pn || !mf) {
                            var scriptTags = document.getElementsByTagName("script");
                            // for (var i = 0; i < scriptTags.length; i++) { //original loop from top to bottom
                            for (var i = scriptTags.length - 1; i >= 0; i--) {
                                if (/\/Csp\//i.test(scriptTags[i].src) == true) {
                                    var t, m, s, l;
                                    $$.log("Found a script tag " + scriptTags[i].src);
                                    var q = scriptTags[i].src.substring(scriptTags[i].src.lastIndexOf("/")).split("&");
                                    for (var j = 0; j < q.length; j++) {
                                        var p = q[j].split("=");
                                        p[0] = p[0].substring(p[0].indexOf("?") + 1);
                                        if (p[0] == "mfrname" && p[1])
                                            m = p[1];
                                        if (p[0] == "mfrsku" && p[1])
                                            s = p[1];
                                        if (p[0] == "lng" && p[1])
                                            l = p[1];
                                    }
                                    if (!mf) {
                                        mf = m;
                                        $(this).attr("mfrname", mf);
                                    }
                                    if (!pn) {
                                        pn = s;
                                        $(this).attr("mfrsku", pn);
                                    }
                                    if (!lng && l) {
                                        $(this).attr("lng", l);
                                    }
                                    break;
                                }
                            }
                        }
                    });
                });
                $.each(components, function(i, component) {
                    $("." + component, el).each(function(j) {
                        var appendpoint = this;
                        var mf = $(this).attr("mfrname");
                        if (mf)
                            mf = mf.replace(/\s/g, '%20'); // in lenovo but not msft
                        var pn = $(this).attr("mfrsku");
                        var lng = $(this).attr("lng");
                        var pp = $(this).attr("p"); //in lenovo but not msft
                        var elementId = $$.getNextElementNum();
                        var _callbackObj = $.extend({}, callbackObj);
                        if (lng)
                            _callbackObj.l = lng;
                        _callbackObj.i = elementId;
                        _callbackObj.q = "?csp_request_type=validate&t=" + component;
                        _callbackObj.q += "&parentUrl=" + escape($$.parentUrl);
                        _callbackObj.q += "&i=" + elementId;
                        //ensures a different scriptname
                        if (pn)
                            _callbackObj.q += "&mfrsku=" + pn;
                        if (mf)
                            _callbackObj.q += "&mfrname=" + mf;
                        if (pp)
                            _callbackObj.q += "&p=" + pp;//in lenovo but not msft
                        if (mf && mf.toLowerCase() != _callbackObj.p.toLowerCase()) {
                            //resolve domain
                            $$.loadAndExecute(protocol + "://" + domain + "/CSP?csp_request_type=resolve&mfrname=" + mf, function(retObj) {
                                _callbackObj.p = retObj.mf;
                                var validateUrl = protocol + "://" + _callbackObj.getUrl() + "/CSP" + _callbackObj.q;
                                validate.call(appendpoint, validateUrl, elementId);
                            });
                        } else {
                            var validateUrl = protocol + "://" + _callbackObj.getUrl() + "/CSP" + _callbackObj.q;
                            validate.call(appendpoint, validateUrl, elementId);
                        }
                    });
                });
            };
        }

        $(document).ready(function() {
            var currentType = "{server:component}";
            if (currentType == "")
                currentType = "showcase";

            //$(document).ready(function() { //was already removed in msft
                $$.handleRequest.call();
            //});//was already removed in msft


            $$.log("*** Type: " + currentType);
            if (currentType == "showcase" && $(".showcase").length > 0) {
                //ignore
            } else {
                var options = {
                    sku : "{server:mfrsku}",
                    t : "{server:component}",
                    mf: escape("{server:mfrname}"),
                    lng : "{server:lng}",
                    locationId : "{server:locationId}",
                    cdsId : "{server:cdsId}",
                    p : "{server:requestPageId}",
                    audience : "{server:audience}",
                    banner : "{server:banner}",
                    category : "{server:category}",
                    keyword : "{server:keyword}",
                    launchtype : "{server:launchtype}"// was not in lenovo
                };
                var appendpoint = document.getElementById("SPAN_" + spanId);
                var instanceId = $$.getNextInlineElementNum();
                var _callbackObj = $.extend({}, callbackObj);
                _callbackObj.q = "?csp_request_type=validate&legacy=true&spanId=" + spanId;
                if (options.sku)
                    _callbackObj.q += "&mfrsku=" + options.sku;
                else if (options.cdsId)
                    _callbackObj.q += "&cdsId=" + options.cdsId;
                if (options.mf)
                    _callbackObj.q += "&mfrname=" + options.mf;
                if (options.t)
                    _callbackObj.q += "&t=" + options.t;
                if (options.lng)
                    _callbackObj.l = options.lng;
                if (options.locationId)
                    _callbackObj.q += "&locationId=" + escape(options.locationId);
                if (options.p)
                    _callbackObj.q += "&p=" + options.p;
                if (options.audience)
                    _callbackObj.q += "&audience=" + options.audience;
                if (options.banner)
                    _callbackObj.q += "&banner=" + options.banner;
                if (options.launchtype)
                    _callbackObj.q += "&launchtype=" + options.launchtype;
                if (options.keyword)
                    _callbackObj.q += "&keyword=" + options.keyword;
                if (options.category) {
                    //Outdated exception in msft
                    //if (typeof (_callbackObj["p"]) != "undefined" && _callbackObj.p == "microsoft") {
                    //    if (",84,90,91".indexOf("," + options.category) >= 0) options.category = 102;
                    //    else if (",87,94,95".indexOf("," + options.category) >= 0) options.category = 103;
                    //}
                    _callbackObj.q += "&category=" + options.category;
                }

                _callbackObj.i = instanceId;
                _callbackObj.q += "&i=" + instanceId;
                _callbackObj.q += "&parentUrl=" + escape($$.parentUrl);
                var validateUrl = protocol + "://" + _callbackObj.getUrl() + "/CSP" + _callbackObj.q;
                $$.loadAndExecute(validateUrl, function(retObj) {
                    var options = $.extend({
                        "elementId" : "_opt" + instanceId
                    }, retObj);
                    if (retObj.show && retObj.show == "true") {
                        if (!appendpoint) {
                            $$.log("Unable to locate append point. Use location id instead.");
                            if (options.locationId) {
                                appendpoint = $("#" + options.locationId) || $("." + options.locationId) || $(options.locationId);
                                appendpoint = appendpoint[0];
                            }
                        }
                        if (appendpoint) {
                            appendpoint.style.display = "inline";
                            render.call(appendpoint, options);
                        }
                    }
                });
            }
        }); //end of document ready
    }); //end of loadAndExecute jquery.min.js
})(CSP_GLOBAL);