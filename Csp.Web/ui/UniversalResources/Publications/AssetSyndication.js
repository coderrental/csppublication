
if (typeof (CspAssets) == "undefined") {
    function CspAsset(id, reportingId, imageThumbnail, url, weight, title, sid, sname, requestedThumbnail, requestedId, fileType, width, height) {
        this._banner = imageThumbnail;
        this._url = url;
        this._weight = weight;
        //this._date = date;
        this._title = title;
        this._sid = sid;
        this._sname = sname;
        this._id = id;
        this._reportingId = reportingId;
        this._requestedThumbnail = requestedThumbnail;
        this._requestedId = requestedId;
		this._filetype = fileType;
        this._width=width;
        this._height=height;
    }
    CspAssets = {
        collection: new Object(),
        Add: function(campaign, instance) {
            if (typeof (this.collection[instance]) != "undefined")
                return this.collection[instance].push(campaign);
        },
        Init: function(instance, count) {
            if (typeof (this.collection[instance]) == "undefined") {
                this.collection[instance] = [];                
                }
        },
        Get: function(instance) {
            if (typeof (this.collection[instance]) != "undefined")
                return this.collection[instance];
            else return null;
        }
    };
}
{$ContentFound}
CspAssets.Init("{get:i}", "{ContentPieceCount}");
{repeat}
(function(collection, instance) {
    // {get:banner} = {F{get:banner}}
    var contentUrl =  "{FContent_Link_Url}";
    if (contentUrl.indexOf("/")==0)
        contentUrl = "{OriginalSourceDomain}" + contentUrl;
    var c = new CspAsset("{FContentInstanceID}", "{EscapeQuotes}{FCSP_Reporting_Id}{/EscapeQuotes}", "{FContent_Image_Thumbnail}", contentUrl, "0", "{EscapeQuotes}{FContent_Title}{/EscapeQuotes}", "{supplier:FConsumerID}", "{supplier:Fcompanyname}","{EscapeQuotes}{F{get:banner}}{/EscapeQuotes}","{EscapeQuotes}{F{get:i}}{/EscapeQuotes}", "{FLocal_File_Type}",
    "{EscapeQuotes}{FContent_File_Dimension_Width}{/EscapeQuotes}","{EscapeQuotes}{FContent_File_Dimension_Height}{/EscapeQuotes}");
    collection.Add(c, instance);
})(CspAssets, "{get:i}");
{/repeat}
{/$ContentFound}
if (typeof(CSP_GLOBAL) != "undefined"){
    (function($$) {
        try {
            var scriptname = "{OriginalRequestQuery}";
            $$.scriptMap[scriptname].ready = true;       
            $$.scriptMap[scriptname].retObj = {id: "{get:i}", l: "{FCTLanguageCode}", fb: {ContentPieceCount}>0?false:true, assets: CspAssets};
            if ($$.scriptMap[scriptname].retObj.fb)
                $$.scriptMap[scriptname].retObj.fbId = 96;
            $$.processQueue(scriptname);
        } catch (ex) {
            $$.log("Error in Queue:" + ex);
        };
    })(CSP_GLOBAL);
}
