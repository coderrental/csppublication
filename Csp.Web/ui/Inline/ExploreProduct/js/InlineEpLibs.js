/*menu*/
(function($) {

    var _ver = 1;
    var selectedTabClassName = "csp-tab-selected";
    var _cspTabClassName = "csp-tabs";

    $.fn.CspTabs = function(options) {
        var _this = $(this);
        _this.data('CspTabs', $.extend({}, options, ($.metadata ? _this.metadata() : {})));

        var lis = $("li", _this);
        //assign tabs
        lis.each(function(index, item) {
            var _div = $("#" + $("a", item).attr("rel"));
            if (!$(_div).hasClass(_cspTabClassName))
                $(_div).addClass(_cspTabClassName);
        });
        //hide all of the div
        var hideTabs = function() {
            var li2Removed = [];
            lis.each(function(index, item) {
                var _i = $(item);
                var _div = $("#" + $("a", _i).attr("rel"));
                if (_div.length > 0 && !_i.hasClass(selectedTabClassName))
                    $("#" + $("a", _i).attr("rel")).hide();
                else if (_div.length == 0) {
                    if (_i.hasClass(selectedTabClassName) && $("a", _i).attr("cspmfrname") == "Dell") {
                        lis[1].firstChild.click()
                    }
                    li2Removed.push(_i);
                }
            });
            while (li2Removed.length > 0)
                li2Removed.pop().remove();
        };
        //bind events
        lis.click(function() {
            var _el = $(this);
            var _id = $("a", _el).attr("rel");
            var selectedTab = $("li." + selectedTabClassName + "a", _this);
            if (_el.hasClass(selectedTabClassName)) return;
            lis.removeClass(selectedTabClassName);
            _el.addClass(selectedTabClassName);
            hideTabs();
            $("#" + _id).show();
            // custom rule for Video
            if (_id != "Video" && $("#Video").length > 0 && $("#Video").html().indexOf("youtube.com/") == -1) {
                try {
                    var p = null;
                    if (navigator.appName.indexOf("Microsoft") != -1) p = window['player'];
                    else p = document['player'];
                    p.sendEvent('STOP');
                } catch (e) {}
            }
        });
        hideTabs();
    };
})(jQuery);

function GetFileName(path) {
	var i = path.lastIndexOf('/');
	if (i==-1) i = 0;
	return path.substring(i+1);
}

var cspVideoEvents = { "play": false, "middle": false, "finish": false }
/*Play Video*/

function PlayVideo(url, autostart, mfrName) {
    if (url.indexOf("youtube.com/watch") != -1) {
        var vnamea = url.split("=");
/*
        if (vnamea[1]) {
            $("#cspVideoBlock").html('<iframe width="579" height="400" src="http://www.youtube.com/embed/' + vnamea[1] + '" frameborder="0" allowfullscreen></iframe>');
        }*/

    } /*
    else {
            var s1 = new SWFObject(document.location.protocol + "//ui.syndication.tiekinetix.net/ui/universalresources/cspvideoplayer/player.swf", "player", "579", "400", "8", "#ffffff");
            s1.addParam('allowfullscreen', 'true');
            s1.addParam('allowscriptaccess', 'always');
            if (autostart)
                s1.addVariable('autostart', 'true');
            else
                s1.addVariable('autostart', 'false');
            s1.addVariable('file', url);
            s1.addVariable('wmode', 'opaque');
            s1.addVariable('backcolor', '#EEEEEE');
            s1.addVariable('frontcolor', '#000000');
            s1.addVariable('lightcolor', '#000000');
            s1.addVariable('screencolor', '#000000');
            s1.addVariable('enablejs', 'true');
            s1.addVariable('useExternalInterface', 'true');
            s1.addParam('vname', GetFileName(url));
            s1.write('cspVideoBlock');
            cspVideoEvents = {
                "play" : false,
                "middle" : false,
                "finish" : false
            };
        }*/
    
    jwplayer("cspVideoBlock").setup({
        file: url,
        width: '100%',
        height: 400
    });
}


function OpenWindow(obj) {
    var left = (screen.width / 2) - 300;
    var top = (screen.height / 2) - 250;
    window.open($(obj).attr("href"), '', 'toolbar=0,menubar=0,width=600,height=500,top=' + top + ',left=' + left);
}

function FireWebTrendsCode(name, status) {
    try {
        if (typeof (_tag) != "undefined") {
            if (name == "") name = video;
            _tag.DCSext.csp_vname = name;
            if (status == "1") _tag.DCSext.csp_vaction = "START";
            else if (status == "3") _tag.DCSext.csp_vaction = "FINISH";
            else if (status == "2") _tag.DCSext.csp_vaction = "50%";
            _tag.DCSext.ConversionType = _tag.DCSext.ConversionShown = _tag.DCSext.ConversionContent = _tag.DCSext.ConversionClick = null;
            _tag.dcsMultiTrack(); // send data to WT
            _tag.DCSext.csp_vname = _tag.DCSext.csp_vaction = null;
        }
    }
    catch (err) {
    }
}

function TimeEventHandler(ev) {
    try {
        if (player != undefined && ev != undefined) {
            if (ev["duration"] != undefined && ev["position"] != undefined) {
                if ((ev.position / ev.duration) >= .5 && !events.middle) {
                    intelVideoTracking(vname, "2"); events.middle = true;
                    FireWebTrendsCode(vname, "2");
                }
                else if ((ev.position / ev.duration) >= .99 && !events.finish) {
                    intelVideoTracking(vname, "3"); events.finish = true;
                    FireWebTrendsCode(vname, "3");
                }
            }
        }
    }
    catch (ex) {
        if (typeof (console) != "undefined") { console.log("Error: %s Ev: %o", ex, ev); }
    }
} 
function StartEventHandler(ev) {
    try {
        if (player != undefined && ev != undefined) {
            if (ev["state"] != undefined && ev.state && !cspVideoEvents.play) {
                cspVideoEvents.play = true;
                FireWebTrendsCode($(player).attr("vname"), "1");
            }
        }
    }
    catch (ex) {
        if (console != undefined) { console.log("Error: %s Ev: %o", ex, ev); }
    }
}
function StopEventHandler(ev) {
    try {
        if (player != undefined && ev != undefined && !cspVideoEvents.finish) {
            cspVideoEvents.finish = true;
            FireWebTrendsCode($(player).attr("vname"), "3");               
        }
    }
    catch (ex) {
        if (console != undefined) { console.log("Error: %s Ev: %o", ex, ev); }
    }
}
function TimeEventHandler(ev) {
    try {
        if (player != undefined && ev != undefined) {
            if (ev["duration"] != undefined && ev["position"] != undefined) {
                if ((ev.position / ev.duration) >= .5 && !cspVideoEvents.middle) {
                    cspVideoEvents.middle = true;
                    FireWebTrendsCode($(player).attr("vname"), "2");
                }
                else if ((ev.position / ev.duration) >= .99 && !cspVideoEvents.finish) {
                    cspVideoEvents.finish = true;
                    FireWebTrendsCode($(player).attr("vname"), "3");
                }
            }
        }
    }
    catch (ex) {
        if (typeof (console) != "undefined") { console.log("Error: %s Ev: %o", ex, ev); }
    }
} 
