// updated 6.11.13
function initApp() {
    setLogoPos();
    initPickYourBanner();
	initValidateURL();
}

function setLogoPos() {
    $("#logo-img").one('load', function() {
        var imgHeight = $(this).height();
        var holderHeight = $('#logo-holder').height();
        if (imgHeight > holderHeight) return;
        var xPos = (holderHeight - imgHeight) / 2;
        $(this).css({ 'padding-top': xPos + 'px' });
    }).each(function() {
        if(this.complete) $(this).load();
    });
    
}

function initPickYourBanner() {
    var wdPickYourBanner = $('#wdPickYourBanner').kendoWindow({
        draggable: true,
        height: "720px",
        width: "880px",
        modal: true,
        resizable: false,
        visible: false,
        title: "Pick Your Banner"
    }).data("kendoWindow");

    var wdTC = $('#kendoWnd').kendoWindow({
        draggable: true,
        height: "720px",
        width: "900px",
        modal: true,
        resizable: false,
        visible: false,
        content: "http://global.syndication.tiekinetix.net/ui/subscription/tie/tc.htm",
        iframe:true,
        title: "Terms and Conditions"
    }).data("kendoWindow");
    
    $('#viewTermsAndConditions,#viewPrivacyPolicy,#viewTermsOfUse').click(function(ev){
        ev.preventDefault();
        var el = $(this);
        var wnd = $('#kendoWnd').data("kendoWindow");
        wnd.refresh(el.attr('href'));
        wnd.title(el.attr('title'));
        wnd.center();
        wnd.open();       
        $('#kendoWnd').show();
    });
        
    $('#btn-pickyourbanner').click(function () {
        var wdPickYourBanner = $('#wdPickYourBanner').data("kendoWindow");
        $('#wdPickYourBanner').show();
        wdPickYourBanner.center();
        wdPickYourBanner.open();
    });
    
    //button banner select clicked
    $('#btnBannerSelected').click(function () {
        var wdPickYourBanner = $('#wdPickYourBanner').data("kendoWindow");
		$('#selectedBanner').val($("input[name='pick-banner-size']:checked","#wdPickYourBanner").val());		
        wdPickYourBanner.close();
    });
    
    $('.banner-img').click(function() {
        var bannerCode = $(this).attr('rel');
        $('input#' + bannerCode).attr('checked', 'checked');
        $('#selectedBanner').val($('input#' + bannerCode).val());
    });
}

function validateFormInput(el){
    var flag = true;
    $(".short-required-field,.long-required-field").each(function(){
        var el = $(this);
        switch (el.attr("type")) {
            case "checkbox":
                flag = flag && el.is(":checked");
                break;
            default:
                if (el.val().length>0) {
                    flag = flag && true;
                    el.removeClass("error-field");
                }
                else {
                    flag = false;
                    //if (!el.hasClass("error-field"))
                        el.addClass("error-field");
                }
                break;
        }        
    });
    return flag;
}
function acceptAgreement(){
    var el = $("#terms-condition-check");
    var flag =el.is(':checked'); 
    if (!flag) el.focus();
    return flag;
}
function initValidateURL(){
	var UrlValid;
	var EmailValid;
	$('#company_url').focusout(function () {
		checkUrl();
	});
	$('#company-email').focusout(function () {
		checkEmail();
	});
	$('#terms-condition-check').click(function(){
		checkAccept();
	});
	
	$('#button-submit').click(function(e) {
			e.preventDefault();
			if(!validateFormInput()||!checkUrl()||!checkEmail()||!checkAccept()){
				 return false; 
			}
			try{if(Recaptcha.get_response()=="")return false;}catch(e){}
			document.getElementById('subscriptionform').submit();
	});
	function checkEmail(){
			var email = $('#company-email').val();
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if (emailReg.test(email)&&email!='') {
				$('#companyemail-msg').html('').hide();
				return true;
			}
			else{
				$('#companyemail-msg').html('Email is not valid!').show();
				return false;
			}			
	}
	function checkUrl(){
		var url = $('#company_url').val();
		//var urlregex = new RegExp("^((http|https|ftp)\://){0,1}([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
		if (/^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(url)&&url!='') {
			$('#companyurl-msg').html('').hide();
			if (url.indexOf('http') < 0)
				$('#company_url').val('http://' + url);
			return true;
		}
		else{
			$('#companyurl-msg').html('URL is not valid!').show();
			return false;
		}
	}
	function checkAccept(){
		if($('#terms-condition-check').attr('checked')) {
			$('#accept-msg').html('').hide();
			return true;
		} else {
			$('#accept-msg').html('( * )').show();
			return false;
		}
	}
}