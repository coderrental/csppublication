function initApp() {
	setLogoPos();
	initCopyToClipboard();
	
	if ($.browser.msie  && parseInt($.browser.version, 10) === 7)
		$('#email-script-value').css({ top: '-3px' });

    var wdTC = $('#kendoWnd').kendoWindow({
        draggable: true,
        height: "720px",
        width: "880px",
        modal: true,
        resizable: false,
        visible: false,
        content: "http://global.syndication.tiekinetix.net/ui/subscription/tie/tc.htm",
        iframe:true,
        title: "Terms and Conditions"
    }).data("kendoWindow");
    
    $('#viewTermsAndConditions,#viewPrivacyPolicy,#viewTermsOfUse').click(function(ev){
        ev.preventDefault();
        var el = $(this);
        var wnd = $('#kendoWnd').data("kendoWindow");
        wnd.refresh(el.attr('href'));
        wnd.title(el.attr('title'));
        wnd.center();
        wnd.open();       
        $('#kendoWnd').show();
    });		
}

function setLogoPos() {
	$("#logo-img").one('load', function() {
		var imgHeight = $(this).height();
		var holderHeight = $('#logo-holder').height();
		if (imgHeight > holderHeight) return;
		var xPos = (holderHeight - imgHeight) / 2;
		$(this).css({ 'padding-top': xPos + 'px' });
	}).each(function() {
		if(this.complete) $(this).load();
	});
	
}

function initCopyToClipboard() {
	$('#btn-copy2clipboard').zclip({
		path: 'http://global.syndication.tiekinetix.com/ui/subscription/birst/js/ZeroClipboard.swf',
		copy: function () { 
		    console.log($.trim($('#script-code').text()));
		    return $.trim($('#script-code').text()); 
		},
		beforeCopy: function () {
			$('#btn-copy2clipboard').hide();
		},
		afterCopy: function () {
			$('#code-copied').show().delay(5000).fadeOut('fast', function () {
				$('#btn-copy2clipboard').show();
			});
		}
	});
}

function emailScript(url) {
    var emailTo = $('#email-script-value').val();
    $.post(url+"[]&emailTo="+encodeURI(emailTo)+"&t="+(new Date()).getTime());
	alert("Email sent.");
}
