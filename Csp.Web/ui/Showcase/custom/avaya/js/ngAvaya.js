var tickValue = '';
var kalturaPId = '';
var kalturaUId = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;
var MAIN_MENU_MAX_LEVEL = 3;

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);		
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

String.prototype.escapeSpecialChars = function() {
return this.replace(/\\n/g, "\\n")
		   .replace(/\\'/g, "\\'")
		   .replace(/\\"/g, '\\"')
		   .replace(/\\&/g, "\\&")
		   .replace(/\\r/g, "\\r")
		   .replace(/\\t/g, "\\t")
		   .replace(/\\b/g, "\\b")
		   .replace(/\\f/g, "\\f")
		   .replace("\\r", "")
		   ;
};

function getURLParameter(name, givenstring) {
    return decodeURI(
        (RegExp('(^|&)' + name + '=(.+?)(&|$)').exec(givenstring)||[,,null])[2]
    );
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

function GoReport() {
	/*
	debugger;
	try {
		jQuery('#csp-report-lib').remove();
		window.setTimeout(function() {
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.id = "csp-report-lib";
			script.src = "js/CspReportLib.js";
			// clear CspReportLib 
			if (typeof (CspReportLib) != "undefined") {
				CspReportLib.wt.DCSext["ConversionContent"] = null;
				CspReportLib.wt.DCSext["ConversionShown"] = null;
				CspReportLib.wt.DCSext["ConversionClick"] = null;
				CspReportLib.wt.DCSext["ConversionType"] = null;
				CspReportLib.wt.DCSext["csp_vname"] = null;
				CspReportLib.wt.DCSext["csp_vaction"] = null;
			}
			
			document.getElementsByTagName('head')[0].appendChild(script);

		}, 1000);
		try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
	} catch (Err) {};
	*/
	
	try{
		if(typeof(_tag)!="undefined"&&typeof(CspReportLib)!="undefined") 
			CspReportLib.collect(_tag);
	}
	catch(e){};
	
}

var avayaModule = angular.module('ngAvaya', []).config(function($routeProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/Avaya/main.html'}).
		when('/i/:tick', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/Avaya/main.html'}).
		when('/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/Avaya/main.html'}).
		when('/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:MainCtrl,templateUrl:'ui/showcase/Custom/Avaya/main.html'}).
		
		when('/products/:id', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		
		when('/products/:id/categoryname/:categoryname', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/categoryname/:categoryname/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/categoryname/:categoryname/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		when('/products/:id/categoryname/:categoryname/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductCtrl,templateUrl:'ui/showcase/Custom/Avaya/productsv2.html'}).
		
		when('/products-a-z/', {controller:AToZCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-a-z.html'}).
		when('/products-a-z/i/:tick', {controller:AToZCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-a-z.html'}).
		
		when('/categorydetail/:id', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/category.html'}).
		when('/categorydetail/:id/i/:tick', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/category.html'}).
		when('/categorydetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/category.html'}).
		when('/categorydetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/category.html'}).
		
		when('/categorydetail/:id', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/products-a-z.html'}).
		when('/categorydetail/:id/i/:tick', {controller:CategoryCtrl, templateUrl:'ui/showcase/Custom/Avaya/products-a-z.html'}).
		
		when('/products/:parentid/productdetail/:id', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		
		when('/products/:parentid/productdetail/:id/tab/:tabname', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		when('/products/:parentid/productdetail/:id/tab/:tabname/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductDetailCtrl,templateUrl:'ui/showcase/Custom/Avaya/products-detail.html'}).
		
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		
		when('/contactus/title/:titlevalue', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		when('/contactus/title/:titlevalue/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Avaya/contactus.html'}).
		//otherwise({ redirectTo: '/' });
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i>=0)
					return path.substring(i+9);
			} catch(e) {}
			return '/';
		} });
});

avayaModule.directive('myPromotionDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last){
			setTimeout(function() {
				jQuery("div.promotionAssets div.csp-tab-link a").each(function(){
					var el = jQuery(this);
					if (!isMobile()) {
						if (el.attr('href').indexOf('.pdf') == -1)
							el.colorbox({iframe:true, height: 600, width: 650});
					}
					else {
						el.attr("target","_blank");
					}
				});
			}, 1000);
		}
	};
});

avayaModule.directive('myCategoryDirective', function() {
	return function(scope, element, attrs) {
		if (scope.$last && scope.categoryName != ''){
			return;
			setTimeout(function() {
				var container = jQuery('body');
				var scrollTo = jQuery('[rel="' + scope.categoryName + '"]');
				$('html, body').stop(true,true).animate({
					scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
				}, 500);
			}, 500);
		}
	};
});

function ContactUsCtrl($rootScope, $scope, $http, $location, $routeParams, LeadgenForm, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;

	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		$scope.contactusMessage = LeadgenForm.submit('form1', $scope.submitted);
	};
        
    LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(resultData[0].supplierinfo[0]);
		LeadgenForm.setFields(resultData[1].contactusformfields, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		$rootScope.updatePageInfo(this);
		setTimeout(function() {
			jQuery(document).click();							
		}, 1000);
	});
}

function CategoryCtrl($scope, $rootScope, $http, $routeParams, $filter, $location, Products, DataProducts, URLHandler, Translation) {
	tickValue = URLHandler.getTick();
	var categoryId = typeof($routeParams.id) == "undefined" ? '' : $routeParams.id;
	$scope.breadcrumbs = [];
	$scope.crrCategoryItem;
	$scope.ajaxLoading = true;
	$scope.productList = [];
	$scope.label = {
		learnmore: 'learnmore'
	};
	$scope.isNotProduct = false;
	
	Translation.promise.then(function(resultData){
		for (key in $scope.label) {
			$scope.label[key] = Translation.translate(resultData, $scope.label[key]);
		}
	});
	
	
	var RequestForBreadcrumb = function ($scope, categoryid, breadcrumbInfo, productList) {
		if (categoryid == Products.rootid()) {
			var bcHomeItem = {title: 'home', link: '#/', level: ''};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
			});
			return;
		}
		var cateItem = $filter('filter')(productList, {categoryId: categoryid}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		if (typeof(cateItem) != 'undefined') {
			breadcrumbInfo.unshift({title: cateItem.title, link: '#/products/' + cateItem.cspuniquekey, level: cateItem.depth});
			RequestForBreadcrumb($scope, cateItem.parentId, breadcrumbInfo, productList);
		}
		else
			RequestForBreadcrumb($scope, Products.rootid(), breadcrumbInfo, productList);
	}
	
	Products.promise.then(function(resultData){
		var requestedBreadcrumb = [];
		$scope.crrCategoryItem = $filter('filter')(resultData, {cspuniquekey: categoryId}, function (expected, actual) {return expected == actual;})[0];
		
		$rootScope.updatePageInfo(null, $scope.crrCategoryItem.title);
		RequestForBreadcrumb($scope, $scope.crrCategoryItem.parentId, requestedBreadcrumb, resultData);
		DataProducts.promise.then(function(resultProductDetailData){
			var tempList = $filter('filter')(resultProductDetailData, {categoryId: $scope.crrCategoryItem.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			if (tempList.length > 1 || tempList.length == 0) {
				$scope.ajaxLoading = false;
				$scope.productList = tempList;
			}
			else
				window.location = '#/products/' + $scope.crrCategoryItem.cspuniquekey + '/productdetail/' + tempList[0].id;
			//console.log($scope.productList);
		});
		
	});
}

function ProductDetailCtrl($scope, $rootScope, $http, $routeParams, $filter, $location, Products, DataProducts, URLHandler, Translation, LeadgenForm) {
	tickValue = URLHandler.getTick();
	var productId = typeof($routeParams.id) == "undefined" ? '' : $routeParams.id;
	var parentId = typeof($routeParams.parentid) == "undefined" ? '' : $routeParams.parentid;
	var addedFeatured = [], placeHolder = [];
	$scope.translatedData;
	$scope.count = 1;
	$scope.addedTypes = [];
	$scope.localFeaturedList = [];
	$scope.currentAssetType = '';
	$scope.ajaxLoading = true;
	$scope.firstVideoBlock = null;
	$scope.productImage = [];
	$scope.breadcrumbs = [];
	$scope.defaultTab = typeof($routeParams.tabname) == "undefined" ? '' : $routeParams.tabname;
	$scope.isSolutionsBranch = true;
	
	$scope.tabClick = function(tabName, $event) {
		var currentUrl = decodeURI(document.URL);
		var urlParts = currentUrl.split('/#/')[1];
		urlParts = urlParts.split('/');
		var newUrl = '';
		for (var i = 0; i < urlParts.length; i++) {
			if (urlParts[i] == 'tab') continue;
			if (urlParts.indexOf('tab') > 0 && i == urlParts.indexOf('tab') + 1) continue;
			if (tickValue != '') {
				if (urlParts[i] == 'i' || i == urlParts.indexOf('i') + 1) continue;
			}
			if (newUrl != '') newUrl += '/';
			newUrl += urlParts[i];
		}
		if (tabName != '')
			newUrl += '/tab/' + tabName;
		if (tickValue != '')
			newUrl += '/i/' + tickValue;
		$location.path(newUrl, false);
		try{parent.postMessage(currentUrl.split('/#/')[0] + '/#/' + newUrl,"*");} catch(ex){};
		//jQuery($event.target).preventDefault();
		return false;
	};
	
	$scope.label = {
		contactus: 'Contact Us',
		contactustoday: 'contactustoday',
		overview: 'overview',
		productgallery: '',
		productvideo: ''
	};
	
	$scope.CallReport = function () {
		console.log('Call report');
	};
	
	$scope.getVideoLink = function (videoUrl) {
		return (videoUrl != '') ? videoUrl : 'javascript: void(0)';
	}
	
	$scope.getLinkTarget = function (launchType) {
		var targetType = '';
		if (launchType == 'newwindow')
			targetType = '_blank';
		return targetType;
	};
	
	$scope.renderredType = function (assetType) {
		var sample = assetType.localfeatured + '-' + assetType.type;
		if ($scope.addedTypes.indexOf(sample) == -1) {
			$scope.addedTypes.push(sample);
			return true;
		}
		return false;
	};
	
	$scope.getEmbedVideoDimension = function (obj) {
		var videoUrl = (obj.videoUrl != undefined) ? obj.videoUrl : obj.linkurl;
		return getURLParameter('width', videoUrl) + 'x' + getURLParameter('height', videoUrl);
	};
	
	$scope.getEmbedVideo = function(obj) {	
		var videoIdParamName = 'entry_id';
		var videoUrl = (obj.videoUrl != undefined) ? obj.videoUrl : obj.linkurl;
		var videoId = getURLParameter(videoIdParamName, videoUrl);
		var friendlyUrlPart = videoUrl.split('?')[0].split('/');
		
		if (placeHolder.indexOf(obj) == -1 && videoUrl.indexOf('embed') > 0) {
			placeHolder.push(obj);
			
			setTimeout(function() {
				if (jQuery('#generated-playerscript-holder').find('script').length == 0) {
					jQuery.getScript(videoUrl.split('?')[0], function() {
					});
					jQuery('#generated-playerscript-holder').append("<script type='text/javascript' src='" + videoUrl.split('?')[0] + "'><\/script>");
					for (var i = 0; i < friendlyUrlPart.length; i++) {
						if (friendlyUrlPart[i] == 'partner_id')
							kalturaPId = friendlyUrlPart[i + 1];
						if (friendlyUrlPart[i] == 'uiconf_id')
							kalturaUId = friendlyUrlPart[i + 1];
					}
				}
			}, 500);
		}
		return videoId;
	}
	
	$scope.CreateTab = function(assetType) {
		showHeader = (assetType.type != $scope.currentAssetType); 
		$scope.currentAssetType = assetType.type;
		assetType.label = Translation.translate($scope.translatedData, assetType.type);
		return showHeader;
	};
	
	$scope.goEncodeTitle = function (title) {
		return encodeURIComponent(title);
	}
	
	$scope.galleryItemClicked = function (item) {
		$scope.firstGalleryBlock = item;
	};
	
	//Get page content blocks (gallery & feature)
	$http.get('d1.aspx?p2009(ct38000&fLocal_Keywords~1=' + productId + '!)[st(ct38000*Status_Block_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(responseData) {
		var galleryBlocks = $filter('filter')(responseData, {blockId: 'gallery'}, function (expected, actual) {return expected == actual;});
		var videoBlocks = $filter('filter')(responseData, {blockId: 'video'}, function (expected, actual) {return expected == actual;});
		var featureBlocks = $filter('filter')(responseData, {blockId: 'feature'}, function (expected, actual) {return expected == actual;});
		$scope.contentBlocks = responseData;
		$scope.videoBlocks = videoBlocks;
		$scope.featureBlocks = featureBlocks;
		
		//Get product detail image from gallery
		if (galleryBlocks.length > 0) {
			$scope.firstGalleryBlock = galleryBlocks[0];
			$scope.label.productgallery = galleryBlocks[0].blockName;
			$scope.productImage.thumbnail = galleryBlocks[0].imageUrl;
			$scope.productImage.image = galleryBlocks[0].imageUrl;
		}
		//Get product detail image from data product
		else {
			DataProducts.promise.then(function(resultData){
				var rawData = $filter('filter')(resultData, {id: productId}, function (expected, actual) {return expected == actual;});
				$scope.productImage.thumbnail = rawData[0].thumbnailUrl;
				$scope.productImage.image = rawData[0].imageUrl;
			});
		}
		
		if (videoBlocks.length > 0) {
			$scope.demoVideoBlocks = $filter('filter')(videoBlocks, {blockName: 'demo'}, function (expected, actual) {return expected.toLowerCase().indexOf(actual) >= 0;});
			$scope.firstVideoBlock = $filter('filter')(videoBlocks, {blockName: 'product'}, function (expected, actual) {return expected.toLowerCase().indexOf(actual) >= 0;});
			if ($scope.demoVideoBlocks.length>0)
				$scope.label.productvideo = $scope.demoVideoBlocks[0].blockName;
			else
				$scope.label.productvideo = "";
			if ($scope.firstVideoBlock.length > 0)
				$scope.firstVideoBlock = $scope.firstVideoBlock[0];
			else 
				$scope.firstVideoBlock = null;
		}
	});
	
	$scope.GetBCLink = function (bcItem) {
		var leafNodeIndex = 0;
		var crrItemIndex = 0;
		for (var i = 0; i < $scope.breadcrumbs.length; i++) {
			if (leafNodeIndex == 0) {
				for (var j = 0; j < $rootScope.LeafCategories.length; j++) {
					if ($scope.breadcrumbs[i].cspuniquekey.toLowerCase() == $rootScope.LeafCategories[j].value.cspuniquekey.toLowerCase()) {
						leafNodeIndex = i;
						break;
					}
				}
			}
			if ($scope.breadcrumbs[i].cspuniquekey == bcItem.cspuniquekey)
				crrItemIndex = i;
		}
		
		if ((leafNodeIndex > 0 && crrItemIndex > leafNodeIndex) || ($scope.isSolutionsBranch && crrItemIndex >= 3))
			return bcItem.link.replace('#/products/', '#/categorydetail/');
		return bcItem.link;
	}
	
	var RequestForBreadcrumb = function ($scope, categoryid, breadcrumbInfo, productList) {
		if (categoryid == Products.rootid()) {
			var bcHomeItem = {title: 'home', link: '#/', level: '', cspuniquekey: ''};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
			});
			return;
		}
		var cateItem = $filter('filter')(productList, {categoryId: categoryid}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		if (typeof (cateItem) != 'undefined') {
			breadcrumbInfo.unshift({title: cateItem.title, link: '#/products/' + cateItem.cspuniquekey, level: cateItem.depth, cspuniquekey: cateItem.cspuniquekey});
			RequestForBreadcrumb($scope, cateItem.parentId, breadcrumbInfo, productList);
		}
		else
			RequestForBreadcrumb($scope, Products.rootid(), breadcrumbInfo, productList);
	}
	
	var SolutionsBranchDetect = function (itemLineage) {
		var arrLineage = itemLineage.split('/');
		for (var i = 0; i < arrLineage.length; i++) {
			if (parseInt(arrLineage[i]) == Products.mainproductid()) {
				$scope.isSolutionsBranch = false;
				break;
			}
		}
	}
	
	DataProducts.promise.then(function(resultData){
		
		var rawData = $filter('filter')(resultData, {id: productId}, function (expected, actual) {return expected == actual;});
		if (rawData.length == 0) {
			window.location = 'http://' + document.domain;
			return;
		}
		
		$scope.product = rawData[0];	
		
		Products.promise.then(function(resultProductData){

			$rootScope.BuildTreeAndGetLeaves(resultProductData);
			var requestedBreadcrumb = [];
			var crrProductItem = $filter('filter')(resultProductData, {cspuniquekey: parentId}, function (expected, actual) {return expected == actual;})[0];
			var mainProductItem = $filter('filter')(resultProductData, {categoryId: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
			
			SolutionsBranchDetect(crrProductItem.lineage);
			
			requestedBreadcrumb.push({title: crrProductItem.title, link: '#/products/' + crrProductItem.cspuniquekey, level: crrProductItem.depth, cspuniquekey: crrProductItem.cspuniquekey});
			if ($scope.isSolutionsBranch && crrProductItem.title.toLowerCase() == $scope.product.title.toLowerCase()) {
				//console.log(crrProductItem.title.toLowerCase() == $scope.product.title.toLowerCase());
				requestedBreadcrumb = [];
			}
			RequestForBreadcrumb($scope, crrProductItem.parentId, requestedBreadcrumb, resultProductData);
		});

		$http({
			method : 'GET',
			url: 'd1.aspx?p2004(ct21000&fLocal_Keywords~1=' + productId + '!)[st(ct21000*Status_Sort_Order*)]' + URLHandler.getUrlTick(),
			responseType : 'text',
			transformResponse: function (resultRawData, headersGetter) {
				var data = $rootScope.fixHElanguage(resultRawData);
				data.pop();

				Translation.promise.then(function (resultData) {
					$scope.translatedData = resultData;

					var validTabName = false;
					
					for (var i = 0; i < data.length; i++) {
						if (addedFeatured.indexOf(data[i].localfeatured) == -1) {
							var temp = {
								name: data[i].localfeatured,
								label: Translation.translate(resultData, data[i].localfeatured).toUpperCase(),
								content: $filter('filter')(data, {localfeatured: data[i].localfeatured}, function (expected, actual) {return expected == actual;})
							};
							addedFeatured.push(data[i].localfeatured);
							$scope.localFeaturedList.push(temp);
							if (data[i].localfeatured == $scope.defaultTab)
								validTabName = true;
						}
					}
					if ($scope.defaultTab != '' && !validTabName) {
						var currentUrl = decodeURI(document.URL);
						var urlParts = currentUrl.split('/#/')[1];
						urlParts = urlParts.split('/');
						//console.log(urlParts);
						var newUrl = '';
						for (var i = 0; i < urlParts.length; i++) {
							if (urlParts[i] == 'tab' || i == urlParts.indexOf('tab') + 1) continue;
							if (newUrl != '') newUrl += '/';
							newUrl += urlParts[i];
						}
						$scope.defaultTab = '';
						$location.path(newUrl, false);
					}
					
					$scope.assets = data;
					
					$scope.label.contactus = Translation.translate(resultData, $scope.label.contactus);
					$scope.label.contactustoday = Translation.translate(resultData, $scope.label.contactustoday);
					$scope.label.overview = Translation.translate(resultData, $scope.label.overview).toUpperCase();

					$scope.ajaxLoading = false;
					
					//Contact Us button label
					$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
						var navContactUs = $filter('filter')(data, {identifier: 'contactus'}, function (expected, actual) {
							expected = expected.toLowerCase();
							expected = expected.replace(/\s/g, '');
							return expected == actual;
						});
						
						if (navContactUs.length > 0) {
							//console.log(navContactUs, navContactUs[0].title);
							$scope.label.contactus = navContactUs[0].title;
						}
					});
					
					$rootScope.updatePageInfo(null, $scope.product.title);
				});
			}
		});
	});

	
	$scope.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		
	};

	$scope.submit = function() {
		LeadgenForm.submitLeadgen('frmleadgen', $scope.submitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		downloadFormFields = LeadgenForm.getDownloadFormFields();
	});
}

function ProductCtrl($rootScope, $scope, $http, $routeParams, $filter, $location, Products, DataProducts, URLHandler, Translation, PromotionAssets) {
	tickValue = URLHandler.getTick();
	$scope.productId = typeof($routeParams.id)=="undefined"?'':$routeParams.id;
	$scope.categoryName = typeof($routeParams.categoryname)=="undefined"?'':$routeParams.categoryname;
	$scope.categoryContent = [];
	$scope.productSpecTypes = [];
	$scope.productSpecTypesHideFrom = 8; //Number of shown spec items (left col)
	$scope.productList = [];
	$scope.childrenProductList = [];
	$scope.selectedSpecs = {spec: []};
	$scope.selectedSpecItems = [];
	$scope.productCount = 0;
	$scope.featuredList = [];
	$scope.featuredCount = -1;
	$scope.baseDomain = $rootScope.baseDomain;
	$scope.breadcrumbs = [];
	$scope.label = {
		narrowresultby: 'narrowresultsby',
		matchedproducts: 'matchedproducts',
		matchedproductspre: 'thereare',
		matchedproductssuf: 'matchselection',
		yourselection: 'yourselections',
		showall: 'showall',
		minimize: 'minimize',
		explore: 'explore'
	};
	$scope.promotionAssets = [];
	$scope.ajaxLoading = true;
	$scope.childrenCategories = [];
	$scope.productTree = [];
	$scope.isLeafNode = false;
	$scope.isSolutions = false;
	$scope.isProductRoot = false;
	$scope.allProduct = [];
	
	var RequestForBreadcrumb = function ($scope, categoryId, breadcrumbInfo, productList) {
		
		if (categoryId != Products.rootid()) {
			for (var i = 0; i < productList.length; i++) {
				if (productList[i].categoryId == categoryId) {
					breadcrumbInfo.unshift({title: productList[i].title, link: '#/products/' + productList[i].cspuniquekey});
					RequestForBreadcrumb($scope, productList[i].parentId, breadcrumbInfo, productList);
					break;
				}
			}
		}
		
		
		if (categoryId == Products.mainproductid() || categoryId == Products.rootid()) {
			var bcHomeItem = {title: 'home', link: '#/'};
			Translation.promise.then(function(resultData){
				bcHomeItem.title = Translation.translate(resultData, bcHomeItem.title);
				if (typeof(breadcrumbInfo[0]) == 'undefined' || breadcrumbInfo[0].title != bcHomeItem.title)
					breadcrumbInfo.unshift(bcHomeItem);
				$scope.breadcrumbs = breadcrumbInfo;
			});
		}
	};
	
	var GetAllChildrenProduct = function ($scope, categoryid, rawdata) {
		var childrenList = $filter('filter')(rawdata, {parentId: categoryid}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		if (childrenList.length == 0) return;
		$scope.childrenProductList = $scope.childrenProductList.concat(childrenList);
		for (var i = 0; i < childrenList.length; i++) {
			GetAllChildrenProduct($scope, childrenList[i].categoryId, rawdata);
		}
		
	};
	
	$scope.CountProduct = function (category) {
		var childrenList = $filter('filter')($scope.allProduct, {categoryId: category.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		var ProNums = childrenList.length;
		if (ProNums == 0) {
			var childrenCat = $filter('filter')($scope.childrenProductList, {parentId: category.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			for (var i = 0; i < childrenCat.length; i++) {
				childrenList = $filter('filter')($scope.allProduct, {categoryId: childrenCat[i].categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				ProNums += childrenList.length;
			}
		}
		return ProNums;
	}
	
	$scope.CheckIfNotSolutionsSingleNode = function (category) {
		if ($scope.isSolutions && parseInt(category.depth) <= 2) return '';
		return '#/categorydetail/' + category.cspuniquekey;
	}
	
	$scope.CheckIfNotSolutions = function (theUrl) {
		if ($scope.isSolutions) return '';
		return theUrl;
	}
	
	$scope.GetProductLink = function (prodItem) {
		var defaultUrl = '#/products/' + $scope.productId + '/productdetail/' + prodItem.id;
		var isLeaf = false;
		if ($rootScope.LeafCategories.length == 0)
			return defaultUrl;
		for (var i = 0; i < $rootScope.LeafCategories.length; i++) {
			if ((prodItem.title.toLowerCase() == $rootScope.LeafCategories[i].value.cspuniquekey.toLowerCase() || $rootScope.LeafCategories[i].value.cspuniquekey.toLowerCase().indexOf(prodItem.title.toLowerCase()) != -1)
				&& $rootScope.LeafCategories[i].value.lineage.indexOf('/' + prodItem.categoryId + '/') >= 0) {
				defaultUrl = '#/products/' + $rootScope.LeafCategories[i].value.cspuniquekey;
				isLeaf = true;
				break;
			}
		}
		if (!isLeaf && prodItem.id == 'PRODUCTPARTNUMP1566'){
			defaultUrl = '#/products/' + $rootScope.LeafCategories[0].value.cspuniquekey;
		}
		return defaultUrl;
	};
	
	$scope.GetCategoryUrl = function (cspUniqueKey) {
		if (!$scope.isProductRoot)
			return '#/categorydetail/' + cspUniqueKey;
		return '#/products/' + cspUniqueKey;
	}
	
	var CheckProductLeafNode = function() {
		$scope.isLeafNode = false;
		return;
		// if ($scope.categoryContent.lineage.indexOf(Products.mainproductid() + '/') == -1) {
			// $scope.isLeafNode = true;
			// return;
		// }
		// for (var i = 0; i < $rootScope.LeafCategories.length; i++) {
			// if ($scope.categoryContent.categoryId == $rootScope.LeafCategories[i].value.categoryId) {
				// $scope.isLeafNode = true;
				// break;
			// }
		// }
	}
	
	//Get data from CT CategoryContent
	Products.promise.then(function(resultData){
		$rootScope.BuildTreeAndGetLeaves(resultData);
		
		$scope.categoryContent = $filter('filter')(resultData, {cspuniquekey: $scope.productId}, function (expected, actual) {return expected == actual;})[0];
		
		CheckProductLeafNode();
		//Get all sub categories
		$scope.childrenProductList.push($scope.categoryContent);
		//if (!$scope.isLeafNode)
		GetAllChildrenProduct($scope, $scope.categoryContent.categoryId, resultData);
	
		
		var mainProductItem = $filter('filter')(resultData, {categoryId: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		var requestedBreadcrumb = [];
		RequestForBreadcrumb($scope, $scope.categoryContent.parentId, requestedBreadcrumb, resultData);
		
		$rootScope.updatePageInfo(null, $scope.categoryContent.title);
			
		if ($scope.categoryContent.lineage.indexOf(mainProductItem.categoryId + '/') != -1) {
			for (var i = $scope.breadcrumbs.length - 1; i >= 0; i--) {
				var bLinkParts = $scope.breadcrumbs[i].link.split('/');
				if (bLinkParts[bLinkParts.length - 1] == mainProductItem.cspuniquekey) {
					$scope.breadcrumbs.splice(i, 1);
					break;
				}
			}
		}
		else
			$scope.isSolutions = true;
		
		//Exception for Products node
		if ($scope.categoryContent.cspuniquekey.toLowerCase() == 'products') {
			$scope.isSolutions = true;
			$scope.isProductRoot = true;
		}
		
		//Get data from CT Products
		DataProducts.promise.then(function(resultProductDetailData){
			//Get all products from current category and its children
			var dataList = [];
			for (var z = 0; z < $scope.childrenProductList.length; z++) {
				var tmpList = $filter('filter')(resultProductDetailData, {categoryId: $scope.childrenProductList[z].categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				if (tmpList.length > 0) {
					dataList = dataList.concat(tmpList);
				}
			}
	
			$scope.allProduct = dataList;
			
			$scope.productCount = dataList.length;
			$scope.featuredList = $filter('filter')(dataList, {statusfeatured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual;});
			$scope.featuredCount = $scope.featuredList.length;
			if (!$scope.isLeafNode) {
				//Get data for filter col
				$scope.productSpecTypes = DataProducts.getProductSpecTypes(dataList);
				//Init spec object, used to compare within filter method
				var specList = '';
				var specItems = '';
				for (var key in $scope.productSpecTypes) {
					var specKey = '';
					var specKeyArr = [];
					$scope.productSpecTypes[key].collapse = true;
					$scope.productSpecTypes[key].showall = false;
					if ($scope.productSpecTypes[key].detail == undefined) continue;
					for (var i = 0; i < $scope.productSpecTypes[key].detail.length; i++) {
						if (specKeyArr.indexOf($scope.productSpecTypes[key].detail[i].key) == -1) {
							specKeyArr.push($scope.productSpecTypes[key].detail[i].key);
							if (specKey != '') specKey += ',';
							specKey += '{"' + $scope.productSpecTypes[key].detail[i].key + '": []}';
						}
					}
					if (specKey == '') specKey = '""';
					else specKey = '[' + specKey + ']';
					if (specItems != '') specItems += ',';
					specItems += '{"' + $scope.productSpecTypes[key].name + '": ' + specKey + '}';
				}
				specList = '[' + specItems + ']';
				$scope.selectedSpecs.spec = JSON.parse(specList);
				
				//Product list
				$scope.productList = dataList.slice(0);
				
				//ltu 6/19/14 remove dups
				var i = 0;
				while (i<$scope.productList.length) {
					var tmp = $scope.productList[i];
					for(var j=i+1;j<$scope.productList.length;j++) {
						if (tmp.id == $scope.productList[j].id) {
							$scope.productList.splice(j,1);
							break;
						}
					}
					i++;
				};
				
				$scope.childrenCategories = $filter('filter')(resultData, {parentId: $scope.categoryContent.categoryId}, function (expected, actual) {return expected == actual;});
				for (var s = 0; s < $scope.childrenCategories.length; s++)
					$scope.childrenCategories[s].nodes = $filter('filter')(resultData, {parentId: $scope.childrenCategories[s].categoryId}, function (expected, actual) {return expected == actual;});
			}
			//Exception Page "Phones" CSP-1341 & Solutions tree
			else {
				$scope.childrenCategories = $filter('filter')(resultData, {parentId: $scope.categoryContent.categoryId}, function (expected, actual) {return expected == actual;});
				for (var s = 0; s < $scope.childrenCategories.length; s++)
					$scope.childrenCategories[s].nodes = $filter('filter')(resultData, {parentId: $scope.childrenCategories[s].categoryId}, function (expected, actual) {return expected == actual;});
			}
		});
		
		PromotionAssets.promise.then(function(resultData){
			$scope.promotionAssets = PromotionAssets.get(resultData, $scope.categoryContent.categoryId);
		});
	});
	
	$scope.getProdLink = function (prodId) {
		return $rootScope.baseDomain + '#/products/' + $scope.productId + '/productdetail/' + prodId;
	}
	
	$scope.getId = function (spec, specDetailIndex) {
		var specName = spec.name.toLowerCase().split(' ').join('-');
		// var specDetailName = specDetail.key.toLowerCase().split(' ').join('-');
		// var specKeyValue = specDetail.value.toLowerCase().split(' ').join('-');
		return (specName + '-' + specDetailIndex);
		//return 'test';
	}
	
	var UpdateSelectedSpecObject = function (specType, specItem, updateFlag) {
		for (var i = 0; i < $scope.selectedSpecs.spec.length; i++) {
			for (var type in $scope.selectedSpecs.spec[i]) {
				if (type == specType) {
					for (var j = 0; j < $scope.selectedSpecs.spec[i][type].length; j++) {
						for (var key in $scope.selectedSpecs.spec[i][type][j]) {
							if (key == specItem.key) {
								if (updateFlag && $scope.selectedSpecs.spec[i][type][j][key].indexOf(specItem.value) == -1)
									$scope.selectedSpecs.spec[i][type][j][key].push(specItem.value);
								else if (!updateFlag){
									for (var k = $scope.selectedSpecs.spec[i][type][j][key].length - 1; k >= 0; k--) {
										if ($scope.selectedSpecs.spec[i][type][j][key][k] == specItem.value) {
											$scope.selectedSpecs.spec[i][type][j][key].splice(k, 1);
											return;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	var removeFromSelectedList = function (specItem) {
		for (var i = $scope.selectedSpecItems.length - 1; i >= 0 ; i--) {
			if ($scope.selectedSpecItems[i].id == specItem.id) {
				$scope.selectedSpecItems.splice(i, 1);
				return;
			}
		}
	}
	
	$scope.isSpecSelected = function (elId, specType, specItem) {
		var temp = {
			id: elId,
			name: specItem.key + ': ' + specItem.value,
			type: specType,
			item: specItem
		}
		if ($scope.selectedSpecItems.indexOf(temp) != -1)
			return true;
		return false;
	}
	
	//Fired everytime filter checkbox clicked, update spec object
	$scope.selectSpec = function (event, specType, specItem) {
		var checkbox = event.target;
		//var label = specItem.key + ': ' + specItem.value;
		var label = specItem.value;
		var temp = {
			id: event.currentTarget.id,
			name: label,
			type: specType,
			item: specItem
		}
		
		if (checkbox.checked) {
			$scope.selectedSpecItems.push(temp);
		}
		else {
			removeFromSelectedList(temp);
		}
		UpdateSelectedSpecObject(specType, specItem, checkbox.checked);
	}
	
	$scope.uncheck = function (selectedSpec) {
		UpdateSelectedSpecObject(selectedSpec.type, selectedSpec.item, false);
		$scope.selectedSpecItems.splice($scope.selectedSpecItems.indexOf(selectedSpec), 1);
		jQuery('#' + selectedSpec.id).attr("checked", false);
	};
	
	//Do the comparision between product item's spec with spec object
	FindInProductSpec = function (type, key, values, itemSpec) {
		var hasValue = false;
		for (var i = 0; i < itemSpec.length; i++) {
			var hasType = false;
			for (var spectype in itemSpec[i]) {
				if (spectype == type) {
					hasType = true;
					for (var j = 0; j < itemSpec[i][spectype].length; j++) {
						var hasKey = false;
						for (var speckey in itemSpec[i][spectype][j]) {
							if (speckey == key) {
								hasKey = true;
								if (values.indexOf(itemSpec[i][spectype][j][speckey]) != -1) {
									return true;
								}
								break;
							}
						}
						if (hasKey) break;
					}
					break;
				}
			}
			if (hasType) break;
		}
		return hasValue;
	}
	
	//Filter, used by product list rendering
	$scope.bySpec = function (entry) {
		var isMatched = true;
		var entrySpec = entry.spec;
		for (var i = 0; i < $scope.selectedSpecs.spec.length; i++) {
			for (var type in $scope.selectedSpecs.spec[i]) {
				for (var j = 0; j < $scope.selectedSpecs.spec[i][type].length; j++) {
					for (var key in $scope.selectedSpecs.spec[i][type][j]) {
						if ($scope.selectedSpecs.spec[i][type][j][key].length > 0) {
							isMatched = FindInProductSpec(type, key, $scope.selectedSpecs.spec[i][type][j][key], entrySpec);
							if (!isMatched) {
								return isMatched;
							}
						}
					}
				}
			}
		}
		return isMatched;
	}
	
	Translation.promise.then(function (resultData) {
		$scope.label.matchedproducts = Translation.translate(resultData, $scope.label.matchedproducts);
		$scope.label.narrowresultby = Translation.translate(resultData, $scope.label.narrowresultby);
		$scope.label.yourselection = Translation.translate(resultData, $scope.label.yourselection);
		$scope.label.showall = Translation.translate(resultData, $scope.label.showall);
		$scope.label.minimize = Translation.translate(resultData, $scope.label.minimize);
		$scope.label.thereare = Translation.translate(resultData, "thereare") + " ";
		$scope.label.matchselection = Translation.translate(resultData, "matchselection");
		$scope.label.products = " " + Translation.translate(resultData, "products") + " ";
		$scope.label.explore = Translation.translate(resultData, $scope.label.explore);
		$scope.ajaxLoading = false;
	});
	
}

function MainCtrl($scope, $http, $rootScope, $filter, Products, URLHandler) {
	tickValue = URLHandler.getTick();	
	$scope.productsList = [];
	$scope.blockHero = [];
	$scope.blockTile = [];
	var blockData = [];
	
	$scope.getTileBlocksLink = function (linkId) {
		var crrProductItem = $filter('filter')($scope.productsList, {categoryId: linkId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
		return $rootScope.baseDomain + "#/products/" + crrProductItem.cspuniquekey;
	};
	
	Products.promise.then(function(resultData){
		$scope.productsList = resultData;
		$http.get('d1.aspx?p2002(ct37000&cd' + Products.rootid() + 'i99)[st(ct37000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
			// blockData = data;
			// $scope.homeblocks=blockData;
			$scope.blockHero = $filter('filter')(data, {type: 'hero'}, function (expected, actual) {return expected == actual;});
			$scope.blockTile = $filter('filter')(data, {type: 'tile'}, function (expected, actual) {return expected == actual;});

			//send reporting codes to WT
			$rootScope.updatePageInfo(this);
		});
	});
}

function AToZCtrl($scope, $window, $http, $rootScope,$location, $filter, Products, URLHandler) {
	$scope.AToZProducts = [];
	$scope.AArray = [];
	$scope.alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z", "1-9"]
	
	$http.get('d1.aspx?p2012(ct51000&cd' + $rootScope.rootProductId + 'i99)[st(ct51000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(responseData) {
		responseData = responseData.reduce(function(field, e1){  
			var matches = field.filter(function(e2){return e1.title== e2.title}); 
			if (matches.length == 0){ 
				field.push(e1);  
			}
			return field;
		}, []);	
		$scope.AToZProducts = responseData;
	});
	
	
	$scope.checkHaveProduct = function(letter){
		var filtered = [];
		
		for (var i = 0; i < $scope.AToZProducts.length; i++) {
			if(typeof($scope.AToZProducts[i].title) == "undefined"){
				continue;
			}
			if ($scope.AToZProducts[i].title.substr(0,1).toLowerCase() == letter[0]) {
				filtered.push($scope.AToZProducts[i]);
			}
		}
		if(filtered.length > 0)
			return "active";
		return "in-active";
	}
	
	$scope.byFirstLetter = function(letter){
		return function(prod) {
			if (typeof(prod.title) == 'undefined') return false;
			if(!isNumeric(letter[0])){
				return prod.title[0].toLowerCase() == letter;
			}
			if(isNumeric(letter[0])){
				return isNumeric(prod.title[0]);
			}
		}
	};
	function isNumeric(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}

	$scope.GoToProduct = function (prod) {
		Products.promise.then(function(resultProductData){
			var categoryItem = $filter('filter')(resultProductData, {categoryId: prod.categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);})[0];
			if (categoryItem == undefined) return;
			$location.url('products/' + categoryItem.cspuniquekey + '/productdetail/' + prod.id);
		});
	}
}

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Products, DataProducts, URLHandler, Translation) {
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	var menuLevelToShow = MAIN_MENU_MAX_LEVEL;
	var allNavItems = [];
	var mainNavItems = [];
	$scope.ProductList = [];
	$scope.SolutionsItem = [];
	//console.log(DataProducts.getProductId());
	var GetFullLink = function (data, i) {
		if (data[i].parent == '') {
			if (data[i].link != '')
				data[i].link = $rootScope.baseDomain + "#/" + data[i].link;
			else
				data[i].link = $rootScope.baseDomain + "#/";
			return data[i].link;
		}
		for (var j = 0; j < data.length; j++) {
			if (data[j].identifier == data[i].parent) {
				data[i].link = data[j].link + '/' + data[i].link;
				if (data[i].link.indexOf('#') != 0)
					data[i].link = $rootScope.baseDomain + '#/' + data[i].link;
				return data[i].link;
			}
		}
	}
	
	var GetLastNodes = function (parentNode) {
		var productNodes = [];
		var childNodes = parentNode.nodes;
		DataProducts.promise.then(function(resultData){
			for (var j = 0; j < childNodes.length; j++) {
				var navItem = childNodes[j];
				var productList = $filter('filter')(resultData, {categoryId: navItem.identifier}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				var featuredProducts = $filter('filter')(productList, {statusfeatured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual;});

				//No featured item
				if (featuredProducts.length == 0) {
					continue;
				}
				
				for (var k = 0; k < featuredProducts.length; k++) {
					var isDup = false;
					for (var l = 0; l < navItem.nodes.length; l++) {
						if (featuredProducts[k].title.toLowerCase() == navItem.nodes[l].title.toLowerCase()) {
							isDup = true;
							break;
						}
					}
					if (isDup) continue;
					var tempChild = 
							{
								"identifier" : featuredProducts[k].id,
								"title" : featuredProducts[k].title,
								"link" : $rootScope.baseDomain + "#/products/" + navItem.cspuniquekey + "/productdetail/" + featuredProducts[k].id,
								"parent" : featuredProducts[k].parentCategoryId,
								"level" : parseInt(navItem.level) + 1,
								"displayorder" : featuredProducts[k].displayOrder,
								"featured" : featuredProducts[k].statusfeatured,
								"reportid" : featuredProducts[k].reportid,
								"cspuniquekey" : ""
							};
					tempChild.nodes = [];
					tempChild.hideSubMenu = false;
					tempChild.hasNodes = false;
					tempChild.dataToggleValue = '';
					productNodes.push(tempChild);
				}
			}
			parentNode.nodes = productNodes;
		});
	}
	
	var GetChildNodes = function (parentNode, treeNode, currentLevel, maxLevel, addedNodes) {
		var childNodes = [];
		if (treeNode.children == undefined) return;
		for (var i = 0; i < treeNode.children.length; i++) {
			var childNode = treeNode.children[i];
			
			//Duplicated Category Content CT 31000
			if (addedNodes.indexOf(childNode.value.categoryId) != -1) {
				console.log('*** DUP CT 31000', childNode);
			}
			
			addedNodes.push(childNode.value.categoryId);
			
			var isProductNode = childNode.value.lineage.indexOf(Products.mainproductid() + '/') != -1;
			var pageKey = isProductNode ? '#/products/' : '#/products/' + $scope.SolutionsItem.value.cspuniquekey + '/categoryname/';
			var temp =
					{
						"identifier" : childNode.value.categoryId,
						"title" : childNode.value.title,
						"link" : $rootScope.baseDomain + pageKey + childNode.value.cspuniquekey,
						"parent" : parentNode.identifier,
						"level" : childNode.value.depth,
						"displayorder" : childNode.value.displayorder,
						"reportid" : childNode.value.reportingId,
						"lineage" : childNode.value.lineage,
						"cspuniquekey" : childNode.value.cspuniquekey
					};
			
			temp.nodes = [];
			temp.hideSubMenu = false;
			temp.hasNodes = true;//(childNode.children != undefined && currentLevel < maxLevel);
			temp.dataToggleValue = '';
			
			// if (temp.level == 3) {
				// temp.link = '';
			// }
			
			if (temp.hasNodes && currentLevel < maxLevel)
				GetChildNodes(temp, childNode, currentLevel + 1, maxLevel, addedNodes);
			childNodes.push(temp);
		}
		parentNode.nodes = childNodes;
		if (!isProductNode && currentLevel == maxLevel - 1) {
			try {
				GetLastNodes(parentNode);
			} catch (exc) {
				console.log(exc);
			}
		}
	}
	
	var UpdateProductsMenu = function (maxLevel) {
		var prodItemIndex = 0;
		var mainProdIndex = 0;
		for (var i = 0; i < $scope.navitems.length; i++) {
			if ($scope.navitems[i].identifier.toLowerCase() == 'products' || typeof($scope.navitems[i].notproduct) != 'undefined') {
				prodItemIndex = i;
				var menuCol = 3;
				if ($scope.navitems[i].identifier.toLowerCase() == 'products') {
					menuCol = 2;
					mainProdIndex = i;
				}
				for (var l = 0; l < $rootScope.AllProducts.length; l++) {
					if ($rootScope.AllProducts[l].value.cspuniquekey.toLowerCase() == $scope.navitems[prodItemIndex].title.toLowerCase()) {
						var addedNodes = [];
						GetChildNodes($scope.navitems[prodItemIndex], $rootScope.AllProducts[l], 1, maxLevel, addedNodes);
						//console.log(GetChildNodes($scope.navitems[prodItemIndex], $rootScope.AllProducts[l], 1, maxLevel, addedNodes));
						break;
					}
				}
				
				//Remove no-children categories in Solutions branch
				if (typeof($scope.navitems[i].notproduct) != 'undefined') {
					for (var m = $scope.navitems[prodItemIndex].nodes.length - 1; m >= 0; m--) {
						if ($scope.navitems[prodItemIndex].nodes[m].nodes.length == 0)
							$scope.navitems[prodItemIndex].nodes.splice(m, 1);
					}
				}
				
				if ($scope.navitems[prodItemIndex].nodes.length <= menuCol) continue;
				var productListColCount = menuCol;
				
				$scope.navitems[prodItemIndex].blocks = [];
				for (var j = 0; j < $scope.navitems[prodItemIndex].nodes.length; j = j + productListColCount) {
					for (var k = 0; k < productListColCount; k++) {
						var crrIndex = k + j;
						if ($scope.navitems[prodItemIndex].nodes[crrIndex] == undefined) break;
						if ($scope.navitems[prodItemIndex].blocks[k] == undefined) $scope.navitems[prodItemIndex].blocks[k] = [];
						$scope.navitems[prodItemIndex].blocks[k].push($scope.navitems[prodItemIndex].nodes[crrIndex]);
					}
				}
			}
		}
		
		GeneratePopularProducts($scope.navitems[mainProdIndex]);
	};
	
	var GeneratePopularProducts = function (prodNode) {
		Products.promise.then(function(resultData){
			DataProducts.promise.then(function(resultProductDetailData){
				var featuredProducts = $filter('filter')(resultProductDetailData, {statusfeatured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual;});
				//var featuredProducts = $filter('orderBy')(featuredProducts, featuredProducts.title, true);
				featuredProducts = featuredProducts.sort(function compare(a,b) {
				  if (a.title < b.title)
					return -1;
				  else if (a.title > b.title)
					return 1;
				  else 
					return 0;
				});
				var featuredProducts = featuredProducts.splice(0,10);
				for(var zi = 0; zi < featuredProducts.length; zi++){
					featuredProducts[zi].displayOrder = zi + 1;
				}
				var popularItem =
						{
							"identifier" : 'popularprods',
							"title" : 'Popular Products',
							"link" : '',
							"parent" : prodNode.identifier,
							"level" : '',
							"displayorder" : '',
							"reportid" : '',
							"lineage" : '',
							"cspuniquekey" : '',
							"hasNodes": true,
							"nodes": []
						};
				
				for (var j = 0; j < featuredProducts.length; j++) {
					var categoryItem = $filter('filter')(resultData, {categoryId: featuredProducts[j].categoryId}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
					if (categoryItem.length == 0) continue;

					var tempChild = 
							{
								"identifier" : featuredProducts[j].id,
								"title" : featuredProducts[j].title,
								"link" : $rootScope.baseDomain + "#/products/" + categoryItem[0].cspuniquekey + "/productdetail/" + featuredProducts[j].id,
								"parent" : featuredProducts[j].parentCategoryId,
								"level" : '',
								"displayorder" : featuredProducts[j].displayOrder,
								"featured" : featuredProducts[j].statusfeatured,
								"reportid" : featuredProducts[j].reportid,
								"cspuniquekey" : ""
							};
					tempChild.nodes = [];
					tempChild.hideSubMenu = false;
					tempChild.hasNodes = false;
					tempChild.dataToggleValue = '';
					popularItem.nodes.push(tempChild);
				}
				
				//CSP-3948: add Phone item into Popular Product list
				var phoneItem = $filter('filter')(resultData, {categoryId: Products.phoneid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
				if (phoneItem.length > 0) {
					if (popularItem.nodes.length == 10) {
						popularItem.nodes.pop();
					}
					phoneItem[0].link = $rootScope.baseDomain + '#/products/' + phoneItem[0].cspuniquekey;
					popularItem.nodes.push(phoneItem[0]);
				}
				
				prodNode.nodes.push(popularItem);
				prodNode.blocks[prodNode.blocks.length] = [];
				prodNode.blocks[prodNode.blocks.length - 1].push(popularItem);
				
			});
		});
	}
	
	//$scope.GetFeaturedProducts();
	
	$scope.GetChildrenList = function (navItem) {
		if (navItem.level != 3) return;
		if (navItem.childrenLoaded != undefined && navItem.childrenLoaded == true) return;
		navItem.childrenLoaded = true;
		DataProducts.promise.then(function(resultData){
			var childNodes = [];
			var productList = $filter('filter')(resultData, {categoryId: navItem.identifier}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			var featuredProducts = $filter('filter')(productList, {statusfeatured: 'yes'}, function (expected, actual) {return expected.toLowerCase() == actual;});

			//No featured item
			if (featuredProducts.length == 0) {
				//window.location = '#/products/' + navItem.cspuniquekey;
				//navItem.childrenLoaded = false;
				var tempChild = 
						{
							"identifier" : '',
							"title" : "Featured item not found",
							"link" : '',
							"parent" : '',
							"level" : parseInt(navItem.level) + 1,
							"displayorder" : '',
							"featured" : '',
							"reportid" : '',
							"cspuniquekey" : ''
						};
				tempChild.nodes = [];
				tempChild.hideSubMenu = false;
				tempChild.hasNodes = false;
				tempChild.dataToggleValue = '';
				navItem.nodes.push(tempChild);
				return;
			}
			
			for (var j = 0; j < featuredProducts.length; j++) {
				var isDup = false;
				for (var k = 0; k < navItem.nodes.length; k++) {
					if (featuredProducts[j].title.toLowerCase() == navItem.nodes[k].title.toLowerCase()) {
						isDup = true;
						break;
					}
				}
				if (isDup) continue;
				var tempChild = 
						{
							"identifier" : featuredProducts[j].id,
							"title" : featuredProducts[j].title,
							"link" : $rootScope.baseDomain + "#/products/" + navItem.cspuniquekey + "/productdetail/" + featuredProducts[j].id,
							"parent" : featuredProducts[j].parentCategoryId,
							"level" : parseInt(navItem.level) + 1,
							"displayorder" : featuredProducts[j].displayOrder,
							"featured" : featuredProducts[j].statusfeatured,
							"reportid" : featuredProducts[j].reportid,
							"cspuniquekey" : ""
						};
				tempChild.nodes = [];
				tempChild.hideSubMenu = false;
				tempChild.hasNodes = false;
				tempChild.dataToggleValue = '';
				navItem.nodes.push(tempChild);
			}
		});
	};
	
	$scope.GenerateOtherTabs = function (resultData) {
		if ($rootScope.AllProducts.length == 1) return;
		
		for (var l = 0; l < $rootScope.AllProducts.length ; l++) {
			if ($rootScope.AllProducts[l].value.categoryId != Products.mainproductid()) {
				if ($rootScope.AllProducts[l].value.categoryId == Products.solutionsid())
					$scope.SolutionsItem = $rootScope.AllProducts[l];
				$scope.navitems.push({
					dataToggleValue: "dropdown",
					displayorder: parseInt($rootScope.AllProducts[l].value.displayorder),
					hasNodes: true, 
					hideSubMenu: false, 
					identifier: $rootScope.AllProducts[l].value.cspuniquekey,
					level: 1, 
					link: "", 
					nodes: [],
					parent: "", 
					title: $rootScope.AllProducts[l].value.title,
					notproduct: true
				});
			}
		}
	}
	
	//Get JSON data for navigation
	$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
		data.pop();
		for (var i = 0; i < data.length; i++) {
			if (data[i].identifier.toLowerCase() == 'solutions') continue;
			data[i].displayorder = parseInt(data[i].displayorder);
			if (data[i].level == '') data[i].level = 1;
			data[i].level == parseInt(data[i].level);
			data[i].nodes = [];
			data[i].hideSubMenu = false;
			data[i].hasNodes = false;
			data[i].dataToggleValue = '';
			if (data[i].level == '' || data[i].level < 2) {
				for (var j = 0; j < data.length; j++) {
					if (data[j].parent == data[i].identifier || data[i].identifier.toLowerCase() == 'products') {
						data[i].hasNodes = true;
						data[i].dataToggleValue = 'dropdown';
						break;
					}
				}
			}
			data[i].link = GetFullLink(data, i);
			// if (data[i].hasNodes == true)
					// data[i].link = '';
			if (data[i].level <= 1) {
				mainNavItems.push(data[i]);
			}       
			else
				allNavItems.push(data[i]);
		}
		
		for (var i = 0; i < mainNavItems.length; i++) {
			if (mainNavItems[i].hasNodes == true)
				mainNavItems[i].link = '';
		}
		
		$scope.navitems = mainNavItems;

		Translation.promise.then(function (resultData) {								
			for(var b in $scope.navitems) {						
				for(var c in resultData) {
					if ($scope.navitems[b].identifier==resultData[c].contextid && $scope.navitems[b].identifier=="home")
						$scope.navitems[b].title=resultData[c].value;
				}
			}
		});	
			
			
		Products.promise.then(function(resultData){
			resultData.pop();
			$rootScope.AllProducts = Products.convertJSONtoTree(resultData);
			$scope.GenerateOtherTabs(resultData);
			UpdateProductsMenu(menuLevelToShow);
		});
	});
	
	//Show/hide for sub menu
	$scope.showSubItems = function (data) {
		data.hideSubMenu = false;
		if (data.nodes.length > 0) return;
		for (var i = 0; i < allNavItems.length; i++) {
			if (allNavItems[i].parent == data.identifier) {
				data.nodes.push(allNavItems[i]);
			}
		}
	}
	
	$scope.hasNoFeaturedItem = function (nodeItem) {
		if (nodeItem.level < 2) return false;
		var noFeaturedItem = true;
		if (!nodeItem.hasNodes) return noFeaturedItem;
		for (var i = 0; i < nodeItem.nodes.length; i++) {
			if (nodeItem.nodes[i].statusfeatured != undefined && nodeItem.nodes[i].statusfeatured.lowercase == 'yes') {
				noFeaturedItem = false;
				break;
			}
		}
		return noFeaturedItem;
	}
}

avayaModule.factory('DataProducts', function ($http, Products) {
	var categoryProductId = Products.mainproductid();
	var data = [];
	return {
		promise: {},
		getProductId: function() {
			return categoryProductId;
		},
		setData: function (productList) {
			data = productList;
		},
		getProductSpecTypes: function (productList) {
			var specTypeList = [];
			var addedName = [];
			var addedDetail = [];
			for (var i = 0; i < productList.length; i++) {
				if (productList[i].id === undefined) continue;
				if (typeof productList[i].spec == 'string') {
					var itemSpec = unescape(productList[i].spec);
					var itemSpecJson = JSON.parse(itemSpec);
					productList[i].spec = itemSpecJson;
				}
				else
					itemSpecJson = productList[i].spec;
				for (var j = 0; j < itemSpecJson.length; j++) {
					for (var specName in itemSpecJson[j]) {
						if (addedName.indexOf(specName) == -1) {
							var specFormat = {
								name: specName,
								detail: []
							};
							specTypeList.push(specFormat);
							addedName.push(specName);
						}
						if (addedDetail[specName] === undefined) addedDetail[specName] = [];
						for (var k = 0; k < itemSpecJson[j][specName].length; k++) {
							var specDetail = [];
							for (var specKey in itemSpecJson[j][specName][k]) {
								if (addedDetail[specName][specKey] === undefined) addedDetail[specName][specKey] = [];
								if (addedDetail[specName][specKey].indexOf(itemSpecJson[j][specName][k][specKey]) == -1) {
									var specDetailFormat = {
										key: specKey,
										value: itemSpecJson[j][specName][k][specKey]
									};
									for (var l = 0; l < specTypeList.length; l++) {
										if (specTypeList[l].name == specName) {
											specTypeList[l].detail.push(specDetailFormat);
											break;
										}
									}
									addedDetail[specName][specKey].push(itemSpecJson[j][specName][k][specKey]);
								}
							}
						}
					}
				}
			}
			return specTypeList;
		}
	}
});

avayaModule.factory('Products', function ($http) {
	var rootId = 1000;
	var categoryProductId = 1001;
	var categorySolutionsId = 1040;
	var categoryPhoneId = 1016;
	var data = [];
	return {
		promise: {},
		rootid: function () {
			return rootId;
		},
		mainproductid: function () {
			return categoryProductId;
		},
		solutionsid: function () {
			return categorySolutionsId;
		},
		phoneid: function () {
			return categoryPhoneId;
		},
		all: function () {
			return data;
		},
		setData: function (productList) {
			data = productList;
		},
		convertJSONtoTree: function (arry) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(arry);
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
						p = item.parentId,
						target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			//console.log(roots[0].children[0].children);
			return roots[0].children;
		}
	}
}).directive('eatClick', function() {
        return function(scope, element, attrs) {
			jQuery(element).click(function(event) {
				event.preventDefault();
			});
        }
});

avayaModule.factory('LeadgenForm', function ($http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Avaya Showcase" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			
			this.downloadFormHeader = this.downloadFormHeader == undefined ? '' : this.downloadFormHeader;
			this.downloadFormDescription = this.downloadFormDescription == undefined ? '' : this.downloadFormDescription;
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

avayaModule.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

avayaModule.factory('Translation', function () {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
});

// ltu 10/29/2013 added PromotionAssets factory
avayaModule.factory('PromotionAssets', function ($filter) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		get: function(dataList, id) {
			//var result = dataList.filter(function(e) {return e.categoryId==id;});
			var result = $filter('filter')(dataList, {categoryId: id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
			//console.log(result);
			return result;
		}
	}
});


avayaModule.run(function ($rootScope, $q, $http, $routeParams, $location, $route, Products, DataProducts, LeadgenForm, URLHandler, Translation,PromotionAssets) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
		var temp = currentUrl.split('/');
		for (var i = 0; i < temp.length; i++) {
			if (temp[i] == 'i') {
				currentTick = '&i=' + temp[i + 1];
				break;
			}
		}
	}
	
	if ($location.$$url.indexOf('/#') == 0) {
		$location.path($location.$$url.replace('/#', ''));
		$location.replace();
	}
	
	var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
	
	$rootScope.AllProducts = [];
	$rootScope.AllTabsAndNavigation = [];
	$rootScope.Dictionary = [];
	$rootScope.rootProductId = Products.rootid();
	$rootScope.CtrlMessage = '';
	$rootScope.PromotionAssets = [];
	$rootScope.baseDomain = document.domain.replace('http://', '');
	$rootScope.LeafCategories = [];
	
	$rootScope.msieversion = function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        // If another browser, return 0
        return 0;
	};
	
	$rootScope.fixHElanguage = function (data) {
			try {
				data = JSON.stringify(data);
				data = jQuery.parseJSON(data.escapeSpecialChars());			
				data = data.replace(/\\/g, "\\\\").replace(/\\\\"/g, '\\"');
				data = jQuery.parseJSON(data);
				return data;
			}
			catch (exc) { 
				console.log('Bad responsed data');
				return jQuery.parseJSON('[]'); 
			}
	};
	
	$rootScope.baseDomain = $rootScope.msieversion() > 0 && $rootScope.msieversion() < 9 ? $rootScope.baseDomain : '';
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ProductCtrl':
					$rootScope.pageTitle = 'Product';
					break;
				case 'ProductDetailCtrl':
					$rootScope.pageTitle = 'Product Detail';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:
					$rootScope.pageTitle = 'Avaya The Power of We';
			}
		}		
		try {
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				jQuery(document).attr('title', $rootScope.pageTitle);
				document.getElementsByTagName('head')[0].appendChild(script);

			}, 1000);
			try{parent.postMessage(window.location.href,"*");} catch(ex){console.log(ex);};
		} catch (Err) {};

		try {
			window.setTimeout(function(){
				jQuery("div.long-desc[keywords='PRODUCTPARTNUMP1566']").each(function(){
					var el = jQuery(this);				
					if (!isMobile()) {
						//console.log("PDF: "+el.prev().text());
						jQuery("a", this).attr("href","global.syndication.tiekinetix.net/customer-resources/showcase/Avaya/Phones/"+el.prev().text().replace(/\s/gi,"")+".pdf");
						jQuery("a", this).colorbox({iframe:true, height: 600, width: 650});
					}
					else {
						jQuery("a", this).attr("href","global.syndication.tiekinetix.net/customer-resources/showcase/Avaya/Phones/"+el.prev().text().replace(/\s/gi,"")+".pdf").attr("target","_blank");
					}
				});
				jQuery("div.first-description-detail[keywords='PRODUCTPARTNUMP0243']","div#first-description").each(function(){
					var el = jQuery(this);				
					var pdf = "global.syndication.tiekinetix.net/customer-resources/showcase/Avaya/InteractionCenter/Avaya_Interaction_Center.pdf";
					el.text("Interaction Center: The Interaction Center software suite provides enterprise-class control of contact-center communications across multiple channels. Click here for details");
					el.css("cursor","pointer");
					if (!isMobile()) {						
						el.colorbox({iframe:true, href: pdf, height: 600, width: 650});
					}
					else {
						el.on("click", function(evt){
							window.open(pdf,"_blank");
						});
					}
				});	
				
				jQuery(document).click();
			}, 1500);
		}
		catch(err) {}
	};
	
	var GetLeafNodes = function (treeNode, maxLevel) {
		if (treeNode.children == undefined) return;
		if (treeNode.value.depth == maxLevel) {
			$rootScope.LeafCategories = $rootScope.LeafCategories.concat(treeNode.children);
			return;
		}
		for (var i = 0; i < treeNode.children.length; i++) {
			GetLeafNodes(treeNode.children[i], maxLevel);
		}
	}
	
	$rootScope.BuildTreeAndGetLeaves = function (rawData) {
		var productTree = Products.convertJSONtoTree(rawData);
		for (var i = 0; i < productTree.length; i++)
			GetLeafNodes(productTree[i], MAIN_MENU_MAX_LEVEL);
	}
	
	if (true) {
		var deferred = $q.defer();
		Products.promise = deferred.promise.then(function (productList) {
			Products.setData(productList);
			return productList;
		});
		
		$http.get('d1.aspx?p2003(ct31000&cd' + $rootScope.rootProductId + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			deferred.resolve(responseData);
		});
		
		// ltu 10/29/2013: added promotion assets
		var deferredPromotionAssets = $q.defer();
		PromotionAssets.promise = deferredPromotionAssets.promise.then(function(dataList){
			PromotionAssets.setData(dataList);
			return dataList;
		});		
		$http.get('d1.aspx?p2003(ct21000&cd' + $rootScope.rootProductId + 'i99&fStatus_Promotion_Y_N~1=yes!)[st(ct21000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {						
			deferredPromotionAssets.resolve(responseData);
		});
		//end of promotion assets


		var deferredDataProduct = $q.defer();
		DataProducts.promise = deferredDataProduct.promise.then(function (productList) {
			DataProducts.setData(productList);
			return productList;
		});

		$http.get('d1.aspx?p2012(ct51000&cd' + $rootScope.rootProductId + 'i99)[st(ct51000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			deferredDataProduct.resolve(responseData);
		});
		
		var translationDeferred = $q.defer();
		Translation.promise = translationDeferred.promise.then(function (dataList) {
			Translation.setData(dataList);
			return dataList;
		});
		
		$http.get('d1.aspx?p2005(ct35000)[]' + currentTick).success(function(responseData) {
			translationDeferred.resolve(responseData);
		});
		
		var leadgenFormDeferred = $q.defer();
		LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
			if (data.length <= 1) return null;
			LeadgenForm.setSupplierInfo(data[0].supplierinfo[0]);
			LeadgenForm.setFields(data[1].contactusformfields);
			return data;
		});
		
		$http.get('d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			leadgenFormDeferred.resolve(responseData);
		});
	}
});