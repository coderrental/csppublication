var urlParser = {
    ourl: null,
    _p: "",
    c: "c",
    p: "p",
    l: "l",
    lowest_domain: "",
    domain: "",
    success: false,
    mask: function(url) {
        var i = this.lowest_domain.lastIndexOf('-');
        if (i >= 0)
            this.c = "-" + this.lowest_domain.substring(i + 1);
        else
            this.c = "-" + this.lowest_domain;
        this.p = "";
        this.success = true;
    },
    legacy: function(url) {
        if (this.lowest_domain.length == 18 && !isNaN(this.lowest_domain)) {
            this.success = true;
            this.p = "p" + this._p;
            this.c = "c" + this.lowest_domain;
			this.l = "";
        }
        else
            throw "legacy";
    },
    proxy: function(url) {
        var arr = this.lowest_domain.split('-');
        var c = "", p = "", l = "";
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].charAt(0) == "l")
                l = arr[i].substring(1);
            else if (arr[i].charAt(0) == "c")
                c = arr[i].substring(1);
            else if (arr[i].charAt(0) == "p")
                p = arr[i].substring(1);
        }
        if (c != "" && p != "" && l != "" && p == this._p) {
            this.success = true;
            this.c = "c" + c;
            this.p = "p" + p;
			this.l = "l" + l;
        }
        else
            throw "not proxy";
    },
    parse: function(url, project) {
        this.ourl = url;
        this.lowest_domain = url.host.substring(0, url.host.indexOf('.'));
        this.domain = url.host.substring(url.host.indexOf('.') + 1);
        this._p = project;
        try {
            this.proxy(url.host);
        }
        catch (e) {
            try {
                this.legacy(url.host);
            }
            catch (e1) {
                this.mask(url.host);
            }
        }
    },
    getUrl: function(lng, replaceDomain, replaceProject) {
        if (this.success) {
			if (lng)
				return this.ourl.protocol + "//l" + lng + "-" + this.p + "-" + this.c + "." + this.domain;
			else {
				return this.ourl.protocol + "//" + (this.l?this.l+"-":"") + (replaceProject?replaceProject+"-":(this.p?this.p+"-":"")) + (this.c?this.c:"") + "." + (replaceDomain?replaceDomain:this.domain);
			}			
        }
        return "";
    },
}
