//array

Array.prototype.findParentId = function(id) {
    for(var i = 0; i < this.length; i++) {
        if(this[i].id == id)
            return i;
    }
    return -1;
}
Array.prototype.sortCategory = function() {
    return this.sort(function(c1, c2) {
        return c1.sortId - c2.sortId;
    });
}
Array.prototype.findItemIndex = function(item) {
    for(var i = 0; i < this.length; i++) {
        if(this[i].id == item.id)
            return i;
    }
}
//category
var categories = {
    selectedCategory : -1,
    selectedBanner : "",
    baseDomain : "",
    audience : "%",
    localPartnerType : "",
    project : "",
    language : "",
    campaignUrl: "",
	supplierId: "",
    items : [],
    campaigns : [],
	featureCampaigns : [],
    Add : function(text, id, parentId, sortOrder, help, reportId, depth) {
        var len = this.items.length;
        if(len == 0)
            this.items.push({
                "text" : text,
                "id" : id,
                "pid" : parentId,
                "sortId" : sortOrder,
                "cnt" : 0,
                "help" : help,
                "reportId" : reportId,
                "depth" : depth
            });
        else if(len > 0 && this.items[len - 1].depth <= depth)
            this.items.push({
                "text" : text,
                "id" : id,
                "pid" : parentId,
                "sortId" : sortOrder,
                "cnt" : 0,
                "help" : help,
                "reportId" : reportId,
                "depth" : depth
            });
        else {
            for(var i = 0; i < len; i++) {
                if(this.items[i].depth > depth) {
                    this.items.splice(i, 0, {
                        "text" : text,
                        "id" : id,
                        "pid" : parentId,
                        "sortId" : sortOrder,
                        "cnt" : 0,
                        "help" : help,
                        "reportId" : reportId,
                        "depth" : depth
                    });
                    break;
                }
            }
        }
    },
    Count : function(id, cid, url1, url2,sid,feature,featureBanner,featureText,uniqueKey) {
        if( typeof (this.campaigns[cid]) == "undefined")
            this.campaigns[cid] = {
                'cid' : cid,
                'cnt' : 1,
                'url' : id,
                'url1' : url1,
                'url2' : url2,
				'feature' : feature,
				'featureBanner' : featureBanner,
				'uniqueKey': uniqueKey
            };
        else
            this.campaigns[cid].cnt++;
		if (feature && feature.toLowerCase() == "yes") {
            var found = false;
            for (var i = 0; i < this.featureCampaigns.length; i++) {
                if (this.featureCampaigns[i].featureBanner == featureBanner && this.featureCampaigns[i].cid == cid) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                this.featureCampaigns.push({
                    'cid': cid,
                    'featureBanner': featureBanner,
                    'featureText': featureText,
					'uniqueKey': uniqueKey
					
                });
            }
        }
    },
    NestCategories : function() {
        var _this = this;
        var i = this.items.length - 1;
        var id = -1;
        //assign campaign counts
        while(i >= 0) {
            if( typeof (this.campaigns[this.items[i].id]) != "undefined")
                this.items[i].cnt = this.campaigns[this.items[i].id].cnt;
            id = this.items.findParentId(this.items[i].pid);
            if(id != -1) {
                var temp = this.items.splice(i, 1);
                if( typeof (this.items[id]) != "undefined") {
                    if( typeof (this.items[id]["children"]) == "undefined")
                        this.items[id]["children"] = [];
                    temp[0].parent = this.items[id];
                    this.items[id].children.push(temp[0]);
                    this.items[id].cnt += temp[0].cnt;
                }
            }
            i--;
        }

        //hard code values for now
        var pid = -1;
        if( typeof (this.project) != "undefined" && this.project != "homedeco") {
            if(this.localPartnerType == "distributor")
                pid = 2;
            else if(this.localPartnerType == "reseller")
                pid = 2;

            // remove unnecessary items
			/*
            i = this.items.length - 1;
            while(i >= 0) {
                if(this.items[i].pid != pid)
                    this.items.splice(i, 1);
                i--;
            }
			*/
        }
        // remove empty categories
        function RemoveEmptyCats(cats, level) {
            if( typeof (cats["children"]) != "undefined") {
                // not the last level
                var i = cats.children.length - 1;
                while(i >= 0) {
                    if(cats.children[i].cnt == 0)
                        cats.children.splice(i, 1);
                    else {
                        if( typeof (cats.url) == "undefined") {
                            if( typeof (_this.campaigns[cats.children[i].id]) != "undefined") {
                                cats.url = cats.children[i].url = _this.campaigns[cats.children[i].id].url;
                                cats.cnId = cats.children[i].id;
                            }
                        }
                        RemoveEmptyCats(cats.children[i], ++level);
                    }
                    i--;
                }
            } else if(cats.cnt <= 0) {
                var j = cats.parent.children.findItemIndex(cats);
                cats.parent.url = _this.campaigns[cats.parent.children[j].id].url;
                cats.parent.children.splice(j);
            } else {
                try {
                    if( typeof (cats.parent) != "undefined") {
                        cats.parent.url = _this.campaigns[cats.id].url;
                        if( typeof (cats.url) == "undefined")
                            cats.url = _this.campaigns[cats.id].url;
                    } else
                        cats.url = _this.campaigns[cats.id].url;
                    cats.cnId = cats.id;
                } catch (e) {
                }
            }
            try {
                if( typeof (cats.cnId) == "undefined" && typeof (cats.children) != "undefined" && cats.children.length == 0)
                    cats.cnId = cats.id;
            } catch(e1) {
            }
            return cats;
        }

        var i = this.items.length - 1;
        while(i >= 0) {
            if(this.items[i].cnt == 0)
                this.items.splice(i, 1);
            else {
                this.items[i] = RemoveEmptyCats(this.items[i], 0);
                if( typeof (this.items[i].children) != "undefined") {
                    if(this.items[i].children.length > 0) {
                        for(var j = 0; j < this.items[i].children.length; j++) {
                            if( typeof (this.items[i].children[j]) != "undefined") {
                                this.items[i].url = this.items[i].children[j].url;
                                if( typeof (this.items[i].children[j].cnId) != "undefined")
                                    this.items[i].cnId = this.items[i].children[j].cnId;
                                break;
                            }
                        }
                    } else if( typeof (_this.campaigns[this.items[i].id]) != "undefined") {
                        this.items[i].url = _this.campaigns[this.items[i].id].url;
                        this.items[i].cnId = this.items[i].id;
                    }
                }
            }
            i--;
        }
    },
    CreateHtml : function(audience) {
        var _this = this;
        function InnerLoop(item, html, level) {
            var rel = item.url;
            if(level == 0 || ( typeof (item["children"]) == "undefined" && (item.parent.children.length > 1 || _this.project == "homedeco" || _this.project == "digitalchannel")) || typeof (item["children"]) != "undefined") {
                html += "<li>";
                if(level == 0)
                    html += "<h2 title='" + item.help + "'><a href='/d1.aspx?p1002((ct12000&cd" + item.id + "i99&fCampaigns_Local_Audience~1=" + audience + "!)&(s"+_this.supplierId+"))[st(ct12000*Status_Sort_Order%20desc*)]' rel='" + rel + "' cid='" + item.id + "' cnId ='" + item.cnId + "' cspObj=\"REPORT\" cspType=\"TOPIC\" cspEnglishValue=\"" + item.reportId + "\">" + item.text + "</a></h2>";
                else {
                    if( typeof (item["children"]) == "undefined" && item.parent.children.length > 1)
                        html += "<a href='/d1.aspx?p1002((ct12000&cd" + item.id + "i99&fCampaigns_Local_Audience~1=" + audience + "!)&(s"+_this.supplierId+"))[st(ct12000*Status_Sort_Order%20desc*)]' rel='" + rel + "' cid='" + item.id + "' cnId ='" + item.cnId + "' cspObj=\"REPORT\" cspType=\"TOPIC\" cspEnglishValue=\"" + item.reportId + "\">" + item.text + " (<strong>" + item.cnt + "</strong>) </a>";
                    else if( typeof (item["children"]) != "undefined")
                        html += "<a href='/d1.aspx?p1002((ct12000&cd" + item.id + "i99&fCampaigns_Local_Audience~1=" + audience + "!)&(s"+_this.supplierId+"))[st(ct12000*Status_Sort_Order%20desc*)]' rel='" + rel + "' cid='" + item.id + "' cnId ='" + item.cnId + "' cspObj=\"REPORT\" cspType=\"TOPIC\" cspEnglishValue=\"" + item.reportId + "\">" + item.text + " (<strong>" + item.cnt + "</strong>) </a>";
                    else if(_this.project == "homedeco" || _this.project == "digitalchannel")
                        html += "<a href='/d1.aspx?p1002((ct12000&cd" + item.id + "i99&fCampaigns_Local_Audience~1=" + audience + "!)&(s"+_this.supplierId+"))[st(ct12000*Status_Sort_Order%20desc*)]' rel='" + rel + "' cid='" + item.id + "' cnId ='" + item.cnId + "' cspObj=\"REPORT\" cspType=\"TOPIC\" cspEnglishValue=\"" + item.reportId + "\">" + item.text + " (<strong>" + item.cnt + "</strong>) </a>";
                }

                if( typeof (item["children"]) != "undefined") {
                    if(item.children.length > 0) {
                        html += "<ul level=" + level + ">";
                        item.children.sortCategory();
                        for(var j = 0; j < item.children.length; j++)
                        html = InnerLoop(item.children[j], html, ++level);
                        html += "</ul>";
                    }
                }
                html += "</li>";
            }
            return html;
        }

        var html = "<ul>";
        this.items.sortCategory();
        for(var i = 0; i < this.items.length; i++) {
            html = InnerLoop(this.items[i], html, 0);
        }
        html += "</ul>";
        return html;
    },
    GetCampaignCode : function() {
        var code = "<script type=\"text/javascript\" src=\"";
        code += window.location.protocol + "//" + this.baseDomain + "/Csp/?t=campaign";
        if(this.selectedCategory)
            code += "&category=" + this.selectedCategory;
        if(this.audience && this.audience != "%")
            code += "&audience=" + this.audience;
        if(this.selectedBanner)
            code += "&banner=" + this.selectedBanner;
        code += "&lng=" + this.language;
        return code + "\"></script>";
        //return code + "\" defer=\"defer\" async=\"true\" ></script>";
    },
    BindEvents : function(location, target, steps, welcomeContainer, introText) {
        var _this = this;
        var _loc = steps;
        $("a", location).click(function(e) {
            e.preventDefault();
            var _t = $(target);
            var _a = $(this);
            if(_t.length > 0) {
                if($(location).hasClass("csp-hide"))
                    $(location).removeClass("csp-hide");
                if(_t.hasClass("csp-hide"))
                    _t.removeClass("csp-hide");
                _this.selectedCategory = _a.attr("cid");

                if(!_t.hasClass("csp-hide"))
                    _t.addClass("csp-hide");
                if(!$(welcomeContainer).hasClass("csp-hide"))
                    $(welcomeContainer).addClass("csp-hide");

                $(steps).fadeOut('slow', function() {
                    $("#csp-loading-indicator").show();
                    _t.load(_a.attr('href'), function(html, status) {
                        $("#csp-loading-indicator").hide();
                        if(status == "success") {
                            // replace the image urls for the steps
                            /*
                             var url1, url2;
                             var campaign = _this.campaigns[_a.attr("cnId")];
                             if (campaign != null) {
                             $("#csp-step1-image").attr("src", campaign.url1);
                             $("#csp-step2-image").attr("src", campaign.url2);

                             if (campaign.url1 != "") {
                             $("#csp-step1-image").css("visibility", "visible");
                             $("#csp-step1-image").css("display", "inline");
                             } else {
                             $("#csp-step1-image").css("visibility", "hidden");
                             $("#csp-step1-image").css("display", "none");
                             }

                             if (campaign.url2 != "") {
                             $("#csp-step2-image").css("visibility", "visible");
                             $("#csp-step2-image").css("display", "inline");
                             } else {
                             $("#csp-step2-image").css("visibility", "hidden");
                             $("#csp-step2-image").css("display", "none");
                             }
                             }*/
                            var campaign = $("div.csp-campaign:first", _t);
                            if(campaign.length > 0) {
                                _this.campaignUrl = campaign.attr("path");                                  
                                if(campaign.attr("thumbnailimage") != "") {
                                    $("#csp-step2-image").attr("src", campaign.attr("thumbnailimage"));
                                    $("#csp-step2-image").css("visibility", "visible");
                                    $("#csp-step2-image").css("display", "inline");
                                } else {
                                    $("#csp-step2-image").css("visibility", "hidden");
                                    $("#csp-step2-image").css("display", "none");
                                }
                                if(campaign.attr("previewimage") != "") {
                                    $("#csp-step1-image").attr("src", campaign.attr("previewimage"));
                                    $("#csp-step1-image").css("visibility", "visible");
                                    $("#csp-step1-image").css("display", "inline");
                                } else {
                                    $("#csp-step1-image").css("visibility", "hidden");
                                    $("#csp-step1-image").css("display", "none");
                                }
                            }

                            if(_a.attr("cid") != 97 && _a.attr("cid") != 98) {
                                $(steps).show();
                                if($("li", this).length > 0)
                                    $("#csp-asset-description").show();
                                else
                                    $("#csp-asset-description").hide();
                            }
							
							//Hide and Collapse for Siemens
							
								//$("a.csp-campaign-preview-link","div#csp-campaign-container").before("<span>hello</span>");
							    $("ul", ".csp-campaign").css("display","none");
								$("a.csp-campaign-preview-link","div#csp-campaign-container").before("<img class='csp_showHide' style='margin-right:10px; cursor:pointer;' src='http://global.syndication.tiekinetix.net/customer-resources/ppp/custom/unify/images/show-icon.png'>");
								$(".csp_showHide","div#csp-campaign-container").click(function(){
									//var ul = $(this);
									$(this).parent().nextAll("ul").toggle(function(){
										var myStyle= $(this).css("display");
										if (myStyle=="none")
										{
											$(this).siblings(".csp-campaign-title").children(".csp_showHide").attr('src','http://global.syndication.tiekinetix.net/customer-resources/ppp/custom/unify/images/show-icon.png');
											$(document).trigger("click");
										}
										else
										{
											$(this).siblings(".csp-campaign-title").children(".csp_showHide").attr('src','http://global.syndication.tiekinetix.net/customer-resources/ppp/custom/unify/images/hide-icon.png');
										    $(document).trigger("click");
										}	
									});
								});
							
							
                            _this.selectedBanner = $("input:checked", "#csp-banner-size-selection").val();
                            $("h2:first", _loc).text(_a.text());
                            //$("#csp-microsite-preview").attr("href", "ftp/campaign/" + _this.project + "/" + _a.attr("rel") + "/" + _this.language);
                            $("#csp-microsite-preview").attr("href", "ftp/campaign/" + _this.project + "/" + _this.campaignUrl + "/" + _this.language);
                            $("#csp-generated-code").text(_this.GetCampaignCode());
                            $("a.csp-campaign-preview-link").each(function() {
                                var pl = $(this);
									if(pl.attr('id') == $("#csp-steps").attr('mykey'))
									{
									pl.siblings(".csp_showHide").trigger("click");
									}
									
                                if(pl.attr("path")) {
									
                                    pl.attr("href", "ftp/campaign/" + _this.project + "/" + pl.attr("path") + "/" + _this.language);
                                    pl.html(pl.html() + " &raquo;");
                                    pl.fancybox(fbOptions);
                                }
                            });
                            var dups = [];
                            $("div.csp-bottom-line[cid]").each(function() {
                                if(dups[$(this).attr("cid")])
                                    $(this).hide();
                                else
                                    dups[$(this).attr("cid")] = true;
                            });
                            // bind the fancy box events
                            $("li a", this).each(function() {
                                var el = $(this);
                                var fbType = (el.attr("fbtype") == undefined) ? "inline" : el.attr("fbtype");
                                el.fancybox({
                                    'transitionIn' : 'elastic',
                                    'transitionOut' : 'elastic',
                                    'speedIn' : 600,
                                    'speedOut' : 200,
                                    'overlayShow' : true,
                                    'type' : fbType,
                                    'autoDimensions' : true,
                                    'autoScale' : false,
                                    'scrolling' : 'no',
									
                                    'onComplete' : function() {
                                        //$(this).height($(this).height() + 50);
                                    }
                                });
                            });
                            // bind reporting
                            CspReportLib.trackAjaxHtml(this);

                            //campaign seperator
                            var divs = $(this).children("div");
                            for(var i = 0; i < divs.length; i++) {
                                if(i < divs.length - 1)
                                    $(divs[i]).addClass("csp-bottom-line");
                                //asset seperator
                                //                                var lis = $("ul:first li", divs[i]);
                                //                                for (var j = 0; j < lis.length; j++) {
                                //                                    if (j < lis.length - 1)
                                //                                        $(lis[j]).addClass("csp-right-line");
                                //                                    if (j > 0)
                                //                                        $(lis[j]).addClass("csp-padding-right-30");
                                //                                }
                            }
                            // add the counter
                            //var value = $("#csp-cpc").val();
                            var value = $(html).filter("div").length;
                            if(!isNaN(value))
                                $("#csp-content-count").html(introText.replace("{campaignNum}", value));
                            if(_t.hasClass("csp-hide"))
                                _t.removeClass("csp-hide");
								


                            $(document).trigger("click");
                        }
                        //end of load
                    });
                    //end of fadeout
                });
            }
            return false;
        });
    },
    Run : function(location, target, steps, welcomeContainer, audience, introText) {
        if(audience == "" || audience == "All")
            audience = "%";
        this.audience = audience;
        this.NestCategories();
        var html = this.CreateHtml(audience);
        $(location).html(html);
        this.BindEvents(location, target, steps, welcomeContainer, introText);
    }
};
//menu
(function($) {
    $.fn.CspMenu = function(prop, callback) {
        // hide the children
        var menu = $(this);
        var selectedIndex = 1;
        setCss = function(el, level) {
            var _el = $(el);
            if(_el.length == 1)
                _el.addClass("sub-menu-" + level);
            var children = el.children("li").children("ul");
            if(children.length > 0)
                children.each(function() {
                    setCss($(this), level + 1)
                });
        }
        setCss(menu, 1);
        //$("ul.sub-menu-3", menu).each(function() { if ($(this).children("li").length == 0) { $(this).remove(); } });
        $("ul.sub-menu-3", menu).each(function() {
            $(this).remove();
        });
        //hide
        $("ul", menu).hide();
        $("li.menu-item-selected", menu).parent().show();

        $("li", menu).hover(function() {
            var el = $(this);
            var pel = el.parent("ul");
            /*
             if (pel.hasClass("sub-menu-1") && !el.hasClass("menu-item-over")) {
             el.addClass("menu-item-over");
             el.children("ul").effect("slide", { direction: "up", mode: "show" }, 200, function() { });
             }*/
            if(pel.hasClass("sub-menu-1") && !el.hasClass("menu-item-over")) {
                el.addClass("menu-item-over");
                if(!$("h2", el).hasClass("menu-item-selected"))
                    el.children("ul").effect("slide", {
                        direction : "up",
                        mode : "show"
                    }, 200, function() {
                        if(!el.hasClass("menu-item-over"))
                            el.children("ul").hide();
                    });
            }
        }, function() {
            var el = $(this);
            var pel = el.parent("ul");

            if(el.hasClass("menu-item-over")) {
                el.removeClass("menu-item-over");
                el.children("ul").hide();
            }
            $("li.menu-item-selected", menu).parent().show();
            $("h2.menu-item-selected", menu).next("ul").show();

        });
        $("li a", menu).click(function() {
            $("li.menu-item-selected", menu).parent().hide();
            $("h2.menu-item-selected", menu).next("ul").hide();
            var el = $(this);
            var className = "menu-item-selected";
            $("." + className).removeClass(className);
            el.parent().addClass(className);
        });
    };
})(jQuery);
