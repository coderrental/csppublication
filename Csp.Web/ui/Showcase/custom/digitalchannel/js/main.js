angular.module('ngSiemens').config(function($routeProvider) {
	$routeProvider.
		when('/', {controller:ListCtrl, templateUrl:'list.html'}).
		when('/edit/:projectId', {controller:EditCtrl, templateUrl:'detail.html'}).
		when('/new', {controller:CreateCtrl, templateUrl:'detail.html'}).
		otherwise({redirectTo:'/'});
});

function MainNavCtrl($scope) {
	// $scope.navitems = [
		// { text: "Home", url: "javascript:void(0)" },
		// { text: "Products", url: "javascript:void(0)" }
	// ];
	
	// $scope.add = function () {
		// var maxItemCount = 5;
		// if (this.navitems.length < maxItemCount)
			// this.navitems.push({text: "New-" + this.navitems.length, url: "javascript:void(0)"});
	// };
}