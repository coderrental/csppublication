
if (typeof (CspReportLib) == "undefined") {
    CspReportLib = {};
    (function(lib) {
        if (typeof (document.getElementsByCspObj) == "undefined") {
            document.getElementsByCspObj = function(type, parent) {
                var p = (parent) ? parent : document;
                var elements = p.getElementsByTagName("*");
                var results = [];
                for (var i = 0; i < elements.length; i++) {
                    var e = elements[i];

                    if (e.attributes)
                        for (var j = 0; j < e.attributes.length; j++) {
                        if (e.attributes[j].name.toLowerCase() == "cspobj") {
                            if (e.attributes[j].value == type) {
                                results.push({
                                    "el": e,
                                    "type": e.getAttribute("csptype", 0),
                                    "name": e.attributes[j].name,
                                    "value": e.getAttribute("cspenglishvalue", 0),
                                    "field": e.getAttribute("cspfield", 0)
                                });
                            }
                            break;
                        }
                    }
                }
                return results;
            }
        }
        var ReplacementTypes = {
            company: function(obj) {
                if (!cspConsumerInfo) return;
                obj.el.innerHTML += cspConsumerInfo[obj.field];
            }
        }
        var ReportTypes = {
            CharCap: 1200, //the assumed max lenth of the first part of the URL. 
            FixedCharCount: 1600, // the assumed max length of the CharCap + ConversionType + ConvertionShown + ConvertionContent
            //TODO: set a single constant to hold the actual max value that we can allow, which is 400 at the moment
            Data: {},
            trackConversion: function(reportObj, wt, type) {
                // if (this.getCharCount(wt.DCSext) < this.CharCap) {
                if (this.getCharCount(wt.DCSext) + this.CharCap < this.FixedCharCount) {
                    if (wt.DCSext["ConversionType"])
                        wt.DCSext["ConversionType"] = wt.DCSext["ConversionType"] + ";" + type;
                    else
                        wt.DCSext["ConversionType"] = type;

                    if (wt.DCSext["ConversionShown"])
                        wt.DCSext["ConversionShown"] = wt.DCSext["ConversionShown"] + ";" + reportObj.value;
                    else
                        wt.DCSext["ConversionShown"] = reportObj.value;

                    if (wt.DCSext["ConversionContent"])
                        wt.DCSext["ConversionContent"] = wt.DCSext["ConversionContent"] + ";" + reportObj.value;
                    else
                        wt.DCSext["ConversionContent"] = reportObj.value;
                }
                else {
                    if (!this.Data["ConversionType"])
                        this.Data["ConversionType"] = [];
                    this.Data["ConversionType"].push(type);

                    if (!this.Data["ConversionShown"])
                        this.Data["ConversionShown"] = [];
                    this.Data["ConversionShown"].push(reportObj.value);

                    if (!this.Data["ConversionContent"])
                        this.Data["ConversionContent"] = [];
                    this.Data["ConversionContent"].push(reportObj.value);
                }
            },
            tabs: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "Tabs");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "Tabs";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            downloads: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "Downloads");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "Downloads";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            buybutton: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "buybutton");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "buybutton";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            title: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                var s = reportObj.el.innerHTML;
                if (s.length > 50) s = s.substring(0, 50);
                wt.DCSext["csp_pageTitle"] = s;
            },
            links: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                var linkType = "link";
                if (reportObj.el.getElementsByTagName("img").length > 0)
                    linkType = "image-link";
                else if (reportObj.el.getElementsByTagName("div").length > 0 || reportObj.el.getElementsByTagName("span").length > 0)
                    linkType = "other-link";
                else
                    linkType = "text-link";
                this.trackConversion(reportObj, wt, linkType);
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = linkType;
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            video: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                //this.trackConversion(reportObj, wt, "Video");
                if (typeof (wt.DCSext["csp_vname"]) == "undefined")
                    wt.DCSext["csp_vname"] = reportObj.value;
                else
                //wt.DCSext["csp_vname"] = ";" + reportObj.value; fixed by EGO 06-09-11 wasn't concatinating
                    wt.DCSext["csp_vname"] = wt.DCSext["csp_vname"] + ";" + reportObj.value;

                if (typeof (wt.DCSext["csp_vaction"]) == "undefined")
                    wt.DCSext["csp_vaction"] = "SHOWN";
                else
                //wt.DCSext["csp_vaction"] = ";SHOWN"; fixed by EGO 06-09-11 wasn't concatinating
                    wt.DCSext["csp_vaction"] = wt.DCSext["csp_vaction"] + ";SHOWN";

            },
            leadgen: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "ContactUs");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "ContactUs";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            scenario: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "scenario");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "scenario";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            nav: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "Nav");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "Nav";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            click: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "Click";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            topic: function(reportObj, wt) {
                if (!cspConsumerInfo) return;
                this.trackConversion(reportObj, wt, "Topic");
                lib.bindEvent(reportObj.el, "click", function(e) {
                    var olDCSext = wt.DCSext;
                    wt.DCSext["ConversionShown"] = null;
                    wt.DCSext["ConversionClick"] = reportObj.value;
                    wt.DCSext["ConversionType"] = "Topic";
                    wt.DCSext["ConversionContent"] = reportObj.value;
                    wt.dcsMultiTrack();
                    wt.DCSext = olDCSext;
                });
            },
            common: function(wt) {
                if (!cspConsumerInfo) return;
                wt.DCSext.csp_companyname = cspConsumerInfo.companyname;
                wt.DCSext.csp_companyId = cspConsumerInfo.companies_Id;
                wt.DCSext.csp_country = cspConsumerInfo.country;
                wt.DCSext.csp_language = cspConsumerInfo.lng;
                if (cspConsumerInfo.lngDesc) {
                    wt.DCSext.language = cspConsumerInfo.lngDesc;
                    wt.DCSext.csp_language_descr = cspConsumerInfo.lngDesc;
                }
                if (typeof (cspConsumerInfo.sId) != "undefined" && cspConsumerInfo.sId) {
                    wt.DCSext.vendorName = cspConsumerInfo.sName;
                    wt.DCSext.csp_vendorid = cspConsumerInfo.sId;
                }
                else if (typeof (cspContentCollection) != "undefined" && cspContentCollection && cspContentCollection.length > 0) {
                    var firstInstance = cspContentCollection[0];
                    if (typeof (firstInstance["sId"] != "undefined")) {
                        wt.DCSext.vendorName = firstInstance.sName;
                        wt.DCSext.csp_vendorid = firstInstance.sId;
                    }
                }
                if (typeof (cspContentCollection) != "undefined" && cspContentCollection && cspContentCollection.length > 0) {
                    for (var i = 0; i < cspContentCollection.length; i++) {
                        if (wt.DCSext["campaignName"])
                            wt.DCSext["campaignName"] = cspContentCollection[i].Microsite_Link_Url; // there shouldnt be more than 1 given campaign running at a given time.
                        //wt.DCSext["campaignName"] = ";" + cspContentCollection[i].Microsite_Link_Url; // updated by EGO 06-14-11 to cover production content types
                        else
                            wt.DCSext["campaignName"] = cspContentCollection[i].Microsite_Link_Url; // updated by EGO 06-14-11 to cover production content types
                    }
                }
            },
            specific: function(wt) {
                if (typeof (cspContentCollection) != "undefined" && cspContentCollection.length > 0 && typeof (staticContentType) != "undefined" && typeof (cspContentCollection[0].CSP_Reporting_Id) != "undefined") {
                    wt.DCSext.csp_stype = staticContentType;
                    wt.DCSext.csp_sname = cspContentCollection[0].CSP_Reporting_Id;
                }
            },
            clear: function(wt) {
                wt.DCSext["ConversionContent"] = null;
                wt.DCSext["ConversionShown"] = null;
                wt.DCSext["ConversionClick"] = null;
                wt.DCSext["ConversionType"] = null;
                wt.DCSext["csp_vname"] = null;
                wt.DCSext["csp_vaction"] = null;
            },
            getCharCount: function(wt) {
                var charCount = 0;
                for (var name in wt)
//TODO: Avoid the *magic* numbers and account for other characters included such as ';'
                    charCount += wt[name] ? name.length + 1 + (""+wt[name]).length : 0;
                return charCount;
            },
            sendMoreWtIfNeeded: function(wt) {
                /* Description: generate aditional requests to WT when the data is too much to be send on a single request. 
                 * Also, sends as few requests as possible 
                 * 
                 * Assumes this.data will be filled with all the extra tagged elements that need to be tracked
                 * 
                 * Receives: wt // this will be modified in this function
                 */
                if (this.getCharCount(this.Data) > 0) {
                    var i = 0; //Always use the first element of the array (element 0)
                    while (this.Data["ConversionType"].length > 0) {
                        this.clear(wt); // we have to clean this on every iteration
                        // var charCount = "ConversionType".length + "ConversionShown".length + "ConversionContent".length + 3, i = 0;
                        var charCount = this.Data["ConversionType"][i].length + this.Data["ConversionContent"][i].length + this.Data["ConversionShown"][i].length + 3;
                        // while (this.Data["ConversionType"].length > 0 && charCount < this.CharCap) {
                        // while (this.Data["ConversionType"].length > 0 && charCount + this.CharCap < this.FixedCharCount) {
                        do {
                            if (wt.DCSext["ConversionType"])
                                wt.DCSext["ConversionType"] = wt.DCSext["ConversionType"] + ";" + this.Data["ConversionType"][i];
                            else
                                wt.DCSext["ConversionType"] = this.Data["ConversionType"][i];

                            if (wt.DCSext["ConversionShown"])
                                wt.DCSext["ConversionShown"] = wt.DCSext["ConversionShown"] + ";" + this.Data["ConversionShown"][i];
                            else
                                wt.DCSext["ConversionShown"] = this.Data["ConversionShown"][i];

                            if (wt.DCSext["ConversionContent"])
                                wt.DCSext["ConversionContent"] = wt.DCSext["ConversionContent"] + ";" + this.Data["ConversionContent"][i];
                            else
                                wt.DCSext["ConversionContent"] = this.Data["ConversionContent"][i];
                            // charCount += this.Data["ConversionType"][i].length + this.Data["ConversionContent"][i].length + this.Data["ConversionShown"][i].length;
                            this.Data.ConversionType.splice(i, 1);
                            this.Data.ConversionShown.splice(i, 1);
                            this.Data.ConversionContent.splice(i, 1);
                            if (this.Data["ConversionType"].length <= 0) { // This makes it IE safe
                                break;
                            }
                            charCount += this.Data["ConversionType"][i].length + this.Data["ConversionContent"][i].length + this.Data["ConversionShown"][i].length + 3;
                        } while (charCount + this.CharCap < this.FixedCharCount)
                        wt.dcsMultiTrack();
                    }
                }
            }
        }

        lib["elements"] = [];

        lib.bindEvent = function(element, type, eventHandler) {
            if (element.addEventListener) //others
                element.addEventListener(type, eventHandler, false);
            else if (element.attachEvent) //ie
                element.attachEvent('on' + type, eventHandler);
            else
                lib.log("Cant bind event listener for %o", element);
        }

        lib.log = function() {
            if (console && console.log)
                console.log(arguments);
        }

        lib.getQueryValue = function(url, name, default_) {
            if (default_ == null) default_ = "";
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
            var qs = regex.exec(url);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }

        lib.trackVideo = function(videoName, status) {
            if (typeof (this["wt"]) == "undefined") {
                this.log("this function needs to be in window.load or document.ready");
                return;
            }
            this["wt"].DCSext["csp_vname"] = videoName;
            this["wt"].DCSext["csp_vaction"] = status;
            this["wt"].dcsMultiTrack();
            this["wt"].DCSext["csp_vname"] = null;
            this["wt"].DCSext["csp_vaction"] = null;
        }

        lib.trackVideoStatus = {
            start: function(videoName) {
                lib.trackVideo(videoName, "START");
            },
            mid: function(videoName) {
                lib.trackVideo(videoName, "50%");
            },
            finish: function(videoName) {
                lib.trackVideo(videoName, "FINISH");
            },
            stop: function(videoName) {
                lib.trackVideo(videoName, "STOP");
            }
        };

        lib.trackAjaxHtml = function(el) {
            if (typeof (this.wt) == "undefined") return;
            ReportTypes.clear(this.wt);
            var elements = document.getElementsByCspObj("REPORT", el);
            for (var i = 0; i < elements.length; i++) {
                try {
                    ReportTypes[elements[i].type.toLowerCase()](elements[i], this.wt);
                    //this["elements"].push(elements[i]);
                }
                catch (ex) {
                    this.log("Error: Type [" + elements[i].type + "] is not supported. Error: %o", ex);
                }
            }
            if (elements.length > 0)
                this.wt.dcsMultiTrack();
            ReportTypes.clear(this.wt);
        };

        lib.executeReplacement = function() {
            if (typeof (cspConsumerInfo) != "undefined") {
                var elements = document.getElementsByCspObj("content");
                for (var i = 0; i < elements.length; i++) {
                    try {
                        ReplacementTypes[elements[i].type.toLowerCase()](elements[i]);
                    }
                    catch (ex) {
                        this.log("Error: Type [" + elements[i].type + "] is not supported. Error: %o", ex);
                    }
                }
            }
        }

        lib.collect = function(webTrendTag) {
            this["wt"] = webTrendTag;
            try {
                if (typeof (cspConsumerInfo) != "undefined" && cspConsumerInfo.companyname.match(/^test_/i)) {
                        return;
                }
                //track for mailing
                var url = document.location.search;
                var emailId = this.getQueryValue(url, "csp_emailid") || "";
                if (emailId != "") {
                    var emailLinkName = this.getQueryValue(url, "csp_emaillinkname");
                    var emailClick = this.getQueryValue(url, "csp_emailclick");
                    var vendorName = this.getQueryValue(url, "csp_vendorname");
                    var vendorId = this.getQueryValue(url, "csp_vendorid");
                    var vendorEmailClick = this.getQueryValue(url, "csp_vendoremailclick");
                    var emailScenarioStep = this.getQueryValue(url, "csp_scenariostep");

                    if (emailLinkName != "") this["wt"].DCSext["csp_emaillinkname"] = emailLinkName;
                    if (emailClick != "") this["wt"].DCSext["csp_emailclick"] = emailClick;
                    if (vendorName != "") this["wt"].DCSext["csp_vendorname"] = vendorName;
                    if (vendorId != "") this["wt"].DCSext["csp_vendorid"] = vendorId;
                    if (vendorEmailClick != "") this["wt"].DCSext["csp_vendoremailclick"] = vendorEmailClick;
                    //this["wt"].DCSext["emailId"] = emailId; //from msft
                    this["wt"].DCSext["csp_emailId"] = emailId; // from siemens
                }
            }
            catch (e) { }


            var elements = document.getElementsByCspObj("REPORT");
            for (var i = 0; i < elements.length; i++) {
                try {
                    ReportTypes[elements[i].type.toLowerCase()](elements[i], webTrendTag);
                    this["elements"].push(elements[i]);
                }
                catch (ex) {
                    this.log("Error: Type [" + elements[i].type + "] is not supported. Error: %o", ex);
                }
            }
            ReportTypes.common(webTrendTag);
            ReportTypes.specific(webTrendTag);
            webTrendTag.dcsCollect();
            ReportTypes.sendMoreWtIfNeeded(webTrendTag);
            ReportTypes.clear(webTrendTag);
        };
    })(CspReportLib);
}
if (typeof (IWillCallCSPReportingWhenImDone) == "undefined" || (typeof (IWillCallCSPReportingWhenImDone) != "undefined" && !IWillCallCSPReportingWhenImDone)) {
    CspReportLib.collect(_tag);
}
CspReportLib.executeReplacement();