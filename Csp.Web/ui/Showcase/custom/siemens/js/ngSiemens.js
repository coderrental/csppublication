var tickValue = '';
var wdWidth = jQuery(window).width();
var wdHeight = 500;

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function UpdateUserCookies() {
	jQuery('#frmleadgen .tieValidate').each(function() {
		setCookie(jQuery(this).attr('id'), jQuery(this).val());
	});
}

function GetUserInfo() {
	return { firstname: getCookie('firstname'), lastname: getCookie('lastname'), companyname: getCookie('companyname'), employees: getCookie('employees'), email: getCookie('email') };
}

function AutoPopulateUserInfo() {
	if (jQuery('#frmleadgen').length == 0 && jQuery('#contactus-page').length == 0) return;
	var userInfo = GetUserInfo();
	jQuery('#firstname').val(userInfo.firstname);
	jQuery('#lastname').val(userInfo.lastname);
	jQuery('#companyname').val(userInfo.companyname);
	jQuery('#employees').val(userInfo.employees);
	jQuery('#email').val(userInfo.email);
}

function reAdjustColorBoxSize(minWidth) {
	wdWidth = jQuery(window).width();
	wdHeight = 500;
	if (wdWidth > 660)
		wdWidth = 600;
	else {
		wdWidth = wdWidth - 150;
		wdHeight = wdWidth;
	}
	if (minWidth > 0 && wdWidth < minWidth) {
		wdWidth = minWidth;
		wdHeight = wdWidth;
	}
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return false;
	}
	return true;
}

String.prototype.trunc = function(n,useWordBoundary){
	if (this.length == 0) return '';
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + '...' : s_;
};

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
		// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
		// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
    return query_string;
} ();

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function ValidURL(str) {
    return /^(http:\/\/|https:\/\/|ftp:\/\/){0,1}(www\.){0,1}([0-9A-Za-z]+\.)/.test(str);
}

function addTick(linkEl) {
	if (tickValue == '' || linkEl.attr('href') == undefined || linkEl.attr('href').indexOf('i/' + tickValue) != -1) return;
	if (linkEl.parents('.nav-tabs').length > 0 || linkEl.parents('.carousel').length > 0) return true;
	
	var suffix = '/';
	var newHref = linkEl.attr('href');
	
	if (ValidURL(newHref) && newHref.indexOf(document.domain) == -1) {
			return;
	}

	if (!endsWith(newHref, suffix))
			newHref += suffix;
			
	newHref += 'i/' + tickValue;
	linkEl.attr('href', newHref);
}

function convertCSPUniqueKey(cspKey) {
	var temp = cspKey.split(' ');
	var convertedKey = temp.join('_');
	return convertedKey;
}

var siemensModule = angular.module('ngSiemens', ['ngSanitize']).config(function($routeProvider, $locationProvider) {
	$routeProvider.
		when('/', {controller:MainCtrl, templateUrl:'ui/showcase/Custom/Siemens/main.html'}).
		when('/i/:tick', {controller:MainCtrl, templateUrl:'ui/showcase/Custom/Siemens/main.html'}).
		when('/companyid/:coid/messageid/:msgid', {controller:MainCtrl, templateUrl:'ui/showcase/Custom/Siemens/main.html'}).
		when('/messageid/:msgid/cspid/:coid/i/:tick', {controller:MainCtrl, templateUrl:'ui/showcase/Custom/Siemens/main.html'}).
		when('/products', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/:id', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/:id/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/parentlist/:plist/prodid/:id', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/parentlist/:plist/prodid/:id/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/parentlist/:plist/prodid/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/products/parentlist/:plist/prodid/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/insights/:assettype/:id', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/insights/:assettype/:id/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/insights/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/insights/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/insights/:assettype', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/insights/:assettype/i/:tick', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/insights/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/insights/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/casestudy/:assettitle/:id', {controller:AssetDetailCtrl, templateUrl:'ui/showcase/Custom/Siemens/assetdetail.html'}).
		when('/casestudy/:assettitle/:id/i/:tick', {controller:AssetDetailCtrl, templateUrl:'ui/showcase/Custom/Siemens/assetdetail.html'}).
		when('/casestudy/:assettitle/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:AssetDetailCtrl, templateUrl:'ui/showcase/Custom/Siemens/assetdetail.html'}).
		when('/casestudy/:assettitle/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:AssetDetailCtrl, templateUrl:'ui/showcase/Custom/Siemens/assetdetail.html'}).
		when('/newwaystowork/:assettype/:id', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/newwaystowork/:assettype/:id/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/newwaystowork/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/newwaystowork/:assettype/:id/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ProductsCtrl, templateUrl:'ui/showcase/Custom/Siemens/products.html'}).
		when('/newwaystowork/:assettype', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/newwaystowork/:assettype/i/:tick', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/newwaystowork/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/newwaystowork/:assettype/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:AssetCtrl, templateUrl:'ui/showcase/Custom/Siemens/asset.html'}).
		when('/contactus', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/:info', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/title/:titlevalue', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/title/:titlevalue/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/title/:titlevalue/messageid/:msgid/cspid/:coid/socialid/:scid', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/contactus/title/:titlevalue/messageid/:msgid/cspid/:coid/socialid/:scid/i/:tick', {controller:ContactUsCtrl, templateUrl:'ui/showcase/Custom/Siemens/contactus.html'}).
		when('/comingsoon', {controller:ComingSoonCtrl, templateUrl:'ui/showcase/Custom/Siemens/comingsoon.html'}).
		//otherwise({ redirectTo: '/' });
		otherwise({ redirectTo: function(obj, path) {
			try {
				var i = path.indexOf("csp_page:");
				if (i>=0)
					return path.substring(i+9);
				
				//Contact Us page with dynamic params
				if (path.indexOf('contactus[]') != -1)
					return path.replace('[]&', '/');
			} catch(e) {}
			return '/';
		} });

		//$locationProvider.html5Mode(true);
});

siemensModule.factory('URLHandler', function ($routeParams) {
	return {
		setTick: function (timeTick) {
			$routeParams.tick = timeTick;
		},
		getTick: function () {
			if (QueryString.ticks != undefined) {
				$routeParams.tick = QueryString.ticks;
				return QueryString.ticks;
			}
			return typeof($routeParams.tick)=="undefined"?'':$routeParams.tick;
		},
		getUrlTick: function () {
			return typeof($routeParams.tick)=="undefined"?'':'&i=' + $routeParams.tick;
		}
	}
});

siemensModule.factory('TranslationData', function ($http) {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		}
	}
});

siemensModule.factory('Translation', function ($rootScope) {
	var data = [];
	var translatedWord = '';
	return {
		promise: {},
		all: function () {
			return data;
		},
		setData: function (dataList) {
			data = dataList;
		},
		parse: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 50;
			var ctId = '35000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["CSP_Context_Id", "Content_Title", "Content_Description_Long"];
			var fieldDic = {
				"CSP_Context_Id"			: "contextid",
				"Content_Title"    			: "value",
				"Content_Description_Long"	: "descriptionlong"
			}
			data = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return data;
		},
		translate: function (dataList, inputWord) {
			for (var i = 0; i < dataList.length; i++) {
				if (dataList[i].contextid == inputWord) {
					inputWord = dataList[i].value;
					break;
				}
			}
			return inputWord;
		}
	}
});

siemensModule.factory('TabsAndNavigation', function ($rootScope, $http) {
	var data = [];
	return {
		promise: {},
		all: function () {
			return data;
		},
		parse: function (rawList) {
			var fieldSplit = 51;
			var ctId = '34000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["CSP_Unique_Key", "Content_Title", "Content_Item_Link", "Status_Navigation_ID", "Status_Level", "Status_Sort_Order"];
			var fieldDic = {
				"CSP_Unique_Key"		: "identifier",
				"Content_Title"			: "title",
				"Content_Item_Link"		: "link",
				"Status_Navigation_ID"	: "parent",
				"Status_Level"			: "level",
				"Status_Sort_Order"		: "displayorder"
			}
			var tabNavList = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return tabNavList;
		},
		setData: function (tnList) {
			data = tnList;
		}
	}
});

siemensModule.factory('Products', function ($rootScope, $http) {
	var rootId = 1;
	var categoryProductId = 2;
	var data = [];
	return {
		promise: {},
		rootid: function () {
			return rootId;
		},
		mainproductid: function () {
			return categoryProductId;
		},
		all: function () {
			return data;
		},
		setData: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 117;
			var ctId = '31000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ['CSP_Unique_Key', 'Content_Title', 'Content_Title_Secondary', 'File_Image_Thumbnail_Url', 'File_Image_Url', 'Content_Description_Short', 'Content_Description_Long', 'Local_Type'
			, 'Local_Syndication_Type', 'Status_Sort_Order', 'Status_Promotion_Y_N', 'ContentCategoryID', 'ContentCategoryParentID', 'ContentCategoryLineage', 'ContentCategoryDepth'
			, 'File_Presentation_1_Name', 'CSP_Reporting_Id', 'CSP_Level','Content_Description_Extended', 'Content_Information_Detail', 'Local_Target_Group', 'File_Video_Url'];
			var fieldDic = {
				"CSP_Unique_Key"			: "cspuniquekey",		
				"Content_Title"				: "title",	
				"Content_Title_Secondary"	: "secondarytitle",				
				"File_Image_Thumbnail_Url"	: "thumbnail",				
				"File_Image_Url"			: "image",
				"File_Video_Url"			: "filevideourl",
				"Content_Description_Short"	: "description",				
				"Content_Description_Long"	: "descriptionlong",				
				"Local_Type"				: "type",	
				"Local_Syndication_Type"	: "syndicationtype",				
				"Status_Sort_Order"			: "displayorder",		
				"Status_Promotion_Y_N"		: "feature",			
				"ContentCategoryID"			: "categoryId",		
				"ContentCategoryParentID"	: "parentId",				
				"ContentCategoryLineage"	: "lineage",				
				"ContentCategoryDepth"		: "depth",			
				"File_Presentation_1_Name"	: "textorientation",				
				"CSP_Reporting_Id"			: "reportingId",		
				"CSP_Level"					: "cspLevel",
				"Content_Information_Detail": "Content_Information_Detail",
				"Content_Description_Extended" : "Content_Description_Extended",
				"Local_Target_Group"			: "availablecountry"
			}
			data = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
		},
		convertJSONtoTree: function (arry) {
			var rootNode = [
				{
					"title" : "Root",
					"categoryId" : rootId,
					"parentId" : ""
				}
			]
			var fullArray = rootNode.concat(arry);
			//var fullArray = arry;
			var roots = [], children = {};

			// find the top level nodes and hash the children based on parent
			for (var i = 0, len = fullArray.length; i < len; ++i) {
				var item = fullArray[i],
					p = item.parentId,
					target = !p ? roots : (children[p] || (children[p] = []));

				target.push({ value: item });
			}

			// function to recursively build the tree
			var findChildren = function(parent) {
				if (children[parent.value.categoryId]) {
					parent.children = children[parent.value.categoryId];
					for (var i = 0, len = parent.children.length; i < len; ++i) {
						findChildren(parent.children[i]);
					}
				}
			};

			// enumerate through to handle the case where there are multiple roots
			for (var i = 0, len = roots.length; i < len; ++i) {
				findChildren(roots[i]);
			}
			//console.log(roots[0].children);
			return roots[0].children;
		}
	}
});

siemensModule.factory('LeadgenForm', function ($rootScope, $http) {
	var fieldsList = '';
	var formHeaderTitle = '';
	var formHeaderDescriptionShort = '';
	var downloadFields = '';
	var downloadFormHeader = '';
	var downloadFormDescription = '';
	var supplierInfo = [];
	return {
		promise: {},
		parseLeadgenItem: function (rawList) {
			if (rawList.length == 0) return new Array();
			var fieldSplit = 45;
			var ctId = '15000';
			fieldSplit = $rootScope.GetFieldSplitNo(ctId, rawList);
			var fieldList = ["Status_Form_Contact_Us", "Status_FormField_Id", "Content_FormFieldLabel", "Status_Formfield_Name", "Content_Title"    , "Content_Description_Short", "Status_Formfield_Type", "Content_RequiredField_ErrorMessage", "Status_Formfield_Validation_Type", "Status_Formfield_Validation_Y_N", "Status_Form_View_Asset", "Status_Form_Trial"];
			var fieldDic = {
				"Status_Form_Contact_Us"			: "statuscontactus",
				"Status_FormField_Id"				: "fieldid",
				"Content_FormFieldLabel"			: "fieldlabel",
				"Status_Formfield_Name"				: "fieldname",
				"Content_Title"					    : "contenttitle",
				"Content_Description_Short"			: "description",
				"Status_Formfield_Type"				: "type",
				"Content_RequiredField_ErrorMessage": "fielderrormessage",			
				"Status_Formfield_Validation_Type"	: "fieldvalidationtype",
				"Status_Formfield_Validation_Y_N"	: "statusvalidation",
				"Status_Form_View_Asset"			: "statusformviewasset",
				"Status_Form_Trial"					: "statusformtrialrequest"
			}
			var jsonLeadgenList = $rootScope.ParseObject(rawList, fieldSplit, ctId, fieldList, fieldDic);
			return jsonLeadgenList;
		},
		setSupplierInfo: function (supplierinfo) {
			this.supplierInfo = supplierinfo;
		},
		setFields: function (fieldData, topicValue) {
			this.fieldsList = '<input value="" type="hidden" name="redirect" id="redirect" />';
			this.fieldsList += '<input value="' + this.supplierInfo.consumerid + '" type="hidden" name="sId" />';
			this.fieldsList += '<input value="' + this.supplierInfo.companyname + '" type="hidden" name="sName" />';
			this.fieldsList += '<input value="Simens" type="hidden" name="from_name" />';
			this.fieldsList += '<input value="ContactUs" type="hidden" name="subject" />';
			this.fieldsList += '<input value="noreply@tiekinetix.com" type="hidden" name="from_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.emailaddress + '" type="hidden" name="to_email" />';
			this.fieldsList += '<input value="' + this.supplierInfo.title + '" name="Microsite_Title" type="hidden" />';
			this.fieldsList += '<div class="tieContentFormHeader">';
			this.fieldsList += '<div class="tieContentFormHeaderTitle">{{formHeaderTitle}} {{formHeaderTitleCompanyName}}';
			this.fieldsList += '<span cspObj="REPORT" cspType="TITLE" style="display:none">{{formHeaderTitle}}</span>' ;
			this.fieldsList += '</div>';
			this.fieldsList += '<div class="tieDivClear"></div>';
			this.fieldsList += '<div class="tieContentShortDescription">{{formHeaderDescriptionShort}}</div>';
			this.fieldsList += '</div>';
			this.downloadFields = this.fieldsList;
			this.formHeaderTitle = '';
			this.formHeaderDescriptionShort = '';
			var formFields = fieldData.slice(0);
			var emailCheckbox = '';
			var submitBtn = '';
			
			//formFields.pop();
			//var downloadFieldIdList = ["firstname", "lastname", "companyname", "email"];
			for (var i = 0; i < formFields.length; i++) {
				if (formFields[i].statuscontactus.toLowerCase() == 'yes' || formFields[i].statusformviewasset.toLowerCase() == 'yes') {
					if (formFields[i].statuscontactus.toLowerCase() == 'yes' && formFields[i].fieldid == 'formheader') {
						this.formHeaderTitle = formFields[i].contenttitle;
						this.formHeaderDescriptionShort = formFields[i].description;
						continue;
					}
					if (formFields[i].statusformviewasset.toLowerCase() == 'yes' && formFields[i].fieldid == 'authorizationformheader') {
						this.downloadFormHeader = formFields[i].contenttitle;
						this.downloadFormDescription = formFields[i].description;
						continue;
					}
					if (formFields[i].type.toLowerCase() == 'field' || formFields[i].type.toLowerCase() == 'textarea') {
						var fieldLabel = '<div class="tieContactUsFormFieldLabel"><label for="' + formFields[i].fieldid + '">' + formFields[i].fieldlabel + '</label></div>';
						var tieValidate = 'tieValidate';
						if (formFields[i].statusvalidation.toLowerCase() != 'yes')
							tieValidate = '';
						if (formFields[i].type.toLowerCase() == 'field') {
							var fieldValue = '';
							if (formFields[i].fieldid == 'topic' && topicValue != undefined)
								fieldValue = topicValue;
							var inputField = fieldLabel + '<div class="tieContactUsFormField"><input type="text" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" value="' + fieldValue + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' csp-form-field form-control" maxlength="50"/></div>';
							
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += inputField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += inputField;
						}
						else if (formFields[i].type.toLowerCase() == 'textarea') {
							var textAreaField = fieldLabel + '<div class="tieContactUsFormField"><textarea rows="8" cols="40" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" valType="' + formFields[i].fieldvalidationtype + '" class="' + tieValidate + ' form-control"/></textarea></div>';
							if (formFields[i].statuscontactus.toLowerCase() == 'yes')
								this.fieldsList += textAreaField;
							if (formFields[i].statusformviewasset.toLowerCase() == 'yes')
								this.downloadFields += textAreaField;
						}
					}
					else {
						if (formFields[i].fieldid == 'addtomailing') {
							emailCheckbox += '<div class="tieContactUsFormFieldCheckboxBlock">';
							emailCheckbox += '<input type="checkbox" id="' + formFields[i].fieldid + '" name="' + formFields[i].fieldname + '" error="' + formFields[i].fielderrormessage + '" value="yes" />';
							emailCheckbox += '<label class="checkbox-label" for="' + formFields[i].fieldid + '">' + formFields[i].description + '</label>';
							emailCheckbox += '</div>';
						}
						else if (formFields[i].type == 'submit') {
							submitBtn += '<div class="tieContactUsForm"><div class="tieContactUsFormSubmit">';
							submitBtn += '<input ng-click="submit()" name="submit_button" class="btn btn-primary" id="' + formFields[i].fieldid + '" value="' + formFields[i].fieldlabel + '" cspenglishvalue="ContactUs" csptype="LEADGEN" cspobj="REPORT" type="submit" />';
							submitBtn += '</div></div>';
						}
					}
				}
			}
			this.fieldsList += emailCheckbox;
			this.fieldsList += submitBtn;
			this.downloadFields += emailCheckbox;
			this.downloadFields += submitBtn;
			
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitle}}/g, this.formHeaderTitle);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderTitleCompanyName}}/g, this.supplierInfo.companyname);
			this.fieldsList = this.fieldsList.replace(/{{formHeaderDescriptionShort}}/g, this.formHeaderDescriptionShort);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitle}}/g, this.downloadFormHeader);
			this.downloadFields = this.downloadFields.replace(/{{formHeaderTitleCompanyName}}/g, '');
			this.downloadFields = this.downloadFields.replace(/{{formHeaderDescriptionShort}}/g, this.downloadFormDescription);
		},
		getAllFields: function () {
			return this.fieldsList;
		},
		getDownloadFormFields: function () {
			return this.downloadFields;
		},
		getSupplierInfo: function () {
			return this.supplierInfo;
		},
		getFormTitle: function () {
			return this.formHeaderTitle;
	
		},
		getFormDesc: function () {
			return this.formHeaderDescriptionShort;
		},
		validateForm: function (form) {
			var errorMessage = "";
			form.find(".tieValidate").each(function() {
				var vErrorMsg = jQuery(this).attr("error");
				var vValType = jQuery(this).attr("valType");
				var vValue = jQuery(this).val() || jQuery(this).text();

				if (vValType == 'mandatoryField') {
					// if mandatory
					if (vValue.length > 1) {
						// do nothing
					} else
						errorMessage += vErrorMsg + "\n";

				}

				if (vValType == 'mandatoryNumber') {
					// if mandatory
					if (vValue.length > 0) {
						// do nothing
						if (isNaN(vValue))
							{errorMessage += vErrorMsg + "\n";}
						else
						   { // do nothing
								}
					} else
						errorMessage += vErrorMsg + "\n";
				}			
				
				if (vValType == 'mandatoryEmail') {
					var emailpattern = /.+@.+\./;
					if (emailpattern.test(vValue)) {
						//do nothing
					} else
						errorMessage += vErrorMsg + "\n";
				}

			});
			if (errorMessage == "") {
				if (jQuery('#redirect').length > 0 && jQuery('#redirect').val() != '')
					window.open(jQuery('#redirect').val());
				return true;
			} else {
				alert(errorMessage);
				return false;
			}
		},
		submit: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2007(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)[]&tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		},
		submitLeadgen: function(formId, callBack) {
			UpdateUserCookies();
			var form = jQuery('#' + formId);
			var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);
			var submitMsg = 0;
			if (this.validateForm(form)) {
				submitMsg = 1;
				form.find('#submitbutton').disabled = true;
				$http({
					method: 'POST',
					url: '/saveform?tick=' + tick,
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
				
				$http({
					method: 'POST',
					url: 'd1.aspx?p2010(ct15000&fStatus_FormField_Id~1=thankyoumessaging!)',
					data: form.serialize(),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					try {
						callBack(data);
						jQuery('#' + formId).each (function(){
							this.reset();
						});
					} catch(err) {
					
					}
				});
			}
			return submitMsg;
		}
	}
});

siemensModule.directive('eatClick', function() {
	return function(scope, element, attrs) {
		jQuery(element).click(function(event) {
			event.preventDefault();
		});
	}
});

siemensModule.run(function ($rootScope, TabsAndNavigation, Translation, Products, LeadgenForm, $q, $http) {
	var currentUrl = document.URL;
	var currentTick = '';
	if (QueryString.ticks != undefined)
		currentTick = '&i=' + QueryString.ticks;
	else if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
					if (temp[i] == 'i') {
							currentTick = '&i=' + temp[i + 1];
							break;
					}
			}
	}
	
	$rootScope.pageTitle = 'Home';
	$rootScope.AllProducts = [];
	$rootScope.AllTabsAndNavigation = [];
	$rootScope.Dictionary = [];
	$rootScope.rootProductId = Products.mainproductid();
	$rootScope.CtrlMessage = '';
	$rootScope.baseDomain = (document.domain.indexOf('http') == -1) ? 'http://' + document.domain : document.domain;
	$rootScope.Supplier = SupplierInfo;
	
	if (typeof($rootScope.Supplier.telephone) == 'undefined')
		$rootScope.Supplier.telephone = '';
	if (typeof($rootScope.Supplier.twitter) == 'undefined')
		$rootScope.Supplier.twitter = '';
	if ($rootScope.Supplier.twitter != '' && $rootScope.Supplier.twitter.indexOf('http') == -1)
		$rootScope.Supplier.twitter = 'http://' + $rootScope.Supplier.twitter;
	
	$rootScope.getTextToCopy = function() {
        return "";
    }
    $rootScope.doSomething = function () {
        //console.log("NgClip...");
    }
	
	$rootScope.validateCspKey = function (cspKey) {
		if (cspKey.indexOf('=') == -1) return cspKey;
		var tmpKey = cspKey.split('=')[0];
		tmpKey = tmpKey.split('&')[0];
		if (typeof(tmpKey) == 'undefined') return cspKey;
		return tmpKey.replace(/[\[\]]/g, '');
	};
	
	$rootScope.GetFieldSplitNo = function (ctId, fieldList) {
		var firstFieldName = '';
		if (fieldList.length == 0)
			return 0;
			
		for (var firstProp in fieldList[0][ctId][0])
			firstFieldName = firstProp;
		
		for (var i = 1; i < fieldList[0][ctId].length; i++) {
			for (var property in fieldList[0][ctId][i]) {
				if (property == firstFieldName) {
					return i;
				}
			}
		}
		
		return fieldList[0][ctId].length - 1;
	}
	
	$rootScope.ParseObject = function (rawList, fieldSplit, ctId, fieldList, fieldDic) {
		var objectList = "[";
		
		for (var i = 0; i < rawList[0][ctId].length; i += fieldSplit) {
			var objectItem = "{";
			for (var j = i; j < i + fieldSplit; j++) {
				for (property in rawList[0][ctId][j]) {
					property = property.trim();
					if (fieldList.indexOf(property) >= 0) {
						if (objectItem != '{') objectItem += ',';
						var fieldValue = rawList[0][ctId][j][property].replace(/\"/g, '\\\"');
						objectItem += '"' +  fieldDic[property] + '":"' + fieldValue + '"';
					}
				}
			}
			objectItem += "}";
			if (objectList != '[') objectList += ',';
			objectList += objectItem;
		}
		objectList += ']';
		return JSON.parse(objectList);
	}
	
	if (true) {
		var deferred = $q.defer();
		Products.promise = deferred.promise.then(function (productList) {
			Products.setData(productList);
			return Products.all();
		});
		
		//$http.get('d1.aspx?p2003(ct31000&cd' + $rootScope.rootProductId + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
		
		var translationDeferred = $q.defer();
		Translation.promise = translationDeferred.promise.then(function (dataList) {
			var translationList = Translation.parse(dataList);
			return translationList;
		});
		
		$http.get('d2.aspx?p2005(ct35000)[]' + currentTick).success(function(responseData) {
			translationDeferred.resolve(responseData);
		});
		
		var leadgenFormDeferred = $q.defer();
		LeadgenForm.promise = leadgenFormDeferred.promise.then(function (data) {
			var parsedData = LeadgenForm.parseLeadgenItem(data);
			//parsedData = JSON.parse(parsedData);
			
			LeadgenForm.setSupplierInfo(SupplierInfo);
			LeadgenForm.setFields(parsedData);
			
			return parsedData;
		});		
		
		var TabsNavDeferred = $q.defer();
		TabsAndNavigation.promise = TabsNavDeferred.promise.then(function (data) {
			//if (data.length <= 1) return null;
			var tabnavList = TabsAndNavigation.parse(data);
			return tabnavList;
		});
		
		$http.get('d2.aspx?p2001(ct34000)[]' + currentTick).success(function(responseData) {
			TabsNavDeferred.resolve(responseData);
		});

		// ltu 9/16/2014 move to the end so it loads last
		$http.get('d2.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			leadgenFormDeferred.resolve(responseData);
		});
		
		$http.get('d2.aspx?p2003(ct31000&cd' + Products.rootid() + 'i99)[st(ct31000*Status_Sort_Order*)]' + currentTick).success(function(responseData) {
			deferred.resolve(responseData);
		});
	}
	
	$rootScope.updatePageInfo = function (theCtrl, value) {
		if (theCtrl == null) {
			$rootScope.pageTitle = value;
		}
		else {
			switch (theCtrl.constructor.name) {
				case 'MainCtrl':
					$rootScope.pageTitle = 'Home';
					break;
				case 'ProductsCtrl':
					$rootScope.pageTitle = 'Product';
					break;
				case 'AssetCtrl':
					$rootScope.pageTitle = 'Asset';
					break;
				case 'AssetDetailCtrl':
					$rootScope.pageTitle = 'Asset Detail';
					break;
				case 'ContactUsCtrl':
					$rootScope.pageTitle = 'Contact Us';
					break;
				default:
					$rootScope.pageTitle = 'Siemens';
			}
		}

		try {
			//handle deep links
			if (window.location.hash&&window.location.hash.indexOf('#/#')==0) {						
				try {
					var url = decodeURIComponent(window.location.hash.substring(2));
					window.location = url;
				} catch(e) {}
			}
			//if (currentSyndicationLng.toLowerCase()=="zh") window.location="/#/comingsoon";
			jQuery('#csp-report-lib').remove();
			window.setTimeout(function() {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "csp-report-lib";
				script.src = "js/CspReportLib.js";
				// clear CspReportLib 
				if (typeof (CspReportLib) != "undefined") {
					CspReportLib.wt.DCSext["ConversionContent"] = null;
					CspReportLib.wt.DCSext["ConversionShown"] = null;
					CspReportLib.wt.DCSext["ConversionClick"] = null;
					CspReportLib.wt.DCSext["ConversionType"] = null;
					CspReportLib.wt.DCSext["csp_vname"] = null;
					CspReportLib.wt.DCSext["csp_vaction"] = null;
				}
				jQuery(document).attr('title', $rootScope.pageTitle);
				document.getElementsByTagName('head')[0].appendChild(script);
			}, 1000);
			try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);};
		} catch (Err) {
		
		}
	};
});


// added 10/21/2013 handle coming soon section for chinese
function ComingSoonCtrl($rootScope, $scope, $http, URLHandler) {
}

function MainCtrl($rootScope, $scope, $http, $filter, URLHandler, Products) {
	tickValue = URLHandler.getTick();
	$rootScope.updatePageInfo(this);
	var blockData = [];
	var ctId = '37000';
	var fieldSplit = 0;
	var fieldList = ["Content_Title", "File_Image_Thumbnail_Url", "File_Image_Url", "Content_Description_Short", "Content_Description_Long", "Local_Type", "File_Presentation_1_Url", "ContentCategoryID", "CSP_Reporting_Id", "Status_Sort_Order"];
	var fieldDic = {
		"Content_Title"				: "title",
		"File_Image_Thumbnail_Url"	: "thumbnail",
		"File_Image_Url"			: "image",
		"Content_Description_Short"	: "description",
		"Content_Description_Long"	: "descriptionlong",
		"Local_Type"				: "type",
		"File_Presentation_1_Url"	: "link",
		"ContentCategoryID"			: "cspcategoryid",
		"CSP_Reporting_Id"			: "reportingId",
		"Status_Sort_Order"			: "displayorder"
	};
	
	$scope.GetLinkTarget = function (itemLink) {
		if ($scope.IsExternalLink(itemLink))
			return '_blank';
		return '_self';
	};
	$scope.IsExternalLink = function (theLink) {
		if (theLink.indexOf('#') == 0) return false;
		var comp = new RegExp(location.host);
		return !comp.test(theLink);
	};
	
	var GetTileBlockList = function (categoryBlocks, defaultBlocks) {
		if (categoryBlocks.length == 0) return defaultBlocks;
		
		categoryBlocks.sort(function(x, y){
			var sortOrder_1 = parseInt(x.displayorder);
			var sortOrder_2 = parseInt(y.displayorder);
			if (x < y) {
				return -1;
			}
			if (x > y) {
				return 1;
			}
			return 0;
		});
		
		for (var i = 0; i < categoryBlocks.length; i++)
			categoryBlocks[i].displayorder = parseInt(categoryBlocks[i].displayorder);
		
		var nextOrder = categoryBlocks[categoryBlocks.length - 1].displayorder + 1;
		for (var j = 0; j < defaultBlocks.length; j++)
			defaultBlocks[j].displayorder = nextOrder + j;
		
		return categoryBlocks.concat(defaultBlocks);
	}
	
	$http.get('d2.aspx?p2002(ct37000&cd1i99)[st(ct37000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
		fieldSplit = $rootScope.GetFieldSplitNo(ctId, data);
		data = $rootScope.ParseObject(data, fieldSplit, ctId, fieldList, fieldDic);
		$scope.homeblocks = data;
		var tileBlocks = $filter('filter')(data, {type: 'tile'}, function (expected, actual) {return expected == actual;});
		var categoryBlocks = $filter('filter')(tileBlocks, {cspcategoryid: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) != parseInt(actual);});
		var defaultBlocks = $filter('filter')(tileBlocks, {cspcategoryid: Products.mainproductid()}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		
		$scope.tileBlocks = GetTileBlockList(categoryBlocks, defaultBlocks);
		if (tileBlocks.length > 3)
			$scope.tileBlocks = tileBlocks.slice(0, 3);
			
		setTimeout(function() {
			jQuery(document).click();							
		}, 1000);				
	});
}

function ContactUsCtrl($rootScope, $scope, $routeParams, $timeout, $http, $location, LeadgenForm, URLHandler) {
	tickValue = URLHandler.getTick();
	$scope.topicTitle = typeof($routeParams.titlevalue)=="undefined"?'':$routeParams.titlevalue;
	$scope.ajaxLoading = false;
	$rootScope.updatePageInfo(this);
	$scope.supplier = [];
	$scope.fieldList = '';
	$scope.formSubmitted = false;
	//$timeout(function() { console.log('changed'); }, 3000);
	$scope.submitted = function (resultMsg) {
		$scope.formSubmitted = true;
		$scope.ajaxLoading = false;
		$timeout(function() { $scope.formSubmitted = false; }, 5000);
		$scope.contactusMessage = resultMsg;
	};

	$scope.submit = function() {
		if (LeadgenForm.submit('form1', $scope.submitted) == 1)
			$scope.ajaxLoading = true;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		LeadgenForm.setSupplierInfo(SupplierInfo);
		LeadgenForm.setFields(resultData, $scope.topicTitle);
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getAllFields();
		setTimeout(function() {
			AutoPopulateUserInfo();
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
}

function AssetCtrl($rootScope, $scope, $http, $routeParams, $filter, Translation, LeadgenForm, URLHandler, TabsAndNavigation) {
	tickValue = URLHandler.getTick();
	var labelCaseStudy = 'casestudy';
	$scope.assetType = typeof($routeParams.assettype)=="undefined"?'':$routeParams.assettype;
	$scope.ajaxLoading = true;
	$scope.assetLongDescription = '';
	
	//Title
	$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
		data.pop();
		for (var i = 0; i < data.length; i++) {
			if (data[i].identifier == $scope.assetType) {
				$scope.assetTitle = data[i].title;
				$rootScope.updatePageInfo(null, data[i].title);
				break;
			}
		}
	});
	
	//Breadcrumb
	var getBreadcrumbItem = function (dataList, currentItem, crrBreadcrumbs) {
		var nextItem;
		for (var i = 0; i < dataList.length; i++) {
			if (currentItem.parent == '') {
				if (dataList[i].identifier.toLowerCase() == 'home') {
					crrBreadcrumbs.push(dataList[i]);
					break;
				}
			}
			else if (dataList[i].identifier == currentItem.parent) {
				crrBreadcrumbs.push(dataList[i]);
				nextItem = dataList[i];
				break;
			}
		}
		if (nextItem != undefined)
			getBreadcrumbItem(dataList, nextItem, crrBreadcrumbs);
	};
	
	TabsAndNavigation.promise.then(function (data) {
		var defaultBreadcrumb = [];
		var crrAsset;
		for (var i = 0; i < data.length; i++) {
			if (data[i].identifier == $scope.assetType) {
				crrAsset = data[i];
				break;
			}
		}
		//defaultBreadcrumb.push(crrAsset);
		getBreadcrumbItem(data, crrAsset, defaultBreadcrumb);
		defaultBreadcrumb.reverse();
		$scope.breadcrumbs = defaultBreadcrumb;
	});
	
	//Asset description
	Translation.promise.then(function (resultData) {
		var textItem = $filter('filter')(resultData, {contextid: $scope.assetType}, function (expected, actual) {return expected == actual;});
		if (textItem.length > 0)
			$scope.assetLongDescription = textItem.descriptionlong;
	});
	
	var assetData = [];
	var featuredData = [];
	var localVertical = new Array();
	$scope.localVerticalName = [];
	var assetLocalTypes = []
	var currentItem;
	$scope.featuredRows = [];
	$scope.featuredAssets = [];
	
	switch ($scope.assetType) {
		case 'assets':
			assetLocalTypes = ['tools', 'brochure', 'datasheet', 'video', 'podcast'];
			break;
		default:
			assetLocalTypes = [$scope.assetType];
	}
	
	//Get assets
	$http.get('d1.aspx?p2004(ct21000)[st(ct21000*CSP_DeDup_Key,Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
		data.pop();
		if (data.length == 0) return;
		data = $filter('filter')(data, {type: $scope.assetType}, function (expected, actual) { 
			if ($scope.assetType == labelCaseStudy)
				return expected.toLowerCase() == labelCaseStudy; 
			return expected.toLowerCase() != labelCaseStudy;
		});		
		
		for (var j = 0; j < data.length; j++) {
			if (assetLocalTypes.length == 1 && assetLocalTypes[0] == labelCaseStudy) {
				var tempTitleArray = data[j].title.toLowerCase().split(" ");
				var urlTitle = encodeURIComponent(tempTitleArray.join('-'));
				//data[j].linkurl = '#/casestudy/' + urlTitle + '/' + data[j].contentid;
				data[j].linkurl = '#/casestudy/' + urlTitle + '/' + data[j].cspuniquekey;
				data[j].launchtype = 'currentwindow';
			}
			switch (data[j].launchtype) {
				case 'newwindow': 
					data[j].target = "_blank";
					break;
				case 'lightbox':
					data[j].target = "_blank";
					break;
				default:
					data[j].target = "_self";
			}
			data[j].launchtype = 'csp-' + data[j].launchtype;
			
			data[j].leadgen = '';
			if (data[j].statusleadgen.toLowerCase() == 'yes') {
				data[j].leadgen = 'call-leadgen';
			}
				
			var blockType = data[j].localvertical;
			if (assetLocalTypes.length > 1)
				blockType = data[j].type;
			if (localVertical[blockType] == undefined) {
				localVertical[blockType] = [];
				$scope.localVerticalName.push(blockType);
			}
			if (data[j].featured.toLowerCase() == "yes")
				featuredData.push(data[j]);
			else {
				assetData.push(data[j]);
				localVertical[blockType].push(data[j]);
			}
		}

		featuredData.sort(function(a, b) {
		return (a.sortorder<b.sortorder)?-1:((a.sortorder>b.sortorder)?1:0);
		});

		
		for (var k = $scope.localVerticalName.length - 1; k >= 0; k--) {
			if (localVertical[$scope.localVerticalName[k]].length == 0)
				$scope.localVerticalName.splice(k, 1);
		}

		//Re-order asset group
		var groupOrder = ['datasheet', 'brochure', 'whitepaper', 'report', 'video'];
		//console.log('#before: ', $scope.localVerticalName);
		for (var i = 0; i < $scope.localVerticalName.length; i++) {
			var tempItem = '';
			var supposedIndex = groupOrder.indexOf($scope.localVerticalName[i]);
			if (supposedIndex != -1 && $scope.localVerticalName[supposedIndex] != $scope.localVerticalName[i]) {
				tempItem = $scope.localVerticalName[supposedIndex];
				$scope.localVerticalName[supposedIndex] = $scope.localVerticalName[i];
				$scope.localVerticalName[i] = tempItem;
			}
		}
		//console.log('#after: ', $scope.localVerticalName);
		//return;
		
		$scope.featuredAssets = featuredData.splice(0, 1);
		
		for (var i = 0; i < Math.round((featuredData.length) / 2); i++) {
			var temp = [];
			temp.push(featuredData[i * 2]);
			if (i * 2 + 1 < featuredData.length) {
				temp.push(featuredData[i * 2 + 1]);
			}
			$scope.featuredRows.push(temp);
		}
		
		$scope.groupRows = [];
		for (var i = 0; i < Math.round($scope.localVerticalName.length / 2); i++) {
			var tempRow = [];
			tempRow.push({'typename': $scope.localVerticalName[i * 2], 'assets': localVertical[$scope.localVerticalName[i * 2]]});
			if (i * 2 + 1 < $scope.localVerticalName.length) {
				tempRow.push({'typename': $scope.localVerticalName[i * 2 + 1], 'assets': localVertical[$scope.localVerticalName[i * 2 + 1]]});
			}
			$scope.groupRows.push(tempRow);
		}
		
		$scope.temp = '';
		
		Translation.promise.then(function (resultData) {
			for (var j = 0; j < $scope.groupRows.length; j++) {
				for (var k = 0; k < $scope.groupRows[j].length; k++) {
					$scope.groupRows[j][k].typename = Translation.translate(resultData, $scope.groupRows[j][k].typename);
				}
			}
			$scope.ajaxLoading = false;
		});	
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
	
	$scope.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		
	};

	$scope.submit = function() {
		LeadgenForm.submit('frmleadgen', $scope.submitted);
	};
	
	LeadgenForm.promise.then(function (resultData) {
		$scope.formHeaderTitle = LeadgenForm.getFormTitle();
		$scope.formHeaderDescriptionShort = LeadgenForm.getFormDesc();
		$scope.supplier = LeadgenForm.getSupplierInfo();
		$scope.fieldList = LeadgenForm.getDownloadFormFields();
		jQuery(document).click();
	});
}

function AssetDetailCtrl($rootScope, $scope, $http, $routeParams, $filter, Translation, URLHandler, TabsAndNavigation) {
	tickValue = URLHandler.getTick();
	var assetTitle = typeof($routeParams.assettitle)=="undefined"?'':$routeParams.assettitle;
	var assetReportId = typeof($routeParams.id)=="undefined"?'':$routeParams.id;
	assetReportId = $rootScope.validateCspKey(assetReportId);
	var isPageValid = false;
	var requestURL = 'd1.aspx?p2004(ct21000&fLocal_Type~1=casestudy!)[]' + URLHandler.getUrlTick();
	$scope.assetItem = [];
	$http.get(requestURL).success(function(data) {
		data.pop();
		if (data.length < 1)
			isPageValid = false;
		else {
			for (var i = 0; i < data.length; i++) {
				if (data[i].cspuniquekey == assetReportId) {
					var tmpContent = data[i];
					data[i] = data[0];
					data[0] = tmpContent;
					isPageValid = true;
					break;
				}
			}
		}
		
		if (!isPageValid) {
			data = $filter('filter')(data, {contentid: assetReportId}, function (expected, actual) {
				return expected == actual;
			});
			isPageValid = data.length > 0;
		}
		if (!isPageValid) {
			$scope.assetTitle = '404 - Not Found';
			return;
		}
				
		$rootScope.updatePageInfo(null, data[0].title);
		var getBreadcrumbItem = function (dataList, currentItem, crrBreadcrumbs) {
			var nextItem;
			for (var i = 0; i < dataList.length; i++) {
				if (currentItem.parent == '') {
					if (dataList[i].identifier.toLowerCase() == 'home') {
						crrBreadcrumbs.push(dataList[i]);
						break;
					}
				}
				else if (dataList[i].identifier == currentItem.parent) {
					crrBreadcrumbs.push(dataList[i]);
					nextItem = dataList[i];
					break;
				}
			}
			if (nextItem != undefined)
				getBreadcrumbItem(dataList, nextItem, crrBreadcrumbs);
		};
		
		TabsAndNavigation.promise.then(function (tabsnavData) {
			var defaultBreadcrumb = [];
			var crrAsset;
			for (var i = 0; i < tabsnavData.length; i++) {
				if (tabsnavData[i].identifier == data[0].type) {
					crrAsset = tabsnavData[i];
					break;
				}
			}
			if (crrAsset != undefined) {
				defaultBreadcrumb.push(crrAsset);
				getBreadcrumbItem(tabsnavData, crrAsset, defaultBreadcrumb);
				defaultBreadcrumb.reverse();
				$scope.breadcrumbs = defaultBreadcrumb;
			}
		});
		
		switch (data[0].launchtype) {
			case 'newwindow': 
				$scope.assetFileLinkTarget = "_blank";
				break;
			case 'lightbox':
				$scope.assetFileLinkTarget = "_blank";
				break;
			default:
				$scope.assetFileLinkTarget = "_self";
		}
		data[0].launchtype = 'csp-' + data[0].launchtype;
		
		$scope.assetTitle = data[0].title;
		$scope.assetBanner = data[0].imgurl;
		$scope.assetDescription = data[0].description;
		$scope.assetLongDescription = data[0].descriptionlong;
		$scope.assetFileLinkUrl = data[0].linkurl;
		$scope.assetFileType = (data[0].filetype != 'video') ? 'document' : data[0].filetype;
		$scope.assetFileDownloadTitle = data[0].filedownloadtitle;
		$scope.assetFileLinkLaunchType = data[0].launchtype;
		$scope.assetItem = data[0];
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);
	});
}

function ProductsCtrl($scope, $http, $routeParams, $rootScope, $filter, Products, Translation, LeadgenForm, URLHandler) {
	tickValue = URLHandler.getTick();
	var blockData = [];
	var subBlockData = [];
	var currentItem;
	var exceptionValue = '';
	$scope.currentProduct;
	$scope.prodRows = [];
	$scope.featuredItems = [];
	$scope.id = 0;//typeof($routeParams.id)=="undefined"?1:$routeParams.id;
	
	var cspKey = typeof($routeParams.id)=="undefined"?1:$routeParams.id;
	
	if (typeof($routeParams.assettype)!="undefined") {
		exceptionValue = $routeParams.assettype;
	}
	
	$scope.getUniqueKey = function (cspKey) {
		if (cspKey == undefined) return '';
		return convertCSPUniqueKey(cspKey);
	};
	
	cspKey = $rootScope.validateCspKey(cspKey);
	
	var RequestForBreadcrumb = function ($scope, categoryId, breadcrumbInfo, productList) {
		for (var i = 0; i < productList.length; i++) {
			if (productList[i].categoryId == categoryId) {
				breadcrumbInfo.unshift({title: productList[i].title, link: $rootScope.baseDomain + '/#/products/' + convertCSPUniqueKey(productList[i].cspuniquekey)});
				if (productList[i].parentId != Products.rootid())
					RequestForBreadcrumb($scope, productList[i].parentId, breadcrumbInfo, productList);
				break;
			}
		}
		
		if (categoryId == Products.mainproductid() || categoryId == Products.rootid()) {
			breadcrumbInfo[0].link = $rootScope.baseDomain + '/#/';//Disable the link of "Products" item
			breadcrumbInfo[0].isProduct = true;
			breadcrumbInfo.unshift({title: 'Home', link: $rootScope.baseDomain + '/#/'});
			$scope.breadcrumbs = breadcrumbInfo;
		}
	}
	
	var GetPageContentBlocks = function ($scope, categoryId) {
		$scope.prodContentRows = [];
		$scope.prodContentFeaturedBlocks = [];
		$http.get('d1.aspx?p2009(ct38000&cd' + categoryId + ')[st(ct38000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
			if (data.length > 1)
				data.pop();
			var addedItem = 0;
			while (addedItem < data.length) {
				var temp = [];
				temp.push(data[addedItem]);
				addedItem++;
				if (data[addedItem - 1].imageUrl != '' || data[addedItem] == undefined) {
					$scope.prodContentRows.push(temp);
					continue;
				}
				if (data[addedItem].imageUrl == '') {
					temp.push(data[addedItem]);
					addedItem++;
				}
				$scope.prodContentRows.push(temp);
			}
			setTimeout(function() {
				jQuery(document).click();							
			}, 1000);
		});
	};
	
	//New update: build for Case Study
	var GetRelatedAssets = function ($scope, categoryId) {
		$scope.assetRows = [];
		$http.get('d1.aspx?p2004(ct21000&cd' + categoryId + 'i0)[st(ct21000*Status_Sort_Order*)]' + URLHandler.getUrlTick()).success(function(data) {
			data.pop();
			for (var i = 0; i < Math.round((data.length) / 2); i++) {
				var temp = [];
				temp.push(data[i * 2]);
				if (i * 2 + 1 < data.length) {
					temp.push(data[i * 2 + 1]);
				}
				for (var j = 0; j < temp.length; j++) {
					//temp[j].description = temp[j].description.trunc(100,true);
					temp[j].imgurl = (temp[j].imgurl == '') ? 'http://www.placehold.it/660x300' : temp[j].imgurl;
					switch (temp[j].launchtype) {
						case 'newwindow': 
							temp[j].target = "_blank";
							break;
						case 'lightbox':
							temp[j].target = "_blank";
							break;
						default:
							temp[j].target = "_self";
					}
					temp[j].launchtype = 'csp-' + temp[j].launchtype;
					temp[j].leadgen = '';
					if (temp[j].statusleadgen.toLowerCase() == 'yes') {
						temp[j].leadgen = 'call-leadgen';
					}
					if (temp[j].type == 'casestudy') {
						var tempTitleArray = temp[j].title.toLowerCase().split(" ");
						var urlTitle = encodeURIComponent(tempTitleArray.join('-'));
						//temp[j].linkurl = '#/casestudy/' + urlTitle + '/' + temp[j].contentid;
						temp[j].linkurl = '#/casestudy/' + urlTitle + '/' + temp[j].cspuniquekey;
						temp[j].target = '';
						temp[j].launchtype = '';
						temp[j].leadgen = '';
					}
				}
				
				$scope.assetRows.push(temp);
			};
			
			setTimeout(function() {
				jQuery(document).click();							
			}, 1000);
		});
	};
	
	var fillImages = function (prodItem) {
		if (prodItem == undefined || prodItem.image == undefined || prodItem.thumbnail == undefined) return;
		if (prodItem.image == '')
			prodItem.image = 'http://placehold.it/656x292';
		if (prodItem.thumbnail == '')
			prodItem.thumbnail = 'http://placehold.it/322x259';
	};
	
	Products.promise.then(function(data){
		currentItem = $filter('filter')(data, {cspuniquekey: cspKey}, function (expected, actual) {
			//var temp = expected.split(' ');
			return convertCSPUniqueKey(expected).toLowerCase() == actual.toLowerCase();
		});
		if (currentItem.length == 0) return;
		currentItem = currentItem[0];
		$scope.currentProduct = currentItem;
		$scope.id = currentItem.categoryId;
		blockData = $filter('filter')(data, {parentId: $scope.id}, function (expected, actual) {return parseInt(expected) == parseInt(actual);});
		blockData = $filter('filter')(blockData, {syndicationtype: 'showcase'}, function (expected, actual) {return (expected == '' || expected.toLowerCase() == 'all' || expected.toLowerCase() == actual);});
		subBlockData = blockData.slice(0);
		var addedItem = 0;
		while (addedItem < blockData.length) {			
			var temp = [];
			
			fillImages(blockData[addedItem]);
			fillImages(blockData[addedItem + 1]);
			
			temp.push(blockData[addedItem]);
			addedItem++;
			if (blockData[addedItem - 1].feature.toLowerCase() == 'yes' || blockData[addedItem] == undefined) {
				$scope.prodRows.push(temp);
				continue;
			}
			
			if (blockData[addedItem].feature.toLowerCase() != 'yes') {
				temp.push(blockData[addedItem]);
				addedItem++;
			}
			$scope.prodRows.push(temp);
		}
		
		if (currentItem != undefined) {
			$scope.productTitle = currentItem.title;//(exceptionValue == '') ? currentItem.title : exceptionValue;
			$scope.Content_Information_Detail = currentItem.Content_Information_Detail;
			$scope.Content_Description_Extended = currentItem.Content_Description_Extended;
			$rootScope.updatePageInfo(null, currentItem.title);
			var requestedBreadcrumb = [];
			if (exceptionValue == '')
				RequestForBreadcrumb($scope, currentItem.parentId, requestedBreadcrumb, data);
			else {
				//breadcrumbInfo.unshift({title: 'Home', link: '#/'});
				$scope.breadcrumbs = [];
				$scope.breadcrumbs.push({title: 'Home', link: $rootScope.baseDomain + '/#/'});
			}
			if (subBlockData.length == 0) {
				if ($scope.prodRows.length == 0) {
					var rowTemp = [];
					var temp = {
						categoryId: currentItem.categoryId,
						image: currentItem.image,
						filevideourl: currentItem.filevideourl,
						title: (currentItem.secondarytitle != '') ? currentItem.secondarytitle : currentItem.title,
						description: currentItem.description,
						textorientation: currentItem.textorientation,
						feature: 'yes',
						lastItem:true
					}
					rowTemp.push(temp)
					$scope.prodRows.push(rowTemp);
				}
				$scope.productLongDescription = currentItem.descriptionlong;
				GetPageContentBlocks($scope, currentItem.categoryId);
				GetRelatedAssets($scope, currentItem.categoryId);
			}
		}
		
		setTimeout(function() {
			//console.log("*** Product Promised CLICKED");
			jQuery(document).click();							
		}, 1000);		
	});
	
	
	//Contact us block
	$scope.btnContactUsText = 'Contact Us';
	$scope.btnTweetUsText = 'Tweet Us'
	$scope.readyToBuy = 'readytobuy';
	$scope.textLearnMore = 'learnmore';
	Translation.promise.then(function (resultData) {
		$scope.btnContactUsText = Translation.translate(resultData, $scope.btnContactUsText);
		$scope.btnTweetUsText = Translation.translate(resultData, $scope.btnTweetUsText);
		$scope.readyToBuy = Translation.translate(resultData, $scope.readyToBuy);
		$scope.textLearnMore = Translation.translate(resultData, $scope.textLearnMore);
		$scope.readyToBuy = $scope.readyToBuy.replace('{consumer:FPersonalize_Contact_Phonenumber}', $rootScope.Supplier.telephone);
	});
	
	$scope.submitted = function (resultMsg) {
		try {
			jQuery.colorbox.close();
		} catch(err) {}
		
	};

	$scope.submit = function() {
		LeadgenForm.submitLeadgen('frmleadgen', $scope.submitted);
	};
	
	$scope.GetColorCode = function(blockBgColor){
		if (typeof(blockBgColor) == 'undefined' || blockBgColor == undefined || blockBgColor == '') return '';
		var bgCode = blockBgColor.replace('#','');
		return 'background-color: #' + bgCode;
	};
	
	LeadgenForm.promise.then(function (resultData) {
		downloadFormFields = LeadgenForm.getDownloadFormFields();
	});
}

function MainNavCtrl($scope, $http, $rootScope, $routeParams, $filter, Products, URLHandler, TabsAndNavigation) {
	tickValue = URLHandler.getTick();
	if (QueryString.ticks != undefined) {
		currentTick = '&i=' + QueryString.ticks;
		tickValue = QueryString.ticks;
		URLHandler.setTick(QueryString.ticks);
	}
	else if ($routeParams.tick == undefined) {
		var currentUrl = document.URL;
		var currentTick = '';
		if (currentUrl.indexOf('/i/') > 0) {
			var temp = currentUrl.split('/');
			for (var i = 0; i < temp.length; i++) {
				if (temp[i] == 'i') {
					currentTick = '&i=' + temp[i + 1];
					tickValue = temp[i + 1];
					URLHandler.setTick(temp[i + 1]);
					break;
				}
			}
		}
	}
	
	$scope.invalidCountryCategory = [];
	var menuLevelToShow = 2;
	var allNavItems = [];
	var mainNavItems = [];
	
	var GetFullLink = function (data, i) {
		if (data[i].parent == '') {
			if (data[i].link != '')
				data[i].link = $rootScope.baseDomain + "/#/" + data[i].link;
			else
				data[i].link = $rootScope.baseDomain + "/#/";
			return data[i].link;
		}
		
		for (var j = 0; j < data.length; j++) {
			if (data[j].identifier == data[i].parent) {
				if (data[j].link.indexOf($rootScope.baseDomain) == -1)
					data[i].link = $rootScope.baseDomain + "/#/" + data[j].link + '/' + data[i].link;
				else
					data[i].link = data[j].link + '/' + data[i].link;
				//console.log(data[i].link);
				return data[i].link;
			}
		}
	}
	
	var GetChildNodes = function (parentNode, treeNode, currentLevel, maxLevel) {
		var childNodes = [];
		var productNode;

		for (var i = 0; i < treeNode.children.length; i++) {
			var childNode = treeNode.children[i];
			if (childNode.value.syndicationtype != '' && childNode.value.syndicationtype.toLowerCase() != 'all' && childNode.value.syndicationtype.toLowerCase() != 'showcase') continue;

			var temp =
				{
					"identifier" : childNode.value.categoryId,
					"title" : childNode.value.title,
					"link" : $rootScope.baseDomain + "/#/products/" + convertCSPUniqueKey(childNode.value.cspuniquekey),// childNode.value.categoryId,
					"parent" : parentNode.identifier,
					"level" : childNode.value.depth,
					"displayorder" : childNode.value.displayorder
				};
			temp.nodes = [];
			temp.hideSubMenu = false;
			temp.hasNodes = (childNode.children != undefined && currentLevel < maxLevel);
			temp.dataToggleValue = '';
			if (temp.hasNodes)
				GetChildNodes(temp, childNode, currentLevel + 1, maxLevel);
			childNodes.push(temp);
		}
		parentNode.nodes = childNodes;
	}
	
	var UpdateProductsMenu = function (maxLevel) {
		if ($rootScope.AllProducts == undefined) return;
		var prodItemIndex = 0;
		for (var i = 0; i < $scope.navitems.length; i++) {
			if ($scope.navitems[i].identifier == 'products') {
				prodItemIndex = i;
				break;
			}
		}
		
		for (var j = 0; j < $rootScope.AllProducts.length; j++) {
			if ($rootScope.AllProducts[j].value.categoryId == Products.mainproductid()) {
				GetChildNodes($scope.navitems[prodItemIndex], $rootScope.AllProducts[j], 1, maxLevel);
				break;
			}
		}
	};
	
	var GetCountryFilteredList = function (resultData) {
		if (typeof(cspConsumerInfo) == 'undefined' || typeof(cspConsumerInfo.country) == undefined) {
			console.log('Invalid cspConsumerInfo');
			return;
		}
		var consumerCountry = cspConsumerInfo.country;
		for (var w = resultData.length - 1; w >= 0; w--) {
			if (resultData[w].availablecountry != '') {
				var countryList = resultData[w].availablecountry.split(',');
				if (countryList.indexOf(consumerCountry) == -1) {
					$scope.invalidCountryCategory.push(parseInt(resultData[w].categoryId));
					resultData.splice(w, 1);
				}
			}
		}
	}
	
	//Get JSON data for navigation
	//$http.get('d1.aspx?p2001(ct34000)[]' + URLHandler.getUrlTick()).success(function(data) {
	TabsAndNavigation.promise.then(function (data) {
		var n = data.length;
		try {
		if (n>0 &&  data[n-1].identifier=="")
		data.pop();
		} catch(ex) {}
		for (var i = 0; i < data.length; i++) {
			data[i].displayorder = parseInt(data[i].displayorder);
			if (data[i].level == '') data[i].level = 1;
			data[i].level == parseInt(data[i].level);
			data[i].nodes = [];
			data[i].hideSubMenu = false;
			data[i].hasNodes = false;
			data[i].dataToggleValue = '';
			if (data[i].level == '' || data[i].level < 2) {
				for (var j = 0; j < data.length; j++) {
					if (data[j].parent == data[i].identifier || data[i].identifier == 'products') {
						data[i].hasNodes = true;
						data[i].dataToggleValue = 'dropdown';
						break;
					}
				}
			}
			data[i].link = GetFullLink(data, i);
			if (data[i].level <= 1) {
				mainNavItems.push(data[i]);
			}	
			else
				allNavItems.push(data[i]);
		}
		
		for (var i = 0; i < mainNavItems.length; i++) {
			if (mainNavItems[i].hasNodes == true)
				mainNavItems[i].link = '';
		}
		$scope.navitems = mainNavItems;
		
		Products.promise.then(function(resultData){
			if (typeof(resultData[0].availablecountry) != 'undefined') {
				GetCountryFilteredList(resultData);
			}
			else
				console.log('Invalid content types field Available_Country');
			
			$rootScope.AllProducts = Products.convertJSONtoTree(resultData);
			UpdateProductsMenu(menuLevelToShow);
		});
	});
	
	//Show/hide for sub menu
	$scope.showSubItems = function (data) {
		data.hideSubMenu = false;
		if (data.nodes.length > 0) return;
		for (var i = 0; i < allNavItems.length; i++) {
			if (allNavItems[i].parent == data.identifier) {
				data.nodes.push(allNavItems[i]);
			}
		}
	}
}
