function cspGetQuerystring(key, default_) {
    if (default_ == null)
        default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid) {
    this.cnid = cnid;
    this.depth = depth;
    this.id = id;
    this.pid = pid;
    this.text = text;
    this.displayOrder = displayOrder;
    this.lineage = lineage.split('/');
    this.level = level;
    this.desc = desc;
    this.pn = pn;
    this.rid = rid;
    this.SearchUp = function(level) {
        for(var i = 0; i < this.lineage.length; i++) {
            if(this.lineage[i] == this.id) {
                try {
                    return this.lineage[i - level];
                } catch(e) {
                    return null;
                }
            }
        }
        return null;
    }
}


$(document).ready(function() {
    setTimeout(function() {
        var a = document.getElementById('csp-tout-area');
        if(a && a.style) {
            a.style.height = a.offsetHeight + 'px'
        }
        setAnchorsValidHREFs();
        checkURL();
        checkCobrandLogo();
        initializeWebtrends();
    }, 500);
});
Array.prototype.findParentId = function(attrId, value) {
    for(var i = 0; i < this.length; i++) {
        if(this[i][attrId] == value)
            return i;
    }
    return -1;
}
Array.prototype.findByAttrs = function() {
    if(arguments.length % 2 != 0)
        throw "invalid name value pair arguments";
    var dict = [], i, j, r = false;
    for( i = 0; i < arguments.length; i = i + 2) {
        dict.push([arguments[i], arguments[i + 1]]);
    }
    for( i = 0; i < this.length; i++) {
        r = true;
        for( j = 0; j < dict.length; j++)
            r = r && (this[i][dict[j][0]] == dict[j][1]);
        if(r)
            break;
    }
    return ( r ? i : -1);
}
var categories = {};
(function(c) {
    c.list = [];
    c.MI = 0; //Constant for the Menu Item value
    c.setMI = function (mi) {
        c.MI = mi;
    }
    c.getMI = function () {
        return c.MI;
    }
    c.Find = function(id) {
        var t = null;
        for(var i = 0; i < this.list.length; i++) {
            t = this.FindInTree(id, this.list[i]);
            if(t != null)
                return t;
        }
        return null;
    }

    c.FindAll = function(id, level) {
        var t = null, results = [];
        for(var i = 0; i < this.list.length; i++) {
            t = this.FindAllInTree(id, level, this.list[i], results);
        }
        return results;
    }

    c.FindByAttr = function(name, value) {
        var t = null, results = [];
        for(var i = 0; i < this.list.length; i++) {
            this.FindInTreeByAttr(name, value, this.list[i], results);
        }
        return results;
    }
    c.FindInTreeByAttr = function(name, value, tree, results) {
        var _t = tree;
        if(_t == null)
            _t = this.list;
        if(_t[name] == value)
            results.push(_t);
        if( typeof (_t.children) == "undefined")
            return null;
        var r = null;
        for(var i = 0; i < _t.children.length; i++) {
            c.FindInTreeByAttr(name, value, _t.children[i], results);
        }
    }
    c.FindAllInTree = function(id, level, tree, results) {
        var _t = tree;
        if(_t == null)
            _t = this.list;
        if(_t.id == id) {
            if(level != null && _t.level == level)
                results.push(_t);
            else if(level == null)
                results.push(_t);
        }
        if( typeof (_t.children) == "undefined")
            return null;
        var r = null;
        for(var i = 0; i < _t.children.length; i++) {
            c.FindAllInTree(id, level, _t.children[i], results);
        }
    }
    c.FindInTree = function(id, tree) {
        var _t = tree;
        if(_t == null)
            _t = this.list;
        if(_t.id == id)
            return _t;
        if( typeof (_t.children) == "undefined")
            return null;
        var r = null;
        for(var i = 0; i < _t.children.length; i++) {
            r = c.FindInTree(id, _t.children[i]);
            if(r != null)
                return r;
        }
        return null;
    }
    c.Add = function(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid) {
        var len = this.list.length;
        if(len == 0 || (len > 0 && this.list[len - 1].depth <= depth))
            this.list.push(new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid));
        else if(len > 0) {
            for(var i = 0; i < len; i++) {
                if(this.list[i].depth >= depth) {
                    this.list.splice(i, 0, new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid));
                    break;
                }
            }
        }
    }
    c.Nest = function() {
        if(this.list.length > 0) {
            var i = this.list.length - 1, id, j, k;
            while(i >= 0) {
                id = this.list.findParentId("id", this.list[i].pid);
                if(id != -1) {
                    var temp = this.list.splice(i, 1);
                    if( typeof (this.list[id]["children"]) == "undefined")
                        this.list[id]["children"] = [];
                    temp[0].parent = this.list[id];
                    this.list[id].children.push(temp[0]);
                }
                i--;
            }
            //if we are missing a level, then we will automatically pull them back
            for( i = 0; i < this.list.length; i++) {
                if( typeof (this.list[i].children) == "undefined")
                    continue;
                j = this.list[i].children.length - 1;
                var cat, pcat;
                while(j >= 0) {
                    cat = this.list[i].children[j];
                    if(cat.level > 1) {
                        // find the sibling that has the same level
                        for( k = 0; k < this.list[i].children.length; k++) {
                            if(this.list[i].children[k].level < cat.level && this.list[i].children[k].id == cat.id)
                                break;
                            continue;
                        }
                        if(k < this.list[i].children.length) {
                            pcat = this.list[i].children[k];
                            if( typeof (pcat.children) == "undefined")
                                pcat.children = [];
                            cat.parent = pcat;
                            pcat.children.push(cat);
                            this.list[i].children.splice(j, 1);
                        }
                    }
                    j--;
                }
            }
        }
    }
    c.sort = function(c1, c2) {
        return parseInt(c1.displayOrder) < parseInt(c2.displayOrder) ? -1 : 1;
    }
    c.sort_level = function(c1, c2) {
        return parseInt(c1.level) > parseInt(c2.level) ? 1: -1;
    }
    c.Render = function(elementId) {
        //this.list.sort(this.sort_level); // This sort causes an error in Chrome
        c.Nest();
        this.list.sort(this.sort);
        var el = $(elementId);
        if(el.length > 0) {
            //startpaste
			var vTicks = cspGetQuerystring('ticks');  //"ticks" is the URL parameter who's value you want
			if (vTicks != "")
				{
				vTicks = "&ticks=" + vTicks;
				}
            var html = "", i = 0;
            for( i = 0; i < this.list.length; i++) {
                html = '';
                cObj0 = this.list[i];
				cObj0.href = '?p11(ct1&cd' + cObj0.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj0.id + 'i0):c5(ct2&cd' + cObj0.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj0.id + '&lvl=1&mi=0' + vTicks;
                html += '<div id=\"csp-wrapperN\"><div id=\"csp-topNav\">';
                html += '<div class=\"nav-left\"></div>';
                html += '<div class=\"nav\">';
                html += '<ul id=\"navigation\">';
                var c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0, c8 = 0, c9 = 0, c10 = 0, c11 = 0, c12 = 0, c13 = 0, c14 = 0, c15 = 0, c16 = 0, c17 = 0, c18 = 0, c19 = 0, c20 = 0, c21 = 0, c22 = 0, c23 = 0, c24 = 0, c25 = 0, c26 = 0, c27 = 0, c28 = 0, c29 = 0, c30 = 0, c31 = 0, c32 = 0, c33 = 0, c34 = 0, c35 = 0, c36 = 0, c37 = 0, c38 = 0, c39 = 0, c40 = 0, c41 = 0, c42 = 0, c43 = 0, c44 = 0, c45 = 0, c46 = 0, c47 = 0, c48 = 0, c49 = 0, c50 = 0;
                var cObj0, cObj1 = 0, cObj2 = 0, cObj3 = 0, cObj4 = 0, cObj5 = 0, cObj6 = 0, cObj7 = 0, cObj8 = 0, cObj9 = 0, cObj10 = 0, cObj11 = 0, cObj12 = 0, cObj13 = 0, cObj14 = 0, cObj15 = 0, cObj16 = 0, cObj17 = 0, cObj18 = 0, cObj19 = 0, cObj20 = 0, cObj21 = 0, cObj22 = 0, cObj23 = 0, cObj24 = 0, cObj25 = 0, cObj26 = 0, cObj27 = 0, cObj28 = 0, cObj29 = 0, cObj30 = 0, cObj31 = 0, cObj32 = 0, cObj33 = 0, cObj34 = 0, cObj35 = 0, cObj36 = 0, cObj37 = 0, cObj38 = 0, cObj39 = 0, cObj40 = 0, cObj41 = 0, cObj42 = 0, cObj43 = 0, cObj44 = 0, cObj45 = 0, cObj46 = 0, cObj47 = 0, cObj48 = 0, cObj49 = 0, cObj50 = 0;
                var mi = 0;
                if( typeof (cObj0.children) != "undefined") {
                    for( c1 = 0; c1 < cObj0.children.length; c1++) {
                        cObj1 = cObj0.children[c1];
						cObj1.href = '?p11(ct1&cd' + cObj1.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj1.id + 'i0):c5(ct2&cd' + cObj1.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj1.id + '&lvl=' + cObj1.level + '&mi=' + c1 + vTicks;
                        var tv = '';
                        if (c1 == c.getMI()) {
                            tv = 'active';
                        } 
                        html += '<li class=\"' + tv + '\">';
                        html += '<a href=\"' + cObj1.href + '\">';
                        var hasChildren = "", ArrowDown = "", patch ="";
                        if( typeof (cObj1.children) != "undefined") {
                            hasChildren = "hasChildren";
                            ArrowDown = "<img src='http://ui.syndication.tiekinetix.com/ui/showcase/custom/thehartford/menu/images/ArrowDown.png' class='Arrows'/>";
                            patch = '<div class=\"menu-mid-patch\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';
                        }
                        html += '<div class=\"menu-mid ' + hasChildren + '\">';
                        html += '<div class=\"menu-mid-sectionTitle\">' + cObj1.text + ArrowDown + '</div>';
                        
                        if( typeof (cObj1.children) != "undefined") {
                            cObj1.children.sort(this.sort);
                            html += patch + '<ul class=\"sub\">';
                            for( c2 = 0; c2 < cObj1.children.length; c2++) {
                                cObj2 = cObj1.children[c2];
								cObj2.href = '?p11(ct1&cd' + cObj2.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj2.id + 'i0):c5(ct2&cd' + cObj2.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj2.id + '&lvl=' + cObj2.level + '&mi=' + c1+ vTicks;
                                var ArrowRight = "<img src='http://ui.syndication.tiekinetix.com/ui/showcase/custom/thehartford/menu/images/ArrowRight.png' class='Arrows'/>";
                                html += '<li><a href=\"' + cObj2.href + '\"><span class="subtext">' + cObj2.text + ArrowRight + '</span></a>';
                                if( typeof (cObj2.children) != "undefined") {
                                    cObj2.children.sort(this.sort);
                                    html += '<ul class=\"subsub\">';
                                    for( c3 = 0; c3 < cObj2.children.length; c3++) {
                                        cObj3 = cObj2.children[c3];
										cObj3.href = '?p11(ct1&cd' + cObj3.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj3.id + 'i0):c5(ct2&cd' + cObj3.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj3.id + '&lvl=' + cObj3.level + '&mi=' + c1 + vTicks;
                                        html += '<li><a href="' + cObj3.href + '">' + cObj3.text + ArrowRight + '</a>';

                                        if( typeof (cObj3.children) != "undefined") {
                                            html += '<div class=\"subHidden"><ul class=\"sub\">';
                                            for( c4 = 0; c4 < cObj3.children.length; c4++) {
                                                cObj4 = cObj3.children[c4];
                                                cObj4.href = '?p11(ct1&cd' + cObj4.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj4.id + 'i0):c5(ct2&cd' + cObj4.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj4.id + '&lvl=' + cObj4.level + '&mi=' + c1 + vTicks;
                                                html += '<li class=\"subHidden"><a href="' + cObj4.href + '">' + cObj4.text + '</a>';

                                                if( typeof (cObj4.children) != "undefined") {
                                                    html += '<div class=\"subHidden"><ul class=\"sub\">';
                                                    for( c5 = 0; c5 < cObj4.children.length; c5++) {
                                                        cObj5 = cObj4.children[c5];
                                                        cObj5.href = '?p11(ct1&cd' + cObj5.id + 'i0)[st(ct1*Status_Sort_Order asc*)]:c4(ct2&cd' + cObj5.id + 'i0):c5(ct2&cd' + cObj5.id + 'i0)[]:c8(ct2&cd12i99)[st(ct2*Status_Sort_Order asc*)]&cat=' + cObj5.id + '&lvl=' + cObj5.level + '&mi=' + c1 + vTicks;
                                                        html += '<li class=\"subHidden"><a href="' + cObj5.href + '">' + cObj5.text + '</a></li>';
/*
                                                        if( typeof (cObj5.children) != "undefined") {
                                                            html += "<ul>";
                                                            for( c6 = 0; c6 < cObj5.children.length; c6++) {
                                                                cObj6 = cObj5.children[c6];
                                                                cObj6.href = '?p7(ct1&cd' + cObj6.id + 'i0):c4(ct2&cd' + cObj6.id + 'i0):c5(ct2&cd' + cObj6.id + 'i0)[]i=' + cObj6.id + '';
                                                                html += '<li><a href="' + cObj6.href + '">' + cObj6.text + '</a></li>';
                                                                if( typeof (cObj6.children) != "undefined") {
                                                                    html += "<ul>";
                                                                    for( c7 = 0; c7 < cObj6.children.length; c7++) {
                                                                        cObj7 = cObj6.children[c7];
                                                                        cObj7.href = '?p7(ct1&cd' + cObj7.id + 'i0):c4(ct2&cd' + cObj7.id + 'i0):c5(ct2&cd' + cObj7.id + 'i0)[]i=' + cObj7.id + '';
                                                                        html += '<li><a href="' + cObj7.href + '">' + cObj7.text + '</a></li>';
                                                                        if( typeof (cObj7.children) != "undefined") {
                                                                            html += "<ul>";
                                                                            for( c8 = 0; c8 < cObj7.children.length; c8++) {
                                                                                cObj8 = cObj7.children[c8];
                                                                                cObj8.href = '?p7(ct1&cd' + cObj8.id + 'i0):c4(ct2&cd' + cObj8.id + 'i0):c5(ct2&cd' + cObj8.id + 'i0)[]i=' + cObj8.id + '';
                                                                                html += '<li><a href="' + cObj8.href + '">' + cObj8.text + '</a></li>';
                                                                                if( typeof (cObj8.children) != "undefined") {
                                                                                    html += "<ul>";
                                                                                    for( c9 = 0; c9 < cObj8.children.length; c9++) {
                                                                                        cObj9 = cObj8.children[c9];
                                                                                        cObj9.href = '?p7(ct1&cd' + cObj9.id + 'i0):c4(ct2&cd' + cObj9.id + 'i0):c5(ct2&cd' + cObj9.id + 'i0)[]i=' + cObj9.id + '';
                                                                                        html += '<li><a href="' + cObj9.href + '">' + cObj9.text + '</a></li>';
                                                                                        if( typeof (cObj9.children) != "undefined") {
                                                                                            html += "<ul>";
                                                                                            for( c10 = 0; c10 < cObj9.children.length; c10++) {
                                                                                                cObj10 = cObj9.children[c10];
                                                                                                cObj10.href = '?p7(ct1&cd' + cObj10.id + 'i0):c4(ct2&cd' + cObj10.id + 'i0):c5(ct2&cd' + cObj10.id + 'i0)[]i=' + cObj10.id + '';
                                                                                                html += '<li><a href="' + cObj10.href + '">' + cObj10.text + '</a></li>';
                                                                                                if( typeof (cObj10.children) != "undefined") {
                                                                                                    html += "<ul>";
                                                                                                    for( c11 = 0; c11 < cObj10.children.length; c11++) {
                                                                                                        cObj11 = cObj10.children[c11];
                                                                                                        cObj11.href = '?p7(ct1&cd' + cObj11.id + 'i0):c4(ct2&cd' + cObj11.id + 'i0):c5(ct2&cd' + cObj11.id + 'i0)[]i=' + cObj11.id + '';
                                                                                                        html += '<li><a href="' + cObj11.href + '">' + cObj11.text + '</a></li>';
                                                                                                        if( typeof (cObj11.children) != "undefined") {
                                                                                                            html += "<ul>";
                                                                                                            for( c12 = 0; c12 < cObj11.children.length; c12++) {
                                                                                                                cObj12 = cObj11.children[c12];
                                                                                                                cObj12.href = '?p7(ct1&cd' + cObj12.id + 'i0):c4(ct2&cd' + cObj12.id + 'i0):c5(ct2&cd' + cObj12.id + 'i0)[]i=' + cObj12.id + '';
                                                                                                                html += '<li><a href="' + cObj12.href + '">' + cObj12.text + '</a></li>';
                                                                                                                if( typeof (cObj12.children) != "undefined") {
                                                                                                                    html += "<ul>";
                                                                                                                    for( c13 = 0; c13 < cObj12.children.length; c13++) {
                                                                                                                        cObj13 = cObj12.children[c13];
                                                                                                                        cObj13.href = '?p7(ct1&cd' + cObj13.id + 'i0):c4(ct2&cd' + cObj13.id + 'i0):c5(ct2&cd' + cObj13.id + 'i0)[]i=' + cObj13.id + '';
                                                                                                                        html += '<li><a href="' + cObj13.href + '">' + cObj13.text + '</a></li>';
                                                                                                                        if( typeof (cObj13.children) != "undefined") {
                                                                                                                            html += "<ul>";
                                                                                                                            for( c14 = 0; c14 < cObj13.children.length; c14++) {
                                                                                                                                cObj14 = cObj13.children[c14];
                                                                                                                                cObj14.href = '?p7(ct1&cd' + cObj14.id + 'i0):c4(ct2&cd' + cObj14.id + 'i0):c5(ct2&cd' + cObj14.id + 'i0)[]i=' + cObj14.id + '';
                                                                                                                                html += '<li><a href="' + cObj14.href + '">' + cObj14.text + '</a></li>';
                                                                                                                                if( typeof (cObj14.children) != "undefined") {
                                                                                                                                    html += "<ul>";
                                                                                                                                    for( c15 = 0; c15 < cObj14.children.length; c15++) {
                                                                                                                                        cObj15 = cObj14.children[c15];
                                                                                                                                        cObj15.href = '?p7(ct1&cd' + cObj15.id + 'i0):c4(ct2&cd' + cObj15.id + 'i0):c5(ct2&cd' + cObj15.id + 'i0)[]i=' + cObj15.id + '';
                                                                                                                                        html += '<li><a href="' + cObj15.href + '">' + cObj15.text + '</a></li>';
                                                                                                                                        if( typeof (cObj15.children) != "undefined") {
                                                                                                                                            html += "<ul>";
                                                                                                                                            for( c16 = 0; c16 < cObj15.children.length; c16++) {
                                                                                                                                                cObj16 = cObj15.children[c16];
                                                                                                                                                cObj16.href = '?p7(ct1&cd' + cObj16.id + 'i0):c4(ct2&cd' + cObj16.id + 'i0):c5(ct2&cd' + cObj16.id + 'i0)[]i=' + cObj16.id + '';
                                                                                                                                                html += '<li><a href="' + cObj16.href + '">' + cObj16.text + '</a></li>';
                                                                                                                                                if( typeof (cObj16.children) != "undefined") {
                                                                                                                                                    html += "<ul>";
                                                                                                                                                    for( c17 = 0; c17 < cObj16.children.length; c17++) {
                                                                                                                                                        cObj17 = cObj16.children[c17];
                                                                                                                                                        cObj17.href = '?p7(ct1&cd' + cObj17.id + 'i0):c4(ct2&cd' + cObj17.id + 'i0):c5(ct2&cd' + cObj17.id + 'i0)[]i=' + cObj17.id + '';
                                                                                                                                                        html += '<li><a href="' + cObj17.href + '">' + cObj17.text + '</a></li>';
                                                                                                                                                        if( typeof (cObj17.children) != "undefined") {
                                                                                                                                                            html += "<ul>";
                                                                                                                                                            for( c18 = 0; c18 < cObj17.children.length; c18++) {
                                                                                                                                                                cObj18 = cObj17.children[c18];
                                                                                                                                                                cObj18.href = '?p7(ct1&cd' + cObj18.id + 'i0):c4(ct2&cd' + cObj18.id + 'i0):c5(ct2&cd' + cObj18.id + 'i0)[]i=' + cObj18.id + '';
                                                                                                                                                                html += '<li><a href="' + cObj18.href + '">' + cObj18.text + '</a></li>';
                                                                                                                                                                if( typeof (cObj18.children) != "undefined") {
                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                    for( c19 = 0; c19 < cObj18.children.length; c19++) {
                                                                                                                                                                        cObj19 = cObj18.children[c19];
                                                                                                                                                                        cObj19.href = '?p7(ct1&cd' + cObj19.id + 'i0):c4(ct2&cd' + cObj19.id + 'i0):c5(ct2&cd' + cObj19.id + 'i0)[]i=' + cObj19.id + '';
                                                                                                                                                                        html += '<li><a href="' + cObj19.href + '">' + cObj19.text + '</a></li>';
                                                                                                                                                                        if( typeof (cObj19.children) != "undefined") {
                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                            for( c20 = 0; c20 < cObj19.children.length; c20++) {
                                                                                                                                                                                cObj20 = cObj19.children[c20];
                                                                                                                                                                                cObj20.href = '?p7(ct1&cd' + cObj20.id + 'i0):c4(ct2&cd' + cObj20.id + 'i0):c5(ct2&cd' + cObj20.id + 'i0)[]i=' + cObj20.id + '';
                                                                                                                                                                                html += '<li><a href="' + cObj20.href + '">' + cObj20.text + '</a></li>';
                                                                                                                                                                                if( typeof (cObj20.children) != "undefined") {
                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                    for( c21 = 0; c21 < cObj20.children.length; c21++) {
                                                                                                                                                                                        cObj21 = cObj20.children[c21];
                                                                                                                                                                                        cObj21.href = '?p7(ct1&cd' + cObj21.id + 'i0):c4(ct2&cd' + cObj21.id + 'i0):c5(ct2&cd' + cObj21.id + 'i0)[]i=' + cObj21.id + '';
                                                                                                                                                                                        html += '<li><a href="' + cObj21.href + '">' + cObj21.text + '</a></li>';
                                                                                                                                                                                        if( typeof (cObj21.children) != "undefined") {
                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                            for( c22 = 0; c22 < cObj21.children.length; c22++) {
                                                                                                                                                                                                cObj22 = cObj21.children[c22];
                                                                                                                                                                                                cObj22.href = '?p7(ct1&cd' + cObj22.id + 'i0):c4(ct2&cd' + cObj22.id + 'i0):c5(ct2&cd' + cObj22.id + 'i0)[]i=' + cObj22.id + '';
                                                                                                                                                                                                html += '<li><a href="' + cObj22.href + '">' + cObj22.text + '</a></li>';
                                                                                                                                                                                                if( typeof (cObj22.children) != "undefined") {
                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                    for( c23 = 0; c23 < cObj22.children.length; c23++) {
                                                                                                                                                                                                        cObj23 = cObj22.children[c23];
                                                                                                                                                                                                        cObj23.href = '?p7(ct1&cd' + cObj23.id + 'i0):c4(ct2&cd' + cObj23.id + 'i0):c5(ct2&cd' + cObj23.id + 'i0)[]i=' + cObj23.id + '';
                                                                                                                                                                                                        html += '<li><a href="' + cObj23.href + '">' + cObj23.text + '</a></li>';
                                                                                                                                                                                                        if( typeof (cObj23.children) != "undefined") {
                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                            for( c24 = 0; c24 < cObj23.children.length; c24++) {
                                                                                                                                                                                                                cObj24 = cObj23.children[c24];
                                                                                                                                                                                                                cObj24.href = '?p7(ct1&cd' + cObj24.id + 'i0):c4(ct2&cd' + cObj24.id + 'i0):c5(ct2&cd' + cObj24.id + 'i0)[]i=' + cObj24.id + '';
                                                                                                                                                                                                                html += '<li><a href="' + cObj24.href + '">' + cObj24.text + '</a></li>';
                                                                                                                                                                                                                if( typeof (cObj24.children) != "undefined") {
                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                    for( c25 = 0; c25 < cObj24.children.length; c25++) {
                                                                                                                                                                                                                        cObj25 = cObj24.children[c25];
                                                                                                                                                                                                                        cObj25.href = '?p7(ct1&cd' + cObj25.id + 'i0):c4(ct2&cd' + cObj25.id + 'i0):c5(ct2&cd' + cObj25.id + 'i0)[]i=' + cObj25.id + '';
                                                                                                                                                                                                                        html += '<li><a href="' + cObj25.href + '">' + cObj25.text + '</a></li>';
                                                                                                                                                                                                                        if( typeof (cObj25.children) != "undefined") {
                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                            for( c26 = 0; c26 < cObj25.children.length; c26++) {
                                                                                                                                                                                                                                cObj26 = cObj25.children[c26];
                                                                                                                                                                                                                                cObj26.href = '?p7(ct1&cd' + cObj26.id + 'i0):c4(ct2&cd' + cObj26.id + 'i0):c5(ct2&cd' + cObj26.id + 'i0)[]i=' + cObj26.id + '';
                                                                                                                                                                                                                                html += '<li><a href="' + cObj26.href + '">' + cObj26.text + '</a></li>';
                                                                                                                                                                                                                                if( typeof (cObj26.children) != "undefined") {
                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                    for( c27 = 0; c27 < cObj26.children.length; c27++) {
                                                                                                                                                                                                                                        cObj27 = cObj26.children[c27];
                                                                                                                                                                                                                                        cObj27.href = '?p7(ct1&cd' + cObj27.id + 'i0):c4(ct2&cd' + cObj27.id + 'i0):c5(ct2&cd' + cObj27.id + 'i0)[]i=' + cObj27.id + '';
                                                                                                                                                                                                                                        html += '<li><a href="' + cObj27.href + '">' + cObj27.text + '</a></li>';
                                                                                                                                                                                                                                        if( typeof (cObj27.children) != "undefined") {
                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                            for( c28 = 0; c28 < cObj27.children.length; c28++) {
                                                                                                                                                                                                                                                cObj28 = cObj27.children[c28];
                                                                                                                                                                                                                                                cObj28.href = '?p7(ct1&cd' + cObj28.id + 'i0):c4(ct2&cd' + cObj28.id + 'i0):c5(ct2&cd' + cObj28.id + 'i0)[]i=' + cObj28.id + '';
                                                                                                                                                                                                                                                html += '<li><a href="' + cObj28.href + '">' + cObj28.text + '</a></li>';
                                                                                                                                                                                                                                                if( typeof (cObj28.children) != "undefined") {
                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                    for( c29 = 0; c29 < cObj28.children.length; c29++) {
                                                                                                                                                                                                                                                        cObj29 = cObj28.children[c29];
                                                                                                                                                                                                                                                        cObj29.href = '?p7(ct1&cd' + cObj29.id + 'i0):c4(ct2&cd' + cObj29.id + 'i0):c5(ct2&cd' + cObj29.id + 'i0)[]i=' + cObj29.id + '';
                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj29.href + '">' + cObj29.text + '</a></li>';
                                                                                                                                                                                                                                                        if( typeof (cObj29.children) != "undefined") {
                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                            for( c30 = 0; c30 < cObj29.children.length; c30++) {
                                                                                                                                                                                                                                                                cObj30 = cObj29.children[c30];
                                                                                                                                                                                                                                                                cObj30.href = '?p7(ct1&cd' + cObj30.id + 'i0):c4(ct2&cd' + cObj30.id + 'i0):c5(ct2&cd' + cObj30.id + 'i0)[]i=' + cObj30.id + '';
                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj30.href + '">' + cObj30.text + '</a></li>';
                                                                                                                                                                                                                                                                if( typeof (cObj30.children) != "undefined") {
                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                    for( c31 = 0; c31 < cObj30.children.length; c31++) {
                                                                                                                                                                                                                                                                        cObj31 = cObj30.children[c31];
                                                                                                                                                                                                                                                                        cObj31.href = '?p7(ct1&cd' + cObj31.id + 'i0):c4(ct2&cd' + cObj31.id + 'i0):c5(ct2&cd' + cObj31.id + 'i0)[]i=' + cObj31.id + '';
                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj31.href + '">' + cObj31.text + '</a></li>';
                                                                                                                                                                                                                                                                        if( typeof (cObj31.children) != "undefined") {
                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                            for( c32 = 0; c32 < cObj31.children.length; c32++) {
                                                                                                                                                                                                                                                                                cObj32 = cObj31.children[c32];
                                                                                                                                                                                                                                                                                cObj32.href = '?p7(ct1&cd' + cObj32.id + 'i0):c4(ct2&cd' + cObj32.id + 'i0):c5(ct2&cd' + cObj32.id + 'i0)[]i=' + cObj32.id + '';
                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj32.href + '">' + cObj32.text + '</a></li>';
                                                                                                                                                                                                                                                                                if( typeof (cObj32.children) != "undefined") {
                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                    for( c33 = 0; c33 < cObj32.children.length; c33++) {
                                                                                                                                                                                                                                                                                        cObj33 = cObj32.children[c33];
                                                                                                                                                                                                                                                                                        cObj33.href = '?p7(ct1&cd' + cObj33.id + 'i0):c4(ct2&cd' + cObj33.id + 'i0):c5(ct2&cd' + cObj33.id + 'i0)[]i=' + cObj33.id + '';
                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj33.href + '">' + cObj33.text + '</a></li>';
                                                                                                                                                                                                                                                                                        if( typeof (cObj33.children) != "undefined") {
                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                            for( c34 = 0; c34 < cObj33.children.length; c34++) {
                                                                                                                                                                                                                                                                                                cObj34 = cObj33.children[c34];
                                                                                                                                                                                                                                                                                                cObj34.href = '?p7(ct1&cd' + cObj34.id + 'i0):c4(ct2&cd' + cObj34.id + 'i0):c5(ct2&cd' + cObj34.id + 'i0)[]i=' + cObj34.id + '';
                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj34.href + '">' + cObj34.text + '</a></li>';
                                                                                                                                                                                                                                                                                                if( typeof (cObj34.children) != "undefined") {
                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                    for( c35 = 0; c35 < cObj34.children.length; c35++) {
                                                                                                                                                                                                                                                                                                        cObj35 = cObj34.children[c35];
                                                                                                                                                                                                                                                                                                        cObj35.href = '?p7(ct1&cd' + cObj35.id + 'i0):c4(ct2&cd' + cObj35.id + 'i0):c5(ct2&cd' + cObj35.id + 'i0)[]i=' + cObj35.id + '';
                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj35.href + '">' + cObj35.text + '</a></li>';
                                                                                                                                                                                                                                                                                                        if( typeof (cObj35.children) != "undefined") {
                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                            for( c36 = 0; c36 < cObj35.children.length; c36++) {
                                                                                                                                                                                                                                                                                                                cObj36 = cObj35.children[c36];
                                                                                                                                                                                                                                                                                                                cObj36.href = '?p7(ct1&cd' + cObj36.id + 'i0):c4(ct2&cd' + cObj36.id + 'i0):c5(ct2&cd' + cObj36.id + 'i0)[]i=' + cObj36.id + '';
                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj36.href + '">' + cObj36.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                if( typeof (cObj36.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                    for( c37 = 0; c37 < cObj36.children.length; c37++) {
                                                                                                                                                                                                                                                                                                                        cObj37 = cObj36.children[c37];
                                                                                                                                                                                                                                                                                                                        cObj37.href = '?p7(ct1&cd' + cObj37.id + 'i0):c4(ct2&cd' + cObj37.id + 'i0):c5(ct2&cd' + cObj37.id + 'i0)[]i=' + cObj37.id + '';
                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj37.href + '">' + cObj37.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                        if( typeof (cObj37.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                            for( c38 = 0; c38 < cObj37.children.length; c38++) {
                                                                                                                                                                                                                                                                                                                                cObj38 = cObj37.children[c38];
                                                                                                                                                                                                                                                                                                                                cObj38.href = '?p7(ct1&cd' + cObj38.id + 'i0):c4(ct2&cd' + cObj38.id + 'i0):c5(ct2&cd' + cObj38.id + 'i0)[]i=' + cObj38.id + '';
                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj38.href + '">' + cObj38.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                if( typeof (cObj38.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                    for( c39 = 0; c39 < cObj38.children.length; c39++) {
                                                                                                                                                                                                                                                                                                                                        cObj39 = cObj38.children[c39];
                                                                                                                                                                                                                                                                                                                                        cObj39.href = '?p7(ct1&cd' + cObj39.id + 'i0):c4(ct2&cd' + cObj39.id + 'i0):c5(ct2&cd' + cObj39.id + 'i0)[]i=' + cObj39.id + '';
                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj39.href + '">' + cObj39.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj39.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                            for( c40 = 0; c40 < cObj39.children.length; c40++) {
                                                                                                                                                                                                                                                                                                                                                cObj40 = cObj39.children[c40];
                                                                                                                                                                                                                                                                                                                                                cObj40.href = '?p7(ct1&cd' + cObj40.id + 'i0):c4(ct2&cd' + cObj40.id + 'i0):c5(ct2&cd' + cObj40.id + 'i0)[]i=' + cObj40.id + '';
                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj40.href + '">' + cObj40.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                if( typeof (cObj40.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                    for( c41 = 0; c41 < cObj40.children.length; c41++) {
                                                                                                                                                                                                                                                                                                                                                        cObj41 = cObj40.children[c41];
                                                                                                                                                                                                                                                                                                                                                        cObj41.href = '?p7(ct1&cd' + cObj41.id + 'i0):c4(ct2&cd' + cObj41.id + 'i0):c5(ct2&cd' + cObj41.id + 'i0)[]i=' + cObj41.id + '';
                                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj41.href + '">' + cObj41.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj41.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                            for( c42 = 0; c42 < cObj41.children.length; c42++) {
                                                                                                                                                                                                                                                                                                                                                                cObj42 = cObj41.children[c42];
                                                                                                                                                                                                                                                                                                                                                                cObj42.href = '?p7(ct1&cd' + cObj42.id + 'i0):c4(ct2&cd' + cObj42.id + 'i0):c5(ct2&cd' + cObj42.id + 'i0)[]i=' + cObj42.id + '';
                                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj42.href + '">' + cObj42.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                if( typeof (cObj42.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                    for( c43 = 0; c43 < cObj42.children.length; c43++) {
                                                                                                                                                                                                                                                                                                                                                                        cObj43 = cObj42.children[c43];
                                                                                                                                                                                                                                                                                                                                                                        cObj43.href = '?p7(ct1&cd' + cObj43.id + 'i0):c4(ct2&cd' + cObj43.id + 'i0):c5(ct2&cd' + cObj43.id + 'i0)[]i=' + cObj43.id + '';
                                                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj43.href + '">' + cObj43.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj43.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                            for( c44 = 0; c44 < cObj43.children.length; c44++) {
                                                                                                                                                                                                                                                                                                                                                                                cObj44 = cObj43.children[c44];
                                                                                                                                                                                                                                                                                                                                                                                cObj44.href = '?p7(ct1&cd' + cObj44.id + 'i0):c4(ct2&cd' + cObj44.id + 'i0):c5(ct2&cd' + cObj44.id + 'i0)[]i=' + cObj44.id + '';
                                                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj44.href + '">' + cObj44.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                if( typeof (cObj44.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                    for( c45 = 0; c45 < cObj44.children.length; c45++) {
                                                                                                                                                                                                                                                                                                                                                                                        cObj45 = cObj44.children[c45];
                                                                                                                                                                                                                                                                                                                                                                                        cObj45.href = '?p7(ct1&cd' + cObj45.id + 'i0):c4(ct2&cd' + cObj45.id + 'i0):c5(ct2&cd' + cObj45.id + 'i0)[]i=' + cObj45.id + '';
                                                                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj45.href + '">' + cObj45.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj45.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                            for( c46 = 0; c46 < cObj45.children.length; c46++) {
                                                                                                                                                                                                                                                                                                                                                                                                cObj46 = cObj45.children[c46];
                                                                                                                                                                                                                                                                                                                                                                                                cObj46.href = '?p7(ct1&cd' + cObj46.id + 'i0):c4(ct2&cd' + cObj46.id + 'i0):c5(ct2&cd' + cObj46.id + 'i0)[]i=' + cObj46.id + '';
                                                                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj46.href + '">' + cObj46.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                                if( typeof (cObj46.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                                    for( c47 = 0; c47 < cObj46.children.length; c47++) {
                                                                                                                                                                                                                                                                                                                                                                                                        cObj47 = cObj46.children[c47];
                                                                                                                                                                                                                                                                                                                                                                                                        cObj47.href = '?p7(ct1&cd' + cObj47.id + 'i0):c4(ct2&cd' + cObj47.id + 'i0):c5(ct2&cd' + cObj47.id + 'i0)[]i=' + cObj47.id + '';
                                                                                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj47.href + '">' + cObj47.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj47.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                                            for( c48 = 0; c48 < cObj47.children.length; c48++) {
                                                                                                                                                                                                                                                                                                                                                                                                                cObj48 = cObj47.children[c48];
                                                                                                                                                                                                                                                                                                                                                                                                                cObj48.href = '?p7(ct1&cd' + cObj48.id + 'i0):c4(ct2&cd' + cObj48.id + 'i0):c5(ct2&cd' + cObj48.id + 'i0)[]i=' + cObj48.id + '';
                                                                                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj48.href + '">' + cObj48.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                                                if( typeof (cObj48.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                                                    html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                                                    for( c49 = 0; c49 < cObj48.children.length; c49++) {
                                                                                                                                                                                                                                                                                                                                                                                                                        cObj49 = cObj48.children[c49];
                                                                                                                                                                                                                                                                                                                                                                                                                        cObj49.href = '?p7(ct1&cd' + cObj49.id + 'i0):c4(ct2&cd' + cObj49.id + 'i0):c5(ct2&cd' + cObj49.id + 'i0)[]i=' + cObj49.id + '';
                                                                                                                                                                                                                                                                                                                                                                                                                        html += '<li><a href="' + cObj49.href + '">' + cObj49.text + '</a></li>';
                                                                                                                                                                                                                                                                                                                                                                                                                        if( typeof (cObj49.children) != "undefined") {
                                                                                                                                                                                                                                                                                                                                                                                                                            html += "<ul>";
                                                                                                                                                                                                                                                                                                                                                                                                                            for( c50 = 0; c50 < cObj49.children.length; c50++) {
                                                                                                                                                                                                                                                                                                                                                                                                                                cObj50 = cObj49.children[c50];
                                                                                                                                                                                                                                                                                                                                                                                                                                cObj50.href = '?p7(ct1&cd' + cObj50.id + 'i0):c4(ct2&cd' + cObj50.id + 'i0):c5(ct2&cd' + cObj50.id + 'i0)[]i=' + cObj50.id + '';
                                                                                                                                                                                                                                                                                                                                                                                                                                html += '<li><a href="' + cObj50.href + '">' + cObj50.text + '</a></li>';

                                                                                                                                                                                                                                                                                                                                                                                                                            }// for cObj50 children
                                                                                                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                                                        } // if cObj50 children

                                                                                                                                                                                                                                                                                                                                                                                                                    }// for cObj49 children
                                                                                                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                                                } // if cObj49 children

                                                                                                                                                                                                                                                                                                                                                                                                            }// for cObj48 children
                                                                                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                                        } // if cObj48 children

                                                                                                                                                                                                                                                                                                                                                                                                    }// for cObj47 children
                                                                                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                                } // if cObj47 children

                                                                                                                                                                                                                                                                                                                                                                                            }// for cObj46 children
                                                                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                        } // if cObj46 children

                                                                                                                                                                                                                                                                                                                                                                                    }// for cObj45 children
                                                                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                                } // if cObj45 children

                                                                                                                                                                                                                                                                                                                                                                            }// for cObj44 children
                                                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                        } // if cObj44 children

                                                                                                                                                                                                                                                                                                                                                                    }// for cObj43 children
                                                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                                } // if cObj43 children

                                                                                                                                                                                                                                                                                                                                                            }// for cObj42 children
                                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                        } // if cObj42 children

                                                                                                                                                                                                                                                                                                                                                    }// for cObj41 children
                                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                                } // if cObj41 children

                                                                                                                                                                                                                                                                                                                                            }// for cObj40 children
                                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                                        } // if cObj40 children

                                                                                                                                                                                                                                                                                                                                    }// for cObj39 children
                                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                                } // if cObj39 children

                                                                                                                                                                                                                                                                                                                            }// for cObj38 children
                                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                                        } // if cObj38 children

                                                                                                                                                                                                                                                                                                                    }// for cObj37 children
                                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                                } // if cObj37 children

                                                                                                                                                                                                                                                                                                            }// for cObj36 children
                                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                                        } // if cObj36 children

                                                                                                                                                                                                                                                                                                    }// for cObj35 children
                                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                                } // if cObj35 children

                                                                                                                                                                                                                                                                                            }// for cObj34 children
                                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                                        } // if cObj34 children

                                                                                                                                                                                                                                                                                    }// for cObj33 children
                                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                                } // if cObj33 children

                                                                                                                                                                                                                                                                            }// for cObj32 children
                                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                                        } // if cObj32 children

                                                                                                                                                                                                                                                                    }// for cObj31 children
                                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                                } // if cObj31 children

                                                                                                                                                                                                                                                            }// for cObj30 children
                                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                                        } // if cObj30 children

                                                                                                                                                                                                                                                    }// for cObj29 children
                                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                                } // if cObj29 children

                                                                                                                                                                                                                                            }// for cObj28 children
                                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                                        } // if cObj28 children

                                                                                                                                                                                                                                    }// for cObj27 children
                                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                                } // if cObj27 children

                                                                                                                                                                                                                            }// for cObj26 children
                                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                                        } // if cObj26 children

                                                                                                                                                                                                                    }// for cObj25 children
                                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                                } // if cObj25 children

                                                                                                                                                                                                            }// for cObj24 children
                                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                                        } // if cObj24 children

                                                                                                                                                                                                    }// for cObj23 children
                                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                                } // if cObj23 children

                                                                                                                                                                                            }// for cObj22 children
                                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                                        } // if cObj22 children

                                                                                                                                                                                    }// for cObj21 children
                                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                                } // if cObj21 children

                                                                                                                                                                            }// for cObj20 children
                                                                                                                                                                            html += "</ul>";
                                                                                                                                                                        } // if cObj20 children

                                                                                                                                                                    }// for cObj19 children
                                                                                                                                                                    html += "</ul>";
                                                                                                                                                                } // if cObj19 children

                                                                                                                                                            }// for cObj18 children
                                                                                                                                                            html += "</ul>";
                                                                                                                                                        } // if cObj18 children

                                                                                                                                                    }// for cObj17 children
                                                                                                                                                    html += "</ul>";
                                                                                                                                                } // if cObj17 children

                                                                                                                                            }// for cObj16 children
                                                                                                                                            html += "</ul>";
                                                                                                                                        } // if cObj16 children

                                                                                                                                    }// for cObj15 children
                                                                                                                                    html += "</ul>";
                                                                                                                                } // if cObj15 children

                                                                                                                            }// for cObj14 children
                                                                                                                            html += "</ul>";
                                                                                                                        } // if cObj14 children

                                                                                                                    }// for cObj13 children
                                                                                                                    html += "</ul>";
                                                                                                                } // if cObj13 children

                                                                                                            }// for cObj12 children
                                                                                                            html += "</ul>";
                                                                                                        } // if cObj12 children

                                                                                                    }// for cObj11 children
                                                                                                    html += "</ul>";
                                                                                                } // if cObj11 children

                                                                                            }// for cObj10 children
                                                                                            html += "</ul>";
                                                                                        } // if cObj10 children

                                                                                    }// for cObj9 children
                                                                                    html += "</ul>";
                                                                                } // if cObj9 children

                                                                            }// for cObj8 children
                                                                            html += "</ul>";
                                                                        } // if cObj8 children

                                                                    }// for cObj7 children
                                                                    html += "</ul>";
                                                                } // if cObj7 children

                                                            }// for cObj6 children
                                                            html += "</ul>";
                                                        } // if cObj6 children
*/
                                                    }// for cObj5 children
                                                    html += '</ul><div class=\"btm-bg\"></div></div>';
                                                } // if cObj5 children
                                                html += "</li>";
                                            }// for cObj4 children
                                            html += '</ul><div class=\"btm-bg\"></div></div>';
                                        } // if cObj4 children
                                        html += "</li>";
                                    }// for cObj3 children
                                    html += '</ul>';
                                } // if cObj3 children
                                html += "</li>";
                            }// for cObj2 children
                            html += '</ul>';
                        } // if cObj2 children
                        html += "</div></a></li>";
                    }// for cObj1 children
                } // if cObj1 children

            }// close this.list for loop
            html += '</ul></div>';
            html += '<div class="nav-right"></div></div></div>';
            el.append(html);
        } // len > 0
    }// function

    c.RenderToutArea = function(elementId) {
        /* var el = $(elementId);
         if (el.length == 0) return;
         var html = "", i = 0, subNavHtml = "", j = 0, k = 0, s = "";
         var cat, ccat, gccat;
         // only render the product level categories at level 1
         cat = this.Find(3);
         if (cat == null) return;
         if (typeof (cat.children) != "undefined") {
         for (i = 0; i < cat.children.length; i++) {
         ccat = cat.children[i];
         if (ccat.level != 1) continue;
         html += '<div class="csp-box">';
         if (ccat.displayOrder.indexOf('Missing') == -1) {
         html += '<a href="' + ccat.href + '" cspobj="REPORT" csptype="LINKS" cspenglishvalue="' + ccat.rid + '">';
         html += '<h2>' + ccat.text + '</h2>';
         html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';
         html += '<p>' + ccat.desc + '</p>';
         html += '</a>';
         } else {
         html += '<h2 style=\"color: #5F798F;text-decoration: none;\">' + ccat.text + '</h2>';
         html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';
         html += '<p>' + ccat.desc + '</p>';
         }
         html += '</div>';
         }
         }
         el.append(html);*/
    }
    c.RenderTitle = function(elId, catId, level, pn) {
        var title = 'The Hartford', titleOnlySW = false;
        if (level == 1) {
            titleOnlySW = true;
        }
        var el = $(elId), html = "", reportingItem = '';
        if(pn) {
            var item = this.FindByAttr("pn", pn);
            if(item.length > 0) {
                el.append('<span class="csp-title" style="display:' + (!titleOnlySW ? "inline" : "none") + '">' + item[0].text + '</span>');
                //el.append('<span cspObj="REPORT" cspType="TITLE" style="display:none">' + item[0].text + '</span>');
                reportingItem = '<span cspObj="REPORT" cspType="TITLE" style="display:none">' + item[0].text + '</span>';
                title += ' - ' + item[0].text;
            }
            return;
        }
        var cats = this.FindAll(catId, level);
        if(el.length == 0 || cats.length == 0)
            return;
        for(var i = 0; i < cats.length; i++) {
            html = '<span class="csp-title" style="display:' + (!titleOnlySW ? "inline" : "none") + '">' + cats[i].text + '</span>';
            reportingItem = '<span cspObj="REPORT" cspType="TITLE" style="display:none">' + cats[i].text + '</span>';
            title += ' - ' + cats[i].text;
            break;
        }
        if(html != "") {
            el.append(html);
            el.append(reportingItem);
        }
        document.title = title;
        return;
    }
    c.RenderBreadcrumb = function(elId, catId, level) {
        if (catId != 13 && level != 1) {
            var el = $(elId), cats = this.FindAll(catId), html = "", p = null;
            if(el.length == 0 || cats.length == 0)
                return;
            for(var i = 0; i < cats.length; i++) {
                if(cats[i].level == level) {
                    if(level == 3) {
                        // look for the level 2 in the same category
                        var lvl2Cat = this.FindAll(catId, 2);
                        if(lvl2Cat.length > 0)
                            html = '<a href="' + ( typeof (lvl2Cat[0].href) != "undefined" ? lvl2Cat[0].href : "javascript:void(0);") + '">' + lvl2Cat[0].text + '</a>  &raquo;  ' + html;
                    } else if(level == 0) {
                        html = '<a href="' + ( typeof (cats[i].href) != "undefined" ? cats[i].href : "javascript:void(0);") + '">' + cats[i].text + '</a>  &raquo;  ' + html;
                    }
                    p = cats[i].parent;
                    while(p != null) {
                        p.href = p.href.replace(/cd12i0/gi, 'cd13i0').replace(/cat=12/, 'cat=13');
                        p.text = p.text.replace('Small Commercial Microsites', 'Home');
                        if (p.text != "Home") {
                            html = '<a href="' + ( typeof (p.href) != "undefined" ? p.href : "javascript:void(0);") + '">' + p.text + '</a>  &raquo;  ' + html;
                        }
                        p = p.parent;
                    }
                    break;
                }
            }
            /*
            if(html != "") {
                el.append(html);
                $('#csp-breadcrumb').css('display', 'block');
            }
             */
   
        }
        return;
    }

})(categories);
function setAnchorsValidHREFs() {
    $('a').each(function(index) {
        if ($(this).attr("cspvalue") != undefined && $(this).attr("href") == "#") {
            $(this).attr("href", getHREF($(this).attr("cspvalue")));
            if (typeof console != 'undefined' && console) {
                console.log("Replaced this link: ", $(this).attr("cspvalue"), "\nwith this href: ", getHREF($(this).attr("cspvalue")));
            }
        }
        if ($(this).attr("cspobj") != undefined && $(this).attr("cspobj") == "CSCLT") {
            $(this).attr("cspobj", "REPORT");
            $(this).attr("cspType", "LINKS");
        }
        if ($(this).attr("href") != undefined) {
            if ($(this).attr("href").indexOf(".zip") != -1 || $(this).attr("href").indexOf(".pdf") != -1) {
                $(this).attr("cspobj", "REPORT");
                $(this).attr("cspType", "DOWNLOADS"); 
                $(this).attr("cspEnglishValue", getHrefFileName($(this).attr("href")));
            }
        }
    });
}
function getHREF(v) {
    var r = "#", c = categories.FindByAttr("pn", v);
    if (c && c.length > 0) {
        r = c[0].href;
    } 
    return r;
}
function getHrefFileName (a) {
    var r = "", aa = a.split("/");
    if (aa && aa.length > 0) {
        r = aa[aa.length - 1];
    }
    return r;
}
var _tag;
function initializeWebtrends() {
    _tag= new WebTrends();
    _tag.dcsGetId();
    //var cspConsumerInfo = { "companies_Id": "{consumer:fConsumerID}", "companyname": "{consumer:fcompanyname}", "country": "{consumer:fcountry}", "lId": "{FCTLanguageID}", "sId": "{supplier:fConsumerID}", "sName": "{supplier:fcompanyname}", "lng": "{FCTLanguageCode}", "lngDesc": "{FCTLanguageDescription}" };
    if (typeof cspConsumerInfo != 'undefined' && cspConsumerInfo) {
        _tag.DCSext.language = cspConsumerInfo.lngDesc;
        _tag.DCSext.ConsumerType= cspConsumerInfo.consumerType;
    }
    _tag.DCSext.csp_stype = _tag.DCSext.csp_sname = "showcase";
    var script   = document.createElement("script");
    script.type  = "text/javascript";
    script.src   = "js/CspReportLib.js";
    document.body.appendChild(script);
}
function checkURL() {
    var d = document.domain;
    if (d.indexOf("i99") != -1) {
        $("#csp-agencyBar").css("display", "block");
        $("#csp-cobrandLogo").css("display", "block");
    }
}
function checkCobrandLogo() {
    if ($("#csp-cobrandLogo").css("display") == "block") {
        $("#csp-pageNav").css("margin-top", "-20px");
    }
}
