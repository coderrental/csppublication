export const environment = {
    production: true,
    projectName: 'parkertest',
    projectMode: 'prod',
    stage: 'test',
    hostUrl: 'https://global.syndication.tiekinetix.net/DgContentApi/api/parkertest/prod/',
    cacheApiUrl: '',
    wsCacheApiUrl: 'https://global.syndication.tiekinetix.net/WsCacheApi/',
    rootId: 6574,
    leadgenUrl: 'd1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]',
    saveFormUrl: '/saveform',
    submitFormUrl: '/emailForm?pid=9999'
  };
  