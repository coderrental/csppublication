export const environment = {
  production: true,
  projectName: 'parker',
  projectMode: 'prod',
  stage: 'prod',
  hostUrl: 'https://global.syndication.tiekinetix.net/DgContentApi/api/parker/prod/',
  cacheApiUrl: 'https://global.syndication.tiekinetix.net/DgCacheApi/',
  wsCacheApiUrl: 'https://global.syndication.tiekinetix.net/WsCacheApi/',
  rootId: 181,
  /*hostUrl: 'http://global.syndication.tiekinetix.net/DgContentApi/api/parkertest/prod/',
  rootId: 6574,*/
  leadgenUrl: '/d1.aspx?p2006(ct15000)[st(ct15000*Status_Sort_Order*)]',
  saveFormUrl: '/saveform',
  submitFormUrl: '/emailForm?pid=9999'
};
