import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { CategoriesService, Category } from '../categories/categories.service';

import 'rxjs/add/operator/switchMap';
import { element } from 'protractor';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'app-product-category',
	templateUrl: './product-category.component.html',
	styleUrls: ['./product-category.component.css']
})
export class ProductCategoryComponent implements OnInit {
	@Output() outputEvent = new EventEmitter();
	categoryId: number;
	categories: Category[];
	subItems: Category[] = []; 
	subCategories: Category[] = [];
	prodLoading: boolean = true;
	
	constructor(private categoryService: CategoriesService,
				private route: ActivatedRoute,
				private location: Location) { }

	ngOnInit() {
		this.route.params
			.switchMap((params: Params) => {
				this.categoryId = +params['id'];
				this.subItems = [];
				this.subCategories = [];
				this.prodLoading = true;
				//return this.categoryService.getCategories(false, this.categoryId);
				return this.categoryService.getCacheCategories(this.categoryId);
			})
			.subscribe(categories => {
				if (categories.length > 0) {
					this.loadedCategories(categories);
				}
				else {
					this.categoryService.getCategories(false, this.categoryId).subscribe(x => this.loadedCategories(x));
				}
			});
	}
	
	loadedCategories (categories): void {
		this.subItems = categories;

		if (categories.length == 0) {
			this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000})
								.subscribe(x => {
									let allProductSeries: Category[] = [];
									allProductSeries.push(x.results[0]);
									this.outputEvent.emit(allProductSeries);
								});
			return;
		}
		this.loadLeafCountFromCache();
		

		/* let loadProductSeriesRequests = [];

		let allProductSeries: number[] = [];
		this.subItems.forEach(x => {
			loadProductSeriesRequests.push(this.categoryService.getCategoryLeafNodesIds({parentId: x.id}));
		});

		Observable.forkJoin(loadProductSeriesRequests).subscribe(results => {
			for (var i = results.length - 1; i >= 0 ; i--) {
				let tmpCategoryList: number[] = <number[]>results[i];
				this.subItems[i].prodCount = tmpCategoryList.length;
				if (tmpCategoryList.length == 1 && tmpCategoryList[0] == this.subItems[i].id) {
					this.subItems.splice(i, 1);
					//continue;
				}				

				allProductSeries = allProductSeries.concat(tmpCategoryList);
			}
			this.subItems.sort(function(a, b) {
				return b.prodCount - a.prodCount;
			});
			this.subCategories = this.subItems;
			if (allProductSeries.length > 0)
				this.outputEvent.emit({categoryList: allProductSeries, firstLoad: false});
		}); */
		
	}

	loadLeafCountFromCache(): void {
		let loadProductSeriesRequests = [];
		
		let allProductSeries: number[] = [];
		this.subItems.forEach(x => {
			loadProductSeriesRequests.push(this.categoryService.getCacheCategoryLeafNodesIds({parentId: x.id}));
		});

		Observable.forkJoin(loadProductSeriesRequests).subscribe(results => {
			
			for (var i = results.length - 1; i >= 0 ; i--) {
				if (results[i] == null) {
					this.subItems.splice(i, 1);
					continue;
				}
				let tmpCategoryList: number[] = <number[]>results[i];
				if (tmpCategoryList.length == 0) continue;
				this.subItems[i].prodCount = tmpCategoryList.length;
				if (tmpCategoryList.length == 1 && tmpCategoryList[0] == this.subItems[i].id) {
					this.subItems.splice(i, 1);
					//continue;
				}				

				allProductSeries = allProductSeries.concat(tmpCategoryList);
			}
			this.loadLeafCountFromProd();
			if (allProductSeries.length == 0) return;

			this.subItems.sort(function(a, b) {
				if (typeof(a.prodCount) == 'undefined' || typeof(b.prodCount) == 'undefined') return 0;
				// 1st: based on count number, 2nd: abc
				return b.prodCount - a.prodCount || a.valueText.localeCompare(b.valueText);
			});
			this.subCategories = this.subItems;
			this.prodLoading = false;
			if (allProductSeries.length > 0)
				this.outputEvent.emit({categoryList: allProductSeries, firstLoad: false});
		});
	}

	loadLeafCountFromProd(): void {
		let loadProductSeriesRequests = [];
		
		let allProductSeries: number[] = [];
		this.subItems.forEach(x => {
			if (typeof(this.subItems[0].prodCount) == 'undefined')
				loadProductSeriesRequests.push(this.categoryService.getCategoryLeafNodesIds({parentId: x.id}));
		});

		if(loadProductSeriesRequests.length == 0) return;

		Observable.forkJoin(loadProductSeriesRequests).subscribe(results => {
			for (var i = results.length - 1; i >= 0 ; i--) {
				if (results[i] == null) {
					this.subItems.splice(i, 1);
					continue;
				}
				let tmpCategoryList: number[] = <number[]>results[i];
				this.subItems[i].prodCount = tmpCategoryList.length;
				if (tmpCategoryList.length == 1 && tmpCategoryList[0] == this.subItems[i].id) {
					this.subItems.splice(i, 1);
					//continue;
				}				

				allProductSeries = allProductSeries.concat(tmpCategoryList);
			}
			this.subItems.sort(function(a, b) {
				// 1st: based on count number, 2nd: abc
				return b.prodCount - a.prodCount || a.valueText.localeCompare(b.valueText);
			});
			this.subCategories = this.subItems;
			this.prodLoading = false;
			if (allProductSeries.length > 0)
				this.outputEvent.emit({categoryList: allProductSeries, firstLoad: false});
		});
	}

	countProducts (category): number {
		return 0;
		/* var totalProd:Array<number> = [0];
		this.countLeafNodes(this.categories.filter(item => item.parentId == category.id), totalProd);
		return totalProd[0]; */
		/*
		var totalProd:Array<number> = [category.count];
		this.getTotalCount(totalProd, this.categories.filter(item => item.parentId == category.id));

		return totalProd[0];
		*/
	}

	private countLeafNodes (categoryList: Category[], currentCount: number[]) {
		categoryList.forEach(item => {
			var validChildren = this.categories.filter(category => category.parentId == item.id);
			if (validChildren.length == 0)
				currentCount[0] += 1;
			else {
				this.countLeafNodes (validChildren, currentCount);
			}
		});
	}

	private getTotalCount(countList: number[], categoryList: Category[]){
		categoryList.forEach(item => {
			countList[0] += item.count;
			this.getTotalCount(countList, this.categories.filter(x => x.parentId == item.id));
		});
	}
}
