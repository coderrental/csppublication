import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { APP_BASE_HREF, Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { FacetComponent } from './facet/facet.component';
import { ProductListComponent } from './product-list/product-list.component';
import { GroupfacetPipe } from './facet/groupfacet.pipe';
import { MainNavComponent } from './main-nav/main-nav.component';
import { CategoriesService } from './categories/categories.service';
import { Category } from './categories/categories.service';
import { ConfiguratorComponent } from './configurator/configurator.component';
import { AppRoutingModule }     from './app-routing/app-routing.module';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { ProductService } from './product-list/product.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { SpinnerComponentModule } from 'ng2-component-spinner';
import { CompareComponent } from './compare/compare.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { HomeComponent } from './home/home.component';
import { FacetService } from './facet/facet.service';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CommonService } from './common/common.service';

import * as $ from 'jquery';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(httpClient: HttpClient) {
    var localizePath = '/customer-resources/showcase/custom/hannifin-configuration/assets/i18n/';
    if (window.location.host.indexOf('localhost') != -1)
      localizePath = '/assets/i18n/';
    return new TranslateHttpLoader(httpClient, localizePath, '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    FacetComponent,
    ProductListComponent,
    GroupfacetPipe,
    MainNavComponent,
    ConfiguratorComponent,
    ProductCategoryComponent,
    CompareComponent,
    ProductDetailComponent,
    HomeComponent,
    ContactUsComponent,
    BreadcrumbComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule,
    NgxPaginationModule,
    SpinnerComponentModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    CategoriesService, 
    ProductService,
    FacetService,
    CommonService,
    {provide: APP_BASE_HREF, useValue: '/d1.aspx?p2000()[]'}
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
