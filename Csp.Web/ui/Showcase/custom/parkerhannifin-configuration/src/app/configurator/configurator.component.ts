import { Component, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Facet } from '../facet/facet.component';
import { CategoriesService, Category } from '../categories/categories.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from '../common/common.service';

@Component({
  selector: 'app-configurator',
  templateUrl: './configurator.component.html',
  styleUrls: ['./configurator.component.css']
})
export class ConfiguratorComponent implements OnInit, AfterViewChecked {
	d2wayBinding = [];
	facetProdKeys = [];
	facetCheckedList: Facet[] = [];
	facetFromProduct: Facet[] = [];
	getArrayUnique = require('array-unique');
	categorySeriesList: number[] = [];
	firstLoadCategorySeries: number[] = [];

	categoryId: number;
	currentCategory: Category;

	tooltipHidden = true;
	isReported: boolean = false;
	
	constructor(private route: ActivatedRoute,
              private router: Router,
			  private categoryService: CategoriesService,
				private commonService: CommonService) { 
		this.currentCategory = new Category();

		this.route.params.subscribe(x => {
			this.categoryService.getCategorySeries({categoryIds: [+x.id], contentTypeId: 31000}).subscribe(categoryItem => this.renderPage(categoryItem));
		});
	}

	ngOnInit() {
		this.route.params
			.switchMap((params: Params) => {
				this.categoryId = +params['id'];
				this.categorySeriesList = [];
				this.firstLoadCategorySeries = [];
				this.facetProdKeys = [];
				this.facetCheckedList = [];
				this.facetFromProduct = [];
				this.isReported = false;
				return this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000});
		})
      	.subscribe(x => this.renderPage(x));
	}

	renderPage(pageData: any): void {
		if (pageData == null) {
			this.router.navigate(['']);
			return;
		}
		this.currentCategory = pageData.results[0];
	}
	
	onFacetChange(facetUpdates) {
		this.facetCheckedList = facetUpdates.selectedFacets.length == 0 ? [] : facetUpdates.selectedFacets.slice();
		this.facetProdKeys = this.getArrayUnique(facetUpdates.prodKeys);
	}

	onProdFacetChange(facetUpdates){
		this.facetFromProduct = facetUpdates.slice();
	}

	onProductCategoryChange(categoryUpdate) {
		this.categorySeriesList = categoryUpdate.categoryList;
		/* if (categoryUpdate.firstLoad == false)
			this.categorySeriesList = categoryUpdate.categoryList;
		else
			this.firstLoadCategorySeries = categoryUpdate.categoryList; *///.slice(0, 20);
		//console.log(this.firstLoadCategorySeries);
	}

	clearFacet(): void {
		this.facetProdKeys = [];
		this.facetCheckedList = [];
		this.facetFromProduct = [];
	}

	ngAfterViewChecked(): void {
		
		if (typeof(this.currentCategory) != 'undefined' && this.currentCategory.valueText != '' && !this.isReported) {
			this.isReported = true;
			setTimeout(() => {
				this.commonService.initCspReport();
			}, 1000);
			
		}
	}
}
