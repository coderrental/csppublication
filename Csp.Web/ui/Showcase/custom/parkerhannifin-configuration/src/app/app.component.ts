import { Component } from '@angular/core';
import { CategoriesService } 		from './categories/categories.service';
import { Category } 		from './categories/categories.service';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from './common/common.service';

declare var cspConsumerInfo: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CategoriesService]
})
export class AppComponent {
  
  constructor(private translate: TranslateService, private commonService: CommonService) {
    //console.log(cspConsumerInfo['lCode'].toLowerCase());
    translate.addLangs(["en", "de",'zh']);
    translate.setDefaultLang(cspConsumerInfo['lCode'].toLowerCase());
    //translate.setDefaultLang('en');
  }
 
  switchLanguage(language: string) {
    this.translate.use(language);
  }
}

