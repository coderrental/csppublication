import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { ProductService, ProductModel } from '../product-list/product.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CategoriesService, Category } from '../categories/categories.service';
import { FacetService } from '../facet/facet.service';
import { Facet } from '../facet/facet.component';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CommonService } from '../common/common.service';

declare var jquery:any;
declare var $ :any;
declare var cspConsumerInfo: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit, AfterViewChecked {
  @ViewChild('assetLightbox') assetLightboxHandler:ElementRef;
  @ViewChild('prodDetailDiv') myDiv: ElementRef;
  categoryId: number;
  prodId: string;
  productDetailItem: ProductModel;
  assetList: ProductModel[] = [];
  productBanner: string;
  itemTitle: string; itemThumbnail: string; itemImage: string; itemDescShort: string; itemDescLong: string; productPartNo: string = ''; categoryPartNo: string = '';
  activeAssetUrl: string = ''; catProperties: string = ''; cspUniqueKey: string = '';
  allFacets: Facet[]; currentFacets: Facet[]; configuredFacets: Facet[] = [];
  prodLoading: boolean = true;
  attrLoading: boolean = false;
  // facetEmpty: boolean = false;
  productOverViewHtml: string = "";
  isOnlyProduct: boolean = false;
  productSeriesTechSpec: Facet[] = [];
  currentProductSeries: Category;
  prodCount$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  pConfigFromCache: boolean = false;
  pConfigFromProd: boolean = false;
  noPartConfigurator: boolean = false;

  getArrayUnique = require('array-unique');
	getArrayUnion = require('array-union');
  getIntersection = require('array-intersection');
  isReported: boolean = false;
  assetLoaded: boolean = false;
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private categoryService: CategoriesService,
              private facetService: FacetService,
              private productService: ProductService, 
              private commonService: CommonService) {
    this.productDetailItem = new ProductModel();
    this.currentProductSeries = null;
  }

  ngOnInit() {
    this.route.params
			.switchMap((params: Params) => {
        this.categoryId = +params['categoryid'];
        this.prodId = params['productid'];
        this.productSeriesTechSpec = [];
        this.currentProductSeries = null;
        this.allFacets = this.currentFacets = [];
        this.attrLoading = this.prodId == undefined ? true : false;
        this.prodLoading = true;
        // this.facetEmpty = false;
        this.isReported = false;
        this.assetLoaded = false;
        
        if (this.prodId == undefined) {
          this.noPartConfigurator = false;
          this.configuredFacets = [];
          this.loadProductSeriesTechSpec();
          this.categoryService.getProductCount(this.categoryId, null, 31000, 51000).subscribe(
            x => {
              this.prodCount$.next(x);
              if (x == 1) {
                this.isOnlyProduct = true;
                this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 51000}).subscribe(x =>{
                      var tmpPartNo = x.results[0].categoryProperties.filter(x => x.fieldName == 'Product_Number_MFPN');
                      this.productPartNo =  typeof(tmpPartNo[0]) != 'undefined' && tmpPartNo[0].valueText != '' ? tmpPartNo[0].valueText : '';
                });
              }
            }
          );

          this.facetService.getCacheProdDetailFacets({ParentId: this.categoryId, ContentTypeId: 51000, FacetClass: 'PartConfigurator'})
          .subscribe(x => {
            this.pConfigFromCache = true;
            if (x.length > 0 || this.pConfigFromProd) {
              this.attrLoading = false;
            }
            this.loadConfiguration(x);
          });

          this.facetService
          .getFacets({categoryId: this.categoryId, contentTypeId: 51000, facetClass: 'PartConfigurator'})
          .then(facetData => {
            this.pConfigFromProd = true;
            if (facetData.length > 0 || this.pConfigFromCache) {
              this.attrLoading = false;
            }
            this.loadConfiguration(facetData);
          });

          this.facetService.getCacheProdDetailFacets({ParentId: this.categoryId, ContentTypeId: 51000, FacetClass: 'Configurator'})
          .subscribe(x => {
            var tmp = x;
          });

          this.categoryService.getCacheCategorySeries(this.categoryId, 31000).subscribe(x => this.renderPage(x));

          return this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000});
        }

        this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000}).subscribe(x => {
          if (x == null)
           this.router.navigate(['']);
        });

        this.loadCachedTabsContent();
        
        //setTimeout(() => {this.loadCachedTabsContent();}, 1000);

        this.facetService
              .getFacets({productKeys: [this.prodId], categoryIds: [this.categoryId], facetClass: 'Configurator'})
              .then(facetData => {
                this.pConfigFromProd = true;
                if (facetData.length > 0 || this.pConfigFromCache)
                  this.attrLoading = false;
                this.loadConfiguration(facetData);
              });
              
        this.facetService
              .getFacets({productKeys: [this.prodId], categoryIds: [this.categoryId], facetClass: 'PartConfigurator'})
              .then(facetData => {
                if (!this.pConfigFromCache || !this.pConfigFromProd)
                  this.attrLoading = true;

                this.prodLoading = false;
                if (facetData.length == 0) return;
                facetData.sort(function(a, b) {
                  if (parseInt(a.sequence) > parseInt(b.sequence))
                    return 1;
                  if (parseInt(a.sequence) < parseInt(b.sequence))
                    return -1;
            
                  if ( a.facetName < b.facetName )
                    return -1;
                  if ( a.facetName > b.facetName )
                      return 1;

                  return 0;
                });
                this.allFacets = this.currentFacets = facetData;
              });

        return this.productService.getProduct(this.prodId);
			})
      .subscribe(x => this.renderPage(x));
  }

  renderPage(pageData: any): void {
    if (this.prodId ==  undefined && pageData == null) {
      this.router.navigate(['']);
      return;
    }
    if (pageData.length == 0) return;
    if (typeof(pageData.results) != 'undefined') {
      if (this.currentProductSeries != null) return;
      
      var itemData = pageData.results[0];
      this.currentProductSeries = itemData;
      this.itemTitle = itemData.valueText;
      this.itemThumbnail = itemData.thumbnailImage;
      this.itemImage = itemData.image;
      this.itemDescLong = itemData.longDescription;
      this.itemDescShort = itemData.shortDescription;

      var catPropertiesField = itemData.categoryProperties.filter(x => x.fieldName == 'Content_Fetch_1');
      this.catProperties = catPropertiesField[0] != undefined ? catPropertiesField[0].valueText : '';

      var cspUniqueKeyField = itemData.categoryProperties.filter(x => x.fieldName == 'CSP_Unique_Key');
      this.cspUniqueKey = cspUniqueKeyField[0] != undefined ? cspUniqueKeyField[0].valueText : '';

      this.productOverViewHtml = itemData.longDescription;
      this.productService
        .getProducts({categoryId: this.categoryId, contentTypeId: 21000, paging: false})
        .then(x => this.loadAssets(x));
    }
    else {
      this.itemTitle = pageData.title;
      this.itemThumbnail = pageData.thumbnailImage;

      //Prod part number
      var tmpPartNo = pageData.productProperties.filter(x => x.fieldName == 'Product_Number_MFPN');
      this.productPartNo =  typeof(tmpPartNo[0]) != 'undefined' && tmpPartNo[0].valueText != '' ? tmpPartNo[0].valueText : ''; 
      
      //Prod image
      var tmpImage = pageData.productProperties.filter(x => x.fieldName == 'File_Image_Url');
      this.itemImage = tmpImage[0] != undefined ? tmpImage[0].valueText : '';

      //Neutral lang text
      var tmpDescShort:any = '';
      var tmpDescLong:any = '';
      if (typeof(cspConsumerInfo['lCode']) == 'undefined') {
        tmpDescShort = pageData.productProperties.filter(x => x.fieldName == 'Content_Description_Short' + cspConsumerInfo['lCode']);
        tmpDescLong = pageData.productProperties.filter(x => x.fieldName == 'Content_Description_Long' + cspConsumerInfo['lCode']);
      }
      this.itemDescLong = typeof(tmpDescLong[0]) != 'undefined' && tmpDescLong[0].valueText != '' ? tmpDescLong[0].valueText : pageData.longDescription;
      this.itemDescShort = typeof(tmpDescShort[0]) != 'undefined' && tmpDescShort[0].valueText != '' ? tmpDescShort[0].valueText : pageData.shortDescription;
      //this.productOverViewHtml = this.itemDescLong;

      if (this.itemDescShort == "" || this.itemDescLong == "") {
        this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000}).subscribe(x => {
          var itemData = x.results[0];
          this.itemDescLong = itemData.longDescription;
          this.itemDescShort = itemData.shortDescription;
          this.productOverViewHtml = this.itemDescLong;
        });
      }
      if(this.itemTitle == null || this.itemTitle == ""){
        this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000}).subscribe(x => {
          var itemData = x.results[0];
          var itemTitle = itemData.categoryProperties.filter( x => x.fieldName == 'Content_Title');
          this.itemTitle = itemTitle[0] != undefined ? itemTitle[0].valueText : '';
        });
      }
      
      this.productService
      .getProductAssets({productKeys: [this.prodId], contentTypeId: 21000, paging: false})
      .then(x => this.loadAssets(x));
    }
  }

  loadCachedTabsContent(): void {
    this.facetService.getCacheProdDetailFacets({ParentId: this.categoryId, ContentTypeId: 51000, FacetClass: 'Configurator'})
    .subscribe(x => {
      this.pConfigFromCache = true;
      if (x.length > 0 || this.pConfigFromProd == true)
        this.attrLoading = false;
      if (x.length == 0) return;
      var prodConfig = x.filter(facet => facet.productKeys.indexOf(this.prodId) > -1);
      this.loadConfiguration(prodConfig);
    });
    
    this.facetService.getCacheProdDetailFacets({ParentId: this.categoryId, ContentTypeId: 51000, FacetClass: 'PartConfigurator'})
    .subscribe(x => {
      if (x.length == 0) return;
      this.prodLoading = false;
      var facetData = x.filter(f => f.productKeys.indexOf(this.prodId) > -1);
      if (facetData.length == 0) return;
      this.pConfigFromProd = true;
      this.prodLoading = false;
      facetData.sort(function(a, b) {
        if (parseInt(a.sequence) > parseInt(b.sequence))
          return 1;
        if (parseInt(a.sequence) < parseInt(b.sequence))
          return -1;
  
        if ( a.facetName < b.facetName )
          return -1;
        if ( a.facetName > b.facetName )
            return 1;

        return 0;
      });
      this.allFacets = this.currentFacets = facetData;
    });
  }

  loadProductSeriesTechSpec (): void {
    this.productSeriesTechSpec = [];
    this.facetService.getCacheProdSeriesFacets({ParentId: this.categoryId, ContentTypeId: 31000, FacetClass: 'Compare'})
    .subscribe(x => {
      this.productSeriesTechSpec = x;
      this.sortTechSpec();
    });
    
    this.facetService
    .getFacets({contentTypeId: 31000, facetClass: 'Compare', categoryIds: [this.categoryId]})
    .then(x => {
      this.productSeriesTechSpec = x;
      this.sortTechSpec();
    })
    ;
  }

  sortTechSpec(): void {
    this.productSeriesTechSpec.sort(function(a, b) {
			if (parseInt(a.sequence) > parseInt(b.sequence))
        return 1;
      if (parseInt(a.sequence) < parseInt(b.sequence))
        return -1;

      if ( a.facetName < b.facetName )
        return -1;
      if ( a.facetName > b.facetName )
          return 1;

      return 0;
		});
  }

  gotoFirstProduct(): void {
    this.productService
        .getProducts({categoryId: this.categoryId, languageId: parseInt(cspConsumerInfo.lId), contentTypeId: 51000, paging: false })
        .then (x => {
          var cspUniqueKey = x[0].productProperties.filter(field => field['fieldName'] == 'CSP_Unique_Key');
          this.router.navigate([`/categoryseries/${this.categoryId}/product/${cspUniqueKey[0]['valueText']}`]);
        });
  }

  loadAssets(assetData: any): void {
    this.assetList = assetData;
    this.assetLoaded = true;
    setTimeout(() => {
      //this.commonService.initCspReport();
      let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
      el.click();
    }, 1000);
  }

  getAssetDownloadTitle(assetItem: ProductModel): string {
    if (typeof(assetItem.title) != 'undefined' && assetItem.title != null) return assetItem.title;
    
    var downloadTitle = assetItem.productProperties.filter(x => x.fieldName == 'Content_Download_Title');
    return typeof(downloadTitle[0]) != 'undefined' && typeof(downloadTitle[0].valueText) != 'undefined' ? downloadTitle[0].valueText : 'No Title';
  }

  renderAsset(assetItem: ProductModel): void {
    
    var assetLink = assetItem.productProperties.filter(x => x.fieldName == 'Content_Link_Url');
    if (assetLink.length == 0) return;
    this.activeAssetUrl = assetLink[0].valueText;
    //this.assetLightboxHandler.nativeElement.click();
    var win = window.open(this.activeAssetUrl, '_blank');
    win.focus();
  }

  getExtensionAsset(assetItem: ProductModel): string {
    var assetLink = assetItem.productProperties.filter(x => x.fieldName == 'Content_Link_Url');
    if (assetLink.length == 0) return "";
    this.activeAssetUrl = assetLink[0].valueText;
    var ext = this.activeAssetUrl.split('.').pop();
    return "pdp-li-" + ext ;
  }

  getAssetUrl(assetItem: ProductModel): string {
    var assetLink = assetItem.productProperties.filter(x => x.fieldName == 'Content_Link_Url');
    if (assetLink.length == 0) return "";
    this.activeAssetUrl = assetLink[0].valueText;
    return this.activeAssetUrl;
  }


  renderProductDetail(prodDetail: ProductModel): void {
    if (typeof (prodDetail.productProperties) == 'undefined' || prodDetail.productProperties.length == 0) return;
    this.productDetailItem = prodDetail;
    this.productBanner = prodDetail.thumbnailImage;
    var prodBanner = prodDetail.productProperties.filter(x => {
      if (x['fieldName'] == 'File_Image_Url')
      return true;
    });
    if (prodBanner.length > 0)
      this.productBanner = prodBanner[0]['valueText'];
  }

  renderConfigUrl(): string{
    var s = document.URL;
    //var url = s.slice(0, s.lastIndexOf("/")) + "/";

    var encodeCbUrl = encodeURIComponent(s);
    return encodeCbUrl;
  }
  getLangCode():string{
    return 'en';
    //return cspConsumerInfo['lCode'].toLowerCase();
  }
  getModeParam(url: string): any{
    try {
      //var newUrl = new URL(url);
      var r = this.getURLParameter(url, 'mod');
      console.log(r);
      return r != undefined ? r : '';
    }
    catch (e) {
      return '';
    }
  }
  getURLParameter(url, param): string
  {
    //var sPageURL = window.location.search.substring(1);
    var sURLVariables = url.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == param)
      {
        return sParameterName[1];
      }
    }
  }​
  
  getDividParam(url: string): string{
    try {
      var newUrl = new URL(url);
      var r = newUrl.searchParams.get('divid');
      return r != undefined ? r : '';
    }
    catch (e) {
      return '';
    }
  }
  checkContactUrl(link: string): boolean{
    if(link.indexOf('ConfiguratorType=Parkereconfigurator') != -1 || link.indexOf('ConfiguratorType=Thomaseconfigurator ') != -1){
      return true;
    }
    else{
      return false;
    }
  }

  validateSelectedFacet (selectedFacet: Facet): void {
    if (this.configuredFacets.filter(x => x.facetName == selectedFacet.facetName).length == 0) {
      this.configuredFacets.push(selectedFacet);
      return;
    }
    for (var i = this.configuredFacets.length - 1; i >= 0; i--) {
      var commonProd = this.getIntersection(this.configuredFacets[i].productKeys, selectedFacet.productKeys);
      if (commonProd.length == 0) {
        this.configuredFacets.splice(i, 1);
      }
    }
    this.configuredFacets.push(selectedFacet);
  }

  updateConfiguration (e, targetFacet: Facet): void {
    var tmpAllFacets = JSON.parse(JSON.stringify(this.allFacets));
    var selectedFacet = tmpAllFacets.filter(x => x.facetName == e.target.id && x.facetValue == e.target.value);
    
    this.validateSelectedFacet(selectedFacet[0]);
    
    var commonProducts = this.configuredFacets.map(x => x.productKeys);
    commonProducts = this.getArrayUnion(...commonProducts);

    var idealList = tmpAllFacets.filter(x => this.getIntersection(x.productKeys, commonProducts).length > 0);
    idealList.forEach(x => {
      var targetProducts = this.getIntersection(x.productKeys, commonProducts);
      x.productKeys = targetProducts;
    });
    
    idealList.sort(function(a, b) {
      if (parseInt(a.sequence) > parseInt(b.sequence))
        return 1;
      if (parseInt(a.sequence) < parseInt(b.sequence))
        return -1;

      if ( a.facetName < b.facetName )
        return -1;
      if ( a.facetName > b.facetName )
          return 1;

      return 0;
    });

    this.currentFacets = idealList;
    

    if (this.getArrayUnique(this.currentFacets.map(x => x.facetName)).length == this.configuredFacets.length) {
      this.configuredFacets.forEach(x => {
        this.setCookie(x.facetName, x.facetValue, 1);
      });
      
      this.preCacheProductDetail(false);
      //this.router.navigate([`/categoryseries/${this.categoryId}/product/${this.configuredFacets[0].productKeys[0]}`]);
    }
  }

  preCacheProductDetail(isPre: boolean): void {
    var uniqueKeys = this.configuredFacets[0].productKeys;
    this.configuredFacets.forEach(x => {
      var tmp = this.getIntersection(x.productKeys, uniqueKeys);
      uniqueKeys = tmp;
    });
    if (!isPre) {
      this.prodLoading = true;
      var prodKey = uniqueKeys.length >= 1 ? uniqueKeys[0] : this.configuredFacets[0].productKeys[0];
      this.router.navigate([`/categoryseries/${this.categoryId}/product/${prodKey}`]);
    }
  }

  isConfigured (facetKey: string, facetValue: string): boolean{
    if (this.configuredFacets.length == 0) return false;
    return (this.configuredFacets.filter(x => x.facetName == facetKey && x.facetValue == facetValue).length > 0);
  }

  loadConfiguration (facetData: Facet[]): void {
    if (facetData != undefined && this.prodId == undefined && !this.isOnlyProduct) {
      for (var i = facetData.length - 1; i >= 0; i--) {
        var facetNameCount = facetData.filter(x => x.facetName == facetData[i].facetName);
        if (facetNameCount.length <= 1)
          facetData.splice(i, 1);
      }
    }
    if (this.prodId == undefined) {
      this.prodLoading = false;
      if (this.pConfigFromCache && this.pConfigFromProd && facetData.length == 0) {
        this.noPartConfigurator = true;
      }
    }
    if (this.allFacets != undefined && this.allFacets.length > 0 && this.prodId == undefined) return;
    
    facetData.sort(function(a, b) {
      if (parseInt(a.sequence) > parseInt(b.sequence))
        return 1;
      if (parseInt(a.sequence) < parseInt(b.sequence))
        return -1;

      if (a.facetName < b.facetName)
        return -1;
      if (a.facetName > b.facetName)
        return 1;
      return 0;
    });
    if (this.prodId != undefined) {
      this.productSeriesTechSpec = facetData;
    }
    else {
      this.prodCount$.subscribe(x => {
        this.allFacets = this.currentFacets = facetData;
        if (this.currentFacets.length > 0){
          this.attrLoading = false;
        }
      });
    }
  }

  resetConfiguration(): void {
    if (this.prodId == undefined) {
      this.configuredFacets = [];
      this.currentFacets = this.allFacets;
    }
    else {
      this.router.navigate([`/categoryseries/${this.categoryId}`]);
    }
  }

  hasValue (facetName: string): boolean {
    if (this.configuredFacets.length == 0) return false;
    return this.configuredFacets.filter(x => x.facetName == facetName).length > 0;
  }

  getConfiguredFacetValue(configuredFacet: any): string {
    if (configuredFacet.value.length == 1)
      return configuredFacet.value[0].facetValue;
    var cookiesValue = this.getCookie(configuredFacet.key);
    if (cookiesValue == '') {
      var facetValue = this.getArrayUnique(configuredFacet.value);
      facetValue.forEach(x => {
        cookiesValue += x.facetValue + ' ';
      })
    }
    return cookiesValue;
  }

  setCookie(cname, cvalue, exdays): void {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname): string {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  openProdImage(): void {
  }

  ngAfterViewChecked(): void {
    if (this.assetLoaded && !this.isReported) {
      this.isReported = true;
      setTimeout(() => {
        this.commonService.initCspReport();
      }, 1000);
    }
  }
}
