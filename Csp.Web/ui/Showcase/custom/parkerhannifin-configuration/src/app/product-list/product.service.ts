import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from "../../environments/environment";
import { CommonService } from '../common/common.service';

import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';

declare var cspConsumerInfo: any;

@Injectable()
export class ProductService {
  private productList$: BehaviorSubject<ProductModel[]>;
  private productList: ProductModel[];
  
  constructor(private http: Http, private commonService: CommonService) {
	}

	getProducts(prodConfig: ProductConfig): Promise<ProductModel[]> {
    var apiUrl = `${environment.hostUrl}product/all`;
    prodConfig.languageId = parseInt(cspConsumerInfo.lId);
    return this.http.post(apiUrl, prodConfig)
            .toPromise()
            .then(response => response.json())
            ;
  };
  
  getProductAssets(prodConfig: ProductConfig): Promise<ProductModel[]> {
    var apiUrl = `${environment.hostUrl}product/getassets`;
    prodConfig.languageId = parseInt(cspConsumerInfo.lId);
    return this.http.post(apiUrl, prodConfig)
            .toPromise()
            .then(response => response.json())
            ;
	};

  getProduct(id: string): Promise<ProductModel> {
    var apiUrl = `${environment.hostUrl}product?cspUniqueKey=${id}`;
    return this.http.get(apiUrl)
            .toPromise()
            .then(response => response.json())
            ;
  }

	getProductsObservable(): Observable<ProductModel[]> {
		return this.productList$.asObservable();
	}

}

interface ProductConfig {
  categoryId?: number;
  languageId?: number;
  productKeys?: any[];
  page?: number;
  contentTypeId?: number;
  paging?: boolean;
}


export class ProductModel {
  productKey: string;
  identifier: string;
  title: string;
  shortDescription: string;
  longDescription: string;
  thumbnailImage: string;
  productProperties: any[];
}