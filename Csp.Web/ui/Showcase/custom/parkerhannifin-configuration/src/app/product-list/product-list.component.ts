import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ProductService, ProductModel } from './product.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location }                 from '@angular/common';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Facet } from '../facet/facet.component';
import { CategoriesService, Category } from '../categories/categories.service';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../common/common.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
  @ViewChild('proListDiv') myDiv: ElementRef;
  allCategories: Category[];
  categoryId: number; beginProdItem: number; endProdItem: number; totalProdItemCount: number;
  productList: ProductModel[];
  dataResults: any;
  crrPage: number = 1;
  prodLoading: boolean = false;
  listMode: string = "";
  isRouteChanged = false;
  compareList: Category[] = [];
  compareExcessLimit: boolean = false;
  categoryItem: Category;
  categoryList: Category[];
  listCategorySeries: Category[];
  listCategorySeriesFull: Category[] = [];
  defaultMaxPageSize: number = 3;
  categorySeiresIdsFirstLoad: number[] = [];
  categorySeiresIdsFullLoad: number[] = [];
  stillLoading: boolean = true;
  productSeriesTitle: string = '';
  pageNavigating: boolean = false;
  getCategoryLeafNodesSubscription: any; getCategorySeriesSubscription: any;
  categorySortStatus: number = 0;
  firstLoad: boolean = true;
  checkedFacetCount: number = 0;

  private _prodKeys = new BehaviorSubject<string[]>([]);
  private _checkedFacet = new BehaviorSubject<Facet[]>([]);
  private _productSeriesListFromConfiguration = new BehaviorSubject<Category[]>([]);
  private _productSeriesIdsFromConfiguration = new BehaviorSubject<number[]>([]);

  getArrayUnique = require('array-unique');

  @Output() outputEvent = new EventEmitter();
  @Output() pageChange: EventEmitter<number>;

  @Input() set facetProdKeys(value) {
    this._prodKeys.next(value);
  };

  get facetProdKeys() {
      return this._prodKeys.getValue();
  }

  @Input() set facetCheckedList(value) {
    this._checkedFacet.next(value);
  };

  @Input() set categorySeriesList(value) {
    //this.categorySeiresIdsFullLoad = this.getArrayUnique(value);
    if (value != undefined)
      value.sort();
    this.categorySeiresIdsFullLoad = this.getArrayUnique(value);
    this._productSeriesIdsFromConfiguration.next(this.categorySeiresIdsFullLoad);
  };
  
  @Input() set firstLoadCategorySeries(value) {
    if (value == undefined || value.length == 0) return;

    this.categorySeiresIdsFirstLoad = value;
  };
  
  @Input() set currentCategory(value) {
    this.categoryItem = value;
    if (typeof(this.categoryItem.valueText) != 'undefined')
      this.productSeriesTitle = this.categoryItem.valueText;
  }

  get facetCheckedList() {
      return this._checkedFacet.getValue();
  }

  setProdKey(value): void {
    this._prodKeys.next(value);
  }

  constructor(private productService: ProductService,
              private categoryService: CategoriesService,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private commonService: CommonService) { 
                this.prodLoading = true;
              }

  ngOnInit() {
    this.route.params
			.switchMap((params: Params) => {
        this.categoryId = + params['id'];
        this.categoryList = [];
        this.listCategorySeries = [];
        this.crrPage = 1;
        this.isRouteChanged = true;
        this._prodKeys.next([]);
        this.totalProdItemCount = 0;
        this.beginProdItem = 0;
        this.endProdItem = 0;
        this._productSeriesIdsFromConfiguration.next([]);
        this.categorySeiresIdsFirstLoad = [];
        this.categorySeiresIdsFullLoad = [];
        this.stillLoading = true;
        this.prodLoading = true;
        this.pageNavigating = false;
        this.firstLoad = false;
        this.categoryService.getCacheCategoryLeafNodes({parentId: this.categoryId, contentTypeId: 31000})
                            .subscribe(x => {
                              this.loadCategorySeries(x, true);
                              this.getAllLeafNodes();
                            });
        if (this._productSeriesIdsFromConfiguration.value.length == 0)
          return this.getCategoryLeafNodesSubscription = this.categoryService.getCategoryLeafNodes({parentId: this.categoryId, contentTypeId: 31000});
        
         return this.getCategorySeriesSubscription = this.categoryService.getCategorySeries({categoryIds: this._productSeriesIdsFromConfiguration.value, contentTypeId: 31000});
			})
      .subscribe(x => {
        this.loadCategorySeries(x, true);
      });
    
    //this._productSeriesListFromConfiguration.subscribe(x => this.loadCategoryItem(x));
    //this._productSeriesIdsFromConfiguration.subscribe(x => this.loadCategoryItem(x));
    this._prodKeys.subscribe(x => {
      this.facetProduct(x)  
    });

    this._checkedFacet.subscribe(x => {
      this.checkedFacetCount = x.length;
    });
  }

  loadCategoryItem(categories: number[]): void {
    if (categories.length == 0) return;

    this.categoryService
        .getCategorySeries({categoryIds: this._productSeriesIdsFromConfiguration.value, contentTypeId: 31000})
        .subscribe(x => this.loadCategorySeries(x));
  }

  loadCategorySeries(data:any, firstLoad:boolean = false): void {
    //if (this.listCategorySeries != undefined && this.listCategorySeries.length > 0 && !this.pageNavigating) return;
    if ((data == undefined || typeof(data.results) == 'undefined') || (firstLoad && this.listCategorySeries != undefined && this.listCategorySeries.length > 0)) return;
    if (this.listCategorySeriesFull.length > 0) {
      this.prodLoading = false;
      this.stillLoading = false;
      return;
    }
    this.listCategorySeries = data.results;
    if (+data.totalItemCount == 0)
      this.beginProdItem = this.endProdItem = 0;
    else {
      this.totalProdItemCount = data.totalItemCount;
      this.beginProdItem = data.firstItemOnPage;
      this.endProdItem = data.lastItemOnPage;
    }

    this.prodLoading = false;
    this.stillLoading = false;
    setTimeout(() => {
      //this.commonService.initCspReport();
      let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
      el.click();
    }, 1500);
  }

  getLeafNodes(categoryItems: Category[]): void {
    if (categoryItems.length == 0) {
      this.categoryList = this.allCategories.filter(x => x.id == this.categoryId);
      return;
    }
    categoryItems.forEach(item => {
			var validChildren = this.allCategories.filter(category => category.parentId == item.id);
			if (validChildren.length == 0)
				this.categoryList.push(item);
			else {
				this.getLeafNodes (validChildren);
			}
		});
  }

  getAllLeafNodes(): void {
    this.categoryService.getCacheCategoryLeafNodes({parentId: this.categoryId, contentTypeId: 31000}, true).subscribe(x => {
      this.listCategorySeriesFull = x;
      if (this.listCategorySeriesFull.length > 0) {
        
        this.listCategorySeries = [];
        this.listCategorySeries = this.listCategorySeriesFull;
        this.updateSorter();
        this.pageNavigated(this.crrPage);
        this.prodLoading = false;
      }
      else
        setTimeout(() => { this.getAllLeafNodes(); }, 1000);
    })
  }

  facetProduct(prodKeys: string[]): void {
    if (this.firstLoad) {
      this.firstLoad = false;
      return;
    }
    this.prodLoading = true;
    var productKeys = [];
    prodKeys.forEach(function(value){
      if (productKeys.filter(x => x.id == value).length == 0) {
        productKeys.push(value);
      }
    });
    this.crrPage = 1;

    if (productKeys.length == 0) {
      if (this.listCategorySeriesFull.length > 0)
        this.listCategorySeries = this.listCategorySeriesFull;
      this.pageNavigated(this.crrPage);
    }
    else {
      if (this.listCategorySeriesFull.length == 0) {
        this.categoryService
            .getCategorySeries({categoryIds: productKeys.map(x => parseInt(x)), contentTypeId: 31000})
            .subscribe(x => this.loadCategorySeries(x));
      }
      else {
        this.prodLoading = false;
        productKeys = productKeys.map(x => parseInt(x));
        this.listCategorySeries = this.listCategorySeriesFull.filter(x => productKeys.indexOf(x.id) != -1);
        if (this.listCategorySeries.length == 0)
          this.beginProdItem = this.endProdItem = 0;
        else {
          this.totalProdItemCount = this.listCategorySeries.length;
          this.beginProdItem = 1;
          this.endProdItem = this.listCategorySeries.length >= 20 ? 20 : this.listCategorySeries.length;
        }
      }
    }
    //this.productService.getProducts({categoryId: this.categoryId, productKeys: productKeys}).then(data => this.extractProductList(data)); 
  }

  extractProductList(data:any): void{
    this.dataResults = data;
    this.productList = data.results;
    
    if (typeof(data.totalItemCount) == 'undefined') return;

    if (+data.totalItemCount == 0)
      this.beginProdItem = this.endProdItem = 0;
    else {
      this.totalProdItemCount = data.totalItemCount;
      this.beginProdItem = data.firstItemOnPage;
      this.endProdItem = data.lastItemOnPage;
    }

    this.prodLoading = false;
  }

  updateSorter(): void {
    //this.listCategorySeries = [];
    
    if (this.listCategorySeriesFull.length > 0) {
      if (this.categorySortStatus == 1) {
        this.listCategorySeries.sort(function(a, b) {
          return (a.valueText.toLowerCase() > b.valueText.toLowerCase()) ? 1 : ((b.valueText.toLowerCase() > a.valueText.toLowerCase()) ? -1 : 0);
        });
      }
      else if (this.categorySortStatus == 2) {
        this.listCategorySeries.sort(function(a, b) {
          return (a.valueText.toLowerCase() < b.valueText.toLowerCase()) ? 1 : ((b.valueText.toLowerCase() < a.valueText.toLowerCase()) ? -1 : 0);
        });
      }
      else {
        this.listCategorySeries.sort(function(a, b) {
          return (a.id - b.id);
        });
      }
      /* switch (this.categorySortStatus) {
        
        case 1:
        console.log(this.categorySortStatus);
          this.listCategorySeriesFull.sort(function(a, b) {
            return (a.valueText > b.valueText) ? 1 : ((b.valueText > a.valueText) ? -1 : 0);
          });
          this.listCategorySeries = this.listCategorySeriesFull;
          break;
        case 2:
        console.log(this.categorySortStatus);
          this.listCategorySeriesFull.sort(function(a, b) {
            return (a.valueText < b.valueText) ? 1 : ((b.valueText < a.valueText) ? -1 : 0);
          });
          this.listCategorySeries = this.listCategorySeriesFull;
          break;
        default:
          break;
      } */
    }
  }

  pageNavigated(event: any): void {
    if (this.listCategorySeriesFull.length > 0) {
      if (this.checkedFacetCount == 0)
        this.listCategorySeries = this.listCategorySeriesFull;
      this.beginProdItem = event == 1 ? 1 : (event - 1) * 20 + 1;
      var endItem = this.beginProdItem + 20 - 1;
      this.endProdItem = endItem > this.listCategorySeries.length ? this.listCategorySeries.length : endItem;
      this.totalProdItemCount = this.listCategorySeries.length;
      this.prodLoading = false;
      setTimeout(() => {
        //this.commonService.initCspReport();
        let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
        el.click();
      }, 1500);
      return;
    }
    this.prodLoading = true;
    this.pageNavigating = true;
    
    if (this._productSeriesIdsFromConfiguration.value.length == 0) {
      this.categoryService
          .getCategoryLeafNodes({parentId: this.categoryId, languageId: 1, contentTypeId: 31000, page: event})
          .subscribe(x => this.loadCategorySeries(x));
      return;
    }
    this.categoryService
        .getCategorySeries({categoryIds: this._productSeriesIdsFromConfiguration.value, contentTypeId: 31000, page: event})
        .subscribe(x => this.loadCategorySeries(x));
  }

  getPageMaxSize(): number {
    return (this.defaultMaxPageSize + this.crrPage) > 7 ? 7 : (this.defaultMaxPageSize + this.crrPage);
  }

  removeSelectedFacet(facetItem: Facet): void {
    var tmpList = this.facetCheckedList;
    tmpList.splice(tmpList.indexOf(facetItem), 1);
    this._checkedFacet.next(tmpList);

    this.outputEvent.emit(this.facetCheckedList);
  }

  clearSelectedFacet(): void {
    this._checkedFacet.next([]);

    this.outputEvent.emit(this.facetCheckedList);
  }

  clearCompareList(): void {
    this.compareList = [];
  }

  compareChecked(e, prodItem: Category): void {
    if (e.target.checked) {
      if (this.compareList.length < 4)
        this.compareList.push(prodItem);
      else {
        this.compareExcessLimit = true;
        e.preventDefault();
      }
    }
    else {
      if (this.compareList.indexOf(prodItem) != -1) {
        this.compareList.splice(this.compareList.indexOf(prodItem), 1);
      }
    }
  }

  goCompare(): void {
    var prodList = "";
    var categoryTitle = this.categoryItem != null ? this.categoryItem.valueText : "";
    this.compareList.forEach(x => {
      if (prodList != "") prodList += "$#$";
      prodList += x.id;
    });
    prodList = btoa(prodList);

    this.router.navigate([`/compare/${prodList}/returnid/${this.categoryId}/title/${categoryTitle}`]);
  }
}
