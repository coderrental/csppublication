import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location }                 from '@angular/common';
import { Facet } from '../facet/facet.component';
import { FacetService } from '../facet/facet.service';
import { ProductService, ProductModel } from '../product-list/product.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/switchMap';
import { Category, CategoriesService } from '../categories/categories.service';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css'],
  providers: [FacetService, ProductService]
})
export class CompareComponent implements OnInit {
  @ViewChild('comparePageDiv') myDiv: ElementRef;
  categoryTitle: string;
  categoryId: number;
  prodIds: string[];
  facetNames: string[];
  productList: Category[];
  productFacet: Facet[] = [];
  maxFacetRowCount: number = 0;
  prodLoading: boolean = true;

  constructor(private route: ActivatedRoute,
              private location: Location,
              private facetService: FacetService,
              private router: Router,
              private productService: ProductService,
              private categoryService: CategoriesService) { }

  ngOnInit() {
    this.route.params
			.switchMap((params: Params) => {
				this.categoryTitle = params['categorytitle'];
        this.categoryId = +params['categoryid'];
        var decodedParam = atob(params['prodlist']);
        this.prodIds = decodedParam.split('$#$');

        return this.facetService.getFacets({contentTypeId: 31000, facetClass: 'Compare', categoryIds: this.prodIds.map(x => parseInt(x))});
			})
      .subscribe (x => { this.getCompareDetail(x) });
  }

  getCompareDetail(facetList: Facet[]): void {
    this.productFacet = facetList;
    this.getProductList();

    this.facetNames = [];
    this.productFacet.forEach(x => {
      if (this.facetNames.indexOf(x.facetName) == -1)
        this.facetNames.push(x.facetName);
    });
    this.facetNames.sort();
    
  }

  getProductList(): void {
    this.productList = [];
    this.categoryService
        .getCategorySeries({categoryIds: this.prodIds.map(x => parseInt(x)), contentTypeId: 31000})
        .subscribe(x => {
          this.productList = x.results;
          if(this.productList.length > 0) 
            this.prodLoading = false;
          setTimeout(() => {
            let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
            el.click();
          }, 1000);
        });
        
  }

  getFacetValue(prodKey: string, facetName: string): string {
    if (prodKey == undefined) return '';
    var facetItem = this.productFacet.filter(x => {
      return (x.facetName == facetName && x.productKeys.indexOf(prodKey) > -1);
    });
    return facetItem.length > 0 ? facetItem[0].facetValue : '';
  }

  removeProd(prod: Category): void {
     this.productList.splice(this.productList.indexOf(prod), 1);
    if (this.productList.length == 1) {
      this.router.navigate([`/category/${this.categoryId}`]);
    } 
  }

  goViewProduct(prod: Category): void {
    this.router.navigate([`/categoryseries/${prod.id}`]);
  }
}

export class CompareProductItem {

}