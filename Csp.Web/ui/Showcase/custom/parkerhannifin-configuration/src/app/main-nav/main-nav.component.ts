import { Component, OnInit, Input } from '@angular/core';
import { CategoriesService } from '../categories/categories.service'
import { Category } from '../categories/categories.service'
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})

export class MainNavComponent implements OnInit {
	categories: Category[];
	menuItems: Category[];
	menuLoading: boolean = true;
	showcaseMode: boolean = environment.production;
	@Input()
	set ready(isReady: boolean) {
		if (isReady) this.menuLoading = false;
	}
	
	constructor(private categoryService: CategoriesService) { }

	ngOnInit() {
		this.menuLoading = true;
		this.categoryService.initializeDataService();
		this.getCategories();
	}
	
	getCategories(): void {
		this.categoryService.getFirstLevelCategoriesObservable().subscribe(data => {
			this.loadedCategories(data);
		});
	}
	
	loadedCategories (categories): void {
		
		categories.sort(function(a, b) {
			if ( a.valueText < b.valueText )
			return -1;
			if ( a.valueText > b.valueText )
				return 1;
			return 0;
		});
		this.menuItems = categories;
		this.menuLoading = false;
	}
}
