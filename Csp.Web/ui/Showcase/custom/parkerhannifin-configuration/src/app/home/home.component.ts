import { Component, OnInit, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { CategoriesService, Category } from '../categories/categories.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../common/common.service';
//require('slick-carousel');

declare var initHomeCarousel: any;
declare var initHomeProductCarousel: any;
declare var cspConsumerInfo: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewChecked {
  @ViewChild('homePageDiv') myDiv: ElementRef;
  featuredList: Category[];
  prodCarouselInited: boolean = false;
  prodLoading: boolean = true;
  isReported: boolean = false;
  constructor(private categoryService: CategoriesService, private commonService: CommonService) {
  }

  ngOnInit() {
    this.categoryService.getApiRootIdObservable().subscribe(x => {
      this.categoryService.getFirstLevelCategoriesObservable().subscribe(data => {
        if (data.length == 0) {
          this.prodLoading = false;
          return;
        }

        data.sort(function(a, b) {
          if ( a.valueText < b.valueText )
          return -1;
          if ( a.valueText > b.valueText )
              return 1;
          return 0;
        });

        this.featuredList = data;
        this.prodLoading = false;
        setTimeout(() => {
          let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
          el.click();
        }, 1500);

      });
    });
  }

  ngAfterViewChecked() {
    if (typeof(this.featuredList) != 'undefined' && this.featuredList.length > 0 && !this.prodLoading && !this.isReported) {
      this.isReported = true;
      
      setTimeout(() => {
        this.commonService.initCspReport();
      }, 1000);
    }
  }
}
