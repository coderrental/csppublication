import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Location } from '@angular/common';
import { LeadgenService, LeadgenField } from '../leadgen/leadgen.service';
import { Http } from '@angular/http';
import { NgForm } from "@angular/forms/src/forms";
import { CommonService } from '../common/common.service';

declare var jquery:any;
declare var $ :any;
declare var CspReportLib: any;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'],
  providers: [ LeadgenService ]
})
export class ContactUsComponent implements OnInit, AfterViewChecked {
  @ViewChild('contactusDiv') myDiv: ElementRef;
  btnSubmit: LeadgenField; formHeader: LeadgenField; msgThankYou: LeadgenField;
  fieldsContactUs: LeadgenField[] = []; otherFields: LeadgenField[];
  excludedFieldIds: string[] = ['submitbutton', 'formheader'];
  supplierInfo: any;
  isSubmitted: boolean = false;
  isReported: boolean = false;

  constructor(private leadgenService: LeadgenService,
              private http: Http, 
              private commonService: CommonService, 
              private location: Location
              ) 
  { }

  ngOnInit() {
    this.leadgenService.getContactUsFieldsObservable().subscribe(x => {this.initLeadgenFields(x)});
    this.leadgenService.getSupplierInfo().subscribe(x => {this.supplierInfo = x;});
    this.leadgenService.getOtherFields().subscribe(x => this.initOtherFields(x));
  }

  initLeadgenFields(fieldList: LeadgenField[]): void {
    if (fieldList.length == 0) return;
    this.fieldsContactUs = fieldList.filter(x => x.fieldid != undefined && this.excludedFieldIds.indexOf(x.fieldid) == -1);
    
    var findBtnSubmit = fieldList.filter(x => x.fieldid != undefined && x.fieldid.toLowerCase() == 'submitbutton');
    var findFormHeader = fieldList.filter(x => x.fieldid != undefined && x.fieldid.toLowerCase() == 'formheader');
    this.btnSubmit = findBtnSubmit.length > 0 ? findBtnSubmit[0] : null;
    this.formHeader = findFormHeader.length > 0 ? findFormHeader[0] : null;

    setTimeout(() => {
      //this.commonService.initCspReport();
      let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
      el.click();
    }, 1000);
  }

  initOtherFields(fieldList: LeadgenField[]): void {
    this.otherFields = fieldList;
    var findThankYouMsg = this.otherFields.filter(x => x.fieldid != undefined && x.fieldid.toLowerCase() == 'thankyoumessaging');
    this.msgThankYou = findThankYouMsg.length > 0 ? findThankYouMsg[0] : new LeadgenField();
  }

  onSubmit(frContactUs: NgForm): void {
    this.isSubmitted = true;
    try {
      this.commonService.initCspReport();
      if (typeof(CspReportLib) != 'undefined') {
        var cContent = jQuery('#submitbutton').attr('cspenglishvalue');
        var olDCSext = CspReportLib.wt.DCSext;
        CspReportLib.wt.DCSext["ConversionShown"] = null;
        CspReportLib.wt.DCSext["ConversionClick"] = cContent;
        CspReportLib.wt.DCSext["ConversionType"] = "Contact Us";
        CspReportLib.wt.DCSext["ConversionContent"] = cContent;//jQuery('#submitbutton').attr('cspenglishvalue');
        CspReportLib.wt.dcsMultiTrack();
        CspReportLib.wt.DCSext = olDCSext;
      }
    } catch (exc) {}
    
    setTimeout(() => {
      frContactUs.reset();
      //this.isSubmitted = false;
    }, 3000);
    this.leadgenService.submitLeadgen(frContactUs.value).subscribe(
      x => this.formSubmitted()
    );
  }

  backToLastPage(): void {
    this.location.back();
  }

  formSubmitted(): void {
    //this.isSubmitted = true;
  }

  ngAfterViewChecked(): void {
    if (typeof(this.fieldsContactUs) != 'undefined' && this.fieldsContactUs.length > 0 && !this.isReported) {
			this.isReported = true;
			setTimeout(() => {
				this.commonService.initCspReport();
			}, 1000);
		}
  }
}
