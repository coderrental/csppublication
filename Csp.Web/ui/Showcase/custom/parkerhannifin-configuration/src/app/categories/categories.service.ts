import { Injectable } from '@angular/core';
import { Headers, Http, BaseRequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { CommonService } from '../common/common.service';

declare var cspConsumerInfo: any;

@Injectable()
export class CategoriesService {
	host:string;
	rootCategoryId: number; apiRootCategoryId: number = 0;
	private categoryApiUrl: string;
	private categoryList$: BehaviorSubject<Category[]>;
	private categoryList: Category[];
	private firstLevelList$: BehaviorSubject<Category[]>;
	private firstLevelList: Category[];
	private categorySeries$: BehaviorSubject<number>;
	private apiRootCategoryId$: BehaviorSubject<number> = <BehaviorSubject<number>> new BehaviorSubject(0);
	private defaultContentTypeId: number = 31000;

	constructor(private http: Http, private commonService: CommonService) {
		//this.rootCategoryId = environment.rootId;//config.getConfig("rootCategoryId");
		this.host = `${environment.hostUrl}`;//config.getConfig("host");
		this.firstLevelList = [];
		if (this.firstLevelList$ == undefined)
			this.firstLevelList$ = <BehaviorSubject<Category[]>> new BehaviorSubject(new Array<Category>());
		if (this.categorySeries$ ==  undefined)
			this.categorySeries$ = <BehaviorSubject<number>> new BehaviorSubject(0);
		this.apiRootCategoryId$ = <BehaviorSubject<number>> new BehaviorSubject(-1);
		//this.initializeDataService();
	}
	
	initializeDataService() {
		this.getCacheRootCategory().subscribe(x => {
			if (x.length == 0) {
				this.getRootCategory().subscribe(x => {
					this.initRootNode(x);
				});
				return;
			}
			this.initRootNode(x);
		});
	}

	initRootNode(x: Category[]): void {
		if (x.length > 1) {
			var rootNode = x.filter(n => n.id != 133);
			if (rootNode.length > 0)
				this.apiRootCategoryId = rootNode[0].id;
		}
		else if (x.length == 1) {
			this.apiRootCategoryId = x[0].id;
		}
		else {
			this.apiRootCategoryId = this.rootCategoryId;
		}
		if (this.apiRootCategoryId$.value <= 0)
			this.apiRootCategoryId$.next(this.apiRootCategoryId);
		this.getCacheCategories(this.apiRootCategoryId).subscribe(x => {
			if (x.length == 0) {
				this.getCategories(false).subscribe(
					allData => {
						this.validateParentNodes(allData);
						
					}
				);
				return;
			}
			this.validateParentNodes(x);
		});
		
	}

	validateParentNodes(cateList: Category[]): void {
		if (cateList.length == 0) return;
		this.firstLevelList = cateList;
		this.firstLevelList$.next(this.firstLevelList);
		/* var data = cateList;
		let validList = [];
		let loadLeafNodesCountRequests = [];
        data.forEach(x => {
		  //loadLeafNodesCountRequests.push(this.getCategoryLeafNodes({parentId: x.id, contentTypeId: 31000, count: true}));
		  this.getCategoryLeafNodes({parentId: x.id, contentTypeId: 31000, count: true}).subscribe(count => {
			if (count > 0) {
				this.firstLevelList.push(x);
				this.firstLevelList$.next(this.firstLevelList);
			}
		  });
        }); */
		/* console.log(data);
        Observable.forkJoin(loadLeafNodesCountRequests).subscribe(results => {
          console.log(results);
		}); */
		//this.firstLevelList = validList;
		
	}

	getCacheRootCategory(): Observable<Category[]> {
		var cacheApiUrl = `${environment.cacheApiUrl}category/getroot?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&parentId=0`;;
		return this.http.get(cacheApiUrl)
			.map(resData => resData.json())
			.catch(this.handleError)
			;
	}

	getRootCategory(): Observable<Category[]> {
		this.categoryApiUrl = `${environment.hostUrl}category/getroot`;
		return this.http.get(this.categoryApiUrl)
			.map(resData => resData.json())
			.catch(this.handleError)
			;
	}

	getCacheCategories(parentId: number): Observable<Category[]> {
		var targetRootId = this.apiRootCategoryId > 0 ? this.apiRootCategoryId : this.rootCategoryId;
		var getByParentId = parentId > -1 ? parentId : targetRootId;
		var urlCacheApi = `${environment.cacheApiUrl}category/subcategories?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&parentId=${getByParentId}`;
				
		return this.http.get(urlCacheApi)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCategories(includeChildren:boolean, parentId: number = -1): Observable<Category[]> {
		var targetRootId = this.apiRootCategoryId > 0 ? this.apiRootCategoryId : this.rootCategoryId;
		var getByParentId = parentId > -1 ? parentId : targetRootId;
		
		this.categoryApiUrl = `${environment.hostUrl}category/all?categoryContentTypeId=${this.defaultContentTypeId}&languageId=${cspConsumerInfo['lId']}&parentId=${getByParentId}&countProduct=false&includeChildren=${includeChildren}&cspId=${cspConsumerInfo.companies_Id}`;
		
		return this.http.get(this.categoryApiUrl)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCacheCategoryLeafNodes(categoryConfig: CategoryConfig, fullList = false): Observable<any> {
		var urlCacheApi = `${environment.cacheApiUrl}category/getleafnodes?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&parentId=${categoryConfig.parentId}&contentTypeId=${categoryConfig.contentTypeId}`;
		if (fullList)
			urlCacheApi += '&fullList=true';
		return this.http.get(urlCacheApi)
            .map(resData => resData.json())
            .catch(this.handleError)
            ;
	}

	getCategoryLeafNodes(categoryConfig: CategoryConfig): Observable<any> {		
		categoryConfig.cspId = parseInt(cspConsumerInfo.companies_Id);
		categoryConfig.languageId = parseInt(cspConsumerInfo.lId);
		var categorySeriesApiUrl = `${environment.hostUrl}category/getleafnodes`;
		return this.http.post(categorySeriesApiUrl, categoryConfig)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCacheCategoryLeafNodesIds(categoryConfig: CategoryConfig): Observable<number[]> {
		var langId = categoryConfig.languageId == undefined || categoryConfig.languageId <= 0 ? parseInt(cspConsumerInfo.lId) : categoryConfig.languageId;
		var firstNItems = categoryConfig.firstNItems != undefined && categoryConfig.firstNItems > 0 ? categoryConfig.firstNItems : 0;
		var urlCacheApi = `${environment.cacheApiUrl}category/getleafids?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&parentId=${categoryConfig.parentId}`;
		return this.http.get(urlCacheApi)
            .map(resData => resData.json())
            .catch(this.handleError)
            ;
	}

	getCategoryLeafNodesIds(categoryConfig: CategoryConfig): Observable<number[]> {
		var langId = categoryConfig.languageId == undefined || categoryConfig.languageId <= 0 ? parseInt(cspConsumerInfo.lId) : categoryConfig.languageId;
		var firstNItems = categoryConfig.firstNItems != undefined && categoryConfig.firstNItems > 0 ? categoryConfig.firstNItems : 0;
		this.categoryApiUrl = `${environment.hostUrl}category/getleavesids?categoryContentTypeId=${this.defaultContentTypeId}&languageId=${langId}&parentId=${categoryConfig.parentId}&cspId=${cspConsumerInfo.companies_Id}&firstNItems=${firstNItems}`;
		return this.http.get(this.categoryApiUrl)
            .map(resData => resData.json())
            .catch(this.handleError)
            ;
	}

	getCacheCategorySeries(categoryId: number,contentTypeId: number): Observable<any> {
		var urlCacheApi = `${environment.cacheApiUrl}productseries/get?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&categoryId=${categoryId}&contentTypeId=${contentTypeId}`;
				
		return this.http.get(urlCacheApi)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCategorySeries(categoryConfig: CategoryConfig): Observable<any> {
		var categorySeriesApiUrl = `${environment.hostUrl}category/all`;
		categoryConfig.cspId = parseInt(cspConsumerInfo.companies_Id);
		categoryConfig.languageId = parseInt(cspConsumerInfo.lId);
		categoryConfig.contentTypeId = categoryConfig.contentTypeId == undefined ? this.defaultContentTypeId : categoryConfig.contentTypeId;
		return this.http.post(categorySeriesApiUrl, categoryConfig)
            .map(resData => resData.json())
            .catch(this.handleError)
            ;
	}

	getProductCount(categoryId: number, langId: number, categoryCTId: number, productCTId: number): Observable<number> {
		var apiLangId = parseInt(cspConsumerInfo.lId);
		this.categoryApiUrl = `${environment.hostUrl}category/all?languageId=${apiLangId}&parentId=${categoryId}&categoryContentTypeId=${categoryCTId}&countProduct=true&productContentTypeId=${productCTId}&includeChildren=true`;
		return this.http.get(this.categoryApiUrl)
            .map(resData => resData.json())
            .catch(this.handleError)
            ;
	}

	getCategoriesObservable(parentId:number = -1): Observable<Category[]> {
		if (this.categoryList$ == undefined) {
			this.categoryList$ = <BehaviorSubject<Category[]>> new BehaviorSubject(new Array<Category>());
		}
		if (parentId != this.categorySeries$.value) {
			this.getCategories(true, parentId).subscribe(
				allData => {
					this.categoryList = allData;
					this.categoryList$.next(allData);
				}
			);
			this.categorySeries$.next(parentId);
		}
		
		return this.categoryList$.asObservable();
	}

	getFirstLevelCategoriesObservable(): Observable<Category[]> {
		return this.firstLevelList$.asObservable();
	}

	getApiRootIdObservable(): Observable<number> {
		return this.apiRootCategoryId$.asObservable();
	}

	private handleError(error: any): Promise<any> {
		//console.error('An error occurred', error); // for demo purposes only
		return Promise.reject(error.message || error);
	}
}

export class Category {
	id: number;
    parentId: number;
    depth: number;
    lineAge: string;
    valueText: string;
	count: number;
	shortDescription: string;
	longDescription: string;
    image: string;
    thumbnailImage: string;
	categoryProperties: CategoryProperties[];
	prodCount: number;
}

export class CategoryProperties {
	fieldName: string;
	valueText: string;
}

interface CategoryConfig {
	parentId?: number;
	languageId?: number;
	categoryIds?: number[];
	page?: number;
	contentTypeId?: number;
	cspId?: number;
	firstNItems?: number;
	count?: boolean;
}