import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { ConfiguratorComponent }   from '../configurator/configurator.component';
import { CompareComponent } from '../compare/compare.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { HomeComponent } from '../home/home.component';
import { ContactUsComponent } from '../contact-us/contact-us.component';
 
const routes: Routes = [
  { path: '', component: HomeComponent },
	{ path: 'category/:id', component: ConfiguratorComponent },
  { path: 'categoryseries/:categoryid/product/:productid', component: ProductDetailComponent },
  { path: 'categoryseries/:categoryid', component: ProductDetailComponent },
  { path: 'compare/:prodlist/returnid/:categoryid/title/:categorytitle', component: CompareComponent },
  { path: 'contactus', component: ContactUsComponent },
  { path: 'contactus/:title', component: ContactUsComponent },
  { path: '**', redirectTo: '' }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}