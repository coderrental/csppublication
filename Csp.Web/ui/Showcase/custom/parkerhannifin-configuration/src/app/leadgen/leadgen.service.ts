import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LeadgenService {
  private leadgenFieldList$: BehaviorSubject<LeadgenField[]>;
  private contactusFieldList$: BehaviorSubject<LeadgenField[]>;
  private assetFieldList$: BehaviorSubject<LeadgenField[]>;
  private supplierInfo$: BehaviorSubject<any>;
  private otherFields$: BehaviorSubject<LeadgenField[]>;

  constructor(private http: Http) {
    if (this.leadgenFieldList$ == undefined) {
      this.leadgenFieldList$ = <BehaviorSubject<LeadgenField[]>> new BehaviorSubject(new Array<LeadgenField>());
      this.contactusFieldList$ = <BehaviorSubject<LeadgenField[]>> new BehaviorSubject(new Array<LeadgenField>());
      this.assetFieldList$ = <BehaviorSubject<LeadgenField[]>> new BehaviorSubject(new Array<LeadgenField>());
      this.otherFields$ = <BehaviorSubject<LeadgenField[]>> new BehaviorSubject(new Array<LeadgenField>());
      this.supplierInfo$ = <BehaviorSubject<any>> new BehaviorSubject(new Array());
			this.getLeadgens().subscribe(
				allData => {
          this.contactusFieldList$.next(allData[1].contactusformfields.filter(x => x.statuscontactus != undefined && x.statuscontactus.toLowerCase() == 'yes'));
          this.assetFieldList$.next(allData[1].contactusformfields.filter(x => x.statusformviewasset != undefined && x.statusformviewasset.toLowerCase() == 'yes'));
          this.leadgenFieldList$.next(allData[1].contactusformfields);
          this.supplierInfo$.next(allData[0].supplierinfo);
          this.otherFields$.next(allData[1].contactusformfields.filter(x => x.statusformviewasset != undefined && x.statusformviewasset.toLowerCase() == ''));
				}, err => {}
			);
		}
  }

  getAllLeadgenFieldsObservable(): Observable<LeadgenField[]> {
		return this.leadgenFieldList$.asObservable();
  }
  
  getContactUsFieldsObservable(): Observable<LeadgenField[]> {
    return this.contactusFieldList$.asObservable();
  }

  getSupplierInfo(): Observable<any> {
    return this.supplierInfo$.asObservable();
  }

  getOtherFields(): Observable<LeadgenField[]> {
    return this.otherFields$.asObservable();
  }

  getLeadgens(): Observable<any> {
    var apiUrl = `${environment.leadgenUrl}`;
    return this.http.get(apiUrl)
            .map(response => response.json())
            .catch(this.handleError)
            ;
  };
  
  submitLeadgen(formData: any): Observable<any> {
    var responsedMessage = '';
    var params = new URLSearchParams();
    let formSerialize = '';
    for(let key in formData) {
      params.set(key, formData[key]);
      if (formSerialize != '') formSerialize += '&';
      formSerialize += key + '=' + formData[key];
    }
    
    let formheaders = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: formheaders });
    var tick = (new Date()).getTime() + "" + Math.floor(Math.random() * 1212);

    this.http.post(environment.saveFormUrl + "?tick=" + tick, formSerialize, options).subscribe(x => console.log(x));
    return this.http.post(environment.submitFormUrl, formSerialize, options).map(response => response.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return error;
	}

}

export class LeadgenField {
  statuscontactus: string;
  fieldid: string;
  fieldlabel: string;
  fieldname: string;
  contenttitle: string;
  description: string;
  type: string;
  fielderrormessage: string;
  fieldvalidationtype: string;
  statusvalidation: string;
  statusformviewasset: string;
  statusformtrialrequest: string;
}