import { TestBed, inject } from '@angular/core/testing';

import { LeadgenService } from './leadgen.service';

describe('LeadgenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeadgenService]
    });
  });

  it('should be created', inject([LeadgenService], (service: LeadgenService) => {
    expect(service).toBeTruthy();
  }));
});
