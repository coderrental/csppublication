import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { CategoriesService, Category } from '../categories/categories.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html'
})
export class BreadcrumbComponent implements OnInit {
  categoryId: number;
  breadcrumbItems: Category[];
  currentNode: Category;
  currentNodeTitle: string = '';
  isProdDetail: boolean = false;

  constructor(private categoryService: CategoriesService,
              private route: ActivatedRoute,
              private location: Location) 
  {  }

  ngOnInit() {
    
    this.route.params
    .switchMap((params: Params) => {
      this.categoryId = typeof(params['id']) != 'undefined' ? +params['id'] : +params['categoryid'];
      this.isProdDetail = typeof(params['productid']) != 'undefined';
      this.breadcrumbItems = [];
      
      return this.categoryService.getCategorySeries({categoryIds: [this.categoryId], contentTypeId: 31000});
    })
    .subscribe(categories => this.loadBreadcrumb(categories));
  }

  loadBreadcrumb (categories): void {
    //this.breadcrumbItems.unshift(categories.results[0]);
    this.currentNode = categories.results[0];
    this.currentNodeTitle = this.currentNode.valueText;
    this.getParentNode(categories.results[0].parentId);
  }

  getParentNode (parentId: number): void {
    if (parentId <= 0) return;
    this.categoryService.getCategorySeries({categoryIds: [parentId], contentTypeId: 31000})
                        .subscribe(x => {
                          if (x.results.length == 0) {
                            //console.log(this.breadcrumbItems);
                            return;
                          }
                          this.breadcrumbItems.unshift(x.results[0]);
                          this.getParentNode(x.results[0].parentId);
                        })
  }
}
