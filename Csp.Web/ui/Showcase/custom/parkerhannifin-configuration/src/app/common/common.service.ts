import { Injectable, Inject } from '@angular/core';
import { environment } from '../../environments/environment';
import { Location } from '@angular/common';
import { DOCUMENT } from '@angular/platform-browser';

declare var jquery:any;
declare var $ :any;
declare var cspConsumerInfo: any;

@Injectable()
export class CommonService {
  dataLanguage = [
    {id: 1,		langCode: "EN" },
    {id: 2,		langCode: "DE" },
    {id: 3,		langCode: "FR" },
    {id: 4,		langCode: "ES" },
    {id: 5,		langCode: "IT" },
    {id: 6,		langCode: "P99"},
    {id: 7,		langCode: "PT" },
    {id: 8,		langCode: "JA" },
    {id: 9,		langCode: "ZH" },
    {id: 10,	langCode: "TR" },
    {id: 11,	langCode: "NL" },
    {id: 12,	langCode: "DA" },
    {id: 13,	langCode: "NO" },
    {id: 14,	langCode: "SV" },
    {id: 15,	langCode: "RU" },
    {id: 16,	langCode: "KO" },
    {id: 17,	langCode: "EZA"},
    {id: 18,	langCode: "UK" },
    {id: 19,	langCode: "CA" },
    {id: 20,	langCode: "EIE"},
    {id: 21,	langCode: "EAU"},
    {id: 22,	langCode: "PTB"},
    {id: 23,	langCode: "ESL"},
    {id: 24,	langCode: "CS" },
    {id: 25,	langCode: "ZHT"},
    {id: 26,	langCode: "ENU"},
    {id: 27,	langCode: "PL" },
    {id: 28,	langCode: "ENZ"},
    {id: 29,	langCode: "BR" },
    {id: 30,	langCode: "HR" },
    {id: 31,	langCode: "EE" },
    {id: 32,	langCode: "GR" },
    {id: 33,	langCode: "HU" },
    {id: 34,	langCode: "LV" },
    {id: 35,	langCode: "LT" },
    {id: 36,	langCode: "RO" },
    {id: 37,	langCode: "RS" },
    {id: 38,	langCode: "SK" },
    {id: 39,	langCode: "SI" },
    {id: 40,	langCode: "UA" },
    {id: 41,	langCode: "SG" },
    {id: 42,	langCode: "HK" },
    {id: 43,	langCode: "BE" },
    {id: 44,	langCode: "FI" },
    {id: 45,	langCode: "HE" },
    {id: 46,	langCode: "CH" },
    {id: 47,	langCode: "FRB"},
    {id: 49,	langCode: "FRC"},
    {id: 51,	langCode: "FRS"}
  ];
  constructor(private location: Location,
              @Inject(DOCUMENT) private document) { 
    this.initConsumerInfo();
  }

  initConsumerInfo(): void {
    if (isNaN(cspConsumerInfo['companies_Id'])) {
      cspConsumerInfo = { "companies_Id": "0", "companyname": "test_ehutch10242017", "country": "CN", "lId": "3", "sId": "12", "sName": "Default", "lng": "IT", "lngDesc": "Italian", "customerId": "995388" };
		}
    //console.log(cspConsumerInfo);
    if (isNaN(cspConsumerInfo["lId"])) cspConsumerInfo["lId"] = "1";

    var crrHostName = document.location.hostname;
    if (crrHostName.indexOf('-') != -1) {
      var plcComponents = crrHostName.split('-');
      var langComponent = plcComponents.filter(x => x.startsWith('l'));
      if (langComponent.length > 0) {
        var defaultLang = cspConsumerInfo['lId'];
        var langComponentValue:any = langComponent[0].replace('l', '');
        try {
          if (!isNaN(langComponentValue)) {
            cspConsumerInfo['lId'] = parseInt(langComponentValue);
          }
          else {
            var dataLangItem = this.dataLanguage.filter(x => x.langCode.toLowerCase() == langComponentValue.toLowerCase());
            if (dataLangItem.length > 0) {
              cspConsumerInfo['lId'] = dataLangItem[0].id;
              cspConsumerInfo['lCode'] = dataLangItem[0].langCode;
            }
          }
        }
        catch (exc) {
          cspConsumerInfo['lId'] = defaultLang;
        }
      }
    }
    
    if (!isNaN(cspConsumerInfo["lId"])) {
      var dataLangItem = this.dataLanguage.filter(x => x.id == cspConsumerInfo['lId']);
      if (dataLangItem.length > 0) {
        cspConsumerInfo['lCode'] = dataLangItem[0].langCode;
      }
    }
  }

  initCspReport(): void {
    jQuery('head title').text(jQuery('[csptype="TITLE"]').text());
    jQuery('head #csp-report-lib').remove();
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.id = "csp-report-lib";
    script.src = "https://global.syndication.tiekinetix.net/js/CspReportLib.js";
    document.getElementsByTagName('head')[0].appendChild(script);
  }
}
