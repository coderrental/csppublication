import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'groupfacet'
})
export class GroupfacetPipe implements PipeTransform {

	transform(value: Array<any>, field: string): Array<any> {
		if (typeof (value) == 'undefined' || value.length == 0) return null;
		
		var facetList = value.slice();
		
		const groupedObj = value.reduce((prev, cur)=> {
			if(!prev[cur[field]]) {
				prev[cur[field]] = [cur];
			} else {
				prev[cur[field]].push(cur);
			}
			return prev;
		}, {});
		return Object.keys(groupedObj).map(key => ({ 
			key,
			value: facetList.filter(item => item.facetName == key)}));
	}

}
