import { TestBed, inject } from '@angular/core/testing';

import { FacetServiceService } from './facet-service.service';

describe('FacetServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FacetServiceService]
    });
  });

  it('should be created', inject([FacetServiceService], (service: FacetServiceService) => {
    expect(service).toBeTruthy();
  }));
});
