import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { environment } from "../../environments/environment";
import { Facet } from './facet.component';
import { CommonService } from '../common/common.service';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';

declare var cspConsumerInfo: any;

@Injectable()
export class FacetService {
	private facetApiUrl = `${environment.hostUrl}Facet`;
	getArrayUnique = require('array-unique');
	constructor(private http: Http, private commonService: CommonService) {

	}

	getCacheFacets(config: CacheFacetConfig): Promise<Facet[]> {		
		config.LanguageId = parseInt(cspConsumerInfo.lId);
		config.CspId = parseInt(cspConsumerInfo.companies_Id);
		config.ProjectName = environment.projectName;
		config.ProjectMode = environment.projectMode;
		var cacheApiUrl = `${environment.cacheApiUrl}facet/getfacet`;
		return this.http.post(cacheApiUrl, config)
            .toPromise()
            .then(response => response.json())
			.catch(this.handleError)
            ;
	}

	getCacheProdDetailFacets(config: CacheFacetConfig): Observable<Facet[]> {
		config.LanguageId = parseInt(cspConsumerInfo.lId);
		config.CspId = parseInt(cspConsumerInfo.companies_Id);
		var cacheApiUrl = `${environment.cacheApiUrl}/productseries/productdetailfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&contentTypeId=${config.ContentTypeId}&facetClass=${config.FacetClass}`;
		return this.http.get(cacheApiUrl)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCacheProdSeriesFacets(config: CacheFacetConfig): Observable<Facet[]> {
		config.LanguageId = parseInt(cspConsumerInfo.lId);
		config.CspId = parseInt(cspConsumerInfo.companies_Id);
		var cacheApiUrl = `${environment.cacheApiUrl}/productseries/productseriesfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&contentTypeId=${config.ContentTypeId}&facetClass=${config.FacetClass}`;
		return this.http.get(cacheApiUrl)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	getCacheTabContent(config: CacheFacetConfig): Observable<Facet[]> {
		//http://global.syndication.tiekinetix.net/DgCacheApi/productseries/tabcontentfacet?projectName=parker&mode=prod&languageId=1&categoryId=77&productKey=2354208&facetClass=Configurator
		config.LanguageId = parseInt(cspConsumerInfo.lId);
		config.CspId = parseInt(cspConsumerInfo.companies_Id);
		//var cacheApiUrl = `${environment.cacheApiUrl}/productseries/productdetailfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&contentTypeId=${config.ContentTypeId}&facetClass=${config.FacetClass}`;
		var cacheApiUrl = `${environment.cacheApiUrl}/productseries/tabcontentfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&productKey=${config.ProductKey}&facetClass=${config.FacetClass}`;
		return this.http.get(cacheApiUrl)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}

	preCacheTabContent(config: CacheFacetConfig): Observable<Facet[]> {
		//http://global.syndication.tiekinetix.net/DgCacheApi/productseries/tabcontentfacet?projectName=parker&mode=prod&languageId=1&categoryId=77&productKey=2354208&facetClass=Configurator
		config.LanguageId = parseInt(cspConsumerInfo.lId);
		config.CspId = parseInt(cspConsumerInfo.companies_Id);
		//var cacheApiUrl = `${environment.cacheApiUrl}/productseries/productdetailfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&cspId=${cspConsumerInfo.companies_Id}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&contentTypeId=${config.ContentTypeId}&facetClass=${config.FacetClass}`;
		var cacheApiUrl = `${environment.wsCacheApiUrl}/productseries/tabcontentfacet?projectName=${environment.projectName}&mode=${environment.projectMode}&languageId=${cspConsumerInfo['lId']}&categoryId=${config.ParentId}&productKey=${config.ProductKey}&facetClass=${config.FacetClass}`;
		return this.http.get(cacheApiUrl)
		.map(resData => resData.json())
		.catch(this.handleError)
		;
	}
	
	getFacets(config: FacetConfig): Promise<Facet[]> {
		if (typeof(config.categoryIds) != 'undefined' && config.categoryIds.length > 0)
			config.categoryIds = this.getArrayUnique(config.categoryIds);
		
		config.languageId = parseInt(cspConsumerInfo.lId);
		return this.http.post(this.facetApiUrl, config)
            .toPromise()
            .then(response => response.json())
			.catch(this.handleError)
            ;
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error); // for demo purposes only
		return Promise.reject(error.message || error);
	}
}

interface FacetConfig {
  categoryId?: number;
  languageId?: number;
  productKeys?: string[];
  fieldName?: string;
  contentTypeId?: number;
  facetClass?: string;
  categoryIds?: number[];
}

interface CacheFacetConfig {
	ProjectName?: string;
	ProjectMode?: string;
	CspId?: number;
	CategoryIds?: number[];
	ParentId: number;
	ContentTypeId: number;
	FacetClass: string;
	LanguageId?: number;
	ProductKey?: string;
}