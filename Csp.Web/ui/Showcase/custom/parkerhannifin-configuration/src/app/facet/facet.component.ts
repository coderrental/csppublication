import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef } 	from '@angular/core';
import { ActivatedRoute, Params }   						from '@angular/router';
import { Location }                 						from '@angular/common';
import { BehaviorSubject } 									from 'rxjs/BehaviorSubject';
import { CategoriesService, Category } 						from '../categories/categories.service';
import { FacetService } 									from './facet.service';
import { MalihuScrollbarModule, MalihuScrollbarService } 	from 'ngx-malihu-scrollbar';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-facet',
  templateUrl: './facet.component.html',
  styleUrls: ['./facet.component.css'],
  providers: [MalihuScrollbarService]
})

export class FacetComponent implements OnInit {
	@ViewChild('facetCol') elfacetCol: ElementRef;
	@Input() test2WayBinding;
	@Output() outputEvent = new EventEmitter();
	private _checkedFacetFromProduct = new BehaviorSubject<Facet[]>([]);
	private _productSeriesListFromConfiguration = new BehaviorSubject<number[]>([]);
	
	categoryId: number;
	commonFacetProducts: string[];
	allSelectedFacetProducts = [];
	allCategories: Category[]; categoryList: Category[];
	fullFacets: Facet[]; facets: Facet[]; checkedFacets: Facet[]; selectedFacetSets: SelectedFacet[];
	getArrayUnique = require('array-unique');
	getArrayUnion = require('array-union');
	getIntersection = require('array-intersection');
	loadFacetFromCache: boolean = false;
	prodFacetRequest: Promise<Facet[]>;
	prodLoading: boolean = true;

	@Input() set facetFromProduct(value) {
		this._checkedFacetFromProduct.next(value);
	};

	@Input() set categorySeriesList(value) {
		this._productSeriesListFromConfiguration.next(value);
	};

	constructor(
		private facetService: FacetService,
		private route: ActivatedRoute,
		private location: Location,
		private categoryService: CategoriesService,
		private mScrollbarService: MalihuScrollbarService) { 
		
		this.checkedFacets = [];
		this.selectedFacetSets = [];
		this.commonFacetProducts = [];
		this.facets = [];
	}
	
	ngOnInit() {
		this.route.params
			.switchMap((params: Params) => {
				this.categoryId = + params['id'];
				this.categoryList = [];
				this.test2WayBinding = [];
				this.checkedFacets = [];
				this.selectedFacetSets = [];
				this.commonFacetProducts = [];
				this.fullFacets = this.facets = [];
				this._productSeriesListFromConfiguration.next([]);
				this.loadFacetFromCache = false;
				this.prodLoading = true;
				return this.facetService.getCacheFacets({ContentTypeId: 31000, ParentId: this.categoryId, FacetClass: 'FacetedSearch'})
			})
			//.subscribe(facets => this.fullFacets = this.facets = facets);
			//.subscribe(categorySeriesPromise => this.loadCategorySeries(categorySeriesPromise));
			.subscribe(categorySeriesPromise => {
				if (categorySeriesPromise.length == 0) {
					return;
				}
				this.prodFacetRequest = null;
				this.loadFacetFromCache = true;
				this.fullFacets = this.facets = categorySeriesPromise;

				// GroupFacet: A-Z. Descending order, based on count number in parentheses
				this.fullFacets.sort(function (a, b) {
					 return a.facetName.localeCompare(b.facetName) || b.productKeys.length - a.productKeys.length;
				});	
				this.prodLoading = false;
				setTimeout(() => {
					this.checkToInitCustomCrollbar();
				}, 500);
			});
		
		this._productSeriesListFromConfiguration.subscribe(x => this.loadCategorySeries(x));

		this._checkedFacetFromProduct.subscribe(x => {
			this.checkedFacets = x.slice();
			this.startNarrowData();
		});
	}

	ngAfterViewInit() {
		//this.checkToInitCustomCrollbar();
	}

	debugFacet(): void {
		this.facets = this.fullFacets.filter(x => x.facetName.indexOf('10') > -1);
	}

	checkToInitCustomCrollbar(): void {
		this.mScrollbarService.destroy('#productsFacets .details_div');
		if (jQuery('#product_attributes .details_div').length > 0) {
			this.mScrollbarService.initScrollbar('#productsFacets .details_div', { axis: 'y', theme: 'dark-thick', scrollButtons: { enable: true } });
			return;
		}
		setTimeout(() => {
			this.checkToInitCustomCrollbar();
		}, 500);
	}

	loadCategorySeries (categories: number[]): void {
		if (categories.length == 0 || this.loadFacetFromCache || this.fullFacets.length > 0) return;
		//this.allCategories = this.categoryList = categories;
		//this.getLeafNodes(categories.filter(x => x.parentId == this.categoryId));
		this.prodFacetRequest = this.facetService
			.getFacets({contentTypeId: 31000, facetClass: 'FacetedSearch', categoryIds: categories})
			/* .then(x => {
				if (this.loadFacetFromCache) return;
				this.fullFacets = this.facets = x;
				setTimeout(() => {
				this.checkToInitCustomCrollbar();
				}, 500);
			}) */
			;

		this.prodFacetRequest.then(x => {
			if (this.loadFacetFromCache) return;
			this.facetService.getCacheFacets({ContentTypeId: 31000, ParentId: this.categoryId, FacetClass: 'FacetedSearch', CategoryIds: categories});
			this.fullFacets = this.facets = x;
			
			// GroupFacet: A-Z. Descending order, based on count number in parentheses
			this.fullFacets.sort(function (a, b) {
				return a.facetName.localeCompare(b.facetName) || b.productKeys.length - a.productKeys.length;
		   });
		    this.prodLoading = false;
			setTimeout(() => {
			this.checkToInitCustomCrollbar();
			}, 500);
		});
	}

	getLeafNodes(categoryItems: Category[]): void {
		if (categoryItems.length == 0) {
			this.categoryList = this.allCategories.filter(x => x.id == this.categoryId);
			return;
		}
		categoryItems.forEach(item => {
			var validChildren = this.allCategories.filter(category => category.parentId == item.id);
			if (validChildren.length == 0)
				this.categoryList.push(item);
			else {
				this.getLeafNodes (validChildren);
			}
		});
	}

	
	getFacets(categoryId: number): void {
		this.facetService.getFacets({categoryId: categoryId}).then(facets => this.fullFacets = this.facets = facets);
	}
	
	getProductCount(prodList): string {
		return (prodList.match(new RegExp(",", "g")) || []).length + 1;
	}
	
	checkFacetStatus(facetItem) {
		var existProd = this.getArraysIntersection([this.test2WayBinding, facetItem.productKeys]);
		if (existProd.length == 0) {
			return false;
		}

		var isValid = true;
		if (this.selectedFacetSets.length == 1) return isValid;

		var commonProd = [];
		var crrItemProd = facetItem.productKeys.slice();
		var otherSelectedFacets = this.selectedFacetSets.filter(facet => {
							return (facet.facetName != facetItem.facetName);
						});

		otherSelectedFacets.forEach(x => {
			x.productKeys.forEach (y => {
				var tmp = this.getArraysIntersection([crrItemProd, y]);
				if (tmp.length == 0) {
					isValid = false;
					return;
				}
			});

			if (!isValid) return;
		});

		return isValid;
	}
	
	facetNarrow() {
		if (this.test2WayBinding.length == 0) {
			this.facets = this.fullFacets;
			return;
		}
		
		if (typeof (this.facets) == 'undefined' || this.facets == undefined)
			this.facets = [];
		this.facets = this.fullFacets.filter(facet => {
			if (this.selectedFacetSets.length == 1 && facet.facetName == this.selectedFacetSets[0].facetName) {
				return true;
			}
				
			return this.checkFacetStatus(facet);
		});
	}

	updateSelectedFacets (): void {
		this.selectedFacetSets = [];
		this.commonFacetProducts = [];
		this.allSelectedFacetProducts = [];
		if (this.checkedFacets.length == 0) return;
		this.checkedFacets.forEach((el) => {
			var facetSelected = false;
			for (var i = 0; i < this.selectedFacetSets.length; i++) {
				if (this.selectedFacetSets[i].facetName == el.facetName) {
					this.selectedFacetSets[i].productKeys.push(el.productKeys);
					facetSelected = true;
				}
			}

			if (!facetSelected){
				var newSelected = new SelectedFacet();
				newSelected.facetName = el.facetName;
				newSelected.productKeys = [];
				newSelected.productKeys.push(el.productKeys); 
				this.selectedFacetSets.push(newSelected);
			}

			this.allSelectedFacetProducts.push(el.productKeys);
		});
	}
	
	facetChecked (e, value): void {
		this.mScrollbarService.destroy('#productsFacets .details_div');
		var prodList = value.productKeys;
		if(e.target.checked) {
			this.checkedFacets.push(value);
		}
		else {
			this.checkedFacets.splice(this.checkedFacets.indexOf(value), 1);
		}
		this.validateLastChecked();
		this.startNarrowData();
		setTimeout(() => {
			let el: HTMLElement = this.elfacetCol.nativeElement as HTMLElement;
			el.click();
		}, 500);
	}

	validateLastChecked(): void {
		var lastChecked = this.checkedFacets[this.checkedFacets.length - 1];
		var tmpCheckedList = this.checkedFacets.filter(x => {
			return x.facetName != lastChecked.facetName;
		});

		tmpCheckedList.forEach(x => {
			var commonProd = this.getArraysIntersection([lastChecked.productKeys, x.productKeys]);
			if (commonProd.length == 0) {
				this.checkedFacets.splice(this.checkedFacets.indexOf(x), 1);
			}
		});
	}

	startNarrowData(): void {
		this.updateSelectedFacets();
		this.getCommonProduct();
		this.facetNarrow();
		this.outputEvent.emit({prodKeys: this.test2WayBinding, selectedFacets: this.checkedFacets});
		setTimeout(() => {
			this.checkToInitCustomCrollbar();
		}, 500); 
	}

	getCommonProduct(): void {
		var groupedSelectedFacets = [];
		this.selectedFacetSets.forEach(sf => {
			groupedSelectedFacets.push(this.getArrayUnion(...sf.productKeys));
		});
		if (this.selectedFacetSets.length == 0)
			this.commonFacetProducts = groupedSelectedFacets;
		else
			this.commonFacetProducts = this.getArraysIntersection(groupedSelectedFacets);
		
		this.test2WayBinding = this.commonFacetProducts = this.getArrayUnique(this.commonFacetProducts);
	}

	isFacetActive (facetList): boolean {
		var isActive = false;
		var activeItems = facetList.value.filter(item => this.isFacetCheck(item));
		isActive = activeItems.length > 0;
		return isActive;
	}

	isFacetCheck (facetItem): boolean {
		return this.checkedFacets.indexOf(facetItem) >= 0;
	}

	countFacetProduct(facetItem): number {
		if (	this.selectedFacetSets.length == 0 || 
				(this.selectedFacetSets.length == 1 && this.selectedFacetSets[0].facetName == facetItem.facetName)
			) {
			return facetItem.productKeys.length;
		}
		var commonProd = [];

		var crrItemProd = facetItem.productKeys.slice();
		var otherSelectedFacets = this.selectedFacetSets.filter(facet => {
							return (facet.facetName != facetItem.facetName);
						});

		otherSelectedFacets.forEach(x => {
			x.productKeys.forEach (y => {
				var tmp = this.getArraysIntersection([crrItemProd, y]);
				if (tmp.length > 0)
					commonProd = commonProd.concat(tmp);
			});
		});
		commonProd = this.getArraysIntersection([commonProd, this.commonFacetProducts]);
		commonProd = this.getArrayUnique(commonProd);

		return commonProd.length;
	}

	getArraysIntersection(theArrays) {
		return this.getIntersection(...theArrays);
	}
}

export class Facet {
	productKeys: string[];
	productKey?: string;
	facetClass: string;
	facetName: string;
	facetValue: string;
	sequence: string;
}

export class SelectedFacet {
	productKeys: any[];
	facetName: string;
}