var GLOBAL_VAR_TEST = 'hello world';
var _tag;

$(document).ready(function(){
    try {
        var Instanceid = 1;
        var iframeName = window.name;
        if (!iframeName) iframeName = "_opt" + Instanceid;
        $(document).iframed("startClient", iframeName, "", function() {
            var hold = ($("#hf-configuration-wrapper").height() < 100 ? 1200 : ($("#hf-configuration-wrapper").height() + 75));
            return {
                x: 665,
                y: hold
            };
        });

    } catch (ex) {
        //console.log(ex);
    };
    
    $(document).on('click', "#left-section .title_expand", function(){
        if($(this).parent().hasClass("prodFacetsExpand")){
            $(this).parent().removeClass("prodFacetsExpand");
        }
        else {
            $("#product_attributes" ).find(".content_legend" ).removeClass("prodFacetsExpand");
            $(this).parent().addClass("prodFacetsExpand");
        }
    });
    $(document).on('click', '[data-toggle="slide-collapse"]', function() {
        $navMenuCont = $($(this).data('target'));
        $navMenuCont.animate({'width':'toggle'}, 350);
    });
    $(document).on('click', 'a', function() {
        // console.log('click');
        // //<![CDATA[
        // var _tag = new WebTrends();
		// _tag.dcsGetId();
        // _tag.DCSext.csp_stype = _tag.DCSext.csp_sname = "showcase";
        // //]]>> 
        // console.log(_tag, GLOBAL_VAR_TEST);
        // try{parent.postMessage(window.location.href,"*");}catch(ex){console.log(ex);}
        
    });
       
    var checker = true;
    $(document).on("click", 'button.navbar-toggle', function() {  
        if (checker) {
            //menu show -> hide
            $(".overlay2").removeClass('hidden');
            $(".close-x").removeClass('hidden');
            $(".open").addClass('hidden');
            $(".navbar-toggle").addClass('pos-fixed');
        } else {
            $(".overlay2").addClass('hidden');
            $(".close-x").addClass('hidden');
            $(".open").removeClass('hidden');
            $(".navbar-toggle").removeClass('pos-fixed');
        }
        var rightpx = checker ? "250px" : "0px";
        $(this).animate({right: rightpx},300);
        checker = !checker;
    });

    // menu hide after select nav item
    if( $(window).width() < 660) {       
        $(document).on("click", "#menu a:not(.dropdown-toggle)", function(e) { 
            $("button.navbar-toggle").click(); 
            checker = true;
        });
    }

    // sort in mobile
    // $(document).on("click", ".mobile_sort_by_filter", function(){
    //     $(".tooltip_sec").toggleClass('hidden');
    // });
    $(document).on("click", ".tooltip_sec li", function(e) { 
        document.getElementById("orderByText").textContent = e.target.outerText;
        $(".tooltip_sec").addClass('hidden');
    });

    //filer in mobie
    $(document).on("click", ".mobile_facet_filter", function(){
        /* $("#filterDiv").addClass("open_filter");
        $("#product_catrgory").removeClass('hidden-xs hidden-sm');
        $("#product_attributes").removeClass('hidden-xs hidden-sm'); */  
        $(".tooltip_sec").addClass('hidden');
        $("#filterDiv").toggleClass("open_filter");
        $("#product_catrgory").toggleClass('hidden-xs hidden-sm');
        $("#product_attributes").toggleClass('hidden-xs hidden-sm');
    });
    $(document).on("click", ".filter_done_btn", function(e){
        $("#filterDiv").removeClass("open_filter");
        $("#product_catrgory").addClass('hidden-xs hidden-sm');
        $("#product_attributes").addClass('hidden-xs hidden-sm');
        e.stopPropagation();
    });

    $(document).on("click", "#clear_all_btn", function(e){
        e.stopPropagation();
        
    });
});

function initHomeCarousel() {
    if ($('#home-carousel').length > 0 && $('#home-carousel').find('img').length > 0) {
        $('#home-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            autoplay: true
        });
    }

    if ($('.sliderLeft').length > 0 && $('.sliderLeft').find('div').length > 0) {
        $('.sliderLeft').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            autoplay: true
        });
    }

    if ($('#slick-thumb').length > 0 && $('#slick-thumb').find('li').length > 0) {
        $("#slick-thumb").slick({
            infinite: true,
            slidesToShow: 4,
            arrows: false,
            slidesToScroll: 1,
            autoplay: true,
            focusOnSelect: true,
            asNavFor: '#home-carousel, .sliderLeft',
        });
    }
}

function initHomeProductCarousel () {
    if ($('.list-product').length == 0) return;
    $(".list-product").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
            }
        ]
    });
}
