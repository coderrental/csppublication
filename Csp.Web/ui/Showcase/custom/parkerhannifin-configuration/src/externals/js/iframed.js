(function($) {

    var attached = false;
    var clientFrameId;
    var parentUrl;
    var intervalId;
    var legacy = (typeof (window.postMessage) == "undefined");

    var obj2str = function(obj) {
        try {
            var str = "";
            for (var key in obj) {
                str += key + "=" + obj[key] + "&";
            }
            return str.substring(0, str.length - 1);
        } catch (ex) { }
    };

    var str2obj = function(str) {
        try {
            var obj = {};
            var arr = str.split("&");
            for (var i = 0; i < arr.length; i++) {
                var pair = arr[i].split("=");
                obj[pair[0]] = pair[1];
            }
            return obj;
        } catch (ex) {
            return null;
        }
    };

    var size_function = function() {
        var MAX_HEIGHT = null, MAX_WIDTH = null;
        if (window.innerHeight) {
            MAX_HEIGHT = window.innerHeight;
            MAX_WIDTH = window.innerWidth;
        } else if (document.documentElement.clientHeight == 0) {
            MAX_HEIGHT = document.body.clientHeight;
            MAX_WIDTH = document.body.clientWidth;
        } else {
            MAX_HEIGHT = document.documentElement.clientHeight;
            MAX_WIDTH = document.documentElement.clientWidth;
        }

        if (MAX_WIDTH == 300) MAX_WIDTH = 730;

        return { x: MAX_WIDTH, y: MAX_HEIGHT };
    };
	

    var listener = function(e) {
        var data = str2obj(e.data);

        if (data && data.id) {
            if (data.id == "TB_iframeContent") {
                var MAX_HEIGHT = null;
                if (window.innerHeight)
                    MAX_HEIGHT = window.innerHeight;
                else if (document.documentElement.clientHeight == 0)
                    MAX_HEIGHT = document.body.clientHeight;
                else
                    MAX_HEIGHT = document.documentElement.clientHeight;

				var o = $("iframe[id="+data.id+"]")[0];
				
                if (data.y == 0 || data.y > MAX_HEIGHT && MAX_HEIGHT > 0) {
                    data.y = MAX_HEIGHT - 50;
                }

                var MAX_WIDTH = document.getElementById("TB_window").offsetWidth;
                if (data.x == 0 || data.x > MAX_WIDTH && MAX_WIDTH > 0) {
                    data.x = MAX_WIDTH - 50;
                }
                if (data.x == 0) {
                    data.x = 700;
                }
				if(typeof (o) != 'undefined')
					{
					o.style.height = parseInt(data.y) + "px";
					o.style.width = parseInt(data.x) + "px";
					}
            } else {
				var o = $("iframe[id="+data.id+"]")[0];
                var mf = document.getElementById("mainFlyout");
                if(typeof (o) != 'undefined')
					{
					o.style.height = parseInt(data.y) + "px";
					o.style.width = parseInt(data.x) + "px";
					}
                if (mf) {
                    mf.style.height = parseInt(data.y) + "px";
                    mf.style.width = parseInt(data.x) + "px";
                }
            }
        }
		
    };

    var listener_legacy = function() {
        var msg;
        var href = window.location.href;
        var index = null;
        try {
            index = href.indexOf("#");
            if (index != -1) {
                msg = href.substring(index + 1);
                if (msg) {
                    try {
                        listener({ data: msg });
                    } catch (Error) { }
                    window.location = href.substring(0, index + 1);
                }
            }
        } catch (Error) {

        }
    };


    var methods = {
        constructor: function() {
            return $(this)
				.bind({
				    resize: function(event, x, y) {
				        $(this).width(x);
				        $(this).height(y);
				    }
				});
        }
		, startHost: function() {
		    if (!attached) {
		        if (typeof (window.addEventListener) != 'undefined')
		            window.addEventListener('message', listener, false);
		        else
		            window.attachEvent('onmessage', listener);
		        attached = true;
		    }
		}
		, startClient: function(elementId, parent_url, custom_size) {
		    if (window != window.top) {
		        if (custom_size) size_function = custom_size;
		        clientFrameId = elementId;
		        parentUrl = parent_url;

		        $(document).click(function() {
		            methods.sendTop(size_function());
		        });

		        $(document).ready(function() {
		            methods.sendTop(size_function());
		        });
		        
				
				$(window).load(function() {
					setTimeout(function(){
						methods.sendTop(size_function());
					},50);
		        });
				$(window).load(function() {
					setTimeout(function(){
						methods.sendTop(size_function());
					},2000);
		        });
		    }
		}
		, sendTop: function(msg) {
		    msg.id = clientFrameId;
			window.parent.postMessage(obj2str(msg), "*");
		    window.top.postMessage(obj2str(msg), "*");
		}
    };

    var methods_legacy = {
        constructor: methods.constructor
		, startHost: function() {
		    if (!attached) {
		        intervalId = setInterval(listener_legacy, 25);
		        attached = true;
		        setTimeout(function() {
		            clearInterval(intervalId);
		            intervalId = setInterval(listener_legacy, 200);
		        }, 5000);
		    }
		}
		, startClient: function(elementId, parent_url, custom_size) {
		    if (window != window.top) {
		        if (custom_size) size_function = custom_size;
		        clientFrameId = elementId;
		        var index = parent_url.indexOf("#");
		        if (index == -1) {
		            parentUrl = parent_url;
		        } else {
		            parentUrl = parent_url.substring(0, index);
		        }

		        $(document).click(function() {
		            methods_legacy.sendTop(size_function());
		        });

		        $(document).ready(function() {
		            methods_legacy.sendTop(size_function());
		        });
				
				$(window).load(function() {
					setTimeout(function(){
						methods_legacy.sendTop(size_function());
					},50);
		        });
		    }
		}
		, sendTop: function(msg) {
		    msg.id = clientFrameId;
		    if (parentUrl) {
		        var url = parentUrl + "#" + obj2str(msg);
		        window.top.location = url;
		    }
		}
    };

    $.fn.iframed = function(method) {
        if (legacy) {
            if (methods_legacy[method]) {
                return methods_legacy[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods_legacy.constructor.apply(this, arguments);
            } else {
                $.error('Method (legacy) ' + method + ' does not exist on jQuery.iframed');
            }
        } else {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if (typeof method === 'object' || !method) {
                return methods.constructor.apply(this, arguments);
            } else {
                $.error('Method ' + method + ' does not exist on jQuery.iframed');
            }
        }
    };

}(jQuery));