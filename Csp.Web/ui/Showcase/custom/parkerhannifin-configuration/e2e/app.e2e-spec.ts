import { HannifinConfigurationPage } from './app.po';

describe('hannifin-configuration App', () => {
  let page: HannifinConfigurationPage;

  beforeEach(() => {
    page = new HannifinConfigurationPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
