if ( typeof (CspUtils) == "undefined") {
    CspUtils = {
        scriptMap : {},
        log : function() {
            if ( typeof (console) == "undefined") {
                console = {};
                console.log = {};
            };
            console.log(arguments);
        },
        LoadScript : function(url) {
            try {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = url;
                document.getElementsByTagName('head')[0].appendChild(script);
            } catch (ex) {
            }
        },
        LoadCss : function(url) {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = url;
            document.getElementsByTagName('head')[0].appendChild(link);
        },
        Load : function(url, callback) {
            var i = url.indexOf("?");
            var name = (i == -1) ? url : url.substring(i + 1);
            if ( typeof (this.scriptMap[name]) == "undefined") {
                this.scriptMap[name] = {
                    ready : false,
                    queue : [callback]
                };
                this.LoadScript(url);
            } else if (this.scriptMap[name].ready) {
                var retObj = this.scriptMap[name].retObj;
                callback(retObj);
            } else
                this.scriptMap[name].queue.push(callback);
        },
        ProcessQueue : function(name) {
            var queue = this.scriptMap[name].queue;
            var retObj = this.scriptMap[name].retObj;
            for (var i = 0; i < queue.length; i++) {
                (function(func, retObj) {
                    try {
                        func(retObj);
                    } catch (error) {
                        CspUtils.log(error);
                    }
                })(queue[i], retObj);
            }
        },
        ProcessEvent : function(wtTag, node, type, value) {
            var i = 0;
            for (var a in wtTag.DCSext) {
                try {
                    i = i + wtTag.DCSext[a].length;
                } catch (e) {
                }
            }
            if (i > 1100) {
                return;
            }

            if (wtTag.DCSext["ConversionType"])
                wtTag.DCSext["ConversionType"] = wtTag.DCSext["ConversionType"] + ";" + "inline_showcase";
            else
                wtTag.DCSext["ConversionType"] = type;
            if (wtTag.DCSext["ConversionShown"])
                wtTag.DCSext["ConversionShown"] = wtTag.DCSext["ConversionShown"] + ";" + type;
            else
                wtTag.DCSext["ConversionShown"] = value;

            if (wtTag.DCSext["ConversionContent"])
                wtTag.DCSext["ConversionContent"] = wtTag.DCSext["ConversionContent"] + ";" + value;
            else
                wtTag.DCSext["ConversionContent"] = value;
            this.$(node).bind("click", function() {
                var olDCSext = wtTag.DCSext;
                wtTag.DCSext["ConversionShown"] = null;
                wtTag.DCSext["ConversionClick"] = value;
                wtTag.DCSext["ConversionType"] = type;
                wtTag.DCSext["ConversionContent"] = value;
                wtTag.dcsMultiTrack();
                wtTag.DCSext = olDCSext;
            });
        },
        ProcessInlineTag : function(className, $, wtTag) {
            var l = $("div." + className);
            var content = {
                path : "http://ui.syndication.tiekinetix.net/",
                project : cspProjectName
            };
            var getFileName = function(path) {
                var i = path.lastIndexOf('/');
                if (i == -1)
                    i = 0;
                return path.substring(i + 1);
            };
            if (l.length > 0) {

                // apply translation
                l.find("[cspObj='translation']").each(function(arg) { 
                    try {
                        var el = $(this);
                        var value = CspUtils.Translation[cspLngCode.toLowerCase()][el.attr("cspValue")];
                        if(value&&value!="") el.html(value);
                    } catch(ex) {}                    
                });


                $(".csp_image_gallery", l).each(function(index) {
                    var tag = $(this), imgUrl = "";
                    tag.append("<span class='zoom-icon'><img src='" + content.path + "ui/universalresources/images/zoom.png' /></span>");
                    imgUrl = (tag.attr("href").lastIndexOf("/") > 0 ? tag.attr("href").substring(tag.attr("href").lastIndexOf("/") + 1) : tag.attr("href"));
					CspUtils.ProcessEvent(wtTag, tag, "ImageGallery", getFileName(tag.attr("href")));
                });
                $('.csp_image_gallery').fancybox({
                    openEffect : 'none',
                    closeEffect : 'none',
                    nextEffect : 'none',
                    prevEffect : 'none',
                    scrolling : 'no'
                });

                $(".csp_video", l).each(function() {
                    var tag = $(this);
                    if (tag.attr('href').indexOf('.wmv') != -1) {
                        tag.attr("class", tag.attr("class").replace("swf", "iframe"));
                        var vurl = function(href) {
                            var key = "video";
                            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
                            var qs = regex.exec(href);
                            if (qs == null)
                                return "";
                            else
                                return qs[1];
                        };
                        //var wmvContentPath = content.path;
                        var vid = window.escape(vurl(tag.attr("href")));
                        var preview = tag.children(0).attr('src') != "" ? "&preview=" + window.escape(tag.children(0).attr('src')) : "";
                        // This is to keep the content path when testing in a different environment than production
                        //tag.attr("href", wmvContentPath.replace('http', 'https') + "/ui/universalresources/cspwmvplayer/cspwmvplayer.html?video=" + vid + preview);
                        // This one always points to the player in production
                        tag.attr("href", content.path + "ui/universalresources/cspwmvplayer/cspwmvplayer.html?video=" + vid + preview);
                    }
                    tag.append("<span class='play-icon'><img src='http://ui.syndication.tiekinetix.net/ui/universalresources/images/video_overlay.png' /></span>");
                    // tag.fancybox($.extend(eval(tag.attr('cspVars')), { 'swf': { 'wmode': 'transparent', 'flashvars': "autostart=1&file=" + tag.attr("csp_video")} }));
                    var videoUrl = (tag.attr("csp_video").lastIndexOf("/") > 0 ? tag.attr("csp_video").substring(tag.attr("csp_video").lastIndexOf("/") + 1) : tag.attr("csp_video"));
                    // inlineReport.call(tag, options, "Video", videoUrl);
                    // types = types + options.SyndicationType.toLowerCase() + ";";
                    // shows = shows + "Video;";
                    // values = values + videoUrl + ";";
                    CspUtils.ProcessEvent(wtTag, tag, "Video", getFileName(videoUrl));
                });

                $(".csp_video").fancybox({
                    maxWidth : 600,
                    maxHeight : 400,
                    fitToView : false,
                    width : 600,
                    height : 400,
                    autoSize : false,
                    closeClick : false,
                    openEffect : 'none',
                    closeEffect : 'none',
                    scrolling : 'no',
                    helpers : {
                        media : {}
                    }
                });

                $(".csp_espec", l).each(function() {
                    var tag = $(this);
                    var target = $("#" + tag.attr("csp_espec"));
                    if (target.length > 0) {
                        try {
                            var espec = eval("(" + tag.text() + ")");

                            if (espec.Espec) {
                                var html = "<table cellpadding='0' cellspacing='0'>";

                                var className = "";
                                for (var i = 0; i < espec.Espec.length; i++) {
                                    html += "<tr><th colspan=2>" + espec.Espec[i].SN + "</th></tr>";
                                    for (var j = 0; j < espec.Espec[i].R.length; j++) {
                                        if (j % 2 == 0)
                                            className = "even";
                                        else
                                            className = "odd";
                                        html += "<tr class='" + className + "'><td>" + espec.Espec[i].R[j].HT + "</td><td>" + espec.Espec[i].R[j].BT + "</td></tr>";
                                    }
                                }
                                html += "</table>";
                                target.html(html);
                            }
                        } catch (e) {
                        }
                    }
                });
                // end of tech spec
                $(".feature-box").fancybox({
                    helpers : {
                        media : {}
                    }
                });
                $(".csp_pdf_container", l).each(function() {
                    var tag = $(this);
                    var PDFUrl = (tag.attr("csp_pdf").lastIndexOf("/") > 0 ? tag.attr("csp_pdf").substring(tag.attr("csp_pdf").lastIndexOf("/") + 1) : tag.attr("csp_pdf"));
                    // inlineReport.call(tag, options, "PDF", PDFUrl);
                    // types = types + options.SyndicationType.toLowerCase() + ";";
                    // shows = shows + "PDF;";
                    // values = values + PDFUrl + ";";
                    CspUtils.ProcessEvent(wtTag, tag, "PDF", getFileName(PDFUrl));
                });

                // inlineReportOnLoad(options, types, shows, values);
                // l.removeClass(("_csp_inline_showcase" + options.elementId));
                // l.attr("csp_state", options.elementId);
                //l.attr("id", l.attr("id") + "_ready");
            }
        }
    };
}

function AttachEvents(util) {
    if ( typeof (WebTrends) == "undefined") {
        setTimeout(function() {
            AttachEvents(util);
        }, 500);
        return;
    }
    var CspWebTrendTag = new WebTrends();
    CspWebTrendTag.dcsGetId();
    CspWebTrendTag.DCSext.ConversionType = CspWebTrendTag.DCSext.ConversionClick = CspWebTrendTag.DCSext.ConversionShown = CspWebTrendTag.DCSext.ConversionContent = "inline_showcase";
    if ( typeof (cspMfrPn) != "undefined") {
        //CspWebTrendTag.DCSext.csp_pageTitle = CspWebTrendTag.DCSext.sku_key = CspWebTrendTag.DCSext.ConversionClick = CspWebTrendTag.DCSext.ConversionShown = CspWebTrendTag.DCSext.ConversionContent = cspMfrPn;
        CspWebTrendTag.DCSext.sku_key = CspWebTrendTag.DCSext.ConversionClick = CspWebTrendTag.DCSext.ConversionShown = CspWebTrendTag.DCSext.ConversionContent = cspMfrPn;
    }
    CspWebTrendTag.DCSext.csp_stype = "direct";
    CspWebTrendTag.DCSext.csp_sname = "inline_showcase";
    CspWebTrendTag.DCSext.embed_url = window.location.href;
    if ( typeof (cspConsumerInfo) != "undefined") {
        CspWebTrendTag.DCSext.csp_companyId = cspConsumerInfo.companies_Id;
        CspWebTrendTag.DCSext.csp_country = cspConsumerInfo.country;
        
        var WTti_Tag = document.createElement('meta');
        WTti_Tag.name = "WT.ti";
        WTti_Tag.content = window.unescape(document.title);
        if (window.unescape(document.title).length > 20) {
            WTti_Tag.content = window.unescape(document.title).substring(0, 20) + "...";
        }
        document.getElementsByTagName('head')[0].appendChild(WTti_Tag);
        /*
        CspWebTrendTag.DCSext.csp_pageTitle = document.title;
        if (CspWebTrendTag.DCSext.csp_pageTitle.length > 65)
            CspWebTrendTag.DCSext.csp_pageTitle = CspWebTrendTag.DCSext.csp_pageTitle.substring(0, 65);
        */
        CspWebTrendTag.DCSext.csp_companyname = cspConsumerInfo.companyname;
        CspWebTrendTag.DCSext.language = cspConsumerInfo.lngDesc;
        if ( typeof (cspConsumerInfo.sId) != "undefined") {
            //CspWebTrendTag.DCSext.vendorName = cspProjectName;
			CspWebTrendTag.DCSext.vendorName = cspConsumerInfo.sName;
            //cspConsumerInfo.sName;
            CspWebTrendTag.DCSext.csp_vendorid = cspConsumerInfo.sId;
        }
        util.ProcessInlineTag("cspisicontainer", util.$, CspWebTrendTag);
        CspWebTrendTag.dcsCollect();
    }
}
//translation
CspUtils.Translation = {"en":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"de":{"video":"Video","flash":"Flash Showcase","imagegallery":"Galerie","features":"Eigenschaften","pdf":"PDF"} ,"fr":{"video":"Vidéo","flash":"Présentation Flash","imagegallery":"Galerie","features":"Caractéristiques","pdf":"PDF"} ,"es":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"it":{"video":"Video","flash":"Vetrina Flash","imagegallery":"Galleria","features":"Caratteristiche","pdf":"PDF"} ,"p99":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"pt":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ja":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"zh":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"tr":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"nl":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Kenmerken","pdf":"PDF"} ,"da":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"no":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"sv":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ru":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ko":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eza":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"uk":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ca":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eie":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"eau":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"ptb":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"esl":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"cs":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"zht":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"enu":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"} ,"esc":{"video":"Video","flash":"Flash Showcase","imagegallery":"Gallery","features":"Features","pdf":"PDF"}}; 
//update
CspUtils.LoadCss("http://ui.syndication.tiekinetix.net/ui/showcase/js/cspfancybox/source/jquery.fancybox.css?v=2.1.3");
CspUtils.LoadCss("http://ui.syndication.tiekinetix.net/ui/showcase/js/cspfancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7");
// CspUtils.LoadCss("http://ui.syndication.tiekinetix.net/js/isi/template.css");
CspUtils.LoadCss("http://ui.syndication.tiekinetix.net/ui/inline/css/template.css");

if (typeof (cspCssPath) != "undefined") {
    CspUtils.LoadCss(cspCssPath);
}
// if ( typeof (cspCssPath1) != "undefined" && cspCssPath1 != "") {
    // CspUtils.LoadCss(cspCssPath1);
// }
CspUtils.Load("http://ui.syndication.tiekinetix.net/global/files/" + cspProjectName + "/script/webtrends.js?" + cspProjectName, function() {});
CspUtils.Load("http://ui.syndication.tiekinetix.net/ui/showcase/js/jquery_min.js?jquery_min.js", function($) {
    CspUtils.Load("http://ui.syndication.tiekinetix.net/ui/showcase/js/cspfancybox/source/jquery.fancybox.pack.js?jquery-fancybox-pack.js", function($) {
        CspUtils.Load("http://ui.syndication.tiekinetix.net/ui/showcase/js/cspfancybox/source/helpers/jquery.fancybox-media.js?jquery.fancybox-media.js", function($) {
            CspUtils.Load("http://ui.syndication.tiekinetix.net/ui/showcase/js/cspfancybox/source/helpers/jquery.fancybox-thumbs.js?jquery.fancybox-thumbs.js", function($) {
                AttachEvents(CspUtils);
            });
        });
    });
}); 