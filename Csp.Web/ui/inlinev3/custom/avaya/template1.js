setTimeout(function(){
CSP_GLOBAL_globalId.cspInlineTranslator = {'1': {'video': 'Videos','flash': 'Flash Showcase','imagegallery': 'Gallery','features': 'Features','pdf': 'Case Studies'},'2': {'video': 'Videos','flash': 'Flash Showcase','imagegallery': 'Galerie','features': 'Eigenschaften','pdf': 'PDF'},'3': {'video': 'Vidéo','flash': 'Présentation Flash','imagegallery': 'Galerie','features': 'Caractéristiques','pdf': 'PDF'},'4': {'video': 'Videos','flash': 'Flash Showcase','imagegallery': 'Gallery','features': 'Features','pdf': 'Case Studies'},'5': {'video': 'Video','flash': 'Vetrina Flash','imagegallery': 'Galleria','features': 'Caratteristiche','pdf': 'PDF'},'17': {'video': 'Videos','flash': 'Flash Showcase','imagegallery': 'Gallery','features': 'Features','pdf': 'Case Studies'} };

CSP_GLOBAL_globalId.jQuery('.CspInlineContainer img').each(function(){
	var imgUrl = CSP_GLOBAL_globalId.jQuery(this).attr('src');
	var crrImgObj = CSP_GLOBAL_globalId.jQuery(this);
	
	if (imgUrl == '') {
		CSP_GLOBAL_globalId.jQuery(this).remove();
		return;
	}
	
	CSP_GLOBAL_globalId.jQuery('<img>', {
		src: imgUrl,
		error: function() { crrImgObj.remove(); }
	});
});

CSP_GLOBAL_globalId.jQuery('[cspObj=translation]','div.CspInlineContainer').each(function(){
	try {
		var el = CSP_GLOBAL_globalId.jQuery(this);
		if (el.attr('cspLngId') && el.attr('cspLngId') != "" && el.attr('cspLngId') != 0) {
			var value = CSP_GLOBAL_globalId.cspInlineTranslator[el.attr('cspLngId')][el.attr('cspValue')];
			if(value&&value!='') el.html(value);
		}
	} catch(ex) { if (typeof(console)!="undefined" && typeof(console.log)!="undefined") console.log(ex)}	
});
  
CSP_GLOBAL_globalId.jQuery('.csp-inline-tabs a').click(function (e) {
	e.preventDefault();
	var tabId = CSP_GLOBAL_globalId.jQuery(this).attr('href');

	CSP_GLOBAL_globalId.jQuery('.csp-inline-tab-current').removeClass('csp-inline-tab-current');
	CSP_GLOBAL_globalId.jQuery('.csp-inline-currenttabcontainer').removeClass('csp-inline-currenttabcontainer');
	CSP_GLOBAL_globalId.jQuery(this).parent('li').addClass('csp-inline-tab-current');
	CSP_GLOBAL_globalId.jQuery(tabId).addClass('csp-inline-currenttabcontainer');
});

var n = CSP_GLOBAL_globalId.jQuery("#documentsandvideos .CspInlineVideo").length;
if(n == 0 || (n == 1 && (CSP_GLOBAL_globalId.jQuery("#documentsandvideos .CspInlineVideo .CspVideoTitle").html()=='' && CSP_GLOBAL_globalId.jQuery("#documentsandvideos .CspInlineVideo .CspVideoDescription").html()==''))){
	CSP_GLOBAL_globalId.jQuery("#documentsandvideos .CspInlineVideo").hide();
	CSP_GLOBAL_globalId.jQuery("#documentsandvideos .CspInlineVideo").prev().hide();
}

CSP_GLOBAL_globalId.jQuery('.CspInlinePdf a').each(function() {
	var value = CSP_GLOBAL_globalId.jQuery(this).attr('href');
	CSP_GLOBAL_globalId.jQuery(this).attr('href', value.replace('http://entrypoints-production.herokuapp.com/assetdownload/?file=','http://global.tiekinetix.net/download/index.aspx?link='));
	if(value.substring(0,7) == 'http://')
		CSP_GLOBAL_globalId.jQuery(this).attr('target', '_blank');
});

},200);