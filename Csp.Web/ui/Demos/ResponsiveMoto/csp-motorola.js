
 function Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid) {
    this.cnid=cnid;
     this.depth = depth;
     this.id = id;
     this.pid = pid;
     this.text = text;
	 this.displayOrder=displayOrder;
	 this.lineage = lineage.split('/');
	 this.level = level;
	 this.desc = desc;
	 this.pn = pn;
	 this.rid = rid;
	 this.SearchUp = function(level){
		for(var i=0;i<this.lineage.length;i++){
			if(this.lineage[i]==this.id) {
				try{
					return this.lineage[i-level];
				}
				catch(e){
					return null;
				}
			}				
		}
		return null;
	 }
 }
$(document).ready(function() {
    setTimeout(function () {
        var a = document.getElementById('csp-tout-area');
        if (a && a.style) {
           a.style.height = a.offsetHeight + 'px'
        }    
    }, 500);  
});
 Array.prototype.findParentId = function(attrId, value) {
     for (var i = 0; i < this.length; i++) {
         if (this[i][attrId] == value) return i;
     }
     return -1;
 }
 Array.prototype.findByAttrs = function() {
     if (arguments.length % 2 != 0) throw "invalid name value pair arguments";
     var dict = [], i, j, r = false;
     for (i = 0; i < arguments.length; i = i + 2) {
         dict.push([arguments[i], arguments[i + 1]]);
     }
     for (i = 0; i < this.length; i++) {
         r = true;
         for (j = 0; j < dict.length; j++)
             r = r && (this[i][dict[j][0]] == dict[j][1]);
         if (r) break;
     }
     return (r ? i : -1);
 }
 
 var categories = {};
 (function(c) {
     c.list = [];
    
	/*c.Find = function(id) {
         var t = null;
         for (var i = 0; i < this.list.length; i++) {
             t = this.FindInTree(id, this.list[i]);
             if (t != null)
                 return t;
         }
         return null;
     }
	*/
	
     c.FindAll = function(id, level) {
         var t = null, results = [];
         for (var i = 0; i < this.list.length; i++) {
             t = this.FindAllInTree(id, level, this.list[i], results);
         }
         return results;
     } 
	
	/*
     c.FindByAttr = function(name, value) {
         var t = null, results = [];
         for (var i = 0; i < this.list.length; i++) {
             this.FindInTreeByAttr(name, value, this.list[i], results);
         }
         return results;
     }
     c.FindInTreeByAttr = function(name, value, tree, results) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t[name] == value)
             results.push(_t);
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             c.FindInTreeByAttr(name, value, _t.children[i], results);
         }
     }
	 */
	 
     c.FindAllInTree = function(id, level, tree, results) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t.id == id) {
             if (level != null && _t.level == level)
                 results.push(_t);
             else if (level == null)
                 results.push(_t);
         }
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             c.FindAllInTree(id, level, _t.children[i], results);
         }
     }
	 /*
     c.FindInTree = function(id, tree) {
         var _t = tree;
         if (_t == null) _t = this.list;
         if (_t.id == id) return _t;
         if (typeof (_t.children) == "undefined") return null;
         var r = null;
         for (var i = 0; i < _t.children.length; i++) {
             r = c.FindInTree(id, _t.children[i]);
             if (r != null)
                 return r;
         }
         return null;
     }
	 */
	 
	 
     c.Add = function(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid) {
         var len = this.list.length;
         if (len == 0 || (len > 0 && this.list[len - 1].depth <= depth))
             this.list.push(new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid));
         else if (len > 0) {
             for (var i = 0; i < len; i++) {
                 if (this.list[i].depth >= depth) {
                     this.list.splice(i, 0, new Category(cnid, depth, id, pid, text, displayOrder, lineage, level, desc, pn, rid));
                     break;
                 }
             }
         }
     }
	 
     c.Nest = function() {
         if (this.list.length > 0) {
             var i = this.list.length - 1, id, j, k;
             while (i >= 0) {
                 id = this.list.findParentId("id", this.list[i].pid);
                 if (id != -1) {
                     var temp = this.list.splice(i, 1);
                     if (typeof (this.list[id]["children"]) == "undefined")
                         this.list[id]["children"] = [];
                     temp[0].parent = this.list[id];
                     this.list[id].children.push(temp[0]);
                 }
                 i--;
             }
             //if we are missing a level, then we will automatically pull them back
             for (i = 0; i < this.list.length; i++) {
                 if (typeof (this.list[i].children) == "undefined") continue;
                 j = this.list[i].children.length - 1;
                 var cat, pcat;
                 while (j >= 0) {
                     cat = this.list[i].children[j];
                     if (cat.level > 1) {
                         // find the sibling that has the same level
                         for (k = 0; k < this.list[i].children.length; k++) {
                             if (this.list[i].children[k].level < cat.level && this.list[i].children[k].id == cat.id)
                                 break;
                             continue;
                         }
                         if (k < this.list[i].children.length) {
                             pcat = this.list[i].children[k];
                             if (typeof (pcat.children) == "undefined") pcat.children = [];
                             cat.parent = pcat;
                             pcat.children.push(cat);
                             this.list[i].children.splice(j, 1);
                         }
                     }
                     j--;
                 }
             }
         }
     }
     c.sort = function(c1, c2) {
         if (c1.displayOrder == c2.displayOrder) return 0;
         if (c1.displayOrder > c2.displayOrder) return 1;
         if (c1.displayOrder < c2.displayOrder) return -1;
         return 0;
     }
	 
     c.Render = function(elementId) {
         c.Nest();
         this.list.sort(this.sort);
         var el = $(elementId);
         if (el.length > 0) {
             var html = "", i = 0, subNavHtml = "", j = 0, k = 0, s = "";
             var cat, ccat, gccat;
             html += '<div class="row-fluid" id="csp-products"><div class="span12">';
             for (i = 0; i < this.list.length; i++) {
                 cat = this.list[i];
                 if (cat.id == 24) {
                     cat.href = '?p13(ct1&cd1i99):c14(ct5)[st(ct5*Status_Sort_Order*)]';
                 } else {
                     cat.href = '?p51(ct1&cd'+ cat.id +'i0):c53(ct1&cd'+ cat.id +'i1)[st(ct1*Status_Sort_Order*)]:c56(cu'+ cat.id +'i99&ct1&fPartNumber~1=!)[st(ct1*ContentCategoryDepth desc*)]';
                 }
                 if (cat.level != 1) {
                    continue;  
                 } 
                 html += '<div class="span4"><div class="menu-overlay-item">';
                 html += '<div class="menu-overlay-item-title"><a class="csp-menu-link" href="' + cat.href + '">' + cat.text + '</a></div>';
                 if (typeof (cat.children) != "undefined") {
                     cat.children.sort(this.sort);
                     html += '<div class="row-fluid">';
                     html += '<div class="menu-overlay-content span12"><ul class="menu-overlay-content-ul">';
                     for (j = 0; j < this.list[i].children.length; j++) {
                         ccat = this.list[i].children[j];
                         if (ccat.level != 2) continue;
						 ccat.href = '?p51(ct1&cd'+ ccat.id +'i0):c53(ct1&cd'+ ccat.id +'i1)[st(ct1*Status_Sort_Order*)]:c56(cu'+ ccat.id +'i99&ct1&fPartNumber~1=!)[st(ct1*ContentCategoryDepth desc*)]';
                         var catCount = this.FindAll(ccat.id);                         
                         html += '<li>';
                         html += '<h5 class="menu-overlay-content-h5">' 
								+ (ccat.displayOrder.indexOf('Missing') == -1 ? '<a class="menu-overlay-content-item" href=\"' + ccat.href + '\">' : '<span style=\"color: #5F798F;text-decoration: none;\">')
								+ ccat.text 
								+ (ccat.displayOrder.indexOf('Missing') == -1 ? '</a>' : '</span>')
								+ '</h5>';
                         if (typeof (ccat.children) != "undefined") {
                             html += "<ul>";
                             for (k = 0; k < ccat.children.length; k++) {
                                 gccat = ccat.children[k];
                                 if (gccat.level != 2) {
                                    if (gccat.level == 3) {
										 gccat.href = '?p4(ct1&cd1i99):c12(ct4&fPartNumber~1=' + gccat.pn + '!)[st(ct4*Content_Group_Header,Status_Sort_Order*)]:c10(ct1&cd' + gccat.id + 'i0)[st(ct1*Status_Sort_Order*)]:c11(ct2|ct3&fPartNumber~1=' + gccat.pn + '!)[st(ct2-3*Status_Sort_Order*)]&ccid=' + gccat.id + '&pn=' + gccat.pn;
                                    } else if (gccat.level == 1) {
										 gccat.href = '?p2(ct1&cd1i99):c8(ct3&cd' + gccat.id + 'i0):c9(ct2&cd' + gccat.id + 'i0)[st(ct2*Tab_ID,Status_Sort_Order*)]:c10(ct1&cd' + gccat.id + 'e10)[]&lvl=' + (gccat.level * 1 + 1) + '&ccid=' + gccat.id;
								    }
								    if (ccat.children.length >= 6 && gccat.level != 1) {
								        continue;   
								    } 
                                 } else if (gccat.level == 2) {
                                     gccat.href = '?p3(ct1&cd1i99):c6(ct1&cd' + gccat.parent.id + 'i1):c7(ct1&cd' + gccat.id + 'i1)[]&ccid=' + gccat.id;
                                 }
                                 html += '<li><a href="' + gccat.href + '">' + gccat.text + '</a></li>';
                             }
                             html += "</ul>";
                         }
                         html += '</li>';
                     }
                     html += '</ul></div></div>';
                 }
                 html += '</div></div>';
                 if (i < this.list.length - 1)
                     html += '';
             }
             html += '</div></div>';
             el.append(html);
         }
     }
	 
	 /*
     c.RenderToutArea = function(elementId) {
         var el = $(elementId);
         if (el.length == 0) return;
         var html = "", i = 0, subNavHtml = "", j = 0, k = 0, s = "";
         var cat, ccat, gccat;
         // only render the product level categories at level 1
         cat = this.Find(3);
         if (cat == null) return;
         if (typeof (cat.children) != "undefined") {
             for (i = 0; i < cat.children.length; i++) {
                 ccat = cat.children[i];
                 if (ccat.level != 1) continue;
                 html += '<div class="csp-box">';
				 if (ccat.displayOrder.indexOf('Missing') == -1) {
					 html += '<a href="' + ccat.href + '" cspobj="REPORT" csptype="LINKS" cspenglishvalue="' + ccat.rid + '">';
					 html += '<h2>' + ccat.text + '</h2>';
					 html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';					 
					 html += '<p>' + ccat.desc + '</p>';
					 html += '</a>';
				 } else {
					 html += '<h2 style=\"color: #5F798F;text-decoration: none;\">' + ccat.text + '</h2>';
					 html += '<img src="global/files/avaya/images/homepage' + ccat.id + '.jpg" />';		
					 html += '<p>' + ccat.desc + '</p>';
				 }
				 html += '</div>';
             }
         }
         el.append(html);
     }
	 
     c.RenderTitle = function(elId, catId, level, pn) {
         var el = $(elId), html = "";
         if (pn) {
             var item = this.FindByAttr("pn", pn);
             if (item.length > 0) {
                 el.append('<span class="csp-title">' + item[0].text + '</span>');
                 el.append('<span cspObj="REPORT" cspType="TITLE" style="display:none">' + item[0].text + '</span>'); // page title
             }
             return;
         }
         var cats = this.FindAll(catId, level);
         if (el.length == 0 || cats.length == 0) return;
         for (var i = 0; i < cats.length; i++) {
             if (cats[i].parent != null)
                 html = '<span class="csp-title">' + cats[i].parent.text + ': <br /> ' + cats[i].text + '</span>';
             else
                 html = '<span class="csp-title">' + cats[i].text + '</span>';
             html += '<span cspObj="REPORT" cspType="TITLE" style="display:none">' + cats[i].text + '</span>'; // page title
             break;
         }
         if (html != "")
             el.append(html);
     }
	 
	 
     c.RenderBreadcrumb = function(elId, catId, level) {
         var el = $(elId), cats = this.FindAll(catId), html = "", p = null;
         if (el.length == 0 || cats.length == 0) return;
         for (var i = 0; i < cats.length; i++) {
             if (cats[i].level == level) {
                 if (level == 3) {
                     // look for the level 2 in the same category
                     var lvl2Cat = this.FindAll(catId, 2);
                     if (lvl2Cat.length > 0)
                         html = '<a href="' + (typeof (lvl2Cat[0].href) != "undefined" ? lvl2Cat[0].href : "javascript:void(0);") + '">' + lvl2Cat[0].text + '</a>  &raquo;  ' + html;
                 }
                 else if (level == 0)
                     html = '<a href="' + (typeof (cats[i].href) != "undefined" ? cats[i].href : "javascript:void(0);") + '">' + cats[i].text + '</a>  &raquo;  ' + html;

                 p = cats[i].parent;
                 while (p != null) {
                     html = '<a href="' + (typeof (p.href) != "undefined" ? p.href : "javascript:void(0);") + '">' + p.text + '</a>  &raquo;  ' + html;
                     p = p.parent;
                 }
                 break;
            }
         }
         if (html != "")
             el.append(html);
     }
	 */
 })(categories);
