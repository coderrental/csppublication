﻿using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using CommonServiceLocator;
using Csp.Core.DataAccess.Context;
using Csp.Core.Extensions;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Master.Context;
using Csp.Core.Master.Services;
using Libcore.Core.Auditing;
using Libcore.Core.Configuration;
using Libcore.DataAccess.Extensions;
using Libcore.Dependency;
using Libcore.Dependency.Extensions;
using Libcore.Dependency.Fake;
using Libcore.Web.Extensions;
using Libcore.Web.Security;
using Microsoft.Owin;
using Owin;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Lifestyles;

namespace Csp.Web
{
    public class DependencyConfig
    {
        public static Container Register(IAppBuilder app)
        {
            var container = new Container();
            var scopeLifestyle = new AsyncScopedLifestyle();

            var hybrid = Lifestyle.CreateHybrid(
                  () => scopeLifestyle.GetCurrentScope(container) != null,
                  scopeLifestyle,
                  new WebRequestLifestyle());

            container.Options.DefaultScopedLifestyle = hybrid;
            container.Options.AllowOverridingRegistrations = true;
            //container.Options.AutoWirePropertiesWithAttribute<InjectAttribute>();

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();

            container.RegisterExtensibleModelMetadataProvider();
            container.RegisterHttpContext();
            container.RegisterEmailSender();
            container.Register<IAuditingStore, LogAuditingStore>(Lifestyle.Scoped);

            container.Register(
                instanceCreator: () =>
                {
                    var connectionString = container.GetInstance<ISettingManager>().MasterConnectionString();
                    return new MasterEfContext(connectionString);
                },
                lifestyle: Lifestyle.Scoped);

            container.Register<EfContext>(
                instanceCreator: () =>
                {
                    if (container.IsVerifying) return new EfContext("EfContext");
                    var masterDbService = container.GetInstance<IMasterDbService>();
                    return new EfContext(masterDbService.GetConnectionString());
                }, lifestyle: Lifestyle.Scoped);

            container.RegisterUnitOfWork<MasterEfContext>();
            container.RegisterUnitOfWork<EfContext>();

            container.Register(() =>
              container.IsVerifying()
                  ? new OwinContext(new Dictionary<string, object>()).Authentication
                  : HttpContext.Current.GetOwinContext().Authentication, Lifestyle.Scoped);

            container.Register<ICurrentUser, CurrentUser>(Lifestyle.Scoped);

            container.RegisterScopedByConventions(typeof(IMasterDbService).Assembly);
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            ServiceLocator.SetLocatorProvider(() => new SimpleInjectorServiceLocatorAdapter(container));

            return container;
        }
    }
}