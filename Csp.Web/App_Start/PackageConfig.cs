﻿using Csp.Core.Master.Services;
using Libcore.AutoMapper;
using Libcore.Web.MetadataModel.Localization;
using Serilog;

namespace Csp.Web
{
    public class PackageConfig
    {
        public static void Register()
        {
            Log.Logger = new LoggerConfiguration()
              .MinimumLevel.Debug()
            .CreateLogger();
            AutoMapperConfigurator.Configure(typeof(ConnectionModel).Assembly);

#if DEBUG
            HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
#endif
        }
    }
}