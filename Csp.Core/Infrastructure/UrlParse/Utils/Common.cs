using System;
using System.Text.RegularExpressions;

namespace Csp.Core.Infrastructure.UrlParse.Utils
{
	internal class Common
	{
		public static string ProxyConnString = "PROXY_CONN_STRING";
		public static string ProxyUrl = "PROXY_URL";
		public static string ProjectName = "PROJECT_NAME";
		public static string CaptchaChallengeField = "recaptcha_challenge_field";
		public static string CaptchaResponseField = "recaptcha_response_field";
		public static string SubscriptionHomePage = "home";
		public static string Channel = "channel";
		public static string Companyname="companyname";
		public static string Address1="address1";
		public static string City="city";
		public static string State="state";
		public static string Zipcode="zipcode";
		public static string Country="country";
		public static string WebsiteUrl="website_url";
		public static string Firstname="firstname";
		public static string Lastname="lastname";
		public static string JobTitle="function";
		public static string Telephone="telephone";
		public static string Emailaddress="emailaddress";
		public static string Acceptmailings="acceptmailings";
		public static string Termsandconditions="termsandconditions";
		public static string DefaultLanguage = "language";
		public static string Redirect = "redirect";
		public static string EmailParameters = "email";
		public static string SelectedBanner = "banner";
		
		public static Regex InputTagRegex = new Regex("<input\\s+[^>]*type=([\\\",\\']*)(?<type>[text,radio,checkbox,hidden]+)\\1[^>]*name=\\1(?<name>[\\w,\\d,\\-,_]*)\\1", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase);
		public static Regex InputValueRegex = new Regex("value=([\",']*)(?<value>[\\w,\\d,\\s,\\',\\-,_,(,),{,},=,&,\\[,\\],\\?]*)([\",']*)", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase);
		public static Regex InputCheckboxRegex = new Regex("checked=([\\\",\\']*)[\\w,\\d]*\\1", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase);

		private static readonly byte[] des_key = { 20, 21, 24, 32, 1, 11, 12, 22, 23, 13, 14, 34, 89, 31, 56, 15, 16, 17, 18, 19, 35, 59, 2, 3 };
		private static readonly byte[] des_iv = { 8, 7, 6, 5, 4, 3, 2, 1 };
		private static readonly TripleDes Des = new TripleDes(des_key, des_iv);
		public static Uri CaptchaVerifyRequestUrl = new Uri("http://www.google.com/recaptcha/api/verify");
		


		public static string Decrypt(string encryptedString)
		{
			return Des.Decrypt(encryptedString);
		}
	}
}