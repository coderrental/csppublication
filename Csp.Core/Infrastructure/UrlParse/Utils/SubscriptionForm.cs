﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Csp.Core.Infrastructure.UrlParse.Utils
{
	internal enum ErrorCode
	{
		FailValidate = 1,

	}
	internal class CspChannel
	{
		public int SupplierId { get; set; }
		public string ExternalId { get; set; }
		public string BaseDomain { get; set; }
		public string DefaultParameters { get; set; }
		public int BasePublicationId { get; set; }
		public List<int> ThemeIds { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="CspChannel" /> class.
		/// </summary>
		/// <param name="supplierId">The supplier id.</param>
		/// <param name="externalId">The external id.</param>
		/// <param name="baseDomain">The base domain.</param>
		/// <param name="defaultParameters">The default parameters.</param>
		/// <param name="basePublicationId">The base publication id.</param>
		public CspChannel(int supplierId, string externalId, string baseDomain, string defaultParameters, int basePublicationId)
		{
			SupplierId = supplierId;
			ExternalId = externalId;
			BaseDomain = baseDomain;
			DefaultParameters = defaultParameters;
			BasePublicationId = basePublicationId;
			ThemeIds = new List<int>();
		}
	}

	public class SubscriptionForm
	{
		private HttpContext _context;
		private string _connectionString;
		private string _requestUrl;
		private HttpRequest _request;

		/// <summary>
		/// Prevents a default instance of the <see cref="SubscriptionForm" /> class from being created.
		/// </summary>
		/// <param name="context">The http context.</param>
		private SubscriptionForm(HttpContext context)
		{
			_context = context;
			if (!_context.Items.Contains(Common.ProxyConnString)) return;

			_connectionString = Common.Decrypt(_context.Items[Common.ProxyConnString] as string);
			_requestUrl = _context.Items[Common.ProxyUrl] as string;
			_request = _context.Request;
		}

		public static SubscriptionForm CreateSubscriptionForm(HttpContext context)
		{
			return new SubscriptionForm(context);
		}

		public bool ProcessRequest()
		{
			if (string.IsNullOrEmpty(_connectionString))
			{
				Write("Invalid Request: Missing connection string.");
			}

			string action = !string.IsNullOrEmpty(_request.QueryString["action"]) ? _request.QueryString["action"] : string.Empty;
			
			switch (action.ToLower())
			{
				case "consumer_add":
					if (!ValidateCaptcha() || !AddConsumer())
						GoBack(ErrorCode.FailValidate);
					else
						_context.Response.Redirect(_request.Form[Common.Redirect].Replace("{ticks}",DateTime.Now.Ticks.ToString()));
					break;
				case "debug":
					Write("Connection String: " + _connectionString);
					break;
				default:
					break;
			}
			return true;
		}

		private void GoBack(ErrorCode code)
		{
			string url = _request.Form[Common.SubscriptionHomePage].Replace("{ticks}",DateTime.Now.Ticks.ToString());
            if (!string.IsNullOrEmpty(url))
            {
                string host = _request.Url.Scheme + "://" + _request.Url.Host +
                              (!(_request.Url.Port == 80 || _request.Url.Port == 443) ? ":" + _request.Url.Port : "") +
                              "/";

                try
                {
                    //WebRequest webRequest = WebRequest.Create(host + url) ;
                    var webRequest = WebRequest.Create(host + url);
                    var localProxyIp = ConfigurationManager.AppSettings["local_proxy_ip"];
                    if (!string.IsNullOrEmpty(localProxyIp))
                    {
                        try
                        {
                            webRequest.Proxy = new WebProxy(new Uri(localProxyIp), true);
                        }
                        catch
                        {
                        }
                    }
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    webRequest.Method = "POST";
                    webRequest.ContentLength = 0;

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (StreamReader streamReader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            StringBuilder respondedHtml = new StringBuilder(streamReader.ReadToEnd().Trim());
                            int startAt = 0, index = 0;
                            Match inputMatch, valueMatch;
                            string fieldValue;
                            while ((inputMatch = Common.InputTagRegex.Match(respondedHtml.ToString(), startAt)).Success)
                            {
                                startAt = inputMatch.Index + inputMatch.Length;
                                fieldValue = _request.Form[inputMatch.Groups["name"].Value];
                                switch (inputMatch.Groups["type"].Value)
                                {
                                    case "checkbox":
                                        valueMatch = Common.InputValueRegex.Match(inputMatch.Value);
                                        if (string.IsNullOrEmpty(fieldValue) && valueMatch.Success)
                                            respondedHtml = respondedHtml.Remove(inputMatch.Index + valueMatch.Index,
                                                                                 valueMatch.Length);
                                        else if (!valueMatch.Success && !string.IsNullOrEmpty(fieldValue))
                                            respondedHtml =
                                                respondedHtml.Insert(
                                                    inputMatch.Index +
                                                    inputMatch.Value.IndexOf("type", StringComparison.OrdinalIgnoreCase),
                                                    string.Format("checked={0}{1}{0} ", inputMatch.Groups[1], fieldValue));
                                        break;
                                    default:
                                        if (!string.IsNullOrEmpty(fieldValue))
                                        {
                                            fieldValue = fieldValue.Replace("\"", "'");
                                            if ((valueMatch = Common.InputValueRegex.Match(inputMatch.Value)).Success)
                                            {
                                                int i = valueMatch.Groups["value"].Index;
                                                respondedHtml = respondedHtml.Remove(inputMatch.Index + i,
                                                                                     valueMatch.Groups["value"].Length);
                                                respondedHtml = respondedHtml.Insert(inputMatch.Index + i, fieldValue);
                                            }
                                            else
                                            {
                                                respondedHtml =
                                                    respondedHtml.Insert(
                                                        inputMatch.Index +
                                                        inputMatch.Value.IndexOf("type",
                                                                                 StringComparison.OrdinalIgnoreCase),
                                                        string.Format("value={0}{1}{0} ", inputMatch.Groups[1],
                                                                      fieldValue));
                                            }
                                        }
                                        break;
                                }
                            }
                            respondedHtml.Insert(
                                respondedHtml.ToString().LastIndexOf("</body>", StringComparison.OrdinalIgnoreCase),
                                string.Format(
                                    @"
<script type='text/javascript'>
	if (typeof(ErrorMessages)!='undefined')
		alert(ErrorMessages['{0}']);
</script>
",
                                    (int) code));
                            _context.Response.Write(respondedHtml.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    _context.Response.Redirect(host+url,true);
                    //throw new Exception(ex.Message + ". " + host + url, ex);
                }
            }
		}


		/// <summary>
		/// Validates the captcha.
		/// </summary>
		/// <returns>true if passed captcha test</returns>
		private bool ValidateCaptcha()
		{
			string privateKey = GetCaptchaPrivateKey();

			if (string.IsNullOrEmpty(privateKey)) return true;

			string postData = string.Format("privatekey={0}&remoteip={1}&challenge={2}&response={3}", privateKey, _request.UserHostAddress, _request.Form[Common.CaptchaChallengeField], _request.Form[Common.CaptchaResponseField]);
			byte[] byteArr = Encoding.UTF8.GetBytes(postData);

			// create a post request
			WebRequest request = WebRequest.Create(Common.CaptchaVerifyRequestUrl);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = byteArr.Length;
			Stream dataStream = request.GetRequestStream();
			dataStream.Write(byteArr, 0, byteArr.Length);
			dataStream.Close();

			// response
			WebResponse response = request.GetResponse();
			dataStream = response.GetResponseStream();
			StreamReader streamReader = new StreamReader(dataStream);
			string message = streamReader.ReadToEnd();
			streamReader.Close();
			dataStream.Close();
			response.Close();

			string[] messages = message.Split('\n');
			return "true".Equals(messages[0]);
		}

		private bool AddConsumer()
		{
		    bool flag = true;
			string baseDomain = string.Empty;
			// company
			using (var connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (var command = new SqlCommand("", connection))
				{
					// get channel info
					CspChannel channel = GetChannel(command, _request.Form[Common.Channel]);

					if (channel != null)
					{
						// create company
						command.CommandText = @"
insert into companies (companyname,address1,address2,city,zipcode,state,country,website_url,is_consumer,is_supplier,displayname,parent_companies_Id,created,date_changed)
	values(@companyname,@address1,@address2,@city,@zipcode,@state,@country,@website_url,1,0,@companyname,@parent_companies_Id,GETDATE(),GETDATE()) ";

						command.Parameters.AddWithValue("@companyname", TruncateText(_request.Form[Common.Companyname], 75));
						command.Parameters.AddWithValue("@address1", TruncateText(_request.Form[Common.Address1], 50));
						command.Parameters.AddWithValue("@address2", string.Empty);
						command.Parameters.AddWithValue("@city", TruncateText(_request.Form[Common.City], 50));
						command.Parameters.AddWithValue("@zipcode", TruncateText(_request.Form[Common.Zipcode], 50));
						command.Parameters.AddWithValue("@state", TruncateText(_request.Form[Common.State], 50));
						command.Parameters.AddWithValue("@country", TruncateText(_request.Form[Common.Country], 50));
						command.Parameters.AddWithValue("@website_url", TruncateText(_request.Form[Common.WebsiteUrl],200));
						command.Parameters.AddWithValue("@parent_companies_Id", channel.SupplierId);
						command.ExecuteNonQuery();

						command.CommandText = "select @@IDENTITY";
						int companyId = Convert.ToInt32(command.ExecuteScalar());

						// create contact
						command.Parameters.Clear();
						command.CommandText = @"
insert into companies_contacts(companies_Id,firstname,lastname,emailaddress,mobile,telephone,department,user_Id)
	values(@companies_Id,@firstname,@lastname,@emailaddress,@mobile,@telephone,@department,@user_Id) ";
						command.Parameters.AddWithValue("@companies_Id", companyId);
						command.Parameters.AddWithValue("@firstname", TruncateText(_request.Form[Common.Firstname], 50));
						command.Parameters.AddWithValue("@lastname", TruncateText(_request.Form[Common.Companyname],50));
						command.Parameters.AddWithValue("@emailaddress", TruncateText(_request.Form[Common.Emailaddress],150));
						command.Parameters.AddWithValue("@mobile", TruncateText(_request.Form[Common.Telephone],20));
						command.Parameters.AddWithValue("@telephone", TruncateText(_request.Form[Common.Telephone],20));
						command.Parameters.AddWithValue("@department", TruncateText(_request.Form[Common.JobTitle],50));
						command.Parameters.AddWithValue("@user_Id", Guid.Empty);
						command.ExecuteNonQuery();

						// create company consumers
						command.Parameters.Clear();
						command.CommandText = @"
declare @id int
if not exists (select 1 from languages a where a.BrowserLanguageCode=@default_language_Id and a.active=1)
	select top 1 @id = a.languages_Id from languages a where a.active=1
else select top 1 @id = a.languages_Id from languages a where a.BrowserLanguageCode=@default_language_Id
insert into companies_consumers(companies_Id,base_domain,base_publication_Id,base_publication_parameters,active,default_language_Id,fallback_language_Id)
	values(@companies_Id,@base_domain,@base_publication_Id,@base_publication_parameters,@active,@id, @id) ";
						command.Parameters.AddWithValue("@companies_Id", companyId);
						baseDomain = DateTime.Now.Ticks + "." + channel.BaseDomain;
						command.Parameters.AddWithValue("@base_domain", baseDomain);
						command.Parameters.AddWithValue("@base_publication_Id", channel.BasePublicationId);
						command.Parameters.AddWithValue("@base_publication_parameters", channel.DefaultParameters);
						command.Parameters.AddWithValue("@active", true);
						command.Parameters.AddWithValue("@default_language_Id", _request.Form[Common.DefaultLanguage]);
						command.ExecuteNonQuery();

						// create company parameters
						CreateCompanyParameter(command, "Personalize_Contact_Jobtitle", companyId, _request.Form[Common.JobTitle]);
						CreateCompanyParameter(command, "Personalize_Contact_Firstname", companyId, _request.Form[Common.Firstname]);
						CreateCompanyParameter(command, "Personalize_Contact_Lastname", companyId, _request.Form[Common.Lastname]);
						CreateCompanyParameter(command, "Personalize_Contact_Emailaddress", companyId, _request.Form[Common.Emailaddress]);
						CreateCompanyParameter(command, "Personalize_Contact_Phonenumber", companyId, _request.Form[Common.Telephone]);

						// create company themes
						command.Parameters.Clear();
						command.CommandText = "insert into companies_themes(companies_Id,themes_Id) values (@companies_Id,@themes_Id)";
						command.Parameters.AddWithValue("@companies_Id", companyId);
						foreach (int id in channel.ThemeIds)
						{
							if (!command.Parameters.Contains("@themes_Id"))
								command.Parameters.AddWithValue("@themes_Id", id);
							else
								command.Parameters["@themes_Id"].Value = id;
							command.ExecuteNonQuery();
						}

                        // get personalize pdf logo to store in a session
                        command.Parameters.Clear();
					    command.CommandText = @"select a.value_text
from companies_parameters a
	join companies_parameter_types b on a.companies_parameter_types_Id=b.companies_parameter_types_Id
where a.companies_Id=@supplierId and a.value_text is not null and a.value_text <> '' and b.parametername = 'Personalize_PDF_Logo'";
					    command.Parameters.AddWithValue("@supplierId", channel.SupplierId);
					    var obj = command.ExecuteScalar();
                        if (obj != null)
                            _context.Session.Add("Personalize_PDF_Logo", obj);

					    command.CommandText = "select c.companyname from companies c where c.companies_Id=@supplierId";
					    obj = command.ExecuteScalar();
                        if (obj != null)
                            _context.Session.Add("supplierName", obj);
					}
					else
					{
					    flag = false;
					}
				}
				connection.Close();
			}

			foreach (string key in _request.Form.AllKeys)
			{
				_context.Session.Add(key, _request.Form[key]);
			}
			_context.Session.Add("baseDomain", baseDomain);
			return flag;
		}

	    private static string TruncateText(string text, int maxLength)
	    {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
	        return (text.Length > maxLength ? text.Substring(0, maxLength) : text);
	    }

	    private void CreateCompanyParameter(SqlCommand command, string parameterName, int companyId, string value)
		{
			command.ResetCommandTimeout();
			command.Parameters.Clear();

			command.CommandText = @"
declare @parameterId int
if not exists (select 1 from companies_parameter_types a where a.parametername=@parameterName)
begin
	insert into companies_parameter_types(parametername, parametertype,sortorder,IsExport) values(@parameterName,1,0,0)
	select @parameterId = @@IDENTITY
end
else 
	select @parameterId = a.companies_parameter_types_Id from companies_parameter_types a where a.parametername=@parameterName
if not exists (select 1 from companies_parameters a where a.companies_parameter_types_Id=@parameterId and a.companies_Id=@companyId)
	insert into companies_parameters(companies_Id,companies_parameter_types_Id,value_text) values(@companyId,@parameterId,@value) ";

			command.Parameters.AddWithValue("@parameterName", parameterName);
			command.Parameters.AddWithValue("@companyId", companyId);
			command.Parameters.AddWithValue("@value", value);
			command.ExecuteNonQuery();

		}

		private CspChannel GetChannel(SqlCommand command, string channelId)
		{
            if (string.IsNullOrEmpty(channelId))
                return null;
			CspChannel channel = null;
			command.ResetCommandTimeout();
			command.Parameters.Clear();
			command.CommandText = @"
select a.CompanyId, a.ExternalIdentifier, a.BaseDomain,a.DefaultParameters,a.BasePublicationId, b.ThemeId 
from Channel a
	join ChannelThemeAssocation b on a.Id=b.ChannelId
where a.ExternalIdentifier = @channelId	";
			command.Parameters.AddWithValue("@channelId", channelId);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader != null || reader.HasRows)
				{
					bool isFirst = false;
					while (reader.Read())
					{
						if (!isFirst)
						{
							channel = new CspChannel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4));
							isFirst = true;
						}
						channel.ThemeIds.Add(reader.GetInt32(5));
					}
				}
				reader.Close();
			}

			return channel;
		}

		private string GetCaptchaPrivateKey()
		{
		    bool useCaptcha = false,
		         useGlobalCaptcha = false;
		    string temp = ConfigurationManager.AppSettings["UseCaptchaForAllSubscriptionForms"] as string;
            if (!string.IsNullOrEmpty(temp) && temp.Equals("true",StringComparison.InvariantCultureIgnoreCase))
                useGlobalCaptcha = true;

			for (int i = 0; !useCaptcha && i < _request.Form.Keys.Count; i++)
			{
				useCaptcha = _request.Form.Keys[i] == Common.CaptchaChallengeField || _request.Form.Keys[i] == Common.CaptchaResponseField;
			}
			//return useCaptcha ? ConfigurationManager.AppSettings["recaptcha_" + (_context.Items[Common.ProjectName] as string) + "_privatekey"] : string.Empty;
		    return useCaptcha
		               ? (useGlobalCaptcha
		                      ? ConfigurationManager.AppSettings["recaptcha_privatekey"]
		                      : ConfigurationManager.AppSettings["recaptcha_" + (_context.Items[Common.ProjectName] as string) + "_privatekey"])
		               : string.Empty;
		}

		private void Write(string message)
		{
#if DEBUG
			_context.Response.Write("<p>" + message + "</p>");
			/*_context.Response.Write(message);*/
#endif
		}
	}
}
