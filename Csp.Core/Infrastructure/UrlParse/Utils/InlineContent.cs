﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace Csp.Core.Infrastructure.UrlParse.Utils
{
    public class InlineContent
    {
        public const string GetCustomParameterSql = 
@"
select b.parametername, a.value_text
from companies_parameters a
	join companies_parameter_types b on a.companies_parameter_types_Id = b.companies_parameter_types_Id
	join companies_consumers c on c.companies_Id = a.companies_Id
where c.base_domain = @base_domain and b.parametertype = 1
";
        public static string CreateJavascript(string html, string connectionString, string projectName, Uri request, string scriptMapName)
        {
            Type t = new System.Diagnostics.StackFrame().GetMethod().DeclaringType;
            string script = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/App_Data/" + t.Name + ".js");
            html = CleanHtml(html);

            string consumerParameters = GetConsumerParameters(connectionString, request);
            if (!string.IsNullOrEmpty(scriptMapName) && scriptMapName[0] == '/')
                scriptMapName = scriptMapName.Substring(1);

            script = script
                .Replace("{server:html}", html)
                .Replace("{server:protocol}", request.Scheme)
                .Replace("{server:domain}", request.Host)
                .Replace("{server:port}", request.Port == 80 ? string.Empty : ":" + request.Port.ToString())
                .Replace("{server:project}", projectName)
                .Replace("{server:consumer_parameters}", consumerParameters)
                .Replace("{server:scriptname}", HttpUtility.UrlDecode(scriptMapName));

            NameValueCollection queryString = HttpUtility.ParseQueryString(request.Query);
            foreach (string s in queryString)
            {
                if (!string.IsNullOrEmpty(s))
                    script = script.Replace("{server:"+s+"}", queryString[s]);
            }

            script = RemoveNotFoundServerTag(script);
                
            return script;
        }

        private static string GetConsumerParameters(string connectionString, Uri request)
        {
            SqlConnection connection = null;
            SqlTransaction transaction = null;
            string consumerParameters = string.Empty;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
                SqlCommand command = new SqlCommand(GetCustomParameterSql, connection, transaction);
                command.CommandText = GetCustomParameterSql;
                command.Parameters.AddWithValue("@base_domain", request.Host);
                SqlDataReader reader = command.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                        if (!string.IsNullOrEmpty(reader.GetString(1)))
                            consumerParameters += string.IsNullOrEmpty(consumerParameters) ? string.Format("\"{0}\":\"{1}\"", reader.GetString(0), reader.GetString(1)) : string.Format(",\"{0}\":\"{1}\"", reader.GetString(0), reader.GetString(1));
                    reader.Close();
                    reader.Dispose();
                }
                transaction.Commit();
                connection.Close();

            }
            catch (Exception ex)
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    if (transaction != null) 
                        transaction.Commit();
                    connection.Close();
                }
                consumerParameters = string.Empty;
            }
            finally
            {
                if (connection != null) connection.Dispose();
            }
            return consumerParameters;
        }

        private static string RemoveNotFoundServerTag(string script)
        {
            Regex regex = new Regex("{server:[a-zA-Z0-9]+}", RegexOptions.None);
            return regex.Replace(script, "");
        }

        private static string CleanHtml(string html)
        {
            html = html.Replace("\r", "").Replace("\n", "").Replace("\"", "\\\"");
            Regex regex = new Regex(@"[ ]{2,}", RegexOptions.None);
            return regex.Replace(html, @" ");
        }
    }
}
