﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Csp.Core.Infrastructure.UrlParse.Utils
{
    public class ReplaceContentTypeJoin
    {
        public ReplaceContentTypeJoin()
        {
        }
        public static string replaceContentTypeJoin(string text, string languageId, SqlConnection con, string url, string domain)
        {
            /*
"<html> <head> </head> <body>{ctjoin value="22796" param="12:modelID:ArtID,11:ArtID:SyncArtNr,14:OEMREF:OEM_Ref"}{/ctjoin} </body> </html> "              
             * 
             * ctjoin\s*value="*[0-9,a-z]*"\s*param="([0-9]+(\:[a-z,A-Z,\_]+)+\,*)*"}\s*{/ctjoin}
             */
            if (text.Contains("{ctjoin"))
            {



                Regex ctJoinPattern = new Regex("(?<ctjoin>{ctjoin\\s*value=\"(?<value>\\[[\\d,\\w]*\\])\"\\s*param=\"(?<param>[\\d,\\w,:]*)\"\\s*}[\\w,\\d,\\s]*{/ctjoin})");
                MatchCollection mc = ctJoinPattern.Matches(text);
                string value = string.Empty;
                string param = string.Empty;
                string result = string.Empty;
                string returnText = text;

                Match match = ctJoinPattern.Match(text);
                while (match.Success)
                {
                    value = match.Groups["value"].Value;
                    param = match.Groups["param"].Value;

                    // check to see where we are going to use the value
                    Regex valuePattern = new Regex(@"\[(?<fieldName>[\d,\w]*)\]");
                    Match valueMatch = valuePattern.Match(value);
                    if (valueMatch.Success) //only capture the first occurrence
                    {
                        value = valueMatch.Groups["fieldName"].Value;
                        if (!string.IsNullOrEmpty(value))
                        {
                            //http://localhost/publish/d1.aspx?p10(ct12&fModelID~1=22727!)
                            valuePattern = new Regex(@"fModelID\~[0-9]\=(?<value>[\w,\d]+)\!");
                            valueMatch = valuePattern.Match(url);

                            if (valueMatch.Success)
                            {
                                value = valueMatch.Groups["value"].Value;
                            }
                        }
                    }

                    // execute stored procedure
                    if (!(string.IsNullOrEmpty(value) && string.IsNullOrEmpty(param)))
                    {
                        try
                        {
                            if (con.State == System.Data.ConnectionState.Closed)
                                con.Open();
                            if (string.IsNullOrEmpty(languageId)) languageId = "-1";

                            SqlCommand command = new SqlCommand("[ct_join]", con);
                            command.CommandType = System.Data.CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@value", value));
                            command.Parameters.Add(new SqlParameter("@variables", param));
                            command.Parameters.Add(new SqlParameter("@lngId", languageId));
                            command.Parameters.Add(new SqlParameter("@domain", domain));
                            command.Prepare();
                            command.CommandTimeout = 0;
                            result = (string)command.ExecuteScalar();
                        }
                        catch (Exception ex)
                        {
                            if (con.State == System.Data.ConnectionState.Open)
                                con.Close();
                            throw new Exception("replaceContentTypeJoin", ex);
                            //result = ex.Message;                        
                        }
                        finally
                        {
                            if (con.State == System.Data.ConnectionState.Open)
                                con.Close();
                        }
                        text = text.Replace(match.Captures[0].Value, result);
                    }
                    else
                    {
                        //return empty string if content isnt found
                        text = text.Replace(match.Captures[0].Value, string.Empty);
                    }
                    match = match.NextMatch();
                }
                return text;
            }
            else
            {
                return text;
            }

        }
    }
}