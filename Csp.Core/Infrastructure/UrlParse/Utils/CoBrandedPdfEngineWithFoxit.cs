﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Foxit.PDF;
using Foxit.PDF.Forms;
using Foxit.PDF.Merger;
using Foxit.PDF.Merger.Forms;
using Image = Foxit.PDF.PageElements.Image;
using Path = System.IO.Path;

namespace Csp.Core.Infrastructure.UrlParse.Utils
{
    public class CoBrandedPdfEngineWithFoxit
    {
            private static HttpContext _context;
        private string _connectionString;
        private Uri _requestUrl;
        private HttpRequest _request;
        private string _projectName;

        private string query = @"
select cpt.parametername as Name,cp.value_text as Value
from companies c
	join companies_parameters cp on c.companies_Id=cp.companies_Id
	join companies_consumers cc on cc.companies_Id=c.companies_Id
	join companies_parameter_types cpt on cpt.companies_parameter_types_Id=cp.companies_parameter_types_Id
where cc.base_domain=@baseDomain";

        private string companyInfoQuery = @"
select Name, Value
from (
	select 
		cast(c.companyname as nvarchar(max)) as companyname, 
		cast(c.address1 as nvarchar(max)) as address1,
		cast(c.city as nvarchar(max)) as city,
		cast(c.zipcode as nvarchar(max)) as zipcode,
        cast(case when c.website_url is null then '' else c.website_url end as nvarchar(max)) as website_url
	from companies c
		join companies_consumers cc on cc.companies_Id=c.companies_Id
	where cc.base_domain=@baseDomain
	) as source
	unpivot
	(
		Value for Name in (companyname, address1, city, zipcode, website_url)
	) as up";
        private List<string> _fieldsThatArentImages = new List<string>() { "website_url", "Deeplink_Landingpage_Url", "Personalize_Twitter_URL","Personalize_LinkedIn_url" };

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public CoBrandedPdfEngineWithFoxit(HttpContext context)
        {
            _context = context;
            if (!_context.Items.Contains(Common.ProxyConnString)) return;

            _connectionString = Common.Decrypt(_context.Items[Common.ProxyConnString] as string);
            _requestUrl = _context.Items[Common.ProxyUrl] as Uri;
            _request = _context.Request;
            _projectName = _context.Items[Common.ProjectName] as string;
        }

        public static CoBrandedPdfEngineWithFoxit Create(HttpContext context)
        {
            return new CoBrandedPdfEngineWithFoxit(context);
        }

        public bool Process()
        {
            string[] queryString = _requestUrl.Query.Split('=');

            // check to see if the request is correct
            if (queryString.Length < 2) return false;

            var parameters = new Dictionary<string, string>();

            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                SqlTransaction transaction = sqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted);

                using (SqlCommand command = new SqlCommand(query, sqlConnection, transaction))
                {
                    command.Parameters.AddWithValue("@baseDomain", _requestUrl.Host.ToLower());
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            if (!parameters.ContainsKey(dataReader["Name"].ToString()))
                                parameters.Add(dataReader["Name"].ToString(), dataReader["Value"].ToString());
                        }
                    }
                    command.CommandText = companyInfoQuery;
                    try
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                if (!parameters.ContainsKey(dataReader["Name"].ToString()))
                                    parameters.Add(dataReader["Name"].ToString(), dataReader["Value"].ToString());
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                transaction.Commit();
            }

            if (parameters.Count > 0)
            {
                // figure out file locations
                string globalBin = ConfigurationManager.AppSettings[_projectName + "_global_bin"],
                       companyBin = ConfigurationManager.AppSettings[_projectName + "_companies_bin"],
                       workingBin = string.Empty,
                       filePath = string.Empty,
                       workingDirectory = string.Empty,
                       processedFilePath = string.Empty;
                bool noFlattenPdf = false;

                if (string.IsNullOrEmpty(globalBin))
                {
                    globalBin = "global_bin";
                }

                if (string.IsNullOrEmpty(companyBin))
                {
                    companyBin = "global_bin";
                }

                if (ConfigurationManager.AppSettings.AllKeys.Any(a => a.Equals(_projectName + "_NoFlattenPdf")))
                {
                    bool.TryParse(ConfigurationManager.AppSettings[_projectName + "_NoFlattenPdf"], out noFlattenPdf);
                }

                // check to see if we use global in or company bin
                if (queryString[1].StartsWith("global/") || queryString[1].Contains("/global/"))
                {
                    workingBin = globalBin;
                    // the original code assumes that there is a /files/ right after the /global/. I have to put it in there to make sure its backward compatible
                    if (queryString[1].Contains("/files/"))
                        filePath = queryString[1].Remove(0, 13); // remove global/files and keep only the file path after that
                    else
                        filePath = queryString[1].Remove(0, 7);
                    if (filePath.StartsWith("/")) filePath = filePath.Remove(0, 1);
                }
                else
                {
                    workingBin = companyBin;
                    filePath = queryString[1];
                }

                if (!filePath.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                {
                    var index = filePath.IndexOf(".pdf", StringComparison.OrdinalIgnoreCase);
                    if (index > 0)
                    {
                        filePath = filePath.Substring(0, index + 4);
                    }
                }

                filePath = _request.PhysicalApplicationPath + workingBin + "\\files\\" + filePath.Replace('/', '\\');
                workingDirectory = _request.PhysicalApplicationPath + workingBin + @"\server_files\test\";
                if (!Directory.Exists(workingDirectory))
                    Directory.CreateDirectory(workingDirectory);

                if (!File.Exists(filePath))
                {
                    Write("Invalid File Request: {0}", filePath);
                    return false;
                }

                // initialize PDF API
                //var l1 = Foxit.PDF.Document.AddLicense("FMG10NPDOI9PMPPn0kVb+Am886Q4MrVdxqu8uWtLIJtGg8nbVBAvzel7x4dwI4sotjlp3/7iLa5YP33C3avoo6ErnorN7FfEon/g");
                //var l2 = Foxit.PDF.Document.AddLicense("FGN10NPDCE3LFCoRt5+rdCgx6xrl9SnnHT87Szk5bqqBrLFbN80ppsjkc331pAfZwkJgGvQSbAWl5qteLuQutfwjWClI41XQGs8g");

                //Foxit.PDF.Document.AddLicense(
                //    "FMG10NEEPMJLLPm4NZi5oiXkZxEX4LuDH5+WfrpoHG+hjszXTXAyFLF0OQqylubCxMHPLFabqGI4jhqFaaCNShWpc6gBNDbWjFyg");

                var pdfDoc = new PdfDocument(filePath);
                MergeOptions options = new MergeOptions();
                options.LogicalStructure = false;
                //var doc = new MergeDocument(pdfDoc, MergeOptions.All);
                var doc = new MergeDocument(pdfDoc, options);


                string value = string.Empty;
                var pdfForm = doc.Form;

                foreach (var parameter in parameters)
                {
                    value = parameter.Value;
                    if (!_fieldsThatArentImages.Contains(parameter.Key) && (value.StartsWith("http://", StringComparison.OrdinalIgnoreCase) || value.StartsWith("https://", StringComparison.OrdinalIgnoreCase)))
                    {
                        FormField field = pdfForm.Fields.Cast<FormField>().FirstOrDefault(f => f.Name.Equals(parameter.Key, StringComparison.OrdinalIgnoreCase));
                        if (field != null)
                        {
                            string tempPath = workingDirectory + Guid.NewGuid();
                            Bitmap image = GetImage(parameter.Value, tempPath);
                            if (image != null)
                            {
                                var fields = new List<PdfFormField>();
                                if (field.HasChildFields)
                                {
                                    var temp = pdfDoc.Form.Fields[field.Name];
                                    for (int i = 0; i < temp.ChildFields.Count; i++)
                                    {
                                        fields.Add(temp.ChildFields[i]);
                                        field.ChildFields[i].IsReadOnly = true;
                                    }
                                }
                                else
                                {
                                    fields.Add(pdfDoc.Form.Fields[field.Name]);
                                    field.IsReadOnly = true;
                                }
                                foreach (var formField in fields)
                                {
                                    Page page = doc.Pages[formField.GetOriginalPageNumber() - 1];
                                    float x = formField.GetX(page),
                                        y = formField.GetY(page),
                                        width = formField.Width,
                                        height = formField.Height;

                                    Image pdfImage = null;

                                    // try to fit the image in the box
                                    if (width < image.Width || height < image.Height)
                                    {
                                        //float ratioX = 1, ratioY = 1;
                                        //if (width < image.Width)
                                        //    ratioX = (image.Width - width)/image.Width;
                                        //if (height < image.Height)
                                        //    ratioY = (image.Height - height)/image.Height;

                                        //float ratio = Math.Min(ratioX, ratioY);
                                        //width = image.Width - image.Width*ratio;
                                        //height = image.Height - image.Height*ratio;

                                        width = width / image.Width;
                                        height = height/image.Height;
                                        float modifier = (width > height ? height : width);
                                        width = image.Width * modifier;
                                        height = image.Height * modifier;


                                        //var scaledImage = new Bitmap((int)width, (int)height);
                                        //scaledImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                                        //using (var g = Graphics.FromImage(scaledImage))
                                        //{
                                        //    g.CompositingQuality =
                                        //        System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                                        //    g.InterpolationMode =
                                        //        System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                        //    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                                        //    g.DrawImage(image, 0, 0, (int)width, (int)height);
                                        //    //g.DrawImage(image,
                                        //    //    new System.Drawing.Rectangle(0, 0, image.Width, image.Height),
                                        //    //    new System.Drawing.Rectangle(0, 0, (int)width, (int)height),
                                        //    //    GraphicsUnit.Pixel);
                                        //    //g.DrawRectangle(new Pen(System.Drawing.Color.Black, 1f), 0, 0, (int) width - 1, (int)height-1);
                                        //}
                                        //pdfImage = new Image(scaledImage, x, y);
                                        pdfImage = new Image(image, x, y)
                                        {
                                            Width = width*0.75f,
                                            Height = height*0.75f
                                        };
                                        //pdfImage.SetDpi(image.HorizontalResolution, image.VerticalResolution);

                                    }
                                    else
                                    {
                                        pdfImage = new Image(image, 0, 0)
                                        {
                                            X = x,
                                            Y = y,
                                            Width = image.Width*0.75f,
                                            Height = image.Height*0.75f,
                                        };
                                        //pdfImage = new Image(image, x, y);
                                    }

                                    page.Elements.Add(pdfImage);
                                }
                            } // if image not null
                            else
                            {
                                // image is null, so this is probably not an image url
                                field.Value = parameter.Value.Replace("\r\n", "\n");
                                field.IsReadOnly = true;
                            }
                        }// if param name matches field name
                    } //if images
                    else // this is NOT an image
                    {
                        foreach (FormField field in pdfForm.Fields)
                        {
                            if (field.Name.Equals(parameter.Key, StringComparison.OrdinalIgnoreCase))
                            {
                                field.Value = parameter.Value.Replace("\r\n", "\n");
                                field.IsReadOnly = true;
                                break;
                            }
                        }

                    } // end else
                } // for each param

                if (!noFlattenPdf)
                {
                    doc.FormFlattening = new FormFlatteningOptions()
                    {
                        DigitalSignatures = SignatureFieldFlatteningOptions.Flatten
                    };
                }
                
                processedFilePath = _request.PhysicalApplicationPath + workingBin + @"\server_files\test\" + Guid.NewGuid().ToString() + ".pdf";
                doc.Draw(processedFilePath);
                if (File.Exists(processedFilePath))
                {
                    _context.Response.ContentType = "application/octet-stream";
                    _context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + Path.GetFileName(filePath) + "\"");
                    _context.Response.WriteFile(processedFilePath);
                }
                else
                    return false;
            }

            return true;
        }

        private static Bitmap GetImage(string source, string localPath)
		{
			// check to see if an image has an original version or not
			string fileName = Path.GetFileNameWithoutExtension(source);
			if (fileName.EndsWith("r")) 
				source = source.Replace(fileName, fileName.TrimEnd('r'));
			Bitmap bitmap = null;
			try
			{
				var req = WebRequest.Create(source);
				{
					var res = req.GetResponse();
					Stream stream = res.GetResponseStream();
					bitmap = System.Drawing.Image.FromStream(stream) as Bitmap;
				    bitmap?.Save(localPath);
				    res.Close();
				}
			}
			catch
			{
				return null;
			}
			return bitmap;
		}

		private static void Write(string message, params string[] parameters)
		{
			_context.Response.Write("<p>" + string.Format(message, parameters) + "</p>");
		}
	}
}