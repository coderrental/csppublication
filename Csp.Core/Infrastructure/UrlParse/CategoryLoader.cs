﻿using System.Collections.Generic;
using System.Web.Caching;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryLoader
    {
        public CategoryItem CleanTree;
        public List<CategoryItem> FlatListOfAllCleanCategories = new List<CategoryItem>();
        public ContentCollection RelatedContentCollection;

        private string RootCategoryID;
        public List<SupplierCatTree> SupplierTrees;
        private Cache TheCache;

        public CategoryLoader(string rootCategory, int depth, string consumerId, string domain,
            List<string> supplierIDs, string contentCategoryTypeId, string sortingParams,
            string fieldSearchParams, string urlEngineConnString, Cache inCache,
            string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategory, CategoryItem cleanTree, List<SupplierCatTree> supplierTree)
        {
            TheCache = inCache;
            RootCategoryID = rootCategoryId;
            FlatListOfAllCleanCategories = flatListOfAllCleanCategory;
            CleanTree = cleanTree;
            SupplierTrees = supplierTree;

            MarkThemesInCleanTreeWithSupplierThemes();
            GetCategoryContentCollections(domain, contentCategoryTypeId, fieldSearchParams, urlEngineConnString);
        }

        public bool IsCategoryActiveInAnyTree(object inCategoryCode)
        {
            foreach (var cat in FlatListOfAllCleanCategories)
            {
                if (cat.CategoryActive && cat.IsInTheme && cat.CategoryID == inCategoryCode)
                {
                    return true;
                }
            }
            return false;
        }

        private void AssignContentToCategories()
        {
            foreach (var curCat in FlatListOfAllCleanCategories)
            {
                if (curCat.IsInTheme)
                {
                    foreach (var item in RelatedContentCollection.fMyContent)
                    {
                        foreach (var itemCat in item.fCategoriesImAssignedTo)
                        {
                            if (itemCat == curCat.CategoryID)
                            {
                                curCat.AssignedCatContentPiece = item;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void MarkCategoryInCleanTreeAsInTheme(string catId)
        {
            foreach (var item in FlatListOfAllCleanCategories)
            {
                if (item.CategoryID == catId)
                {
                    item.IsInTheme = true;
                    return;
                }
            }
        }

        private void MarkThemesInCleanTreeWithSupplierThemes()
        {
            foreach (var item in SupplierTrees)
            {
                foreach (var cat in item.ListOfAllCategories)
                {
                    if (cat.IsInTheme)
                    {
                        MarkCategoryInCleanTreeAsInTheme(cat.CategoryID);
                    }
                }
            }
        }

        private string GetURLStringForCategoryContentCollection(string contentCategoryTypeId, string fieldSearchParams)
        {
            if (string.IsNullOrEmpty(fieldSearchParams))
            {
                return "p0(ct" + contentCategoryTypeId + ")";
            }
            return "p0(ct" + contentCategoryTypeId + "&" + fieldSearchParams + ")";
        }

        private void GetCategoryContentCollections(string domain, string contentCategoryTypeId,
            string fieldSearchParams, string urlEngineConnString)
        {
            List<URLNameValuePair> t1 = null;
            LoggingInfo t2 = null;
            CategoryLoaderParams t3 = null;
            var curUrlString = GetURLStringForCategoryContentCollection(contentCategoryTypeId, fieldSearchParams);
            var curUrlEngine = URLEngine.GetOrCreateURLEngineFromCache(curUrlString, urlEngineConnString, domain,
                ref t1, ref t2, false, true, t3, ref TheCache, "",
                RootCategoryID, FlatListOfAllCleanCategories, CleanTree, SupplierTrees);

            RelatedContentCollection = curUrlEngine.MyContentCollectionTasks[0].Item2.Result;
            AssignContentToCategories();
        }

    }
}