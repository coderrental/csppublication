﻿using System;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class SortParams
    {
        public string SortType;
        public string SortParam;

        public SortParams(string vResultModifier)
        {
            if (vResultModifier.Contains("st("))
            {
                var hold = vResultModifier.Substring(vResultModifier.IndexOf("st(", StringComparison.Ordinal));
                hold = hold.Substring(0, hold.IndexOf(")", StringComparison.Ordinal));
                hold = hold.Substring(hold.IndexOf("ct", StringComparison.Ordinal) + 2);
                var holdType = hold.Substring(0, hold.IndexOf("*", StringComparison.Ordinal));
                hold = hold.Substring(hold.IndexOf("*", StringComparison.Ordinal) + 1);
                hold = hold.Substring(0, hold.IndexOf("*", StringComparison.Ordinal));
                SortType = holdType;
                SortParam = hold;
            }
            else
            {
                SortType = "";
                SortParam = "";
            }
        }
    }
}