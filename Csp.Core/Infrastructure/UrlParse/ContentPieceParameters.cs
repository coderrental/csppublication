﻿using System;
using System.Collections;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ContentPieceParameters //ContentCollectionCode.vb //this is a struct changed to class
    {
        public string fCTMainContentIdentifier;
        public string fCTMainID;
        public string fCTMainSupplierID;
        public string fCTMainConsumerID;
        public ArrayList fCTCategories;
        public string fThemeID;
        public string fContentTypeID;
        public string fContentInstanceID;
        public string fContentMainKey;
        public string fCTLanguageID;
        public string fCTLanguageCode;
        public string fCTLanguageDescription;
        public bool fContentCategoryHasChildren;
        public int fContentCategoryChildDepth;
        public int fNextAvailableContentCategoryChildId;

        public ContentPieceParameters()
        {
            
        }
        public ContentPieceParameters(string vCtMainContentIdentifier, string vCtMainId, string vCtMainSupplierId,
            string vCtMainConsumerId, ArrayList vCtCategories, string vThemeId, string vContentTypeId,
            string vContentInstanceId, string vContentMainKey, string vCtLanguageId, string vCtLanguageCode,
            string vCtLanguageDescription, bool children, int depth, int nextAvailCatId)
        {
            fCTMainContentIdentifier = vCtMainContentIdentifier;
            fCTMainID = vCtMainId;
            fCTMainSupplierID = vCtMainSupplierId;
            fCTMainConsumerID = vCtMainConsumerId;
            fCTCategories = vCtCategories;
            fThemeID = vThemeId;
            fContentTypeID = vContentTypeId;
            fContentInstanceID = vContentInstanceId;
            fContentMainKey = vContentMainKey ?? throw new ArgumentNullException(nameof(vContentMainKey));
            fCTLanguageID = vCtLanguageId;
            fCTLanguageCode = vCtLanguageCode;
            fCTLanguageDescription = vCtLanguageDescription;
            fContentCategoryHasChildren = children;
            fContentCategoryChildDepth = depth;
            fNextAvailableContentCategoryChildId = nextAvailCatId;
        }
    }
}