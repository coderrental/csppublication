﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using Microsoft.VisualBasic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class Functions
    {
        public static string ContentPieceCount(string html, int count)
        {
            return html.Replace("{ContentPieceCount}", count.ToString());
        }

        public static string BrowserLanguageCode(string html)
        {
            var s = Thread.CurrentThread.CurrentCulture.ToString();
            return html.Replace("{BrowserLanguageCode}", s);
        }

        public static string StrToHex(ref string Data)
        {
            string sVal;
            var sHex = "";
            while (Data.Length > 0)
            {
                //ToDo : remove VB dll
                sVal = $"{Strings.Asc(Data.Substring(0, 1)):X}";
                Data = Data.Substring(1, Data.Length - 1);
                sHex = sHex + " - " + sVal;
            }

            return sHex;
        }

        public static string EscapeQuotes(string html)
        {
            // Dim i As Integer = html.IndexOf("{EscapeQuotes}"), n As Integer
            // Dim s As String = ""
            if (!html.Contains("{EscapeQuotes}"))
            {
                return html;
            }

            var hold = html;
            var outStr = new StringBuilder(html.Length * 2);
            // Dim out As StringBuilder = New StringBuilder()
            while (hold.Contains("{EscapeQuotes}"))
            {
                // Dim left As String = hold.Substring(0, hold.IndexOf("{EscapeQuotes}"))
                var left = "";
                left = hold.Substring(0, hold.IndexOf("{EscapeQuotes}"));
                // End If
                outStr.Append(left);
                hold = hold.Substring(hold.IndexOf("{EscapeQuotes}") + 14);
                // Dim holdClosure As String = hold
                // Dim right As String = hold.Substring(hold.IndexOf("{/EscapeQuotes}") + 15)
                // Dim right As Tasks.Task(Of String) = Tasks.Task(Of String).Factory.StartNew(Function()
                // Dim s As String = holdClosure
                // Return EscapeQuotes(s.Substring(s.IndexOf("{/EscapeQuotes}") + 15))
                var n = hold.IndexOf("{/EscapeQuotes}");
                var right = String.Empty;
                if (n >= 0)
                {
                    right = hold.Substring(n + "{/EscapeQuotes}".Length);
                    hold = hold.Substring(0, hold.IndexOf("{/EscapeQuotes}"));
                }

                //                                                                            End Function)
                hold = hold.Replace("\"", "\\" + "\"").Replace("\\r", "").Replace("\n", "").Replace("“", "\\" + "\"")
                    .Replace("\r", "");
                // html = html.Insert(i, s.Replace(ControlChars.Quote, "\" + ControlChars.Quote).Replace("\r", "").Replace("\r", "").Replace(ChrW(8220), "\" + ControlChars.Quote))
                // html = html.Insert(i, s + "HIME" + StrToHex(s) + "REALLY")
                // Else
                //    Exit While
                // End If
                // i = html.IndexOf("{EscapeQuotes}", i + 1)
                // hold = left + hold + right
                // hold = left + hold + Right.Result
                outStr.Append(hold);
                hold = right;
            }

            outStr.Append(hold);
            return outStr.ToString();
        }

        public static string EscapeQuotesOld(string html)
        {
            int n;
            var i = html.IndexOf("{EscapeQuotes}");
            var s = "";
            while (i >= 0)
            {
                n = html.IndexOf("{/EscapeQuotes}", i + 1);
                if (n >= 0)
                {
                    // s = html.Substring(i + 14, n - i + 14)
                    s = html.Substring(i + 14, n - i - 14);
                    html = html.Remove(i, n - i + 15);
                    html = html.Insert(i,
                        s.Replace("\"", "\\" + "\"").Replace("\\r", "").Replace("\\r", "").Replace("“", "\\" + "\""));
                    // html = html.Insert(i, s + "HIME" + StrToHex(s) + "REALLY")
                }
                else
                {
                    break;
                }

                i = html.IndexOf("{EscapeQuotes}", i + 1);
            }

            return html;
        }

        public static string EscapeChars(string html)
        {
            int n;
            var i = html.IndexOf("{EscapeChars:");
            int m;
            int l;
            var replaceSt = "";
            var s = "";
            var findSt = "";
            while (i >= 0)
            {
                n = html.IndexOf("{/EscapeChars}", i + 1);
                m = html.IndexOf(":", i + 13);
                if (m == -1)
                {
                    break;
                }

                // exit if the collon isnt found
                findSt = html.Substring(i + 13, m - i - 13);
                l = html.IndexOf("}", m + 1);
                if (l == -1)
                {
                    break;
                }

                // exit if the collon isnt found
                replaceSt = html.Substring(m + 1, l - m - 1);
                if (n >= 0)
                {
                    // s = html.Substring(i + 14, n - i + 14)
                    s = html.Substring(l + 1, n - l - 1);
                    html = html.Remove(i, n - i + 14);
                    html = html.Insert(i, s.Replace(findSt, replaceSt));
                }
                else
                {
                    break;
                }

                i = html.IndexOf("{EscapeChars:", l + 1);
            }

            return html;
        }

        // {for:index=1:index<10}
        // {if {get:Feature_Text_{index}_Long_Text}}
        // {/eIF}{/if}
        // {/for}
        public static string ForLoop(string html, ContentPiece piece)
        {
            var s =
                "(?\'forblock\'\\{for\\:(?\'var1\'[\\w\\d]+)[=]+(?\'index\'[\\d]+):(?\'var2\'[\\w\\d]+)(?\'c\'[<,=]+)(?\'m\'[\\d]+).*\\}(?\'" +
                "text\'(?s).*?)\\{/for\\})";
            var r = new Regex(s, RegexOptions.Multiline);
            var m = r.Match(html);
            while (m.Success)
            {
                var t = m.Value;
                if (m.Groups.Count > 0)
                {
                    var forBlock = m.Groups["forblock"];
                    html = html.Remove(forBlock.Index, forBlock.Length);
                    var text = m.Groups["text"].Value;
                    var index = -1;
                    var variableName = String.Empty;
                    var condition = "<";
                    var cap = -1;
                    if (m.Groups["m"].Success)
                    {
                        cap = int.Parse(m.Groups["m"].Value);
                    }

                    if (m.Groups["c"].Success)
                    {
                        condition = m.Groups["c"].Value;
                    }

                    if (m.Groups["index"].Success)
                    {
                        index = int.Parse(m.Groups["index"].Value);
                    }

                    if (m.Groups["var1"].Success)
                    {
                        variableName = m.Groups["var1"].Value;
                    }

                    if (condition == "<")
                    {
                        cap = cap - 1;
                    }

                    var i = cap;
                    while (i >= index)
                    {
                        html = html.Insert(forBlock.Index, text.Replace("{" + variableName + "}", i.ToString()).Trim());
                        i = i - 1;
                    }
                }

                m = r.Match(html);
            }

            return html;
        }

        // {ife:fieldname}{/ife}
        public static string FieldExists(string html, ContentPiece piece)
        {
            var r = new Regex(
                "(?\'fieldExistBlock\'\\{ife\\:(?\'var1\'[\\w,\\d,_,-\\{\\}]+)\\}(?\'text\'(?s).*?)\\{/ife\\})",
                RegexOptions.Multiline);
            var m = r.Match(html);
            while (m.Success)
            {
                // it is possible that there are nested {ife}
                var i = html.IndexOf("{ife:", m.Index + 5);
                while (i > 0)
                {
                    var temp = r.Match(html, i);
                    if (temp.Success)
                    {
                        m = temp;
                        i = html.IndexOf("{ife:", m.Index + 5);
                    }
                    else
                    {
                        break;
                    }
                }

                var t = m.Value;
                if (m.Groups.Count > 0)
                {
                    var fieldExistBlock = m.Groups["fieldExistBlock"];
                    html = html.Remove(fieldExistBlock.Index, fieldExistBlock.Length);
                    var text = m.Groups["text"].Value;
                    var ifeBlock = "";
                    var elseBlock = "";
                    var index = text.IndexOf("{ifne}", StringComparison.CurrentCultureIgnoreCase);
                    if (index >= 0)
                    {
                        ifeBlock = text.Substring(0, index);
                        elseBlock = text.Substring(index + 6);
                    }
                    else
                    {
                        ifeBlock = text;
                        elseBlock = String.Empty;
                    }

                    var variableName = String.Empty;
                    if (m.Groups["var1"].Success)
                    {
                        variableName = m.Groups["var1"].Value;
                    }

                    if (!string.IsNullOrEmpty(variableName))
                    {
                        if (ContentPieceTypeCreator.ExistsAndHasValue(piece, variableName))
                        {
                            html = html.Insert(fieldExistBlock.Index, ifeBlock.Trim());
                        }
                        else
                        {
                            html = html.Insert(fieldExistBlock.Index, elseBlock.Trim());
                        }
                    }
                    else
                    {
                        html = html.Insert(fieldExistBlock.Index, elseBlock.Trim());
                    }

                    m = r.Match(html);
                }
            }

            return html;
        }

        public static string DateTimeFunctions(string html)
        {
            var s = html;
            s = s.Replace("{Date:GetCurrentDate}", DateTime.Now.ToString("MM/dd/yyyy"));
            s = s.Replace("{Date:GetCurrentTime}", DateTime.Now.ToString("HH:mm:ss zzz"));
            return s;
        }

        public static string OriginalRequestQuery(string s, string url)
        {
            try
            {
                var tempUrl = new Uri(url);
                var tempSt = tempUrl.PathAndQuery;
                if (tempSt[0] == '/')
                {
                    tempSt = tempSt.Substring(1);
                }

                return s.Replace("{OriginalRequestQuery}", HttpUtility.UrlDecode(tempSt));
            }
            catch (Exception ex)
            {
                return s;
            }
        }

        public static string AppendHttpToDeepLinkUrl(string s, string requestedUrl)
        {
            try
            {
                var uri = new Uri(requestedUrl);
                if (!s.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase)
                    && !s.StartsWith("https://", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (uri.Port == 443
                        && uri.Scheme.Equals("http", StringComparison.CurrentCultureIgnoreCase))
                    {
                        s = "https://" + s;
                    }
                    else
                    {
                        s = uri.Scheme + Uri.SchemeDelimiter + s;
                    }
                }
            }
            catch (Exception ex)
            {
                //  ignore exception and return the original requested string
            }

            return s;
        }

        public static string ConvertStringToURI(string html, string requestedUrl)
        {
            const string landmark = "ConvertStringToURI";
            var endLandmark = "{/" + landmark + "}";
            var startLandmark = "{" + landmark + "}";
            int n;
            var i = html.IndexOf(startLandmark, StringComparison.Ordinal);
            var l = startLandmark.Length;
            var s = "";
            while (i >= 0)
            {
                n = html.IndexOf(endLandmark, i + 1, StringComparison.CurrentCulture);
                if (n >= 0)
                {
                    s = html.Substring(i + l, n - i - l);
                    html = html.Remove(i, n - i + l + 1);
                    var uri = new Uri(requestedUrl);
                    if (!s.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase)
                        && !s.StartsWith("https://", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (uri.Port == 443
                            && uri.Scheme.Equals("http", StringComparison.CurrentCultureIgnoreCase))
                        {
                            s = "https://" + s;
                        }
                        else
                        {
                            s = uri.Scheme + Uri.SchemeDelimiter + s;
                        }
                    }

                    html = html.Insert(i, s);
                }
                else
                {
                    break;
                }

                i = html.IndexOf(startLandmark, i + 1, StringComparison.CurrentCulture);
            }

            return html;
        }
    }
}