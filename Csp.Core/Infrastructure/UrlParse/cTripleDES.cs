﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class cTripleDES
    {
        //  Define the triple des provider
        private TripleDESCryptoServiceProvider m_des = new TripleDESCryptoServiceProvider();

        //  Define the string handler
        private UTF8Encoding m_utf8 = new UTF8Encoding();

        //  Define the local property arrays
        private byte[] m_key;

        private byte[] m_iv;

        public cTripleDES(byte[] key, byte[] iv)
        {
            m_key = key;
            m_iv = iv;
        }

        public byte[] Encrypt(byte[] input)
        {
            return Transform(input, m_des.CreateEncryptor(m_key, m_iv));
        }

        public byte[] Decrypt(byte[] input)
        {
            return Transform(input, m_des.CreateDecryptor(m_key, m_iv));
        }

        public string Encrypt(string text)
        {
            var input = m_utf8.GetBytes(text);
            var output = Transform(input, m_des.CreateEncryptor(m_key, m_iv));
            return Convert.ToBase64String(output);
        }

        public string Decrypt(string text)
        {
            var input = Convert.FromBase64String(text);
            var output = Transform(input, m_des.CreateDecryptor(m_key, m_iv));
            return m_utf8.GetString(output);
        }

        private byte[] Transform(byte[] input, ICryptoTransform cryptoTransform)
        {
            var memStream = new MemoryStream();
            var cryptStream = new CryptoStream(memStream, cryptoTransform, CryptoStreamMode.Write);
            //  Transform the bytes as requested
            cryptStream.Write(input, 0, input.Length);
            cryptStream.FlushFinalBlock();
            //  Read the memory stream and convert it back into byte array
            memStream.Position = 0;
            var result = new byte[memStream.Length - 1];
            memStream.Read(result, 0, result.Length);
            //  Close and release the streams
            memStream.Close();
            cryptStream.Close();
            //  Hand back the encrypted buffer
            return result;
        }
    }
}