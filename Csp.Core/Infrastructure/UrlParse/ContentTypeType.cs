﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ContentTypeType //ContentCollectionCode.vb
    {
        public string fContentType;
        public Type fType;

        public ContentTypeType(string vContentType, Type vType)
        {
            fContentType = vContentType;
            fType = vType;
        }

        public static ContentTypeType GetContentTypeType(ref List<ContentTypeType> myTypeList, string whichCType)
        {
            var inTypeList = whichCType.Split('-');
            //  EGO 02-07-11 allow them to pass more than one type in the sort parameters incase the one specfied was null in the result set. 
            foreach (var inTypeInstance in inTypeList)
            {
                foreach (var item in myTypeList)
                {
                    if (item.fContentType == inTypeInstance)
                    {
                        return item;
                        //  EGO loop through the types in order, try the fall backs too 
                    }

                    if ("ct" + item.fContentType == inTypeInstance)
                    {
                        return item;
                        //  EGO loop through the types in order, try the fall backs too 
                    }
                }
            }

            return null;
        }

        public static ContentTypeType GetTypeByContentTypewPropValuePairList(ref List<ContentTypeType> myTypesList,
            string contentType, ref List<PropValuePair> curPropValuePairList)
        {
            //  Search in MyTypesList for ContentType using GetContentTypeType 
            //  if it doesn't exist create a type using the prop value pair and add it to the MyTypesList
            //  if it does exist return it
            var theTypeType = GetContentTypeType(ref myTypesList, contentType);
            if (theTypeType == null)
            {
                //  create the type!
                var customTypeBuilder = new ContentPieceTypeCreator(contentType);
                foreach (var item in curPropValuePairList)
                {
                    customTypeBuilder.PropertyList.Add(new PropertyInfo(item.fPropertyInfo.Name,
                        item.fPropertyInfo.TypeInfo));
                }

                var newType = customTypeBuilder.BuildDynamicTypeWithProperties(typeof(ContentPiece));
                theTypeType = new ContentTypeType(contentType, newType);
                myTypesList.Add(theTypeType);
                return theTypeType;
            }
            return theTypeType;
        }
    }
}