﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ContentPieceTypeCreator
    {
        public string Name { get; set; }

        public List<PropertyInfo> PropertyList { get; set; }

        public ContentPieceTypeCreator(string name)
        {
            Name = name;
            PropertyList = new List<PropertyInfo>();
        }

        public void BuildConstructor(Type baseType, ref TypeBuilder myTypeBuilder)
        {
            Type[] ctorParams = {new ContentPieceParameters().GetType()};
            var bc = baseType.GetConstructors()[0];
            var cb = myTypeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, ctorParams);
            var ctorIl = cb.GetILGenerator();
            if (!(bc == null))
            {
                ctorIl.Emit(OpCodes.Ldarg_0);
                ctorIl.Emit(OpCodes.Ldarg_1);
                ctorIl.Emit(OpCodes.Call, bc);
                ctorIl.Emit(OpCodes.Ret);
            }
        }

        public Type BuildDynamicTypeWithProperties(Type baseClass)
        {
            var myDomain = Thread.GetDomain();
            var myAsmName = new AssemblyName();
            myAsmName.Name = "CustomDynamicAssembly";
            var myAsmBuilder = myDomain.DefineDynamicAssembly(myAsmName, AssemblyBuilderAccess.Run);
            //  Generate a single-module assembly.
            var myModBuilder = myAsmBuilder.DefineDynamicModule(myAsmName.Name);
            var myTypeBuilder = myModBuilder.DefineType(Name, TypeAttributes.Public);
            myTypeBuilder.SetParent(baseClass);
            BuildConstructor(baseClass, ref myTypeBuilder);
            //  The property set and property get methods require a special
            //  set of attributes.
            var getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;
            //  The last argument of DefineProperty is Nothing, because the
            //  property has no parameters. (If you don't specify Nothing, you must
            //  specify an array of Type objects. For a parameterless property,
            //  use an array with no elements: New Type() {})
            foreach (var prop in PropertyList)
            {
                //  Define a private field to hold the property value.
                var privateFieldBldr =
                    myTypeBuilder.DefineField("m_" + prop.Name, prop.TypeInfo, FieldAttributes.Private);
                var custNamePropBldr =
                    myTypeBuilder.DefineProperty(prop.Name, PropertyAttributes.HasDefault, prop.TypeInfo, null);
                //  Define the "get" accessor method for CustomerName.
                var custNameGetPropMthdBldr =
                    myTypeBuilder.DefineMethod("get_" + prop.Name, getSetAttr, prop.TypeInfo, Type.EmptyTypes);
                var custNameGetIl = custNameGetPropMthdBldr.GetILGenerator();
                custNameGetIl.Emit(OpCodes.Ldarg_0);
                custNameGetIl.Emit(OpCodes.Ldfld, privateFieldBldr);
                custNameGetIl.Emit(OpCodes.Ret);
                //  Define the "set" accessor method for CustomerName.
                var custNameSetPropMthdBldr = myTypeBuilder.DefineMethod("set_" + prop.Name, getSetAttr, null,
                    new Type[]
                    {
                        prop.TypeInfo
                    });
                var custNameSetIl = custNameSetPropMthdBldr.GetILGenerator();
                custNameSetIl.Emit(OpCodes.Ldarg_0);
                custNameSetIl.Emit(OpCodes.Ldarg_1);
                custNameSetIl.Emit(OpCodes.Stfld, privateFieldBldr);
                custNameSetIl.Emit(OpCodes.Ret);
                //  Last, we must map the two methods created above to our PropertyBuilder to 
                //  their corresponding behaviors, "get" and "set" respectively. 
                custNamePropBldr.SetGetMethod(custNameGetPropMthdBldr);
                custNamePropBldr.SetSetMethod(custNameSetPropMthdBldr);
            }

            var retval = myTypeBuilder.CreateType();
            return retval;
        }

        // BuildDynamicTypeWithProperties
        public static void SetProperty(object o, string name, object value)
        {
            // If IsDBNull(value) Then
            // value = Nothing
            // End If
            if (Convert.IsDBNull(value))
            {
                value = null;
            }
            else if (string.IsNullOrEmpty(value.ToString()))
            {
                value = null;
            }

            o.GetType().InvokeMember(name, BindingFlags.SetProperty | BindingFlags.SetField, null, o, new object[]
            {
                value
            });
        }

        public static object GetProperty(object o, string name)
        {
            try
            {
                return o.GetType().InvokeMember(name, BindingFlags.GetProperty, null, o, null);
                //  BindingFlags.IgnoreCase didn't work so I had to hack below
            }
            catch (MissingMethodException ex)
            {
                var mytype = o.GetType();
                var myPropertyInfos = mytype.GetProperties();
                //object returnObj = null;
                foreach (var prop in myPropertyInfos)
                {
                    if (string.Equals(prop.Name, name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Not String.Compare(Prop.Name, name, True) Then
                        try
                        {
                            return o.GetType().InvokeMember(prop.Name, BindingFlags.GetProperty, null, o, null);
                        }
                        catch (Exception subex)
                        {
                            return null;
                        }
                    }
                }

                return null;
                // "Invalid Property Access on Property: " + name + " Content Main ID: " + CType(o, ContentPiece).fCTMainID
            }
        }

        public static bool ExistsAndHasValue(object o, string propertyName)
        {
            // Try
            //     Dim result As Object = o.GetType().InvokeMember(propertyName, (BindingFlags.GetProperty OrElse BindingFlags.IgnoreCase), Nothing, o, Nothing)
            //     Return (Not result Is Nothing) And (Not String.IsNullOrEmpty(result.ToString()))
            // Catch ex As Exception
            //     Return False
            // End Try
            var mytype = o.GetType();
            var myPropertyInfos = mytype.GetProperties();
            foreach (var prop in myPropertyInfos)
            {
                if (prop.Name.Equals(propertyName, StringComparison.CurrentCultureIgnoreCase))
                {
                    var result = o.GetType().InvokeMember(prop.Name, BindingFlags.GetProperty, null, o, null);
                    // If result Is Nothing Then Return False
                    if (string.IsNullOrEmpty(result.ToString()))
                    {
                        return false;
                    }

                    return true;
                }
            }

            return false;
        }

        public static string GetPropertyAsSafeString(object o, string name)
        {
            var holdValue = GetProperty(o, name.Replace(" ", "_"));
            //  to convert to property name as needed
            return holdValue == null ? "" : holdValue.ToString();
        }

        public static ContentPiece GetCustomContentPieceInstanceFromTypewPropValuePairList(Type curType,
            ref List<PropValuePair> curPropValuePairList, ref ContentPieceParameters contentPieceParams)
        {
            var contentPieceInstance = (ContentPiece) Activator.CreateInstance(curType, contentPieceParams);
            foreach (var item in curPropValuePairList)
            {
                SetProperty(contentPieceInstance, item.fPropertyInfo.Name, item.fPropertyValue);
            }

            return contentPieceInstance;
        }
    }
}