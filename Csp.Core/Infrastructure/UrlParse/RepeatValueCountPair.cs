﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class RepeatValueCountPair : IComparable<RepeatValueCountPair>
    {
        public string Value;

        public int Count;

        public RepeatValueCountPair(string inValue)
        {
            Value = inValue;
            Count = 1;
        }

        public static void GetOrAddRepeatValueCountPairToList(string inValue,
            ref List<RepeatValueCountPair> inValueCountList)
        {
            var foundIt = false;
            foreach (var item in inValueCountList)
            {
                if (item.Value.ToUpper() == inValue.ToUpper())
                {
                    foundIt = true;
                    item.Count = item.Count + 1;
                    return;
                }
            }

            if (!foundIt)
            {
                inValueCountList.Add(new RepeatValueCountPair(inValue));
            }
        }

        public int CompareTo(RepeatValueCountPair other)
        {
            return Count.CompareTo(other.Count);
        }
    }
}