﻿namespace Csp.Core.Infrastructure.UrlParse
{
    public class InnerRepeatBlock
    {
        public string WithDataRepeatBlock;

        public string WithoutDataRepeatBlock;

        public string InnerRepeatBlockID;

        public static string PluckOutTagSet(string inString, string leadingTag, string trailingTag)
        {
            //  will take out everything from leading tag to trailing tag from InString
            var holdFront = inString.Substring(0, inString.IndexOf(leadingTag));
            var holdRear = inString.Substring(inString.IndexOf(trailingTag));
            holdRear = holdRear.Substring(trailingTag.Length);
            return holdFront + holdRear;
        }

        private string GetFirstInnerRepeatBlockID(string innerRepeatText)
        {
            //  find first inner repeat block get id 
            // {InnerRepeatXXXX} get XXXX
            var holdString = innerRepeatText;
            holdString = holdString.Substring(holdString.IndexOf("{InnerRepeat"));
            holdString = holdString.Substring(12);
            holdString = holdString.Substring(0, holdString.IndexOf("}"));
            return holdString;
        }

        private void StripOutEmptyRepeatAnyAsNeeded(string innerRepeatText)
        {
            //  if there are no more inner repeats, pull off empty repeat any if it exists
            if (!innerRepeatText.Contains("{InnerRepeat"))
            {
                if (innerRepeatText.Contains("{EmptyRepeatANY}"))
                {
                    innerRepeatText = PluckOutTagSet(innerRepeatText, "{EmptyRepeatANY}", "{/EmptyRepeatANY}");
                }
            }
        }

        private string GetAndStripReleatedEmptyRepeatByID(ref string innerRepeatText, string InnerRepeatBlockID)
        {
            //  gets related {EmptyRepeatXXX}Bla{/EmptyRepeatXXX} where XXX = InnerRepeat block ID
            //  If Nothing is found use EmptyRepeatAny, if that's not found return ""
            //  Strip EmptyRepeatXXX out of the InnerRepeatText
            var holdString = innerRepeatText;
            var holdRepeatTextId = "EmptyRepeat" + InnerRepeatBlockID;
            if (holdString.Contains("{" + holdRepeatTextId + "}"))
            {
                holdString = holdString.Substring(holdString.IndexOf("{" + holdRepeatTextId));
                holdString = holdString.Substring(holdRepeatTextId.Length + 2);
                holdString = holdString.Substring(0, holdString.IndexOf("{/" + holdRepeatTextId));
                innerRepeatText = PluckOutTagSet(innerRepeatText, "{" + holdRepeatTextId + "}",
                    "{/" + holdRepeatTextId + "}");
            }
            else if (holdString.Contains("{" + "EmptyRepeatANY" + "}"))
            {
                holdRepeatTextId = "EmptyRepeatANY";
                holdString = holdString.Substring(holdString.IndexOf("{" + holdRepeatTextId));
                holdString = holdString.Substring(holdRepeatTextId.Length + 2);
                holdString = holdString.Substring(0, holdString.IndexOf("{/" + holdRepeatTextId));
                //   InnerRepeatText = InnerRepeatBlock.PluckOutTagSet(InnerRepeatText, "{" + HoldRepeatTextID + "}", "{/" + HoldRepeatTextID + "}")
            }
            else
            {
                holdString = "";
            }

            return holdString;
        }

        private string GetAndStripInnerRepeatBlockByID(ref string innerRepeatText, string InnerRepeatBlockID)
        {
            // Grabs {InnerRepeatXXX by ID 
            //  returns text inside the bracket
            //  strips InnerRepeat out of InnerRepeatText
            var holdString = innerRepeatText;
            var holdRepeatTextId = "InnerRepeat" + InnerRepeatBlockID;
            holdString = holdString.Substring(holdString.IndexOf("{" + holdRepeatTextId));
            holdString = holdString.Substring(holdRepeatTextId.Length + 2);
            holdString = holdString.Substring(0, holdString.IndexOf("{/" + holdRepeatTextId));
            innerRepeatText =
                PluckOutTagSet(innerRepeatText, "{" + holdRepeatTextId + "}", "{/" + holdRepeatTextId + "}");
            return holdString;
        }

        public InnerRepeatBlock(ref string innerRepeatText)
        {
            // Should take:
            // {repeat}
            // {InnerRepeat1}Background = BLUE partno:{Fpartno}{/InnerRepeat1}
            // {InnerRepeat2}Background = Yellow partno:{Fpartno}{/InnerRepeat2}
            // {EmptyRepeat1} Background = BLUE {/EmptyRepeat1}
            // {EmptyRepeat2} Background = Yellow {/EmptyRepeat2}
            // {EmptyRepeatANY} Background = BLUE {/EmptyRepeatANY}
            // {/repeat}
            //  and strip the top Inner Repeat Off (if it exists)
            //  should pair with Empty Repeat on bottom (strip off too)
            //  if empty repeat not found use EmptyRepeatANY
            //  if last inner repeat pull emptyRepeatAny out too
            InnerRepeatBlockID = GetFirstInnerRepeatBlockID(innerRepeatText);
            WithDataRepeatBlock =
                GetAndStripInnerRepeatBlockByID(ref innerRepeatText, InnerRepeatBlockID);
            WithoutDataRepeatBlock =
                GetAndStripReleatedEmptyRepeatByID(ref innerRepeatText, InnerRepeatBlockID);
            StripOutEmptyRepeatAnyAsNeeded(innerRepeatText);
        }
    }
}