﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ConsumerBaseType //ContentCollectionCode.vb
    {
        public string BaseDomain { get; set; }

        public string ConsumerID { get; set; }

        public string companyname { get; set; }

        public string address1 { get; set; }

        public string address2 { get; set; }

        public string city { get; set; }

        public string zipcode { get; set; }

        public string country { get; set; }

        public bool is_consumer { get; set; }

        public bool is_supplier { get; set; }

        public string global_bin_local_path { get; set; }

        public string global_bin_base_url { get; set; }

        public string consumer_bin_local_path { get; set; }

        public string consumer_bin_base_url { get; set; }

        public string supplier_bin_local_path { get; set; }

        public string supplier_bin_base_url { get; set; }

        public string displayname { get; set; }

        public string state { get; set; }

        public string DefaultLanguage { get; set; }

        public string website_url { get; set; }

        public ConsumerBaseType(ConsumerConstructorParams inParams)
        {
            BaseDomain = inParams.BaseDomain;
            ConsumerID = inParams.ConsumerID;
            companyname = inParams.companyname;
            address1 = inParams.address1;
            address2 = inParams.address2;
            city = inParams.city;
            zipcode = inParams.zipcode;
            country = inParams.country;
            is_consumer = inParams.is_consumer;
            is_supplier = inParams.is_supplier;
            global_bin_local_path = inParams.global_bin_local_path;
            global_bin_base_url = inParams.global_bin_base_url;
            consumer_bin_local_path = inParams.consumer_bin_local_path;
            consumer_bin_base_url = inParams.consumer_bin_base_url;
            supplier_bin_local_path = inParams.supplier_bin_local_path;
            supplier_bin_base_url = inParams.supplier_bin_base_url;
            displayname = inParams.displayname;
            state = inParams.state;
            DefaultLanguage = inParams.DefaultLanguage;
            website_url = inParams.website_url;
        }

        public static ConsumerBaseType FindConsumerBaseObjInListOfConsumerBase(ref List<ConsumerBaseType> listOfConsumerBase, string baseDomain, string supplierId)
        {
            if (baseDomain != "")
            {
                foreach (var item in listOfConsumerBase)
                {
                    if (item.BaseDomain == baseDomain)
                    {
                        return item;
                    }

                }

            }
            else
            {
                foreach (var item in listOfConsumerBase)
                {
                    if (item.ConsumerID == supplierId)
                    {
                        return item;
                    }

                }

            }

            return null;
        }

        public static void AssignRightConsumerDataToContentPiece(ref List<ConsumerBaseType> inListOfConsumer, ref ContentPiece inContentPiece)
        {
            var supplierId = inContentPiece.CTMainSupplierID;
            if (supplierId != "")
            {
                var relatedConsumer = FindConsumerBaseObjInListOfConsumerBase(ref inListOfConsumer, "", supplierId);
                inContentPiece.fRelatedSupplierInfo = relatedConsumer;
            }

        }

        public static ConsumerBaseType GetConsumerSupplierData(string baseDomain, string supplierId, ref List<ConsumerBaseType> currentConsumerBaseTypeList, ref List<ConsumerTypeType> consumerTypeTypeList, ref SqlConnection openSqlConnection)
        {
            //  base domain or supplier id should be filled out but ot both
            bool isBaseDomain = false;
            if ((baseDomain != ""))
            {
                isBaseDomain = true;
            }

            //  should search through CurrentConsumerBaseTypeList first and return that match if it exists
            //  otherwise look it up in SQL and add it to the list
            var holdConsumer = FindConsumerBaseObjInListOfConsumerBase(ref currentConsumerBaseTypeList, baseDomain, supplierId);
            if (!(holdConsumer == null))
            {
                return holdConsumer;
            }

            var fSqlConnection = openSqlConnection;
            // Dim fBaseDomain As String = "dcm1.aemotion.com"
            // fSQLConnection.Open()
            SqlCommand command;
            if (isBaseDomain)
            {
                var holdSqlParams = new List<SqlParameter>();
                holdSqlParams.Add(new SqlParameter("@basedomain", baseDomain));
                //  fixed incase there are no companyies parameters command = New SqlClient.SqlCommand("SELECT companies_consumers.companies_Id, companies_consumers.base_domain, companies.companies_Id AS Expr1, companies.companyname, companies.address1, companies.address2, companies.city, companies.zipcode, companies.country, companies.is_consumer, companies.is_supplier,   companies.displayname, companies_parameters.companies_parameter_types_Id, companies_parameters.value_integer, companies_parameters.value_text, companies_parameters.value_date, companies_parameters.value_double, companies_parameters.value_boolean, companies_parameter_types.parametername, companies_parameter_types.parametertype fROM companies_consumers INNER JOIN companies ON companies_consumers.companies_Id = companies.companies_Id INNER JOIN companies_parameters ON companies.companies_Id = companies_parameters.companies_Id INNER JOIN companies_parameter_types ON companies_parameters.companies_parameter_types_Id = companies_parameter_types.companies_parameter_types_Id WHERE        (companies_consumers.base_domain = @basedomain)")
                // command = New SqlClient.SqlCommand("SELECT companies_consumers.companies_Id, companies_consumers.base_domain, companies.companies_Id AS Expr1, companies.companyname, companies.address1, companies.address2, companies.city, companies.zipcode, companies.country, companies.is_consumer, companies.is_supplier,   companies.displayname, companies.state, companies_parameters.companies_parameter_types_Id, companies_parameters.value_integer, companies_parameters.value_text, companies_parameters.value_date, companies_parameters.value_double, companies_parameters.value_boolean, companies_parameter_types.parametername, companies_parameter_types.parametertype FROM companies_parameter_types INNER JOIN companies_parameters ON companies_parameter_types.companies_parameter_types_Id = companies_parameters.companies_parameter_types_Id RIGHT OUTER JOIN companies_consumers INNER JOIN companies ON companies_consumers.companies_Id = companies.companies_Id ON  companies_parameters.companies_Id = companies.companies_Id WHERE        (companies_consumers.base_domain = @basedomain)")
                var selectStatement = "";
                selectStatement +=
                    " SELECT    companies_consumers.companies_Id, companies_consumers.base_domain, companies.companies_Id " +
                    "AS Expr1, companies.companyname, ";
                selectStatement +=
                    " companies.address1, companies.address2, companies.city, companies.zipcode, companies.country, compan" +
                    "ies.website_url, companies.is_consumer, companies.is_supplier, ";
                selectStatement +=
                    " companies.displayname, companies.state, companies_parameters.companies_parameter_types_Id, companies" +
                    "_parameters.value_integer, ";
                selectStatement +=
                    " companies_parameters.value_text, companies_parameters.value_date, companies_parameters.value_double," +
                    " companies_parameters.value_boolean, ";
                selectStatement +=
                    " companies_parameter_types.parametername, companies_parameter_types.parametertype, companies_consumer" +
                    "s.default_language_Id";
                selectStatement += " FROM      companies_parameter_types INNER JOIN";
                selectStatement += " companies_parameters ON ";
                selectStatement +=
                    " companies_parameter_types.companies_parameter_types_Id = companies_parameters.companies_parameter_ty" +
                    "pes_Id RIGHT OUTER JOIN";
                selectStatement += " companies_consumers INNER JOIN";
                selectStatement += " companies ON companies_consumers.companies_Id = companies.companies_Id ON ";
                selectStatement += " companies_parameters.companies_Id = companies.companies_Id";
                selectStatement += " WHERE     (companies_consumers.base_domain = @basedomain) ";
                command = new SqlCommand(selectStatement);
                // companies.global_bin_local_path, companies.global_bin_base_url, companies.consumer_bin_local_path, companies.consumer_bin_base_url, companies.supplier_bin_local_path, companies.supplier_bin_base_url, removed from table
                command.Connection = fSqlConnection;
                foreach (var item in holdSqlParams)
                {
                    command.Parameters.Add(item);
                }

            }
            else
            {
                var holdSqlParams = new List<SqlParameter>();
                holdSqlParams.Add(new SqlParameter("@SupplierID", supplierId));
                // command = New SqlClient.SqlCommand("SELECT companies_consumers.companies_Id, companies_consumers.base_domain, companies.companies_Id AS Expr1, companies.companyname, companies.address1, companies.address2, companies.city, companies.zipcode, companies.country, companies.is_consumer, companies.is_supplier,   companies.displayname, companies.state, companies_parameters.companies_parameter_types_Id, companies_parameters.value_integer, companies_parameters.value_text, companies_parameters.value_date, companies_parameters.value_double, companies_parameters.value_boolean, companies_parameter_types.parametername, companies_parameter_types.parametertype fROM            companies LEFT OUTER JOIN  companies_consumers ON companies.companies_Id = companies_consumers.companies_Id LEFT OUTER JOIN companies_parameters ON companies.companies_Id = companies_parameters.companies_Id LEFT OUTER JOIN  companies_parameter_types ON companies_parameters.companies_parameter_types_Id = companies_parameter_types.companies_parameter_types_Id wHERE        (companies.companies_Id = @SupplierID)")
                // companies.global_bin_local_path,companies.global_bin_base_url, companies.consumer_bin_local_path, companies.consumer_bin_base_url, companies.supplier_bin_local_path, companies.supplier_bin_base_url, removed from the table
                var selectStatement = "";
                selectStatement +=
                    " SELECT     companies_consumers.companies_Id, companies_consumers.base_domain, companies.companies_Id" +
                    " AS Expr1, companies.companyname, ";
                selectStatement +=
                    " companies.address1, companies.address2, companies.city, companies.zipcode, companies.country, compan" +
                    "ies.website_url, companies.is_consumer, companies.is_supplier, ";
                selectStatement +=
                    " companies.displayname, companies.state, companies_parameters.companies_parameter_types_Id, companies" +
                    "_parameters.value_integer, ";
                selectStatement +=
                    " companies_parameters.value_text, companies_parameters.value_date, companies_parameters.value_double," +
                    " companies_parameters.value_boolean, ";
                selectStatement +=
                    " companies_parameter_types.parametername, companies_parameter_types.parametertype, companies_consumer" +
                    "s.default_language_Id ";
                selectStatement += " FROM         companies LEFT OUTER JOIN";
                selectStatement +=
                    " companies_consumers ON companies.companies_Id = companies_consumers.companies_Id LEFT OUTER JOIN";
                selectStatement +=
                    " companies_parameters ON companies.companies_Id = companies_parameters.companies_Id LEFT OUTER JOIN";
                selectStatement += " companies_parameter_types ON ";
                selectStatement +=
                    " companies_parameters.companies_parameter_types_Id = companies_parameter_types.companies_parameter_ty" +
                    "pes_Id";
                selectStatement += " WHERE(companies.companies_Id = @SupplierID)";
                command = new SqlCommand(selectStatement);
                command.Connection = fSqlConnection;
                foreach (var item in holdSqlParams)
                {
                    command.Parameters.Add(item);
                }

            }

            var reader = command.ExecuteReader();
            ConsumerBaseType curConsumerObj;
            try
            {
                var isFirst = true;
                var consumerConstructor = new ConsumerConstructorParams();
                var consumerId = "";
                var curPropValuePairList = new List<PropValuePair>();
                while (reader.Read())
                {
                    if (isFirst)
                    {
                        isFirst = false;
                        consumerConstructor = new ConsumerConstructorParams(reader["base_domain"].ToString(),
                            reader["Expr1"].ToString(), reader["companyname"].ToString(), reader["address1"].ToString(),
                            reader["address2"].ToString(), reader["city"].ToString(),
                            reader["zipcode"].ToString(), reader["country"].ToString(),
                            reader["is_consumer"] as bool? ?? false, reader["is_supplier"] as bool? ?? false, "", "", "", "",
                            "", "", reader["displayname"].ToString(), reader["state"].ToString(),
                            reader["default_language_Id"].ToString(), reader["website_url"].ToString());
                        // reader.Item("global_bin_local_path").ToString,reader.Item("global_bin_base_url").ToString,reader.Item("consumer_bin_local_path").ToString,reader.Item("consumer_bin_base_url").ToString, reader.Item("supplier_bin_local_path").ToString, reader.Item("supplier_bin_base_url").ToString, removed from table
                        consumerId = consumerConstructor.ConsumerID;
                    }

                    if (reader["Expr1"].ToString() != consumerId)
                    {
                        break;
                    }

                    if (!(reader["parametername"].ToString() == ""))
                    {
                        curPropValuePairList.Add(PropValuePair.GetPropValuePairFromConsumerReaderRow(reader));
                    }

                }

                var curType = ConsumerTypeType.GetTypeByConsumerInfowPropValuePairList(ref consumerTypeTypeList, supplierId, baseDomain, ref curPropValuePairList).fType;
                //  create an instance with values
                curConsumerObj = ConsumerTypeCreator.GetCustomConsumerInstanceFromTypewPropValuePairList(curType, ref curPropValuePairList, ref consumerConstructor);
                currentConsumerBaseTypeList.Add(curConsumerObj);
            }
            finally
            {
                reader.Close();
            }

            return curConsumerObj;
        }

        public ConsumerBaseType(){}
    }
}