﻿using System;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class PageParams
    {
        public int CountPerPage;
        public int CurrentPageOffset;

        public PageParams(string vResultModifier)
        {
            if (vResultModifier.Contains("pg("))
            {
                var hold = vResultModifier.Substring(vResultModifier.IndexOf("pg(", StringComparison.Ordinal));
                hold = hold.Substring(0, hold.IndexOf(")", StringComparison.Ordinal));
                hold = hold.Substring(hold.IndexOf("(", StringComparison.Ordinal) + 1);
                var holdCountPerPage = hold.Substring(0, hold.IndexOf("*", StringComparison.Ordinal));
                hold = hold.Substring(hold.IndexOf("*", StringComparison.Ordinal) + 1);
                CountPerPage = Int32.Parse(holdCountPerPage);
                CurrentPageOffset = Int32.Parse(hold);
            }
            else
            {
                CountPerPage = 5000;
                CurrentPageOffset = 1;
            }
        }
    }
}