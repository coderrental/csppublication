﻿using System;
using System.Data.SqlClient;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class PropValuePair //ContentCollectionCode.vb
    {
        public PropertyInfo fPropertyInfo;
        public object fPropertyValue;

        public PropValuePair(PropertyInfo vPropertyInfo, object vPropertyValue)
        {
            fPropertyInfo = vPropertyInfo;
            fPropertyValue = vPropertyValue;
        }

        public static PropValuePair GetPropValuePairFromReaderRow(SqlDataReader reader, string inFieldName, int inFieldType)
        {
            var fieldName = inFieldName;
            object fieldValue;
            var dataType = inFieldType;
            PropertyInfo myPropInfo;

            switch (dataType)
            {
                case 1:
                case 7:
                    myPropInfo = new PropertyInfo(fieldName, "".GetType());
                    fieldValue = reader["value_text"];
                    break;
                case 2:
                    myPropInfo = new PropertyInfo(fieldName, new int().GetType());
                    fieldValue = reader["value_integer"];
                    break;
                case 3:
                    myPropInfo = new PropertyInfo(fieldName, new DateTime().GetType());
                    fieldValue = reader["value_date"];
                    break;
                case 4:
                    myPropInfo = new PropertyInfo(fieldName, new double().GetType());
                    fieldValue = reader["value_double"];
                    if (string.IsNullOrEmpty(fieldValue.ToString()))
                    {
                        fieldValue = "0.0";
                    }
                    break;
                case 5:
                    myPropInfo = new PropertyInfo(fieldName, true.GetType());
                    fieldValue = reader["value_boolean"];
                    break;
                default:
                    throw new Exception("Prop value pair created for datatype that isn't implemented! dt: " + dataType);
            }

            return new PropValuePair(myPropInfo, fieldValue);
        }

        public static PropValuePair GetPropValuePairFromConsumerReaderRow(SqlDataReader reader)
        {
            var dataType = reader["parametertype"].ToString();
            var fieldName = reader["parametername"].ToString();
            object fieldValue;

            PropertyInfo myPropInfo;

            switch (dataType)
            {
                case "1":
                case "7":
                    myPropInfo = new PropertyInfo(fieldName, "".GetType());
                    fieldValue = reader["value_text"];
                    break;
                case "2":
                    myPropInfo = new PropertyInfo(fieldName, new int().GetType());
                    fieldValue = reader["value_integer"];
                    break;
                case "3":
                    myPropInfo = new PropertyInfo(fieldName, new DateTime().GetType());
                    fieldValue = reader["value_date"];
                    break;
                case "4":
                    myPropInfo = new PropertyInfo(fieldName, new double().GetType());
                    fieldValue = reader["value_double"];
                    break;
                case "5":
                    myPropInfo = new PropertyInfo(fieldName, true.GetType());
                    fieldValue = reader["value_boolean"];
                    break;
                default:
                    throw new Exception("Prop value pair created for datatype that isn't implemented! dt: " + dataType);
            }

            return new PropValuePair(myPropInfo, fieldValue);
        }
    }
}