﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class DepthListOfCategories
    {
        public List<CategoryItem> CategoryItems;
        public int Depth;

        public DepthListOfCategories(int vDepth)
        {
            Depth = vDepth;
            CategoryItems = new List<CategoryItem>();
        }

        public CategoryItem GetCategoryItemFromListByCategoryID(string inCategoryId)
        {
            foreach (var item in CategoryItems)
            {
                if (item.CategoryID == inCategoryId) return item;
            }
            return null;
        }

        public static DepthListOfCategories GetOrAddByDepth(int inDepth, ref List<DepthListOfCategories> listOfDepthLists)
        {
            foreach (var item in listOfDepthLists)
            {
                if (item.Depth == inDepth)
                {
                    return item;
                }
            }

            var holdNewDepthList = new DepthListOfCategories(inDepth);
            listOfDepthLists.Add(holdNewDepthList);
            return holdNewDepthList;
        }
    }
}