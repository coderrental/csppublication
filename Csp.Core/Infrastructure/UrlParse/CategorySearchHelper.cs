﻿namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategorySearchHelper
    {
        public bool SearchUp;
        public int CategoryID;
        public bool IncludeLevel0;
        public int Depth;
        public bool IsHunt;
        public bool NoStrip;

        public CategorySearchHelper(bool vSearchUp, int vCategoryId, bool vIncludeLevel0, bool vIsHunt, int vDepth,
            bool vNoStrip)
        {
            SearchUp = vSearchUp;
            CategoryID = vCategoryId;
            IncludeLevel0 = vIncludeLevel0;
            IsHunt = vIsHunt;
            Depth = vDepth;
            NoStrip = vNoStrip;
        }
    }
}