﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ConsumerTypeType //ContentCollectionCode.vb
    {
        public string fSupplierID;
        public string fBaseDomain;
        public Type fType;

        public ConsumerTypeType(string vSupplierId, string vBaseDomain, Type vType)
        {
            fSupplierID = vSupplierId;
            fBaseDomain = vBaseDomain;
            fType = vType;
        }

        public static ConsumerTypeType GetConsumerTypeType(List<ConsumerTypeType> myTypeList, string supplierId, string baseDomain)
        {
            if (!string.IsNullOrEmpty(baseDomain))
            {
                foreach (var item in myTypeList)
                {
                    if (item.fBaseDomain == baseDomain)
                    {
                        return item;
                    }
                }
            }
            else
            {
                foreach (var item in myTypeList)
                {
                    if (item.fSupplierID == supplierId)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        public static string GetSafeBaseDomain(string baseDomain)
        {
            return baseDomain.Replace(".", "_");
        }

        public static ConsumerTypeType GetTypeByConsumerInfowPropValuePairList(ref List<ConsumerTypeType> myTypesList,
            string supplierId, string baseDomain, ref List<PropValuePair> curPropValuePairList)
        {
            //Search in MyTypesList for ContentType using GetContentTypeType 
            //if it doesn't exist create a type using the prop value pair and add it to the MyTypesList
            //if it does exist return it
            var theTypeType = GetConsumerTypeType(myTypesList, supplierId, baseDomain);
            if (theTypeType != null) return theTypeType;

            var customTypeBuilder = new ConsumerTypeCreator(supplierId + GetSafeBaseDomain(baseDomain) + "ConsumerType");
            foreach (var item in curPropValuePairList)
            {
                customTypeBuilder.PropertyList.Add(new PropertyInfo(item.fPropertyInfo.Name, item.fPropertyInfo.TypeInfo));
            }
            var newType = customTypeBuilder.BuildDynamicTypeWithProperties(new ConsumerBaseType().GetType());
            theTypeType = new ConsumerTypeType(supplierId, baseDomain, newType);
            myTypesList.Add(theTypeType);
            return theTypeType;
        }

    }
}