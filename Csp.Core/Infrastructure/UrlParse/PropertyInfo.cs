﻿using System;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class PropertyInfo //ContentCollectionCode.vb
    {
        public string Name { get; set; }
        public Type TypeInfo { get; set; }

        public PropertyInfo(string name, Type typeInfo)
        {
            Name = name.Replace(" ", "_");
            TypeInfo = typeInfo;
        }
    }
}