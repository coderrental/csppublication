﻿namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryResultHelper
    {
        public int CategoryID;
        public string Description;
        public int ParentID;
        public int Depth;

        public CategoryResultHelper(int vCategoryId, string vDescription, int vParentId, int vDepth)
        {
            CategoryID = vCategoryId;
            Description = vDescription;
            ParentID = vParentId;
            Depth = vDepth;
        }
    }
}