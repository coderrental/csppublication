﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryLoaderParams
    {
        public string RootCategory;
        public int Depth;
        public string ConsumerID;
        public string Domain;
        public List<string> SupplierIDs;
        public string ContentCategoryTypeID;
        public string SortingParams;
        public string FieldSearchParams;
        public string URLEngineConnString;

        public CategoryLoaderParams(string vRootCategory, int vDepth, string vConsumerId, string vDomain,
            List<string> vSupplierIDs, string vContentCategoryTypeId, string vSortingParams, string vFieldSearchParams,string vUrlEngineConnString)
        {
            RootCategory = vRootCategory;
            Depth = vDepth;
            ConsumerID = vConsumerId;
            Domain = vDomain;
            SupplierIDs = vSupplierIDs;
            ContentCategoryTypeID = vContentCategoryTypeId;
            SortingParams = vSortingParams;
            FieldSearchParams = vFieldSearchParams;
            URLEngineConnString = vUrlEngineConnString;
        }

        public static string GetBaseDomainOutOfURL(string inUrl)
        {
            var holdUrl = inUrl.ToUpper();
            if (holdUrl.StartsWith("HTTP://"))
            {
                holdUrl = holdUrl.Substring(7);
            }
            else if (holdUrl.StartsWith("HTTPS://"))
            {
                holdUrl = holdUrl.Substring(8);
            }
            else
            {
                throw new Exception(
                    "Error breaking URL into base domain in CategoryLoaderParams.GetBaseDomainOutOfURL. URL causing error: " +
                    inUrl);
            }

            if (holdUrl.Contains("/"))
            {
                holdUrl = holdUrl.Substring(0, holdUrl.IndexOf("/", StringComparison.Ordinal));
            }

            return holdUrl;
        }

        public static string PullCatParamsFromURL(string inUrl)
        {
            var holdCatParams = inUrl.Substring(inUrl.ToUpper().IndexOf("&!CAT:", StringComparison.Ordinal));
            holdCatParams = holdCatParams.Substring(6);
            holdCatParams = holdCatParams.Substring(0, holdCatParams.IndexOf("&!", StringComparison.Ordinal));
            return holdCatParams;
        }

        public static string PullFirstColonOut(ref string inCatParams)
        {
            if (!inCatParams.Contains(":")) return inCatParams;
            var result = inCatParams.Substring(0, inCatParams.IndexOf(":", StringComparison.Ordinal));
            inCatParams = inCatParams.Substring(inCatParams.IndexOf(":", StringComparison.Ordinal) + 1);
            return result;
        }

        public static CategoryLoaderParams GetNewCategoryLoaderParamsFromURL(string inUrl, string vUrlEngineConnString, string consumerId)
        {
            if (!inUrl.ToUpper().Contains("&!CAT:")) return null;

            var baseDomain = GetBaseDomainOutOfURL(inUrl);

            if (string.IsNullOrEmpty(baseDomain) || string.IsNullOrEmpty(consumerId))
                throw new Exception("Error Parsing Category URL issues with base domain: " + baseDomain +
                                    " ConsumerID found: " + consumerId);

            var catParams = PullCatParamsFromURL(inUrl);
            var rootCategory = PullFirstColonOut(ref catParams);
            var depth = PullFirstColonOut(ref catParams);
            var supplierId = PullFirstColonOut(ref catParams);
            var contentTypeId = PullFirstColonOut(ref catParams);

            if (string.IsNullOrEmpty(rootCategory))
                throw new Exception("RootCategory not specified in URL");
            if (string.IsNullOrEmpty(depth))
                throw new Exception("Depth not specified in URL");
            if (string.IsNullOrEmpty(supplierId))
                throw new Exception("SupplierID not specified in URL, if all suppliers desired enter 0 as the supplier id.");
            if (string.IsNullOrEmpty(contentTypeId))
                throw new Exception("Content Type not specified in URL");

            ///todo
            /// check the int.Parse(Depth)
            if (supplierId == "0")
                return new CategoryLoaderParams(rootCategory, int.Parse(depth), consumerId, baseDomain, null,
                    contentTypeId, "", "", vUrlEngineConnString);
            var holdSupplierIdList = new List<string> {supplierId};

            return new CategoryLoaderParams(rootCategory, int.Parse(depth), consumerId, baseDomain, holdSupplierIdList,
                contentTypeId, "", "", vUrlEngineConnString);
        }
    }
}