﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class URLNameValuePair
    {
        public string Name;
        public string Value;

        public URLNameValuePair(string vName, string vValue)
        {
            Name = vName;
            Value = vValue;
        }

        public string GetValueFromName(string nametoFind, List<URLNameValuePair> nameValueList)
        {
            foreach (var item in nameValueList)
            {
                if (item.Name == nametoFind)
                {
                    return item.Value;
                }
            }

            return "";
        }
    }
}
