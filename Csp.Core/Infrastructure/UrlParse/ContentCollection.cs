﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ContentCollection //ContentCollectionCode.vb
    {
        public List<CategoryResultHelper> CategoryResultHelpers;

        // 03-12-08 added to support looking up content information from the publication side
        public List<CategorySearchHelper> CategorySearchHelpers;

        public ConsumerBaseType fBaseDomainData;

        public string fConString;

        // Public fFilterExpressionChain As URLParser.ExpressionChain
        public string fDomain;

        public string fEntireURL;

        public string fFilterExpression;

        public CategoryLoader fMyCategories;

        public List<ContentPiece> fMyContent;

        public List<CategoryItem> fMyEmptyCategories = new List<CategoryItem>();

        public string fOriginalSourceDomain;

        public PageParams fPageParams;

        public string fPublication;

        public string fRawURL;

        public List<ConsumerBaseType> fRelatedConsumerSupplierData;

        public SortParams fSortParams;

        public SqlConnection fSQLConnection;

        // Public Sub AssignRelatedConsumerDataWBaseDomain(ByVal BaseDomain As String, ByRef RelatedConsumerSupplierData As List(Of ConsumerBaseType))
        //     fBaseDomainData = ConsumerBaseType.FindConsumerBaseObjInListOfConsumerBase(RelatedConsumerSupplierData, BaseDomain, "")
        //     fRelatedConsumerSupplierData = RelatedConsumerSupplierData
        // End Sub
        public ContentCollection(string vFilterExpression, string vDomain, string vPublication,
            ref SortParams vSortParams, ref PageParams vPageParams, string rawUrl, ref SqlConnection vSqlConnection,
            string inConString, string originalSourceDomain = null)
        {
            fFilterExpression = vFilterExpression;
            // Warning!!! Optional parameters not supported
            // fFilterExpressionChain = vFilterExpressionChain
            fSortParams = vSortParams;
            fPageParams = vPageParams;
            fDomain = vDomain;
            fConString = inConString;
            fPublication = vPublication;
            fMyContent = new List<ContentPiece>();
            fRawURL = rawUrl;
            fRelatedConsumerSupplierData = null;
            fBaseDomainData = null;
            fSQLConnection = vSqlConnection;
            fEntireURL = "";
            if (string.IsNullOrEmpty(originalSourceDomain))
            {
                fOriginalSourceDomain = fRawURL;
            }
            else
            {
                fOriginalSourceDomain = originalSourceDomain;
            }
        }

        public ContentCollection GetDeepCopy()
        {
            var holdCc = new ContentCollection(fFilterExpression, fDomain, fPublication, ref fSortParams,
                ref fPageParams, fRawURL, ref fSQLConnection, fConString, fOriginalSourceDomain);
            foreach (var c in fMyContent)
            {
                holdCc.fMyContent.Add(c);
            }

            holdCc.fMyCategories = fMyCategories;
            holdCc.fMyEmptyCategories = fMyEmptyCategories;
            if (!(fRelatedConsumerSupplierData == null))
            {
                holdCc.fRelatedConsumerSupplierData = new List<ConsumerBaseType>();
                foreach (var cbt in fRelatedConsumerSupplierData)
                {
                    holdCc.fRelatedConsumerSupplierData.Add(cbt);
                }
            }

            holdCc.fBaseDomainData = fBaseDomainData;
            holdCc.fSQLConnection = new SqlConnection(fConString);
            if (!(CategorySearchHelpers == null))
            {
                holdCc.CategorySearchHelpers = new List<CategorySearchHelper>();
                foreach (var csh in CategorySearchHelpers)
                {
                    holdCc.CategorySearchHelpers.Add(csh);
                }
            }

            if (!(CategoryResultHelpers == null))
            {
                holdCc.CategoryResultHelpers = new List<CategoryResultHelper>();
                foreach (var csh in CategoryResultHelpers)
                {
                    holdCc.CategoryResultHelpers.Add(csh);
                }
            }

            return holdCc;
        }

        public static void CreateEmptyCategoryList(ref ContentCollection inContentCollection)
        {
            //  Go through the items, go through all their category assignments
            //  for each category assignment, add that category and all it's parents to a list of non empty items
            //  Any category, not in the non empty list is added to the local empty list
            // what if i make a big list (unique) of all categories assigned to all the items
            //  as I assign categories, assign all parents too
            //  then just run though that list and mark all cateogries as non empty
            //  what if I add all categoreis to the empty categories list, then, remove them as they are found to be assigned to an item (and their parents)
            //  how do I find their parents (yes it has parent go me!)
            //  is InContentCollection.fMyCategories.FlatListOfAllCleanCategories = to running through incontentcollection.fMyCategories.CleanTree (yes!)
            inContentCollection.fMyEmptyCategories.Clear();
            if (inContentCollection.fMyCategories == null)
            {
                return;
            }
            if (inContentCollection.fMyCategories.FlatListOfAllCleanCategories == null)
            {
                return;
            }

            //  first add all categories to the empty list
            foreach (var item in inContentCollection.fMyCategories.FlatListOfAllCleanCategories)
            {
                inContentCollection.fMyEmptyCategories.Add(item);
            }

            //  then loop through all content, and remove from empty tree any category that is assigned, and any of it's parents
            foreach (var item in inContentCollection.fMyContent)
            {
                foreach (var cat in item.fCategoriesImAssignedTo)
                {
                    inContentCollection.RemoveCategoryFromEmptyCategoriesAndAllParents(cat);
                }
            }
        }

        public void RemoveCategoryFromEmptyCategoriesAndAllParents(string catId)
        {
            CategoryItem catToRemove = null;
            foreach (var item in fMyEmptyCategories)
            {
                if (item.CategoryID == catId)
                {
                    //  i'm here, get rid of me, then my parents
                    catToRemove = item;
                    break;
                }
            }

            while (!(catToRemove == null))
            {
                fMyEmptyCategories.Remove(catToRemove);
                catToRemove = catToRemove.ParentCategoryItem;
            }
        }

        public bool IsCategoryEmptyInMyCollection(ref CategoryItem inCategoryItem)
        {
            foreach (var item in fMyEmptyCategories)
            {
                if (item.CategoryID == inCategoryItem.CategoryID)
                {
                    return true;
                }
            }

            return false;
        }

        private int GetStartAt()
        {
            return (fPageParams.CurrentPageOffset - 1) * fPageParams.CountPerPage;
        }

        private int GetFinishAt(int startAt)
        {
            return startAt + (fPageParams.CountPerPage - 1);
        }

        public ContentCollection GetCurPageAsNewContentCollection()
        {
            if (fPageParams.CountPerPage != 0)
            {
                // Start at = PageOffset -1 * Count +1
                var startAt = GetStartAt();
                var finishAt = GetFinishAt(startAt);
                var returnCollection = new ContentCollection(fFilterExpression, fDomain, fPublication, ref fSortParams,
                    ref fPageParams, fRawURL, ref fSQLConnection, fConString, fOriginalSourceDomain);
                returnCollection.fEntireURL = fEntireURL;
                //  very ugly as a result of paging inside components 
                var contentTop = fMyContent.Count - 1;
                if (fMyContent.Count == 0)
                {
                    return returnCollection;
                }
                if (startAt > contentTop)
                {
                    return returnCollection;
                    // emptyPage!
                }
                // Start at inside MyContent.Count
                if (finishAt > contentTop)
                {
                    finishAt = contentTop;
                }

                if (startAt == finishAt)
                {
                    // just get the one piece
                    returnCollection.fMyContent.Add(fMyContent[startAt]);
                }
                else if (startAt > finishAt)
                {
                    // shouldn't happen!
                    throw new Exception("StartAt > FinishAt!");
                }
                else
                {
                    for (var index = startAt; index <= finishAt; index++)
                    {
                        var holdContentPiece = fMyContent[index];
                        returnCollection.fMyContent.Add(holdContentPiece);
                    }
                }

                return returnCollection;
            }
            return this;
        }

        // {paging:next}NextPage URL: A Href={PURL}
        // {/paging:next}{paging:previous}PreviousPage URL: A Href={PURL}
        // {/paging:previous} Current Page: {paging:currentpage}
        // Total Pages : {paging:totalpages}
        // Dim HoldCurPage As Integer = Me.fRelatedContent.fPageParams.CurrentPageOffset
        // Dim NeedsNextPage As Boolean = NeedsNextPage()
        // Dim NeedsPriorPage As Boolean = NeedsPriorPage()
        // Dim TotalPages As Integer = Me.fRelatedContent.fPageParams.TotalPages
        public bool NeedsNextPage()
        {
            //  returns true if there could exist a next page given me.fPageParams and the length of fMYContent
            if (fPageParams.CountPerPage != 0)
            {
                var startAt = GetStartAt();
                var finishAt = GetFinishAt(startAt);
                if (fMyContent.Count - 1 > finishAt)
                {
                    //  there is content on a page after this one
                    return true;
                }
            }

            return false;
        }

        public bool NeedsPriorPage()
        {
            //  returns true if there could exist a prior page given me.fPageParams and the length of fMYContent
            if (fPageParams.CountPerPage != 0)
            {
                var startAt = GetStartAt();
                if (startAt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public int TotalActualPages()
        {
            //  returns and integer based on the fPageParams and the Length of fMyContent
            if (fPageParams.CountPerPage != 0)
            {
                // Dim StartAt As Integer = 0
                // Dim FinishAt As Integer = 0
                // Start at = PageOffset -1 * Count +1
                // StartAt = GetStartAt()
                // FinishAt = GetFinishAt(StartAt)
                var totalPages = fMyContent.Count / fPageParams.CountPerPage;
                if (fMyContent.Count % fPageParams.CountPerPage > 0)
                {
                    totalPages = totalPages + 1;
                }

                return totalPages;
            }

            return 0;
        }

        public string GetPageXsURL(ref List<Tuple<string, Task<ContentCollection>>> siblingsCCs, int pageNum)
        {
            //  Gets me.fFilterExpression and all my sibling filter expressions, builds a new URL string pointing to the right page
            //  May need to check to see if me.fFilterExpression actually equals the unmodified source URL
            var holdPageNum = pageNum;
            if (holdPageNum < 1)
            {
                holdPageNum = 1;
            }
            else if (holdPageNum > TotalActualPages())
            {
                holdPageNum = TotalActualPages();
            }

            var holdUrl = fRawURL;
            string getCurPaging;
            var holdOldPaging = "";
            if (fEntireURL != "")
            {
                if (fEntireURL.Contains("pg("))
                {
                    // Dim HoldLeftSide As String = fEntireURL.Substring(0, fEntireURL.IndexOf("pg("))
                    // Dim HoldRightSide As String = fEntireURL.Substring(fEntireURL.IndexOf("pg("))
                    var holdLeftSide = fOriginalSourceDomain.Substring(0, fOriginalSourceDomain.IndexOf("pg("));
                    var holdRightSide = fOriginalSourceDomain.Substring(fOriginalSourceDomain.IndexOf("pg("));
                    var holdPaging = holdRightSide.Substring(0, holdRightSide.IndexOf(")") + 1);
                    holdRightSide = holdRightSide.Substring(holdRightSide.IndexOf(")") + 1);
                    holdPaging = holdPaging.Substring(0, holdPaging.IndexOf("*") + 1);
                    holdPaging = holdPaging + (pageNum + ")");
                    holdUrl = holdLeftSide + holdPaging + holdRightSide;
                    // hold left should be left side of URL, hold right should be right side of URL Hold paging should contain pg(##*##)
                    return holdUrl;
                }
            }
            else
            {
                if (fRawURL.Contains("["))
                {
                    getCurPaging = fRawURL.Substring(fRawURL.IndexOf("[") + 1);
                    if (getCurPaging.Contains("pg("))
                    {
                        getCurPaging = getCurPaging.Substring(getCurPaging.IndexOf("pg("));
                        holdOldPaging = getCurPaging.Substring(0, getCurPaging.IndexOf(")") + 1);
                        getCurPaging = getCurPaging.Substring(0, getCurPaging.IndexOf("*") + 1);
                        //  now = to pg(CountPerPage*
                        getCurPaging = getCurPaging
                                       + (holdPageNum + ")");
                    }
                    else
                    {
                        getCurPaging = "";
                    }
                }
                else
                {
                    getCurPaging = "";
                }

                if (getCurPaging != "")
                {
                    holdUrl = holdUrl.Replace(holdOldPaging, getCurPaging);
                }

                if (!(siblingsCCs == null))
                {
                    foreach (var item in siblingsCCs)
                    {
                        if (!(fRawURL == item.Item2.Result.fRawURL))
                        {
                            // if they are the same no need to add it! 
                            holdUrl = holdUrl + ":" + item.Item2.Result.fRawURL;
                        }
                    }
                }

                // HoldURL = "HTTP://" + Me.fDomain + "/" + HoldURL
                // ltu
                holdUrl = "http://" + fDomain + "/publish/d1.aspx?" + holdUrl;
                return holdUrl;
            }
            return "";
        }

        public static Tuple<string, Task<ContentCollection>> GetContentCollectionByPubFromCcList(
            ref List<Tuple<string, Task<ContentCollection>>> inContentCollectionList, string publication)
        {
            foreach (var cc in inContentCollectionList)
            {
                if (cc.Item1 == publication)
                {
                    return cc;
                }
            }

            return null;
        }

        public static void LookupAndAssignSupplierDataToContentCollectionObjects(string baseDomain,
            ref ContentCollection inContentCollection, ref List<ConsumerBaseType> relatedSupplierConsumerData,
            ref List<ConsumerTypeType> consumerTypeTypeList, ref SqlConnection openSqlConnection)
        {
            // Looks up the Supplier or base domain from the database as needed, otherwise uses in memory copy
            // ConsumerBaseType.GetConsumerSupplierData
            // Assigns to InContentCollection the base domain consumer object
            var baseDomainData = ConsumerBaseType.GetConsumerSupplierData(baseDomain, "",
                ref relatedSupplierConsumerData, ref consumerTypeTypeList, ref openSqlConnection);
            inContentCollection.fBaseDomainData = baseDomainData;
            ConsumerBaseType itemConsumerData;
            foreach (var item in inContentCollection.fMyContent)
            {
                //  should go through entire collection and load the related supplier data
                if (item.CTMainSupplierID != "")
                {
                    itemConsumerData = ConsumerBaseType.GetConsumerSupplierData("", item.CTMainSupplierID,
                        ref relatedSupplierConsumerData, ref consumerTypeTypeList, ref openSqlConnection);
                    item.fRelatedSupplierInfo = itemConsumerData;
                }
            }

            // Assigns to all ContentPieces in the ContentCollection the right supplier object
            // ConsumerBaseType.AssignRightConsumerDataToContentPiece
            inContentCollection.fRelatedConsumerSupplierData = relatedSupplierConsumerData;
        }
    }
}