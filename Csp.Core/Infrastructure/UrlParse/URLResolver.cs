﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class URLResolver
    {
        private const string cResultModifierOpen = "[";

        private const string cFilterParamOpen = "(";

        private const string cOperatorAnd = "&";

        private const string cOperatorOr = "|";

        private const string cOperatorAlso = "^";

        private const string cFilterSupplierId = "s";

        private List<ExpressionChain> fExpressionChains;

        public List<string> fExpressionPublications;

        private List<string> fExpressionResultModifiers;

        private string FieldValuesConcat = "";

        public List<PageParams> fPageParamsList;

        public List<string> fRawURL;

        public List<SortParams> fSortParamsList;

        private List<string> fSpecificContentIDsRequested;

        public string FullInURL = "";

        private bool HasFilters = true;

        public string LanguageCode = "";

        public string LoggingCode = "";

        public string PageID = "";

        public URLResolver(string InExpression)
        {
            //  will break out InExpression into tokens and turn them into a set of chains / links / expressions
            //  break in expression into tokens, break also's into multiple lines with same publication id
            FullInURL = InExpression;
            try
            {
                var Exp = "";
                var pub = "";
                var FilterExpression = "";
                var ResultModifier = "";
                ExpressionChain MainChain;
                fExpressionChains = new List<ExpressionChain>();
                fSortParamsList = new List<SortParams>();
                fExpressionPublications = new List<string>();
                fExpressionResultModifiers = new List<string>();
                fPageParamsList = new List<PageParams>();
                fRawURL = new List<string>();
                fSpecificContentIDsRequested = new List<string>();
                if (InExpression.IndexOf(":") != -1)
                {
                    while (InExpression.IndexOf(":") != -1)
                    {
                        Exp = InExpression.Substring(0, InExpression.IndexOf(":"));
                        pub = GetPublicationExpression(Exp);
                        FilterExpression = GetFilterExpression(Exp);
                        ResultModifier = GetResultModifier(Exp);
                        MainChain = GetMainExpression(FilterExpression);
                        fRawURL.Add(Exp);
                        fExpressionChains.Add(MainChain);
                        fExpressionPublications.Add(pub);
                        fExpressionResultModifiers.Add(ResultModifier);
                        fSortParamsList.Add(new SortParams(ResultModifier));
                        fPageParamsList.Add(new PageParams(ResultModifier));
                        InExpression = InExpression.Substring(InExpression.IndexOf(":") + 1);
                    }
                }

                // grab last part of string
                Exp = InExpression;
                pub = GetPublicationExpression(Exp);
                FilterExpression = GetFilterExpression(Exp);
                ResultModifier = GetResultModifier(Exp);
                MainChain = GetMainExpression(FilterExpression);
                fRawURL.Add(Exp);
                fExpressionChains.Add(MainChain);
                fExpressionPublications.Add(pub);
                fExpressionResultModifiers.Add(ResultModifier);
                fSortParamsList.Add(new SortParams(ResultModifier));
                fPageParamsList.Add(new PageParams(ResultModifier));
            }
            catch (Exception ex)
            {
                throw new Exception("error parsing URL: " + InExpression);
            }
        }

        public string GetFieldValueConcatList()
        {
            //  for logging
            return FieldValuesConcat;
        }

        public string GetPageID()
        {
            //  for logging
            return PageID;
        }

        public List<Guid> GetListOfContentIDsRequested()
        {
            var OutGUIDs = new List<Guid>();
            foreach (var Item in fSpecificContentIDsRequested)
            {
                try
                {
                    var NewGuid = new Guid(Item);
                    OutGUIDs.Add(NewGuid);
                }
                catch (Exception ex)
                {
                    // do nothing
                }
            }

            return OutGUIDs;
        }

        private string GetFilterExpression(string InExpression)
        {
            var Result = InExpression;
            Result = Result.Substring(Result.IndexOf(cFilterParamOpen));
            if (Result.IndexOf(cResultModifierOpen) > 0)
            {
                Result = Result.Substring(0, Result.IndexOf(cResultModifierOpen));
            }

            return Result.Trim();
        }

        private string StripLoggingCodeOut(string InString)
        {
            var HoldString = InString;
            if (HoldString.Contains("rc("))
            {
                var HoldLeft = HoldString.Substring(0, HoldString.IndexOf("rc("));
                var HoldRight = HoldString.Substring(HoldString.IndexOf("rc("));
                var HoldRC = HoldRight.Substring(0, HoldRight.IndexOf(")"));
                HoldRight = HoldRight.Substring(HoldRight.IndexOf(")") + 1);
                HoldRC = HoldRC.Substring(3);
                // trim off rc(
                LoggingCode = HoldRC;
                HoldString = HoldLeft + HoldRight;
            }

            return HoldString;
        }

        private string GetResultModifier(string InExpression)
        {
            var OpenB = InExpression.IndexOf("[");
            var CloseB = InExpression.IndexOf("]");
            var ColonLocation = InExpression.IndexOf(":");
            if (OpenB != -1
                && CloseB == -1
                || CloseB != -1
                && OpenB == -1)
            {
                throw new Exception("Error! Result Modifier brackets [] should always be in pairs :" + InExpression);
            }

            if (ColonLocation != -1)
            {
                if (OpenB != -1
                    && OpenB < ColonLocation)
                {
                    var HoldResult = InExpression.Substring(OpenB + 1, CloseB
                                                                       - (OpenB - 1));
                    HoldResult = StripLoggingCodeOut(HoldResult);
                    return HoldResult;
                }
                else
                {
                    //  open bracket is after : so return nothing
                    return "no sorting specified";
                }
            }
            else if (OpenB != -1)
            {
                var HoldResult = InExpression.Substring(OpenB + 1, CloseB
                                                                   - (OpenB - 1));
                HoldResult = StripLoggingCodeOut(HoldResult);
                return HoldResult;
            }
            else
            {
                //  open bracket is after : so return nothing
                return "no sorting specified";
            }
        }

        private string GetPublicationExpression(string InExpression)
        {
            var s = InExpression;
            if (s.Contains(cFilterParamOpen))
            {
                s = s.Substring(0, InExpression.IndexOf(cFilterParamOpen));
            }
            else
            {
            }

            var result = s;
            if (result.StartsWith("p"))
            {
                PageID = result.Substring(1);
                //  for logging trim off the p
            }

            return result;
        }

        private static void GetLowestLinkOperator(ref string InExpression, ref int Lowest, ref string LowestStr)
        {
            var IndexOfAnd = InExpression.IndexOf(cOperatorAnd);
            var IndexOfOr = InExpression.IndexOf(cOperatorOr);
            var IndexOfAlso = InExpression.IndexOf(cOperatorAlso);
            var IndexOfCloseParen = InExpression.IndexOf(")");
            Lowest = 999999999;
            LowestStr = "";
            if (IndexOfAnd != -1)
            {
                if (IndexOfAnd < Lowest)
                {
                    Lowest = IndexOfAnd;
                    LowestStr = cOperatorAnd;
                }
            }

            if (IndexOfOr != -1)
            {
                if (IndexOfOr < Lowest)
                {
                    Lowest = IndexOfOr;
                    LowestStr = cOperatorOr;
                }
            }

            if (IndexOfAlso != -1)
            {
                if (IndexOfAlso < Lowest)
                {
                    Lowest = IndexOfAlso;
                    LowestStr = cOperatorAlso;
                }
            }

            if (IndexOfCloseParen != -1)
            {
                if (IndexOfCloseParen < Lowest)
                {
                    Lowest = IndexOfCloseParen;
                    LowestStr = ")";
                }
            }
        }

        private string GetFieldRemainder(string FieldExpression)
        {
            //  should take in fName%1=bla*boo*ka#
            //  should return fName%1=boo*ka#
            //  should take in fName%1=boo*ka#
            //  Should return fName%1=ka#
            //  should take in fName%1=ka#
            //  should return ""
            if (FieldExpression.IndexOf("*") == -1)
            {
                return "";
            }
            else
            {
                var LeftSide = FieldExpression.Substring(0, FieldExpression.IndexOf("%") + 3);
                var NewRightSide = FieldExpression.Substring(FieldExpression.IndexOf("*") + 1);
                return LeftSide + NewRightSide;
            }
        }

        private string TrimOffOrExpressions(string FieldExpression)
        {
            //  should get rid of all *'s in field expression return only f%Tvalue#
            if (FieldExpression.IndexOf("*") == -1)
            {
                return FieldExpression;
            }
            else
            {
                object LeftSide = FieldExpression.Substring(0, FieldExpression.IndexOf("*"));
                return LeftSide + "#";
            }
        }

        private string GetFieldOp(string FieldExpression)
        {
            //  should take fName%1=ka# and return "="
            return FieldExpression.Substring(FieldExpression.IndexOf("%") + 2, 1);
        }

        private Expression GetActualLeadingExpressionLeaveLeftovers(ref string InExpression)
        {
            //  should assume leading text is an Expression, strip it off and leave the trailing operator
            // Turns: sHP&ctPrinters&cLaserprinters|(sSuperHP&ctPrinters))
            //  Into a new Expression Object: Field: SupplierID Operator: "=" Value:"HP" and
            //  should hack off the leading part of inner expression so the caller can build a link
            //  InnerExpression = &ctPrinters&cLaserprinters|(sSuperHP&ctPrinters))
            var Lowest = 0;
            var LowestStr = "";
            Expression OutExp;
            GetLowestLinkOperator(ref InExpression, ref Lowest, ref LowestStr);
            var NextLowest = 9999999;
            var NextLowestStr = "";
            var NextExpression = "";
            if (LowestStr != ")")
            {
                NextExpression = InExpression.Substring(Lowest);
                GetLowestLinkOperator(ref NextExpression, ref NextLowest, ref NextLowestStr);
            }
            else
            {
                NextExpression = InExpression.Substring(Lowest);
                //  no more operators! trim off )))))??????
                GetLowestLinkOperator(ref NextExpression, ref NextLowest, ref NextLowestStr);
            }

            string FieldValue;
            if (InExpression.IndexOf("t") == 0)
            {
                FieldValue = InExpression.Substring(1);
                //  skip ct
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression("t", "=", FieldValue);
            }
            else if (InExpression.IndexOf("ct") == 0)
            {
                FieldValue = InExpression.Substring(2);
                //  skip ct
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression("ct", "=", FieldValue);
            }
            else if (InExpression.IndexOf("cn") == 0)
            {
                FieldValue = InExpression.Substring(2);
                //  skip ct
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression("cn", "=", FieldValue);
                fSpecificContentIDsRequested.Add(FieldValue);
                // to log all content id's requested
            }
            else if (InExpression.IndexOf("cfx") == 0)
            {
                FieldValue = InExpression.Substring(3);
                //  skip ct
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression("cfx", "=", FieldValue);
            }
            else if (InExpression.IndexOf("c") == 0)
            {
                FieldValue = InExpression.Substring(1);
                //  skip c
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression("c", "=", FieldValue);
            }
            else if (InExpression.IndexOf(cFilterSupplierId) == 0)
            {
                FieldValue = InExpression.Substring(1);
                //  skip s
                FieldValue = FieldValue.Substring(0, FieldValue.IndexOf(NextLowestStr));
                OutExp = new Expression(cFilterSupplierId, "=", FieldValue);
            }
            else if (InExpression.IndexOf("f") == 0)
            {
                //  validate *'s and #'s
                var FieldExpression = InExpression.Substring(0, InExpression.IndexOf("#") + 1);
                object RemainderToAppendToNextExpression = GetFieldRemainder(FieldExpression);
                // should be prepended to Next Expression might be ""
                if (RemainderToAppendToNextExpression != "")
                {
                    NextExpression = "|"
                                     + (RemainderToAppendToNextExpression + NextExpression);
                }

                FieldExpression = TrimOffOrExpressions(FieldExpression);
                //  should get rid of all *'s in field expression return only f%Tvalue#
                var FieldOp = GetFieldOp(FieldExpression);
                FieldValue = FieldExpression.Substring(FieldExpression.IndexOf(FieldOp) + 1);
                // FieldValue = FieldValue.Substring(0, FieldValue.IndexOf("#"))
                FieldValue =
                    HttpUtility.UrlDecode(FieldValue.Substring(0, FieldValue.IndexOf("#")), Encoding.UTF8);
                FieldExpression = FieldExpression.Substring(0, FieldExpression.IndexOf("%") + 2);
                OutExp = new Expression(FieldExpression, FieldOp, FieldValue);
                FieldValuesConcat = FieldValuesConcat + "-*-" + FieldExpression + " " + FieldOp + " " + FieldValue;
            }
            else
            {
                throw new Exception("Expression Field Type not valid " + InExpression);
            }

            InExpression = NextExpression;
            return OutExp;
        }

        private ExpressionChain GetExpressionChainLeaveLeftovers(ref string InExpression)
        {
            //  should only be run if it's known that inExpression contains ()\
            //  will strip () and build an Expression chain Object Containing all ELO's and After ELO's, may need to create inner EC's
            //  should take (sHP&ctPrinters&cLaserprinters|(sSuperHP&ctPrinters)) and turn it into a
            //  new EC with it's Prime ELO linking 
            InExpression = InExpression.Substring(1);
            var HoldPrime = GetExpressionLinkOrChainLeaveLeftovers(ref InExpression);
            //  In Expression should now be a )
            if (!InExpression.StartsWith(")"))
            {
                throw new Exception("Expression missing closing )!");
            }

            InExpression = InExpression.Substring(1);
            //  trim off )
            var Lowest = 0;
            var LowestStr = "";
            GetLowestLinkOperator(ref InExpression, ref Lowest, ref LowestStr);
            if (Lowest != 0
                && !(InExpression == ""))
            {
                throw new Exception("After close ) should be another link operator &|^ or another close )");
            }

            ExpressionRoot HoldAfter = null;
            if (LowestStr == cOperatorAnd || LowestStr == cOperatorOr || LowestStr == cOperatorAlso)
            {
                InExpression = InExpression.Substring(1);
                //  trim off operator
                HoldAfter = GetExpressionLinkOrChainLeaveLeftovers(ref InExpression);
                //  get after
            }
            else if (LowestStr == ")"
                     || InExpression == "")
            {
                LowestStr = "none";
            }

            var TheExpressionChain = new ExpressionChain(ref HoldPrime, ref HoldAfter, LowestStr);
            return TheExpressionChain;
            // should return new chain with HoldExpRoot as Prime
            //  needs to handle trailing )
        }

        private ExpressionRoot GetExpressionLinkOrChainLeaveLeftovers(ref string InExpression)
        {
            //  should be able to take Filter Expression and return the right linked type
            if (InExpression.StartsWith(cFilterParamOpen))
            {
                //  is Chain
                var HoldChain = GetExpressionChainLeaveLeftovers(ref InExpression);
                // (sHP&ctPrinters&cLaserprinters|(sSuperHP&ctPrinters))
                return HoldChain;
            }
            else if (InExpression.StartsWith(")"))
            {
                return null;
            }
            else
            {
                //  is Normal Expression
                // get expression link
                // ctPrinters&cLaserprinters|(sSuperHP&ctPrinters))
                var ExpressionHold = GetActualLeadingExpressionLeaveLeftovers(ref InExpression);
                //  should get back something like this &ctPrinters&cLaserprinters|(sSuperHP&ctPrinters))
                var LowestLO = 0;
                var LinkOperator = "";
                GetLowestLinkOperator(ref InExpression, ref LowestLO, ref LinkOperator);
                var vLeftSide = (ExpressionRoot) ExpressionHold;
                ExpressionLinkOperator ELO;
                if (LinkOperator != ")")
                {
                    InExpression = InExpression.Substring(LowestLO + 1);
                    var expressionLinkOrChainLeaveLeftovers =
                        GetExpressionLinkOrChainLeaveLeftovers(ref InExpression);
                    ELO = new ExpressionLinkOperator(ref vLeftSide,
                        ref expressionLinkOrChainLeaveLeftovers, LinkOperator);
                }
                else
                {
                    ExpressionRoot vRightSide = null;
                    ELO = new ExpressionLinkOperator(ref vLeftSide, ref vRightSide, "none");
                }

                return ELO;
            }
        }

        private ExpressionChain GetMainExpression(string InExpression)
        {
            var MainChain = GetExpressionLinkOrChainLeaveLeftovers(ref InExpression);
            return (ExpressionChain) MainChain;
        }

        public int ChainCount()
        {
            return fExpressionChains.Count;
        }

        private string StripFieldSymbols(string InString)
        {
            var Hold = InString;
            Hold = InString.Replace("$$$", "");
            Hold = Hold.Replace("@@@", "");
            return Hold;
        }

        private string MoveLinkOperatorToFront(string InString)
        {
            //  this funtion is used incase the field filter is specified first; so it's operator union with the other filters is at the end; this function move it to the front because the field filter is specified second in the giant SQL statement. 
            var Hold = InString.Trim();
            if (Hold.EndsWith("AND"))
            {
                Hold = Hold.Substring(0, Hold.LastIndexOf("AND"));
                return "AND " + Hold;
            }
            else if (InString.EndsWith("OR"))
            {
                Hold = Hold.Substring(0, Hold.LastIndexOf("OR"));
                return "OR " + Hold;
            }
            else if (InString.EndsWith("^^^OR^&^"))
            {
                Hold = Hold.Substring(0, Hold.LastIndexOf("^^^OR^&^"));
                return "^^^OR^&^ " + Hold;
            }
            else if (InString.EndsWith("^^^AND^&^"))
            {
                Hold = Hold.Substring(0, Hold.LastIndexOf("^^^AND^&^"));
                return "^^^AND^&^ " + Hold;
            }
            else
            {
                return Hold;
            }
        }

        public List<SqlParameter> ConvertValuesIntoParametersArray(ref string FieldFilter, ref string OtherFilter)
        {
            object Params = new List<SqlParameter>();
            var Hold = FieldFilter;
            string HoldLeft;
            string HoldRight;
            string HoldValue;
            var ParamNum = 1;
            var ParamName = "";
            var ParamList = new List<SqlParameter>();
            while (Hold.Contains("#$#"))
            {
                HoldLeft = Hold.Substring(0, Hold.IndexOf("#$#"));
                HoldRight = Hold.Substring(Hold.IndexOf("%$%") + 3);
                HoldValue = Hold.Substring(Hold.IndexOf("#$#") + 3);
                HoldValue = HoldValue.Substring(0, HoldValue.IndexOf("%$%"));
                ParamName = "@Param" + ParamNum.ToString();
                ParamList.Add(new SqlParameter(ParamName, HoldValue));
                Hold = HoldLeft + ParamName + HoldRight;
                ParamNum = ParamNum + 1;
            }

            FieldFilter = Hold;
            Hold = OtherFilter;
            while (Hold.Contains("#$#"))
            {
                HoldLeft = Hold.Substring(0, Hold.IndexOf("#$#"));
                HoldRight = Hold.Substring(Hold.IndexOf("%$%") + 3);
                HoldValue = Hold.Substring(Hold.IndexOf("#$#") + 3);
                HoldValue = HoldValue.Substring(0, HoldValue.IndexOf("%$%"));
                ParamName = "@Param" + ParamNum.ToString();
                ParamList.Add(new SqlParameter(ParamName, HoldValue));
                Hold = HoldLeft + ParamName + HoldRight;
                ParamNum = ParamNum + 1;
            }

            OtherFilter = Hold;
            return ParamList;
        }

        public string GetFieldFilterFromMainChain(string InString)
        {
            string PreFieldExpression;
            string PostFieldExpression;
            string FieldExpression;
            //  get everything from the first $$$ to the last @@@ and return that
            // Will have leading or trailing and / or / nothing on either end should move to leading
            if (InString.IndexOf("$$$") != -1)
            {
                PreFieldExpression = InString.Substring(0, InString.IndexOf("$$$"));
                PostFieldExpression = InString.Substring(InString.LastIndexOf("@@@") + 3);
                FieldExpression = InString.Substring(InString.IndexOf("$$$"),
                    InString.LastIndexOf("@@@") + (3 - InString.IndexOf("$$$")));
                return MoveLinkOperatorToFront(StripFieldSymbols(FieldExpression));
            }
            else
            {
                return "";
            }
        }

        public string GetOtherThanFieldFilterFromMainChain(string InString)
        {
            string PreFieldExpression;
            string PostFieldExpression;
            //  remove everything from the first $$$ to the last @@@ and return that
            if (InString.IndexOf("$$$") != -1)
            {
                PreFieldExpression = InString.Substring(0, InString.IndexOf("$$$"));
                PostFieldExpression = InString.Substring(InString.LastIndexOf("@@@") + 3);
                //  FieldExpression = InString.Substring(InString.IndexOf("$$$"), InString.LastIndexOf("@@@") + 3)
                // Return StripFieldSymbols(FieldExpression)
                return PreFieldExpression + PostFieldExpression;
            }
            else
            {
                return InString;
            }

            return "";
        }

        public string GetMainChainText(int Index)
        {
            ExpressionChain MainChain;
            var S = "";
            MainChain = fExpressionChains[Index];
            S = MainChain.GenenerateSQLFromExpression();
            // S = GetFieldFilterFromMainChain(S)
            // Next
            return S;
        }

        public string GetMainChainPub(int Index)
        {
            var S = "";
            S = fExpressionPublications[Index];
            return S;
        }

        public string GetMainChainResultModifier(int Index)
        {
            var S = "";
            S = fExpressionResultModifiers[Index];
            return S;
        }

        private string TrimOffLeadingAndTrailingParen(string InString)
        {
            if (InString.StartsWith("("))
            {
                InString = InString.Substring(1);
            }

            if (InString.EndsWith(")"))
            {
                InString = InString.Substring(0, InString.Length - 1);
            }

            return InString;
        }

        private string GetFieldFiltersFromDecoradedFieldFilter(string InFieldFilter)
        {
            //  assumes any precursor has been removed
            //  will read the following string and return multiple exists statements connecting the field filters
            //  this is an example string:
            //  (UPPER(CONTENT_TYPES_FIELDS_1.FIELDNAME) = UPPER(@Param1) AND  content_fields_1.value_text like @Param2) ^^^OR^&^  (UPPER(CONTENT_TYPES_FIELDS_1.FIELDNAME) = UPPER(@Param3) AND  content_fields_1.value_text like @Param4) ^^^AND^&^ another filter
            var s1 = InFieldFilter;
            // Dim SQLStatement1 As String = " EXISTS (SELECT content_1.content_Id, content_1.content_main_Id, content_1.content_types_languages_Id, "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + "content_1.stage_Id, content_1.active, content_fields_1.content_Id AS Expr1, content_fields_1.value_text, "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + " content_fields_1.content_types_fields_Id, content_types_fields_1.content_types_fields_Id AS Expr2, "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + " content_types_fields_1.fieldname FROM [content] AS content_1 with (nolock) INNER JOIN content_fields AS "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + " content_fields_1 with (nolock) ON content_1.content_Id = content_fields_1.content_Id INNER JOIN "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + " content_types_fields AS content_types_fields_1 with (nolock) ON content_fields_1.content_types_fields_Id = "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + "content_types_fields_1.content_types_fields_Id WHERE (content_main.content_main_Id = content_1.content_main_Id) "
            // SQLStatement1 = SQLStatement1 + vbCrLf
            // SQLStatement1 = SQLStatement1 + " AND ([content].content_Id = content_1.content_Id) And (" '+ FieldFilter + "))"
            // Dim SQLStatement1 As String = "  t_1.content_id in (Select t_2.content_id from "
            // SQLStatement1 = SQLStatement1 + " (SELECT co_2.content_Id, co_2.content_main_Id, co_2.content_types_languages_Id, co_2.stage_Id, co_2.active, c_f_2.content_Id AS Expr1, c_f_2.value_text, "
            // SQLStatement1 = SQLStatement1 + " c_f_2.content_types_fields_Id, c_t_f_2.content_types_fields_Id AS Expr2, c_t_f_2.fieldname "
            // SQLStatement1 = SQLStatement1 + " FROM [content] AS co_2 with (nolock) "
            // SQLStatement1 = SQLStatement1 + " INNER JOIN content_fields AS c_f_2 with (nolock) ON co_2.content_Id = c_f_2.content_Id "
            // SQLStatement1 = SQLStatement1 + " INNER JOIN content_types_fields AS c_t_f_2 with (nolock) ON c_f_2.content_types_fields_Id = c_t_f_2.content_types_fields_Id "
            // SQLStatement1 = SQLStatement1 + " INNER JOIN content_main as c_m_2 on (c_m_2.content_main_Id = co_2.content_main_Id) "
            // SQLStatement1 = SQLStatement1 + " where ( "
            var SQLStatement1 = "";
            SQLStatement1 = SQLStatement1 + "t_1.content_id in (";
            SQLStatement1 = SQLStatement1 + " select SearchableAttributes.ContentId as content_Id ";
            SQLStatement1 = SQLStatement1 + " from SearchableAttributes where ";
            SQLStatement1 = SQLStatement1 + "(";
            var HoldCurrentFilter1 = "";
            var HoldCurOperator1 = "";
            var HoldResult1 = "";
            while (s1.IndexOf("^^^") != -1)
            {
                HoldCurrentFilter1 = s1.Substring(0, s1.IndexOf("^^^"));
                // hold current filter = to the actual expression
                HoldResult1 = HoldResult1 + " " + HoldCurOperator1 + " " + SQLStatement1 + HoldCurrentFilter1 + ")) )";
                s1 = s1.Substring(s1.IndexOf("^^^") + 3);
                //  trim off leading Filter
                HoldCurOperator1 = s1.Substring(0, s1.IndexOf("^&^"));
                s1 = s1.Substring(s1.IndexOf("^&^") + 3);
            }

            HoldResult1 = HoldResult1 + " " + HoldCurOperator1 + " " + SQLStatement1 + s1 + "))  ";
            return HoldResult1;
        }

        public string GenerateCommandText(string FieldFilter, string OtherFilter, string LanguageCode,
            bool languageFlag, SqlConnection connection, SqlParameterCollection collection,
            ref CategorySearchListAndCleanOtherFilterHelper CategorySearchListAndCleanOtherFilterHelpers)
        {
            //  Build this string from the Filed and other filters
            // Dim S As String = "SELECT content_main.content_main_Id, content_main.content_main_key, content_main.content_types_Id, content_main.content_identifier, content_main.supplier_Id, content_main.consumer_Id, content_main.publication_scheme_Id, [content].content_types_languages_Id, [content].content_main_Id AS Expr1, [content].content_Id, content_fields.content_Id AS Expr2, content_fields.value_boolean, content_fields.value_double, content_fields.value_date, content_fields.value_text, content_fields.value_integer, content_fields.content_types_fields_Id, content_types_fields.content_types_fields_Id AS Expr3, content_types_fields.fieldtype, content_types_fields.fieldname "
            // S = S + ", content_main.content_staging_scheme_Id, [content].stage_Id"
            // S = S + " FROM content_main with (nolock) INNER JOIN [content] with (nolock) ON content_main.content_main_Id = [content].content_main_Id INNER JOIN content_fields with (nolock) ON [content].content_Id = content_fields.content_Id INNER JOIN content_types_fields ON content_fields.content_types_fields_Id = content_types_fields.content_types_fields_Id WHERE EXISTS"
            // S = S + "(SELECT companies_consumers.companies_Id, companies_consumers.base_domain, companies_consumers.default_language_Id, consumers_themes.consumer_Id AS ThemeCID, consumers_themes.themes_Id, consumers_themes.category_Id,content_categories.category_Id AS CCCatID, content_categories.content_main_Id AS ValidCTMains, content_categories.publication_scheme_Id, publication_schemes.publication_schemes_Id, publication_schemes.description, publication_schemes.active, publication_schemes_rules.publication_schemes_rules_Id, publication_schemes_rules.publication_schemes_Id AS Expr1, publication_schemes_rules.from_date, publication_schemes_rules.to_date, publication_schemes_rules.active AS Expr2"
            // S = S + ", staging.staging_Id, staging.active AS Expr4, staging_stages.staging_stages_Id, staging_stages.staging_Id AS Expr5, staging_stages.action, categories.categoryId, categories.active AS Expr3, categories.ExternalCategoryCode"
            // S = S + " FROM  publication_schemes_rules INNER JOIN publication_schemes ON publication_schemes_rules.publication_schemes_Id = publication_schemes.publication_schemes_Id INNER JOIN companies_consumers INNER JOIN consumers_themes ON companies_consumers.companies_Id = consumers_themes.consumer_Id INNER JOIN content_categories with (nolock) ON consumers_themes.category_Id = content_categories.category_Id ON publication_schemes.publication_schemes_Id = content_categories.publication_scheme_Id"
            // S = S + " INNER JOIN categories with (nolock) ON categories.categoryId = content_categories.category_Id "
            // S = S + " INNER JOIN staging_stages INNER JOIN staging ON staging_stages.staging_Id = staging.staging_Id ON content_main.content_staging_scheme_Id = staging.staging_Id AND [content].stage_Id = staging_stages.staging_stages_Id "
            // S = S + " WHERE (categories.active = 1) AND (companies_consumers.base_domain = @BaseDomain)  AND (consumers_themes.supplier_Id = content_main.supplier_Id) AND (content_main.content_main_Id = content_categories.content_main_Id) AND (publication_schemes_rules.from_date < GETDATE()) AND (publication_schemes_rules.to_date > GETDATE()) "
            // S = S + " and (publication_schemes_rules.active = 1) AND (publication_schemes.active = 1) "
            // S = S + " AND (staging.active = 1) AND (staging_stages.action  = 3)  "
            var S = "";
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                S = S + CategorySearchListAndCleanOtherFilterHelpers.GetSQLToLoadCatsTmpTable() + " ";
            }

            S += "";
            S = S +
                @" SELECT content_main.content_main_Id, content_main.content_main_key, content_main.content_types_Id, content_main.content_identifier, content_main.supplier_Id, content_main.consumer_Id, content_main.publication_scheme_Id, [content].content_types_languages_Id, [content].content_main_Id AS Expr1, [content].content_Id, content_fields.content_Id AS Expr2, content_fields.value_boolean, content_fields.value_double, content_fields.value_date, content_fields.value_text, content_fields.value_integer, content_fields.content_types_fields_Id, content_types_fields.content_types_fields_Id AS Expr3, content_types_fields.fieldtype, content_types_fields.fieldname ";
            S = S +
                ", content_main.content_staging_scheme_Id, [content].stage_Id, languages.languagecode, languages.descr" +
                "iption ";
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                S = S + ", content_categories.category_Id, Cats.CategoryName,Cats.Depth,Cats.ParentID, Cats.Lineage ";
            }

            // S = S + " FROM content_main with (nolock) INNER JOIN [content] with (nolock) ON content_main.content_main_Id = [content].content_main_Id INNER JOIN content_fields with (nolock) ON [content].content_Id = content_fields.content_Id INNER JOIN content_types_fields ON content_fields.content_types_fields_Id = content_types_fields.content_types_fields_Id "
            S = S + "FROM content_main with (nolock) ";
            S = S + "INNER JOIN [content] with (nolock) ON content_main.content_main_Id = [content].content_main_Id ";
            S = S + "INNER JOIN content_fields with (nolock) ON [content].content_Id = content_fields.content_Id ";
            S = S +
                "INNER JOIN content_types_fields with (nolock) ON content_fields.content_types_fields_Id = content_typ" +
                "es_fields.content_types_fields_Id  ";
            S = S +
                "INNER JOIN languages with (nolock) on languages.languages_Id = [content].content_types_languages_Id";
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                S = S +
                    " inner join content_categories with (nolock) on content_categories.content_main_Id = content_main.con" +
                    "tent_main_Id ";
                S = S + " inner join @Cats as Cats on Cats.CategoryID = content_categories.category_Id ";
            }

            S = S + " WHERE ";
            S = S + "  content.content_Id  in ";
            S = S + GetContentIdsFromContentTypesAndSuppliers(FieldFilter, OtherFilter, collection, connection,
                    ref CategorySearchListAndCleanOtherFilterHelpers);
            //  break off this query to increase the speed
            // S = S + "  (Select t_1.content_id from "
            // S = S + "(SELECT c_m_1.content_main_Id, c_m_1.content_main_key, c_m_1.content_types_Id, c_m_1.content_identifier, c_m_1.supplier_Id, c_m_1.consumer_Id, c_m_1.content_staging_scheme_Id,"
            // S = S + vbCrLf
            // S = S + " co_1.content_types_languages_Id, co_1.content_Id, co_1.stage_Id, c_co_1.companies_Id, c_co_1.base_domain, c_co_1.default_language_Id,                            c_t_1.consumer_Id AS ThemeCID, c_t_1.themes_Id, c_t_1.category_Id, "
            // S = S + vbCrLf
            // S = S + " c_ca_1.category_Id AS CCCatID, c_ca_1.content_main_Id AS ValidCTMains, c_ca_1.publication_scheme_Id, "
            // S = S + vbCrLf
            // S = S + " p_s_1 .publication_schemes_Id, p_s_1 .description, p_s_1.active, p_s_r_1.publication_schemes_rules_Id, p_s_r_1.publication_schemes_Id AS Expr1, "
            // S = S + vbCrLf
            // S = S + " p_s_r_1.from_date, p_s_r_1.to_date, p_s_r_1.active AS Expr2, s_1.staging_Id, "
            // S = S + vbCrLf
            // S = S + " s_1.active AS Expr4, s_s_1.staging_stages_Id, s_s_1.staging_Id AS Expr5, s_s_1.action, ca_1.categoryId, ca_1.active AS Expr3, ca_1.ExternalCategoryCode"
            // S = S + vbCrLf
            // S = S + " From "
            // S = S + vbCrLf
            // S = S + " content_main c_m_1 WITH (nolock) INNER JOIN "
            // S = S + vbCrLf
            // S = S + " consumers_themes c_t_1 ON c_t_1.supplier_Id = c_m_1.supplier_id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " companies_consumers c_co_1 on c_co_1.companies_Id = c_t_1.consumer_Id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " [content] co_1 WITH (nolock) ON c_m_1.content_main_Id = [co_1].content_main_Id "
            // S = S + vbCrLf
            // S = S + " INNER JOIN "
            // S = S + vbCrLf
            // S = S + " content_categories c_ca_1 WITH (nolock) ON c_t_1.category_Id = c_ca_1.category_Id AND c_ca_1.content_main_Id=c_m_1.content_main_id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " categories ca_1 WITH (nolock) ON ca_1.categoryId = c_ca_1.category_Id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " publication_schemes p_s_1 ON p_s_1.publication_schemes_Id = c_ca_1.publication_scheme_Id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " publication_schemes_rules p_s_r_1 ON p_s_r_1.publication_schemes_Id = p_s_1.publication_schemes_Id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " staging_stages s_s_1 ON [co_1].stage_Id = s_s_1.staging_stages_Id INNER JOIN "
            // S = S + vbCrLf
            // S = S + " staging s_1 ON s_s_1.staging_Id = s_1.staging_Id AND c_m_1.content_staging_scheme_Id = s_1.staging_Id "
            // S = S + vbCrLf
            // S = S + " WHERE      (ca_1.active = 1) AND (c_co_1.base_domain = @BaseDomain) "
            // S = S + vbCrLf
            // S = S + " AND (p_s_r_1.from_date < GETDATE()) AND "
            // S = S + vbCrLf
            // S = S + " (p_s_r_1.to_date > GETDATE()) AND (p_s_r_1.active = 1) AND (p_s_1.active = 1) AND "
            // S = S + vbCrLf
            // S = S + " (s_1.active = 1) AND (s_s_1.action = 3) "
            // If LanguageCode <> "" Then
            //     S = S + " and (co_1.content_types_languages_Id = " + LanguageCode + ")" 'companies_consumers.default_language_Id) "
            // ElseIf (languageFlag) Then
            //     S = S + " and (co_1.content_types_languages_Id = c_co_1.default_language_Id) "
            // ElseIf (Not languageFlag) Then
            //     S = S + " and (co_1.content_types_languages_Id = c_co_1.fallback_language_Id) "
            // End If
            // S = S + vbCrLf
            // If (OtherFilter <> "" And OtherFilter <> "()") Then
            //     S = S + " And (" + OtherFilter + ")) " 'to close off the appended other filter and box it into an and statement so you don't get content you shoudln't
            // Else
            //     S = S + ")"
            // End If
            // S = S + vbCrLf
            // 'append the close to the where in
            // S = S + "t_1" 't1 now needs a close ) further down
            // S = S + vbCrLf
            // If (FieldFilter = "" Or FieldFilter = "()") And OtherFilter <> "" And OtherFilter <> "()" Then
            //     Return S + " ORDER BY content_main.content_main_Id" ' skip field filter, other filter a-ok
            //     ' close ) added to close off t_1
            // End If
            // If (FieldFilter = "" And OtherFilter = "") Then
            //     ' blank url filter; show all content
            //     Return S + " ORDER BY content_main.content_main_Id"
            //     ' close ) added to close off t_1
            // End If
            // 'More filtering based on theme / category / Domain here!) 
            // 'AND / or Exists field section
            // Dim Precursor As String = ""
            // If FieldFilter.StartsWith("^^^AND^&^") Then
            //     FieldFilter = FieldFilter.Substring(9) 'trim off and
            //     Precursor = "AND"
            // ElseIf FieldFilter.StartsWith("^^^OR^&^") Then
            //     FieldFilter = FieldFilter.Substring(8)
            //     Precursor = "OR"
            // End If
            // If Precursor = "" And (OtherFilter = "" Or OtherFilter = "()") And (FieldFilter <> "" Or FieldFilter <> "()") Then
            //     Precursor = "AND" ' this is because BaseDomain is always in the prior select so it must be an and if it's nothing aka should be an and always if the OtherFilter is blank
            //     ' this may need to be changed to the where in statement
            // End If
            // If Precursor <> "" Then
            //     ' write field exists / otherwise you can skip it. 
            //     'Pre cursor removed due to performance, and it's not used by the team must be hard coded to an and S = S + Precursor + "("
            //     'S = S + " where " ' t_1.content_id in moved into field filter call
            //     'S = S + GetContentIdsFromFieldFilter(" content_id in ", FieldFilter, connection, collection)
            //     S = S + " AND " + GetContentIdsFromFieldFilter(" content_fields.content_Id  in  ", FieldFilter, connection, collection)
            // ElseIf Precursor = "" And FieldFilter <> "" And FieldFilter <> "()" Then
            //     Throw New Exception("Field Filter error, missing precursor: " + FieldFilter)
            // Else
            //     'S = S + ")"
            //     S = S + ""
            // End If
            // 'Return S + ") ORDER BY content_main.content_main_Id"
            // Return S + " ORDER BY content_main.content_main_Id" 'ltu 225
            // If Precursor is an or, will need to add domain level filtering on the content in the field based exists statement potentially 
            S = S + " ORDER BY content_main.content_main_Id, content.content_Id ";
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                S = S + " , category_Id";
            }

            return S;
            // ltu 225
        }

        private void SetFilterParameters(ref SqlParameterCollection ParameterCollection, SqlConnection sqlConnection,
            int PageID = -1)
        {
            //  Param collection will have all the filtering parameters added and values set
            // Warning!!! Optional parameters not supported
            //        Should already have @BaseDomain parameter
            //  sql connection should be open and active
            //  pageId is the page to get filtering params for
            if (PageID == -1)
            {
                ParameterCollection.AddWithValue("@SkipCategoryActive", 0);
                ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesDate", 0);
                ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesActive", 0);
                ParameterCollection.AddWithValue("@SkipPublicationSchemeActive", 0);
                ParameterCollection.AddWithValue("@SkipStagingActive", 0);
                ParameterCollection.AddWithValue("@SkipStagingStagePublish", 0);
                ParameterCollection.AddWithValue("@SkipContentAccessFilter", 0);
                ParameterCollection.AddWithValue("@SkipSkuFilter", 0);
                return;
            }

            var any = 0;
            if (HasFilters)
            {
                var AnyFilters = "SELECT COUNT (1) FROM Custom_Consumer_Filter_Per_Page";
                var holdcmd = new SqlCommand(AnyFilters, sqlConnection);
                any = int.Parse(holdcmd.ExecuteScalar().ToString());
            }

            if (any == 0)
            {
                HasFilters = false;
            }

            var query =
                @"SELECT top 1 Custom_Consumer_Filter_Per_Page.CompanyID, Custom_Consumer_Filter_Per_Page.PageID, Custom_Consumer_Filter_Per_Page.SkipCategoryActive, Custom_Consumer_Filter_Per_Page.SkipPublicationRulesDate, Custom_Consumer_Filter_Per_Page.SkipPublicationRulesActive, Custom_Consumer_Filter_Per_Page.SkipPublicationSchemeActive, Custom_Consumer_Filter_Per_Page.SkipStagingActive, Custom_Consumer_Filter_Per_Page.SkipStagingStagePubish, Custom_Consumer_Filter_Per_Page.SkipContentAccessFilter, Custom_Consumer_Filter_Per_Page.SkipSkuFilter, companies_consumers.companies_consumers_Id, companies_consumers.companies_Id, companies_consumers.base_domain, companies_consumers.base_publication_Id, companies_consumers.base_publication_parameters, companies_consumers.active, companies_consumers.default_language_Id, companies_consumers.fallback_language_Id FROM Custom_Consumer_Filter_Per_Page left JOIN companies_consumers ON Custom_Consumer_Filter_Per_Page.CompanyID = companies_consumers.companies_Id ";
            query = query +
                    " where (base_domain = @BaseDomain or Custom_Consumer_Filter_Per_Page.CompanyID = 0) and (PageID = @Pa" +
                    "geID or Custom_Consumer_Filter_Per_Page.PageID = 0)";
            query = query + " order by Custom_Consumer_Filter_Per_Page.CompanyID desc, PageID desc ";
            var sqlCommand = new SqlCommand(query, sqlConnection);
            foreach (SqlParameter param in ParameterCollection)
            {
                sqlCommand.Parameters.AddWithValue(param.ParameterName, param.Value);
            }

            sqlCommand.Parameters.AddWithValue("@PageID", PageID);
            var FoundFilter = false;
            SqlDataReader sqlReader = null;
            try
            {
                if (any > 0)
                {
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        // If (String.IsNullOrEmpty(result)) Then
                        // result = result + String.Format("'{0}'", sqlReader("content_Id").ToString())
                        // Else
                        // result = result + String.Format(",'{0}'", sqlReader("content_Id").ToString())
                        // End If
                        ParameterCollection.AddWithValue("@SkipCategoryActive",
                            int.Parse(sqlReader["SkipCategoryActive"].ToString()));
                        ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesDate",
                            int.Parse(sqlReader["SkipPublicationRulesDate"].ToString()));
                        ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesActive",
                            int.Parse(sqlReader["SkipPublicationRulesActive"].ToString()));
                        ParameterCollection.AddWithValue("@SkipPublicationSchemeActive",
                            int.Parse(sqlReader["SkipPublicationSchemeActive"].ToString()));
                        ParameterCollection.AddWithValue("@SkipStagingActive",
                            int.Parse(sqlReader["SkipStagingActive"].ToString()));
                        ParameterCollection.AddWithValue("@SkipStagingStagePublish",
                            int.Parse(sqlReader["SkipStagingStagePubish"].ToString()));
                        ParameterCollection.AddWithValue("@SkipContentAccessFilter",
                            int.Parse(sqlReader["SkipContentAccessFilter"].ToString()));
                        ParameterCollection.AddWithValue("@SkipSkuFilter",
                            int.Parse(sqlReader["SkipSkuFilter"].ToString()));
                        FoundFilter = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Throw New Exception(String.Format("GetContentIdsFromContentTypesAndSuppliers: [{0}] Command [{1}]", ex.Message, query))
            }
            finally
            {
                if (!(sqlReader == null))
                {
                    sqlReader.Close();
                }

                sqlCommand.Dispose();
                if (FoundFilter == false)
                {
                    //  turn off all filters
                    ParameterCollection.AddWithValue("@SkipCategoryActive", 0);
                    ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesDate", 0);
                    ParameterCollection.AddWithValue("@SkipPublicationSchemeRulesActive", 0);
                    ParameterCollection.AddWithValue("@SkipPublicationSchemeActive", 0);
                    ParameterCollection.AddWithValue("@SkipStagingActive", 0);
                    ParameterCollection.AddWithValue("@SkipStagingStagePublish", 0);
                    ParameterCollection.AddWithValue("@SkipContentAccessFilter", 0);
                    ParameterCollection.AddWithValue("@SkipSkuFilter", 0);
                }
            }
        }

        private string GetContentIdsFromContentTypesAndSuppliers(string fieldFilter, string otherFilter,
            SqlParameterCollection parameterCollection, SqlConnection sqlConnection,
            ref CategorySearchListAndCleanOtherFilterHelper CategorySearchListAndCleanOtherFilterHelpers)
        {
            var expression = String.Empty;
            var result = String.Empty;
            var query = String.Empty;
            var CatQuery = CategorySearchListAndCleanOtherFilterHelpers.GetSQLToLoadCatsTmpTable();
            //         query = "SELECT TOP(1000) co_1.content_Id " & _
            //         " From   content_main c_m_1 WITH (nolock) " & _
            //        " INNER JOIN   consumers_themes c_t_1 ON c_t_1.supplier_Id = c_m_1.supplier_id " & _
            //       " INNER JOIN   companies_consumers c_co_1 on c_co_1.companies_Id = c_t_1.consumer_Id" & _
            //      " INNER JOIN   [content] co_1 WITH (nolock) ON c_m_1.content_main_Id = [co_1].content_main_Id   " & _
            //     " INNER JOIN   content_categories c_ca_1 WITH (nolock) ON c_t_1.category_Id = c_ca_1.category_Id AND c_ca_1.content_main_Id=c_m_1.content_main_id " & _
            //    " INNER JOIN   categories ca_1 WITH (nolock) ON ca_1.categoryId = c_ca_1.category_Id  " & _
            //   " INNER JOIN   publication_schemes p_s_1 ON p_s_1.publication_schemes_Id = c_ca_1.publication_scheme_Id  " & _
            //  " INNER JOIN   publication_schemes_rules p_s_r_1 ON p_s_r_1.publication_schemes_Id = p_s_1.publication_schemes_Id  " & _
            // " INNER JOIN   staging_stages s_s_1 ON [co_1].stage_Id = s_s_1.staging_stages_Id  " & _
            // " INNER JOIN   staging s_1 ON s_s_1.staging_Id = s_1.staging_Id AND c_m_1.content_staging_scheme_Id = s_1.staging_Id   " & _
            //         " WHERE      (ca_1.active = 1) AND (c_co_1.base_domain = @BaseDomain)   AND (p_s_r_1.from_date < GETDATE()) AND   (p_s_r_1.to_date > GETDATE()) " & _
            //        " AND (p_s_r_1.active = 1) AND (p_s_1.active = 1) AND   (s_1.active = 1) AND (s_s_1.action = 3)  " & _
            //       " and (co_1.content_types_languages_Id = c_co_1.default_language_Id) "
            // Declare @SkipCategoryActive as bit = 1
            // Declare @SkipPublicationSchemeRulesDate as bit = 1
            // Declare @SkipPublicationSchemeRulesActive as bit = 1
            // Declare @SkipPublicationSchemeActive as bit = 1
            // declare @SkipStagingActive as bit = 1
            // declare @SkipStagingStagePublish as bit = 1
            // declare @SkipContentAccessFilter as bit = 1
            // declare @SkipSkuFilter as bit = 1
            // Dim TurnOffFilters As Integer = 0
            // If Me.PageID = 58 Or Me.PageID = 28 Then
            //     TurnOffFilters = 1
            // End If
            //   query = "select top(1000) co_1.content_id " & _
            //   " from   content_main c_m_1 with (nolock) " & _
            //   " inner join   consumers_themes c_t_1 on c_t_1.supplier_id = c_m_1.supplier_id " & _
            //   " inner join   companies_consumers c_co_1 on c_co_1.companies_id = c_t_1.consumer_id" & _
            //   " inner join   [content] co_1 with (nolock) on c_m_1.content_main_id = [co_1].content_main_id   " & _
            //   " inner join   content_categories c_ca_1 with (nolock) on c_t_1.category_id = c_ca_1.category_id and c_ca_1.content_main_id=c_m_1.content_main_id " & _
            //   " inner join   categories ca_1 with (nolock) on ca_1.categoryid = c_ca_1.category_id  " & _
            //   " inner join   publication_schemes p_s_1 on p_s_1.publication_schemes_id = c_ca_1.publication_scheme_id  " & _
            //   " inner join   publication_schemes_rules p_s_r_1 on p_s_r_1.publication_schemes_id = p_s_1.publication_schemes_id  " & _
            //   " inner join   staging_stages s_s_1 on [co_1].stage_id = s_s_1.staging_stages_id  " & _
            //   " inner join   staging s_1 on s_s_1.staging_id = s_1.staging_id and c_m_1.content_staging_scheme_id = s_1.staging_id   " & _
            //   " left join Sku_ImpactedCompanies on Sku_ImpactedCompanies.CompanyID= c_t_1.consumer_Id " & _
            //   " left join Sku_Company_ContentGUID_Include on Sku_Company_ContentGUID_Include.CompanyID = c_t_1.consumer_Id and Sku_Company_ContentGUID_Include.ContentID= co_1.content_Id   " & _
            //   " left join Sku_ImpactedContentTypes as Sku_ImpactedContentTypes on c_m_1.content_types_Id = Sku_ImpactedContentTypes.ContentTypeID   " & _
            //   " left join Sku_Company_Category_Include as Sku_Company_Category_Include on Sku_Company_Category_Include.CompanyID = c_t_1.consumer_Id and Sku_Company_Category_Include.CategoryID = c_ca_1.category_Id  " & _
            //   " where " & _
            //   " ( Sku_ImpactedCompanies.CompanyID is Null or (not Sku_ImpactedCompanies.CompanyID is Null and ( ( not Sku_ImpactedContentTypes.ContentTypeID is null " & _
            //   " and  not Sku_Company_ContentGUID_Include.ContentID is null ) or ( Sku_ImpactedContentTypes.ContentTypeID is null and " & _
            //   " not Sku_Company_Category_Include.CategoryID is null )))) and " & _
            //   " (ca_1.active = 1) and (c_co_1.base_domain = @basedomain)   and (p_s_r_1.from_date < getdate()) and   (p_s_r_1.to_date > getdate()) " & _
            //  " and (p_s_r_1.active = 1) and (p_s_1.active = 1) and   (s_1.active = 1) and (s_s_1.action = 3)  " & _
            // " and (co_1.content_types_languages_id = c_co_1.default_language_id) "
            if (int.TryParse(PageID, out var pageId))
            {
                SetFilterParameters(ref parameterCollection, sqlConnection, pageId);
            }
            else
            {
                SetFilterParameters(ref parameterCollection, sqlConnection);
            }

            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                query =
                    " select distinct top (1000) co_1.content_Id,  ca_1.categoryId, ca_1.parentId, ca_1.lineage, cat.Depth" +
                    ", c_m_1.content_types_Id ";
            }
            else
            {
                query = " select distinct top (1000)  co_1.content_Id ";
            }

            // moved joins to companies consumers
            // "left join consumers_themes c_t_1 on c_t_1.supplier_id = c_m_1.supplier_id  and  c_t_1.category_id = c_ca_1.category_id " & _
            // "inner join companies_consumers c_co_1 on c_co_1.companies_Id = c_t_1.consumer_Id " & _
            // "left join Sku_ImpactedCompanies on Sku_ImpactedCompanies.CompanyID= c_t_1.consumer_Id  " & _
            // "left join Sku_Company_ContentGUID_Include on Sku_Company_ContentGUID_Include.CompanyID = c_t_1.consumer_Id and Sku_Company_ContentGUID_Include.ContentID= co_1.content_Id  " & _
            // "left join Sku_Company_Category_Include as Sku_Company_Category_Include on Sku_Company_Category_Include.CompanyID = c_t_1.consumer_Id and Sku_Company_Category_Include.CategoryID = c_ca_1.category_Id "
            // "(c_co_1.base_domain = @basedomain)   and " & _
            query = query + "from   content_main c_m_1 with (nolock)  " +
                    "inner join   [content] co_1 with (nolock) on c_m_1.content_main_id = [co_1].content_main_id " +
                    "inner join   content_categories c_ca_1 with (nolock) on c_ca_1.content_main_id=c_m_1.content_main_id " +
                    " " + "inner join   categories ca_1 with (nolock) on ca_1.categoryid = c_ca_1.category_id   " +
                    "inner join   publication_schemes p_s_1 on p_s_1.publication_schemes_id = c_ca_1.publication_scheme_id" +
                    " " +
                    "inner join   publication_schemes_rules p_s_r_1 on p_s_r_1.publication_schemes_id = p_s_1.publication_" +
                    "schemes_id " + "inner join   staging_stages s_s_1 on [co_1].stage_id = s_s_1.staging_stages_id  " +
                    "inner join   staging s_1 on s_s_1.staging_id = s_1.staging_id and c_m_1.content_staging_scheme_id = s" +
                    "_1.staging_id " + " inner join companies_consumers c_co_1 on c_co_1.base_domain = @basedomain " +
                    " left join consumers_themes c_t_1 on c_t_1.supplier_id = c_m_1.supplier_id  and  c_t_1.category_id = " +
                    "c_ca_1.category_id and c_t_1.consumer_Id = c_co_1.companies_Id " +
                    "left join languages lng on lng.languages_Id = co_1.content_types_languages_Id " +
                    "left join Sku_ImpactedCompanies on Sku_ImpactedCompanies.CompanyID= c_co_1.companies_Id  " +
                    "left join Sku_Company_ContentGUID_Include on Sku_Company_ContentGUID_Include.CompanyID = c_co_1.compa" +
                    "nies_Id and Sku_Company_ContentGUID_Include.ContentID= co_1.content_Id  " +
                    "left join Sku_ImpactedContentTypes as Sku_ImpactedContentTypes on c_m_1.content_types_Id = Sku_Impact" +
                    "edContentTypes.ContentTypeID " +
                    "left join Sku_Company_Category_Include as Sku_Company_Category_Include on Sku_Company_Category_Includ" +
                    "e.CompanyID = c_co_1.companies_Id and Sku_Company_Category_Include.CategoryID = c_ca_1.category_Id ";
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                query = query + "left join @Cats cat on cat.CategoryID = ca_1.categoryId";
            }

            // query = query + " where " & _
            // "(@SkipSkuFilter = 1 or Sku_ImpactedCompanies.CompanyID is Null or (not Sku_ImpactedCompanies.CompanyID is Null and ( ( not Sku_ImpactedContentTypes.ContentTypeID is null  and  not Sku_Company_ContentGUID_Include.ContentID is null ) or ( Sku_ImpactedContentTypes.ContentTypeID is null and  not Sku_Company_Category_Include.CategoryID is null )))) and  " & _
            // "(ca_1.active = 1 or @SkipCategoryActive = 1) and " & _
            // "(p_s_r_1.from_date < getdate() or @SkipPublicationSchemeRulesDate = 1) and   " & _
            // "(p_s_r_1.to_date > getdate() or @SkipPublicationSchemeRulesDate = 1)  and " & _
            // "(p_s_r_1.active = 1 or @SkipPublicationSchemeRulesActive = 1 ) and " & _
            // "(p_s_1.active = 1 or @SkipPublicationSchemeActive = 1) and   " & _
            // "(s_1.active = 1 or @SkipStagingActive = 1) and " & _
            // "(s_s_1.action = 3 or @SkipStagingStagePublish = 1)  " & _
            // " and (not c_t_1.category_Id is null or @SkipContentAccessFilter = 1) " '& _
            query = query + " where " +
                    @"(@SkipSkuFilter = 1 or Sku_ImpactedCompanies.CompanyID is Null or (not Sku_ImpactedCompanies.CompanyID is Null and ( ( not Sku_ImpactedContentTypes.ContentTypeID is null  and  not Sku_Company_ContentGUID_Include.ContentID is null ) or ( Sku_ImpactedContentTypes.ContentTypeID is null and  not Sku_Company_Category_Include.CategoryID is null )))) and  " +
                    "(ca_1.active = 1 or @SkipCategoryActive = 1) and " +
                    "(p_s_r_1.active = 1 or @SkipPublicationSchemeRulesActive = 1 ) and " +
                    "(p_s_1.active = 1 or @SkipPublicationSchemeActive = 1) and   " +
                    "(s_1.active = 1 or @SkipStagingActive = 1) and " +
                    "(s_s_1.action = 3 or @SkipStagingStagePublish = 1)  " +
                    " and (not c_t_1.category_Id is null or @SkipContentAccessFilter = 1) ";
            if (string.IsNullOrEmpty(LanguageCode))
            {
                query = query + "and (co_1.content_types_languages_id = c_co_1.default_language_id) ";
            }
            else
            {
                parameterCollection.AddWithValue("@LanguageCode", LanguageCode);
                // query = query + "and (co_1.content_types_languages_id = @LanguageId) "
                query = query + " and (lng.languagecode = @LanguageCode) ";
            }

            //  sometime otherfilter comes as (), and hence we should not include it in the query
            if (!(string.IsNullOrEmpty(otherFilter) || otherFilter.Equals("()")))
            {
                query = query + " AND (" + otherFilter + ")";
            }

            // clean up field filter
            if (!string.IsNullOrEmpty(fieldFilter))
            {
                if (fieldFilter.StartsWith("^^^AND^&^"))
                {
                    fieldFilter = fieldFilter.Substring(9);
                    expression = " AND ";
                }
                else if (fieldFilter.StartsWith("^^^OR^&^"))
                {
                    fieldFilter = fieldFilter.Substring(8);
                    expression = " OR ";
                }

                if (string.IsNullOrEmpty(expression))
                {
                    expression = "AND";
                }

                var useUppercaseHashColumn = false;
                using (var cmd =
                    new SqlCommand(
                        "select count(*) from sys.columns with (nolock) where Name = N'UpperCaseHash' and Object_ID = Object_ID(N'Content_Field_HashTable')",
                        sqlConnection))
                {
                    useUppercaseHashColumn = Int32.Parse(cmd.ExecuteScalar().ToString()) > 0;
                }


                query = query + expression + "(" + GetContentIdsFromFieldFilter(" co_1.content_Id  in  ", fieldFilter,
                            sqlConnection, parameterCollection, useUppercaseHashColumn) + ")";
            }

            if (!string.IsNullOrEmpty(CatQuery))
            {
                query = CatQuery + query;
            }
// order the result to parse the content correctly.
            if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
            {
                query = query + " order by depth, ca_1.lineage";
            }

            var sqlCommand = new SqlCommand(query, sqlConnection);
            foreach (SqlParameter param in parameterCollection)
            {
                sqlCommand.Parameters.AddWithValue(param.ParameterName, param.Value);
            }

            SqlDataReader sqlReader = null;
            try
            {
                sqlReader = sqlCommand.ExecuteReader();
                if (CategorySearchListAndCleanOtherFilterHelpers.CategorySearchHelpers.Count > 0)
                {
                    result = CategoryHunt.Process(ref sqlReader, CategorySearchListAndCleanOtherFilterHelpers,
                        sqlCommand);
                }
                else
                {
                    while (sqlReader.Read())
                    {
                        if (string.IsNullOrEmpty(result))
                        {
                            result = result + string.Format("\'{0}\'", sqlReader["content_Id"].ToString());
                        }
                        else
                        {
                            result = result + string.Format(",\'{0}\'", sqlReader["content_Id"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Throw New Exception(String.Format("GetContentIdsFromContentTypesAndSuppliers: [{0}] Command [{1}]", ex.Message, query))
            }
            finally
            {
                if (!(sqlReader == null))
                {
                    sqlReader.Close();
                }

                sqlCommand.Dispose();
            }

            if (string.IsNullOrEmpty(result))
            {
                result = "(\'00000000-0000-0000-0000-000000000000\')";
            }
            else
            {
                result = string.Format("({0})", result);
            }

            return result;
        }

        private void WriteToLog(string message, string s)
        {
            // Dim StreamWriter As System.IO.StreamWriter = New System.IO.StreamWriter("tm_log.txt", True)
            // StreamWriter.WriteLine(String.Format("{0}: {1} [{2}]", DateTime.Now.ToString("yyMMdd-HHmmss"), message, s))
            // StreamWriter.Close()
        }

        private string GetContentIdsFromFieldFilter(string preMessage, string fieldFilter, SqlConnection sqlConnection,
            SqlParameterCollection collection, bool useUpperCaseColumn = false)
        {
            var curOperator = String.Empty;
            // Warning!!! Optional parameters not supported
            var s = String.Empty;
            var cmdText = String.Empty;
            var preSt = String.Empty;
            var query = String.Empty;
            var tempSt = String.Empty;
            var searchableAttributeTableName = String.Empty;
            var command = new SqlCommand();
            foreach (SqlParameter parameter in collection)
            {
                // command.Parameters.Add(New SqlParameter(parameter.ParameterName, parameter.DbType, parameter.Size, parameter.Direction, parameter.IsNullable, parameter.Precision, 0, parameter.SourceColumn, DataRowVersion.Default, parameter.Value))
                command.Parameters.AddWithValue(parameter.ParameterName, parameter.Value);
            }

            command.Connection = sqlConnection;
            // " (UPPER(NAME)=UPPER(@Param1)) AND (value like @Param2 ) ^^^AND^&^  (UPPER(NAME)=UPPER(@Param3)) AND (value like @Param4 )"
            while (fieldFilter.IndexOf("^^^") >= 0)
            {
                preSt = fieldFilter.Substring(0, fieldFilter.IndexOf("^^^"));
                // (UPPER(NAME)=UPPER(@Param1)) AND (value like @Param2 )
                if (FoundSearchableAttributeTable(fieldFilter, command, ref searchableAttributeTableName))
                {
                    cmdText = string.Format("select ContentId from {0} where ( {1} )", searchableAttributeTableName,
                        preSt);
                    s = GetContentIds(cmdText, command, sqlConnection);
                    //  if the field isnt found in the searchable attr, we shouldnt search it else where
                    if (string.IsNullOrEmpty(s))
                    {
                        s = "\'00000000-0000-0000-0000-000000000000\'";
                    }
                }
                else
                {
                    s = String.Empty;
                }

                //  append content id list to query
                if (!string.IsNullOrEmpty(s))
                {
                    tempSt = tempSt + string.Format(" {0} {1} ( {2} )", curOperator, preMessage, s);
                }
                else
                {
                    //  todo: if s is empty then that means this attribute isnt in the searchable table, perhaps we can use the main table
                    // 'cmdText = String.Format(" SELECT co_2.content_Id as ContentId FROM [content] AS co_2 WITH (nolock) INNER JOIN " & _
                    // '                        " content_fields AS c_f_2 WITH (nolock) ON co_2.content_Id = c_f_2.content_Id INNER JOIN" & _
                    // '                        " content_types_fields AS c_t_f_2 WITH (nolock) ON c_f_2.content_types_fields_Id = c_t_f_2.content_types_fields_Id INNER JOIN" & _
                    // '                        " content_main AS c_m_2 ON c_m_2.content_main_Id = co_2.content_main_Id " & _
                    // '                        " WHERE ( {0} )", preSt.Replace("UPPER(NAME)", "UPPER(c_t_f_2.fieldname)").Replace("(value like", "(c_f_2.value_text LIKE"))
                    if (preSt.Contains("value equal"))
                    {
                        // "where UPPER (ContentTypesFieldName) = UPPER (@Param1) and HashValue = HASHBYTES('SHA1',@Param2)"
                        if (useUpperCaseColumn)
                        {
                            var sSearchText = "value equal";
                            var i = preSt.IndexOf(sSearchText) + sSearchText.Length;
                            var insertIndex = preSt.IndexOf("@", i);
                            if (insertIndex > 0)
                            {
                                preSt = preSt.Insert(insertIndex, "UPPER(");
                                insertIndex = preSt.LastIndexOf(")", i);
                                preSt = preSt.Insert(insertIndex, ")");
                            }

                            cmdText = string.Format(
                                "select ContentGUID as ContentID from Content_Field_HashTable WITH (NOLOCK)  WHERE ( {0} )",
                                preSt.Replace("UPPER(NAME)", "UPPER(ContentTypesFieldName)").Replace("(value equal",
                                    "UpperCaseHash = HASHBYTES(\'SHA1\',"));
                        }
                        else
                        {
                            cmdText = string.Format(
                                "select ContentGUID as ContentID from Content_Field_HashTable WITH (NOLOCK)  WHERE ( {0} )",
                                preSt.Replace("UPPER(NAME)", "UPPER(ContentTypesFieldName)")
                                    .Replace("(value equal", "HashValue = HASHBYTES(\'SHA1\',"));
                        }
                    }
                    else if (preSt.Contains("value like"))
                    {
                        cmdText = string.Format(
                            "select ContentGUID as ContentID from Content_Field_HashTable  WITH (NOLOCK) inner join content_types_" +
                            "fields  WITH (NOLOCK) on content_types_fields.content_types_fields_Id = Content_field_HashTable.Cont" +
                            "entTYpesFIeldID" + " where ({0})",
                            preSt.Replace("UPPER(NAME)", "UPPER(content_types_fields.fieldname)")
                                .Replace("(value like", "(value_text like "));
                    }
                    else
                    {
                        //  shouldn't ever happen but if it does I'll run the old way
                        cmdText = string.Format(
                            " SELECT co_2.content_Id as ContentId FROM [content] AS co_2 WITH (nolock) INNER JOIN " +
                            " content_fields AS c_f_2 WITH (nolock) ON co_2.content_Id = c_f_2.content_Id INNER JOIN" +
                            " content_types_fields AS c_t_f_2 WITH (nolock) ON c_f_2.content_types_fields_Id = c_t_f_2.content_typ" +
                            "es_fields_Id INNER JOIN" +
                            " content_main AS c_m_2  WITH (NOLOCK) ON c_m_2.content_main_Id = co_2.content_main_Id " +
                            " WHERE ( {0} )",
                            preSt.Replace("UPPER(NAME)", "UPPER(c_t_f_2.fieldname)")
                                .Replace("(value like", "(c_f_2.value_text LIKE")
                                .Replace("(value equal", "(c_f_2.value_text LIKE"));
                    }

                    s = GetContentIds(cmdText, command, sqlConnection);
                    if (!string.IsNullOrEmpty(s))
                    {
                        tempSt = tempSt + string.Format(" {0} {1} ( {2}  )", curOperator, preMessage, s);
                    }
                    else
                    {
                        tempSt = tempSt + string.Format(" {0} {1} ( \'00000000-0000-0000-0000-000000000000\' )",
                                     curOperator, preMessage);
                    }
                }

                //  keep parsing
                fieldFilter = fieldFilter.Substring(fieldFilter.IndexOf("^^^") + 3);
                // remove ^^^ --> AND^&^  (UPPER(NAME)=UPPER(@Param3)) AND (value like @Param4)
                curOperator = fieldFilter.Substring(0, fieldFilter.IndexOf("^&^"));
                //  AND
                fieldFilter = fieldFilter.Substring(fieldFilter.IndexOf("^&^") + 3);
                // (UPPER(NAME)=UPPER(@Param3)) AND (value like @Param4)
            }

            //  check to see if the table exists for the field name we are searching for.
            if (FoundSearchableAttributeTable(fieldFilter, command, ref searchableAttributeTableName))
            {
                s = GetContentIds(
                    string.Format("select ContentId from {0} where ( {1} )", searchableAttributeTableName, fieldFilter),
                    command, sqlConnection);
                //  if the field isnt found in the searchable attr, we shouldnt search it else where
                if (string.IsNullOrEmpty(s))
                {
                    s = "\'00000000-0000-0000-0000-000000000000\'";
                }
            }
            else
            {
                s = String.Empty;
            }

            // s = GetContentIds(String.Format("select ContentId from SearchableAttributes where ( {0} )", fieldFilter), command)
            if (!string.IsNullOrEmpty(s))
            {
                tempSt = tempSt + string.Format(" {0} {1} ( {2} )", curOperator, preMessage, s);
            }
            else
            {
                //  todo: if s is empty then that means this attribute isnt in the searchable table, perhaps we can use the main table
                // Updated by EGO 042710 to add hash
                // '   cmdText = String.Format(" SELECT co_2.content_Id as ContentId FROM [content] AS co_2 WITH (nolock) INNER JOIN " & _
                // '                        " content_fields AS c_f_2 WITH (nolock) ON co_2.content_Id = c_f_2.content_Id INNER JOIN" & _
                // '                        " content_types_fields AS c_t_f_2 WITH (nolock) ON c_f_2.content_types_fields_Id = c_t_f_2.content_types_fields_Id INNER JOIN" & _
                // '                        " content_main AS c_m_2 ON c_m_2.content_main_Id = co_2.content_main_Id " & _
                // '                        " WHERE ( {0} )", fieldFilter.Replace("UPPER(NAME)", "UPPER(c_t_f_2.fieldname)").Replace("(value like", "(c_f_2.value_text LIKE"))
                if (fieldFilter.Contains("value equal"))
                {
                    // "where UPPER (ContentTypesFieldName) = UPPER (@Param1) and HashValue = HASHBYTES('SHA1',@Param2)"
                    if (useUpperCaseColumn)
                    {
                        // " (UPPER(NAME)=UPPER(@Param1)) AND (value equal @Param2 )"
                        var sSearchText = "value equal";
                        var i = fieldFilter.IndexOf(sSearchText) + sSearchText.Length;
                        var insertIndex = fieldFilter.IndexOf("@", i);
                        if (insertIndex > 0)
                        {
                            fieldFilter = fieldFilter.Insert(insertIndex, "UPPER(");
                            insertIndex = fieldFilter.LastIndexOf(")", i);
                            fieldFilter = fieldFilter.Insert(insertIndex, ")");
                        }

                        cmdText = string.Format(
                            "select ContentGUID as ContentID from Content_Field_HashTable  WITH (NOLOCK) WHERE ( {0} )",
                            fieldFilter.Replace("UPPER(NAME)", "UPPER(ContentTypesFieldName)")
                                .Replace("(value equal", "UpperCaseHash = HASHBYTES(\'SHA1\',"));
                    }
                    else
                    {
                        cmdText = string.Format(
                            "select ContentGUID as ContentID from Content_Field_HashTable  WITH (NOLOCK) WHERE ( {0} )",
                            fieldFilter.Replace("UPPER(NAME)", "UPPER(ContentTypesFieldName)")
                                .Replace("(value equal", "HashValue = HASHBYTES(\'SHA1\',"));
                    }
                }
                else if (fieldFilter.Contains("value like"))
                {
                    cmdText = string.Format(
                        "select ContentGUID as ContentID from Content_Field_HashTable  WITH (NOLOCK) inner join content_types_" +
                        "fields  WITH (NOLOCK) on content_types_fields.content_types_fields_Id = Content_field_HashTable.Cont" +
                        "entTYpesFIeldID" + " where ({0})",
                        fieldFilter.Replace("UPPER(NAME)", "UPPER(content_types_fields.fieldname)")
                            .Replace("(value like", "(value_text like "));
                }
                else
                {
                    //  shouldn't ever happen but if it does I'll run the old way
                    cmdText = string.Format(
                        " SELECT co_2.content_Id as ContentId FROM [content] AS co_2 WITH (nolock) INNER JOIN " +
                        " content_fields AS c_f_2 WITH (nolock) ON co_2.content_Id = c_f_2.content_Id INNER JOIN" +
                        " content_types_fields AS c_t_f_2 WITH (nolock) ON c_f_2.content_types_fields_Id = c_t_f_2.content_typ" +
                        "es_fields_Id INNER JOIN" +
                        " content_main AS c_m_2  WITH (NOLOCK) ON c_m_2.content_main_Id = co_2.content_main_Id " +
                        " WHERE ( {0} )",
                        fieldFilter.Replace("UPPER(NAME)", "UPPER(c_t_f_2.fieldname)")
                            .Replace("(value like", "(c_f_2.value_text LIKE")
                            .Replace("(value equal", "(c_f_2.value_text LIKE"));
                }

                //  value equal - select ContentGUID from Content_Field_HashTable where UPPER (ContentTypesFieldName) = UPPER (@Param1) and HashValue = HASHBYTES('SHA1',@Param2)
                // value like - select ContentGUID from Content_Field_HashTable inner join content_types_fields on content_types_fields.content_types_fields_Id = Content_field_HashTable.ContentTYpesFIeldID where UPPER (content_types_fields.fieldname) = UPPER (@Param1) and value_text like @Param2
                s = GetContentIds(cmdText, command, sqlConnection);
                if (!string.IsNullOrEmpty(s))
                {
                    tempSt = tempSt + string.Format(" {0} {1} ( {2} )", curOperator, preMessage, s);
                }
                else
                {
                    tempSt = tempSt + string.Format(" {0} {1} ( \'00000000-0000-0000-0000-000000000000\' )",
                                 curOperator, preMessage);
                }
            }

            return tempSt;
        }

        private string ConvertSQLLikeIntoLuceneLike(string Value)
        {
            return Value.Replace("%", "*").Replace("_", "?");
        }

        private string GetLuceneSearch(string commandText, SqlCommand command)
        {
            //  will return either "" or the lucene search string
            //  should eventually check web.config to see if we even want to do a lucene search
            //  here are the 4 query possiblities passed in:
            //  Value Equal Hash select ContentGUID as ContentID from Content_Field_HashTable  WHERE (  (UPPER(ContentTypesFieldName)=UPPER(@Param1)) AND HashValue = HASHBYTES('SHA1', @Param2 ) )
            //  Value Like post hash select ContentGUID as ContentID from Content_Field_HashTable inner join content_types_fields on content_types_fields.content_types_fields_Id = Content_field_HashTable.ContentTYpesFIeldID where ( (UPPER(content_types_fields.fieldname)=UPPER(@Param1)) AND (value_text like  @Param2 ))
            // Searchable attributs select ContentId from {0} where ( {1} )
            //  non hashed value like SELECT co_2.content_Id as ContentId FROM [content] AS co_2 WITH (nolock) INNER JOIN content_fields AS c_f_2 WITH (nolock) ON co_2.content_Id = c_f_2.content_Id INNER
            // Only use the ones where the hash existed because that's also used for indexing
            return "";
            if (commandText.Contains("select ContentId from"))
            {
                return "";
            }
            else if (commandText.Contains("SELECT co_2.content_Id as ContentId FROM [content]"))
            {
                return "";
            }
            else if (commandText.Contains("select ContentGUID"))
            {
                //  this one could be lucened
                // get the first parameter and grab the field name from the SQL command
                // get the second parameter and grabl the field value from the SQL command
                // if it's a value Like change the syntax as needed
                var HoldCommand2 = commandText.Substring(commandText.IndexOf("@"));
                var HoldFieldParamName = HoldCommand2.Substring(0, HoldCommand2.IndexOf(")"));
                HoldCommand2 = HoldCommand2.Substring(2).Substring(HoldCommand2.Substring(2).IndexOf("@"));
                var HoldFieldValueParamName = HoldCommand2.Substring(0, HoldCommand2.IndexOf(")"));
                HoldFieldParamName = " ".Trim();
                HoldFieldValueParamName = " ".Trim();
                //  now I have both parameters the first one is the field name, the second is the field value
                var FieldValue = "";
                var FieldName = "";
                foreach (SqlParameter HoldParam in command.Parameters)
                {
                    if (FieldValue == ""
                        || FieldName == "")
                    {
                        if (HoldParam.ParameterName == HoldFieldValueParamName)
                        {
                            FieldValue = HoldParam.Value.ToString();
                        }

                        if (HoldParam.ParameterName == HoldFieldParamName)
                        {
                            FieldName = HoldParam.Value.ToString();
                        }
                    }
                }

                if (FieldValue == ""
                    || FieldName == "")
                {
                    return "";
                }

                if (commandText.ToUpper().Contains("LIKE"))
                {
                    FieldValue = ConvertSQLLikeIntoLuceneLike(FieldValue);
                }

                return FieldName + ":" + FieldValue;
            }
            else
            {
                return "";
            }

            return "";
        }

        private bool FoundSearchableAttributeTable(string fieldFilter, SqlCommand command, ref string tableName)
        {
            // input: (UPPER(NAME)=UPPER(@ParamX)) AND (value like @ParamY )
            //  get the field name
            var paramName = String.Empty;
            var flag = false;
            if (string.IsNullOrEmpty(fieldFilter))
            {
                return flag;
            }

            tableName = String.Empty;
            fieldFilter = fieldFilter.Trim();
            paramName = fieldFilter.Substring(19, fieldFilter.IndexOf(")", 19) - 19);
            if (string.IsNullOrEmpty(paramName))
            {
                return false;
            }

            try
            {
                var param = command.Parameters[paramName];
                var query = string.Format(
                    "SELECT COUNT([name]) FROM sys.objects WHERE object_id = OBJECT_ID(N\'SearchableAttributes_{0}\') AND ty" +
                    "pe in (N\'U\')", FormatParameterValue(param.Value.ToString()));
                command.CommandText = query;
                if (int.Parse(command.ExecuteScalar().ToString()) > 0)
                {
                    tableName = string.Format("SearchableAttributes_{0}", FormatParameterValue(param.Value.ToString()));
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                // todo: CSPException
            }

            return flag;
        }

        private string FormatParameterValue(string s)
        {
            s = s.ToLower();
            return s.Replace(" ", "");
        }

        private string GetLuceneDirectory(string SystemID, SqlConnection SQLConnection)
        {
            // open connection
            //  use system id to get the right file path for whatever system this class is executed on.
            //  Set IndexPath And IndexLastUpdated 
            // close connection
            // 
            var Folder = "";
            var Command = new SqlCommand("select * from LuceneIndex where SystemID = @SystemID", SQLConnection);
            Command.Parameters.AddWithValue("@SystemID", SystemID);
            SqlDataReader Reader = null;
            Reader = Command.ExecuteReader();
            try
            {
                while (Reader.Read())
                {
                    Folder = Reader["IndexFolder"].ToString();
                    // Me.IndexLastUpdated = Reader.Item("LastIndexRun")
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Reader.Close();
            }

            return Folder;
        }

        private string LuceneQuery(string Directory, string QueryString)
        {
            Directory LDir = FSDirectory.GetDirectory(Directory, false);
            // Dim LDir As Lucene.Net.Store.RAMDirectory = New Lucene.Net.Store.RAMDirectory()
            Analyzer LAnalyzer = new StandardAnalyzer();
            var LQueryParser =
                new QueryParser("ALLCONTENT", LAnalyzer);
            var LQuery = LQueryParser.Parse(QueryString);
            var LSerach = new IndexSearcher(LDir);
            var Start = DateTime.Now;
            DateTime finish;
            var LHits = LSerach.Search(LQuery);
            var s = String.Empty;
            try
            {
                for (var I = 0;
                    I
                    <= LHits.Length() - 1;
                    I++)
                {
                    var LDoc2 = LHits.Doc(I);
                    if (string.IsNullOrEmpty(s))
                    {
                        s = s + string.Format("\'{0}\'", LDoc2.GetField("CONTENTID").StringValue());
                    }
                    else
                    {
                        s = s + string.Format(",\'{0}\'", LDoc2.GetField("CONTENTID").StringValue());
                    }
                }

                finish = DateTime.Now;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                LDir.Close();
                LSerach.Close();
            }

            return s;
        }

        private string GetContentIds(string commandText, SqlCommand command, SqlConnection sqlConnection)
        {
            var s = String.Empty;
            //  run a query to get result
            var LuceneStart = DateTime.Now;
            DateTime LuceneFinish;
            var HoldLuceneString = GetLuceneSearch(commandText, command);
            if (HoldLuceneString != "")
            {
                var Folder = GetLuceneDirectory("1", sqlConnection);
                if (Folder != "")
                {
                    //  do the search!
                    try
                    {
                        var Guids = String.Empty;
                        Guids = LuceneQuery(Folder, HoldLuceneString);
                        // Return Guids
                        LuceneFinish = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        //  if the query throws an error just use the default CSP query
                    }
                }
            }

            var ReaderStart = DateTime.Now;
            command.CommandText = commandText;
            SqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
                //  get content id list
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(s))
                    {
                        s = s + string.Format("\'{0}\'", reader["ContentId"].ToString());
                    }
                    else
                    {
                        s = s + string.Format(",\'{0}\'", reader["ContentId"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                // Throw New Exception(String.Format("GetContentIds: [{0}] Command [{1}]", ex.Message, commandText))
            }
            finally
            {
                if (!(reader == null))
                {
                    reader.Close();
                }
            }

            var ReaderFinish = DateTime.Now;
            return s;
        }

        public SqlCommand GenerateSQLCommand(string FieldFilter, string OtherFilter, string BaseDomain,
            List<SqlParameter> ParamsWValues, string LanguageCode, bool languageFlag, SqlConnection connection,
            ref CategorySearchListAndCleanOtherFilterHelper CategorySearchListAndCleanOtherFilterHelpers)
        {
            var HoldCommand = new SqlCommand();
            foreach (var Item in ParamsWValues)
            {
                try
                {
                    // HoldCommand.Parameters.Add(Item)
                    HoldCommand.Parameters.AddWithValue(Item.ParameterName, Item.Value);
                }
                catch (Exception End)
                {
                }
            }
            HoldCommand.Parameters.AddWithValue("@BaseDomain", BaseDomain);
            HoldCommand.CommandText = GenerateCommandText(FieldFilter, OtherFilter, LanguageCode, languageFlag,
                connection, HoldCommand.Parameters, ref CategorySearchListAndCleanOtherFilterHelpers);
            return HoldCommand;
        }
    }
}