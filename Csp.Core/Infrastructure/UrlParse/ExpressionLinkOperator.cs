﻿using System;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ExpressionLinkOperator : ExpressionRoot
    {
        protected string GetSQLOperator()
        {
            if (fOperatorType == "&")
            {
                return "AND";
            }
            if (fOperatorType == "|")
            {
                return "OR";
            }
            return "MissingOperator";
        }

        public bool HasRightSide => fHasRightSide;

        public string fOperatorType;

        //  and / or / none aka doesn't have a right side
        public ExpressionRoot fLeftSide;

        // could be expression or Expression chain...
        public ExpressionRoot fRightSide;

        // could be expression or Expression chain...
        public bool fHasRightSide;

        private ExpressionRoot vLeftSide;
        private ExpressionRoot vRightSide;
        private string vOperatorType;

        //  should be = true only if the expression has a right side
        //  generate SQL here? 
        public ExpressionLinkOperator(ref ExpressionRoot vLeftSide, ref ExpressionRoot vRightSide, string vOperatorType)
        {
            fLeftSide = vLeftSide;
            fOperatorType = vOperatorType;
            if (fOperatorType != "none")
            {
                fHasRightSide = true;
                fRightSide = vRightSide;
            }
            else
            {
                fRightSide = null;
            }
        }

        public void AddRightSide(ref ExpressionRoot vRightSide, string vOperatorType)
        {
            if (fRightSide != null || fOperatorType != "none")
            {
                throw new Exception("You can\'t add a new right side to an expression if the right side isn\'t null");
            }

            fRightSide = vRightSide;
            fOperatorType = vOperatorType;
        }

        public override string GenenerateSQLFromExpression()
        {
            var s = fLeftSide.GenenerateSQLFromExpression();
            if (fOperatorType != "none")
            {
                var RightSide = fRightSide.GenenerateSQLFromExpression();
                if (RightSide.IndexOf("$$$") != -1
                    && RightSide.IndexOf("$$$") < 4
                    && s.IndexOf("$$$") == -1)
                {
                    s = s + " $$$ " + "^^^" + GetSQLOperator() + "^&^" + " " + RightSide;
                    //  this happens if the left of me isn't a field but the right side is
                    //  this is the first instance of a field being used; ends up being the precursor
                    //  tries to check to see if the right side starts with the $$$ to see if the immediate right is a field. 
                }
                else if (s.IndexOf("$$$") != -1 && RightSide.IndexOf("$$$") != -1)
                {
                    //  my left side and right side are a field
                    s = s + " " + "^^^" + GetSQLOperator() + "^&^" + " @@@ ";
                    s = s + RightSide;
                }
                else if (s.IndexOf("$$$") == -1 && (RightSide.IndexOf("$$$") == -1 || RightSide.IndexOf("$$$") >= 4))
                {
                    s = s + " " + GetSQLOperator() + " ";
                    s = s + RightSide;
                }
                else
                {
                    // shouldn't get here but just in case
                    s = s + " " + GetSQLOperator() + " ";
                    s = s + RightSide;
                }
            }
            else
            {
                // no right side
                if (s.IndexOf("$$$") != -1)
                {
                    s = s + "@@@";
                }
            }

            return s;
        }
    }
}