﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategorySearchListAndCleanOtherFilterHelper
    {
        public List<CategorySearchHelper> CategorySearchHelpers;
        public CategoryHunt CategoryHunter;
        public string CleanOtherFieldFilter;

        public CategorySearchListAndCleanOtherFilterHelper(ref List<CategorySearchHelper> inCategorySearchHelpers, string inCleanOtherFieldFilter)
        {
            CategorySearchHelpers = inCategorySearchHelpers;
            CleanOtherFieldFilter = inCleanOtherFieldFilter;
        }

        public string GetSQLToLoadCatsTmpTable()
        {
            if (CategorySearchHelpers.Count == 0)
            {
                return "";
            }

            var s =
                "  DECLARE @Cats TABLE ( CategoryID int, CategoryName varchar (255), ParentID int, Depth int, Lineage " +
                "varchar(255) )  " + "declare @CategoryID as int = 0 " + "declare @MaxDepth as int = 1 " +
                "declare @Skip0Level as bit = 0 ";
            foreach (var csh in CategorySearchHelpers)
            {
                // ' down
                var Params = " set @CategoryID = " + csh.CategoryID + " set @MaxDepth = " + csh.Depth + " set @Skip0Level = ";
                if (csh.IncludeLevel0)
                {
                    Params = Params + "0 ";
                }
                else
                {
                    Params = Params + "1 ";
                }

                s = s + Params;
                if (csh.SearchUp)
                {
                    const string subQ = " ;With CatParents (CategoryID, CategoryName,ParentID, Depth, Lineage) as" + "(" +
                                        "select CSuper.categoryId, CSuper.categoryText, CSuper.ParentID, 0 as Depth, CSuper.lineage from " +
                                        "categories CSuper inner join categories on categories.CategoryID = CSuper.categoryId where CSuper.cat" +
                                        "egoryId = @CategoryID " + "union all " +
                                        "select Csub.categoryId, Csub.categoryText,cSub.ParentID, CH.Depth + 1, Csub.lineage " +
                                        "from categories as Csub inner join CatParents as CH on CH.ParentID = CSub.categoryId " +
                                        ") " + "insert into @Cats (CategoryID, CategoryName, ParentID,Depth,Lineage ) " +
                                        "select CategoryID, CategoryName,ParentID,Depth,Lineage from CatParents where Depth <=@MaxDepth and (@" +
                                        "Skip0Level <> 1 or Depth <> 0)   ";
                    s = s + subQ;
                }
                else
                {
                    const string subQ = " ;With CatChildren (CategoryID, CategoryName,ParentID, Depth, Lineage) as " +
                                        "( " +
                                        "select CSuper.categoryId, CSuper.categoryText, CSuper.ParentID, 0 as Depth, CSuper.lineage from " +
                                        "categories CSuper inner join categories on categories.CategoryID = CSuper.categoryId where CSuper.cat" +
                                        "egoryId = @CategoryID " + "union all " +
                                        "select Csub.categoryId, Csub.categoryText,cSub.ParentID, CH.Depth + 1, Csub.lineage " +
                                        "from categories as Csub inner join CatChildren as CH on CH.categoryId = CSub.ParentID " +
                                        ") " +
                                        "insert into @Cats (CategoryID, CategoryName, ParentID,Depth,Lineage ) " +
                                        "select CategoryID, CategoryName,ParentID,Depth,Lineage from CatChildren where Depth <=@MaxDepth and (" +
                                        "@Skip0Level <> 1 or Depth <> 0)  ";
                    s = s + subQ;
                }

            }

            return s;
        }
    }
}