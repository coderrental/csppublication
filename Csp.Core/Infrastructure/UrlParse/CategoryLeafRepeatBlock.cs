﻿namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryLeafRepeatBlock
    {
        public string RepeatText;
        public string RepeatLandmark;
        public CategoryLeafRepeatBlock(string inRepeatText, string inLandmark)
        {
            RepeatText = inRepeatText;
            RepeatLandmark = inLandmark;
        }
    }
}