﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryHuntNode
    {
        public CategoryHuntNode(Guid contentId, int categoryId, int parentCategory, string lineage, int depth, int contentTypeId)
        {
            ContentId = contentId;
            CategoryId = categoryId;
            ParentCategoryId = parentCategory;
            Lineage = lineage;
            Depth = depth;
            ContentTypeId = contentTypeId;
            Children = new List<CategoryHuntNode>();
        }

        public CategoryHuntNode()
        {
            ContentId = Guid.Empty;
            CategoryId = -1;
            ParentCategoryId = -1;
            Lineage = string.Empty;
            Depth = -1;
            Children = new List<CategoryHuntNode>();
        }

        public Guid ContentId { get; set; }

        public int CategoryId { get; set; }

        public int ParentCategoryId { get; set; }

        public string Lineage { get; set; }

        public int Depth { get; set; }

        public List<CategoryHuntNode> Children { get; set; }

        public bool HasChildren { get; set; }

        public int ContentTypeId { get; set; }

        public override string ToString()
        {
            return $"l: {Lineage} d: {Depth}";
        }
    }
}