﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class SupplierCatTree //CategoryLoader.vb
    {
        public string ConsumerID;
        public string SupplierID;
        public CategoryItem RootCategoryItem;
        public List<CategoryItem> ListOfAllCategories;
        public ContentCollection RelatedContentCollection;

        public SupplierCatTree(string vConsumerId, string vSupplierId, CategoryItem vRootCategoryItem)
        {
            ConsumerID = vConsumerId;
            SupplierID = vSupplierId;
            ListOfAllCategories = new List<CategoryItem>();
            FillLocalCategories(vRootCategoryItem);
            RelatedContentCollection = null;
        }

        private void FillLocalCategories(CategoryItem inRootCategoryItem)
        {
            RootCategoryItem = GetNewCopyOfCategoryItem(null, inRootCategoryItem);
            ListOfAllCategories.Add(RootCategoryItem);
            CopyAllChildren(ref RootCategoryItem, inRootCategoryItem);
        }

        private void CopyAllChildren(ref CategoryItem inLocalParent, CategoryItem inSourceParent)
        {
            foreach (var item in inSourceParent.ChildCategories)
            {
                var curLocalItem = GetNewCopyOfCategoryItem(inLocalParent, item);
                inLocalParent.ChildCategories.Add(curLocalItem);
                ListOfAllCategories.Add(curLocalItem);
                CopyAllChildren(ref curLocalItem, item);
            }
        }

        private CategoryItem GetNewCopyOfCategoryItem(CategoryItem localParent, CategoryItem inSourceItem)
        {
            return new CategoryItem(localParent, inSourceItem.ExternalCategoryCode, inSourceItem.CategoryID,
                inSourceItem.Depth, inSourceItem.CategoryActive, inSourceItem.PublicationSchemePublishToday);
        }

        public void MarkCatIDAsInTheme(string catId)
        {
            foreach (var item in ListOfAllCategories)
            {
                if (item.CategoryID == catId)
                {
                    item.IsInTheme = true;
                    return;
                }
            }
        }

        public void AssignContentToCategories()
        {
            foreach (var curCat in ListOfAllCategories)
            {
                if (curCat.IsInTheme)
                {
                    foreach (var item in RelatedContentCollection.fMyContent)
                    {
                        foreach (var itemCat in item.fCategoriesImAssignedTo)
                        {
                            if (itemCat== curCat.CategoryID)
                            {
                                curCat.AssignedCatContentPiece = item;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}