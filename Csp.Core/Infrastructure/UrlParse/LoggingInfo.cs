﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class LoggingInfo
    {
        public string fstrIP;
        public string fstrUserAgent;
        public string fstrReferer;
        public string fstrRemoteHost;
        public string fstrRequestMethod;
        public string fstrURL;
        public Guid fContentID;

        public string fstrAbsPath;
        public string fstrAbsUri;
        public string fstrOrgString;
        public string fstrQuery;
        public string fstrRawURL;
        public string fDomain = "";
        public string fPageID = "";
        public string fConsumerID = "";
        public string fFieldValues = "";
        public string fLoggingCode = "";

        public LoggingInfo(string vstrIp, string vstrUserAgent, string vstrReferer, string vstrRemoteHost,
            string vstrRequestMethod, string vstrUrl, Guid vContentId, string vstrAbsPath, string vstrAbsUri,
            string vstrOrgString, string vstrQuery, string vstrRawUrl)
        {
            fstrIP = vstrIp;
            fstrUserAgent = vstrUserAgent;
            fstrReferer = vstrReferer;
            fstrRequestMethod = vstrRequestMethod;
            fstrRemoteHost = vstrRemoteHost;
            fstrURL = vstrUrl;
            fContentID = vContentId;
            fstrAbsPath = vstrAbsPath;
            fstrAbsUri = vstrAbsUri;
            fstrOrgString = vstrOrgString;
            fstrQuery = vstrQuery;
            fstrRawURL = vstrRawUrl;
        }

        public void WriteLog(SqlConnection connection)
        {
            var didOpen = false;
            if (connection.State == ConnectionState.Closed)
            {
                didOpen = true;
                connection.Open();
            }
            const string splString = @"INSERT INTO Logging (IPAddress, HTTPUserAgent, HTTPReferer, RemoteHost, RequestMethod, FullURL, ContentIDRequested, DateTimeStamp, AbsPath, AbsUri,OrgString,Query, RawURL, Domain, ConsumerID, PageID, LoggingCode, FieldValues) 
                                        VALUES (@IP,@HTTPUA,@HTTPR,@RemoteHost,@RequestMethod,@FullURL,@ContentID, @DateTime,@AbsPath,@AbsUri,@OrigString,@Query,@RawURL,@Domain,@ConsumerID,@PageID,@LoggingCode,@FieldValues)";

            var command = new SqlCommand(splString) {Connection = connection};
            command.Parameters.Add(new SqlParameter("@IP", fstrIP));
            command.Parameters.Add(new SqlParameter("@HTTPUA", fstrUserAgent));
            command.Parameters.Add(new SqlParameter("@HTTPR", fstrReferer));
            command.Parameters.Add(new SqlParameter("@RemoteHost", fstrRemoteHost));
            command.Parameters.Add(new SqlParameter("@RequestMethod", fstrRequestMethod));
            command.Parameters.Add(new SqlParameter("@FullURL", fstrURL));
            command.Parameters.Add(new SqlParameter("@ContentID", fContentID));
            command.Parameters.Add(new SqlParameter("@DateTime", DateTime.Now));
            command.Parameters.Add(new SqlParameter("@AbsPath", fstrAbsPath));
            command.Parameters.Add(new SqlParameter("@AbsUri", fstrAbsUri));
            command.Parameters.Add(new SqlParameter("@OrigString", fstrOrgString));
            command.Parameters.Add(new SqlParameter("@Query", fstrQuery));
            command.Parameters.Add(new SqlParameter("@RawURL", fstrRawURL));
            command.Parameters.Add(new SqlParameter("@Domain", fDomain));
            command.Parameters.Add(new SqlParameter("@ConsumerID", fConsumerID));
            command.Parameters.Add(new SqlParameter("@PageID", fPageID));
            command.Parameters.Add(new SqlParameter("@LoggingCode", fLoggingCode));
            command.Parameters.Add(new SqlParameter("@FieldValues", fFieldValues));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw exception;
            }

            if (didOpen)
            {
                connection.Close();
            }
        }

    }
}
