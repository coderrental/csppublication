﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class IfBlock
    {
        public string IfCondition;

        public string LeftSideValue;

        public string RightSideValue;

        public string TheOperator;

        // =<> < >
        public string IfTrueThisText;

        public ContentCollection fRelatedContent;

        private void SplitLeftRightByOperator(string inExpression, ref string left, ref string right,
            ref string vOperator)
        {
            if (inExpression.Contains(">="))
            {
                vOperator = ">=";
            }
            else if (inExpression.Contains("<="))
            {
                vOperator = "<=";
            }
            else if (inExpression.Contains("="))
            {
                vOperator = "=";
            }
            else if (inExpression.Contains("<>"))
            {
                vOperator = "<>";
            }
            else if (inExpression.Contains("!="))
            {
                vOperator = "<>";
            }
            else if (inExpression.Contains(">"))
            {
                vOperator = ">";
            }
            else if (inExpression.Contains("<"))
            {
                vOperator = "<";
            }
            else
            {
                // shouldn't happen!
            }

            left = inExpression.Substring(0, inExpression.IndexOf(vOperator));
            right = inExpression.Substring(inExpression.IndexOf(vOperator) + vOperator.Length);
        }

        public IfBlock(string inIfBlock, ref Tuple<string, Task<ContentCollection>> vContentCollection)
        {
            // takes in:
            // {if:FPrice=7.89}Wow thats cheap!{eIF}
            // {elseif:FPrice>49.00}Wow that's pricey!{eIF}
            // {elseif:FPrice=FContentTypeID}shoudln't happen{eIF}
            // {else}Eh, thats an ok price :-|{eIF}{/if}.
            // fill local variables
            if (inIfBlock == null)
            {
                return;
            }

            fRelatedContent = vContentCollection.Item2.Result;
            var HoldExpression = "";
            var HoldContent = "";
            var Left = "";
            var Right = "";
            var vOperator = "";
            if (inIfBlock.StartsWith("{if:") || inIfBlock.StartsWith("{elseif:"))
            {
                HoldExpression = inIfBlock.Substring(inIfBlock.IndexOf(":") + 1);
                HoldContent = HoldExpression.Substring(HoldExpression.IndexOf("}") + 1);
                HoldExpression = HoldExpression.Substring(0, HoldExpression.IndexOf("}"));
                // Hold Expression = FPrice=FContentTypeID or FPrice>49.00
                SplitLeftRightByOperator(HoldExpression, ref Left, ref Right, ref vOperator);
                TheOperator = vOperator;
                RightSideValue = Right;
                LeftSideValue = Left;
                IfTrueThisText = HoldContent;
            }
            else if (inIfBlock.StartsWith("{else}"))
            {
                HoldContent = inIfBlock.Substring(inIfBlock.IndexOf("}") + 1);
                TheOperator = "=";
                RightSideValue = "a";
                LeftSideValue = "a";
                IfTrueThisText = HoldContent;
            }
            else
            {
                // shouldn't happen!
            }
        }

        private Type GetFieldType(string fName, ref ContentPiece inContentPiece)
        {
            var name = fName.Substring(1);
            name = name.Replace(" ", "_");
            var mytype = inContentPiece.GetType();
            var myPropertyInfos = mytype.GetProperties();
            object returnObj = null;
            foreach (var prop in myPropertyInfos)
            {
                if (prop.Name.ToUpper() == name.ToUpper())
                {
                    // Not String.Compare(Prop.Name, name, True) Then
                    return prop.PropertyType;
                }
            }

            return null;
        }

        private Type GetTypeFromString(string inString)
        {
            try
            {
                if (bool.TryParse(inString, out var boolOut))
                {
                    //  = System.Convert.ToBoolean(InString)
                    return boolOut.GetType();
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (int.TryParse(inString, out var intOut))
                {
                    // = System.Convert.ToInt32(InString)
                    return intOut.GetType();
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (double.TryParse(inString, out var doub))
                {
                    // = System.Convert.ToDouble(InString)
                    return doub.GetType();
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                // = System.Convert.ToDateTime(InString)
                if (DateTime.TryParse(inString, out var adate))
                {
                    return adate.GetType();
                }
            }
            catch (Exception ex)
            {
            }

            return inString.GetType();
        }

        private Type GetTheType(string inSideValue, ref ContentPiece inContentPiece)
        {
            Type theType = null;
            if (inSideValue.StartsWith("F"))
            {
                // Assume it's a field
                // look up that field type....
                theType = GetFieldType(inSideValue, ref inContentPiece);
            }
            else
            {
                //  Try to guess it's type
                theType = GetTypeFromString(inSideValue);
            }

            return theType;
        }

        public object GetValueFromStringAndType(string inValueStr, ref Type inType, ref ContentPiece inContentPiece)
        {
            var holdFieldValue = "";
            if (inValueStr.StartsWith("F"))
            {
                holdFieldValue =
                    ContentPieceTypeCreator.GetPropertyAsSafeString(inContentPiece, inValueStr.Substring(1));
            }
            else
            {
                holdFieldValue = inValueStr;
            }

            switch (inType.Name)
            {
                case "String":
                    var holdStr = holdFieldValue;
                    return holdStr;
                case "Int32":
                    int.TryParse(holdFieldValue, out var holdInt);
                    return holdInt;
                case "Double":
                    double.TryParse(holdFieldValue, out var holdDoub);
                    return holdDoub;
                case "DateTime":
                    DateTime.TryParse(holdFieldValue, out var holdDate);
                    return holdDate;
                case "Boolean":
                    bool.TryParse(holdFieldValue, out var holdBool);
                    return holdBool;
                default:
                    return holdFieldValue;
            }
        }

        public bool Evaluate(ref ContentPiece inContentPiece)
        {
            // return true if if is true
            var leftSideType = GetTheType(LeftSideValue, ref inContentPiece);
            var rightSideType = GetTheType(RightSideValue, ref inContentPiece);
            if (leftSideType == null
                || rightSideType == null)
            {
                return false;
            }

            if (leftSideType.Name != rightSideType.Name)
            {
                //  force allowable type upconversion
                if (leftSideType.Name == "Double")
                {
                    if (rightSideType.Name == "Int32")
                    {
                        rightSideType = leftSideType;
                    }
                }
                else if (rightSideType.Name == "Double")
                {
                    if (leftSideType.Name == "Int32")
                    {
                        leftSideType = rightSideType;
                    }

                    // added by ed to upconvert to string from int, and not return false
                }
                else if (rightSideType.Name == "Int32"
                         && leftSideType.Name == "String")
                {
                    rightSideType = leftSideType;
                }
                else if (leftSideType.Name == "Int32"
                         && rightSideType.Name == "String")
                {
                    leftSideType = rightSideType;
                }
                else if (rightSideType.Name == "DateTime"
                         && leftSideType.Name == "String")
                {
                    leftSideType = rightSideType;
                }
                else
                {
                    return false;
                }
            }

            var leftSideObj = GetValueFromStringAndType(LeftSideValue, ref leftSideType, ref inContentPiece);
            var rightSideObj =
                GetValueFromStringAndType(RightSideValue, ref rightSideType, ref inContentPiece);
            switch (TheOperator)
            {
                case "=":
                    return leftSideObj == rightSideObj;
                case ">":
                    return Evaluate(leftSideObj, rightSideObj, leftSideType) > 0;
                case ">=":
                    return Evaluate(leftSideObj, rightSideObj, leftSideType) >= 0;
                case "<":
                    return Evaluate(leftSideObj, rightSideObj, leftSideType) < 0;
                case "<=":
                    return Evaluate(leftSideObj, rightSideObj, leftSideType) <= 0;
                case "<>":
                    return leftSideObj != rightSideObj;
            }
            return false;
        }


        private int Evaluate(object leftSideObj, object rightSideObj, Type type)
        {
            switch (type.Name)
            {
                case "Int32":
                    return ((int) leftSideObj).CompareTo((int) rightSideObj);
                case "Double":
                    return ((double) leftSideObj).CompareTo((double) rightSideObj);
                case "DateTime":
                    return ((DateTime) leftSideObj).CompareTo((DateTime) rightSideObj);
                case "Boolean":
                    return ((bool) leftSideObj).CompareTo((bool) rightSideObj);
                //case "String":
                default:
                    return ((double) leftSideObj).CompareTo((double) rightSideObj);
            }
            //-1: LessThan; 0: Equal; 1: GreaterThan
        }

        public static string GetFirstIfStatementTrimOffRest(ref string inHtml)
        {
            //  takes in any if block, pulls the first if out returns that, trims InHTML to remove the first if
            //  if InHTML is the last If in the If Else block (ends with {/if}) return block, set InHTML to ""
            // got to do somethign with the last {/if}
            var holdHtml = inHtml;
            var posOfIf = int.MaxValue;
            var posOfElseIf = int.MaxValue;
            var posOfElse = int.MaxValue;
            var posOfEndIf = int.MaxValue;
            if (holdHtml.Contains("{if:"))
            {
                posOfIf = holdHtml.IndexOf("{if:");
            }

            if (holdHtml.Contains("{elseif:"))
            {
                posOfElseIf = holdHtml.IndexOf("{elseif:");
            }

            if (holdHtml.Contains("{else}"))
            {
                posOfElse = holdHtml.IndexOf("{else}");
            }

            if (holdHtml.Contains("{/if}"))
            {
                posOfEndIf = holdHtml.IndexOf("{/if}");
            }

            if (posOfIf == int.MaxValue && posOfElseIf == int.MaxValue && posOfElse == int.MaxValue
                || posOfEndIf == int.MaxValue)
            {
                holdHtml = "";
                return null;
            }

            var holdFirstIfBranch = "";
            if (posOfIf < posOfElseIf
                && posOfIf < posOfElse)
            {
                // read from the start to the next {eIF}
                holdFirstIfBranch = holdHtml.Substring(posOfIf);
                holdFirstIfBranch = holdFirstIfBranch.Substring(0, holdFirstIfBranch.IndexOf("{eIF}"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{eIF}") + 5);
            }
            else if (posOfElseIf < posOfIf
                     && posOfElseIf < posOfElse)
            {
                // readfrom the start to the next {eIF}
                holdFirstIfBranch = holdHtml.Substring(posOfElseIf);
                holdFirstIfBranch = holdFirstIfBranch.Substring(0, holdFirstIfBranch.IndexOf("{eIF}"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{eIF}") + 5);
            }
            else if (posOfElse < posOfElseIf
                     && posOfElse < posOfIf)
            {
                // readfrom the start to the {eIF}
                holdFirstIfBranch = holdHtml.Substring(posOfElse);
                holdFirstIfBranch = holdFirstIfBranch.Substring(0, holdFirstIfBranch.IndexOf("{eIF}"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{eIF}") + 5);
            }

            inHtml = holdHtml;
            return holdFirstIfBranch;
        }

        public static List<IfBlock> LoadIfBlockList(string inHtml,
            ref Tuple<string, Task<ContentCollection>> vContentCollection)
        {
            var holdHtml = inHtml;
            var ifBlockList = new List<IfBlock>();
            while (holdHtml.Contains("{if:") || holdHtml.Contains("{elseif:") || holdHtml.Contains("{else}"))
            {
                var holdIfBlock = new IfBlock(GetFirstIfStatementTrimOffRest(ref holdHtml), ref vContentCollection);
                ifBlockList.Add(holdIfBlock);
            }

            return ifBlockList;
        }

        public static string ResolveIfBlockList(ref List<IfBlock> holdIfBlockList, ref ContentPiece inContentPiece)
        {
            if (holdIfBlockList.Count == 0)
            {
                return "";
            }

            foreach (var item in holdIfBlockList)
            {
                if (item.Evaluate(ref inContentPiece))
                {
                    return item.IfTrueThisText;
                }
            }

            return "";
        }
    }
}