﻿namespace Csp.Core.Infrastructure.UrlParse
{
    public struct ConsumerConstructorParams
    {
        public string BaseDomain;
        public string ConsumerID;
        public string companyname;
        public string address1;
        public string address2;
        public string city;
        public string zipcode;
        public string country;
        public bool is_consumer;
        public bool is_supplier;
        public string global_bin_local_path;
        public string global_bin_base_url;
        public string consumer_bin_local_path;
        public string consumer_bin_base_url;
        public string supplier_bin_local_path;
        public string supplier_bin_base_url;
        public string displayname;
        public string state;
        public string website_url;
        public string DefaultLanguage { get; set; }


        public ConsumerConstructorParams(string VBaseDomain, string vConsumerID, string Vcompanyname, string Vaddress1,
            string Vaddress2, string Vcity, string Vzipcode, string Vcountry, bool Vis_consumer, bool Vis_supplier,
            string Vglobal_bin_local_path, string Vglobal_bin_base_url, string Vconsumer_bin_local_path,
            string Vconsumer_bin_base_url, string Vsupplier_bin_local_path, string Vsupplier_bin_base_url,
            string Vdisplayname, string vState, string vWebsiteUrl, string vLanguage)
        {
            BaseDomain = VBaseDomain;
            ConsumerID = vConsumerID;
            companyname = Vcompanyname;
            address1 = Vaddress1;
            address2 = Vaddress2;
            city = Vcity;
            zipcode = Vzipcode;
            country = Vcountry;
            is_consumer = Vis_consumer;
            is_supplier = Vis_supplier;
            global_bin_local_path = Vglobal_bin_local_path;
            global_bin_base_url = Vglobal_bin_base_url;
            consumer_bin_local_path = Vconsumer_bin_local_path;
            consumer_bin_base_url = Vconsumer_bin_base_url;
            supplier_bin_local_path = Vsupplier_bin_local_path;
            supplier_bin_base_url = Vsupplier_bin_base_url;
            displayname = Vdisplayname;
            state = vState; //01/26 ltu: added state
            website_url = vWebsiteUrl;
            DefaultLanguage = vLanguage;
        }
    }
}