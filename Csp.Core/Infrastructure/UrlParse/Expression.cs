﻿using System;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class Expression : ExpressionRoot
    {
        public string fExpressionField;

        //  publication id / cat id
        public string fExpressionOperator;

        //  = <> > < >= <= 
        public string fExpressionValue;

        private string fExpressionType;

        private string GetSQLFieldName()
        {
            switch (fExpressionField)
            {
                case "s":
                    // Return "Content_Main.Supplier_Id"
                    return "c_m_1.Supplier_Id";
                case "c":
                    // Return "Consumers_Themes.Category_Id"
                    return "+=+CATEGORY+=+";
                case "ct":
                    // Return "Content_Main.Content_Types_Id"
                    return "c_m_1.Content_Types_Id";
                case "t":
                    // Return "Consumers_Themes.Themes_Id"
                    return "c_t_1.Themes_Id";
                case "cn":
                    // Return "Content.content_Id"
                    return "co_1.content_Id";
                case "cfx":
                    return "CategoryFollow";
            }
            if (fExpressionField.IndexOf("f") == 0)
            {
                // Return "$$$" + "(Upper(content_types_fields_1.fieldname) = Upper(#$#" + Me.fExpressionField.Substring(1) + "%$%) and "  'todo turn this into the field Name
                // Return "$$$" + "(Upper(c_t_f_2.FIELDNAME) = Upper(#$#" + Me.fExpressionField.Substring(1) + "%$%) and "  'todo turn this into the field Name
                return string.Format("$$$(upper(Name)=upper(#$#{0}%$%)) and ", fExpressionField.Substring(1));
            }
            return "";
        }

        public Expression(string vExpressionField, string vExpressionOperator, string vExpressionValue)
        {
            fExpressionType = "";
            fExpressionField = vExpressionField;
            // set type here if a real field is passed, change 
            fExpressionOperator = vExpressionOperator;
            fExpressionValue = "#$#" + vExpressionValue + "%$%";
            if (fExpressionField.IndexOf("f") == 0)
            {
                var type = fExpressionField.Substring(fExpressionField.IndexOf("%") + 1);
                fExpressionType = type;
                fExpressionField = fExpressionField.Substring(0, fExpressionField.IndexOf("%"));
            }
        }

        private bool HasLikeWildCards(string vInFieldExpressionValue)
        {
            var hold = vInFieldExpressionValue;
            hold = hold.Replace("#$#", "");
            hold = hold.Replace("%$%", "");
            return hold.Contains("%") || hold.Contains("_");
        }

        public override string GenenerateSQLFromExpression()
        {
            var s = GetSQLFieldName().ToUpper();
            var isCategory = s == "+=+CATEGORY+=+";

            if (fExpressionType == "")
            {
                if (!isCategory)
                {
                    s = s + " " + fExpressionOperator;
                    s = s + " " + fExpressionValue + "";
                }
                else
                {
                    s = s + " " + "in";
                    s = s + " +=+CatValue" + fExpressionValue + "+=+EndCatValue";
                }
            }
            else
            {
                // is field needs proper operator
                if (fExpressionType == "1")
                {
                    //  string
                    if (fExpressionOperator == "=")
                    {
                        // s = s + " content_fields_1.value_text like " + Me.fExpressionValue + ")"
                        // s = s + " c_f_2.value_text like " + Me.fExpressionValue + ")"
                        var hasWildCard = HasLikeWildCards(fExpressionValue);
                        if (fExpressionValue.IndexOf("NOCASE%", StringComparison.Ordinal) >= 0)
                        {
                            s = s + string.Format("(value like {0} )", fExpressionValue.Replace("NOCASE%", ""));
                            // ltu 06/1/2012
                        }
                        else if (hasWildCard)
                        {
                            s = s + string.Format("(value like {0} )", fExpressionValue);
                            // ltu 02/25
                        }
                        else
                        {
                            s = s + string.Format("(value equal {0} )", fExpressionValue);
                            // ltu 02/25
                        }
                    }
                    else
                    {
                        // s = s + " content_fields_1.value_text " + Me.fExpressionOperator + " '" + Me.fExpressionValue + "')"
                        // s = s + " c_f_2.value_text " + Me.fExpressionOperator + " '" + Me.fExpressionValue + "')"
                        s = s + string.Format("(value {0} \'{1}\' )", fExpressionOperator, fExpressionValue);
                        // ltu 02/25
                    }
                }
                else if (fExpressionType == "2")
                {
                    //  int?
                    // s = s + " content_fields_1.value_integer " + Me.fExpressionOperator + " " + Me.fExpressionValue + ")"
                    // s = s + " c_f_2.value_integer " + Me.fExpressionOperator + " " + Me.fExpressionValue + ")"
                    s = s + string.Format("(value {0} {1} )", fExpressionOperator, fExpressionValue);
                    // ltu 02/25
                }
            }

            return s;
        }
    }
}