﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ConsumerTypeCreator
    {
        public string Name { get; set; }
        public List<PropertyInfo> PropertyList;

        public ConsumerTypeCreator(string name)
        {
            Name = name;
            PropertyList = new List<PropertyInfo>();
        }

        public void BuildConstructor(Type baseType, TypeBuilder myTypeBuilder)
        {
            Type[] ctorParams = {new ConsumerConstructorParams().GetType()};
            var bc = baseType.GetConstructors()[0];
            var cb = myTypeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, ctorParams);
            var ctorIl = cb.GetILGenerator();
            if (bc!= null)
            {
                ctorIl.Emit(OpCodes.Ldarg_0);
                ctorIl.Emit(OpCodes.Ldarg_1);
                ctorIl.Emit(OpCodes.Call, bc);
                ctorIl.Emit(OpCodes.Ret);
            }
        }

        public Type BuildDynamicTypeWithProperties(Type baseClass)
        {
            var myDomain = Thread.GetDomain();
            var myAsmName = new AssemblyName {Name = "CustomDynamicAssembly"};

            var myAsmBuilder = myDomain.DefineDynamicAssembly(myAsmName, AssemblyBuilderAccess.Run);
            var myModBuilder = myAsmBuilder.DefineDynamicModule(myAsmName.Name);

            var myTypeBuilder = myModBuilder.DefineType(Name, TypeAttributes.Public);

            myTypeBuilder.SetParent(baseClass);
            BuildConstructor(baseClass, myTypeBuilder);

            var getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

            foreach (var prop in PropertyList)
            {
                var privateFieldBldr =
                    myTypeBuilder.DefineField("m_" + prop.Name, prop.TypeInfo, FieldAttributes.Private);

                var custNamePropBldr =
                    myTypeBuilder.DefineProperty(prop.Name, PropertyAttributes.HasDefault, prop.TypeInfo, null);

                var custNameGetPropMthdBldr =
                    myTypeBuilder.DefineMethod("get_" + prop.Name, getSetAttr, prop.TypeInfo, Type.EmptyTypes);

                var custNameGetIl = custNameGetPropMthdBldr.GetILGenerator();
                custNameGetIl.Emit(OpCodes.Ldarg_0);
                custNameGetIl.Emit(OpCodes.Ldfld, privateFieldBldr);
                custNameGetIl.Emit(OpCodes.Ret);

                var custNameSetPropMthdBldr = myTypeBuilder.DefineMethod("set_" + prop.Name, getSetAttr,null, new[]{prop.TypeInfo});
                var custNameSetIl = custNameSetPropMthdBldr.GetILGenerator();
                custNameSetIl.Emit(OpCodes.Ldarg_0);
                custNameSetIl.Emit(OpCodes.Ldarg_1);
                custNameSetIl.Emit(OpCodes.Stfld, privateFieldBldr);
                custNameSetIl.Emit(OpCodes.Ret);

                custNamePropBldr.SetGetMethod(custNameGetPropMthdBldr);
                custNamePropBldr.SetSetMethod(custNameSetPropMthdBldr);
            }

            return myTypeBuilder.CreateType();
        }

        public static ConsumerBaseType GetCustomConsumerInstanceFromTypewPropValuePairList(Type curType,
            ref List<PropValuePair> curPropValuePairList, ref ConsumerConstructorParams vConsumerConstructorParams)
        {
            ConsumerBaseType consumerInstance = (ConsumerBaseType) Activator.CreateInstance(curType, vConsumerConstructorParams);
            //var consumerInstance = new ConsumerBaseType(vConsumerConstructorParams);

            foreach (var item in curPropValuePairList)
            {
                SetProperty(consumerInstance, item.fPropertyInfo.Name, item.fPropertyValue);
            }
            return consumerInstance;
        }

        public static void SetProperty(object o, string name, object value)
        {
            try
            {
                o.GetType().InvokeMember(name, (BindingFlags.SetProperty | BindingFlags.SetField), null, o, new[]
                {
                    value
                });

            }
            catch (Exception e)
            {
            }
        }

        public static object GetProperty(object o, string name)
        {
            try
            {
                return o.GetType().InvokeMember(name, BindingFlags.GetProperty, null, o, null);
            }
            catch (MissingMethodException exception)
            {
                var mytype = o.GetType();
                var myPropertyInfos = mytype.GetProperties();
                foreach (var prop in myPropertyInfos)
                {
                    if (prop.Name.ToUpper() == name.ToUpper())
                    {
                        try
                        {
                            return o.GetType().InvokeMember(prop.Name, BindingFlags.GetProperty, null, o, null);
                        }
                        catch (Exception subex)
                        {
                            return null;
                        }
                    }
                }
                return null;
            }
        }

        public static string GetPropertyAsSafeString(object o, string name)
        {
            var holdValue = GetProperty(o, name.Replace(" ", "_"));
            if (holdValue == null)
            {
                return "";
            }
            return holdValue.ToString();
        }

        
    }
}