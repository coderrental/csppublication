﻿using System.Data.SqlClient;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class HTMLRecord
    {
        private string fConnectionString;
        private SqlConnection fSQLConnection;

        public HTMLRecord(string vConnectionString, string vURL, string vLogType, string vHTMLResult)
        {
            fConnectionString = vConnectionString;
            fSQLConnection = new SqlConnection(fConnectionString);
            fSQLConnection.Open();
            const string cmdText = @"INSERT INTO Record_HTMLPages (URL, LogType, HTMLResult, URLHash)
                                    VALUES (@URL,
                                            @LogType,
                                            @HTMLResult,
                                            HASHBYTES('SHA1', convert(nVarchar(4000), @URL)))";
            var command = new SqlCommand(cmdText) {Connection = fSQLConnection};

            command.Parameters.Add(new SqlParameter("@URL", vURL));
            command.Parameters.Add(new SqlParameter("@LogType", vLogType));
            command.Parameters.Add(new SqlParameter("@HTMLResult", vHTMLResult));
            command.ExecuteNonQuery();
            fSQLConnection.Close();
        }
    }
}
