﻿using System.Collections;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ExpressionChain : ExpressionLinkOperator
    {
        //  Same as expression link operator, adds () around the sql result
        private ArrayList fInnerExpressionLinks;

        public ExpressionChain(ref ExpressionRoot vLeftSide, ref ExpressionRoot vRightSide, string vOperatorType) :
            base(ref vLeftSide,ref vRightSide, vOperatorType)
        {
        }

        public override string GenenerateSQLFromExpression()
        {
            //  should insert () around result? 
            var innerText = "";
            if (fLeftSide != null)
            {
                innerText = "(" + fLeftSide.GenenerateSQLFromExpression() + ")";
                if (HasRightSide)
                {
                    var RightSide = fRightSide.GenenerateSQLFromExpression();
                    if (RightSide.IndexOf("$$$") != -1 && innerText.IndexOf("$$$") == -1)
                    {
                        //  if the right side and the left side aren't both field filters then this needs to be appended
                        innerText = innerText + " $$$ " + "^^^" + GetSQLOperator() + "^&^" + " " + RightSide;
                    }
                    else
                    {
                        //  if they aren't different then they don't need modification. 
                        innerText = innerText + " " + GetSQLOperator() + " " + RightSide;
                    }
                }
            }
            return innerText;
        }
    }
}