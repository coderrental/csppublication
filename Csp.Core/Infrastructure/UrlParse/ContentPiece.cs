﻿using System.Collections;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class ContentPiece
    {
        public ConsumerBaseType fRelatedSupplierInfo;
        public List<string> fCategoriesImAssignedTo;
        public bool HasActiveCategories = true;
        public string CTMainContentIdentifier { get; set; }
        public string CTMainID { get; set; }
        public string CTMainSupplierID { get; set; }
        public string CTMainConsumerID { get; set; }
        public ArrayList fCTCategories; //of string??
        public string ThemeID { get; set; }
        public string ContentTypeID { get; set; }
        public string ContentInstanceID { get; set; }
        public string ContentMainKey { get; set; }
        public string CTLanguageID { get; set; }
        public string CTLanguageCode { get; set; }
        public string CTLanguageDescription { get; set; }
        public int ContentCategoryID { get; set; }
        public int ContentCategoryDepth { get; set; }
        public int ContentCategoryParentID { get; set; }
        public string ContentCategoryName { get; set; }
        public bool ContentCategoryHasChildren { get; set; }
        public int ContentCategoryChildDepth { get; set; }
        public int NextAvailableContentCategoryChildId { get; set; }
        public string ContentCategoryLineage { get; set; }

        public ContentPiece(ContentPieceParameters vInParam)
        {
            if (vInParam != null)
            {
                CTMainContentIdentifier = vInParam.fCTMainContentIdentifier;
                CTMainID = vInParam.fCTMainID;
                CTMainSupplierID = vInParam.fCTMainSupplierID;
                CTMainConsumerID = vInParam.fCTMainConsumerID;
                fCTCategories = vInParam.fCTCategories;
                ThemeID = vInParam.fThemeID;
                ContentTypeID = vInParam.fContentTypeID;
                ContentInstanceID = vInParam.fContentInstanceID;
                ContentMainKey = vInParam.fContentMainKey;
                fRelatedSupplierInfo = null;
                CTLanguageID = vInParam.fCTLanguageID;
                CTLanguageCode = vInParam.fCTLanguageCode;
                CTLanguageDescription = vInParam.fCTLanguageDescription;
            }
            ContentCategoryDepth = -1;
            ContentCategoryID = -1;
            ContentCategoryName = "";
            ContentCategoryParentID = -1;
        }
    }
}