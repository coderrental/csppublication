﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class OnChangeSupportObj
    {
        public string HTML;

        public string FieldName;

        public string LastValue;

        public string OnChangeID;

        public OnChangeSupportObj(string inHtml, string inFieldName, string inOnChangeId)
        {
            HTML = inHtml;
            FieldName = inFieldName;
            OnChangeID = inOnChangeId;
            LastValue = "(#@&$(@#&$(@#$&(@&#$(*@#&$(@&#%*^&";
        }

        public bool ShouldBeRenderedThisTime(string inValue)
        {
            if (inValue.ToUpper() != LastValue.ToUpper())
            {
                LastValue = inValue;
                return true;
            }
            return false;
        }

        public static string GetHTMLForOnChangeIDUsingContentPiece(string onChangeId,
            ref List<OnChangeSupportObj> onChangeObjList, ref ContentPiece curContentPiece)
        {
            //  will iterate through OnChangeObjList, looking for OnChangeID, return "" 
            //  if the last value is = the cur value of CurContentPiece.Field.Value
            foreach (var item in onChangeObjList)
            {
                if (item.OnChangeID == onChangeId)
                {
                    var holdCurValueOfField = ContentPieceTypeCreator.GetPropertyAsSafeString(curContentPiece, item.FieldName);
                    if (item.ShouldBeRenderedThisTime(holdCurValueOfField))
                    {
                        return item.HTML;
                    }
                    return "";
                }
            }

            return "";
        }
    }
}