﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Caching;
using System.Xml;
using UrlParserToJson;

// BIG NOTE HERE

namespace Csp.Core.Infrastructure.UrlParse
{
    public class DCMXMLExport //DCMXMLExportCode.vb
    {
        public string XMLResult;

        public DCMXMLExport(LoggingInfo inLoggingInfo, string connString, string urlParams, string baseDomain,
            Cache theCache, bool exportMetaData, string lngCode)
        {
            var holdLogger = inLoggingInfo;
            var sqlConnection = new SqlConnection(connString);

            List<URLNameValuePair> t1 = null;
            CategoryLoaderParams t2 = null;


            var hold = URLEngine.GetOrCreateURLEngineFromCache(urlParams, connString, baseDomain, ref t1, ref holdLogger,
                true, true, t2, ref theCache, lngCode,
                null, null, null, null);
            var holdStream = new MemoryStream();
            var xWrite = new XmlTextWriter(holdStream, null);
            xWrite.Formatting = Formatting.Indented;
            xWrite.WriteStartDocument();
            xWrite.WriteStartElement("Document");

            var holdFirstCollection = hold.MyContentCollectionTasks[0].Item2.Result;

            var a = new ContentCollectionToJson();
            var b = holdFirstCollection.fMyContent.Cast<object>().ToList();

            XMLResult = a.GetJson(b);
        }

        //2 more private function without be called
    }
}
