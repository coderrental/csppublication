﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Caching;
using System.Web.Security;
using Phydeaux.Utilities;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class URLEngine
    {
        public static int CacheTime = 480;

        public CategoryLoaderParams CategoryLoaderParams = null;

        public CategoryLoader CategoryTreesLoaded = null;

        private string fBaseDomainConsumerID = "";

        private string fConnectionString;

        public string fDomain;

        private string FieldValueConcat = "";

        //  for logging
        private string fLoggingCode = "";

        public LoggingInfo fLoggingInfo;

        private string fPageID = "";

        private SqlConnection fSQLConnection;

        private string fURLExpression;

        public string LanguageCode = "";

        public List<Tuple<string, Task<ContentCollection>>> MyContentCollectionTasks =
            new List<Tuple<string, Task<ContentCollection>>>();

        public List<ConsumerTypeType> MyDynamicConsumerSupplierTypes = new List<ConsumerTypeType>();

        public List<ContentTypeType> MyDynamicContentTypes = new List<ContentTypeType>();

        public List<ConsumerBaseType> MyRelatedConsumerSupplierData = new List<ConsumerBaseType>();

        private List<Guid> SpecificContentIDs;

        public SemaphoreSlim ss = new SemaphoreSlim(1);

        private Cache TheCache = null;

        public URLEngine(string URLExpression, string vConnectionString, string Domain,
            ref List<URLNameValuePair> SearchNameValuePairList, ref LoggingInfo vLoggingInfo, bool WriteLogging,
            bool GetCategoriesOnContentCollections, CategoryLoaderParams vCategoryLoaderParams,
            ref Cache InURLCache, string InLanguageCode,
            string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategory, CategoryItem cleanTree, List<SupplierCatTree> supplierTree)
        {
            LanguageCode = LanguageCode;
            URLExpression = StripCategoryFromURL(URLExpression);
            CategoryLoaderParams = vCategoryLoaderParams;
            TheCache = InURLCache;
            fDomain = Domain;
            fURLExpression = URLExpression;
            fLoggingInfo = vLoggingInfo;
            LanguageCode = InLanguageCode;
            URLResolver tURLResolver;
            try
            {
                tURLResolver = GetURLResolver(fURLExpression, ref SearchNameValuePairList);
                tURLResolver.LanguageCode = LanguageCode;
            }
            catch (Exception ex)
            {
                var s = "Error Processing URL: " + fURLExpression + " Error: " + ex.Message;
                if (!(ex.InnerException == null))
                {
                    s = s + " Inner: " + ex.InnerException.Message;
                }

                throw new Exception(s);
            }

            var fUrlEngine = this;
            SetLoggingFromURLResolverOnURLEngine(ref fUrlEngine, ref tURLResolver);
            if (vCategoryLoaderParams != null)
            {
                CategoryTreesLoaded = new CategoryLoader(vCategoryLoaderParams.RootCategory,
                    vCategoryLoaderParams.Depth, vCategoryLoaderParams.ConsumerID, vCategoryLoaderParams.Domain,
                    vCategoryLoaderParams.SupplierIDs, vCategoryLoaderParams.ContentCategoryTypeID,
                    vCategoryLoaderParams.SortingParams, vCategoryLoaderParams.FieldSearchParams, vCategoryLoaderParams.URLEngineConnString, TheCache,
                    rootCategoryId,flatListOfAllCleanCategory,cleanTree,supplierTree);
            }

            if (vConnectionString.ToLower().Contains("max pool size=100"))
            {
                // (vConnectionString.ToLower().Contains("asynchronous processing=true")) Then
            }
            else
            {
                // vConnectionString = vConnectionString + "; Asynchronous Processing=true"
                vConnectionString = vConnectionString + "; Max Pool Size=100";
            }

            fConnectionString = vConnectionString;
            // "Server=62.58.16.243;user=dcm_dev_user;pwd=parrot122;database=dcm"
            fSQLConnection = new SqlConnection(fConnectionString);
            //   testAsync(tURLResolver, False).Wait()
            //  testAsync(tURLResolver, True).Wait()
            TestParallel(tURLResolver, fConnectionString, fDomain, LanguageCode, this, ref InURLCache);
            // Me.WriteLoging() move down to access more data
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            // fSQLConnection.Open()
            // For index As Integer = 0 To tURLResolver.ChainCount() - 1
            //     Dim ChainFilterText As String
            //     ChainFilterText = tURLResolver.GetMainChainText(index)
            //     Dim HoldFieldFilter As String = tURLResolver.GetFieldFilterFromMainChain(ChainFilterText)
            //     Dim HoldOtherThanFieldFilter As String = tURLResolver.GetOtherThanFieldFilterFromMainChain(ChainFilterText)
            //     ' may have the category flag +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue
            //     Dim HoldCategorySearchListAndCleanOtherFilterHelper As CategorySearchListAndCleanOtherFilterHelper
            //     HoldCategorySearchListAndCleanOtherFilterHelper = Me.ReplaceCategoryFilter(HoldOtherThanFieldFilter, fSQLConnection)
            //     HoldOtherThanFieldFilter = HoldCategorySearchListAndCleanOtherFilterHelper.CleanOtherFieldFilter
            //     ' need to do something to capture the category parameter(s) into a list run the next statement
            //     ' need to preserve the location inside the query string logic
            //     Dim HoldSQLParams As List(Of SqlClient.SqlParameter) = tURLResolver.ConvertValuesIntoParametersArray(HoldFieldFilter, HoldOtherThanFieldFilter)
            //     'parameter replacement should probably ignore the SQL category parameters
            //     'ltu
            //     'add a check to ensure if we get nothing in return from using the default language, we can use the fallback language
            //     Dim command As SqlClient.SqlCommand = Nothing
            //     Dim start As DateTime = DateTime.Now
            //     ' maybe pass the extra category header sql so it will be appeneded to the top
            //     command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, fDomain, HoldSQLParams, LanguageCode, True, fSQLConnection, HoldCategorySearchListAndCleanOtherFilterHelper)
            //     command.Connection = fSQLConnection
            //     command.CommandTimeout = 150
            //     Dim reader As SqlClient.SqlDataReader = Nothing
            //     Try
            //         reader = command.ExecuteReader()
            //         If (Not reader.HasRows) Then
            //             'if no result set try fall back language
            //             reader.Close()
            //             command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, fDomain, HoldSQLParams, LanguageCode, False, fSQLConnection, HoldCategorySearchListAndCleanOtherFilterHelper)
            //             command.Connection = fSQLConnection
            //             reader = command.ExecuteReader()
            //         End If
            //     Catch ex As Exception
            //         If Not reader Is Nothing Then
            //             reader.Close()
            //         End If
            //         Throw New Exception(String.Format("Error [{0}] Command [{1}]", ex.Message, command.CommandText))
            //     Finally
            //         'nothing
            //     End Try
            //     Dim ts As TimeSpan = DateTime.Now.Subtract(start)
            //     Dim CleanFilter As String = ChainFilterText
            //     CleanFilter = CleanFilter.Replace("#$#", "")
            //     CleanFilter = CleanFilter.Replace("%$%", "")
            //     Dim CurContentCollection As ContentCollection = New ContentCollection(CleanFilter, Domain, tURLResolver.fExpressionPublications.Item(index), tURLResolver.fSortParamsList.Item(index), tURLResolver.fPageParamsList.Item(index), tURLResolver.fRawURL(index), fSQLConnection)
            //     If Not IsNothing(Me.CategoryTreesLoaded) Then
            //         CurContentCollection.fMyCategories = Me.CategoryTreesLoaded
            //     End If
            //     Try
            //         If True Then
            //             Dim i As Integer = 0
            //             'ListBox1.Items.Add("Pre Read Records" + DateTime.Now.ToString())
            //             Dim LastContentInstanceID As String = ""
            //             Dim CTMainKey As String
            //             Dim ContentInstanceID As String
            //             Dim CategoryId As Integer = -1
            //             Dim LastCategoryID As Integer = -1
            //             Dim CategoryName As String = ""
            //             Dim Depth As Integer = -1
            //             Dim ParentID As Integer = -1
            //             Dim ContentType As String
            //             Dim CTCat As ArrayList = Nothing ' todo! fCTCategories
            //             Dim CTMainConsumerID As String
            //             Dim CTMainContentIdentifier As String
            //             Dim CTThemeID As String = "0" ' todo!
            //             Dim CTMainID As String
            //             Dim CTMainSupplierID As String
            //             Dim CTLanguageID As String = ""
            //             'ltu: 8/27 language code for reporting
            //             Dim CTLanguageCode As String = ""
            //             Dim CTLanguageDescription As String = ""
            //             'ltu: 8/31
            //             Dim CCHasChildren As Boolean = False
            //             Dim CCChildDepth As Integer = -1
            //             Dim NextAvailableCCId As Integer = -1
            //             Dim tmpCategoryNode As CategoryHuntNode
            //             'ltu: 11/15 lineage
            //             Dim lineage As String = ""
            //             Dim HasCategoryInResults As Boolean = False
            //             If HoldCategorySearchListAndCleanOtherFilterHelper.CategorySearchHelpers.Count > 0 Then
            //                 HasCategoryInResults = True ' that way we know to check for category id so we can append it to the default types and create new content pieces when the content guid doesn't increment but the category does. 
            //             End If
            //             Dim CurPropValuePairList As List(Of PropValuePair) = New List(Of PropValuePair)
            //             Dim CurContentPiece As ContentPiece
            //             Dim CurType As Type
            //             Dim FoundAnything As Boolean = False
            //             While reader.Read()
            //                 If (ContentInstanceID = LastContentInstanceID And CategoryId <> LastCategoryID) Then
            //                     Dim imhere = True
            //                 End If
            //                 FoundAnything = True
            //                 ContentInstanceID = reader.Item("content_Id").ToString ' content instance ID
            //                 If HasCategoryInResults Then
            //                     CategoryId = Integer.Parse(reader.Item("category_Id").ToString())
            //                 End If
            //                 If ContentInstanceID <> LastContentInstanceID And LastContentInstanceID = "" Then
            //                     ' first instance!
            //                     If HasCategoryInResults Then
            //                         CategoryId = Integer.Parse(reader.Item("category_Id").ToString())
            //                         CategoryName = reader.Item("CategoryName").ToString
            //                         Depth = Integer.Parse(reader.Item("Depth").ToString)
            //                         ParentID = Integer.Parse(reader.Item("ParentID").ToString)
            //                         lineage = reader.Item("Lineage")
            //                         'ltu: 08/31 category hunt routine
            //                         If Not HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter Is Nothing Then
            //                             'tmpCategoryNode = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(Function(n As CategoryHuntNode) (n.ContentId.Equals(ContentInstanceID)))
            //                             tmpCategoryNode = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(Function(n As CategoryHuntNode) (n.ContentId.ToString().Equals(ContentInstanceID)))
            //                             If (Not tmpCategoryNode Is Nothing) Then
            //                                 CCHasChildren = tmpCategoryNode.HasChildren
            //                                 CCChildDepth = tmpCategoryNode.Depth
            //                                 If (tmpCategoryNode.Children.Count > 0) Then
            //                                     NextAvailableCCId = tmpCategoryNode.Children.Item(0).ParentCategoryId
            //                                 Else
            //                                     NextAvailableCCId = -1
            //                                 End If
            //                             Else
            //                                 CCHasChildren = False
            //                                 NextAvailableCCId = -1
            //                                 CCChildDepth = -1
            //                             End If
            //                         End If
            //                     End If
            //                     CurPropValuePairList.Clear()
            //                     CTMainKey = reader.Item("content_main_key").ToString 'fContentMainKey
            //                     ContentType = reader.Item("content_types_Id").ToString 'fcontentTypeID
            //                     CTMainConsumerID = reader.Item("consumer_Id").ToString ' fCTMainConsumerID
            //                     CTMainContentIdentifier = reader.Item("content_identifier").ToString 'fCTMainContentIdentifier
            //                     CTMainID = reader.Item("content_main_id").ToString 'fCTMainID
            //                     CTMainSupplierID = reader.Item("supplier_Id").ToString 'fCTMainSupplierID
            //                     CTLanguageID = reader.Item("content_types_languages_Id").ToString
            //                     'ltu 8/27
            //                     CTLanguageCode = reader.Item("languagecode").ToString()
            //                     CTLanguageDescription = reader.Item("description").ToString() 'todo: might need to change the column name to language_description to avoid conflict.
            //                     LastContentInstanceID = ContentInstanceID
            //                     If HasCategoryInResults Then
            //                         LastCategoryID = CategoryId
            //                     End If
            //                     CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader, reader.Item("fieldname").ToString, reader.Item("fieldtype")))
            //                 ElseIf (ContentInstanceID <> LastContentInstanceID) Or (ContentInstanceID = LastContentInstanceID And CategoryId <> LastCategoryID) Then
            //                     ' At new ContentMain, need to create prior CTMain object instance
            //                     ' First get the actual underlying type
            //                     Me.DoubleCheckandFillPropValuePairList(MyDynamicContentTypes, CurPropValuePairList, ContentType)
            //                     CurType = ContentTypeType.GetTypeByContentTypewPropValuePairList(MyDynamicContentTypes, ContentType, CurPropValuePairList).fType
            //                     ' create an instance with values
            //                     'CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList,  New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, ContentInstanceID, CTMainKey))
            //                     CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList, New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, LastContentInstanceID, CTMainKey, CTLanguageID, CTLanguageCode, CTLanguageDescription, CCHasChildren, CCChildDepth, NextAvailableCCId))
            //                     If HasCategoryInResults Then
            //                         CurContentPiece.ContentCategoryID = LastCategoryID
            //                         CurContentPiece.ContentCategoryDepth = Depth
            //                         CurContentPiece.ContentCategoryName = CategoryName
            //                         CurContentPiece.ContentCategoryParentID = ParentID
            //                         CurContentPiece.ContentCategoryLineage = lineage
            //                         'ltu: 08/31 category hunt routine
            //                         If Not HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter Is Nothing Then
            //                             tmpCategoryNode = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(Function(n As CategoryHuntNode) (n.ContentId.ToString().Equals(LastContentInstanceID)))
            //                             If (Not tmpCategoryNode Is Nothing) Then
            //                                 CurContentPiece.ContentCategoryHasChildren = tmpCategoryNode.HasChildren
            //                                 CurContentPiece.ContentCategoryChildDepth = tmpCategoryNode.Depth
            //                                 If (tmpCategoryNode.Children.Count > 0) Then
            //                                     CurContentPiece.NextAvailableContentCategoryChildId = tmpCategoryNode.Children.Item(0).ParentCategoryId
            //                                 Else
            //                                     CurContentPiece.NextAvailableContentCategoryChildId = -1
            //                                 End If
            //                             Else
            //                                 CurContentPiece.ContentCategoryHasChildren = False
            //                                 CurContentPiece.ContentCategoryChildDepth = -1
            //                                 CurContentPiece.NextAvailableContentCategoryChildId = -1
            //                             End If
            //                         End If
            //                     End If
            //                     CurContentCollection.fMyContent.Add(CurContentPiece)
            //                     i = i + 1
            //                     CurPropValuePairList.Clear()
            //                     CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader, reader.Item("fieldname").ToString, reader.Item("fieldtype"))) 'add first prop
            //                     CTMainKey = reader.Item("content_main_key").ToString 'fContentMainKey
            //                     ContentType = reader.Item("content_types_Id").ToString 'fcontentTypeID
            //                     CTMainConsumerID = reader.Item("consumer_Id").ToString ' fCTMainConsumerID
            //                     CTMainContentIdentifier = reader.Item("content_identifier").ToString 'fCTMainContentIdentifier
            //                     CTMainID = reader.Item("content_main_id").ToString 'fCTMainID
            //                     CTMainSupplierID = reader.Item("supplier_Id").ToString 'fCTMainSupplierID
            //                     If HasCategoryInResults Then
            //                         CategoryId = Integer.Parse(reader.Item("category_Id").ToString())
            //                         CategoryName = reader.Item("CategoryName").ToString
            //                         Depth = Integer.Parse(reader.Item("Depth").ToString)
            //                         ParentID = Integer.Parse(reader.Item("ParentID").ToString)
            //                         lineage = reader.Item("Lineage")
            //                     End If
            //                     LastContentInstanceID = ContentInstanceID
            //                     LastCategoryID = CategoryId
            //                 Else
            //                     ' add current field as paramValue
            //                     CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader, reader.Item("fieldname").ToString, reader.Item("fieldtype")))
            //                 End If
            //             End While
            //             If FoundAnything Then
            //                 ' create last instance
            //                 Me.DoubleCheckandFillPropValuePairList(MyDynamicContentTypes, CurPropValuePairList, ContentType)
            //                 CurType = ContentTypeType.GetTypeByContentTypewPropValuePairList(MyDynamicContentTypes, ContentType, CurPropValuePairList).fType
            //                 ' create an instance with values
            //                 'CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList, New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, ContentInstanceID, CTMainKey, CTLanguageID, CTLanguageCode, CTLanguageDescription))
            //                 CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList, New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, LastContentInstanceID, CTMainKey, CTLanguageID, CTLanguageCode, CTLanguageDescription, CCHasChildren, CCChildDepth, NextAvailableCCId))
            //                 If HasCategoryInResults Then
            //                     CurContentPiece.ContentCategoryID = LastCategoryID
            //                     CurContentPiece.ContentCategoryDepth = Depth
            //                     CurContentPiece.ContentCategoryName = CategoryName
            //                     CurContentPiece.ContentCategoryParentID = ParentID
            //                     'ltu: 08/31 category hunt routine
            //                     If Not HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter Is Nothing Then
            //                         tmpCategoryNode = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(Function(n As CategoryHuntNode) (n.ContentId.ToString().Equals(LastContentInstanceID)))
            //                         If (Not tmpCategoryNode Is Nothing) Then
            //                             CurContentPiece.ContentCategoryHasChildren = tmpCategoryNode.HasChildren
            //                             CurContentPiece.ContentCategoryChildDepth = tmpCategoryNode.Depth
            //                             If (tmpCategoryNode.Children.Count > 0) Then
            //                                 CurContentPiece.NextAvailableContentCategoryChildId = tmpCategoryNode.Children.Item(0).ParentCategoryId
            //                             Else
            //                                 CurContentPiece.NextAvailableContentCategoryChildId = -1
            //                             End If
            //                         Else
            //                             CurContentPiece.ContentCategoryHasChildren = False
            //                             CurContentPiece.ContentCategoryChildDepth = -1
            //                             CurContentPiece.NextAvailableContentCategoryChildId = -1
            //                         End If
            //                     End If
            //                 End If
            //                 CurContentCollection.fMyContent.Add(CurContentPiece)
            //                 i = i + 1
            //                 Dim SortContentType As String = CurContentCollection.fSortParams.SortType
            //                 Dim SortString As String = CurContentCollection.fSortParams.SortParams
            //                 If SortString <> "" Then
            //                     Dim HoldCTT As ContentTypeType = ContentTypeType.GetContentTypeType(MyDynamicContentTypes, SortContentType)
            //                     If Not IsNothing(HoldCTT) Then
            //                         CurType = HoldCTT.fType
            //                         If Not IsNothing(CurType) Then
            //                             Try
            //                                 Dim DN As DynamicComparer(Of ContentPiece) = New DynamicComparer(Of ContentPiece)(SortString, CurType)
            //                                 CurContentCollection.fMyContent.Sort(DN.Comparer)
            //                             Catch ex As Exception
            //                                 Dim hold As ContentPiece = New ContentPiece(Nothing)
            //                                 Dim DN As DynamicComparer(Of ContentPiece) = New DynamicComparer(Of ContentPiece)(SortString, hold.GetType)
            //                                 CurContentCollection.fMyContent.Sort(DN.Comparer)
            //                             End Try
            //                         Else
            //                             Throw New Exception("Error, Content Type specified in search not found in result set. Search Type: " + SortContentType + " Sort Params: " + SortString + " URL : " + CurContentCollection.fFilterExpression)
            //                         End If
            //                     Else
            //                         Throw New Exception("Error, Content Type specified in search not found in result set. Search Type: " + SortContentType + " Sort Params: " + SortString + " URL : " + CurContentCollection.fFilterExpression)
            //                     End If
            //                 End If
            //                 Me.MyContentCollections.Add(CurContentCollection)
            //             Else
            //                 Me.MyContentCollections.Add(CurContentCollection) ' add the empty content collection
            //             End If
            //         End If
            //     Finally
            //         reader.Close()
            //         ContentCollection.LookupAndAssignSupplierDataToContentCollectionObjects(Me.fDomain, CurContentCollection, MyRelatedConsumerSupplierData, MyDynamicConsumerSupplierTypes, fSQLConnection)
            //         fBaseDomainConsumerID = CurContentCollection.fBaseDomainData.ConsumerID
            //     End Try
            // Next ' for loop
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            fSQLConnection.Open();
            if (WriteLogging)
            {
                WriteLoging();
            }

            if (GetCategoriesOnContentCollections)
            {
                GoThroughContentCollectionsAndAssignContentCategories(ref fSQLConnection);
                // at some point I'll  need to link the categories assiged to the supplier trees in the category loader, not sure which strucutre should hold the assignment
                // the supplier tree or the content collection. I have to be careful that a supplier tree doesn't get categories destined for some other publication and they get displayed on the wrong page
                if (!(CategoryTreesLoaded == null))
                {
                    //  loop through content, and the category trees, and mark those categories which don't have an active category assigned. 
                    RemoveContentWithEmptyCategories();
                }
            }

            fSQLConnection.Close();
        }

        public static string ReplaceSearchParams(ref string URLWithParams,
            ref List<URLNameValuePair> SearchNameValuePairList)
        {
            var HoldURL = URLWithParams;
            foreach (var Item in SearchNameValuePairList)
            {
                if (Item.Value != "")
                {
                    HoldURL = HoldURL.Replace("{U" + Item.Name + "}", Item.Value);
                }
            }

            if (HoldURL.Contains("{U"))
            {
                throw new Exception("Error! URL didn\'t have all search parameters replaced: " + HoldURL);
            }

            return HoldURL;
        }

        private void WriteLoging()
        {
            if (!(fLoggingInfo == null))
            {
                fLoggingInfo.fDomain = fDomain;
                fLoggingInfo.fFieldValues = FieldValueConcat;
                fLoggingInfo.fConsumerID = fBaseDomainConsumerID;
                fLoggingInfo.fPageID = fPageID;
                fLoggingInfo.fLoggingCode = fLoggingCode;
                var FoundContent = false;
                foreach (var Item in SpecificContentIDs)
                {
                    FoundContent = true;
                    fLoggingInfo.fContentID = Item;
                    fLoggingInfo.WriteLog(fSQLConnection);
                }

                if (!FoundContent)
                {
                    fLoggingInfo.WriteLog(fSQLConnection);
                }
            }
        }

        public static URLResolver GetURLResolver(string URLExpression,
            ref List<URLNameValuePair> SearchNameValuePairList)
        {
            URLExpression = StripCategoryFromURL(URLExpression);
            if (!(SearchNameValuePairList == null))
            {
                //  run find replace on URLExpression
                URLExpression = ReplaceSearchParams(ref URLExpression, ref SearchNameValuePairList);
            }

            return new URLResolver(URLExpression);
        }

        public static void SetLoggingFromURLResolverOnURLEngine(ref URLEngine fURLEngine, ref URLResolver URLResolver)
        {
            fURLEngine.SpecificContentIDs = URLResolver.GetListOfContentIDsRequested();
            //  for logging
            fURLEngine.FieldValueConcat = URLResolver.GetFieldValueConcatList();
            //  for logging
            fURLEngine.fPageID = URLResolver.GetPageID();
            //  for logging
            fURLEngine.fLoggingCode = URLResolver.LoggingCode;
            //  for logging
        }

        private static string StripFilterParmsFromRawURL(string InRawURL)
        {
            if (InRawURL.Contains("["))
            {
                return InRawURL.Substring(0, InRawURL.IndexOf("["));
            }
            else
            {
                return InRawURL;
            }
        }

        private static bool HasSameCategoryParams(ref URLEngine CachedURLEngine,
            ref CategoryLoaderParams InCategoryParams)
        {
            if (CachedURLEngine.CategoryLoaderParams == null
                && InCategoryParams == null)
            {
                return true;
            }
            else if (CachedURLEngine.CategoryLoaderParams == null
                     && !(InCategoryParams == null)
                     || !(CachedURLEngine.CategoryLoaderParams == null)
                     && InCategoryParams == null)
            {
                return false;
            }
            else if (CachedURLEngine.CategoryLoaderParams.ConsumerID == InCategoryParams.ConsumerID &&
                     InCategoryParams.ContentCategoryTypeID ==
                     CachedURLEngine.CategoryLoaderParams.ContentCategoryTypeID &&
                     CachedURLEngine.CategoryLoaderParams.Depth == InCategoryParams.Depth &&
                     CachedURLEngine.CategoryLoaderParams.Domain == InCategoryParams.Domain &&
                     CachedURLEngine.CategoryLoaderParams.FieldSearchParams == InCategoryParams.FieldSearchParams &&
                     CachedURLEngine.CategoryLoaderParams.RootCategory == InCategoryParams.RootCategory &&
                     CachedURLEngine.CategoryLoaderParams.SortingParams == InCategoryParams.SortingParams &&
                     CachedURLEngine.CategoryLoaderParams.URLEngineConnString == InCategoryParams.URLEngineConnString &&
                     CachedURLEngine.CategoryLoaderParams.SupplierIDs.Count == InCategoryParams.SupplierIDs.Count)
            {
                var Allmatched = true;
                var i = 0;
                foreach (var item in CachedURLEngine.CategoryLoaderParams.SupplierIDs)
                {
                    if (CachedURLEngine.CategoryLoaderParams.SupplierIDs[i] != item)
                    {
                        Allmatched = false;
                    }
                }

                if (Allmatched == true)
                {
                    return true;
                }
            }
        else

        {
                return false;
            }

            return false;
        }

        private static bool IsRightEngine(ref URLResolver InURLResolver, ref URLEngine CachedURLEngine, string InDomain)
        {
            var IsSame = false;
            if (InDomain == CachedURLEngine.fDomain)
            {
                try
                {
                    // same domain
                    for (var index = 0;
                        index
                        <= InURLResolver.fRawURL.Count - 1;
                        index++)
                    {
                        // If Not IsNothing(CachedURLEngine.MyContentCollections.Item(index)) Then
                        if (!(CachedURLEngine.MyContentCollectionTasks[index].Item2.Result == null))
                        {
                            // same number of Content Collections
                            // If StripFilterParmsFromRawURL(CachedURLEngine.MyContentCollections.Item(index).fRawURL) = StripFilterParmsFromRawURL(InURLResolver.fRawURL.Item(index)) Then
                            if (StripFilterParmsFromRawURL(CachedURLEngine.MyContentCollectionTasks[index].Item2.Result
                                    .fRawURL) == StripFilterParmsFromRawURL(InURLResolver.fRawURL[index]))
                            {
                                //  same raw url without paging, sorting, or logging
                                // If InURLResolver.fSortParamsList.Item(index).SortParams = CachedURLEngine.MyContentCollections.Item(index).fSortParams.SortParams And InURLResolver.fSortParamsList.Item(index).SortType = CachedURLEngine.MyContentCollections.Item(index).fSortParams.SortType Then
                                if (InURLResolver.fSortParamsList[index].SortParam == CachedURLEngine
                                        .MyContentCollectionTasks[index].Item2.Result.fSortParams.SortParam
                                    && InURLResolver.fSortParamsList[index].SortType == CachedURLEngine
                                        .MyContentCollectionTasks[index].Item2.Result.fSortParams.SortType)
                                {
                                    // same component, same sorting, same domain, same component order
                                    if (InURLResolver.LanguageCode == CachedURLEngine.LanguageCode)
                                    {
                                        IsSame = true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    //  different sorting
                                    return false;
                                }
                            }
                            else
                            {
                                //  different raw URL params or component
                                return false;
                            }
                        }
                        else
                        {
                            // not the same number of components loaded
                            return false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            if (IsSame == true)
            {
                //  passed all checks on all components!
                return true;
            }
            else
            {
                return false;
            }
        }

        public static URLEngine AddNewEngineToCache(string URLExpression, string vConnectionString, string Domain,
            ref List<URLNameValuePair> SearchNameValuePairList, ref LoggingInfo vLoggingInfo, bool WriteLogging,
            bool GetCategoriesOnContentCollections, CategoryLoaderParams vCategoryLoaderParams, ref Cache TheCache,
            string LanguageCode,
            string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategory, CategoryItem cleanTree, List<SupplierCatTree> supplierTree, int cacheTime = 480)
        {
            var CurEngine = new URLEngine(URLExpression, vConnectionString, Domain, ref SearchNameValuePairList,
                ref vLoggingInfo, WriteLogging, GetCategoriesOnContentCollections, vCategoryLoaderParams,
                ref TheCache, LanguageCode,
                rootCategoryId,flatListOfAllCleanCategory, cleanTree,supplierTree);
            return CurEngine;
        }

        private static void SetPaging(ref URLResolver InURLResolver, ref URLEngine InURLEngine)
        {
            for (var index = 0;
                index
                <= InURLResolver.fPageParamsList.Count - 1;
                index++)
            {
                // InURLEngine.MyContentCollections.Item(index).fPageParams.CountPerPage = InURLResolver.fPageParamsList.Item(index).CountPerPage
                // InURLEngine.MyContentCollections.Item(index).fPageParams.CurrentPageOffset = InURLResolver.fPageParamsList.Item(index).CurrentPageOffset
                // InURLEngine.MyContentCollections.Item(index).fRawURL = InURLResolver.fRawURL.Item(index)
                InURLEngine.MyContentCollectionTasks[index].Item2.Result.fPageParams.CountPerPage =
                    InURLResolver.fPageParamsList[index].CountPerPage;
                InURLEngine.MyContentCollectionTasks[index].Item2.Result.fPageParams.CurrentPageOffset =
                    InURLResolver.fPageParamsList[index].CurrentPageOffset;
                InURLEngine.MyContentCollectionTasks[index].Item2.Result.fRawURL = InURLResolver.fRawURL[index];
            }

            InURLEngine.fURLExpression = InURLResolver.FullInURL;
        }

        public static URLEngine GetOrCreateURLEngineFromCache(string URLExpression, string vConnectionString,
            string Domain, ref List<URLNameValuePair> SearchNameValuePairList, ref LoggingInfo vLoggingInfo,
            bool WriteLogging, bool GetCategoriesOnContentCollections, CategoryLoaderParams vCategoryLoaderParams,
            ref Cache TheCache, string LanguageCode,
            string rootCategoryId = null, List<CategoryItem> flatListOfAllCleanCategory = null, CategoryItem cleanTree = null, List<SupplierCatTree> supplierTree = null)
        {
            if (!(TheCache == null))
            {
                URLEngine CurEngine = null;
                var CacheEnum = TheCache.GetEnumerator();
                if (!(CacheEnum == null))
                {
                    var HoldURLResolver = GetURLResolver(URLExpression, ref SearchNameValuePairList);
                    HoldURLResolver.LanguageCode = LanguageCode;
                    // Domain = Domain & "/dcm"
                    var TheDomain = Domain;
                    var CurKey = "";
                    while (CacheEnum.MoveNext())
                    {
                        if (CacheEnum.Key != null) CurKey = CacheEnum.Key.ToString();
                        if (CurKey.StartsWith("URLENGINE"))
                        {
                            CurEngine = (URLEngine) CacheEnum.Value;
                            if (IsRightEngine(ref HoldURLResolver, ref CurEngine, Domain) == true
                                && HasSameCategoryParams(ref CurEngine, ref vCategoryLoaderParams) == true)
                            {
                                //  got the right one!
                                SetPaging(ref HoldURLResolver, ref CurEngine);
                                CurEngine.fLoggingInfo = vLoggingInfo;
                                SetLoggingFromURLResolverOnURLEngine(ref CurEngine, ref HoldURLResolver);
                                if (WriteLogging)
                                {
                                    CurEngine.WriteLoging();
                                }

                                return CurEngine;
                            }
                        }
                    }

                    //  at this point the Right URL engine wasn't found create one and add it to the cache
                    // CurEngine = New URLEngine(URLExpression, vConnectionString, Domain, SearchNameValuePairList, vLoggingInfo)
                    // TheCache.Add("URLENGINE" + (TheCache.Count + 1).ToString, CurEngine, Nothing, DateTime.Now.AddSeconds(500), System.Web.Caching.Cache.NoSlidingExpiration, Web.Caching.CacheItemPriority.Normal, Nothing)
                    return AddNewEngineToCache(URLExpression, vConnectionString, Domain, ref SearchNameValuePairList,
                        ref vLoggingInfo, WriteLogging, GetCategoriesOnContentCollections, vCategoryLoaderParams,
                        ref TheCache, LanguageCode, rootCategoryId, flatListOfAllCleanCategory, cleanTree, supplierTree, CacheTime);
                }
                else
                {
                    // no caching enum, add an item to the list return that item
                    // CurEngine = New URLEngine(URLExpression, vConnectionString, Domain, SearchNameValuePairList, vLoggingInfo)
                    // TheCache.Add("URLENGINE" + (TheCache.Count + 1).ToString, CurEngine, Nothing, DateTime.Now.AddSeconds(500), System.Web.Caching.Cache.NoSlidingExpiration, Web.Caching.CacheItemPriority.Normal, Nothing)
                    return AddNewEngineToCache(URLExpression, vConnectionString, Domain, ref SearchNameValuePairList,
                        ref vLoggingInfo, WriteLogging, GetCategoriesOnContentCollections, vCategoryLoaderParams,
                        ref TheCache, LanguageCode, rootCategoryId, flatListOfAllCleanCategory, cleanTree, supplierTree, CacheTime);
                }
            }
            else
            {
                //  no caching object!
                return new URLEngine(URLExpression, vConnectionString, Domain, ref SearchNameValuePairList,
                    ref vLoggingInfo, WriteLogging, GetCategoriesOnContentCollections, vCategoryLoaderParams,
                    ref TheCache, LanguageCode,
                    rootCategoryId,flatListOfAllCleanCategory,cleanTree,supplierTree);
            }

            return null;
        }

        private void GoThroughContentCollectionsAndAssignContentCategories(ref SqlConnection SQLConnection)
        {
            var SQLCommand = new SqlCommand(
                @"SELECT DISTINCT content_categories.category_Id FROM publication_schemes_rules INNER JOIN publication_schemes ON publication_schemes_rules.publication_schemes_Id = publication_schemes.publication_schemes_Id INNER JOIN content_categories with (nolock) ON publication_schemes.publication_schemes_Id = content_categories.publication_scheme_Id AND publication_schemes_rules.from_date < GETDATE() AND publication_schemes_rules.to_date > GETDATE() AND publication_schemes_rules.active = 1 AND publication_schemes.active = 1 WHERE (content_categories.content_main_Id = @CTMainID)");
            SQLCommand.Connection = SQLConnection;
            SqlDataReader Reader = null;
            foreach (var CurCollection in MyContentCollectionTasks)
            {
                // For Each item As ContentPiece In CurCollection.fMyContent
                foreach (var item in CurCollection.Item2.Result.fMyContent)
                {
                    SQLCommand.Parameters.Clear();
                    SQLCommand.Parameters.Add(new SqlParameter("@CTMainID", item.CTMainID));
                    Reader = SQLCommand.ExecuteReader();
                    while (Reader.Read())
                    {
                        if (item.fCategoriesImAssignedTo == null)
                        {
                            item.fCategoriesImAssignedTo = new List<string>();
                        }

                        item.fCategoriesImAssignedTo.Add(Reader["category_Id"].ToString());
                    }

                    Reader.Close();
                }

                // build and assign list of all categories in CurCollection that don't have any children (and self) that have an content item assigned
                var inContentCollection = CurCollection.Item2.Result;
                ContentCollection.CreateEmptyCategoryList(ref inContentCollection);
            }
        }

        private bool IsPropAlreadyInPropList(ref List<PropValuePair> InPropValuePairList, string PropName)
        {
            PropName = PropName.Replace(" ", "_");
            foreach (var item in InPropValuePairList)
            {
                if (item.fPropertyInfo.Name == PropName)
                {
                    return true;
                }
            }

            return false;
        }

        private void DoubleCheckandFillPropValuePairList(ref List<ContentTypeType> MyTypesList,
            ref List<PropValuePair> InPropValuePairList, string InContentType)
        {
            var TheTypeType = ContentTypeType.GetContentTypeType(ref MyTypesList, InContentType);
            if (TheTypeType == null)
            {
                var LocalSQLConnection = new SqlConnection(fConnectionString);
                LocalSQLConnection.Open();
                var command2 = new SqlCommand(
                    @"SELECT content_types_fields.content_types_fields_Id, content_types_fields.content_types_Id, content_types_fields.mandatory, content_types_fields.fieldtype, content_types_fields.fieldname, content_types_fields.default_value, content_types_fields.readonly FROM content_types INNER JOIN content_types_fields ON content_types.content_types_Id = content_types_fields.content_types_Id WHERE (content_types.content_types_Id = @CTTypeID) ORDER BY content_types.content_types_Id");
                command2.Parameters.Add(new SqlParameter("@CTTypeID", InContentType));
                command2.Connection = LocalSQLConnection;
                var reader2 = command2.ExecuteReader();
                var CurPropName = "";
                while (reader2.Read())
                {
                    CurPropName = reader2["fieldname"].ToString();
                    if (IsPropAlreadyInPropList(ref InPropValuePairList, CurPropName))
                    {
                        // do nothing
                    }
                    else
                    {
                        //  add property to row
                        PropValuePair HoldNewProp;
                        var FieldType = int.Parse(reader2["fieldtype"].ToString());
                        var DefaultValue = reader2["default_value"];
                        if (Convert.IsDBNull(DefaultValue))
                        {
                            DefaultValue = null;
                        }

                        var ActualType = typeof(string);
                        if (FieldType == 1
                            || FieldType == 7)
                        {
                            // do nothing leave as string
                        }
                        else if (FieldType == 2)
                        {
                            ActualType = typeof(int);
                        }
                        else if (FieldType == 3)
                        {
                            ActualType = typeof(DateTime);
                        }
                        else if (FieldType == 4)
                        {
                            ActualType = typeof(double);
                        }
                        else if (FieldType == 5)
                        {
                            ActualType = typeof(bool);
                        }

                        //  now I have the field name, default value and actual type
                        HoldNewProp = new PropValuePair(new PropertyInfo(CurPropName, ActualType), DefaultValue);
                        InPropValuePairList.Add(HoldNewProp);
                    }
                }

                reader2.Close();
                LocalSQLConnection.Close();
            }
            else
            {
                // nothing to do the type already exists!
            }
        }

        private void RemoveContentWithEmptyCategories()
        {
            var FoundActiveCat = false;
            foreach (var CC in MyContentCollectionTasks)
            {
                // For Each CC As ContentCollection In Me.MyContentCollections
                foreach (var Item in CC.Item2.Result.fMyContent)
                {
                    // For Each Item As ContentPiece In CC.fMyContent
                    Item.HasActiveCategories = false;
                    foreach (var Cat in Item.fCategoriesImAssignedTo)
                    {
                        if (CategoryTreesLoaded.IsCategoryActiveInAnyTree(Cat))
                        {
                            Item.HasActiveCategories = true;
                            // TODO: Continue For... Warning!!! not translated
                        }
                    }
                }
            }
        }

        public static string StripCategoryFromURL(string InURL)
        {
            if (InURL.Contains("&!Cat:"))
            {
                return InURL.Substring(0, InURL.IndexOf("&!Cat:"));
            }
            else if (InURL.Contains("&#Cat:"))
            {
                return InURL.Substring(0, InURL.IndexOf("&#Cat:"));
                //  because Tamar's piece does a find and replace on ! points
            }
            else
            {
                return InURL;
            }
        }

        private CategorySearchHelper GetCategorySearchHelperFromOtherFilter(string InOtherFilter)
        {
            // take in a string like this (C_M_1.CONTENT_TYPES_ID = #$#3%$% AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue )
            var CategoryLandmark = "+=+CATEGORY+=+";
            var HoldOther = InOtherFilter.Substring(InOtherFilter.IndexOf(CategoryLandmark) + CategoryLandmark.Length);
            // should have  in +=+CatValue#$#d13e10%$%+=+EndCatValue & other stuff)
            CategoryLandmark = "+=+CatValue#$#";
            HoldOther = HoldOther.Substring(HoldOther.IndexOf(CategoryLandmark) + CategoryLandmark.Length);
            //  should have d13e10%$%+=+EndCatValue & other stuff)
            var SearchUp = false;
            if (HoldOther.StartsWith("u", StringComparison.OrdinalIgnoreCase))
            {
                SearchUp = true;
                HoldOther = HoldOther.Substring(1);
                // trim off the up or down
            }
            else if (HoldOther.StartsWith("d", StringComparison.OrdinalIgnoreCase))
            {
                SearchUp = false;
                HoldOther = HoldOther.Substring(1);
                // trim off the up or down
            }
            else
            {
                SearchUp = false;
            }

            CategoryLandmark = "%$%+=+EndCatValue";
            HoldOther = HoldOther.Substring(0, HoldOther.IndexOf(CategoryLandmark));
            // should now have 13e10
            var IncludeLevel0 = false;
            var isHunt = false;
            var IsNoStrip = false;
            if (HoldOther.Contains("i") || HoldOther.Contains("I"))
            {
                IncludeLevel0 = true;
            }
            else
            {
                IncludeLevel0 = false;
            }

            // ego 02-17-11
            if (HoldOther.IndexOf("hns", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                IsNoStrip = true;
                HoldOther = HoldOther.Remove(HoldOther.IndexOf("ns", StringComparison.CurrentCultureIgnoreCase), 2);
            }

            // ltu 08/31: added hunt variable
            if (HoldOther.IndexOf("h", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                isHunt = true;
                HoldOther = HoldOther.Remove(HoldOther.IndexOf("h", StringComparison.CurrentCultureIgnoreCase), 1);
            }

            HoldOther = HoldOther.Replace("i", "*");
            HoldOther = HoldOther.Replace("I", "*");
            HoldOther = HoldOther.Replace("e", "*");
            HoldOther = HoldOther.Replace("E", "*");
            // should now have 13*10
            var HoldCatId = "";
            var Depth = -1;
            var CatID = -1;
            if (HoldOther.IndexOf("*") >= 0)
            {
                HoldCatId = HoldOther.Substring(0, HoldOther.IndexOf("*"));
                CatID = int.Parse(HoldCatId);
                HoldOther = HoldOther.Substring(HoldOther.IndexOf("*") + 1);
                //  hold other should have the depth
                Depth = int.Parse(HoldOther);
            }
            else
            {
                //  if cXXXX is used (legacy code, it is the same as cdXXXXi0
                HoldCatId = HoldOther;
                CatID = int.Parse(HoldCatId);
                Depth = 0;
                IncludeLevel0 = true;
            }

            var Helper = new CategorySearchHelper(SearchUp, CatID, IncludeLevel0, isHunt, Depth, IsNoStrip);
            return Helper;
            //  will leave in other filter alone, the caller is responsable for modifying the string after I strip the parameters. 
            //  Return a category search helper with all the values filled in. 
        }

        private string ReplaceFirstCategoryOtherFilter(string InOtherFilter)
        {
            //  takes in one or many category filters and replaces it
            // (C_M_1.CONTENT_TYPES_ID = #$#3%$% AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue & maybe another one! AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue)
            var CategoryLandmark = "+=+CATEGORY+=+";
            var hold = InOtherFilter;
            var holdleft = hold.Substring(0, hold.IndexOf(CategoryLandmark));
            hold = hold.Substring(hold.IndexOf(CategoryLandmark) + CategoryLandmark.Length);
            //  hold left should be (C_M_1.CONTENT_TYPES_ID = #$#3%$% AND 
            //  hold should be in +=+CatValue#$#d13e10%$%+=+EndCatValue & maybe another one! AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+
            CategoryLandmark = "+=+EndCatValue";
            var holdright = hold.Substring(hold.IndexOf(CategoryLandmark) + CategoryLandmark.Length);
            //  hold right should be  & maybe another one! AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+
            hold = hold.Substring(0, hold.IndexOf(CategoryLandmark));
            // hold should be +=+CatValue#$#d13e10%$%
            var Result = holdleft + " c_ca_1.CATEGORY_ID in (select CategoryID from @Cats) " + holdright;
            return Result;
        }

        private CategorySearchListAndCleanOtherFilterHelper ReplaceCategoryFilter(string InOtherFilter,
            ref SqlConnection InSQLConnection)
        {
            //  takes in string like this and replaces it with the proper where in
            // (C_M_1.CONTENT_TYPES_ID = #$#3%$% AND +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue )
            var HoldOther = InOtherFilter;
            var HoldSearchHelper = new List<CategorySearchHelper>();
            while (HoldOther.Contains("+=+CATEGORY+=+"))
            {
                var hold = GetCategorySearchHelperFromOtherFilter(HoldOther);
                if (!(hold == null))
                {
                    HoldSearchHelper.Add(hold);
                }

                HoldOther = ReplaceFirstCategoryOtherFilter(HoldOther);
            }

            //  replace the flag with the right category field c_ca_1.CATEGORY_ID in 
            //  grab if it's up or down as a boolean
            //  grab the include / exclude as a boolean
            //  grab the depth
            var HoldReturn = new CategorySearchListAndCleanOtherFilterHelper(ref HoldSearchHelper, HoldOther);
            return HoldReturn;
        }

        private async Task testAsync(URLResolver tURLResolver, bool RunAsync)
        {
            if (RunAsync)
            {
                await fSQLConnection.OpenAsync();
            }
            else
            {
                fSQLConnection.Open();
            }

            for (var index = 0;
                index
                <= tURLResolver.ChainCount() - 1;
                index++)
            {
                string ChainFilterText;
                ChainFilterText = tURLResolver.GetMainChainText(index);
                var HoldFieldFilter = tURLResolver.GetFieldFilterFromMainChain(ChainFilterText);
                var HoldOtherThanFieldFilter = tURLResolver.GetOtherThanFieldFilterFromMainChain(ChainFilterText);
                //  may have the category flag +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue
                CategorySearchListAndCleanOtherFilterHelper HoldCategorySearchListAndCleanOtherFilterHelper;
                HoldCategorySearchListAndCleanOtherFilterHelper =
                    ReplaceCategoryFilter(HoldOtherThanFieldFilter, ref fSQLConnection);
                HoldOtherThanFieldFilter = HoldCategorySearchListAndCleanOtherFilterHelper.CleanOtherFieldFilter;
                //  need to do something to capture the category parameter(s) into a list run the next statement
                //  need to preserve the location inside the query string logic
                var HoldSQLParams =
                    tURLResolver.ConvertValuesIntoParametersArray(ref HoldFieldFilter, ref HoldOtherThanFieldFilter);
                // parameter replacement should probably ignore the SQL category parameters
                // ltu
                // add a check to ensure if we get nothing in return from using the default language, we can use the fallback language
                SqlCommand command = null;
                var start = DateTime.Now;
                //  maybe pass the extra category header sql so it will be appeneded to the top
                command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, fDomain,
                    HoldSQLParams, LanguageCode, true, fSQLConnection,
                    ref HoldCategorySearchListAndCleanOtherFilterHelper);
                command.Connection = fSQLConnection;
                command.CommandTimeout = 150;
                SqlDataReader reader = null;
                try
                {
                    if (RunAsync)
                    {
                        reader = await command.ExecuteReaderAsync();
                    }
                    else
                    {
                        reader = command.ExecuteReader();
                    }

                    if (!reader.HasRows)
                    {
                        // if no result set try fall back language
                        reader.Close();
                        command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, fDomain,
                            HoldSQLParams, LanguageCode, false, fSQLConnection,
                            ref HoldCategorySearchListAndCleanOtherFilterHelper);
                        command.Connection = fSQLConnection;
                        // reader = command.ExecuteReader()
                        if (RunAsync)
                        {
                            reader = await command.ExecuteReaderAsync();
                        }
                        else
                        {
                            reader = command.ExecuteReader();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (!(reader == null))
                    {
                        reader.Close();
                    }

                    throw new Exception(string.Format("Error [{0}] Command [{1}]", ex.Message, command.CommandText));
                }
                finally
                {
                    reader.Close();
                    // nothin
                }
            }

            fSQLConnection.Close();
        }

        // Public Shared sqlcons As New SemaphoreSlim(3)
        private static string getInumber(string holdinurl)
        {
            // Dim holdinurl As String = "p18(ct2&cd2i99&fCSP_Content_Id%1=podcast#):c12(ct2):c61(ct5|ct7|ct10):c57(ct3&cd1i99hns):c24(ct7&cd1i99&fLocal_Type%1=podcast#)[st(ct7*Status_Sort_Order asc*)]:c31(ct2&fCSP_Content_Id%1=fearesour#):c30(ct5|ct10|ct7&cd99i99)[st(ct5-7-10*Status_Featured_Sort_Order desc*)]:c33(ct2&fCSP_Content_Id%1=prodDoc#):c34(ct5&cd999e0&fLocal_Type%1=brochure#|fLocal_Type%1=datasheet#)[st(ct5*Status_Sort_Order asc*)]:c38(ct2&fCSP_Content_Id%1=contactrep#):c37(ct2):c39(ct3|ct2&cd99i99):c50(ct2&fCSP_Content_Id%1=youarehere#):c49(ct2|ct3&cu2i99)[st(ct2-ct3*ContentCategoryDepth desc*)]:c104(ct3&cd1i99)[st(ct3*Status_Sort_Order asc*)]&i=1234&ed=asdf()"
            if (holdinurl.Contains("&i="))
            {
                var holdsub = holdinurl.Substring(holdinurl.IndexOf("&i=") + 3);
                if (holdsub.Contains("&"))
                {
                    holdsub = holdsub.Substring(0, holdsub.IndexOf("&"));
                }

                if (holdsub.Contains("("))
                {
                    holdsub = holdsub.Substring(0, holdsub.IndexOf("("));
                }

                return holdsub;
                // holdsub.Dump()
            }

            if (holdinurl.Contains("&ticks="))
            {
                var holdsub = holdinurl.Substring(holdinurl.IndexOf("&ticks=") + 7);
                if (holdsub.Contains("&"))
                {
                    holdsub = holdsub.Substring(0, holdsub.IndexOf("&"));
                }

                if (holdsub.Contains("("))
                {
                    holdsub = holdsub.Substring(0, holdsub.IndexOf("("));
                }

                return holdsub;
                // holdsub.Dump()
            }

            return "none";
        }

        private static void ClearWebCacheAsNeeded(SqlConnection AnOpenConnection, Cache TheCache)
        {
            var HoldResult = int.Parse(new SqlCommand("select count (1) from pubcache", AnOpenConnection)
                .ExecuteScalar().ToString());
            if (!(TheCache == null))
            {
                if (HoldResult < 100)
                {
                    var wenum = TheCache.GetEnumerator();
                    while (wenum.MoveNext())
                    {
                        TheCache.Remove(wenum.Key.ToString());
                    }
                }
            }
        }

        private static ContentCollection LoadContent(URLResolver tURLResolver, string ConString, int index,
            string inDomain, string inLangaugeCode, URLEngine CurUrlEngine, ref Cache InURLCache)        {
            var fSQLConnectionLocal = new SqlConnection(ConString);
            // try
            //        sqlcons.Wait()
            fSQLConnectionLocal.Open();
            //  Finally
            //       sqlcons.Release()
            // End Try
            var CacheKey = FormsAuthentication.HashPasswordForStoringInConfigFile("EMPTYNEVERMATCH", "MD5");
            ContentCollection CurContentCollection;
            try
            {
                string ChainFilterText;
                ChainFilterText = tURLResolver.GetMainChainText(index);
                var HoldFieldFilter = tURLResolver.GetFieldFilterFromMainChain(ChainFilterText);
                var HoldOtherThanFieldFilter = tURLResolver.GetOtherThanFieldFilterFromMainChain(ChainFilterText);
                //  may have the category flag +=+CATEGORY+=+ in +=+CatValue#$#d13e10%$%+=+EndCatValue
                CategorySearchListAndCleanOtherFilterHelper HoldCategorySearchListAndCleanOtherFilterHelper;
                try
                {
                    CurUrlEngine.ss.Wait();
                    ClearWebCacheAsNeeded(fSQLConnectionLocal, InURLCache);
                    HoldCategorySearchListAndCleanOtherFilterHelper =
                        CurUrlEngine.ReplaceCategoryFilter(HoldOtherThanFieldFilter, ref fSQLConnectionLocal);
                }
                finally
                {
                    CurUrlEngine.ss.Release();
                }

                HoldOtherThanFieldFilter = HoldCategorySearchListAndCleanOtherFilterHelper.CleanOtherFieldFilter;
                //  need to do something to capture the category parameter(s) into a list run the next statement
                //  need to preserve the location inside the query string logic
                var HoldSQLParams =
                    tURLResolver.ConvertValuesIntoParametersArray(ref HoldFieldFilter, ref HoldOtherThanFieldFilter);
                // parameter replacement should probably ignore the SQL category parameters
                // ltu
                // add a check to ensure if we get nothing in return from using the default language, we can use the fallback language
                SqlCommand command = null;
                var start = DateTime.Now;
                var Params = "asdf";
                foreach (var p in HoldSQLParams)
                {
                    Params = Params + p.ParameterName + "$" + p.Value.ToString() + " - ";
                }

                try
                {
                    if (!(HoldCategorySearchListAndCleanOtherFilterHelper == null))
                    {
                        if (!(HoldCategorySearchListAndCleanOtherFilterHelper.CategorySearchHelpers == null))
                        {
                            foreach (var p in HoldCategorySearchListAndCleanOtherFilterHelper.CategorySearchHelpers)
                            {
                                Params = Params + "CATSHELP" + (p.CategoryID + ("$"
                                                                                + (p.Depth + ("$"
                                                                                              + (p.IncludeLevel0 + ("$"
                                                                                                                    + (
                                                                                                                        p.IsHunt +
                                                                                                                        ("$"
                                                                                                                         + (
                                                                                                                             p.NoStrip +
                                                                                                                             ("$" +
                                                                                                                              p.SearchUp
                                                                                                                             ))
                                                                                                                        ))
                                                                                                 ))))));
                            }
                        }

                        if (!(HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter == null))
                        {
                            var h = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Helper;
                            Params = Params + "CATSHELPHUNT" + (h.CategoryID + ("$"
                                                                                + (h.Depth + ("$"
                                                                                              + (h.IncludeLevel0 + ("$"
                                                                                                                    + (
                                                                                                                        h.IsHunt +
                                                                                                                        ("$"
                                                                                                                         + (
                                                                                                                             h.NoStrip +
                                                                                                                             ("$" +
                                                                                                                              h.SearchUp
                                                                                                                             ))
                                                                                                                        ))
                                                                                                 ))))));
                        }
                    }
                }
                catch
                {
                }


                var sSortParams = tURLResolver.fSortParamsList[index];
                if (!(sSortParams == null))
                {
                    Params = Params + "SORT: " + sSortParams.SortParam + sSortParams.SortType;
                }

                Params = getInumber(tURLResolver.FullInURL) + " eye number " + Params;
                // For Each p As CategoryHuntNode In HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes
                //     Params = Params + "CATSHELP" + p.CategoryId + "$" + p.Children.+ "$" + p.ContentId.ToString + "$" + p.ContentTypeId + "$" + p.Depth + "$" + p.HasChildren + "$" + p.Lineage + "$" + p.ParentCategoryId
                // Next
                // HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Helper.
                //  maybe pass the extra category header sql so it will be appeneded to the top
                CacheKey = FormsAuthentication.HashPasswordForStoringInConfigFile(
                    Params + HoldFieldFilter + HoldOtherThanFieldFilter + inDomain + inLangaugeCode, "MD5");
                ContentCollection CachedCC;
                if (!(InURLCache == null))
                {
                    // If False Then
                    CachedCC = (ContentCollection) InURLCache[CacheKey];
                    try
                    {
                        CurUrlEngine.ss.Wait();
                        if (!(CachedCC == null))
                        {
                            // cachedString = "Hello, World."
                            // Cache.Insert("CacheItem", cachedString)
                            var CopiedCC = CachedCC.GetDeepCopy();
                            fSQLConnectionLocal.Close();
                            CopiedCC.fSQLConnection = fSQLConnectionLocal;
                            CopiedCC.fConString = ConString;
                            CopiedCC.fPageParams = tURLResolver.fPageParamsList[index];
                            return CopiedCC;
                        }
                    }
                    finally
                    {
                        CurUrlEngine.ss.Release();
                    }
                }

                command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, inDomain,
                    HoldSQLParams, inLangaugeCode, true, fSQLConnectionLocal,
                    ref HoldCategorySearchListAndCleanOtherFilterHelper);
                command.Connection = fSQLConnectionLocal;
                command.CommandTimeout = 150;
                SqlDataReader reader = null;
                try
                {
                    reader = command.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        // if no result set try fall back language
                        reader.Close();
                        command = tURLResolver.GenerateSQLCommand(HoldFieldFilter, HoldOtherThanFieldFilter, inDomain,
                            HoldSQLParams, inLangaugeCode, false, fSQLConnectionLocal,
                            ref HoldCategorySearchListAndCleanOtherFilterHelper);
                        command.Connection = fSQLConnectionLocal;
                        // reader = command.ExecuteReader()                    
                        reader = command.ExecuteReader();
                    }
                }
                catch (Exception ex)
                {
                    if (!(reader == null))
                    {
                        reader.Close();
                    }

                    throw new Exception(string.Format("Error [{0}] Command [{1}]", ex.Message, command.CommandText));
                }
                finally
                {
                    // reader.Close()
                    // nothin
                }

                var ts = DateTime.Now.Subtract(start);
                var CleanFilter = ChainFilterText;
                CleanFilter = CleanFilter.Replace("#$#", "");
                CleanFilter = CleanFilter.Replace("%$%", "");
                var fSortParams = tURLResolver.fSortParamsList[index];
                var fPageParams = tURLResolver.fPageParamsList[index];
                CurContentCollection = new ContentCollection(CleanFilter, inDomain,
                    tURLResolver.fExpressionPublications[index], ref fSortParams,
                    ref fPageParams, tURLResolver.fRawURL[index], ref fSQLConnectionLocal, ConString);
                try
                {
                    CurUrlEngine.ss.Wait();
                    if (!(CurUrlEngine.CategoryTreesLoaded == null))
                    {
                        CurContentCollection.fMyCategories = CurUrlEngine.CategoryTreesLoaded;
                    }
                }
                finally
                {
                    CurUrlEngine.ss.Release();
                }

                try
                {
                    if (true)
                    {
                        var i = 0;
                        // ListBox1.Items.Add("Pre Read Records" + DateTime.Now.ToString())
                        var LastContentInstanceID = "";
                        var CTMainKey = "";
                        var ContentInstanceID = "";
                        var CategoryId = -1;
                        var LastCategoryID = -1;
                        var CategoryName = "";
                        var Depth = -1;
                        var ParentID = -1;
                        var ContentType = "";
                        ArrayList CTCat = null;
                        var CTMainConsumerID = "";
                        var CTMainContentIdentifier = "";
                        var CTThemeID = "0";
                        var CTMainID = "";
                        var CTMainSupplierID = "";
                        var CTLanguageID = "";
                        var CTLanguageCode = "";
                        var CTLanguageDescription = "";
                        var CCHasChildren = false;
                        var CCChildDepth = -1;
                        var NextAvailableCCId = -1;
                        CategoryHuntNode tmpCategoryNode;
                        // ltu: 11/15 lineage
                        var lineage = "";
                        var HasCategoryInResults = false;
                        if (HoldCategorySearchListAndCleanOtherFilterHelper.CategorySearchHelpers.Count > 0)
                        {
                            HasCategoryInResults = true;
                        }

                        var CurPropValuePairList = new List<PropValuePair>();
                        ContentPiece CurContentPiece;
                        Type CurType;
                        var FoundAnything = false;
                        while (reader.Read())
                        {
                            if (ContentInstanceID == LastContentInstanceID
                                && CategoryId != LastCategoryID)
                            {
                                object imhere = true;
                            }

                            FoundAnything = true;
                            ContentInstanceID = reader["content_Id"].ToString();
                            //  content instance ID
                            if (HasCategoryInResults)
                            {
                                CategoryId = int.Parse(reader["category_Id"].ToString());
                            }

                            if (ContentInstanceID != LastContentInstanceID
                                && LastContentInstanceID == "")
                            {
                                //  first instance!
                                if (HasCategoryInResults)
                                {
                                    CategoryId = int.Parse(reader["category_Id"].ToString());
                                    CategoryName = reader["CategoryName"].ToString();
                                    Depth = int.Parse(reader["Depth"].ToString());
                                    ParentID = int.Parse(reader["ParentID"].ToString());
                                    lineage = reader["Lineage"].ToString();
                                    // ltu: 08/31 category hunt routine
                                    if (!(HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter == null))
                                    {
                                        // tmpCategoryNode = HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(Function(n As CategoryHuntNode) (n.ContentId.Equals(ContentInstanceID)))
                                        tmpCategoryNode =
                                            HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(
                                                n => n.ContentId.ToString().Equals(ContentInstanceID));
                                        if (!(tmpCategoryNode == null))
                                        {
                                            CCHasChildren = tmpCategoryNode.HasChildren;
                                            CCChildDepth = tmpCategoryNode.Depth;
                                            if (tmpCategoryNode.Children.Count > 0)
                                            {
                                                NextAvailableCCId = tmpCategoryNode.Children[0].ParentCategoryId;
                                            }
                                            else
                                            {
                                                NextAvailableCCId = -1;
                                            }
                                        }
                                        else
                                        {
                                            CCHasChildren = false;
                                            NextAvailableCCId = -1;
                                            CCChildDepth = -1;
                                        }
                                    }
                                }

                                CurPropValuePairList.Clear();
                                CTMainKey = reader["content_main_key"].ToString();
                                // fContentMainKey
                                ContentType = reader["content_types_Id"].ToString();
                                // fcontentTypeID
                                CTMainConsumerID = reader["consumer_Id"].ToString();
                                //  fCTMainConsumerID
                                CTMainContentIdentifier = reader["content_identifier"].ToString();
                                // fCTMainContentIdentifier
                                CTMainID = reader["content_main_id"].ToString();
                                // fCTMainID
                                CTMainSupplierID = reader["supplier_Id"].ToString();
                                // fCTMainSupplierID
                                CTLanguageID = reader["content_types_languages_Id"].ToString();
                                // ltu 8/27
                                CTLanguageCode = reader["languagecode"].ToString();
                                CTLanguageDescription = reader["description"].ToString();
                                // todo: might need to change the column name to language_description to avoid conflict.
                                LastContentInstanceID = ContentInstanceID;
                                if (HasCategoryInResults)
                                {
                                    LastCategoryID = CategoryId;
                                }

                                CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader,
                                    reader["fieldname"].ToString(), int.Parse(reader["fieldtype"].ToString())));
                            }
                            else if (ContentInstanceID != LastContentInstanceID
                                     || ContentInstanceID == LastContentInstanceID
                                     && CategoryId != LastCategoryID)
                            {
                                //  At new ContentMain, need to create prior CTMain object instance
                                //  First get the actual underlying type
                                try
                                {
                                    CurUrlEngine.ss.Wait();
                                    CurUrlEngine.DoubleCheckandFillPropValuePairList(
                                        ref CurUrlEngine.MyDynamicContentTypes, ref CurPropValuePairList, ContentType);
                                    CurType = ContentTypeType
                                        .GetTypeByContentTypewPropValuePairList(ref CurUrlEngine.MyDynamicContentTypes,
                                            ContentType, ref CurPropValuePairList).fType;
                                }
                                finally
                                {
                                    CurUrlEngine.ss.Release();
                                }

                                //  create an instance with values
                                // CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList,  New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, ContentInstanceID, CTMainKey))
                                var temp = new ContentPieceParameters(CTMainContentIdentifier, CTMainID,
                                    CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType,
                                    LastContentInstanceID, CTMainKey, CTLanguageID, CTLanguageCode,
                                    CTLanguageDescription, CCHasChildren, CCChildDepth, NextAvailableCCId);
                                CurContentPiece =
                                    ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(
                                        CurType, ref CurPropValuePairList, ref temp);
                                if (HasCategoryInResults)
                                {
                                    CurContentPiece.ContentCategoryID = LastCategoryID;
                                    CurContentPiece.ContentCategoryDepth = Depth;
                                    CurContentPiece.ContentCategoryName = CategoryName;
                                    CurContentPiece.ContentCategoryParentID = ParentID;
                                    CurContentPiece.ContentCategoryLineage = lineage;
                                    // ltu: 08/31 category hunt routine
                                    if (!(HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter == null))
                                    {
                                        tmpCategoryNode =
                                            HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(
                                                n => n.ContentId.ToString().Equals(LastContentInstanceID));
                                        if (!(tmpCategoryNode == null))
                                        {
                                            CurContentPiece.ContentCategoryHasChildren = tmpCategoryNode.HasChildren;
                                            CurContentPiece.ContentCategoryChildDepth = tmpCategoryNode.Depth;
                                            if (tmpCategoryNode.Children.Count > 0)
                                            {
                                                CurContentPiece.NextAvailableContentCategoryChildId =
                                                    tmpCategoryNode.Children[0].ParentCategoryId;
                                            }
                                            else
                                            {
                                                CurContentPiece.NextAvailableContentCategoryChildId = -1;
                                            }
                                        }
                                        else
                                        {
                                            CurContentPiece.ContentCategoryHasChildren = false;
                                            CurContentPiece.ContentCategoryChildDepth = -1;
                                            CurContentPiece.NextAvailableContentCategoryChildId = -1;
                                        }
                                    }
                                }

                                CurContentCollection.fMyContent.Add(CurContentPiece);
                                i = i + 1;
                                CurPropValuePairList.Clear();
                                CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader,
                                    reader["fieldname"].ToString(), int.Parse(reader["fieldtype"].ToString())));
                                // add first prop
                                CTMainKey = reader["content_main_key"].ToString();
                                // fContentMainKey
                                ContentType = reader["content_types_Id"].ToString();
                                // fcontentTypeID
                                CTMainConsumerID = reader["consumer_Id"].ToString();
                                //  fCTMainConsumerID
                                CTMainContentIdentifier = reader["content_identifier"].ToString();
                                // fCTMainContentIdentifier
                                CTMainID = reader["content_main_id"].ToString();
                                // fCTMainID
                                CTMainSupplierID = reader["supplier_Id"].ToString();
                                // fCTMainSupplierID
                                if (HasCategoryInResults)
                                {
                                    CategoryId = int.Parse(reader["category_Id"].ToString());
                                    CategoryName = reader["CategoryName"].ToString();
                                    Depth = int.Parse(reader["Depth"].ToString());
                                    ParentID = int.Parse(reader["ParentID"].ToString());
                                    lineage = reader["Lineage"].ToString();
                                }

                                LastContentInstanceID = ContentInstanceID;
                                LastCategoryID = CategoryId;
                            }
                            else
                            {
                                //  add current field as paramValue
                                CurPropValuePairList.Add(PropValuePair.GetPropValuePairFromReaderRow(reader,
                                    reader["fieldname"].ToString(), int.Parse(reader["fieldtype"].ToString())));
                            }
                        }

                        if (FoundAnything)
                        {
                            //  create last instance
                            try
                            {
                                CurUrlEngine.ss.Wait();
                                CurUrlEngine.DoubleCheckandFillPropValuePairList(ref CurUrlEngine.MyDynamicContentTypes,
                                    ref CurPropValuePairList, ContentType);
                                CurType = ContentTypeType
                                    .GetTypeByContentTypewPropValuePairList(ref CurUrlEngine.MyDynamicContentTypes,
                                        ContentType, ref CurPropValuePairList).fType;
                            }
                            finally
                            {
                                CurUrlEngine.ss.Release();
                            }

                            //  create an instance with values
                            // CurContentPiece = ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType, CurPropValuePairList, New ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID, CTMainConsumerID, CTCat, CTThemeID, ContentType, ContentInstanceID, CTMainKey, CTLanguageID, CTLanguageCode, CTLanguageDescription))
                            var temp = new ContentPieceParameters(CTMainContentIdentifier, CTMainID, CTMainSupplierID,
                                CTMainConsumerID, CTCat, CTThemeID, ContentType, LastContentInstanceID, CTMainKey,
                                CTLanguageID, CTLanguageCode, CTLanguageDescription, CCHasChildren, CCChildDepth,
                                NextAvailableCCId);
                            CurContentPiece =
                                ContentPieceTypeCreator.GetCustomContentPieceInstanceFromTypewPropValuePairList(CurType,
                                    ref CurPropValuePairList, ref temp);
                            if (HasCategoryInResults)
                            {
                                CurContentPiece.ContentCategoryID = LastCategoryID;
                                CurContentPiece.ContentCategoryDepth = Depth;
                                CurContentPiece.ContentCategoryName = CategoryName;
                                CurContentPiece.ContentCategoryParentID = ParentID;
                                CurContentPiece.ContentCategoryLineage = lineage;
                                // ltu: 08/31 category hunt routine
                                if (!(HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter == null))
                                {
                                    tmpCategoryNode =
                                        HoldCategorySearchListAndCleanOtherFilterHelper.CategoryHunter.Nodes.Find(n =>
                                            n.ContentId.ToString().Equals(LastContentInstanceID));
                                    if (!(tmpCategoryNode == null))
                                    {
                                        CurContentPiece.ContentCategoryHasChildren = tmpCategoryNode.HasChildren;
                                        CurContentPiece.ContentCategoryChildDepth = tmpCategoryNode.Depth;
                                        if (tmpCategoryNode.Children.Count > 0)
                                        {
                                            CurContentPiece.NextAvailableContentCategoryChildId =
                                                tmpCategoryNode.Children[0].ParentCategoryId;
                                        }
                                        else
                                        {
                                            CurContentPiece.NextAvailableContentCategoryChildId = -1;
                                        }
                                    }
                                    else
                                    {
                                        CurContentPiece.ContentCategoryHasChildren = false;
                                        CurContentPiece.ContentCategoryChildDepth = -1;
                                        CurContentPiece.NextAvailableContentCategoryChildId = -1;
                                    }
                                }
                            }

                            CurContentCollection.fMyContent.Add(CurContentPiece);
                            i = i + 1;
                            var SortContentType = CurContentCollection.fSortParams.SortType;
                            var SortString = CurContentCollection.fSortParams.SortParam;
                            var failovertype = false;
                            if (SortString != "")
                            {
                                ContentTypeType HoldCTT;
                                try
                                {
                                    CurUrlEngine.ss.Wait();
                                    HoldCTT = ContentTypeType.GetContentTypeType(ref CurUrlEngine.MyDynamicContentTypes,
                                        SortContentType);
                                    if (HoldCTT == null
                                        && !(CurUrlEngine.MyDynamicContentTypes[0] == null))
                                    {
                                        failovertype = true;
                                        HoldCTT = ContentTypeType.GetContentTypeType(
                                            ref CurUrlEngine.MyDynamicContentTypes,
                                            CurUrlEngine.MyDynamicContentTypes[0].fType.Name);
                                        // try the first avail one, it will probably work :)
                                    }
                                }
                                finally
                                {
                                    CurUrlEngine.ss.Release();
                                }

                                if (!(HoldCTT == null))
                                {
                                    if (!failovertype)
                                    {
                                        CurType = HoldCTT.fType;
                                        if (!(CurType == null))
                                        {
                                            try
                                            {
                                                CurUrlEngine.ss.Wait();
                                                var DN = new DynamicComparer<ContentPiece>(SortString, CurType);
                                                CurContentCollection.fMyContent.Sort(DN.Comparer);
                                            }
                                            catch (Exception ex)
                                            {
                                                // Dim hold As ContentPiece = New ContentPiece(Nothing)
                                                // Dim DN As DynamicComparer(Of ContentPiece) = New DynamicComparer(Of ContentPiece)(SortString, hold.GetType)
                                                // CurContentCollection.fMyContent.Sort(DN.Comparer)
                                                throw ex;
                                            }
                                            finally
                                            {
                                                CurUrlEngine.ss.Release();
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception(
                                                "Error, Content Type specified in search not found in result set. Search Type: " +
                                                SortContentType + " Sort Params: " + SortString + " URL : " +
                                                CurContentCollection.fFilterExpression);
                                        }
                                    }
                                    else
                                    {
                                        // isfailover
                                        var ti = 0;
                                        var foundthetype = false;
                                        foreach (var o in CurUrlEngine.MyDynamicContentTypes)
                                        {
                                            HoldCTT = ContentTypeType.GetContentTypeType(
                                                ref CurUrlEngine.MyDynamicContentTypes, o.fType.Name);
                                            // try the first avail one, it will probably work :)
                                            try
                                            {
                                                CurUrlEngine.ss.Wait();
                                                var DN = new DynamicComparer<ContentPiece>(SortString, HoldCTT.fType);
                                                CurContentCollection.fMyContent.Sort(DN.Comparer);
                                                foundthetype = true;
                                            }
                                            catch (Exception ex)
                                            {
                                                foundthetype = false;
                                            }
                                            finally
                                            {
                                                CurUrlEngine.ss.Release();
                                            }

                                            if (foundthetype)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception(
                                        "Error, Content Type specified in search not found in result set. Search Type: " +
                                        SortContentType + " Sort Params: " + SortString + " URL : " +
                                        CurContentCollection.fFilterExpression);
                                }
                            }

                            try
                            {
                                // CurUrlEngine.ss.Wait()
                                // CurUrlEngine.MyContentCollections.Add(CurContentCollection)
                            }
                            finally
                            {
                                // CurUrlEngine.ss.Release()
                            }
                        }
                        else
                        {
                            try
                            {
                                // CurUrlEngine.ss.Wait()
                                // CurUrlEngine.MyContentCollections.Add(CurContentCollection) ' add the empty content collection
                            }
                            finally
                            {
                                // CurUrlEngine.ss.Release()
                            }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                    try
                    {
                        CurUrlEngine.ss.Wait();
                        ContentCollection.LookupAndAssignSupplierDataToContentCollectionObjects(CurUrlEngine.fDomain,
                            ref CurContentCollection, ref CurUrlEngine.MyRelatedConsumerSupplierData,
                            ref CurUrlEngine.MyDynamicConsumerSupplierTypes, ref fSQLConnectionLocal);
                    }
                    finally
                    {
                        CurUrlEngine.ss.Release();
                    }

                    try
                    {
                        CurUrlEngine.ss.Wait();
                        CurUrlEngine.fBaseDomainConsumerID = CurContentCollection.fBaseDomainData.ConsumerID;
                    }
                    finally
                    {
                        CurUrlEngine.ss.Release();
                    }
                }
            }
            finally
            {
                fSQLConnectionLocal.Close();
            }

            // Dim trash
            // While (True)
            // Tasks.Task.Delay(500).Wait()
            // Dim x As Integer = 1
            // x = x + 1
            // End While
            if (!(InURLCache == null))
            {
                InURLCache.Insert(CacheKey, CurContentCollection);
            }

            return CurContentCollection;
        }

        private static void TestParallel(URLResolver tURLResolver, string ConString, string inDomain,
            string inLanguageCode, URLEngine CurUrlEngine, ref Cache InURLCache)
        {
            var holdtasks = new List<Task>();
            for (var i = 0;
                i
                <= tURLResolver.ChainCount() - 1;
                i++)
            {
                var index = i;
                var ClosureCache = InURLCache;
                var HoldTask = Task<ContentCollection>.Factory.StartNew(() =>
                {
                    return LoadContent(tURLResolver, ConString, index, inDomain, inLanguageCode, CurUrlEngine,
                        ref ClosureCache);
                });
                try
                {
                    CurUrlEngine.ss.Wait();
                    CurUrlEngine.MyContentCollectionTasks.Add(
                        new Tuple<string, Task<ContentCollection>>(tURLResolver.fExpressionPublications[index],
                            HoldTask));
                }
                finally
                {
                    CurUrlEngine.ss.Release();
                }

                holdtasks.Add(HoldTask);
            }

            // Tasks.Task.WaitAll(holdtasks.ToArray)
            //return "test";
        }
    }
}