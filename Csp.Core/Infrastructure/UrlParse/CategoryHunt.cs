﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryHunt
    {

        private List<CategoryHuntNode> _nodes;

        public List<CategoryHuntNode> NonStrippedNodes;

        private CategorySearchHelper _helper;

        private CategoryHunt(SqlDataReader reader, CategorySearchHelper helper)
        {
            _helper = helper;
            _nodes = ParseContent(reader);
        }

        public void SwitchNodesToNoStripNodesWithHasChildren()
        {
            _nodes = NonStrippedNodes;
            List<CategoryHuntNode> HasChildrenHelper;
            foreach (var node in _nodes)
            {
                var holdNode = node;
                // .net warned me to not use the ideration variable in lamba expressions
                HasChildrenHelper = _nodes.FindAll(x => x.ParentCategoryId == holdNode.CategoryId);
                node.HasChildren = HasChildrenHelper.Count > 0;
            }

        }

        public List<CategoryHuntNode> Nodes
        {
            get
            {
                return _nodes;
            }
            set
            {
                _nodes = value;
            }
        }

        public CategorySearchHelper Helper
        {
            get
            {
                return _helper;
            }
        }

        private List<CategoryHuntNode> ParseContent(SqlDataReader reader)
        {
            var tmpTree = new List<CategoryHuntNode>();
            while (reader.Read())
            {
                tmpTree.Add(new CategoryHuntNode(reader.GetGuid(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(3), reader.GetInt32(4), reader.GetInt32(5)));
            }

            NonStrippedNodes = tmpTree.FindAll(x => true);
            // since i already order the content by depth, we should only concern with the first level
            var nodes = new List<CategoryHuntNode>();
            var extra = new List<CategoryHuntNode>();
            var results = tmpTree.FindAll(n => n.Depth == 0);
            if ((results.Count > 0))
            {
                // probably they include level 0
                foreach (var r in results)
                {
                    nodes.Add(r);
                    tmpTree.Remove(r);
                }

            }

            if (!Helper.SearchUp)
            {
                // lets get everything that depth is 1 or lower
                results = tmpTree.FindAll(n => n.Depth == 1);
                foreach (var node in results)
                {
                    nodes.Add(node);
                    tmpTree.Remove(node);
                    TraverseDownTreeNode(node,ref tmpTree, 1);
                }

                // if there isnt any immdiate children, then we cant traverse down the tree.
                if ((results.Count == 0))
                {
                    if ((tmpTree.Count > 0))
                    {
                        // since the array is sorted already, we will peak into the first one to find out
                        var nextLowestLevel = tmpTree[0].Depth;
                        results = tmpTree.FindAll(n => n.Depth == nextLowestLevel);
                        foreach (var node in results)
                        {
                            nodes.Add(node);
                            tmpTree.Remove(node);
                            TraverseDownTreeNode(node,ref tmpTree, 1);
                        }

                    }

                }

            }
            else
            {
                results = tmpTree.FindAll(n => n.Depth == 1);
                foreach (var node in results)
                {
                    nodes.Add(node);
                    tmpTree.Remove(node);
                    TraverseUpTreeNode(node, ref tmpTree, 1);
                }

                // if there isnt any immdiate children, then we cant traverse down the tree.
                if (results.Count == 0 && tmpTree.Count > 0)
                {
                    // since the array is sorted already, we will peak into the first one to find out
                    var nextLowestLevel = tmpTree[0].Depth;
                    results = tmpTree.FindAll(n => n.Depth == nextLowestLevel);
                    foreach (var node in results)
                    {
                        nodes.Add(node);
                        tmpTree.Remove(node);
                        TraverseUpTreeNode(node, ref tmpTree, 1);
                    }
                }
            }

            return nodes;
        }

        private int Comparer(CategoryHuntNode n1, CategoryHuntNode n2)
        {
            if ((n1.CategoryId > n2.CategoryId))
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        private CategoryHuntNode TraverseDownTreeNode(CategoryHuntNode node, ref List<CategoryHuntNode> tree, int depth)
        {
            var id = node.CategoryId;
            var tmpNode = node;
            // Dim result = tree.Find(Function(n As CategoryHuntNode) (n.ParentCategoryId = id))
            //  path traversing
            var results = tree.FindAll(n => n.ParentCategoryId == id);
            foreach (var r in results)
            {
                tmpNode.Children.Add(r);
                tmpNode.HasChildren = true;
                tree.Remove(r);
                TraverseDownTreeNode(r,ref tree, depth);
            }

            // before i end the traversing, lets go through the left over items using linage to ensure i capture the left over
            if (node.Depth == 1)
            {
                var leftover = tree.FindAll(n => n.Lineage.Contains("/" + node.CategoryId + "/") && n.ContentTypeId.Equals(node.ContentTypeId));
                if ((leftover.Count > 0))
                {
                    // i only care about the top most level
                    node.HasChildren = true;
                    node.Children.Add(leftover[0]);
                }

                //  if nothing else, this branch has no children
            }

            return tmpNode;
        }

        private CategoryHuntNode TraverseUpTreeNode(CategoryHuntNode node, ref List<CategoryHuntNode> tree, int depth)
        {
            var id = node.ParentCategoryId;
            var tmpNode = node;
            //  path traversing
            var results = tree.FindAll(n => n.CategoryId == id);
            foreach (var r in results)
            {
                tmpNode.Children.Add(r);
                tmpNode.HasChildren = true;
                tree.Remove(r);
                TraverseUpTreeNode(r,ref tree, depth);
            }

            // before i end the traversing, lets go through the left over items using linage to ensure i capture the left over
            // If (node.Depth = 1) Then
            //     Dim leftover As List(Of CategoryHuntNode) = tree.FindAll(Function(n As CategoryHuntNode) (n.Lineage.Contains("/" & node.CategoryId & "/") And n.ContentTypeId.Equals(node.ContentTypeId)))
            //     If (leftover.Count > 0) Then
            //         'i only care about the top most level
            //         node.HasChildren = True
            //         node.Children.Add(leftover.Item(0))
            //     End If
            //     ' if nothing else, this branch has no children
            // End If
            return tmpNode;
        }

        public static string Process(SqlDataReader reader)
        {
            var result = string.Empty;
            while (reader.Read())
            {
                if (string.IsNullOrEmpty(result))
                {
                    result = result + string.Format("\'{0}\'", reader["content_Id"].ToString());
                }
                else
                {
                    result = (result + string.Format(",\'{0}\'", reader["content_Id"].ToString()));
                }

            }

            if (string.IsNullOrEmpty(result))
            {
                result = string.Format("\'{0}\'", Guid.Empty.ToString());
            }

            return result;
        }

        public static string Process(ref SqlDataReader reader, CategorySearchListAndCleanOtherFilterHelper helpers, SqlCommand command)
        {
            var result = string.Empty;
            // this reader contains all content ids that match requested criteria
            // if we hunt, then we will need to clean out other info.
            foreach (var c in helpers.CategorySearchHelpers)
            {
                if (c.IsHunt)
                {
                    // if we search on parent (example product cat)
                    // then the next immediate children have to be there (laptop, desktop and workstation)
                    //    + if next immediate child doesnt exist, that branch becomes orphan (if workstation doesnt exist)
                    //    + if next immediate child exists, but its next children dont exist and its grand children exist, then that branch stays
                    //  and even if i go down 99 levels, i will deal with the immediate children
                    //    + the question is : what do i give so that they have enough info to link to that page.
                    //        + that child category id: so then they can do ct3&cd[thatcatid]i99h
                    // Dim huntTree As CategoryHunt = New CategoryHunt(reader, c)
                    helpers.CategoryHunter = new CategoryHunt(reader, c);
                    if (c.NoStrip)
                    {
                        helpers.CategoryHunter.SwitchNodesToNoStripNodesWithHasChildren();
                        reader.Close();
                        // reader.Dispose()
                        reader = command.ExecuteReader();
                        result = result + Process(reader);
                    }
                    else
                    {
                        result = result + helpers.CategoryHunter.FilterContentIds();
                    }

                }
                else
                {
                    result = result + Process(reader);
                }

            }

            return result;
        }

        private string FilterContentIds()
        {
            //  return only the first level
            var s = string.Empty;
            if ((Nodes.Count == 0))
            {
                return string.Format("\'{0}\'", Guid.Empty.ToString());
            }

            foreach (var n in Nodes)
            {
                if (string.IsNullOrEmpty(s))
                {
                    s = string.Format("\'{0}\'", n.ContentId.ToString());
                }
                else
                {
                    s = string.Format("{0},\'{1}\'", s, n.ContentId.ToString());
                }

            }

            return s;
        }
    }
}