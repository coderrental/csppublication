﻿using System.Collections.Generic;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class CategoryItem //CategoryLoader.vb
    {
        public string CategoryID;
        public int Depth;
        public bool CategoryActive;
        public bool PublicationSchemePublishToday;
        public string ExternalCategoryCode;
        public CategoryItem ParentCategoryItem;
        public List<CategoryItem> ChildCategories;
        public bool IsInTheme = false;
        public ContentPiece AssignedCatContentPiece;

        public CategoryItem(CategoryItem vParentCategoryItem, string vExternalCategoryCode, string vCategoryId,
            int vDepth, bool vCategoryActive, bool vPublicationSchemePublishToday)
        {
            ParentCategoryItem = vParentCategoryItem;
            ExternalCategoryCode = vExternalCategoryCode;
            CategoryID = vCategoryId;
            Depth = vDepth;
            CategoryActive = vCategoryActive;
            PublicationSchemePublishToday = vPublicationSchemePublishToday;
            ChildCategories = new List<CategoryItem>();
        }
    }
}