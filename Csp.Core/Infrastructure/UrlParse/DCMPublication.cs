﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Csp.Core.Infrastructure.DAO.Dto;
using Csp.Core.Infrastructure.UrlParse.Utils;

namespace Csp.Core.Infrastructure.UrlParse
{
    public class DCMPublication
    {
        string fHTMLText;
        Tuple<string, Task<ContentCollection>> fRelatedContent;
        Tuple<string, Task<ContentCollection>> fCurPageContent;
        List<Tuple<string, Task<ContentCollection>>> fMySiblingContentCollections;
        List<HTMLwPubName> fMyRelatedPubHTML;
        public string SourceURL = "";
        long timer;
        public string OriginalSourceDomain = string.Empty;
        public string LngID { get; set; }

        public DCMPublication(string vHtmlText, ref Tuple<string, Task<ContentCollection>> vRelatedContentCollection,
            List<Tuple<string, Task<ContentCollection>>> componentContentCollections,
            List<HTMLwPubName> relatedPubHtml)
        {
            fHTMLText = vHtmlText;
            fRelatedContent = vRelatedContentCollection;
            fCurPageContent = new Tuple<string, Task<ContentCollection>>(fRelatedContent.Item1,
                Task<ContentCollection>.Factory.StartNew(
                    () => { return fRelatedContent.Item2.Result.GetCurPageAsNewContentCollection(); }));
            fMySiblingContentCollections = componentContentCollections;
            fMyRelatedPubHTML = relatedPubHtml;
            timer = DateTime.Now.Ticks;
        }

        private string ReplaceContentTypeFieldInstance(string xDmlCode, ref ContentPiece curContentPiece)
        {
            var contentType = xDmlCode.Substring(xDmlCode.IndexOf(":") + 1);
            contentType = contentType.Substring(0, contentType.IndexOf(":"));
            var contentField = xDmlCode.Substring(xDmlCode.IndexOf(":") + 1);
            contentField = contentField.Substring(contentField.IndexOf(":") + 2);
            contentField = contentField.Substring(0, contentField.IndexOf("}"));
            if (curContentPiece.ContentTypeID == contentType)
            {
                return ContentPieceTypeCreator.GetPropertyAsSafeString(curContentPiece, contentField);
            }
            return "";
        }

        private string ReplaceContentTypeFields(string inHtml, ref ContentPiece curContentPiece)
        {
            var holdHtml = inHtml;
            while (holdHtml.Contains("{contenttype"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{contenttype"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{contenttype"));
                var curContentTypeField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                curContentTypeField = ReplaceContentTypeFieldInstance(curContentTypeField, ref curContentPiece);
                holdHtml = holdLeft + curContentTypeField + holdRight;
            }
            return holdHtml;
        }

        private string ReplaceSupplierFieldInstance(string xDmlCode, ref ContentPiece curContentPiece)
        {
            var supplierField = xDmlCode;
            supplierField = supplierField.Substring(supplierField.IndexOf(":") + 2);
            supplierField = supplierField.Substring(0, supplierField.IndexOf("}"));
            return ConsumerTypeCreator.GetPropertyAsSafeString(curContentPiece.fRelatedSupplierInfo, supplierField);
        }

        private string ReplaceSupplierFields(string inHtml, ref ContentPiece curContentPiece)
        {
            var holdHtml = inHtml;
            while (holdHtml.Contains("{supplier"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{supplier"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{supplier"));
                var curSupplierField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                curSupplierField = ReplaceSupplierFieldInstance(curSupplierField, ref curContentPiece);
                holdHtml = holdLeft + curSupplierField + holdRight;
            }

            return holdHtml;
        }

        private string ReplaceConsumerFieldInstance(string xDmlCode)
        {
            var curConsumerPiece = fRelatedContent.Item2.Result.fBaseDomainData;
            var consumerField = xDmlCode;
            consumerField = consumerField.Substring(consumerField.IndexOf(":") + 2);
            consumerField = consumerField.Substring(0, consumerField.IndexOf("}"));
            return ConsumerTypeCreator.GetPropertyAsSafeString(curConsumerPiece, consumerField);
        }

        private string ReplaceConsumerFields(string inHtml)
        {
            var holdHtml = inHtml;
            while (holdHtml.Contains("{consumer"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{consumer"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{consumer"));
                var curConsumerField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                if (curConsumerField.Equals("{consumer:FDeeplink_Landingpage_Url}",
                    StringComparison.CurrentCultureIgnoreCase))
                {
                    curConsumerField =
                        Functions.AppendHttpToDeepLinkUrl(ReplaceConsumerFieldInstance(curConsumerField), SourceURL);
                }
                else
                {
                    curConsumerField = ReplaceConsumerFieldInstance(curConsumerField);
                }

                holdHtml = holdLeft + curConsumerField + holdRight;
            }

            return holdHtml;
        }

        private string ReplaceVRConsumerFields(string inHtml)
        {
            var holdHtml = inHtml;
            var i = holdHtml.IndexOf("{VR:");
            while (i >= 0)
            {
                var holdLeft = holdHtml.Substring(0, i);
                holdHtml = holdHtml.Substring(i + 4);
                var curConsumerField = holdHtml.Substring(0, holdHtml.IndexOf("}"));
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                holdHtml = holdLeft + "{" + curConsumerField + "}" + holdRight;
                i = holdHtml.IndexOf("{VR:");
            }

            return holdHtml;
        }

        private string ReplaceFFieldWithValue(string inContentField, ref ContentPiece curContentPiece)
        {
            // takes in {FContentTypeID} returns the value only
            var holdContentField = inContentField.Substring(2);
            //  Trim off {F
            holdContentField = holdContentField.Substring(0, holdContentField.IndexOf("}"));
            holdContentField = ContentPieceTypeCreator.GetPropertyAsSafeString(curContentPiece, holdContentField);
            return holdContentField;
        }

        private string ReplaceFFieldsSB(string inHtml, ref ContentPiece curContentPiece)
        {
            //  looks for all {fField} and replaces them with GetPropertyAsSafeString for that field
            if (!inHtml.Contains("{F"))
            {
                return inHtml;
            }

            var holdHtml = inHtml;
            var holdCcp = curContentPiece;
            var r = "";
            var sb = new StringBuilder(inHtml.Length * 2);
            while (holdHtml.Contains("{F"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{F"));
                sb.Append(holdLeft);
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{F"));
                r = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                var curContentField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                curContentField = ReplaceFFieldWithValue(curContentField, ref curContentPiece);
                sb.Append(curContentField);
                holdHtml = r;
            }

            sb.Append(r);
            return sb.ToString();
        }

        private string ReplaceFFieldsRecursive(string inHtml, ref ContentPiece curContentPiece)
        {
            var holdHtml = inHtml;
            var holdCcp = curContentPiece;
            while (holdHtml.Contains("{F"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{F"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{F"));
                var right = Task<string>.Factory.StartNew(() =>
                {
                    var r = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                    var hc = holdCcp;
                    return ReplaceFFieldsRecursive(r, ref hc);
                });
                var curContentField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                curContentField = ReplaceFFieldWithValue(curContentField, ref curContentPiece);
                holdHtml = holdLeft + curContentField + right.Result;
            }

            return holdHtml;
        }

        private string ReplaceFFields(string inHtml, ref ContentPiece curContentPiece)
        {
            var holdHtml = inHtml;
            while (holdHtml.Contains("{F"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{F"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{F"));
                var curContentField = holdHtml.Substring(0, holdHtml.IndexOf("}") + 1);
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                curContentField = ReplaceFFieldWithValue(curContentField, ref curContentPiece);
                holdHtml = holdLeft + curContentField + holdRight;
            }

            return holdHtml;
        }

        private List<OnChangeSupportObj> GetOnChangeListReplaceWLandmarks(ref string inHtml)
        {
            var holdHtml = inHtml;
            var onChangeCount = 0;
            var holdOnChangeObjs = new List<OnChangeSupportObj>();
            while (holdHtml.Contains("{OnChange:"))
            {
                onChangeCount = onChangeCount + 1;
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{OnChange:"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{OnChange:"));
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("{/OnChange}") + 11);
                var holdOnChangeHtml = holdHtml.Substring(0, holdHtml.IndexOf("{/OnChange}"));
                var holdFieldName = holdOnChangeHtml.Substring(holdOnChangeHtml.IndexOf(":") + 1);
                holdFieldName = holdFieldName.Substring(0, holdFieldName.IndexOf("}"));
                holdFieldName = holdFieldName.Substring(1);
                //  trim off F
                holdOnChangeHtml = holdOnChangeHtml.Substring(holdOnChangeHtml.IndexOf("}") + 1);
                //  trim off leading {onchange:bla}
                holdOnChangeObjs.Add(new OnChangeSupportObj(holdOnChangeHtml, holdFieldName,
                    "OnChangeNum" + onChangeCount));
                holdHtml = holdLeft + "{*OnChangeNum" + (onChangeCount + ("*}" + holdRight));
            }

            inHtml = holdHtml;
            //  decoraded with OnChange num landmarks for future processing
            return holdOnChangeObjs;
        }

        private string ReplaceOnChangeLandmarksWithHTMLAsNeeded(string inHtml, ref ContentPiece curContentPiece,
            ref List<OnChangeSupportObj> curOnChangeObjList)
        {
            var holdHtml = inHtml;
            while (holdHtml.Contains("{*OnChange"))
            {
                var holdLeft = holdHtml.Substring(0, holdHtml.IndexOf("{*OnChange"));
                holdHtml = holdHtml.Substring(holdHtml.IndexOf("{*OnChange"));
                var holdRight = holdHtml.Substring(holdHtml.IndexOf("}") + 1);
                var holdOnChangeLandmark = holdHtml.Substring(holdHtml.IndexOf("{*") + 2);
                holdOnChangeLandmark = holdOnChangeLandmark.Substring(0, holdOnChangeLandmark.IndexOf("*}"));
                var holdOnChangeResult = OnChangeSupportObj.GetHTMLForOnChangeIDUsingContentPiece(holdOnChangeLandmark,
                    ref curOnChangeObjList, ref curContentPiece);
                holdLeft = holdLeft + holdOnChangeResult;
                holdHtml = holdLeft + holdRight;
            }

            return holdHtml;
        }

        private List<CategoryLeafRepeatBlock> ReplaceRepeatBlockWithLandMark(ref string inHtml)
        {
            if (inHtml.Contains("{repeat}"))
            {
                var counter = 0;
                var holdBlocks = new List<CategoryLeafRepeatBlock>();
                while (inHtml.Contains("{repeat}"))
                {
                    counter = counter + 1;
                    var holdLeft = inHtml.Substring(0, inHtml.IndexOf("{repeat}"));
                    var holdRight = inHtml.Substring(inHtml.IndexOf("{/repeat}") + 9);
                    var holdRepeat = inHtml.Substring(inHtml.IndexOf("{repeat}"));
                    holdRepeat = holdRepeat.Substring(0, holdRepeat.IndexOf("{/repeat}") + 9);
                    inHtml = holdLeft + "%$^$RepeatLandmark" + counter + holdRight;
                    holdBlocks.Add(new CategoryLeafRepeatBlock(holdRepeat, "%$^$RepeatLandmark" + counter));
                    //  hold repeat should have just the repeat group now
                }

                return holdBlocks;
            }
            return null;
        }

        private string ReplaceCategoryRepeatBlockLandmarks(string InHTML, ref List<CategoryLeafRepeatBlock> HoldBlocks)
        {
            if (!(HoldBlocks == null))
            {
                var HoldHTML = InHTML;
                foreach (var item in HoldBlocks)
                {
                    HoldHTML = HoldHTML.Replace(item.RepeatLandmark, item.RepeatText);
                }

                return HoldHTML;
            }
            else
            {
                return InHTML;
            }
        }

        private string ReplaceCatSpecificFields(string InHTML, string DepthIndent, ref CategoryItem InCatItem)
        {
            var HoldResult = InHTML;
            while (HoldResult.Contains("{cInternalCatID}"))
            {
                HoldResult = HoldResult.Replace("{cInternalCatID}", InCatItem.CategoryID);
            }

            while (HoldResult.Contains("{cExternalCatID}"))
            {
                HoldResult = HoldResult.Replace("{cExternalCatID}", InCatItem.ExternalCategoryCode);
            }

            if (!(InCatItem.AssignedCatContentPiece == null))
            {
                // add code here to pull out anything inside {repeat}{/repeat} 
                var HoldBlocks = this.ReplaceRepeatBlockWithLandMark(ref HoldResult);
                HoldResult = this.DoInnerxDMLReplacement(HoldResult, ref InCatItem.AssignedCatContentPiece, true);
                //  add code here to put it back in!
                HoldResult = ReplaceCategoryRepeatBlockLandmarks(HoldResult, ref HoldBlocks);
            }

            while (HoldResult.Contains("{DI}"))
            {
                HoldResult = HoldResult.Replace("{DI}", DepthIndent);
            }

            //  add call here for {repeat for catid}
            return HoldResult;
        }

        private string LoopThroughCatTree(string InCatRepeatText, string CleanIndentText,
            ref CategoryItem CurCategoryItem, int InDepth)
        {
            var HoldResult = "";
            var CleanInText = InCatRepeatText;
            if (InCatRepeatText.Contains("{RemoveEmptyCategories}"))
            {
                InCatRepeatText = InCatRepeatText.Replace("{RemoveEmptyCategories}", "");
                if (this.fRelatedContent.Item2.Result.IsCategoryEmptyInMyCollection(ref CurCategoryItem))
                {
                    // If Me.fRelatedContent.IsCategoryEmptyInMyCollection(CurCategoryItem) Then
                    return "";
                }
            }

            var EnforceTheme = false;
            if (InCatRepeatText.Contains("{RemoveLeafsNotInTheme}"))
            {
                EnforceTheme = true;
                InCatRepeatText = InCatRepeatText.Replace("{RemoveLeafsNotInTheme}", "");
            }

            var CurDepthIndenter = "";
            var CurDepth = InDepth + 1;
            for (var index = 1; index <= CurDepth; index++)
            {
                CurDepthIndenter = CurDepthIndenter + CleanIndentText;
            }

            if (EnforceTheme == true
                && CurCategoryItem.IsInTheme == false)
            {
                HoldResult = "";
            }
            else if (EnforceTheme == true
                     && CurCategoryItem.IsInTheme == true
                     || EnforceTheme == false)
            {
                HoldResult = this.ReplaceCatSpecificFields(InCatRepeatText, CurDepthIndenter, ref CurCategoryItem);
                //  replace current leaf
            }
            else
            {
                // shouldn't happen
                //  do the normal process
                HoldResult = this.ReplaceCatSpecificFields(InCatRepeatText, CurDepthIndenter, ref CurCategoryItem);
                //  replace current leaf
            }

            // HoldResult = Me.ReplaceCatSpecificFields(InCatRepeatText, CurDepthIndenter, CurCategoryItem)
            foreach (var item in CurCategoryItem.ChildCategories)
            {
                var curCategoryItem = item;
                HoldResult = HoldResult +
                             this.LoopThroughCatTree(CleanInText, CleanIndentText, ref curCategoryItem, CurDepth);
            }

            return HoldResult;
        }

        private string ReplaceCatTreeRepeat(string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var HoldRepeatText = "";
            var HoldRepeatBlock = "";
            var HoldRepeatOnChangeText = "";
            while (HoldHTML.Contains("{RepeatCatTree}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{RepeatCatTree}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{RepeatCatTree}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/RepeatCatTree}") + 16);
                HoldRepeatText = HoldHTML.Substring(HoldHTML.IndexOf("{RepeatCatTree}") + 15);
                HoldRepeatText = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{/RepeatCatTree}"));
                // HoldRepeatText should just = the stuff between {repeatcattree} and {/repeatcattree}
                // grab DepthIndenter if any
                var DepthIndenter = "";
                if (HoldRepeatText.Contains("{DepthIndenter}"))
                {
                    // pull out value in depth indenter store in local var, remove depth intenter tags from Holdrepeat text
                    DepthIndenter = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{DepthIndenter}"));
                    DepthIndenter = DepthIndenter.Substring(15);
                    // trim off depth indenter leading tag
                    DepthIndenter = DepthIndenter.Substring(0, DepthIndenter.IndexOf("{/DepthIndenter}"));
                    //  Depth Indenter should now = just the stuff in the middle
                    var HoldRepeatLeft = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{DepthIndenter}"));
                    HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{/DepthIndenter}") + 16);
                    HoldRepeatText = HoldRepeatLeft + HoldRepeatText;
                    // Now I should have depth indenter in one var, and the rest of the repeating block in the other.
                    // time to loop thorugh my associated categories!
                }

                HoldRepeatText = this.LoopThroughCatTree(HoldRepeatText, DepthIndenter,
                    ref this.fRelatedContent.Item2.Result.fMyCategories.CleanTree, 0);
                // HoldRepeatText = Me.LoopThroughCatTree(HoldRepeatText, DepthIndenter, Me.fRelatedContent.fMyCategories.CleanTree, 0)
                HoldHTML = HoldLeft + HoldRepeatText + HoldRight;
            }

            return HoldHTML;
        }

        private string ReplaceRepeatGroups(string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var HoldRepeatText = "";
            var HoldRepeatBlock = "";
            var HoldRepeatOnChangeText = "";
            var CategoryFilter = "";

            while (HoldHTML.Contains("{repeat}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{repeat}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{repeat}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/repeat}") + 9);
                HoldRepeatText = HoldHTML.Substring(HoldHTML.IndexOf("{repeat}") + 8);
                HoldRepeatText = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{/repeat}"));
                if (HoldRepeatText.Contains("{CatFilter:"))
                {
                    CategoryFilter = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{CatFilter:"));
                    var HoldRLeft = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{CatFilter:"));
                    CategoryFilter = CategoryFilter.Substring(0, CategoryFilter.IndexOf("}"));
                    // 
                    HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{CatFilter:"));
                    HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("}") + 1);
                    //  should = the string value after the cat filter
                    HoldRepeatText = HoldRLeft + HoldRepeatText;
                    // pulled out cat filter
                    CategoryFilter = CategoryFilter.Substring(CategoryFilter.IndexOf(":") + 1);
                }
                List<OnChangeSupportObj> OnChangeObjList;
                if (HoldRepeatText.Contains("{OnChange:"))
                {
                    OnChangeObjList = GetOnChangeListReplaceWLandmarks(ref HoldRepeatText);
                }
                else
                {
                    OnChangeObjList = null;
                }

                var HoldInnerRepeatText = HoldRepeatText;
                List<InnerRepeatBlock> InnerRepeatList = null;
                while (HoldInnerRepeatText.Contains("{InnerRepeat"))
                {
                    if (InnerRepeatList == null)
                    {
                        InnerRepeatList = new List<InnerRepeatBlock>();
                    }

                    InnerRepeatList.Add(new InnerRepeatBlock(ref HoldInnerRepeatText));
                }

                if (InnerRepeatList == null)
                {
                    var OkToRepeat = false;
                    if (!(OnChangeObjList == null))
                    {
                        foreach (var Item in fCurPageContent.Item2.Result.fMyContent)
                        {
                            // For Each Item As ContentPiece In fCurPageContent.fMyContent
                            OkToRepeat = false;
                            if (CategoryFilter == "")
                            {
                                OkToRepeat = true;
                            }
                            else
                            {
                                //  should be filtered by category
                                foreach (var Cat in Item.fCategoriesImAssignedTo)
                                {
                                    if (Cat == CategoryFilter)
                                    {
                                        OkToRepeat = true;
                                        break;
                                    }
                                }
                            }

                            if (OkToRepeat)
                            {
                                if (!(OnChangeObjList == null))
                                {
                                    var curContentPiece = Item;
                                    HoldRepeatOnChangeText = ReplaceOnChangeLandmarksWithHTMLAsNeeded(HoldRepeatText,
                                        ref curContentPiece, ref OnChangeObjList);
                                }
                                else
                                {
                                    HoldRepeatOnChangeText = HoldRepeatText;
                                }

                                var inContentPiece = Item;
                                HoldRepeatBlock =
                                    this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                                HoldLeft = HoldLeft + HoldRepeatBlock;
                            }
                        }
                    }
                    else
                    {
                        //panda
                        IEnumerable<Tuple<int, string>> Query = fCurPageContent.Item2.Result.fMyContent.AsParallel()
                            .Select(
                                (inItem, inindex) =>
                                {
                                    var Item = inItem;
                                    var Index = inindex;
                                    var HoldRText = HoldRepeatText;
                                    // HoldRepeatBlock = Me.DoInnerxDMLReplacement(HoldRText, Item)
                                    // HoldLeft = HoldLeft + HoldRepeatBlock
                                    return new Tuple<int, string>(Index,
                                        this.DoInnerxDMLReplacement(HoldRText, ref Item, false));
                                });
                        //panda

                        var sb = new StringBuilder();
                        sb.Append(HoldLeft);
                        var sorted = Query.OrderBy(x => x.Item1);
                        foreach (var HoldRepeatBlockItem in sorted)
                        {
                            // HoldLeft = HoldLeft + HoldRepeatBlockItem.Item2
                            sb.Append(HoldRepeatBlockItem.Item2);
                        }

                        HoldLeft = sb.ToString();
                        HoldLeft = Functions.EscapeChars(HoldLeft);
                    }
                }
                else
                {
                    var CurStep = 0;
                    var OkToRepeat = false;
                    foreach (var Item in fCurPageContent.Item2.Result.fMyContent)
                    {
                        // For Each Item As ContentPiece In fCurPageContent.fMyContent
                        OkToRepeat = false;
                        if (CategoryFilter == "")
                        {
                            OkToRepeat = true;
                        }
                        else
                        {
                            //  should be filtered by category
                            foreach (var Cat in Item.fCategoriesImAssignedTo)
                            {
                                if (Cat == CategoryFilter)
                                {
                                    OkToRepeat = true;
                                    break;
                                }
                            }
                        }

                        if (OkToRepeat)
                        {
                            HoldRepeatText = InnerRepeatList[CurStep].WithDataRepeatBlock;
                            if (!(OnChangeObjList == null))
                            {
                                string HoldCurValueOfField;
                                var curContentPiece = Item;
                                HoldRepeatOnChangeText = ReplaceOnChangeLandmarksWithHTMLAsNeeded(HoldRepeatText,
                                    ref curContentPiece, ref OnChangeObjList);
                                foreach (var OItem in OnChangeObjList)
                                {
                                    HoldCurValueOfField =
                                        ContentPieceTypeCreator.GetPropertyAsSafeString(Item, OItem.FieldName);
                                    OItem.ShouldBeRenderedThisTime(HoldCurValueOfField);
                                    //  added to keep on changes up to date inside of inner repeat blocks where an inner repeat block may not contain every onchangelandmark object
                                }
                            }
                            else
                            {
                                HoldRepeatOnChangeText = HoldRepeatText;
                            }

                            var inContentPiece = Item;
                            HoldRepeatBlock =
                                this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                            HoldLeft = HoldLeft + HoldRepeatBlock;
                            CurStep = CurStep + 1;
                            if (CurStep + 1 > InnerRepeatList.Count)
                            {
                                CurStep = 0;
                            }
                        }

                        // ok to repeat
                    }

                    while (!(CurStep + 1 > InnerRepeatList.Count)
                           && !(CurStep == 0))
                    {
                        HoldRepeatOnChangeText = InnerRepeatList[CurStep].WithoutDataRepeatBlock;
                        ContentPiece inContentPiece = null;
                        HoldRepeatBlock = this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                        HoldLeft = HoldLeft + HoldRepeatBlock;
                        CurStep = CurStep + 1;
                    }
                }

                HoldHTML = HoldLeft + HoldRight;
            }

            return HoldHTML;
        }

        private List<RepeatValueCountPair> GetRepeatValueCountPairListFor(string InField)
        {
            var RepeatValueCountList = new List<RepeatValueCountPair>();
            var HoldValue = "";
            foreach (var Item in this.fRelatedContent.Item2.Result.fMyContent.ToList())
            {
                // For Each Item As ContentPiece In Me.fRelatedContent.fMyContent
                HoldValue = ContentPieceTypeCreator.GetPropertyAsSafeString(Item, InField);
                if (HoldValue != "")
                {
                    RepeatValueCountPair.GetOrAddRepeatValueCountPairToList(HoldValue, ref RepeatValueCountList);
                }
            }

            return RepeatValueCountList;
        }

        private string ReplacePoundFieldCounts(string InHTML)
        {
            //  looks for all {#FPrice:49.95} replaces with a count of all content pieces that have that property with that value
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var CurContentField = "";
            var CurContentValue = "";
            var CurHoldValue = "";
            var Count = 0;
            while (HoldHTML.Contains("{#F"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{#F"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{#F"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                CurContentField = HoldHTML.Substring(0, HoldHTML.IndexOf(":"));
                CurContentField = CurContentField.Substring(3);
                // skip {#F
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf(":") + 1);
                CurContentValue = HoldHTML.Substring(0, HoldHTML.IndexOf("}"));
                foreach (var Item in this.fRelatedContent.Item2.Result.fMyContent)
                {
                    // For Each Item As ContentPiece In Me.fRelatedContent.fMyContent
                    CurHoldValue = ContentPieceTypeCreator.GetPropertyAsSafeString(Item, CurContentField);
                    if (CurHoldValue == CurContentValue)
                    {
                        Count = Count + 1;
                    }
                }

                // CurContentField = ReplaceFFieldWithValue(CurContentField, CurContentPiece)
                HoldHTML = HoldLeft
                           + (Count + HoldRight);
            }

            return HoldHTML;
        }

        private string ReplaceValueCounts(string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var HoldRepeatText = "";
            var HoldRepeatField = "";
            var HoldRepeatOutput = "";
            var HoldRepeatOutputLeft = "";
            var HoldRepeatOutputRight = "";
            var HoldRepeatCount = "";
            var HoldRepeatSortOrder = "";
            while (HoldHTML.Contains("{repeat:"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{repeat:"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{repeat:"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/repeat}") + 9);
                HoldRepeatText = HoldHTML.Substring(HoldHTML.IndexOf("{repeat:") + 8);
                HoldRepeatText = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{/repeat}"));
                // HoldRepeatText should just = the stuff between {repeat: and {/repeat} "FContentTypeID:3:D}ContentType Name: {RepeatValue} - ({RepeatValueCount})"
                HoldRepeatField = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf(":"));
                HoldRepeatField = HoldRepeatField.Substring(1);
                //  trim off F
                var HoldValueCountPairList = GetRepeatValueCountPairListFor(HoldRepeatField);
                HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf(":") + 1);
                // trim off field
                HoldRepeatCount = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf(":"));
                HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf(":") + 1);
                // trim off Count
                HoldRepeatSortOrder = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("}"));
                HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("}") + 1);
                //  Should only contain ContentType Name: {RepeatValue} - ({RepeatValueCount})
                HoldValueCountPairList.Sort();
                var IntCount = 0;
                int.TryParse(HoldRepeatCount, out IntCount);
                var CurCount = 0;
                if (HoldRepeatSortOrder == "D")
                {
                    RepeatValueCountPair Item = null;
                    var index = HoldValueCountPairList.Count - 1;
                    while (index >= 0)
                    {
                        CurCount = CurCount + 1;
                        if (CurCount <= IntCount)
                        {
                            Item = HoldValueCountPairList[index];
                            HoldRepeatOutput = HoldRepeatText;
                            HoldRepeatOutputLeft = "";
                            HoldRepeatOutputRight = "";
                            while (HoldRepeatOutput.Contains("{RepeatValue}"))
                            {
                                HoldRepeatOutputLeft =
                                    HoldRepeatOutput.Substring(0, HoldRepeatOutput.IndexOf("{RepeatValue}"));
                                HoldRepeatOutputRight =
                                    HoldRepeatOutput.Substring(HoldRepeatOutput.IndexOf("{RepeatValue}") + 13);
                                HoldRepeatOutput = HoldRepeatOutputLeft + Item.Value + HoldRepeatOutputRight;
                            }

                            HoldRepeatOutputLeft = "";
                            HoldRepeatOutputRight = "";
                            while (HoldRepeatOutput.Contains("{RepeatValueCount}"))
                            {
                                HoldRepeatOutputLeft =
                                    HoldRepeatOutput.Substring(0, HoldRepeatOutput.IndexOf("{RepeatValueCount}"));
                                HoldRepeatOutputRight =
                                    HoldRepeatOutput.Substring(HoldRepeatOutput.IndexOf("{RepeatValueCount}") + 18);
                                HoldRepeatOutput = HoldRepeatOutputLeft
                                                   + (Item.Count + HoldRepeatOutputRight);
                            }

                            HoldLeft = HoldLeft + HoldRepeatOutput;
                        }

                        index = index - 1;
                    }
                }
                else
                {
                    // sort order A
                    foreach (var Item in HoldValueCountPairList)
                    {
                        CurCount = CurCount + 1;
                        if (CurCount <= IntCount)
                        {
                            HoldRepeatOutput = HoldRepeatText;
                            HoldRepeatOutputLeft = "";
                            HoldRepeatOutputRight = "";
                            while (HoldRepeatOutput.Contains("{RepeatValue}"))
                            {
                                HoldRepeatOutputLeft =
                                    HoldRepeatOutput.Substring(0, HoldRepeatOutput.IndexOf("{RepeatValue}"));
                                HoldRepeatOutputRight =
                                    HoldRepeatOutput.Substring(HoldRepeatOutput.IndexOf("{RepeatValue}") + 13);
                                HoldRepeatOutput = HoldRepeatOutputLeft + Item.Value + HoldRepeatOutputRight;
                            }

                            HoldRepeatOutputLeft = "";
                            HoldRepeatOutputRight = "";
                            while (HoldRepeatOutput.Contains("{RepeatValueCount}"))
                            {
                                HoldRepeatOutputLeft =
                                    HoldRepeatOutput.Substring(0, HoldRepeatOutput.IndexOf("{RepeatValueCount}"));
                                HoldRepeatOutputRight =
                                    HoldRepeatOutput.Substring(HoldRepeatOutput.IndexOf("{RepeatValueCount}") + 18);
                                HoldRepeatOutput = HoldRepeatOutputLeft
                                                   + (Item.Count + HoldRepeatOutputRight);
                            }

                            HoldLeft = HoldLeft + HoldRepeatOutput;
                        }
                    }
                }

                //  sort order D
                HoldHTML = HoldLeft + HoldRight;
            }

            return HoldHTML;
        }

        private string ReplacePrevPageCode(string InHTML)
        {
            var NeedsPrevPage = this.fRelatedContent.Item2.Result.NeedsPriorPage();
            var HoldHTML = InHTML;
            var HoldPrevPageURL = "";
            var HoldLeft = "";
            var HoldRight = "";
            var HoldPageText = "";
            while (HoldHTML.Contains("{paging:previous}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:previous}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:previous}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:previous}") + 18);
                if (NeedsPrevPage)
                {
                    if (HoldPrevPageURL == "")
                    {
                        HoldPrevPageURL = this.fRelatedContent.Item2.Result.GetPageXsURL(
                            ref this.fMySiblingContentCollections,
                            this.fRelatedContent.Item2.Result.fPageParams.CurrentPageOffset - 1);
                    }

                    HoldPageText = HoldHTML.Substring(HoldHTML.IndexOf("{paging:previous}") + 17);
                    HoldPageText = HoldPageText.Substring(0, HoldPageText.IndexOf("{/paging:previous}"));
                    HoldPageText = HoldPageText.Replace("{paging:PURL}", HoldPrevPageURL);
                    HoldHTML = HoldLeft + HoldPageText + HoldRight;
                }
                else
                {
                    HoldHTML = HoldLeft + HoldRight;
                }

                // HoldRepeatText should just = the stuff between {repeat} and {/repeat}
            }

            return HoldHTML;
        }

        private string ReplaceNextPageCode(string InHTML)
        {
            var NeedsNextPage = this.fRelatedContent.Item2.Result.NeedsNextPage();
            // Dim NeedsNextPage As Boolean = Me.fRelatedContent.NeedsNextPage()
            var HoldHTML = InHTML;
            var HoldNextPageURL = "";
            var HoldLeft = "";
            var HoldRight = "";
            var HoldPageText = "";
            while (HoldHTML.Contains("{paging:next}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:next}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:next}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:next}") + 14);
                if (NeedsNextPage)
                {
                    if (HoldNextPageURL == "")
                    {
                        HoldNextPageURL = this.fRelatedContent.Item2.Result.GetPageXsURL(
                            ref this.fMySiblingContentCollections,
                            this.fRelatedContent.Item2.Result.fPageParams.CurrentPageOffset + 1);
                        // HoldNextPageURL = Me.fRelatedContent.GetPageXsURL(Me.fMySiblingContentCollections, Me.fRelatedContent.fPageParams.CurrentPageOffset + 1)
                    }

                    HoldPageText = HoldHTML.Substring(HoldHTML.IndexOf("{paging:next}") + 13);
                    HoldPageText = HoldPageText.Substring(0, HoldPageText.IndexOf("{/paging:next}"));
                    HoldPageText = HoldPageText.Replace("{paging:PURL}", HoldNextPageURL);
                    HoldHTML = HoldLeft + HoldPageText + HoldRight;
                }
                else
                {
                    HoldHTML = HoldLeft + HoldRight;
                }

                // HoldRepeatText should just = the stuff between {repeat} and {/repeat}
            }

            return HoldHTML;
        }

        private string ReplaceCurrentPageCode(string InHTML)
        {
            // {paging:currentpage}
            var HoldHTML = InHTML;
            while (HoldHTML.Contains("{paging:currentpage}"))
            {
                HoldHTML = HoldHTML.Replace("{paging:currentpage}",
                    this.fRelatedContent.Item2.Result.fPageParams.CurrentPageOffset.ToString());
            }

            return HoldHTML;
        }

        private string ReplaceTotalPageCode(string InHTML)
        {
            // {paging:currentpage}
            var HoldHTML = InHTML;
            while (HoldHTML.Contains("{paging:totalpages}"))
            {
                HoldHTML = HoldHTML.Replace("{paging:totalpages}",
                    this.fRelatedContent.Item2.Result.TotalActualPages().ToString());
            }

            return HoldHTML;
        }

        private string ReplaceRawURLPageCode(string InHTML)
        {
            // {paging:currentpage}
            var HoldHTML = InHTML;
            while (HoldHTML.Contains("{PageRawURL}"))
            {
                if (string.IsNullOrEmpty(this.OriginalSourceDomain))
                {
                    HoldHTML = HoldHTML.Replace("{PageRawURL}", this.SourceURL);
                }
                else
                {
                    HoldHTML = HoldHTML.Replace("{PageRawURL}", this.OriginalSourceDomain);
                }
            }

            return HoldHTML;
        }

        private string GetPagingDelimeter(ref string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldDelim = "";
            var HoldLeft = "";
            var HoldRight = "";
            if (HoldHTML.Contains("{paging:Delimiter}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:Delimiter}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:Delimiter}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:Delimiter}") + 19);
                HoldDelim = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                HoldDelim = HoldDelim.Substring(0, HoldDelim.IndexOf("{/paging:Delimiter}"));
                HoldHTML = HoldLeft + HoldRight;
            }

            InHTML = HoldHTML;
            return HoldDelim;
        }

        private string GetPagingNotCurrentText(ref string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldNotCur = "";
            var HoldLeft = "";
            var HoldRight = "";
            if (HoldHTML.Contains("{paging:NotCurrent}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:NotCurrent}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:NotCurrent}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:NotCurrent}") + 20);
                HoldNotCur = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                HoldNotCur = HoldNotCur.Substring(0, HoldNotCur.IndexOf("{/paging:NotCurrent}"));
                HoldHTML = HoldLeft + HoldRight;
            }

            InHTML = HoldHTML;
            return HoldNotCur;
        }

        private string GetPagingCurrentText(ref string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldCur = "";
            var HoldLeft = "";
            var HoldRight = "";
            if (HoldHTML.Contains("{paging:Current}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:Current}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:Current}"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:Current}") + 17);
                HoldCur = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                HoldCur = HoldCur.Substring(0, HoldCur.IndexOf("{/paging:Current}"));
                HoldHTML = HoldLeft + HoldRight;
            }

            InHTML = HoldHTML;
            return HoldCur;
        }

        private string ReplacePURLPNUMCodes(string InHTML, int PageNum, string PageURL)
        {
            var HoldHTML = InHTML;
            HoldHTML = HoldHTML.Replace("{paging:PURL}", PageURL);
            HoldHTML = HoldHTML.Replace("{paging:PNUM}", PageNum.ToString());
            return HoldHTML;
        }

        private string GeneratePagingIndex(string sWidth, string Cur, string NotCur, string Delim)
        {
            var CurrentPage = this.fRelatedContent.Item2.Result.fPageParams.CurrentPageOffset;
            var TotalPages = this.fRelatedContent.Item2.Result.TotalActualPages();
            var Width = int.Parse(sWidth);
            var LastPage = 0;
            var FirstPage = 0;
            var WasEven = false;
            if (Width % 2
                == 0)
            {
                WasEven = true;
                Width = Width - 1;
                // make it odd!
            }

            LastPage = (Width - 1)
                       / 2
                       + CurrentPage;
            FirstPage = CurrentPage
                        - (Width - 1)
                        / 2;
            while (FirstPage < 1)
            {
                FirstPage = FirstPage + 1;
                LastPage = LastPage + 1;
                // tack extra leading pages to end!
            }

            if (WasEven)
            {
                // add an extra page!
                LastPage = LastPage + 1;
            }

            while (LastPage > TotalPages)
            {
                LastPage = LastPage - 1;
                FirstPage = FirstPage - 1;
                //  tack a page onto the front
            }

            if (FirstPage < 1)
            {
                FirstPage = 1;
                //  Trim off non existing pages as needed
            }

            var OutPagingIndex = "";
            var OutPageURL = "";
            for (var index = FirstPage; index <= LastPage; index++)
            {
                if (index == CurrentPage)
                {
                    if (index == FirstPage)
                    {
                        OutPagingIndex = ReplacePURLPNUMCodes(Cur, index, "");
                    }
                    else
                    {
                        OutPagingIndex = OutPagingIndex + Delim + ReplacePURLPNUMCodes(Cur, index, "");
                    }
                }
                else
                {
                    OutPageURL =
                        this.fRelatedContent.Item2.Result.GetPageXsURL(ref this.fMySiblingContentCollections, index);
                    if (index == FirstPage)
                    {
                        OutPagingIndex = ReplacePURLPNUMCodes(NotCur, index, OutPageURL);
                    }
                    else
                    {
                        OutPagingIndex = OutPagingIndex + Delim + ReplacePURLPNUMCodes(NotCur, index, OutPageURL);
                    }
                }
            }

            return OutPagingIndex;
            // HoldPrevPageURL = Me.fRelatedContent.GetPageXsURL(Me.fMySiblingContentCollections, Me.fRelatedContent.fPageParams.CurrentPageOffset - 1)
        }

        private string ReplacePagingIndex(string InHTML)
        {
            var HoldHTML = InHTML;
            object HoldPrevPageURL = "";
            string HoldDelim;
            var HoldLeft = "";
            var HoldRight = "";
            var HoldPagingIndexText = "";
            var HoldBackupPagingIndexText = "";
            var HoldPagingWidth = "";
            var HoldNotCur = "";
            var HoldCur = "";
            while (HoldHTML.Contains("{paging:index"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:index"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{paging:index"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/paging:index}") + 15);
                HoldPagingIndexText = HoldHTML.Substring(HoldHTML.IndexOf(":") + 1);
                HoldPagingIndexText = HoldPagingIndexText.Substring(HoldPagingIndexText.IndexOf(":") + 1);
                HoldPagingWidth = HoldPagingIndexText.Substring(0, HoldPagingIndexText.IndexOf("}"));
                HoldPagingIndexText = HoldPagingIndexText.Substring(HoldPagingIndexText.IndexOf("}") + 1);
                HoldPagingIndexText = HoldPagingIndexText.Substring(0, HoldPagingIndexText.IndexOf("{/paging:index}"));
                HoldBackupPagingIndexText = HoldPagingIndexText;
                HoldDelim = GetPagingDelimeter(ref HoldPagingIndexText);
                HoldNotCur = GetPagingNotCurrentText(ref HoldPagingIndexText);
                HoldCur = GetPagingCurrentText(ref HoldPagingIndexText);
                if (HoldCur != "" && HoldNotCur != "" && HoldDelim != "")
                {
                    HoldLeft = HoldLeft + GeneratePagingIndex(HoldPagingWidth, HoldCur, HoldNotCur, HoldDelim);
                }
                else
                {
                    throw new Exception("Paging Index Code error missing codes! IndexCode:" +
                                        HoldBackupPagingIndexText + " CurrentCode:" + HoldCur + " NotCurrentCode:" +
                                        HoldNotCur + " Delimiter:" + HoldDelim);
                }

                HoldHTML = HoldLeft + HoldRight;
            }

            return HoldHTML;
        }

        private string RemoveAllPagingCodes(string InHTML)
        {
            var HoldHTML = InHTML;
            // Private Function RemoveFoundNotFound(ByVal StartString As String, ByVal EndString As String, ByVal InHTML As String) As String
            HoldHTML = this.RemoveFoundNotFound("{paging:next}", "{/paging:next}", HoldHTML);
            HoldHTML = this.RemoveFoundNotFound("{paging:previous}", "{/paging:previous}", HoldHTML);
            // HoldHTML = Me.RemoveFoundNotFound("{paging:}", "{/paging:next}", HoldHTML) paging index!
            HoldHTML = this.RemoveTag("{paging:totalpages}", HoldHTML);
            HoldHTML = this.RemoveTag("{paging:currentpage}", HoldHTML);
            var HoldLeftSide = "";
            var HoldRightSide = "";
            var EndString = "{/paging:index}";
            var LenOfFinish = EndString.Length;
            while (HoldHTML.Contains("{paging:index"))
            {
                HoldLeftSide = HoldHTML.Substring(0, HoldHTML.IndexOf("{paging:index"));
                HoldRightSide = HoldHTML.Substring(HoldHTML.IndexOf("{paging:index") + 13);
                HoldRightSide = HoldRightSide.Substring(HoldRightSide.IndexOf("}") + 1);
                //  trim off leading paging has dynamic length
                HoldRightSide = HoldRightSide.Substring(HoldRightSide.IndexOf(EndString) + LenOfFinish);
                HoldHTML = HoldLeftSide + HoldRightSide;
            }

            return HoldHTML;
        }

        private string ReplacePagingCodes(string InHTML)
        {
            var HoldHTML = InHTML;
            if (this.fRelatedContent.Item2.Result.fPageParams.CountPerPage != 0)
            {
                HoldHTML = ReplaceNextPageCode(HoldHTML);
                HoldHTML = ReplacePrevPageCode(HoldHTML);
                HoldHTML = ReplaceCurrentPageCode(HoldHTML);
                HoldHTML = ReplaceTotalPageCode(HoldHTML);
                HoldHTML = ReplacePagingIndex(HoldHTML);
            }
            else if (HoldHTML != "")
            {
                HoldHTML = RemoveAllPagingCodes(HoldHTML);
            }

            return HoldHTML;
        }

        private string ReplaceIfElseBlocks(string InHTML, ref ContentPiece InContentPiece)
        {
            var HoldHTML = InHTML;
            // Return InHTML
            var HoldIfBlock = "";
            var HoldLeft = "";
            var HoldRight = "";
            var HoldIfResult = "";
            List<IfBlock> HoldIfBlockList;
            var index = 0;
            var Count = 0;
            var ifTag = "{if:";
            var eifTag = "{/if}";
            var nestedIfBlocks = new List<NestedIfBlock>();
            while (HoldHTML.IndexOf(ifTag, index) >= 0)
            {
                index = HoldHTML.IndexOf(ifTag, index);
                nestedIfBlocks.Add(new NestedIfBlock(index, HoldHTML.IndexOf(eifTag, index)));
                index = index + ifTag.Length;
            }

            if (nestedIfBlocks.Count > 0)
            {
                // there are something there
                while (nestedIfBlocks.Count > 0)
                {
                    var _ifBlock = nestedIfBlocks[nestedIfBlocks.Count - 1];
                    HoldLeft = HoldHTML.Substring(0, _ifBlock.Index);
                    HoldHTML = HoldHTML.Substring(_ifBlock.Index);
                    HoldRight = HoldHTML.Substring(HoldHTML.IndexOf(eifTag) + 5);
                    HoldIfBlock = HoldHTML.Substring(0, HoldHTML.IndexOf(eifTag) + 5);
                    HoldIfBlockList = IfBlock.LoadIfBlockList(HoldIfBlock, ref this.fRelatedContent);
                    HoldIfResult = IfBlock.ResolveIfBlockList(ref HoldIfBlockList, ref InContentPiece);
                    HoldHTML = HoldLeft + HoldIfResult + HoldRight;
                    nestedIfBlocks.Remove(_ifBlock);
                }
            }

            return HoldHTML;
        }

        private class NestedIfBlock
        {
            public int Index;

            public int EndIndex;

            public NestedIfBlock(int index, int endIndex)
            {
                this.Index = index;
                this.EndIndex = endIndex;
            }
        }

        private string GetConsumerFieldValue(string InFieldName, string InSupplierID, ref ContentPiece InContentPiece)
        {
            var MyCon = new SqlConnection(this.fRelatedContent.Item2.Result.fConString);
            MyCon.Open();
            var Command = new SqlCommand(
                @"SELECT [content].content_Id, content_fields.content_fields_Id, content_fields.content_types_fields_Id, content_types_fields.fieldtype, content_types_fields.fieldname, custom_content_fields.value_integer, custom_content_fields.value_text, custom_content_fields.value_date, custom_content_fields.value_double, custom_content_fields.value_boolean, custom_content_fields.consumer_Id FROM [content] with (nolock) INNER JOIN                           content_fields with (nolock) ON [content].content_Id = content_fields.content_Id INNER JOIN content_types_fields ON content_fields.content_types_fields_Id = content_types_fields.content_types_fields_Id INNER JOIN custom_content_fields with (nolock) ON content_fields.content_fields_Id = custom_content_fields.content_fields_Id WHERE ([content].content_Id = @ContentID) AND (content_types_fields.fieldname = @FieldName) and (custom_content_fields.consumer_Id = @SupplierID)");
            Command.Connection = MyCon;
            // Me.fRelatedContent.Item2.Result.fSQLConnection
            Command.Parameters.Add(new SqlParameter("@ContentID", InContentPiece.ContentInstanceID));
            // Command.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContentID", New Guid("51761433-6acc-4ada-b8b3-2ff9b1055719")))
            Command.Parameters.Add(new SqlParameter("@FieldName", InFieldName));
            Command.Parameters.Add(new SqlParameter("@SupplierID", InSupplierID));
            var reader = Command.ExecuteReader();
            var FieldName = "";
            var FieldType = "";
            var ValueInt = "";
            var ValueText = "";
            var ValueDate = "";
            var ValueDouble = "";
            var ValueBoolean = "";
            var FoundIt = false;
            var UltimateValue = "";
            while (reader.Read())
            {
                FoundIt = true;
                FieldType = reader["fieldtype"].ToString();
                ValueInt = reader["value_integer"].ToString();
                ValueText = reader["value_text"].ToString();
                ValueDate = reader["value_date"].ToString();
                ValueDouble = reader["value_double"].ToString();
                ValueBoolean = reader["value_boolean"].ToString();
            }

            if (!FoundIt)
            {
                UltimateValue = ContentPieceTypeCreator.GetPropertyAsSafeString(InContentPiece, InFieldName);
            }
            else if (FieldType == "1"
                     || FieldType == "7")
            {
                UltimateValue = ValueText;
            }
            else if (FieldType == "2")
            {
                UltimateValue = ValueInt;
            }
            else if (FieldType == "3")
            {
                UltimateValue = ValueDate;
            }
            else if (FieldType == "4")
            {
                UltimateValue = ValueDouble;
            }
            else if (FieldType == "5")
            {
                UltimateValue = ValueBoolean;
            }

            MyCon.Close();
            return UltimateValue;
        }

        private string ReplaceCustomConsumerFields(string InHTML, ref ContentPiece InContentPiece)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var CurContentField = "";
            var HoldContentFieldName = "";
            var Value = "";
            while (HoldHTML.Contains("{cxF"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{cxF"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{cxF"));
                CurContentField = HoldHTML.Substring(0, HoldHTML.IndexOf("}") + 1);
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                HoldContentFieldName = CurContentField.Substring(4);
                //  Trim off {cxF
                HoldContentFieldName = HoldContentFieldName.Substring(0, HoldContentFieldName.IndexOf("}"));
                Value = this.GetConsumerFieldValue(HoldContentFieldName,
                    this.fRelatedContent.Item2.Result.fBaseDomainData.ConsumerID, ref InContentPiece);
                HoldHTML = HoldLeft + Value + HoldRight;
            }

            return HoldHTML;
        }

        private string DoInnerxDMLReplacement(string InHTML, ref ContentPiece InContentPiece, bool ReplaceEscapeChars)
        {
            var HoldHTML = InHTML;
            if (HoldHTML == "")
            {
                return HoldHTML;
            }

            if (!(InContentPiece == null))
            {
                try
                {
                    //HoldHTML = Functions.ForLoop(HoldHTML, InContentPiece);
                    //HoldHTML = Functions.FieldExists(HoldHTML, InContentPiece);
                    if (string.IsNullOrEmpty(HoldHTML))
                    {
                        return HoldHTML;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + "Error in Process For Loop", ex.InnerException);
                }
            }

            if (!(InContentPiece == null))
            {
                HoldHTML = this.ReplaceCustomConsumerFields(HoldHTML, ref InContentPiece);
            }

            try
            {
                HoldHTML = this.ReplaceConsumerFields(HoldHTML);
            }
            catch (Exception ex)
            {
                var s = "Error In Replace Consumer Fields: " + ex.Message + " Html causing error: " + HoldHTML;
                throw new Exception(s);
            }

            if (!(InContentPiece == null))
            {
                try
                {
                    HoldHTML = this.ReplaceContentTypeFields(HoldHTML, ref InContentPiece);
                }
                catch (Exception ex)
                {
                    var s = "Error In Replace Content Type Fields: " + ex.Message + " Html causing error: " + HoldHTML;
                    throw new Exception(s);
                }

                try
                {
                    HoldHTML = this.ReplaceSupplierFields(HoldHTML, ref InContentPiece);
                }
                catch (Exception ex)
                {
                    var s = "Error In Replace Supplier Fields: " + ex.Message + " Html causing error: " + HoldHTML;
                    throw new Exception(s);
                }
            }

            if (!(InContentPiece == null))
            {
                try
                {
                    HoldHTML = this.ReplaceFFieldsSB(HoldHTML, ref InContentPiece);
                }
                catch (Exception ex)
                {
                    var s = "Error In Replace FFields: " + ex.Message + " Html causing error: " + HoldHTML;
                    throw new Exception(s);
                }
            }

            try
            {
                HoldHTML = this.ReplaceValueCounts(HoldHTML);
            }
            catch (Exception ex)
            {
                var s = "Error In Replace ValueCounts: " + ex.Message + " Html causing error: " + HoldHTML;
                throw new Exception(s);
            }

            try
            {
                HoldHTML = this.ReplacePoundFieldCounts(HoldHTML);
            }
            catch (Exception ex)
            {
                var s = "Error In Replace #Field Counts: " + ex.Message + " Html causing error: " + HoldHTML;
                throw new Exception(s);
            }

            try
            {
                HoldHTML = this.ReplaceRawURLPageCode(HoldHTML);
            }
            catch (Exception ex)
            {
                var s = "Error In Replace Raw URL Page Code: " + ex.Message + " Html causing error: " + HoldHTML;
                throw new Exception(s);
            }

            if (ReplaceEscapeChars)
            {
                HoldHTML = Functions.EscapeChars(HoldHTML);
            }

            if (!(InContentPiece == null))
            {
                try
                {
                    HoldHTML = this.ReplaceIfElseBlocks(HoldHTML, ref InContentPiece);
                    if (HoldHTML == "")
                    {
                        return HoldHTML;
                    }
                }
                catch (Exception ex)
                {
                    var s = "Error In if else block: " + ex.Message + " Html causing error: " + HoldHTML;
                    throw new Exception(s);
                }
            }

            try
            {
                HoldHTML = this.ReplacePagingCodes(HoldHTML);
            }
            catch (Exception ex)
            {
                var s = "Error In Replace Paging Codes: " + ex.Message + " Html causing error: " + HoldHTML;
                throw new Exception(s);
            }

            // ltu
            // modified on 04/09 for wecare4
            try
            {
                HoldHTML = ReplaceContentTypeJoin.replaceContentTypeJoin(HoldHTML, InContentPiece.CTLanguageID,
                    fCurPageContent.Item2.Result.fSQLConnection, this.SourceURL,
                    this.fCurPageContent.Item2.Result.fDomain);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            // end of modification
            //  modify on 09/04/2015 for VR
            // Try
            //     HoldHTML = Me.ReplaceVRConsumerFields(HoldHTML)
            // Catch ex As Exception
            //     Dim s As String = "Error In Replace VR Consumer Fields: " + ex.Message + " Html causing error: " + HoldHTML
            //     Throw New Exception(s)
            // End Try
            HoldHTML = this.ReplaceTimer(HoldHTML, ref this.timer);
            return HoldHTML;
        }

        private string ReplaceTimer(string html, ref long l)
        {
            var timer = new TimeSpan(DateTime.Now.Ticks - l);
            html = html.Replace("{timer}",
                string.Format("{0} hr(s), {1} min(s), {2} sec(s) - {3}", timer.Hours, timer.Minutes, timer.Seconds,
                    timer.Milliseconds));
            return html;
        }

        private string ReplaceComonentTextWithRelatedPubHTML(string InHTML)
        {
            var HoldHTML = InHTML;
            var ComponentID = HoldHTML.Substring(InHTML.IndexOf(":") + 1);
            ComponentID = ComponentID.Substring(0, ComponentID.IndexOf(":"));
            ComponentID = "c" + ComponentID;
            //  to get it to line up with URL string :c31(blablabla)
            foreach (var Item in fMyRelatedPubHTML)
            {
                if (Item.PubID == ComponentID)
                {
                    return Item.HTML;
                }
            }

            // Return InHTML
            return "--- Component " + ComponentID + " Not Found!";
        }

        private string ReplaceComonentTextWithRelatedPubHTMLwTask(string InHTML)
        {
            var HoldHTML = InHTML;
            var ComponentID = HoldHTML.Substring(InHTML.IndexOf(":") + 1);
            ComponentID = ComponentID.Substring(0, ComponentID.IndexOf(":"));
            ComponentID = "c" + ComponentID;
            //  to get it to line up with URL string :c31(blablabla)
            foreach (var Item in fMyRelatedPubHTML)
            {
                if (Item.PubID == ComponentID)
                {
                    Item.MyTask.Wait();
                    return Item.HTML;
                }
            }

            // Return InHTML
            return "--- Component " + ComponentID + " Not Found!";
        }

        private string ReplaceComponentsWithSiblingsHTML(string InHTML)
        {
            // expects fMyRelatedPubHTML to be filled with already replaced replacement codes. {component:32:header} 
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var CurComponent = "";
            while (HoldHTML.Contains("{component"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{component"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{component"));
                CurComponent = HoldHTML.Substring(0, HoldHTML.IndexOf("}") + 1);
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                CurComponent = ReplaceComonentTextWithRelatedPubHTML(CurComponent);
                HoldHTML = HoldLeft + CurComponent + HoldRight;
            }

            return HoldHTML;
        }

        private string ReplaceComponentsWithSiblingsHTMLSB(string InHTML)
        {
            // expects fMyRelatedPubHTML to be filled with already replaced replacement codes. {component:32:header} 
            if (!InHTML.Contains("{component"))
            {
                return InHTML;
            }

            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var CurComponent = "";
            var sb = new StringBuilder();
            while (HoldHTML.Contains("{component"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{component"));
                sb.Append(HoldLeft);
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{component"));
                HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1);
                CurComponent = HoldHTML.Substring(0, HoldHTML.IndexOf("}") + 1);
                // HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1)
                CurComponent =
                    ReplaceComponentsWithSiblingsHTMLSB(ReplaceComonentTextWithRelatedPubHTMLwTask(CurComponent));
                // CurComponent = ReplaceComonentTextWithRelatedPubHTMLwTask(CurComponent)
                // HoldHTML = HoldLeft + CurComponent + Right.Result
                sb.Append(CurComponent);
                HoldHTML = HoldRight;
            }

            sb.Append(HoldRight);
            return sb.ToString();
            return HoldHTML;
        }

        public static string ReplaceAllHTTPStrings(string InString)
        {
            InString = InString.Replace("*LB*", "<");
            InString = InString.Replace("*RB*", ">");
            InString = InString.Replace("*Q*", "?");
            InString = InString.Replace("*FS*", "/");
            InString = InString.Replace("*BS*", "\\");
            InString = InString.Replace("*AMP*", "&");
            InString = InString.Replace("*CRLF*", Environment.NewLine);
            return InString;
        }

        private string ReplaceComponentsWithSiblingsHTMLRecursive(string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var CurComponent = "";
            while (HoldHTML.Contains("{component"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{component"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{component"));
                var holdclosure = HoldHTML;
                var Right = Task<string>.Factory.StartNew(() =>
                {
                    return ReplaceComponentsWithSiblingsHTMLRecursive(
                        holdclosure.Substring(holdclosure.IndexOf("}") + 1));
                });
                CurComponent = HoldHTML.Substring(0, HoldHTML.IndexOf("}") + 1);
                // HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("}") + 1)
                CurComponent =
                    ReplaceComponentsWithSiblingsHTMLRecursive(
                        ReplaceComonentTextWithRelatedPubHTMLwTask(CurComponent));
                HoldHTML = HoldLeft + CurComponent + Right.Result;
            }

            return HoldHTML;
            return HoldHTML;
        }


        private string GetSiblingsHTMLResult(string InHTML)
        {
            var HoldHTML = InHTML;
            if (!(fMySiblingContentCollections == null))
            {
                var holdtasks = new List<Task>();
                var holdsim = new SemaphoreSlim(999);
                foreach (var holditem in fMyRelatedPubHTML)
                {
                    var item = holditem;
                    item.MyTask = Task<string>.Factory.StartNew(() =>
                    {
                        if (item.HTML != "")
                        {
                            Tuple<string, Task<ContentCollection>> HoldContentCollection = null;
                            try
                            {
                                holdsim.Wait();
                                HoldContentCollection =
                                    ContentCollection.GetContentCollectionByPubFromCcList(
                                        ref this.fMySiblingContentCollections, item.PubID);
                            }
                            finally
                            {
                                holdsim.Release();
                            }

                            if (HoldContentCollection == null)
                            {
                                HoldContentCollection = this.fRelatedContent;
                                // default content from my parent publication not paged!
                            }

                            DCMPublication HoldPub = null;
                            try
                            {
                                holdsim.Wait();
                                HoldPub = new DCMPublication(item.HTML, ref HoldContentCollection, null, null);
                                HoldPub.SourceURL = this.SourceURL;
                            }
                            finally
                            {
                                holdsim.Release();
                            }

                            // item.HTML = UrlParse.Functions.EscapeQuotes(HoldPub.GetHTMLResult())
                            // item.HTML = HoldPub.GetHTMLResult()
                            item.HTML = Functions.EscapeQuotes(HoldPub.GetHTMLResult());
                        }

                        return "hi";
                    });
                    holdtasks.Add(item.MyTask);
                }

                // HoldHTML = ReplaceComponentsWithSiblingsHTMLRecursive(HoldHTML)
                HoldHTML = ReplaceComponentsWithSiblingsHTMLSB(HoldHTML);
            }
            else
            {
                // doesn't have siblings, do nothing!
            }

            return HoldHTML;
        }

        private List<URLNameValuePair> GetFetchReplaceNameValuePair(string InString)
        {
            var HoldList = new List<URLNameValuePair>();
            if (InString.Length > 0)
            {
                foreach (var HoldNameValuePair in InString.Split(':'))
                {
                    var Pair = HoldNameValuePair.Split(',');
                    // split by ,
                    var HoldName = Pair[0].Replace("\"", "");
                    var HoldValue = Pair[1].Replace("\"", "");
                    // do replacement on these parameters
                    HoldName = ReplaceAllHTTPStrings(HoldName);
                    HoldValue = ReplaceAllHTTPStrings(HoldValue);
                    HoldList.Add(new URLNameValuePair(HoldName, HoldValue));
                }
            }

            return HoldList;
        }


        private string TryToGetURL_FromCacheFirst(string InURL)
        {
            // Dim SQLConn As SqlClient.SqlConnection = Me.fRelatedContent.Item2.Result.fSQLConnection
            var SQLConn = new SqlConnection(this.fRelatedContent.Item2.Result.fConString);
            SQLConn.Open();
            var S = "";
            var EncodingCode = "";
            var DaysToFetchCache = Convert.ToInt32(ConfigurationManager.AppSettings.Get("DaysToStoreFetchCache"));
            try
            {
                //  connect to db
                //  look up url, only get it if it's less than 48 hours old
                var GetHTMLFromCache = new SqlCommand(
                    "SELECT TOP 1 [URL],[HTML],[DateTimeStamp],[URLHash],[Encoding] FROM [FetchCache] with (nolock)  where" +
                    " (DATEDIFF(DAY,DateTimeStamp,GETDATE())) < @DaysToCache  and URLHash = HASHBYTES(\'SHA1\',@InUrl)",
                    SQLConn);
                GetHTMLFromCache.Parameters.AddWithValue("@InURL", InURL);
                GetHTMLFromCache.Parameters.AddWithValue("@DaysToCache", DaysToFetchCache);
                var Reader = GetHTMLFromCache.ExecuteReader();
                try
                {
                    while (Reader.Read())
                    {
                        EncodingCode = Reader["Encoding"].ToString();
                        Encoding e;
                        try
                        {
                            e = Encoding.GetEncoding(EncodingCode);
                        }
                        catch (Exception ex)
                        {
                            e = Encoding.UTF8;
                        }

                        var buffer = (byte[]) Reader["HTML"];
                        S = e.GetString(buffer);
                    }
                }
                finally
                {
                    Reader.Close();
                }

                // Catch ex As Exception
            }
            finally
            {
                SQLConn.Close();
            }

            //  return the content or blank. 
            return S;
        }

        private void TryToGetUrl_CacheIt(string InURL, string InHTML, Encoding e)
        {
            if (InHTML != "")
            {
                // Dim SQLConn As SqlClient.SqlConnection = Me.fRelatedContent.Item2.Result.fSQLConnection
                var SQLConn = new SqlConnection(this.fRelatedContent.Item2.Result.fConString);
                // If Not SQLConn.State = ConnectionState.Open Then
                SQLConn.Open();
                // End If
                // delete prior url from cache if it's already there
                // add new url content to cache
                try
                {
                    var DelOldURL = new SqlCommand("delete from fetchcache where urlhash = HASHBYTES(\'SHA1\',@InHash)",
                        SQLConn);
                    DelOldURL.Parameters.AddWithValue("@InHash", InURL);
                    DelOldURL.ExecuteNonQuery();
                    var AddNewContent = new SqlCommand(
                        "insert into FetchCache (URL,HTML,DateTimeStamp,URLHash, Encoding) values (@InURL, @InHTML, getdate()," +
                        " HASHBYTES(\'SHA1\', @InURL), @Encoding)", SQLConn);
                    AddNewContent.Parameters.AddWithValue("@InURL", InURL);
                    AddNewContent.Parameters.AddWithValue("@Encoding", e.BodyName);
                    var HTMLParam = AddNewContent.Parameters.Add("@InHTML", SqlDbType.VarBinary);
                    HTMLParam.Value = e.GetBytes(InHTML);
                    // AddNewContent.Parameters.Add("@InHTML", InHTML)
                    AddNewContent.ExecuteNonQuery();
                }
                finally
                {
                    SQLConn.Close();
                }
            }
        }

        private string TryToGetURL(string InURL, string EncodingCode)
        {
            HttpWebRequest request = null;
            var s = "";
            Encoding e;
            try
            {
                e = Encoding.GetEncoding(EncodingCode);
            }
            catch (Exception ex)
            {
                e = Encoding.UTF8;
            }

            var CacheResult = TryToGetURL_FromCacheFirst(InURL);
            if (CacheResult == "")
            {
                try
                {
                    // Try and get the content from the cache here
                    request = (HttpWebRequest) WebRequest.Create(InURL);
                    var proxy = ConfigurationManager.AppSettings.Get("local_proxy_ip");
                    if (!string.IsNullOrWhiteSpace(proxy))
                    {
                        if (InURL.ToUpper().Contains("digitalchannel".ToUpper()) ||
                            InURL.ToUpper().Contains("tiekinetix.".ToUpper()))
                        {
                            request.Proxy = new WebProxy(new Uri(proxy), true);
                        }
                    }

                    //  Set some reasonable limits on resources used by this request
                    request.MaximumAutomaticRedirections = 4;
                    request.MaximumResponseHeadersLength = 4;
                    //  Set credentials to use for this request.
                    request.Credentials = CredentialCache.DefaultCredentials;
                    HttpWebResponse response = null;
                    Stream receiveStream = null;
                    StreamReader readStream = null;
                    var timeOut = 1000 * 10;
                    try
                    {
                        request.ReadWriteTimeout = timeOut;
                        request.Timeout = timeOut;
                        response = (HttpWebResponse) request.GetResponse();
                        receiveStream = response.GetResponseStream();
                        //  Pipes the stream to a higher level stream reader with the required encoding format. 
                        readStream = new StreamReader(receiveStream, e);
                        s = readStream.ReadToEnd();
                    }
                    catch (Exception ex)
                    {
                        s = "";
                    }
                    finally
                    {
                        if (!(response == null))
                        {
                            response.Close();
                        }

                        if (!(readStream == null))
                        {
                            readStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    s = "";
                }

                // if s <> "" and wasn't just grabbed from the cache, then cache it
                TryToGetUrl_CacheIt(InURL, s, e);
            }
            else
            {
                s = CacheResult;
            }

            return s;
        }

        private string ReplaceFetchedNameValues(string WebPage, ref List<URLNameValuePair> TokensToReplace)
        {
            foreach (var item in TokensToReplace)
            {
                WebPage = WebPage.Replace(item.Name, item.Value);
            }

            return WebPage;
        }

        private string Do_PostP_Fetch(string InHTML)
        {
            var Hold = InHTML;
            var HoldLeft = "";
            var HoldRight = "";
            var EncodingCode = String.Empty;
            var InvalidUrls = new ArrayList();
            Int32 Index1;
            Int32 Index2;
            while (Hold.Contains("{fetch:replace:"))
            {
                Index1 = Hold.IndexOf("{fetch:replace:");
                // begining of text
                Index2 = Hold.IndexOf("}", Index1) + 1;
                // end of brace
                HoldLeft = Hold.Substring(0, Index1);
                // everything before statment
                HoldRight = Hold.Substring(Index2);
                //  everything after statement
                Hold = Hold.Substring(Index1, Index2 - Index1);
                //  the actual statment including {}
                Hold = Hold.Substring(15);
                //  remove {fetch:replace
                Hold = Hold.Substring(0, Hold.Length - 1);
                //  remove end brace }
                Index1 = Hold.IndexOf("\",\"");
                //  get first "," this occurs in the name value pairs
                string HoldURL;
                if (Index1 == -1)
                {
                    // no more silly "asdf","asdf" needed
                    HoldURL = Hold;
                    Hold = "";
                }
                else
                {
                    Index2 = Hold.Substring(0, Index1).LastIndexOf(":");
                    // last index of colon before name value pairs start
                    HoldURL = Hold.Substring(0, Index2);
                    // only url
                    Hold = Hold.Substring(Index2 + 1);
                    // only name value pairs
                }

                // use first name value pair to determine encoding
                try
                {
                    EncodingCode = Hold.Substring(0, EncodingCode.IndexOf(","));
                    EncodingCode = EncodingCode.Replace("\"", "");
                }
                catch (Exception ex)
                {
                    EncodingCode = "asdf";
                }

                // do some custom replacements
                HoldURL = ReplaceAllHTTPStrings(HoldURL);
                var FindReplaceList = this.GetFetchReplaceNameValuePair(Hold);
                var HoldFetchedResult = String.Empty;
                var IsFound = false;
                foreach (string st in InvalidUrls)
                {
                    if (HoldURL.IndexOf(st) >= 0)
                    {
                        IsFound = true;
                    }
                }

                if (IsFound == false)
                {
                    HoldFetchedResult = TryToGetURL(HoldURL, EncodingCode);
                    if (string.IsNullOrEmpty(HoldFetchedResult))
                    {
                        var uri = new Uri(HoldURL);
                        InvalidUrls.Add(uri.Host);
                    }
                }

                // set replace and continue to next fetch statmenet
                if (Index1 == -1)
                {
                    Hold = HoldLeft + HoldFetchedResult + HoldRight;
                    // no name value pairs
                }
                else
                {
                    Hold = HoldLeft + this.ReplaceFetchedNameValues(HoldFetchedResult, ref FindReplaceList) + HoldRight;
                }
            }

            return Hold;
        }

        private string Do_PostProcessing(string InHTML)
        {
            // codes to execute at the very end of xDML processing; should have a different syntax to avoid confusion
            // any calls to web protocols (http, smtp) should happen here 
            var HoldHtml = InHTML;
            HoldHtml = Do_PostP_Fetch(HoldHtml);
            return HoldHtml;
        }

        private void UpdatedRelatedCollectionURL()
        {
            if (!(fMySiblingContentCollections == null))
            {
                foreach (var item in this.fMySiblingContentCollections)
                {
                    // item.Item2.ContinueWith(Sub(t)
                    var holdsource = this.SourceURL;
                    item.Item2.Result.fEntireURL = holdsource;
                    var holdOSource = this.OriginalSourceDomain;
                    item.Item2.Result.fOriginalSourceDomain = holdOSource;
                    //                         End Sub)
                    // fEntireURL = Me.SourceURL
                    // item.Item2.ContinueWith(Sub(t) t.Result.fEntireURL = Me.SourceURL)
                }
            }

            if (!(this.fRelatedContent == null))
            {
                // Me.fRelatedContent.Item2.ContinueWith(Sub(t)
                var holdsrcurl = this.SourceURL;
                this.fRelatedContent.Item2.Result.fEntireURL = holdsrcurl;
                //                                       End Sub)
            }
        }

        private string RemoveFoundNotFound(string StartString, string EndString, string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeftSide = "";
            var HoldRightSide = "";
            var LenofStart = StartString.Length;
            var LenofFinish = EndString.Length;
            while (HoldHTML.Contains(StartString))
            {
                HoldLeftSide = HoldHTML.Substring(0, HoldHTML.IndexOf(StartString));
                HoldRightSide = HoldHTML.Substring(HoldHTML.IndexOf(StartString) + LenofStart);
                HoldRightSide = HoldRightSide.Substring(HoldRightSide.IndexOf(EndString) + LenofFinish);
                HoldHTML = HoldLeftSide + HoldRightSide;
            }

            return HoldHTML;
        }

        private string RemoveTag(string TagToRemove, string InHTML)
        {
            var HoldHTML = InHTML;
            while (HoldHTML.Contains(TagToRemove))
            {
                HoldHTML = HoldHTML.Replace(TagToRemove, "");
            }

            return HoldHTML;
        }

        public string GetHTMLResult()
        {
            UpdatedRelatedCollectionURL();
            //  passes source URL through for paging hack
            fHTMLText = GetSiblingsHTMLResult(fHTMLText);
            try
            {
                fHTMLText = Functions.DateTimeFunctions(fHTMLText);
                if (this.fRelatedContent.Item2.Result.fMyContent.Count == 0)
                {
                    // remove {$ContentFound} {/$ContentFound}
                    fHTMLText = RemoveFoundNotFound("{$ContentFound}", "{/$ContentFound}", fHTMLText);
                    fHTMLText = RemoveTag("{$ContentNotFound}", fHTMLText);
                    fHTMLText = RemoveTag("{/$ContentNotFound}", fHTMLText);
                    //  because there doesnt exist any content we are going to have to handle "custom" functions seperately
                    // Removed by EGO 08-14-2013 don't think we use this
                    // fHTMLText = ReplaceContentTypeJoin.replaceContentTypeJoin(fHTMLText, Me.LngID, Me.fCurPageContent.Item2.Result.fSQLConnection, Me.SourceURL, Me.fCurPageContent.Item2.Result.fDomain)
                }
                else
                {
                    // remove {$ContentNotFound} {/$ContentNotFound}
                    fHTMLText = RemoveFoundNotFound("{$ContentNotFound}", "{/$ContentNotFound}", fHTMLText);
                    fHTMLText = RemoveTag("{$ContentFound}", fHTMLText);
                    fHTMLText = RemoveTag("{/$ContentFound}", fHTMLText);
                }
            }
            catch (Exception ex)
            {
                var s = "Error In Replace Content Found / Not Found: " + this.SourceURL + ex.Message + ex.StackTrace +
                        ex.InnerException.Message + ex.InnerException.StackTrace + " Html causing error: " + fHTMLText;
                throw new Exception(s);
            }

            fHTMLText = this.ReplaceCatTreeRepeat(fHTMLText);
            fHTMLText = this.ReplaceRepeatGroupsRecursive(fHTMLText);
            fHTMLText = Functions.ContentPieceCount(fHTMLText, fRelatedContent.Item2.Result.fMyContent.Count);
            fHTMLText = Functions.BrowserLanguageCode(fHTMLText);
            fHTMLText = Functions.OriginalRequestQuery(fHTMLText, SourceURL);
            // this is used for proxy js engine
            //  added 12/29/2014 to support a orignal domain replacement code 
            try
            {
                var tempUrl = this.OriginalSourceDomain;
                if (string.IsNullOrEmpty(tempUrl))
                {
                    tempUrl = this.SourceURL;
                }

                if (!string.IsNullOrEmpty(tempUrl))
                {
                    var tempUri = new Uri(tempUrl);
                    fHTMLText = fHTMLText.Replace("{OriginalSourceDomain}", tempUri.Scheme + "://" + tempUri.Host);
                }
            }
            catch (Exception ex)
            {
                //  if there is an error, just replace the code with empty string
                fHTMLText = fHTMLText.Replace("{OriginalSourceDomain}", "");
            }

            if (fRelatedContent.Item2.Result.fMyContent.Count > 0)
            {
                var inContentPiece = fRelatedContent.Item2.Result.fMyContent[0];
                fHTMLText = this.DoInnerxDMLReplacement(fHTMLText, ref inContentPiece, true);
                if (fHTMLText.Contains("{F") || fHTMLText.Contains("{consumer"))
                {
                    // replace again the html to ensure the data coming from the sheet can be replaced correctly.
                    fHTMLText = this.DoInnerxDMLReplacement(fHTMLText, ref inContentPiece, true);
                }
            }
            else
            {
                // at least replace the consumer field
                try
                {
                    fHTMLText = this.ReplaceConsumerFields(fHTMLText);
                    fHTMLText = this.ReplaceVRConsumerFields(fHTMLText);
                }
                catch (Exception ex)
                {
                    // nothing, just suppress the error
                }
            }

            try
            {
                fHTMLText = this.ReplaceVRConsumerFields(fHTMLText);
            }
            catch (Exception ex)
            {
                // nothing, just suppress the error
            }

            fHTMLText = Do_PostProcessing(fHTMLText);
            fHTMLText = Functions.EscapeQuotes(fHTMLText);
            fHTMLText = Functions.ConvertStringToURI(fHTMLText, SourceURL);
            return fHTMLText;
        }


        private string ReplaceRepeatGroupsRecursive(string InHTML)
        {
            var HoldHTML = InHTML;
            var HoldLeft = "";
            var HoldRepeatText = "";
            var HoldRepeatBlock = "";
            var HoldRepeatOnChangeText = "";
            var CategoryFilter = "";
            while (HoldHTML.Contains("{repeat}"))
            {
                HoldLeft = HoldHTML.Substring(0, HoldHTML.IndexOf("{repeat}"));
                HoldHTML = HoldHTML.Substring(HoldHTML.IndexOf("{repeat}"));
                // ''''HoldRight = HoldHTML.Substring(HoldHTML.IndexOf("{/repeat}") + 9)
                var Right = Task<string>.Factory.StartNew(() =>
                {
                    return ReplaceRepeatGroupsRecursive(HoldHTML.Substring(HoldHTML.IndexOf("{/repeat}") + 9));
                });
                HoldRepeatText = HoldHTML.Substring(HoldHTML.IndexOf("{repeat}") + 8);
                HoldRepeatText = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{/repeat}"));
                if (HoldRepeatText.Contains("{CatFilter:"))
                {
                    CategoryFilter = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{CatFilter:"));
                    var HoldRLeft = HoldRepeatText.Substring(0, HoldRepeatText.IndexOf("{CatFilter:"));
                    CategoryFilter = CategoryFilter.Substring(0, CategoryFilter.IndexOf("}"));
                    // 
                    HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("{CatFilter:"));
                    HoldRepeatText = HoldRepeatText.Substring(HoldRepeatText.IndexOf("}") + 1);
                    //  should = the string value after the cat filter
                    HoldRepeatText = HoldRLeft + HoldRepeatText;
                    // pulled out cat filter
                    CategoryFilter = CategoryFilter.Substring(CategoryFilter.IndexOf(":") + 1);
                }

                List<OnChangeSupportObj> OnChangeObjList;
                if (HoldRepeatText.Contains("{OnChange:"))
                {
                    OnChangeObjList = GetOnChangeListReplaceWLandmarks(ref HoldRepeatText);
                }
                else
                {
                    OnChangeObjList = null;
                }

                var HoldInnerRepeatText = HoldRepeatText;
                List<InnerRepeatBlock> InnerRepeatList = null;
                while (HoldInnerRepeatText.Contains("{InnerRepeat"))
                {
                    if (InnerRepeatList == null)
                    {
                        InnerRepeatList = new List<InnerRepeatBlock>();
                    }

                    InnerRepeatList.Add(new InnerRepeatBlock(ref HoldInnerRepeatText));
                }

                if (InnerRepeatList == null)
                {
                    var OkToRepeat = false;
                    if (!(OnChangeObjList == null))
                    {
                        // If True Then
                        foreach (var Item in fCurPageContent.Item2.Result.fMyContent)
                        {
                            // For Each Item As ContentPiece In fCurPageContent.fMyContent
                            OkToRepeat = false;
                            if (CategoryFilter == "")
                            {
                                OkToRepeat = true;
                            }
                            else
                            {
                                //  should be filtered by category
                                foreach (var Cat in Item.fCategoriesImAssignedTo)
                                {
                                    if (Cat == CategoryFilter)
                                    {
                                        OkToRepeat = true;
                                        break;
                                    }
                                }
                            }

                            if (OkToRepeat)
                            {
                                if (!(OnChangeObjList == null))
                                {
                                    var curContentPiece = Item;
                                    HoldRepeatOnChangeText = ReplaceOnChangeLandmarksWithHTMLAsNeeded(HoldRepeatText,
                                        ref curContentPiece, ref OnChangeObjList);
                                }
                                else
                                {
                                    HoldRepeatOnChangeText = HoldRepeatText;
                                }

                                var inContentPiece = Item;
                                HoldRepeatBlock =
                                    this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                                HoldLeft = HoldLeft + HoldRepeatBlock;
                            }
                        }
                    }
                    else
                    {
                        IEnumerable<Tuple<int, string>> Query = fCurPageContent.Item2.Result.fMyContent.AsParallel()
                            .Select(
                                (inItem, inindex) =>
                                {
                                    var Item = inItem;
                                    var Index = inindex;
                                    var HoldRText = HoldRepeatText;
                                    // HoldRepeatBlock = Me.DoInnerxDMLReplacement(HoldRText, Item)
                                    // HoldLeft = HoldLeft + HoldRepeatBlock
                                    return new Tuple<int, string>(Index,
                                        this.DoInnerxDMLReplacement(HoldRText, ref Item, false));
                                });
                        var sorted = Query.OrderBy(x => x.Item1);
                        var sb = new StringBuilder();
                        // Query.Count() * 1000)
                        sb.Append(HoldLeft);
                        foreach (var HoldRepeatBlockItem in sorted)
                        {
                            // HoldLeft = HoldLeft + HoldRepeatBlockItem.Item2
                            sb.Append(HoldRepeatBlockItem.Item2);
                        }

                        HoldLeft = Functions.EscapeChars(sb.ToString());
                    }
                }
                else
                {
                    var CurStep = 0;
                    var OkToRepeat = false;
                    foreach (var Item in fCurPageContent.Item2.Result.fMyContent)
                    {
                        // For Each Item As ContentPiece In fCurPageContent.fMyContent
                        OkToRepeat = false;
                        if (CategoryFilter == "")
                        {
                            OkToRepeat = true;
                        }
                        else
                        {
                            //  should be filtered by category
                            foreach (var Cat in Item.fCategoriesImAssignedTo)
                            {
                                if (Cat == CategoryFilter)
                                {
                                    OkToRepeat = true;
                                    break;
                                }
                            }
                        }

                        if (OkToRepeat)
                        {
                            HoldRepeatText = InnerRepeatList[CurStep].WithDataRepeatBlock;
                            if (!(OnChangeObjList == null))
                            {
                                string HoldCurValueOfField;
                                var curContentPiece = Item;
                                HoldRepeatOnChangeText = ReplaceOnChangeLandmarksWithHTMLAsNeeded(HoldRepeatText,
                                    ref curContentPiece, ref OnChangeObjList);
                                foreach (var OItem in OnChangeObjList)
                                {
                                    HoldCurValueOfField =
                                        ContentPieceTypeCreator.GetPropertyAsSafeString(Item, OItem.FieldName);
                                    OItem.ShouldBeRenderedThisTime(HoldCurValueOfField);
                                    //  added to keep on changes up to date inside of inner repeat blocks where an inner repeat block may not contain every onchangelandmark object
                                }
                            }
                            else
                            {
                                HoldRepeatOnChangeText = HoldRepeatText;
                            }

                            var inContentPiece = Item;
                            HoldRepeatBlock =
                                this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                            HoldLeft = HoldLeft + HoldRepeatBlock;
                            CurStep = CurStep + 1;
                            if (CurStep + 1 > InnerRepeatList.Count)
                            {
                                CurStep = 0;
                            }
                        }

                        // ok to repeat
                    }

                    while (!(CurStep + 1 > InnerRepeatList.Count)
                           && !(CurStep == 0))
                    {
                        HoldRepeatOnChangeText = InnerRepeatList[CurStep].WithoutDataRepeatBlock;
                        ContentPiece inContentPiece = null;
                        HoldRepeatBlock = this.DoInnerxDMLReplacement(HoldRepeatOnChangeText, ref inContentPiece, true);
                        HoldLeft = HoldLeft + HoldRepeatBlock;
                        CurStep = CurStep + 1;
                    }
                }

                HoldHTML = HoldLeft + Right.Result;
            }

            // Return InHTML
            return HoldHTML;
        }
    }
}