﻿using System;
using System.Collections.Generic;
using System.Data;
using Csp.Core.Infrastructure.Publication.Repositories;

namespace Csp.Core.Infrastructure.DAO.Models
{
    public class ProxyScriptReference
    {
        private DataTable dataTable;
        private const string key = "SyndicationType";
        public const string PublicationPageUrl = "PublicationPageUrl";
        public const string PublicationPageUrlForHover = "PublicationPageUrlForHover";

        public ProxyScriptReference(SynSqlRepository dao)
        {
            dataTable = dao.GetDataTable(@"
select [SyndicationType]
      ,[DefaultSettings]
      ,[Js1]
      ,[Js2]
      ,[PublicationPageUrl]
      ,[ActionCode]
      ,[PublicationPageUrlForHover]  
from ProxyScriptReferences");
            if (dataTable == null)
            {
                dataTable = dao.GetDataTable(@"
select [SyndicationType]
      ,[DefaultSettings]
      ,[Js1]
      ,[Js2]
      ,[PublicationPageUrl]
      ,[ActionCode]
from ProxyScriptReferences");

            }
        }

        public string Get(string syndicationType, string columnName)
        {
            if (dataTable == null || (dataTable != null&&!dataTable.Columns.Contains(key)) || (dataTable != null&&!dataTable.Columns.Contains(columnName)))
                return string.Empty;

            foreach (DataRow row in dataTable.Rows)
            {
                if (string.Equals(syndicationType, row[key].ToString(), StringComparison.OrdinalIgnoreCase))
                    return row[columnName].ToString();
            }
            return string.Empty;
        }

        public string GetUrl(string syndicationType)
        {
            return Get(syndicationType, PublicationPageUrl);
        }

        public bool IsEmpty(string type)
        {
            if (dataTable == null) return true;

            if (!dataTable.Columns.Contains(key)) 
                return true;

            return string.IsNullOrEmpty(Get(type, key));
        }
    }
    public class ProxyScriptReferences : Dictionary<string, ProxyScriptReference>
    {
        public static ProxyScriptReferences Create()
        {
            return new ProxyScriptReferences();
        }

        public ProxyScriptReference Get(string project, SynSqlRepository dao)
        {
            if (this.ContainsKey(project)) return this[project];

            ProxyScriptReference scriptReference = new ProxyScriptReference(dao);
            if (!this.ContainsKey(project)) 
                this.Add(project, scriptReference);
            return scriptReference;
        }
    }
}
