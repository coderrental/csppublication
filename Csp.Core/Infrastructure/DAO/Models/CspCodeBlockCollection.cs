using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication.Repositories;
using Serilog.Core;

namespace Csp.Core.Infrastructure.DAO.Models
{
    public class CspCodeBlockCollection
    {
        private readonly string _html;
        //private const string CodeBlockRegex = @"(?'block'{csp(?<parameters>(?=\s)+.*)}(?<text>(?s).*){/csp})";
        //private const string CodeBlockRegex = @"(?<block>{csp(?<parameters>(?=\s)+.*)}(?<text>(?s).*?){/csp})";
        private const string CodeBlockRegex = "(?<block>{csp(?<parameters>[\\s,\\w,\\d,\\/,=,\\\",\\.,\\(,\\),\\!,\\&,\\~,\\?,\\:,\\-]*)}(?<text>(?s).*?){/csp})";
        static readonly Regex ParseRegex = new Regex(CodeBlockRegex, RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
        static readonly Regex SplitRegex = new Regex("((?'name'\\w+)=\"(?'value'.*?)\")+", RegexOptions.Compiled | RegexOptions.IgnoreCase);


        public static string ProcessXdml(string html, Logger log, SynSqlRepository synDao, DomainBase domainUtil, string contentId)
        {
            string s = html;
            //int startAt = 0;

            List<CodeBlock>  collection = new List<CodeBlock>();

            string cspOpenStag = "{csp ", cspCloseTag = "{/csp}";

            while (s.IndexOf(cspOpenStag, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                int startPos = s.IndexOf(cspOpenStag, StringComparison.OrdinalIgnoreCase);

                // find the next good position for the closing tag
                int endPos = s.IndexOf(cspCloseTag, startPos + 1, StringComparison.OrdinalIgnoreCase);

                // find any nested inside the block
                int tempPos = s.IndexOf(cspOpenStag, startPos + 1, endPos - startPos - 1, StringComparison.OrdinalIgnoreCase);
                while (tempPos > startPos && tempPos < endPos)
                {
                    int temp = endPos;
                    endPos = s.IndexOf(cspCloseTag, endPos + 1, StringComparison.OrdinalIgnoreCase);
                    tempPos = s.IndexOf(cspOpenStag, temp + 1, endPos - temp - 1, StringComparison.OrdinalIgnoreCase);
                }

                var dict = new Dictionary<string, string>();
                int closeBracket = s.IndexOf("}", startPos + 1, StringComparison.OrdinalIgnoreCase);
                if (closeBracket == -1)
                    break; // bad syntax
                var parameters = s.Substring(startPos + 4, closeBracket - startPos - 4);
                if (!string.IsNullOrEmpty(parameters))
                {
                    foreach (Match m in SplitRegex.Matches(parameters))
                    {
                        if (m.Success && !dict.ContainsKey(m.Groups["name"].Value))
                            dict.Add(m.Groups["name"].Value, m.Groups["value"].Value);
                    }
                }
                CodeBlock codeBlock = new CodeBlock(dict, synDao, domainUtil, contentId);
                s = codeBlock.ApplyXdmlReplacementCodes(s, startPos, endPos, closeBracket);
            }

            return s;
        }
    }
}
