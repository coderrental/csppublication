using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using Csp.Core.Infrastructure.DAO.Dto;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication;
using Csp.Core.Infrastructure.Publication.Repositories;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Infrastructure.UrlParse;

namespace Csp.Core.Infrastructure.DAO.Models
{
    public class CodeBlock
    {
        private readonly Match _match;
        private readonly SynSqlRepository _synDao;
        private readonly DomainBase _domainUtil;
        private Dictionary<string, string> _parameters;
        private static Regex NotExistRegex = new Regex("{notexist}(?<text>(?s).*?){/notexist}", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
        private readonly string _contentId;

        public CodeBlock(Dictionary<string, string> parameters, Match match, SynSqlRepository synDao, DomainBase domainUtil, string contentId)
        {
            _parameters = parameters;
            _match = match;
            _synDao = synDao;
            _domainUtil = domainUtil;
            _contentId = contentId;
        }

        public CodeBlock(Dictionary<string, string> parameters, SynSqlRepository synDao, DomainBase domainUtil, string contentId)
        {
            _parameters = parameters;
            _match = null;
            _synDao = synDao;
            _domainUtil = domainUtil;
            _contentId = contentId;
        }

        public string ApplyXdmlReplacementCodes(string html)
        {
            string s = html;
            string content = string.Empty;

            string queryString = string.Empty;

            if (_parameters.ContainsKey("type"))
                queryString = _synDao.FindXdmlInjectionQueryString(_parameters["type"]);

            #region Rendering Engine - Required code from the engine
            List<URLNameValuePair> searchNameValueList = new List<URLNameValuePair>();
            HttpRequest request = HttpContext.Current.Request;
            LoggingInfo loggingInfo = new LoggingInfo(request.ServerVariables["REMOTE_ADDR"], request.ServerVariables["HTTP_USER_AGENT"],
                                                      request.ServerVariables["HTTP_REFERER"], request.ServerVariables["REMOTE_HOST"], request.ServerVariables["REQUEST_METHOD"], string.Empty, Guid.Empty, request.Path, request.Path, request.Path, "", "");

            CategoryLoaderParams categoryLoaderParams = null;
            Cache cache = HttpContext.Current.Cache;
            List<HTMLwPubName> relatedPubHtml = new List<HTMLwPubName>();

            #endregion

            if (!string.IsNullOrEmpty(queryString))
            {
                // get data from publication
                int pageId = -1;
                if (!int.TryParse(queryString.Substring(1, queryString.IndexOf('(') - 1), out pageId))
                    return s;

                queryString = FormatQueryString(queryString);
                queryString = ReplaceSpecialCharacters(queryString);
                URLEngine Hold = URLEngine.GetOrCreateURLEngineFromCache(queryString, _synDao.ConnectionString, _domainUtil.BaseDomain, ref searchNameValueList, ref loggingInfo, false, false, categoryLoaderParams, ref cache, _domainUtil.Language.code);
                
                content = ExecuteReplacementCodes(Hold, ref relatedPubHtml, queryString, pageId);

                //foreach (ContentCollection contentCollection in Hold.MyContentCollections)
                foreach (Tuple<string,System.Threading.Tasks.Task<ContentCollection>> contentCollection in Hold.MyContentCollectionTasks)
                {
                    if (contentCollection.Item1 == "p" + pageId)
                    {
                        Tuple<string, System.Threading.Tasks.Task<ContentCollection>> item = contentCollection;
                        DCMPublication dcmPublication = new DCMPublication(content, ref item, Hold.MyContentCollectionTasks, relatedPubHtml);
                        content = ReplaceSpecialCharacters(dcmPublication.GetHTMLResult());
                    }
                }
            } // has query string
            else if (_parameters.ContainsKey("query"))
            {
                queryString = ReplaceSpecialCharacters(_parameters["query"]);
                //string innerXdml = _match.Groups["text"].Value;
                content = _match.Groups["text"].Value;
                URLEngine Hold = URLEngine.GetOrCreateURLEngineFromCache(queryString, _synDao.ConnectionString, _domainUtil.BaseDomain, ref searchNameValueList, ref loggingInfo, false, false, categoryLoaderParams, ref cache, _domainUtil.Language.code);
                foreach (Tuple<string, System.Threading.Tasks.Task<ContentCollection>> contentCollection in Hold.MyContentCollectionTasks)
                {
                    Tuple<string, System.Threading.Tasks.Task<ContentCollection>> item = contentCollection;
                    DCMPublication dcmPublication = new DCMPublication(content, ref item, Hold.MyContentCollectionTasks, relatedPubHtml);
                    content = ReplaceSpecialCharacters(dcmPublication.GetHTMLResult());
                }
            }
            else if (_parameters.ContainsKey("parameter"))
            {
                content = _match.Groups["text"].Value;
                Match notExistMatch = NotExistRegex.Match(content);
                if (notExistMatch.Success)
                    content = content.Remove(notExistMatch.Index, notExistMatch.Length);

                    /* @Name: Buy Now button
                     * @Description: Get the Buy Now button URL and replace it in the publication
                     * @Parameters:  
                     * contentId = the content_Id
                     * customContentField = contains the URL to use in the Buy Now button
                     * @Created on: 06/12/2014
                     * @Author: Victor Roman
                     **/
                string customContentField = getCustomContentField(_domainUtil.Project.ToLower(), _contentId, _domainUtil.ConsumerOptions.GetStringValue("companies_Id"), _parameters["parameter"]);
                if (!string.IsNullOrEmpty(customContentField)) 
                {
                    content = content.Replace("{value}", customContentField).Replace("{Value}", customContentField);
                } 
                else if (_domainUtil.ConsumerOptions.ContainsKey(_parameters["parameter"]))
                {

                    string parameterValue = _domainUtil.ConsumerOptions.GetStringValue(_parameters["parameter"]);
                    content = content.Replace("{value}", parameterValue).Replace("{Value}", parameterValue);
                }
                else if (notExistMatch.Success && notExistMatch.Groups["text"] != null)
                    content = notExistMatch.Groups["text"].Value;
            }

            if (!string.IsNullOrEmpty(content))
            {
                // custom replacement using the parameters
                foreach (string key in _parameters.Keys)
                {
                    content = content.Replace("{f" + key + "}", _parameters[key]);
                }
            }

            s = s.Remove(_match.Index, _match.Length);
            s = s.Insert(_match.Index, content);
            return s;
        }

        public string ExecuteReplacementCodes(URLEngine engine,ref List<HTMLwPubName> RelatedPubHTML, string queryString, int pageId)
        {
            string html = string.Empty;

            SqlDataReader r = null;
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            //List<HTMLwPubName> RelatedPubHTML;
            //RelatedPubHTML = new List<HTMLwPubName>();
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(
                    @"
select a.publications_pages_Id, a.publications_Id,  a.type, a.content 
from publications_pages a
	join pages_components b on a.publications_pages_Id = b.components_Id	
where b.pages_Id = @pageId
union
select a.publications_pages_Id, a.publications_Id, a.type, a.content 
from publications_pages a	
where a.publications_pages_Id = @pageId
", conn, transaction);

                stmt.Parameters.AddWithValue("@pageId", pageId);
                r = stmt.ExecuteReader();
                int publicationId = -1;
                if (r != null)
                {
                    while (r.Read())
                    {
                        if (r["type"] != null)
                        {
                            html = CodeBlock.ReplaceSpecialCharacters(r["content"].ToString());
                            switch (Convert.ToInt32(r["type"]))
                            {
                                case 0:
                                    html = CodeBlock.ReplaceSpecialCharacters(r["content"].ToString());
                                    publicationId = Convert.ToInt32(r["type"]);
                                    break;
                                case 1:
                                    if (pageId.Equals(r["publications_pages_Id"]))
                                    {
                                        html = CodeBlock.ReplaceSpecialCharacters(r["content"].ToString());
                                        publicationId = Convert.ToInt32(r["type"]);
                                        RelatedPubHTML.Add(new HTMLwPubName(r["content"].ToString(), "c" + r["publications_pages_Id"]));
                                    }
                                    else
                                    {
                                        RelatedPubHTML.Add(new HTMLwPubName(r["content"].ToString(), "c" + r["publications_pages_Id"]));
                                    }
                                    break;
                            }
                        }
                    }
                    r.Close();
                }
            }
            catch
            {
                if (r != null) r.Close();
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return html;
        }

        private SqlConnection getReadConnection(out SqlTransaction transaction)
        {
            SqlConnection conn = new SqlConnection(_synDao.ConnectionString);
            conn.Open();
            transaction = conn.BeginTransaction(IsolationLevel.ReadUncommitted);
            return conn;
        }
        private void closeConnection(SqlConnection conn, SqlTransaction transaction)
        {
            try
            {
                transaction.Commit();
            }
            catch (Exception) { }

            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public static string ReplaceSpecialCharacters(string queryString)
        {
            string s = queryString;

            s = s.Replace("%", "~");
            s = s.Replace("#", "!");
            s = s.Replace("~", "%");
            s = s.Replace("!", "#");
            s = s.Replace("*LB*", "<");
            s = s.Replace("*RB*", ">");
            s = s.Replace("*LB*", "<");
            s = s.Replace("*RB*", ">");
            s = s.Replace("*Q*", "?");
            s = s.Replace("*Q*", "?");
            s = s.Replace("*FS*", "/");
            s = s.Replace("*FS*", "/");
            s = s.Replace("*BS*", "\\");
            s = s.Replace("*AMP*", "&");
            s = s.Replace("*AMP*", "&");
            s = s.Replace("%5b", "[");
            s = s.Replace("%5d", "]");
            s = s.Replace("%28", "(");
            s = s.Replace("%29", ")");
            s = s.Replace("*C*", ":");

            return s;
        }

        private string FormatQueryString(string queryString)
        {
            string st = queryString;
            int pos = st.IndexOf('{', 0);
            int i = 0;
            string s = string.Empty, name = string.Empty;
            while (pos >= 0)
            {
                i = st.IndexOf('}', pos);
                if (i == -1)
                    break;
                s = st.Substring(pos + 1, i - pos - 1);
                if (s.IndexOf('.') == -1)
                {
                    st = st.Remove(pos, i - pos);
                    continue;
                }
                name = s.Substring(0, s.IndexOf('.'));

                if (_parameters.ContainsKey(name))
                {
                    st = st.Replace("{" + name + ".name}", name);
                    st = st.Replace("{" + name + ".value}", _parameters[name]);
                }
                else
                {
                    st = st.Replace("{" + name + ".name}", "");
                    st = st.Replace("{" + name + ".value}", "");
                }

                pos = st.IndexOf('{', pos);
            }
            return st;
        }

        private string getCustomContentField(string project, string contentId, string consumerId, string parameterValue) 
        {
            //String connStr = PublicationEngine.Connections[project];
            //SynDAO synDao = new SynDAO(CspDecoder.Decode(connStr));
            return _synDao.GetCustomContenFieldValue(contentId, consumerId, parameterValue);
        }

        public string ApplyXdmlReplacementCodes(string html, int startPos, int endPos, int closeBracket)
        {
            string closeTag = "{/csp}";
            string s = html;
            string content = string.Empty;

            string queryString = string.Empty;

            if (_parameters.ContainsKey("type"))
                queryString = _synDao.FindXdmlInjectionQueryString(_parameters["type"]);

            #region Rendering Engine - Required code from the engine
            List<URLNameValuePair> searchNameValueList = new List<URLNameValuePair>();
            HttpRequest request = HttpContext.Current.Request;
            LoggingInfo loggingInfo = new LoggingInfo(request.ServerVariables["REMOTE_ADDR"], request.ServerVariables["HTTP_USER_AGENT"],
                                                      request.ServerVariables["HTTP_REFERER"], request.ServerVariables["REMOTE_HOST"], request.ServerVariables["REQUEST_METHOD"], string.Empty, Guid.Empty, request.Path, request.Path, request.Path, "", "");

            CategoryLoaderParams categoryLoaderParams = null;
            Cache cache = HttpContext.Current.Cache;
            List<HTMLwPubName> relatedPubHtml = new List<HTMLwPubName>();

            #endregion

            if (!string.IsNullOrEmpty(queryString))
            {
                // get data from publication
                int pageId = -1;
                if (!int.TryParse(queryString.Substring(1, queryString.IndexOf('(') - 1), out pageId))
                    return s;

                queryString = FormatQueryString(queryString);
                queryString = ReplaceSpecialCharacters(queryString);
                URLEngine Hold = URLEngine.GetOrCreateURLEngineFromCache(queryString, _synDao.ConnectionString, _domainUtil.BaseDomain, ref searchNameValueList, ref loggingInfo, false, false, categoryLoaderParams, ref cache, _domainUtil.Language.code);

                content = ExecuteReplacementCodes(Hold, ref relatedPubHtml, queryString, pageId);

                // ltu 01/14/2016 re-replace the xdml query
                content = content.Replace("%1=", "~1=").Replace("%1<", "~1<").Replace("#)", "!)");

                //foreach (ContentCollection contentCollection in Hold.MyContentCollections)
                foreach (Tuple<string, System.Threading.Tasks.Task<ContentCollection>> contentCollection in Hold.MyContentCollectionTasks)
                {
                    if (contentCollection.Item1 == "p" + pageId)
                    {
                        Tuple<string, System.Threading.Tasks.Task<ContentCollection>> item = contentCollection;
                        DCMPublication dcmPublication = new DCMPublication(content, ref item, Hold.MyContentCollectionTasks, relatedPubHtml);
                        //content = ReplaceSpecialCharacters(dcmPublication.GetHTMLResult());
                    }
                }
            } // has query string
            else if (_parameters.ContainsKey("query"))
            {
                queryString = ReplaceSpecialCharacters(_parameters["query"]);
                //string innerXdml = _match.Groups["text"].Value;
                content = _match.Groups["text"].Value;
                URLEngine Hold = URLEngine.GetOrCreateURLEngineFromCache(queryString, _synDao.ConnectionString, _domainUtil.BaseDomain, ref searchNameValueList, ref loggingInfo, false, false, categoryLoaderParams, ref cache, _domainUtil.Language.code);
                foreach (Tuple<string, System.Threading.Tasks.Task<ContentCollection>> contentCollection in Hold.MyContentCollectionTasks)
                {
                    Tuple<string, System.Threading.Tasks.Task<ContentCollection>> item = contentCollection;
                    DCMPublication dcmPublication = new DCMPublication(content, ref item, Hold.MyContentCollectionTasks, relatedPubHtml);
                    content = ReplaceSpecialCharacters(dcmPublication.GetHTMLResult());
                }
            }
            else if (_parameters.ContainsKey("parameter"))
            {
                //content = _match.Groups["text"].Value;
                //Match notExistMatch = NotExistRegex.Match(content);
                //if (notExistMatch.Success)
                //    content = content.Remove(notExistMatch.Index, notExistMatch.Length);

                //string customContentField = getCustomContentField(_domainUtil.project.ToLower(), _contentId, (string)_domainUtil.consumerOptions.tryGet("companies_Id"), _parameters["parameter"]);
                //if (!string.IsNullOrEmpty(customContentField))
                //{
                //    content = content.Replace("{value}", customContentField).Replace("{Value}", customContentField);
                //}
                //else if (_domainUtil.consumerOptions.ContainsKey(_parameters["parameter"]))
                //{

                //    string parameterValue = (string)_domainUtil.consumerOptions.tryGet(_parameters["parameter"]);
                //    content = content.Replace("{value}", parameterValue).Replace("{Value}", parameterValue);
                //}
                //else if (notExistMatch.Success && notExistMatch.Groups["text"] != null)
                //    content = notExistMatch.Groups["text"].Value;


                content = s.Substring(closeBracket + 1, endPos - closeBracket - 1);

                int notExistStartPos = -1, notExistEndPos = -1;
                string notExistContent = "";
                notExistStartPos = content.IndexOf("{notexist}", StringComparison.OrdinalIgnoreCase);
                if (notExistStartPos >= 0)
                {
                    notExistEndPos = content.LastIndexOf("{/notexist}", content.Length, notExistStartPos + 1, StringComparison.OrdinalIgnoreCase);
                    if (notExistEndPos >= 0)
                    {
                        notExistContent = content.Substring(notExistStartPos + "{notexist}".Length, notExistEndPos - notExistStartPos - "{notexist}".Length);
                        content = content.Substring(0, notExistStartPos);
                    }
                }

                string customContentField = getCustomContentField(_domainUtil.Project.ToLower(), _contentId, _domainUtil.ConsumerOptions.GetStringValue("companies_Id"), _parameters["parameter"]);
                if (!string.IsNullOrEmpty(customContentField))
                {
                    content = content.Replace("{value}", customContentField).Replace("{Value}", customContentField);
                }
                else if (_domainUtil.ConsumerOptions.ContainsKey(_parameters["parameter"]))
                {
                    string parameterValue = _domainUtil.ConsumerOptions.GetStringValue(_parameters["parameter"]);
                    content = content.Replace("{value}", parameterValue).Replace("{Value}", parameterValue);
                }
                else if (notExistStartPos >= 0 && notExistEndPos >= 0)
                {
                    content = notExistContent;
                }
                else
                {
                    content = content.Replace("{value}", "").Replace("{Value}", "");
                }
            }

            if (!string.IsNullOrEmpty(content))
            {
                // custom replacement using the parameters
                foreach (string key in _parameters.Keys)
                {
                    content = content.Replace("{f" + key + "}", _parameters[key]);
                }
            }

            s = s.Remove(startPos, endPos - startPos + closeTag.Length);
            s = s.Insert(startPos, content);
            return s;            
        }
    }
}
