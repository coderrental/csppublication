using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.DAO.Models
{
    public class ProjectContentType
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Project { get; set; }
        public int ContentTypeId { get; set; }
        public int ContentTypeFieldId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ProjectContentType(int id, string identifier, string project, int contentTypeId, int contentTypeFieldId)
        {
            Id = id;
            Identifier = identifier;
            Project = project;
            ContentTypeId = contentTypeId;
            ContentTypeFieldId = contentTypeFieldId;
        }
    }

    public class ProjectContentTypeMap : List<ProjectContentType>
    {
        public ProjectContentType Find(string identifier, string project)
        {
            return Find(a => a.Identifier.Equals(identifier, StringComparison.OrdinalIgnoreCase) && a.Project.Equals(project, StringComparison.OrdinalIgnoreCase));
        }
    }
}