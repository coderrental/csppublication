using System;
using System.Collections.Generic;
using Csp.Core.Infrastructure.Domain;

namespace Csp.Core.Infrastructure.DAO.Models
{
    public class CspContentCollection : List<CspContent>
    {
        #region Overrides of Object

        public override string ToString()
        {
            if (Count == 0) return "{\"length\":0}";

            string jsonStr = "{";
            for (int i = 0; i < Count; i++)
            {
                jsonStr += "\"" + i + "\"" + ":" + this[i] + ",";
            }
            jsonStr += "\"length\":" + Count + "}";

            return jsonStr;
        }

        #endregion
    }

    public class CspContent : MapBase<string>
    {
        public CspContent(IDictionary<string, string> map)
        {
            foreach (string key in map.Keys)
                Add(key, map[key]);
        }

        public void Add(IDictionary<string, string> map)
        {
            foreach (string key in map.Keys)
                Add(key, map[key]);
        }

        #region Overrides of Object

        public override string ToString()
        {
            string jsonStr = "{";
            foreach (string key in Keys)
            {
                if (!key.Substring(0, 3).Equals("xls", StringComparison.OrdinalIgnoreCase) && !this[key].Equals("Null") && !(this[key].Length > 100))
                    jsonStr += "\"" + key + "\"" + ":\"" + this[key].Replace("\n", "").Replace("\r", "").Replace("\"", "").Replace("'", "") + "\",";
            }

            if (jsonStr.Length > 1)
                return jsonStr.Substring(0, jsonStr.Length - 1) + "}";
            return "{}";
        }

        #endregion
    }
}
