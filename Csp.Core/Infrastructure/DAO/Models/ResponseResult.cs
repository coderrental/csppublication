﻿namespace Csp.Core.Infrastructure.DAO.Models
{
    public class ResponseResult
    {
        public string Type { get; set; }
        public string Content { get; set; }
    }
}
