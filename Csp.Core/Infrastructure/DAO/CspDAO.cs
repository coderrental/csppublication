using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Csp.Core.Infrastructure.DAO.Models;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication.Models;
using Serilog;

namespace Csp.Core.Infrastructure.DAO
{
    public class CspDAO:DAO
    {
        private const String getAliasMapSql = "select lower(Alias),lower(Value) from Alias";
        private const String getAliasAssociationMapSql = "select lower(Alias),ContentTypeFieldIdAssociation from Alias";
        private const String getConnectionSql = "select lower(Project),Connection from Connections where Mode = @mode and mode <> 'notused'";
        private const String getDomainSql = "select lower(Project),Domain from Connections where Mode = @mode";
        private const String getProjectFromMaskSql = "select project from Domain_Mask where mask = @mask";
        private const String storeHashSql = "exec StoreHash @Content";
        private const String getHashContentSql = "select Content from HashStore where HashStoreID = @HashStoreID ";
        private const string GetLanguageMapSql = "select lower(LanguageCode), CspLanguageCode from LanguageMap";
        private const string GetContentCacheQuery =
@"
    select top(1) Content
    from ProxyCache with (nolock)
    where HASHBYTES('sha1',convert(nvarchar(4000),@url)) = UrlHash and (DATEDIFF(DAY,CreatedOn,GETDATE())) < @daysToCache and IsExpired = 0 and [RequestLanguageCode] like @lng
";

        private const string InsertToCacheQuery =
@"
    insert into [ProxyCache]
           ([UrlHash]
           ,[Project]
           ,[SyndicationType]
           ,[RequestType]
           ,[RequestLanguageCode]
           ,[Content]
           ,[RawUrl]
           ,[IsExpired]
           ,[CreatedOn])
    values (
        HASHBYTES('sha1',convert(nvarchar(4000),@url))
        ,@project
        ,@syndicationType
        ,@requestType
        ,@lng
        ,@content
        ,@url
        ,0
        ,GETDATE())
";


        public CspDAO(String connStr)
            :base(connStr)
        {
        }

        public CspDAO(String connStr, string mode)
            : base(connStr)
        {
            this.mode = mode;
        }

        //returns alias definition if found
        //otherwise returns alias
        public MapBase<String> getAliasMap()
        {
            MapBase<String> result = new MapBase<String>();
            result.AddRange(getMap(getAliasMapSql, null));
            return result;           
        }

        public MapBase<int> getCTAliasAssociationMap()
        {
            MapBase<int> result = new MapBase<int>();
            result.AddRange(getMapInt(getAliasAssociationMapSql, null));
            return result;
        }

        //returns encrypted connectionStr definition if found
        public IDictionary<String,String> getConnections()
        {
            IDictionary<String, String> target = getMap(getConnectionSql, new Dictionary<String, Object>(){
                    {"mode",mode}
                });
            
            return target;
        }

        //returns encrypted connectionStr definition if found
        public IDictionary<String, String> getDomains()
        {
            IDictionary<String, String> target = getMap(getDomainSql, new Dictionary<String, Object>(){
                    {"mode",mode}
                });

            return target;
        }

        //returns the project for the given mask
        public String DomainMaskLookUp(String mask)
        {
            String result = (String)executeScalar(getProjectFromMaskSql, new Dictionary<String, Object>(){
                    {"mask",mask}
                });
            return result;
        }

        public int storeHash(String content) {
            int result = (int)executeScalar(storeHashSql, new Dictionary<String, Object>(){
                    {"Content",content}
                });
            return result;
        }

        public string getHashContent(int hashId)
        {
            string result = (string)executeScalar(getHashContentSql, new Dictionary<String, Object>(){
                    {"HashStoreID",hashId}
                });
            return result;
        }

        public AliasCollection GetCtAliasAssociationCollection()
        {
            return getAliasCollection(AliasCollection.getAliasQuery);
        }

        public MapBase<string> GetLanguageMap()
        {
            MapBase<String> result = new MapBase<String>();
            result.AddRange(getMap(GetLanguageMapSql, null));
            return result;           
        }

        /// <summary>
        /// insert to cache
        /// </summary>
        /// <param name="requestedUrl"></param>
        /// <param name="project"></param>
        /// <param name="syndicationType"></param>
        /// <param name="requestType"></param>
        /// <param name="text"></param>
        /// <param name="daysToCache"></param>
        /// <param name="language"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public bool InsertToCache(string requestedUrl, string project, string syndicationType, string requestType, string text, int daysToCache, string languageCode)
        {
            bool f = true;
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand command = new SqlCommand(GetContentCacheQuery, conn, transaction);
                fillStatement(command, new Dictionary<string, object>
                                        {
                                            {"@url", requestedUrl},
                                            {"@daysToCache", daysToCache},
                                            {"@lng", string.IsNullOrEmpty(languageCode) ? "%" : languageCode}
                                        });
                object result = command.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    // this record exists already, we should ignore this one
                }
                else
                {
                    command.Parameters.Clear();
                    command.CommandText = InsertToCacheQuery;
                    fillStatement(command, new Dictionary<string, object>
                                        {
                                            {"@url", requestedUrl},
                                            {"@project", project},
                                            {"@syndicationType", syndicationType},
                                            {"@requestType", requestType},
                                            {"@content", Encoding.UTF8.GetBytes(text)},
                                            {"@lng", languageCode}
                                        });
                    command.ExecuteNonQuery();                    
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "InsertToCache");
                Log.Logger.Error(ex.StackTrace);
                f = false;
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return f;
        }

        public string GetContentCache(string url, string languageCode, int daysToCache)
        {
            string f = string.Empty;
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand command = new SqlCommand(GetContentCacheQuery, conn, transaction);
                fillStatement(command, new Dictionary<string, object>
                                           {
                                               {"@url", url},
                                               {"@daysToCache", daysToCache},
                                               {"@lng", string.IsNullOrEmpty(languageCode) ? "%" : languageCode}
                                           });
                object result = command.ExecuteScalar();
                if (result!=null&&result != DBNull.Value)
                    f = Encoding.UTF8.GetString((byte[]) result);
            }
            catch (Exception ex)
            {
                f = string.Empty;
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return f;
        }

        public int ClearCache(string url)
        {
            string query =
@"
    update [ProxyCache]
    set IsExpired = 1
    where [RawUrl] like @url+'%' and IsExpired = 0
";
            return ExecuteNonQuery(query, new Dictionary<string, object>
                                              {
                                                  {"@url", url}
                                              });
        }

        public ProjectContentTypeMap GetProjectContentTypeMap()
        {
            ProjectContentTypeMap map = new ProjectContentTypeMap();
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            SqlDataReader reader = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand command = new SqlCommand("SELECT [Id] ,[Identifier] ,[Project] ,[ContentTypeId] ,[ContentTypeFieldId] FROM [ProjectContentTypeMap]", conn, transaction);
                reader = command.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                        map.Add(new ProjectContentType(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3), reader.GetInt32(4)));
                    reader.Close();
                }

            }
            catch (Exception ex)
            {
                if (reader != null) reader.Close();
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return map;
        }

        // find project using the domain column
        public string FindProject(string domain)
        {
            return (string) executeScalar("select top 1 project from Connections c where c.Domain = @domain ", new Dictionary<String, Object>()
                                                                                                                   {
                                                                                                                       {"domain", domain}
                                                                                                                   });
        }

        /// <summary>
        /// find project using mf name and type
        /// </summary>
        /// <param name="mfrName">Mfr Name</param>
        /// <param name="requestedType">Request Type: campaign, inline, explore product, etc...</param>
        /// <returns>project or null</returns>
        public string FindProject(string mfrName, string requestedType)
        {
            return (string) executeScalar("select top 1 [Project] from [MfrNameRequestedTypeAndProjectMap] c where c.[ManufacturerName] = @mfrName and c.[RequestedType] = @type ",
                                          new Dictionary<String, Object>()
                                              {
                                                  {"mfrName", mfrName},
                                                  {"type", requestedType}
                                              });
        }
    }
}
