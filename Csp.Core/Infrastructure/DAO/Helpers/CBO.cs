using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace Csp.Core.Infrastructure.DAO.Helpers
{
    // ReSharper disable once InconsistentNaming
    internal class CBO
    {
        private static readonly IDictionary<string, List<PropertyInfo>> DataCache =
            new ConcurrentDictionary<string, List<PropertyInfo>>();

        public static List<PropertyInfo> GetPropertyInfo(Type objType)
        {
            DataCache.TryGetValue(objType.FullName, out var objProperties);
            if (objProperties != null)
            {
                return objProperties;
            }

            objProperties = new List<PropertyInfo>();
            foreach (var tempLoopVar_objProperty in objType.GetProperties())
            {
                objProperties.Add(tempLoopVar_objProperty);
            }
            objProperties.TrimExcess();
            DataCache.Add(objType.FullName, objProperties);
            return objProperties;
        }

        private static int[] GetOrdinals(List<PropertyInfo> objProperties, IDataReader dr)
        {
            var arrOrdinals = new int[objProperties.Count];
            if (dr != null)
            {
                var count = objProperties.Count - 1;
                for (var i = 0; i <= count; i++)
                {
                    arrOrdinals[i] = -1;
                    try
                    {
                        arrOrdinals[i] = dr.GetOrdinal(objProperties[i].Name);
                    }
                    catch
                    {
                        // property does not exist in datareader
                    }
                }
            }
            return arrOrdinals;
        }

        private static object CreateObject(Type objType, IDataReader dr, List<PropertyInfo> objProperties, int[] arrOrdinals)
        {
            var objObject = Activator.CreateInstance(objType);
            var count = objProperties.Count - 1;
            for (var i = 0; i <= count; i++)
                if (objProperties[i].CanWrite)
                    if (arrOrdinals[i] != -1)
                        if (Convert.IsDBNull(dr.GetValue(arrOrdinals[i])))
                            objProperties[i].SetValue(objObject, Null.SetNull(objProperties[i]), null);
                        else
                            try
                            {
                                objProperties[i].SetValue(objObject, dr.GetValue(arrOrdinals[i]), null);
                            }
                            catch
                            {
                                try
                                {
                                    var pType = objProperties[i].PropertyType;
                                    if (pType.BaseType == typeof(Enum))
                                        objProperties[i].SetValue(objObject,
                                            Enum.ToObject(pType, dr.GetValue(arrOrdinals[i])), null);
                                    else
                                        objProperties[i].SetValue(objObject,
                                            Convert.ChangeType(dr.GetValue(arrOrdinals[i]), pType), null);
                                }
                                catch
                                {
                                    // property does not exist in datareader
                                    objProperties[i].SetValue(objObject, Null.SetNull(objProperties[i]), null);
                                }
                            }
            return objObject;
        }

        public static object FillObject(DbDataReader dr, Type objType)
        {
            object objFillObject;
            var objProperties = GetPropertyInfo(objType);
            var arrOrdinals = GetOrdinals(objProperties, dr);
            if (dr != null && dr.Read())
                objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals);
            else
                objFillObject = null;
            dr?.Close();
            return objFillObject;
        }

        public static ArrayList FillCollection(DbDataReader dr, Type objType)
        {
            var objFillCollection = new ArrayList();
            if (dr == null)
                return objFillCollection;
            var objProperties = GetPropertyInfo(objType);
            var arrOrdinals = GetOrdinals(objProperties, dr);
            while (dr.Read())
            {
                var objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals);
                objFillCollection.Add(objFillObject);
            }
            dr.Close();
            return objFillCollection;
        }

        public static IList FillCollection(IDataReader dr, Type objType, IList objToFill)
        {
            if (dr == null)
                return objToFill;
            var objProperties = GetPropertyInfo(objType);
            var arrOrdinals = GetOrdinals(objProperties, dr);
            while (dr.Read())
            {
                var objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals);
                objToFill.Add(objFillObject);
            }
            dr.Close();
            return objToFill;
        }

        private static T CreateObject<T>(IDataReader dr, List<PropertyInfo> objProperties, int[] arrOrdinals)
            where T : class, new()
        {
            var objObject = new T();
            var count = objProperties.Count - 1;
            for (var i = 0; i <= count; i++)
                if (objProperties[i].CanWrite)
                    if (arrOrdinals[i] != -1)
                        if (Convert.IsDBNull(dr.GetValue(arrOrdinals[i])))
                            objProperties[i].SetValue(objObject, Null.SetNull(objProperties[i]), null);
                        else
                            try
                            {
                                objProperties[i].SetValue(objObject, dr.GetValue(arrOrdinals[i]), null);
                            }
                            catch
                            {
                                try
                                {
                                    var pType = objProperties[i].PropertyType;
                                    if (pType.BaseType == typeof(Enum))
                                        objProperties[i].SetValue(objObject,
                                            Enum.ToObject(pType, dr.GetValue(arrOrdinals[i])), null);
                                    else
                                        objProperties[i].SetValue(objObject,
                                            Convert.ChangeType(dr.GetValue(arrOrdinals[i]), pType), null);
                                }
                                catch
                                {
                                    // property does not exist in datareader
                                    objProperties[i].SetValue(objObject, Null.SetNull(objProperties[i]), null);
                                }
                            }
            return objObject;
        }

        public static T FillObject<T>(IDataReader dr) where T : class, new()
        {
            T objFillObject;
            var objProperties = GetPropertyInfo(typeof(T));
            var arrOrdinals = GetOrdinals(objProperties, dr);
            if (dr != null && dr.Read())
                objFillObject = CreateObject<T>(dr, objProperties, arrOrdinals);
            else
                objFillObject = null;
            dr?.Close();
            return objFillObject;
        }

        public static C FillCollection<T, C>(IDataReader dr)
            where T : class, new()
            where C : ICollection<T>, new()
        {
            var objFillCollection = new C();
            if (dr == null)
                return objFillCollection;
            var objProperties = GetPropertyInfo(typeof(T));
            var arrOrdinals = GetOrdinals(objProperties, dr);
            while (dr.Read())
            {
                var objFillObject = CreateObject<T>(dr, objProperties, arrOrdinals);
                objFillCollection.Add(objFillObject);
            }
            dr.Close();
            return objFillCollection;
        }

        public static List<T> FillCollection<T>(DbDataReader dr) where T : class, new()
        {
            return FillCollection<T, List<T>>(dr);
        }

        public static IList<T> FillCollection<T>(IDataReader dr, IList<T> objToFill) where T : class, new()
        {
            if (dr == null)
                return objToFill;
            var objProperties = GetPropertyInfo(typeof(T));
            var arrOrdinals = GetOrdinals(objProperties, dr);
            while (dr.Read())
            {
                var objFillObject = CreateObject<T>(dr, objProperties, arrOrdinals);
                objToFill.Add(objFillObject);
            }
            dr.Close();
            return objToFill;
        }

        public static object InitializeObject(object objObject, Type objType)
        {
            var objProperties = GetPropertyInfo(objType);
            var count = objProperties.Count - 1;
            for (var i = 0; i <= count; i++)
                if (objProperties[i].CanWrite)
                    objProperties[i].SetValue(objObject, Null.SetNull(objProperties[i]), null);
            return objObject;
        }
    }
}