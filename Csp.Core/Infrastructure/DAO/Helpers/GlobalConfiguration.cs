﻿using System.Configuration;
using Csp.Core.Infrastructure.Security;
using Libcore.Core.Extensions;

namespace Csp.Core.Infrastructure.DAO.Helpers
{
    public class GlobalConfiguration
    {

        public static string Mode { get; }
        public static string RootPubDir { get; }
        public static string MasterConnectionString { get; }
        public static string LogConnectionString { get; }
        public static string ReloadFilePath { get; }
        public static int DaysToUseProxyCache { get; }
        public static string DefaultCspJsPath { get; }
        public static string CampaignsMigrationCspJsPath { get; }
        public static int CacheTime { get; }
        public static int ProxyTimeOut { get; }
        public static string LogType { get; }
        public static string LogDomain { get; }
        public static string SmtpServer { get; }
        public static string SmtpUserName { get; }
        public static string SmtpPassword { get; }

        static GlobalConfiguration()
        {
            Mode = ConfigurationManager.AppSettings["mode"];
            RootPubDir = ConfigurationManager.AppSettings["RootPubDir"];
            ReloadFilePath = ConfigurationManager.AppSettings["reload_file_path"];
            DefaultCspJsPath = ConfigurationManager.AppSettings["default.csp.js"];
            CampaignsMigrationCspJsPath = ConfigurationManager.AppSettings["CampaignsMigration.csp.js"];
            LogType = ConfigurationManager.AppSettings["LogType"];
            LogDomain = ConfigurationManager.AppSettings["LogDomain"];
            SmtpServer = ConfigurationManager.AppSettings["smtpServer"];
            SmtpUserName = ConfigurationManager.AppSettings["smtpUserName"];
            SmtpPassword = ConfigurationManager.AppSettings["smtpPassword"];
            ProxyTimeOut = ConfigurationManager.AppSettings["ProxyTimeOut"].ChangeTypeTo<int>(60);
            CacheTime = ConfigurationManager.AppSettings["cacheTime"].ChangeTypeTo<int>(480);
            DaysToUseProxyCache = ConfigurationManager.AppSettings["DaysToUseProxyCache"].ChangeTypeTo<int>(-1);
            MasterConnectionString = CspDecoder.Decode(ConfigurationManager.AppSettings[$"{Mode}.csp_master"]);
            LogConnectionString = CspDecoder.Decode(ConfigurationManager.AppSettings[$"{Mode}.csp_redirect_logs"]);
        }
    }
}
