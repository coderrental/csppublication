﻿using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.DAO
{
    public class LogDAO:DAO
    {
        private const String getProductIdSql = "select Id from Product where MfrName = @mf and MfrPartNum = @pn and SyndicationType = @type";
        private const String insertProductSql = "insert into Product(MfrName, MfrPartNum, SyndicationType) values (@mf, @pn, @type);SELECT @@IDENTITY ";
        private const String insertProductLogSql =  "insert into ProductLog " +
            " (CompanyId, ProductId, AccessedDateTime, Browser, BrowserVersion, Referer, Host, RequestedUrl) values " +
            " (@companyId,@productId,@accessedDateTime,@browser,@browserVersion,@referer,@host,@requestedUrl); SELECT @@IDENTITY ";

        public LogDAO(String connStr)
            :base(connStr)
        {
        }

        //returns id from product table, null indicates not exist
        private int getProductId(String mf,String pn, String type)
        {
            int? target = (int?)executeScalar(getProductIdSql, new Dictionary<String, Object>(){
                    {"mf", mf},
                    {"pn", pn},
                    {"type", type}
                });
         
            return (target == null ? -1 : (int) target);
        }

        //returns id of row created
        private int insertProduct(String mf, String pn, String type)
        {
            Object id = executeScalar(insertProductSql, new Dictionary<String, Object>(){
                    {"mf", mf},
                    {"pn", pn},
                    {"type", type}
                });

            return id != null ? (int) ((Decimal) id) : -1;
        }

        public int insertProductIfEmpty(String mf, String pn, String type)
        {
            int id = getProductId(mf, pn, type);
            if (id == -1)
            {
                id = insertProduct(mf, pn, type);
            }
            return id;
        }

        //returns id of row created
        public int insertProductLog(int companyId, int productId, DateTime accessedDateTime, String browser, String browserVersion, String referer, String host, String requestedUrl)
        {
            Object id = executeScalar(insertProductLogSql, new Dictionary<String, Object>(){
                    {"companyId", companyId},
                    {"productId", productId},
                    {"accessedDateTime", accessedDateTime},
                    {"browser",browser},
                    {"browserVersion",browserVersion},
                    {"referer",referer},
                    {"host",host},
                    {"requestedUrl",requestedUrl}
                });

            return (int)((Decimal)id);
        }
        
      

    }
}
