﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class PublicationOptions
    {
        public string RootPubDir { get; set; }
    }
}
