﻿using System.Threading.Tasks;

namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class HTMLwPubName
    {
        public string HTML;
        public string PubID;
        public Task MyTask;

        public HTMLwPubName(string myHtml, string myPubId)
        {
            HTML = myHtml;
            PubID = myPubId;
        }
    }
}
