﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class StyleSheetModel
    {
        public string page_title { get; set; }
        public string meta_description { get; set; }
        public string meta_keywords { get; set; }
        public string inline_stylesheet { get; set; }
        public string external_stylesheet { get; set; }
        public string external_javascript { get; set; }
        public string backgroundcolor { get; set; }
        public string backgroundimage { get; set; }
        public string onload_script { get; set; }
        public string body_tag_extension { get; set; }
        public string inline_javascript { get; set; }
        public string Class { get; set; }
        public string pseudo_class { get; set; }
        public string element { get; set; }
        public string style_content { get; set; }
    }
}
