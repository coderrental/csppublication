﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class CspCategoryModel
    {
        public string category_Id { get; set; }
        public string categoryText { get; set; }
    }
}
