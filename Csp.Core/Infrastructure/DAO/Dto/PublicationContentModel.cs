﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class PublicationContentModel
    {
        public int? type { get; set; }
        public int? publications_pages_Id { get; set; }
        public string content { get; set; }
    }
}
