﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class CspParameterModel
    {
        public string base_publication_parameters { get; set; }
        public int publications_Id { get; set; }
        public int? index_page_Id { get; set; }
    }
}
