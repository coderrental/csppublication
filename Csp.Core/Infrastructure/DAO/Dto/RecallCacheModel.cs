﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class RecallCacheModel
    {
        public string Encoding { get; set; }
        public byte[] Content { get; set; }
    }
}
