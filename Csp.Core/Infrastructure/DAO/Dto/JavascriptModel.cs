﻿namespace Csp.Core.Infrastructure.DAO.Dto
{
    public class JavascriptModel
    {
        public string publications_pages_Id { get; set; }
        public string description { get; set; }
        public string content { get; set; }
    }
}
