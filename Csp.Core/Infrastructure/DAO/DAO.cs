using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication.Models;

namespace Csp.Core.Infrastructure.DAO
{
    public class DAO
    {
        protected String connStr;
        protected String mode;

        //empty constructor
        public DAO()
        {
        }

        //creates a dao to given project
        public DAO(String connStr)
        {
            this.mode = System.Configuration.ConfigurationManager.AppSettings["mode"];
            this.connStr = connStr;
        }

        //add parameters to SqlCommand
        protected void fillStatement(SqlCommand stmt, Dictionary<String, Object> parameters)
        {
            if (parameters != null)
            {
                foreach (String key in parameters.Keys)
                {
                    stmt.Parameters.Add(new SqlParameter(key, parameters[key]));
                }
            }
        }

        //opens a connection with "no lock" for reading
        protected SqlConnection getReadConnection(out SqlTransaction transaction)
        {
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            transaction = conn.BeginTransaction(IsolationLevel.ReadUncommitted);
            return conn;
        }

        //opens a connection with "no lock" for reading
        protected void closeConnection(SqlConnection conn, SqlTransaction transaction)
        {
            try
            {
                transaction.Commit();
            }
            catch (Exception) { }

            try
            {
                conn.Close();
            }
            catch (Exception) { }
        }

        //parses a 2 column resultset into a <key=column1,value=column2> dictionary
        public IDictionary<String, String> getMap(String sql, Dictionary<String, Object> parameters)
        {

            IDictionary<String, String> results = new MapBase<String>();
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            SqlDataReader reader = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                reader = stmt.ExecuteReader();
                while (reader.Read())
                {
                    String key = reader.GetSqlValue(0).ToString();
                    String value = reader.GetSqlValue(1).ToString();
                    results[key] = value;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            finally
            {
                if (reader != null) reader.Close();
                closeConnection(conn, transaction);
            }

            return results;
        }


        public IDictionary<String, int> getMapInt(String sql, Dictionary<String, Object> parameters)
        {
            IDictionary<String, int> results = new MapBase<int>();
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            SqlDataReader reader = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                reader = stmt.ExecuteReader();
                while (reader.Read())
                {
                    String key = reader.GetSqlValue(0).ToString();
                    int value = reader.GetInt32(1);
                    results[key] = value;
                }
            }
            finally
            {
                try { reader.Close(); }
                catch (Exception) { }
                closeConnection(conn, transaction);
            }

            return results;
        }

        //parses a 1 row resultset into a <key=column_name,value=column_value> dictionary
        public IDictionary<String, String> getMapTranspose(String sql, Dictionary<String, Object> parameters)
        {

            IDictionary<String, String> results = new MapBase<String>();
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            SqlDataReader reader = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                reader = stmt.ExecuteReader();
                if (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        String key = reader.GetName(i);
                        String value = reader.GetSqlValue(i).ToString();
                        results[key] = value;
                    }
                }
            }
            finally
            {
                try { reader.Close(); }
                catch (Exception) { }
                closeConnection(conn, transaction);
            }
            return results;
        }

        public AliasCollection getAliasCollection(String sql)
        {
            AliasCollection collection = new AliasCollection();
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            SqlDataReader reader = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                reader = stmt.ExecuteReader();
                if (reader != null)
                    while (reader.Read())
                    {
                        try
                        {
                            // Colum 4 null exception int32
                            collection.Add(new Alias(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),
                                reader.GetString(3), reader.GetInt32(4), reader[5].ToString()));
                        }
                        catch (Exception e)
                        {
                        }
                    }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            finally
            {
                try
                {
                    reader.Close();
                }
                catch (Exception)
                {
                }
                closeConnection(conn, transaction);
            }
            return collection;
        }

        public Object executeScalar(String sql, Dictionary<String, Object> parameters)
        {
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            Object retObj = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                retObj = stmt.ExecuteScalar();
            }
            catch (Exception ex)
            {
                retObj = null;
            }
            finally
            {
                closeConnection(conn, transaction);
            }

            return retObj;
        }
        public int ExecuteNonQuery(string sql, Dictionary<string, object> parameters)
        {
            int r = -1;
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                r = stmt.ExecuteNonQuery();
            }
            catch
            {
                r = -1;
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return r;
        }
        public DataTable ExecuteReader(string sql, Dictionary<string, object> parameters)
        {
            DataTable dataTable = null;
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                SqlDataAdapter adapter = new SqlDataAdapter(stmt);
                adapter.FillLoadOption = LoadOption.OverwriteChanges;
                dataTable = new DataTable();
                adapter.Fill(dataTable);
            }
            catch
            {
                dataTable = null;
            }
            finally
            {
                closeConnection(conn, transaction);
            }
            return dataTable;
        }

        public DataTable getTable(String sql, Dictionary<String, Object> parameters)
        {
            SqlConnection conn = null;
            SqlTransaction transaction = null;
            DataTable retObj = new DataTable();
            try
            {
                conn = getReadConnection(out transaction);
                SqlCommand stmt = new SqlCommand(sql, conn, transaction);
                fillStatement(stmt, parameters);
                retObj.Load(stmt.ExecuteReader());
            }
            finally
            {
                closeConnection(conn, transaction);
            }

            return retObj;

        }
    }
}
