using System;
using System.Collections.Generic;
using Csp.Core.Infrastructure.DAO.Models;
using Csp.Core.Infrastructure.Domain;
using Serilog;

namespace Csp.Core.Infrastructure.DAO
{
    public class EdTuple3<T,U,Z> // not supported in vs2008 :(
    {
        public T Item1 { get; private set; }
        public U Item2 { get; private set; }
        public Z Item3 { get; private set; }

        public EdTuple3(T item1, U item2, Z item3)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
        }
    }

    public static class EdTuple3
    {
        public static EdTuple3<T, U, Z> Create<T, U, Z>(T item1, U item2, Z item3)
        {
            return new EdTuple3<T, U, Z>(item1, item2, item3);
        }
    }

    public class SynDAO1:DAO
    {
        private static readonly ILogger log = Log.Logger;
        public const String validateSkuSql = "select COUNT(1) from content_main a " +
            " join content b on a.content_main_Id = b.content_main_Id " +
            " join content_fields c on b.content_id = c.content_Id " +
            " where b.content_types_languages_Id = @lng and b.stage_Id = 50 " +
            " and c.content_types_fields_Id = @ctfid and upper(c.value_text) like '%'+upper(@sku)+'%' ";


        public const String getConsumerOptionsSql = 
@"
select c.parametername,b.value_text 
from companies_consumers a
    join companies_parameters b on a.companies_Id = b.companies_Id
    join companies_parameter_types c on b.companies_parameter_types_Id=c.companies_parameter_types_Id
    where a.base_domain = @base_dom 
";

        public const String getConsumerOptionsSql2 =
@"
select a.*, b.*, c.*, d.companyname as sId, d.companies_Id as sName
from companies_consumers a
    join companies b on a.companies_Id = b.companies_Id
    join languages c on a.default_language_Id = c.languages_Id
    join companies d on d.companies_Id = b.parent_companies_Id
where a.base_domain = @base_dom 
";

        public const String getSynOptionsSql = "select SyndicationType,PublicationPageUrl from ProxyScriptReferences";
        public const String getSynExtraSql = "select SyndicationType,Js2 from ProxyScriptReferences";
        public const String getSynOptionActionSql = "select SyndicationType,ActionCode from ProxyScriptReferences";

        public const String getLanguageIdSql = "select languages_Id from languages where lower(languagecode) = lower(@code)";
        public const String getLanguageCodeSql = "select languagecode from languages where languages_Id = @id";
     
        public const String getTranslatorRowsSql = "select condition,replacement from url_translate where priority is not null order by priority";

        public const String createFormSql = "insert into Forms(companyId,submittedDate) values (@companyId,@submittedDate);SELECT SCOPE_IDENTITY();";
        public const String saveFormFieldSql = "insert into Form_Fields(formId,field_name,value_text) values(@formId,@field_name,@value_text)";

        public const String getFormSql = "select * from Forms where formId = @formId";
        public const String getFormDataSql = "select field_name,value_text from form_fields where formId = @formId";

        const string clearCacheSql =
@"
    update PubCache
    set IsExpired = 1
    from PubCache a 
	    join companies_consumers b on b.companies_Id = a.CompanyId
    where a.IsExpired = 0 and b.base_domain = @baseDomain
";

        const string getShowHoverSql =
@"DECLARE @OffMatchCount INT = 0
SELECT @OffMatchCount = COUNT(1) FROM dbo.companies_parameter_types	cpt 
INNER JOIN dbo.companies_parameters cp ON cpt.companies_parameter_types_Id = cp.companies_parameter_types_Id
WHERE cpt.parametername = 'CSP_ShowHover' AND cp.value_text = '0'
AND cp.companies_Id = @InCompanyId
IF @OffMatchCount = 0
SELECT '1'
ELSE	
SELECT '0'
";

        const string getCustomContenFieldSql =
@"if exists (
select ccf.value_text, ctf.fieldname,ccf.consumer_Id
from content c 
  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id  
        INNER JOIN dbo.custom_content_fields ccf ON ccf.content_fields_Id = cf.content_fields_Id
WHERE (ccf.consumer_Id = @consumerid AND c.content_Id = @incontentid AND ctf.fieldname = @fieldName) 
)
select ccf.value_text, ctf.fieldname,ccf.consumer_Id
from content c 
  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id  
        INNER JOIN dbo.custom_content_fields ccf ON ccf.content_fields_Id = cf.content_fields_Id
WHERE (ccf.consumer_Id = @consumerid AND c.content_Id = @incontentid AND ctf.fieldname = @fieldName) 
else
select cf.value_text, ctf.fieldname,@consumerid as 'consumer_Id'
from content c 
  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id          
WHERE (c.content_Id = @incontentid AND ctf.fieldname = @fieldName)
";
        public SynDAO1(String connStr)
            : base(connStr)
        {
        }

        public string ConnectionString
        {
            get { return connStr; }
        }

        public Language getLanguage(int id)
        {
            String code = (String) executeScalar(getLanguageCodeSql, new Dictionary<String, Object>(){
                    { "id", id }
                });
            return new Language(code, id);
        }
        public Language getLanguage(String codeOrId)
        {
            try
            {
                return getLanguage(int.Parse(codeOrId));
            }
            catch (FormatException)
            {
                int id = (int)executeScalar(getLanguageIdSql, new Dictionary<String, Object>(){
                    { "code", codeOrId }
                });
                return new Language(codeOrId, id);
            }
        }
        public Language getFirstAvailLanguage()
        {
            var result = getMap("select top 1 languages_Id, languagecode from languages", null);
            foreach (KeyValuePair<string, string> pair in result)
            {
                return new Language(pair.Value, Convert.ToInt32(pair.Key));
            }
            return null;
        }


        //returns consumer options from companies_parameters and companies_consumers table 
        public JsonMap getConsumerOptions(String consumer_base)
        {
            log.Information("retriving consumerOptions for "+consumer_base);
            JsonMap options = new JsonMap();
            options.AddRange(
                getMap(getConsumerOptionsSql, new Dictionary<String, Object>(){
                    { "base_dom", consumer_base }
                })
            );

            //get additional parameters
            var temp = getMapTranspose(getConsumerOptionsSql2, new Dictionary<String, Object>()
            {
                {"base_dom", consumer_base}
            });

            //var temp1 = options.Where(x => !temp.ContainsKey(x.Key)).ToDictionary(x=>x.Key, x=>x.Value);
            //var bad = options.Where(x => temp.ContainsKey(x.Key)).ToDictionary(x => x.Key, x => x.Value);

            //options.AddRange(
            //    temp1
            //);

            foreach (var p in temp)
            {
                if (!options.ContainsKey(p.Key))
                    options.Add(p.Key, p.Value);
                else
                {
                }
            }

           
            log.Information("result: " + options);

            return options;

        }

        //returns syndication default options
        public JsonMap getSynOptions()
        {
            JsonMap options = new JsonMap();
            options.AddRange(getMap(getSynOptionsSql, null));

            JsonMap extra = new JsonMap();
            extra.AddRange(getMap(getSynExtraSql, null));
            options.Add("extra", extra);

            JsonMap actionCodes = new JsonMap();
            actionCodes.AddRange(getMap(getSynOptionActionSql, null));
            options.Add("actionCodes", actionCodes);

            if (options.Count == 0)
            {
                throw new Exception("No data found[" + getSynOptionsSql + "][" + getSynExtraSql + "]");
            }
            return options;
        }

        public EdTuple3<string, string, string> GetVendorCompanyInfoFromProjectAliasResult(string InProjectAliasResult)
        {
            string queryCompanyID = @"
            DECLARE @found AS INT = 0; 
            SELECT  @found = COUNT(*) FROM dbo.companies c WHERE c.externalCode = @Project
            IF (@found > 0)
	            begin
	            SELECT TOP 1 CAST (companies_Id AS NVARCHAR(50)) FROM dbo.companies c WHERE c.externalCode = @Project
	            end
            ELSE 
	            begin
	            SELECT '0000'
	            end
            ";

            //companyname

            string queryCompanyName = @"
            DECLARE @found AS INT = 0; 
            SELECT  @found = COUNT(*) FROM dbo.companies c WHERE c.externalCode = @Project
            IF (@found > 0)
	            begin
	            SELECT TOP 1 CAST (companyname AS NVARCHAR(500)) FROM dbo.companies c WHERE c.externalCode = @Project
	            end
            ELSE 
	            begin
	            SELECT 'CompanyNameNotFound'
	            end
            ";

            string queryDCSID = @"
                DECLARE @found AS INT = 0; 
                SELECT  @found = COUNT(*) FROM dbo.companies c
		            INNER JOIN dbo.companies_parameter_types cpt ON cpt.parametername = 'WebTrendsDCSID'
		            INNER JOIN dbo.companies_parameters cp ON cp.companies_Id = c.companies_Id AND cp.companies_parameter_types_Id = cpt.companies_parameter_types_Id
		            WHERE c.externalCode = @Project
                IF (@found > 0)
	                begin
	                    SELECT TOP 1 cp.value_text FROM dbo.companies c
		                INNER JOIN dbo.companies_parameter_types cpt ON cpt.parametername = 'WebTrendsDCSID'
		                INNER JOIN dbo.companies_parameters cp ON cp.companies_Id = c.companies_Id AND cp.companies_parameter_types_Id = cpt.companies_parameter_types_Id
		                WHERE c.externalCode = @Project
	                end
                ELSE 
	                begin                    
	                SELECT  'ZZZZZZZZZZZZZZZZZZZZZZZZZ_ZZZZ'
	                end
            ";
            string companyid = (string)executeScalar(queryCompanyID, new Dictionary<String, Object>(){
                        {"project",InProjectAliasResult}                    
                });

            string companyname = (string)executeScalar(queryCompanyName, new Dictionary<String, Object>(){
                        {"project",InProjectAliasResult}                    
                });
            string DCSID = (string)executeScalar(queryDCSID, new Dictionary<String, Object>(){
                        {"project",InProjectAliasResult}                    
                });

            return new EdTuple3<string, string, string>(companyid, companyname, DCSID);
        }

        public bool validateSku(int ctfId, String sku, int lng)
        {
            int count = (Int32)executeScalar(validateSkuSql, new Dictionary<String, Object>(){
                    {"ctfId",ctfId},
                    {"sku", sku},
                    {"lng", lng}
                });
            return (count > 0);
        }

		public bool ValidateSkuUsingHashTable(int ctfId, string sku, int lngId)
		{
			if (ctfId == -1)
				return false;

			int count = 0;
			string query = @"
if exists (select 1 from sys.columns col where col.name = N'UpperCaseHash' and col.object_id = OBJECT_ID(N'Content_Field_HashTable'))
	select COUNT(*)
	from Content_Field_HashTable a WITH (NOLOCK) 
		join content b WITH (NOLOCK)  on a.ContentGUID=b.content_Id
	where a.ContentTypesFieldID=@ctfId
		and HASHBYTES('SHA1',convert(nvarchar(max), @sku)) = a.UpperCaseHash
		and b.content_types_languages_Id=@lngId
		and b.stage_Id=50
else
	select COUNT(*)
	from Content_Field_HashTable a WITH (NOLOCK) 
		join content b WITH (NOLOCK)  on a.ContentGUID=b.content_Id
	where a.ContentTypesFieldID=@ctfId
		and HASHBYTES('SHA1',convert(nvarchar(max), @sku)) = a.HashValue
		and b.content_types_languages_Id=@lngId
		and b.stage_Id=50 ";
			try
			{
				count = (int) executeScalar(query, new Dictionary<string, object>()
				                                   	{
				                                   		{"ctfId", ctfId},
				                                   		{"sku", sku},
				                                   		{"lngId", lngId}
				                                   	});
			}
			catch(Exception ex)
			{
				return validateSku(ctfId, sku, lngId);
			}
			return count > 0;
		}

		/// <summary>
		/// Using Field Name to check whether a SKU exists or not. By using ctfId, we can figure out which content type id it is to get the CTFID of the field name
		/// </summary>
		/// <param name="fieldName">Field Name to search</param>
		/// <param name="ctfId">content type field Id to determine which content type id it is to search in</param>
		/// <param name="sku">sku number to search</param>
		/// <param name="lngId">requested language</param>
		/// <returns></returns>
		public bool ValidateSkuUsingHashTable(string fieldName, int ctfId, string sku, int lngId)
		{
			if (ctfId == -1)
				return false;

			string query = @"
select * from content_types_fields a
	join (
select ctf.content_types_Id
from content_types_fields ctf 
where ctf.content_types_fields_Id=@ctfId) b on a.content_types_Id=b.content_types_Id
where a.fieldname = @fieldName ";
			int id = (int) executeScalar(query, new Dictionary<string, object>
			                                    	{
			                                    		{"ctfId", ctfId},
			                                    		{"fieldName", fieldName}
			                                    	});
			return ValidateSkuUsingHashTable(id, sku, lngId);
		}

    	public bool ValidateKeywordUsingHashTable(int ctfId, string keyword, int languageid)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            string validateSkuUsingHashTableSql = @"
    select COUNT(*) from Content_Field_HashTable a 
	    join content b on a.ContentGUID=b.content_Id
    where ContentTypesFieldID=@ctfId and b.content_types_languages_Id=@lngId and b.stage_Id=50
";
            parameters.Add("ctfId", ctfId);
            parameters.Add("lngId", languageid);

            string[] keywords = keyword.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
            if (keywords.Length == 0) return false;

            for (int i = 0; i < keywords.Length; i++)
            {
                parameters.Add("key" + i, keywords[i]);
                if (i == 0)
                    validateSkuUsingHashTableSql += " and (";
                else
                    validateSkuUsingHashTableSql += " or ";
                validateSkuUsingHashTableSql += "a.value_text=@key" + i + " ";
            }
            validateSkuUsingHashTableSql += ")";
            return (int) executeScalar(validateSkuUsingHashTableSql, parameters) > 0;
        }
        

        public IDictionary<String,String> getTranslatorRows()
        {
            return getMap(getTranslatorRowsSql, null);
        }

        internal List<string> getEmailAccounts()
        {
            throw new NotImplementedException();
        }

        internal List<string> getEmailUrls()
        {
            throw new NotImplementedException();
        }


        internal int createForm(int companyId)
        {
            Object result = executeScalar(createFormSql, new Dictionary<String, Object>(){
                    {"companyId",companyId},
                    {"submittedDate", DateTime.Now}
                });

            log.Information("result:" + result);

            return int.Parse(result.ToString());

        }

        internal void saveFormField(int formId, string field_name, string value_text)
        {
            executeScalar(saveFormFieldSql, new Dictionary<String, Object>(){
                    {"formId",formId},
                    {"field_name", field_name},
                    {"value_text", value_text}
                });
        }

        public IDictionary<String,String> getForm(int formId)
        {
            return getMapTranspose(getFormSql,new Dictionary<String, Object>(){
                    {"formId",formId}
            });
        }

        public IDictionary<String, String> getFormData(int formId)
        {
            return getMap(getFormDataSql, new Dictionary<String, Object>(){
                    {"formId",formId}
            });
        }

        public bool isBaseDomain(string baseDomain)
        {
            int i = (int) executeScalar("select count(base_domain) from companies_consumers where base_domain = @baseDomain ", new Dictionary<string, object>()
                                                                                                                                   {
                                                                                                                                       {"baseDomain", baseDomain}
                                                                                                                                   });
            return (i == 1);
        }

        public int ClearCache(string url)
        {
            return ExecuteNonQuery(clearCacheSql, new Dictionary<string, object>()
                                              {
                                                  {"@baseDomain", url}
                                              });
        }

        public int ClearFetchCache()
        {
            return ExecuteNonQuery("delete from FetchCache", new Dictionary<string, object>());
        }

        public CspContentCollection GetConnectionCollection(ProjectContentType projectContentType, string identifier, int languageId, int consumerId)
        {
            CspContentCollection collection = new CspContentCollection();
            try
            {
                IDictionary<string, string> contentPieces = getMap(@"
select cf.content_Id, cm.content_main_Id
from content_fields cf
    join content c on c.content_Id = cf.content_Id 
    join content_main cm on cm.content_main_Id = c.content_main_Id 
    join content_categories cc on cc.content_main_Id = cm.content_main_Id
    join consumers_themes ct on ct.category_Id = cc.category_Id and ct.supplier_Id = cm.supplier_Id and ct.consumer_Id = @consumerId
where cf.content_types_fields_Id = @ctfId and c.stage_Id = @stageId  and c.content_types_languages_Id = @lngId and cm.content_types_Id = @ctId
    and cf.value_text like @identifier
group by cm.content_main_Id, cf.content_Id
",
                                                            new Dictionary<string, object>
                                                                {
                                                                    {"@ctfId", projectContentType.ContentTypeFieldId},
                                                                    {"@stageId", 50},
                                                                    {"@lngId", languageId},
                                                                    {"@ctId", projectContentType.ContentTypeId},
                                                                    {"@identifier", identifier},
                                                                    {"@consumerId", consumerId}
                                                                });

                if (contentPieces.Count > 0)
                {
                    foreach (KeyValuePair<string, string> contentPiece in contentPieces)
                    {
                        CspContent content = new CspContent(getMap(@"
select ctf.fieldname, 
	case when ccf.value_text is null or CONVERT(nvarchar(max), ccf.value_text) = '' then cf.value_text
		else ccf.value_text
	end
from content_fields cf
	join content_types_fields ctf on ctf.content_types_fields_Id = cf.content_types_fields_Id
	left join custom_content_fields ccf on ccf.content_fields_Id=cf.content_fields_Id and ccf.consumer_Id=@consumerId
where cf.content_Id = @contentId ", new Dictionary<string, object>
                                                 {
                                                     {"@contentId", contentPiece.Key},
													 {"@consumerId", consumerId}
                                                 }));
                        if (content.Count > 0)
                        {
                            content.Add(getMapTranspose(@"
select a.supplier_Id as sId, c.companyname as sName, b.content_Id from content_main a
join content b  on a.content_main_Id = b.content_main_Id
join companies c on c.companies_Id = a.supplier_Id
where b.content_Id = @contentId	",
                                                        new Dictionary<string, object>
                                                            {
                                                                {"@contentId", contentPiece.Key}
                                                            }));
                            if (!content.ContainsKey("content_Id"))
                                content.Add("content_Id", contentPiece.Key);
                            collection.Add(content);
                        }
                    }
                }

//                object contentId = executeScalar(
//                                            @"
//select top 1 cf.content_Id
//from content_fields cf
//    join content c on c.content_Id = cf.content_Id 
//    join content_main cm on cm.content_main_Id = c.content_main_Id 
//    join content_categories cc on cc.content_main_Id = cm.content_main_Id
//    join consumers_themes ct on ct.category_Id = cc.category_Id and ct.supplier_Id = cm.supplier_Id and ct.consumer_Id = @consumerId
//where cf.content_types_fields_Id = @ctfId and c.stage_Id = @stageId  and c.content_types_languages_Id = @lngId and cm.content_types_Id = @ctId
//    and cf.value_text like @identifier
//",
//                                            new Dictionary<string, object>
//                                                {
//                                                    {"@ctfId", projectContentType.ContentTypeFieldId},
//                                                    {"@stageId", 50},
//                                                    {"@lngId", languageId},
//                                                    {"@ctId", projectContentType.ContentTypeId},
//                                                    {"@identifier", identifier},
//                                                    {"@consumerId", consumerId}
//                                                });

//                if (contentId != null)
//                {
//                    CspContent content = new CspContent(getMap(@"
//select ctf.fieldname, cf.value_text
//from content_fields cf
//	join content_types_fields ctf on ctf.content_types_fields_Id = cf.content_types_fields_Id
//where cf.content_Id = @contentId "
//                                                               , new Dictionary<string, object>
//                                                                     {
//                                                                         {"@contentId", contentId}
//                                                                     }));
//                    if (content.Count > 0)
//                    {
//                        content.Add(getMapTranspose(@"
//select a.supplier_Id as sId, c.companyname as sName from content_main a
//join content b  on a.content_main_Id = b.content_main_Id
//join companies c on c.companies_Id = a.supplier_Id
//where b.content_Id = @contentId	",
//                                                    new Dictionary<string, object>
//                                                        {
//                                                            {"@contentId", contentId}
//                                                        }));
//                        collection.Add(content);
//                    }

//                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error(ex.StackTrace);
            }
            return collection;            
        }

        //todo: need to implement this method
        public string FindXdmlInjectionQueryString(string queryType)
        {
            object obj = executeScalar("select QueryString from XDMLInjectionQueryMap where RequestType = @queryType",
                                       new Dictionary<string, object>
                                           {
                                               {"@queryType", queryType}
                                           });
            //return "p157(ct15|ct6&fMfPN~1={mfrsku.value}!)";
            return obj != null ? (string) obj : string.Empty;
        }

        /* @Name: getShowHoverValue
         * @Description: Get the Opt-out parameter for the Fly-out UI element from the DB.
         * @Parameters:  
         * InCompanyId = the current company ID
         * @Created on: 01/08/2014
         * @Author: Victor Roman
         * */
        public string getShowHoverValue(string InCompanyId)
        {
            object obj = executeScalar(getShowHoverSql,
                                       new Dictionary<string, object>
                                           {
                                               {"@InCompanyId", InCompanyId}
                                           });
          
            return obj != null ? (string)obj : string.Empty;
        }
        /* @Name: getCustomContenFieldValue
        * @Description: Get the Custom Content Field Value for the Buy Now button.
        * @Parameters:  
        * contentId = the content_Id
        * @Created on: 06/11/2014
        * @Author: Victor Roman
        * */
        public string getCustomContenFieldValue(string contentId, string consumerId, string customField)
        {
            object obj = executeScalar(getCustomContenFieldSql,
                                       new Dictionary<string, object>
                                           {
                                               {"@incontentid", contentId},
                                               {"@consumerid", consumerId},
                                               {"@fieldName", customField}
                                           });

            return obj != null ? (string)obj : string.Empty;
        }
    }
}
