﻿using System.Collections.Generic;
using Proxy.Domain;

namespace WebApplicationNet.Models
{
    public class DomainModel
    {
        public string project;
        public Language lng;
        public string RawUrlWithoutPort;
        public string remaining_domain;
        public string instanceId;
        public JsonMap consumerOptions;

        public string consumer_guid;
        private string _baseDomain;
        private string _getCallbackObj;
        protected static IDictionary<string, string> domains;

        public string BaseDomain
        {
            get => _baseDomain ?? consumer_guid + "." + domains[project];
            set => _baseDomain = value;
        }

        public string getCallbackObj
        {
            get => string.IsNullOrEmpty(_getCallbackObj) ? "{l:'" + lng.id + "',i:'" + instanceId + "',p:'" + project + "',q:'',getUrl:function(){return 'c" + consumer_guid + "'+(this.p?'-p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-i'+this.i:'')+'." + remaining_domain + "';}}" : _getCallbackObj;
            set => _getCallbackObj = value;
        }
        //{
        //    return "{l:'" + lng.id + "',i:'" + instanceId + "',p:'" + project + "',q:'',getUrl:function(){return 'c" + consumer_guid + "'+(this.p?'-p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-i'+this.i:'')+'." + remaining_domain + "';}}";
        //}

        public string UpdateUrl(Language language)
        {
            return string.Format("c{0}-p{1}-l{2}-i{3}.{4}", consumer_guid, project, language.id, instanceId, remaining_domain);
        }

    }
}
