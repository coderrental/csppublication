﻿using System;
using System.Collections.Specialized;
using System.Web.Caching;

namespace Csp.Core.Infrastructure.Publication.Models
{
    public class ProxyModel
    {
        public bool BlUseSearchUrl { get; set; }
        public string StrRawUrl { get; set; }
        public string RawUrl { get; set; }
        public int IntPublicationId { get; set; }
        public int IntPageId { get; set; }
        public string StrReferer { get; set; }
        public string ConnectionString { get; set; }
        public string StrServerName { get; set; }
        public string StrWorkUrl { get; set; }
        public long Ticks { get; set; }
        public string ReqSerVarQryStr { get; set; }
        public int? LngId { get; set; }
        public string LngCode { get; set; }
        public string ProjectName { get; set; }
        public string ProxyQuery { get; set; } = string.Empty;
        public bool IsNoCache { get; set; } = false;
        public bool IsInlineRequest { get; set; } = false;
        public int DaysToFetchCache { get; set; }

        public Uri InlineRequestUri { get; set; }
        public Uri OriginalRequestedUrl { get; set; }
        public Uri RequestUrl { get; set; }

        public NameValueCollection Form { get; set; }
        public NameValueCollection ServerVariables { get; set; }
        public Cache Cache { get; set; }
    }
}