﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Csp.Core.Infrastructure.Publication.Models
{
    public class Alias
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public Alias(int id, string aliasName, string aliasValue, string searchFieldName, int searchFieldValue, string syndicationType)
        {
            Id = id;
            SyndicationType = syndicationType;
            SearchFieldValue = searchFieldValue;
            AliasName = aliasName;
            AliasValue = aliasValue;
            SearchFieldName = searchFieldName;
        }

        public int Id { get; set; }

        public string AliasName { get; set; }

        public string AliasValue { get; set; }

        public string SearchFieldName { get; set; }

        public int SearchFieldValue { get; set; }

        public string SyndicationType { get; set; }

        #region Overrides of Object

        public override string ToString()
        {
            return string.Format("Alias Name: {0} --> {1}, {2} --> {3}", AliasName, AliasValue, SearchFieldName, SearchFieldValue);
        }

        #endregion
    }
    public class AliasCollection : List<Alias>
    {
        public const string getAliasQuery = "select id, lower(alias), value, SearchFieldName, ContentTypeFieldIdAssociation, SyndicationType from Alias";

        public int TryGetCtfIdByAliasNameAndSearchField(string aliasName, string searchFieldName, StringComparison stringComparison)
        {
            return TryGetCtfIdByAliasNameAndSearchField(aliasName, searchFieldName, null, stringComparison);
        }

        public int TryGetCtfIdByAliasNameAndSearchField(string aliasName, string searchFieldName, string syndicationType, StringComparison stringComparison)
        {
            Alias alias = null;
            if (!string.IsNullOrEmpty(syndicationType))
                alias = this.FirstOrDefault(a => !string.IsNullOrEmpty(a.SyndicationType) && a.SyndicationType.Equals(syndicationType, stringComparison)
                                             && a.AliasName.Equals(aliasName, stringComparison) && a.SearchFieldName.Equals(searchFieldName, stringComparison));
            if (alias == null)
                alias = this.FirstOrDefault(a => a.AliasName.Equals(aliasName, stringComparison) && a.SearchFieldName.Equals(searchFieldName, stringComparison) && string.IsNullOrEmpty(a.SyndicationType));

            return (alias != null) ? alias.SearchFieldValue : -1;
        }
    }
}
