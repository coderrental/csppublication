﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using CommonServiceLocator;
using Csp.Core.Infrastructure.DAO;
using Csp.Core.Infrastructure.DAO.Dto;
using Csp.Core.Infrastructure.DAO.Helpers;
using Csp.Core.Infrastructure.DAO.Models;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.MapApi.Utilities;
using Csp.Core.Infrastructure.Publication.Models;
using Csp.Core.Infrastructure.Publication.Repositories;
using Csp.Core.Infrastructure.Publication.Services;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Infrastructure.UrlParse;
using Csp.Core.Infrastructure.UrlParse.Utils;
using Csp.Core.Master.Services;
using Libcore.Core.Extensions;
using Serilog;

namespace Csp.Core.Infrastructure.Publication
{
    public static class PublicationEngine
    {
        #region CspHandler's properties
        //EntryPoint Mapping
        private static IDictionary<string, string> ElementTypes
            = new Dictionary<string, string> {
                {"1","link"},
                {"2","image"},
                {"3","iframe"},
                {"4","iframed"},
                {"10","campaign"}
            };

        //LaunchType Mapping
        private static Dictionary<string, string> ElementActions
            = new Dictionary<string, string>
            {
                {"1","lightbox"},
                {"2","newwindow"},
                {"3","none"}
            };

        // From Global
        //public static CspDAO CspDao;

        //public static MasterDbService MasterDb;

        public static IDictionary<String, String> Connections;
        public const string FTP_ROOT = "FtpRoot";

        // From Csp Handler
        public static readonly ILogger log = Log.Logger;
        private static Regex csp_query_pattern = new Regex(@"^p[0-9]+[\(%]", RegexOptions.IgnoreCase);
        private static Regex csp_d_aspx_pattern = new Regex(@"^\/d[\d]+.aspx", RegexOptions.IgnoreCase);
        private static Regex redirectPathPattern = new Regex(@"^\/[a-zA-Z0-9\-\.]+\/");

        private static LogDAO logDao = new LogDAO(GlobalConfiguration.LogConnectionString);

        public static IDictionary<string, string> AliasMap;
        public static IDictionary<string, string> LanguageMap;
        //public static MapBase<int> CTAliasAssociationMap;
        public static AliasCollection CtAliasAssociationCollection;
        public static ProxyScriptReferences ScriptReferences;

        //templates
        private static String csp_json_template;
        private static String csp_js;

        private static string reloadDateStamp = string.Empty;

        #endregion

        #region D*'s properties
        //  Variable to see if we need to use the URL from a search (action=)
        private static bool blUseSearchUrl;
        private static URLEngine Hold;
        private static string strRawURL = "";
        private static int intPublicationID;

        //  Holds the ID for the publication (which could have 1 or more pages and/or components). This is used to get 
        //  only pages/components from this publication
        private static string strReferer = "";
        private static string connectionString = "";
        private static string strServerName = "";

        private static string strWorkURL = "";
        private static long ticks;

        //  Holds the URL which will be used to get the content (will be used for the URLParser)
        private static Uri Request_URL;

        private static int? lngId;

        private static HttpRequestBase Request;
        private static HttpContextBase Context;
        private static HttpResponseBase Response;
        private static HttpSessionStateBase Session;

        //private static SqlConnection con => PublicationDao._con;
        #endregion

        #region Csp Handler

        public static void CspHandlerInit()
        {
            reloadDateStamp = ReadFromReloadFile(GlobalConfiguration.ReloadFilePath);

            //log.Information("load template files");
            //csp_json_template = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/App_Data/retJson.js");
            //csp_js = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/App_Data/csp.js");
            ////csp_js = File.ReadAllText("C:/Users/Public/Documents/Workspace/UI/universalresources/js/csp.js");
            ////csp_js = File.ReadAllText(@"C:\Users\Public\Documents\Repos\ui-uiux-8\UniversalResources\js\csp.js");
            ////csp_js = File.ReadAllText(@"C:\Users\Public\Documents\Repos\UI-cqnli-27\UniversalResources\js\csp.js");

            ////load alias map
            //try
            //{
            //    AliasMap = masterDb.GetAliasMap();
            //    //CTAliasAssociationMap = PublicationEngine.CspDao.getCTAliasAssociationMap();
            //    CtAliasAssociationCollection = masterDb.GetCtAliasAssociationCollection();
            //    LanguageMap = masterDb.GetLanguageMap();
            //}
            //catch (Exception ex)
            //{
            //    log.Error(ex.Message);
            //    log.Error(ex.StackTrace);

            //    if (masterDb == null)
            //    {
            //        log.Error("CspDAO is null");
            //        log.Warn("Re-initializing CspDAO");

            //        ReloadCsp(masterDb);

            //    }
            //}
            ReloadCsp(ServiceLocator.Current.GetInstance<IMasterDbService>());
        }

        public static void CspHandlerProcessRequest(HttpContext context)
        {
            var masterDb = ServiceLocator.Current.GetInstance<IMasterDbService>();
            var synDb = ServiceLocator.Current.GetInstance<ISynService>();

            var request = context.Request;
            var response = context.Response;

            CheckAndReloadIfApplicable(GlobalConfiguration.ReloadFilePath, masterDb);

            //string URLHashFilename = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(request.Url.AbsoluteUri));

            var mfrname = GetQueryString(request, "mfrname").ToLower().Trim();
            var locationId = HttpUtility.HtmlEncode(GetQueryString(request, "locationId", "location"));
            var requestPageId = GetQueryString(request, "p", "pId", "pageId");

            NameValueCollection customRequestVariables = new NameValueCollection();
            customRequestVariables.Add("audience", GetQueryString(request, "audience"));
            customRequestVariables.Add("category", GetQueryString(request, "category"));
            customRequestVariables.Add("banner", GetQueryString(request, "banner"));
            customRequestVariables.Add("keyword", GetQueryString(request, "key", "keyword", "keywords"));
            customRequestVariables.Add("launchtype", GetQueryString(request, "launchtype"));

            String host = string.Empty;
            host = string.IsNullOrEmpty(request.Headers["Host"]) ? context.Request.Url.Host : request.Headers["Host"];

            DomainBase domainUtil = null;
            var aliasLookup = GetNotEmpty(AliasMap.GetValueOrDefault(mfrname), mfrname);
            bool noCache = (request.QueryString["NOCACHE"] != null) || (request.QueryString["nocache"] != null);

            //log.Information("request recived: "+request.RawUrl);
            log.Information($"Host: {host}, Request Received: {request.RawUrl}");

            #region Handle Cache

            var daysToCache = GlobalConfiguration.DaysToUseProxyCache;
            if (noCache)
                daysToCache = -1;

            // Cache always disabled
            if (daysToCache > 0)
            {
                log.Information($"Cache is set to {daysToCache}");
                string requestLanguageCode = GetQueryString(request, "lng");
                string content = string.Empty;
                if (!string.IsNullOrEmpty(requestLanguageCode))
                    content = masterDb.GetContentCache(host + "/" + request.QueryString, string.Empty, daysToCache);
                else
                {
                    //we need to find the language code first
                    //the language code would be the consumer default language
                    domainUtil = DomainFactory.Get(host, aliasLookup, GetQueryString(request, "t"));

                    if (domainUtil != null && domainUtil.ConsumerOptions.TryGetValue("languagecode", out var languageCode))
                        content = masterDb.GetContentCache(host + "/" + request.QueryString, languageCode.ToString(), daysToCache);
                }
                if (!string.IsNullOrEmpty(content))
                {
                    log.Debug("\t\tFound cache content for : [" + host + "/" + request.QueryString + "]");
                    response.ContentType = "application/javascript";
                    response.Write(content);
                    response.End();
                }
                else
                {
                    log.Debug("\t\tCouldn't find cache content for : [" + host + "/" + request.QueryString + "]");
                }
            }
            else
                log.Warning("Cache is disabled");
            #endregion
            //using IBM p3p header editor to generate this
            //this is only for all IE browsers (this is for IE7 or lower because IE8+ uses postMessage to handle resizing)
            response.AddHeader("p3p", "CP=\"NOI CURa ADMa DEVa TAIa OUR BUS IND UNI COM NAV INT\"");
            //response.AddHeader("pragma", "no-cache");
            //response.CacheControl = "no-cache";
            //response.AddHeader("expires", "-1");

            //refresh csp_js for easier loading -- remove this line on prod
            //csp_js = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/App_Data/csp.js");
            //csp_js = File.ReadAllText("C:/Users/Public/Documents/Workspace/UI/universalresources/js/csp.js");
            //csp_js = File.ReadAllText(@"C:\Users\Public\Documents\Repos\ui-uiux-8\UniversalResources\js\csp.js");
            //csp_js = File.ReadAllText(@"C:\Users\Public\Documents\Repos\UI-cqnli-27\UniversalResources\js\csp.js");

            //process resolve request asap
            if (request.QueryString["csp_request_type"] == "resolve")
            {
                response.ContentType = "application/javascript";
                JsonMap retObj = new JsonMap();
                retObj["mf"] = aliasLookup;
                String scriptname = request.Url.Segments[request.Url.Segments.Length - 1] + request.Url.Query;
                string content = csp_json_template.Replace("{server:scriptname}", scriptname)
                    .Replace("{server:retObj}", retObj.ToString())
                    .Replace("{server:locationId}", locationId)
                    .Replace("{server:requestPageId}", requestPageId);
                response.Write(content
                );
                response.End();
            }

            /* The following section will attempt to load a CSP.JS file based on the configuration defined in the Web.config file
             * The expected parameter should be in the form of 
             */
            string cspjsFile = System.Configuration.ConfigurationManager.AppSettings[aliasLookup + ".csp.js"];
            string defaultcspjspath = GlobalConfiguration.DefaultCspJsPath;
            String syndicationType = GetNotEmpty(request.QueryString["t"], "showcase");
            if (syndicationType.Equals("campaign"))
            {
                cspjsFile = GlobalConfiguration.CampaignsMigrationCspJsPath;
            }
            if (cspjsFile == null)
            {
                if (defaultcspjspath == null)
                {
                    cspjsFile = AppDomain.CurrentDomain.BaseDirectory + "\\App_Data\\csp.js";
                }
                else
                {
                    cspjsFile = AppDomain.CurrentDomain.BaseDirectory + GlobalConfiguration.DefaultCspJsPath;
                    if (!File.Exists(cspjsFile))
                        cspjsFile = GlobalConfiguration.DefaultCspJsPath;
                }
            }

            csp_js = File.ReadAllText(cspjsFile);


            #region Custom Request Type: reload, clearcache and clearfetchcache
            string cspRequestType = GetQueryString(request, "csp_request_type");
            if (!string.IsNullOrEmpty(cspRequestType) && cspRequestType.Equals("reload", StringComparison.CurrentCultureIgnoreCase))
            {
                try
                {
                    masterDb = null;
                    Connections = null;
                    GC.Collect();

                    ReloadCsp(masterDb);

                    //upload reload flag file
                    reloadDateStamp = DateTime.Now.Ticks.ToString();
                    UpdateReloadFlagFile(GlobalConfiguration.ReloadFilePath, reloadDateStamp);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                    log.Error(ex.StackTrace);
                }
                response.End();
            }
            else if (!string.IsNullOrEmpty(cspRequestType) && cspRequestType.Equals("clearcache", StringComparison.CurrentCultureIgnoreCase))
            {
                string clearCacheMessage = string.Empty;
                try
                {
                    DomainBase domain = DomainFactory.Get(host, aliasLookup);
                    int n = synDb.ClearCache(domain.RawUrlWithoutPort);
                    clearCacheMessage = $"{n} records of publication cache has been cleared for {domain.RawUrlWithoutPort}<br />";

                    n = masterDb.ClearCache(domain.RawUrlWithoutPort);
                    clearCacheMessage += $"{n} records of proxy (script) cache have been cleared for {domain.RawUrlWithoutPort}<br />";
                }
                catch (Exception ex)
                {
                    clearCacheMessage = "Error: " + ex.Message;
                }
                finally
                {
                    response.Write(clearCacheMessage);
                    response.End();
                }
            }
            else if (!string.IsNullOrEmpty(cspRequestType) && cspRequestType.Equals("clearfetchcache", StringComparison.CurrentCultureIgnoreCase))
            {
                string clearCacheMessage = string.Empty;
                try
                {
                    DomainBase domain = DomainFactory.Get(host, aliasLookup);
                    int n = synDb.ClearFetchCache();
                    clearCacheMessage = $"{n} records of fetch cache have been cleared";
                }
                catch (Exception ex)
                {
                    clearCacheMessage = "Error: " + ex.Message;
                }
                finally
                {
                    response.Write(clearCacheMessage);
                    response.End();
                }

            }
            #endregion

            //if (host.Contains("pdigium2") && host.Contains(".syndication."))
            //{
            //    host = host.Replace("syndication", "digium2");
            //}


            if (domainUtil == null)
            {
                domainUtil = DomainFactory.Get(host, aliasLookup, GetQueryString(request, "t"));
            }

            if (domainUtil == null)
            {
                string type = GetQueryString(request, "t");
                if (type.Equals("exploreproduct", StringComparison.CurrentCultureIgnoreCase))
                {
                    //todo: record this request entry
                    //int id = -1, companyId;
                    //id = logDao.insertProductIfEmpty(mfrname, mfrname, "exploreproduct");
                    //String browser = string.IsNullOrEmpty(request.Browser.Browser) ? string.Empty : request.Browser.Browser;
                    //String browserVersion = string.IsNullOrEmpty(request.Browser.Version) ? string.Empty : request.Browser.Version;
                    //String referer = request.UrlReferrer == null ? string.Empty : request.UrlReferrer.AbsoluteUri;
                    //companyId = int.Parse((String)consumerOptions["companies_Id"]);
                    //logDao.insertProductLog(companyId, id, DateTime.Now, browser, browserVersion, referer, request.Headers["Host"], request.Url.Query);
                }
                log.Error("Domain Util is null");
                response.ContentType = "application/javascript";
                response.Write("//");
                response.End();
            }

            //PROJECT
            //String project = getNotEmpty(aliasLookup, domainUtil.project);

            // since we use alias lookup to initialize domain util, there is no need to use alias lookup again.
            string project = GetNotEmpty(domainUtil.Project, aliasLookup);

            String connStr = Connections[project.ToLower()];
            log.Information("create synDao[" + project + "]");
            SynSqlRepository synDao = SynSqlRepository.Create(connStr);

            //assign LANGUAGE
            Language language = null;

            String lng = GetQueryString(request, "lng");
            try
            {
                if (lng == "ez") lng = "en";
                language = synDao.GetLanguage(lng);
            }
            catch (Exception e)
            {
                if (LanguageMap == null)
                    LanguageMap = masterDb.GetLanguageMap();
                if (LanguageMap != null && !string.IsNullOrEmpty(lng))
                {
                    string lngTmp = TextUtil.removeNonAlphaNumeric(lng).ToLower();
                    if (LanguageMap.ContainsKey(lngTmp))
                    {
                        try
                        {
                            language = synDao.GetLanguage(LanguageMap[lngTmp]);
                        }
                        catch
                        {
                            language = domainUtil.Language;
                        }
                    }
                }

                if (language == null)
                    language = domainUtil.Language;
            }

            //add a few more variables to be used later
            customRequestVariables.Add("project", project);
            customRequestVariables.Add("consumerId", domainUtil.BaseDomain);

            //Determine request type
            var requestType = "script"; //default request type
            var proxy_query = request.ServerVariables["QUERY_STRING"];
            var proxyOriginalQuery = request.RawUrl;
            var consumerOptions = domainUtil.ConsumerOptions;

            #region @ltu 02/14/18 https://tiekinetix.atlassian.net/browse/CSP-6890
            // google injects gclid into the url, so we need to intercept and move the parameter to the end of the query string request
            // example: http://pgroupemichelin-c636407366762332861.syndication.tiekinetix.net/?gclid=a_abc%23/category/2035
            var gclIdIndex = request.RawUrl.IndexOf("?gclid", StringComparison.OrdinalIgnoreCase);
            if (gclIdIndex >= 0)
            {
                var hashTagIndex = request.RawUrl.IndexOf("%23", StringComparison.OrdinalIgnoreCase);
                if (hashTagIndex > 0 && hashTagIndex > gclIdIndex)
                {
                    var gclid = request.RawUrl.Substring(gclIdIndex, hashTagIndex - gclIdIndex);
                    proxyOriginalQuery = $"{proxyOriginalQuery.Substring(0, gclIdIndex)}{proxyOriginalQuery.Substring(hashTagIndex)}{gclid}";
                }
                proxy_query = string.Empty; // set this empty so we can force it to publication type
            }

            #endregion


            //JsonMap synOptions = synDao.getSynOptions();
            if (ScriptReferences == null)
                ScriptReferences = ProxyScriptReferences.Create();

            ProxyScriptReference synOptions = ScriptReferences.Get(project, synDao);


            if (csp_d_aspx_pattern.IsMatch(request.Url.AbsolutePath))
            {
                requestType = "publication";
            }

            if (request.QueryString["csp_request_type"] == "validate")
            {
                requestType = "validate";
            }
            else if (csp_query_pattern.IsMatch(proxy_query))
            {
                requestType = "publication";
            }
            else if (String.IsNullOrEmpty(proxy_query))
            {
                requestType = "publication";
                //proxy_query = getNotEmpty((String)consumerOptions.tryGet("base_publication_parameters"), (String)synOptions.tryGet(syndicationType));
                proxy_query = GetNotEmpty(consumerOptions.GetStringValue("base_publication_parameters"), synOptions.GetUrl(syndicationType));
                if (proxy_query != null && !csp_query_pattern.IsMatch(proxy_query))
                {
                    //get terms
                    Dictionary<String, String> terms = new Dictionary<String, String>();
                    foreach (String part in proxy_query.Split('&'))
                    {
                        String[] pair = part.Split('=');
                        terms[pair[0]] = pair[1];
                    }

                    //create translate table
                    TranslateTable translator = TranslateTable.GetTranslator(project);
                    proxy_query = translator.translate(terms);
                }
                else if (proxyOriginalQuery.IndexOf("?gclid", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    proxy_query = proxy_query + proxyOriginalQuery.Replace("%23", "#");
                }
            }
            else
            {
                //get terms
                Dictionary<String, String> terms = new Dictionary<String, String>();
                foreach (String name in request.QueryString.AllKeys)
                {
                    if (name == null) continue;

                    String value = request.QueryString[name];
                    if (name == "mfrsku")
                    {
                        value = TextUtil.removeNonAlphaNumeric(value);
                    }
                    terms[name] = value;
                }

                //create translate table 
                TranslateTable translator = TranslateTable.GetTranslator(project);
                String translated = translator.translate(terms);
                if (translated != null)
                {
                    proxy_query = translated;
                    requestType = "publication";
                }
            }

            //log.InformationFormat("Request Type: {0}", requestType);
            //log.Information("\tresult:" + consumerOptions.ToString());

            EdTuple3<string, string, string> VendorID_VendorName_VendorDCSID = synDao.GetVendorCompanyInfoFromProjectAliasResult(project);
            //retObj["VendorID"] = VendorID_VendorName_VendorDCSID.Item1;
            //retObj["VendorName"] = VendorID_VendorName_VendorDCSID.Item2;
            var DCSID = VendorID_VendorName_VendorDCSID.Item3;

            string text = "";
            switch (requestType)
            {
                case "validate":
                    log.Information("request is a validation");
                    response.CacheControl = "no-cache";
                    context.Session["PARENT_URL"] = request.QueryString["parentUrl"];
                    response.ContentType = "application/javascript";

                    text = GetJson(synDao, syndicationType, project, language, consumerOptions, synOptions, request, locationId, requestPageId, domainUtil, customRequestVariables);
                    if (!noCache && masterDb != null && daysToCache > 0)
                    {
                        //log.DebugFormat("Insert an entry to cache. daysToCache = {0}", daysToCache);
                        masterDb.InsertToCache(host + "/" + request.QueryString, project, syndicationType, requestType, text, daysToCache, language.code);
                    }
                    response.Write(text);
                    response.End();
                    break;

                case "script":
                    // var tmpLaunchType = getNotEmpty((String)consumerOptions.tryGet("LaunchType"),"");
                    String protocol = request.Url.Scheme;
                    var tmpEntryPoint = GetNotEmpty(consumerOptions.GetStringValue("EntryPoint"), "");
                    //if (mfrname.Equals("digitalchannel") 
                    var entryPointsAppURL = System.Configuration.ConfigurationManager.AppSettings["entryPointsAppURL" + (protocol.Equals("https") ? "s" : "")];
                    var entryPointsAppCampignsURL = System.Configuration.ConfigurationManager.AppSettings["entryPointsAppCampignsURL" + (protocol.Equals("https") ? "s" : "")];
                    var entryPointsAppSalesRCURL = System.Configuration.ConfigurationManager.AppSettings["entryPointsAppSalesRCURL" + (protocol.Equals("https") ? "s" : "")];
                    var entryPointsAppAssetSyndicationURL = ConfigurationManager.AppSettings["entryPointsAppAssetSyndicationURL" + (protocol.Equals("https") ? "s" : "")];
                    var entryPointsAppAssetSyndicationKey =
                        ConfigurationManager.AppSettings.AllKeys.Contains("entryPointsAppAssetSyndicationKey")
                            ? ConfigurationManager.AppSettings["entryPointsAppAssetSyndicationKey"]
                            : "AssetSyndication2";

                    var entryPointsAppInlineSyndicationURL = ConfigurationManager.AppSettings["entryPointsAppInlineSyndicationURL" + (protocol.Equals("https") ? "s" : "")];
                    var entryPointsAppInlineSyndicationKey =
                        ConfigurationManager.AppSettings.AllKeys.Contains("entryPointsAppInlineSyndicationKey")
                            ? ConfigurationManager.AppSettings["entryPointsAppInlineSyndicationKey"]
                            : "InlineSyndication";


                    var entryPointsAppPartnerLocatorSyndicationURL = ConfigurationManager.AppSettings["entryPointsAppPartnerLocatorSyndicationURL" + (protocol.Equals("https") ? "s" : "")];

                    var tmpBaseDomain = GetNotEmpty(consumerOptions.GetStringValue("base_domain"), "");
                    string[] tmpGUID = null;
                    if (!string.IsNullOrEmpty(tmpBaseDomain))
                    {
                        tmpGUID = tmpBaseDomain.Split('.');
                    }
                    if (syndicationType.Equals("showcase") && Convert.ToInt32(tmpEntryPoint) <= 4)
                    {
                        if (tmpGUID.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(entryPointsAppURL))
                            {
                                response.Redirect(entryPointsAppURL
                                    + mfrname
                                    + "/" + tmpGUID[0]
                                    + "/" + language.id.ToString()
                                    + "/" + syndicationType);

                                response.End();
                                break;
                            }
                        }
                    }
                    else if (syndicationType.Equals("campaign") && !project.Equals("quantore"))
                    {
                        if (tmpGUID.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(entryPointsAppCampignsURL))
                            {
                                response.Redirect(entryPointsAppCampignsURL
                                    + project
                                    + "/" + tmpGUID[0]
                                    + "/" + language.id.ToString()
                                    + "/" + GetNotEmpty((String)GetQueryString(request, "category"), "")
                                    + "/" + GetNotEmpty((String)GetQueryString(request, "audience"), "")
                                    + "/" + GetNotEmpty((String)GetQueryString(request, "banner"), ""));

                                response.End();
                                break;
                            }
                        }
                    }
                    else if (syndicationType.Equals(entryPointsAppAssetSyndicationKey, StringComparison.OrdinalIgnoreCase))
                    {
                        if (tmpGUID.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(entryPointsAppAssetSyndicationURL))
                            {
                                response.Redirect(entryPointsAppAssetSyndicationURL
                                                  + project
                                                  + "/" + tmpGUID[0]
                                                  + "/" + language.id.ToString()
                                                  + "/" + GetNotEmpty((String)GetQueryString(request, "keyword"), "")
                                                  + (!string.IsNullOrEmpty(GetQueryString(request, "banner")) ? "/" + GetQueryString(request, "banner") : ""));
                                response.End();
                                break;
                            }
                        }
                    }
                    else if (syndicationType.Equals(entryPointsAppInlineSyndicationKey, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(entryPointsAppInlineSyndicationURL))
                    {
                        var temp = HttpUtility.UrlEncode(GetNotEmpty((String)GetQueryString(request, "mfrsku"), GetNotEmpty((String)GetQueryString(request, "keyword"), "")));
                        response.Redirect(entryPointsAppInlineSyndicationURL
                                          + project
                                          + "/" + tmpGUID[0]
                                          + "/" + language.id.ToString()
                                          + "/" + temp
                                          + GetNotEmpty((String)"/" + GetQueryString(request, "filter"), ""));
                        response.End();
                        break;
                    }
                    else if (syndicationType.Equals("ppp"))
                    {
                        if (tmpGUID.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(entryPointsAppSalesRCURL))
                            {
                                response.Redirect(entryPointsAppSalesRCURL
                                    + project
                                    + "/" + tmpGUID[0]
                                    + "/" + language.id.ToString());
                                response.End();
                                break;
                            }
                        }
                    }
                    else if (syndicationType.Equals("partnerlocator"))
                    {
                        if (tmpGUID.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(entryPointsAppPartnerLocatorSyndicationURL))
                            {
                                response.Redirect(entryPointsAppPartnerLocatorSyndicationURL
                                    + project
                                    + "/" + tmpGUID[0]
                                    + "/" + language.id.ToString());
                                response.End();
                                break;
                            }
                        }
                    }
                    log.Information("script requested");
                    response.CacheControl = "no-cache";
                    String pn = TextUtil.removeNonAlphaNumeric(request.QueryString["mfrsku"]);
                    string cdsId = TextUtil.removeNonAlphaNumeric(GetQueryString(request, "cds", "cdsId"));
                    //Return csp.js with replacements
                    text = csp_js
                        .Replace("{consumer:dcsId}", consumerOptions.GetStringValue("dcsId")) //ltu
                        .Replace("{consumer:FIntegratedType}", GetNotEmpty(consumerOptions.GetStringValue("IntegratedType"), ""))
                        .Replace("{consumer:FBaseDomain}", domainUtil.BaseDomain)
                        .Replace("{consumer:FConsumerID}", consumerOptions.GetStringValue("companies_Id"))
                        .Replace("{consumer:FCompanyName}", consumerOptions.GetStringValue("companyname"))
                        .Replace("{server:protocol}", request.Url.Scheme)
                        .Replace("{server:domain}", host)
                        .Replace("{ server: callbackObj }", domainUtil.GetCallbackObj())
                        .Replace("{server:callbackObj}", domainUtil.GetCallbackObj())
                        .Replace("{server:component}", request.QueryString["t"])
                        .Replace("{server:mfrsku}", pn)
                        .Replace("{server:mfrname}", request.QueryString["mfrname"])
                        .Replace("{server:cleanmframe}", String.IsNullOrEmpty(aliasLookup) ? mfrname : aliasLookup)
                        .Replace("{server:lng}", language.id.ToString())
                        .Replace("{server:locationId}", locationId)
                        .Replace("{server:cdsId}", cdsId)
                        .Replace("{server:requestPageId}", requestPageId)
                        .Replace("{server:DCSID}", DCSID)
                        ;

                    text = ReplaceCustomRequestVariables(text, customRequestVariables);
                    text = ReplaceConsumerOptionParameters(text, consumerOptions);

                    if (!noCache && masterDb != null && daysToCache > 0)
                    {
                        //log.DebugFormat("Insert an entry to cache. daysToCache = {0}", daysToCache);
                        masterDb.InsertToCache(host + "/" + request.QueryString, project, syndicationType, requestType, text, daysToCache, language.code);
                    }

                    response.ContentType = "application/javascript";
                    response.Write(text);
                    response.End();
                    break;

                case "publication":
                    if (redirectPathPattern.IsMatch(request.Url.AbsolutePath))
                    {
                        log.Information("publication url not at root, sending redirect");
                        response.Redirect("/d1.aspx?" + request.QueryString);
                    }
                    else
                    {
                        log.Information("Setting the server side variables");
                        log.Debug($"PROXY QUERY: {proxy_query}");
                        log.Debug($"CS: {connStr}");
                        //Return publication via d1.aspx
                        //use context.items for request scope data transfer
                        context.Items["PROXY_CONN_STRING"] = connStr;
                        String new_url = "http://" + domainUtil.BaseDomain + "/d1.aspx?" + proxy_query;
                        //if (project.Equals("digium2"))
                        //{
                        //    new_url = new_url.Replace("syndication", "digium2");
                        //}
                        context.Items["PROXY_URL"] = new Uri(new_url);
                        context.Items["REQUESTED_URL"] = request.Url;
                        context.Items["PROXY_SERVER_VAR_QUERY"] = proxy_query;
                        context.Items["PROXY_SERVER_ORIGINAL_QUERY"] = proxyOriginalQuery;
                        context.Items["PROJECT_NAME"] = project;
                        context.Session["QUERY_STRING"] = proxy_query;
                        context.Session["INSTANCE_ID"] = domainUtil.InstanceId;
                        context.Session["CONSUMER_ID"] = domainUtil.ConsumerOptions.GetValueOrDefault("companies_Id");
                        //my session is exposed to xdml
                        context.Session["PROJECT_NAME"] = project;
                        //context.Session["language_Id"] = language.code;
                        context.Session["language_code"] = language.code;
                        context.Session["language_Id"] = language.id;
                        context.Items["NOCACHE"] = noCache;

                        log.Information("transfer to d1");

                        //get d number from request
                        string transferPath = null;
                        try
                        {
                            transferPath = request.Url.AbsolutePath;
                            int num = 1;
                            try
                            {
                                num = int.Parse(transferPath.Substring(2, transferPath.IndexOf('.', 2) - 2));
                            }
                            catch (Exception)
                            {
                                num = int.Parse(transferPath.Substring(2, 1));
                            }
                            finally
                            {
                                transferPath = "d" + num + ".aspx";
                                log.Information($"Transfer Path: {transferPath}");
                            }
                        }
                        catch (Exception)
                        {
                            transferPath = "d1.aspx";
                            log.Warning($"Default Transfer Path To: {transferPath}");
                        }
                        finally
                        {
                            if (transferPath != "d3.aspx")
                            {
                                response.CacheControl = "no-cache";
                            }
                            log.Information($"Actual Transfer: {transferPath}");

                            //if (!request.Url.AbsolutePath.Contains(transferPath))
                            //{
                            //    context.Server.TransferRequest(transferPath, true);
                            //}
                            context.RewritePath(transferPath, true);

                            log.Information("Actual Transfer is done");
                        }
                    }
                    break;
            }
        }

        private static string ReadFromReloadFile(string fileName)
        {
            string value = string.Empty;
            if (File.Exists(fileName))
            {
                using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[fileStream.Length];
                    bytes.Initialize();
                    fileStream.Read(bytes, 0, bytes.Length);
                    value = Encoding.ASCII.GetString(bytes);
                }
            }
            log.Information("Read From Reload File: {0} value: {1}", fileName, value);
            return value;
            //return string.IsNullOrEmpty(value) ? DateTime.Now.Ticks.ToString() : value;
        }

        private static void ReloadCsp(IMasterDbService masterDb)
        {
            log.Information("Begin Reload Csp");

            //initialize csp master connection
            Connections = masterDb.GetConnections();

            //initialize DomainUtil
            //DomainUtil.init(Connections, masterDb.GetDomains(), GlobalConfiguration.MasterConnectionString);

            //init translator factory
            TranslateTable.init(Connections);

            AliasMap = masterDb.GetAliasMap();
            LanguageMap = masterDb.GetLanguageMap();

            //CTAliasAssociationMap = Global.CspDao.getCTAliasAssociationMap();
            CtAliasAssociationCollection = masterDb.GetCtAliasAssociationCollection();

            //proxy script references
            if (ScriptReferences == null)
                ScriptReferences = ProxyScriptReferences.Create();

            log.Information("End Reload Csp");
        }

        private static void CheckAndReloadIfApplicable(string fileName, IMasterDbService masterDb)
        {
            log.Information("CheckAndReloadIfApplicable");
            string s = ReadFromReloadFile(fileName);
            if (s != reloadDateStamp)
            {
                log.Information("Reload Csp because time stamp is different");
                ReloadCsp(masterDb);
                reloadDateStamp = s;
            }
        }

        private static string GetQueryString(HttpRequest request, params string[] queryParams)
        {
            foreach (string param in queryParams)
            {
                foreach (string key in request.QueryString.AllKeys)
                {
                    if (param.Equals(key, StringComparison.CurrentCultureIgnoreCase)) return request.QueryString[key];
                }
            }
            return string.Empty;
        }

        private static string GetNotEmpty(string str1, string str2)
        {
            if (!string.IsNullOrEmpty(str1))
                return str1;
            return !string.IsNullOrEmpty(str2) ? str2 : null;
        }

        private static void UpdateReloadFlagFile(string fileName, string dateStamp)
        {
            log.Information("UpdateReloadFlagFile: " + dateStamp);
            ReaderWriterLock lk = new ReaderWriterLock();
            lk.AcquireWriterLock(TimeSpan.FromMinutes(1));
            try
            {
                using (var fileStream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(dateStamp);
                    fileStream.Write(bytes, 0, bytes.Length);
                    fileStream.Flush();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
            finally
            {
                lk.ReleaseWriterLock();
            }
        }

        private static string ReplaceTokens(string path, NameValueCollection collection)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string key in collection.AllKeys)
            {
                if (!dictionary.ContainsKey(key)) dictionary.Add("[" + key + "]", collection[key]);
            }
            return ReplaceTokens(path, dictionary);
        }

        private static string ReplaceTokens(string path, Dictionary<string, string> tokens)
        {
            int i = 0;
            UrlToken token = UrlToken.GetToken(path, '<', '>', i);
            while (token != null)
            {
                path = path.Remove(token.StartIndex, token.EndIndex - token.StartIndex + 1);
                UrlToken key = UrlToken.GetToken(token.Token, '[', ']', 0);
                if (key != null && tokens.ContainsKey(key.Token))
                {
                    if (!string.IsNullOrEmpty(tokens[key.Token]))
                    {
                        token.Token = token.Token.Replace(key.Token, tokens[key.Token]).Replace("<", "").Replace(">", "");
                        path = path.Insert(token.StartIndex, token.Token);
                        i = token.StartIndex + token.Token.Length;
                    }
                }
                token = UrlToken.GetToken(path, '<', '>', i);
            }

            // replace the rests
            foreach (string key in tokens.Keys)
                path = path.Replace(key, tokens[key]);
            return path;
        }

        public static string GetJson(SynSqlRepository synDao, string syndicationType, string project, Language language, JsonMap consumerOptions, ProxyScriptReference synOptions, HttpRequest request, string locationId, string requestPageId, DomainBase domainUtil, NameValueCollection customVariables)
        {
            String pn = TextUtil.removeNonAlphaNumeric(request.QueryString["mfrsku"]);
            String host = string.Empty;
            String mfrname = "";
            mfrname = request.QueryString["mfrname"];
            mfrname = string.IsNullOrEmpty(mfrname) ? "" : mfrname.ToLower().Trim();
            //mfrname = mfrname.ToLower().Trim();

            string cdsId = GetQueryString(request, "cds", "cdsId");
            String syncQueryString = "";

            //translate db objects
            JsonMap retObj = new JsonMap();

            host = string.IsNullOrEmpty(request.Headers["Host"]) ? request.Url.Host : request.Headers["Host"];

            /// Originally mfrname and project are the same, so we pass the project to retObj["project"] and use it to select 
            /// the correct webtrend file.
            /// However we need to seperate project and mfrname so that we have more control on which database we use to get content.
            /// Therefore we can't pass project to retObj["project"] anymore, we need to use mfrname instead.
            /// 
            /// There is another routine before this to find the alias (i.e. mfrname=LenovoDesktop ==> lenovo
            mfrname = GetNotEmpty(AliasMap.GetValueOrDefault(mfrname), mfrname);

            if (!string.IsNullOrEmpty(mfrname))
            {
                mfrname = mfrname.ToLower();
                retObj["project"] = mfrname;
            }
            else
            {
                retObj["project"] = project;
            }

            if (!string.IsNullOrEmpty(mfrname))
                mfrname = mfrname.Trim();
            EdTuple3<string, string, string> VendorID_VendorName_VendorDCSID = synDao.GetVendorCompanyInfoFromProjectAliasResult(retObj["project"].ToString());
            retObj["VendorID"] = VendorID_VendorName_VendorDCSID.Item1;
            retObj["VendorName"] = VendorID_VendorName_VendorDCSID.Item2;
            retObj["DCSID"] = VendorID_VendorName_VendorDCSID.Item3;


            retObj["SyndicationType"] = syndicationType;

            //get the default language
            Language defaultLanguage = null;
            try
            {
                defaultLanguage = new Language(domainUtil.ConsumerOptions.GetStringValue("languagecode"),
                    Convert.ToInt32(domainUtil.ConsumerOptions.GetDefaultLanguage()));
            }
            catch (Exception ex)
            {
                // do nothing
                defaultLanguage = null;
            }

            /* @Parameter: CSP_ShowHover
             * @Description: Opt-out parameter for the Fly-out UI element.
             * @Rules:  
             * 1 or empty = on (on by default)
             * 0 = off
             * Just to be used in the showcase syndication type
             * @Created on: 01/06/2014
             * @Author: Victor Roman
             * Pending Tasks: to connect and retrive the value of this parameter from the actual customer
             * */
            var CSP_ShowHover = int.Parse(synDao.GetShowHoverValue((String)consumerOptions["companies_Id"]));

            syndicationType = syndicationType.ToLower();
            switch (syndicationType.ToLower())
            {
                case "showcase":
                    retObj["show"] = "true";
                    //retObj["url"] = host + "/d1.aspx?" + getNotEmpty((String)synOptions.tryGet("showcase"), (String)consumerOptions.tryGet("base_publication_parameters"));
                    retObj["url"] = host + "/d1.aspx?" + GetNotEmpty(consumerOptions.GetStringValue("base_publication_parameters"), synOptions.GetUrl("showcase"));
                    retObj["ElementType"] = ElementTypes[consumerOptions.GetStringValue("EntryPoint")];
                    retObj["ElementAction"] = ElementActions[consumerOptions.GetStringValue("LaunchType")];
                    retObj["src"] = consumerOptions.GetValueOrDefault("SyndicationLinkImage");
                    retObj["text"] = consumerOptions.GetValueOrDefault("SyndicationLinkText");
                    retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                    retObj["CSP_ShowHover"] = CSP_ShowHover;
                    break;
                case "ppp":
                    retObj["show"] = "true";
                    retObj["url"] = host + "/d1.aspx?" + ReplaceTokens(GetNotEmpty(synOptions.GetUrl(syndicationType), consumerOptions.GetStringValue("base_publication_parameters")), request.QueryString);
                    retObj["ElementType"] = ElementTypes["4"];
                    retObj["ElementAction"] = ElementActions["3"];
                    retObj["src"] = consumerOptions.GetValueOrDefault("SyndicationLinkImage");
                    retObj["text"] = consumerOptions.GetValueOrDefault("SyndicationLinkText");
                    retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                    retObj["sId"] = consumerOptions.GetValueOrDefault("sId");
                    retObj["sName"] = consumerOptions.GetValueOrDefault("sName");
                    break;
                case "campaign":
                    /*
                     * if keyword exists, 
                     *      validate keyword: true
                     *          return campaign
                     *      validate keyword: false
                     *          if category is used, then use category
                     *  else
                     *      if category is used, then use category
                     *      
                     */
                    string hoverUrl = string.Empty;
                    if (!string.IsNullOrEmpty(customVariables.Get("keyword")))
                    {
                        int i = CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname ?? project, "keyword", syndicationType, StringComparison.OrdinalIgnoreCase);
                        if (synOptions.IsEmpty(syndicationType) || i == -1)
                        {
                            retObj["error"] = "unable to find key";
                        }
                        // check if request contains keyword
                        if (i > 0 && synDao.ValidateKeywordUsingHashTable(i, customVariables.Get("keyword"), language.id))
                        {
                            // request contains keyword, so use key_campaign instead of campaign.
                            string queryString = ReplaceTokens(synOptions.GetUrl("key_" + syndicationType), request.QueryString);
                            if (!string.IsNullOrEmpty(queryString))
                            {
                                retObj["url"] = host + "/d1.aspx" + queryString;
                                retObj["extra"] = synOptions.Get("key_" + syndicationType, "Js2");
                                hoverUrl = synOptions.Get("key_" + syndicationType, "PublicationPageUrlForHover");
                                if (!string.IsNullOrEmpty(hoverUrl))
                                {
                                    retObj["hover_url"] = host + "/d1.aspx" + ReplaceTokens(hoverUrl, request.QueryString);
                                    retObj["options"] = synOptions.Get("key_" + syndicationType, "ActionCode");
                                }
                            }
                        }
                        else
                        {
                            retObj["error"] = "failed validation";
                        }
                    }

                    retObj["show"] = "true";
                    if (!retObj.ContainsKey("url"))
                    {
                        // request doesn't contain keyword, use the default campaign entry
                        retObj["url"] = host + "/d1.aspx" + ReplaceTokens(synOptions.GetUrl(syndicationType.ToLower()), request.QueryString);
                        retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                        hoverUrl = synOptions.Get(syndicationType, "PublicationPageUrlForHover");
                        if (!string.IsNullOrEmpty(hoverUrl))
                        {
                            retObj["hover_url"] = host + "/d1.aspx" + ReplaceTokens(hoverUrl, request.QueryString);
                            retObj["options"] = synOptions.Get(syndicationType, "ActionCode");
                        }
                    }
                    retObj["ElementType"] = ElementTypes["10"]; // campaign type
                    if ("newwindow".Equals(customVariables["launchtype"]))
                        retObj["ElementAction"] = ElementActions["2"]; // new window
                    else
                        retObj["ElementAction"] = ElementActions["1"]; // default to lightbox
                    retObj["audience"] = GetQueryString(request, "audience");
                    retObj["category"] = GetQueryString(request, "category");
                    retObj["banner"] = GetQueryString(request, "banner");
                    retObj["domain"] = host;

                    break;
                case "exploreproduct":
                    //ltu : 4/4/11 - update the way the validation works
                    //if (synDao.validateSku(CTAliasAssociationMap.tryGet(mfrname),pn, language.id))
                    if (!string.IsNullOrEmpty(pn) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", StringComparison.CurrentCultureIgnoreCase), pn, language.id)
                        || (!string.IsNullOrEmpty(cdsId) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "cdsid", StringComparison.CurrentCultureIgnoreCase), cdsId, language.id)))
                    {
                        retObj["show"] = "true";
                        String path = host + "/d1.aspx" + synOptions.GetUrl(syndicationType);
                        //retObj["url"] = path.Replace("[lng]", language.code).Replace("[mfrsku]", pn);

                        retObj["url"] = ReplaceTokens(path, new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", language.code},
                                                                    {"[mfrsku]", pn},
                                                                    {"[cdsid]", cdsId}
                                                                });

                        retObj["ElementType"] = ElementTypes[consumerOptions.GetStringValue("SyndicationExploreProductEp")];
                        retObj["ElementAction"] = GetNotEmpty(ElementActions.GetValueOrDefault(consumerOptions.GetStringValue("SyndicationExploreProductLt")), "lightbox");
                        retObj["text"] = GetNotEmpty(consumerOptions.GetStringValue("SyndicationExploreProductText"), "Explore Product");
                        retObj["src"] = consumerOptions.GetValueOrDefault("SyndicationExploreProductImage");
                        retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                        retObj["locationId"] = locationId;
                        retObj["mfrsku"] = pn;
                    }
                    // switch to default language
                    else if ((defaultLanguage != null && language.id != defaultLanguage.id) &&
                        (!string.IsNullOrEmpty(pn) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", StringComparison.CurrentCultureIgnoreCase), pn, defaultLanguage.id)
                        || (!string.IsNullOrEmpty(cdsId) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "cdsid", StringComparison.CurrentCultureIgnoreCase), cdsId, defaultLanguage.id))))
                    {
                        retObj["show"] = "true";
                        //update the host with the right language
                        host = domainUtil.UpdateUrl(defaultLanguage);

                        String path = host + "/d1.aspx" + synOptions.GetUrl(syndicationType);
                        retObj["url"] = ReplaceTokens(path, new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", defaultLanguage.code},
                                                                    {"[mfrsku]", pn},
                                                                    {"[cdsid]", cdsId}
                                                                });

                        retObj["ElementType"] = ElementTypes[consumerOptions.GetStringValue("SyndicationExploreProductEp")];
                        retObj["ElementAction"] = GetNotEmpty(ElementActions.GetValueOrDefault(consumerOptions.GetStringValue("SyndicationExploreProductLt")), "lightbox");
                        retObj["text"] = GetNotEmpty(consumerOptions.GetStringValue("SyndicationExploreProductText"), "Explore Product");
                        retObj["src"] = consumerOptions.GetValueOrDefault("SyndicationExploreProductImage");
                        retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                        retObj["locationId"] = locationId;
                    }
                    // switch to the default domain language
                    else if ((language.id != domainUtil.Language.id) && (!string.IsNullOrEmpty(pn) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", StringComparison.CurrentCultureIgnoreCase), pn, domainUtil.Language.id)
                        || (!string.IsNullOrEmpty(cdsId) && synDao.ValidateSku(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "cdsid", StringComparison.CurrentCultureIgnoreCase), cdsId, domainUtil.Language.id))))
                    {
                        retObj["show"] = "true";
                        String path = host + "/d1.aspx" + synOptions.GetUrl(syndicationType);
                        retObj["url"] = ReplaceTokens(path, new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", domainUtil.Language.code},
                                                                    {"[mfrsku]", pn},
                                                                    {"[cdsid]", cdsId}
                                                                });
                        retObj["ElementType"] = ElementTypes[consumerOptions.GetStringValue("SyndicationExploreProductEp")];
                        retObj["ElementAction"] = GetNotEmpty(ElementActions.GetValueOrDefault(consumerOptions.GetStringValue("SyndicationExploreProductLt")), "lightbox");
                        retObj["text"] = GetNotEmpty(consumerOptions.GetStringValue("SyndicationExploreProductText"), "Explore Product");
                        retObj["src"] = consumerOptions.GetValueOrDefault("SyndicationExploreProductImage");
                        retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                        retObj["locationId"] = locationId;
                    }
                    else
                    {
                        //log invalid mf,pn,lng combo's
                        log.Information("invalid sku (" + CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", StringComparison.CurrentCultureIgnoreCase) + "," + pn + "," + language.code + ")");
                        retObj["message"] = "not valid (" + CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", StringComparison.CurrentCultureIgnoreCase) + "," + pn + "," + language.code + ")";
                        if (!string.IsNullOrEmpty(pn))
                        {
                            int id = logDao.insertProductIfEmpty(project, pn, "exploreproduct");
                            String browser = string.IsNullOrEmpty(request.Browser.Browser) ? string.Empty : request.Browser.Browser;
                            String browserVersion = string.IsNullOrEmpty(request.Browser.Version) ? string.Empty : request.Browser.Version;
                            String referer = request.UrlReferrer == null ? string.Empty : request.UrlReferrer.AbsoluteUri;
                            int companyId = int.Parse((String)consumerOptions["companies_Id"]);
                            logDao.insertProductLog(companyId, id, DateTime.Now, browser, browserVersion, referer, request.Headers["Host"], request.Url.Query);
                        }
                    }
                    break;
                case "inline_showcase2":

                    //ctfId = CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", syndicationType, StringComparison.CurrentCultureIgnoreCase);

                    //if (!string.IsNullOrEmpty(pn) && synDao.ValidateSkuUsingHashTable(ctfId, pn, language.id)
                    //    || (!string.IsNullOrEmpty(cdsId) && synDao.ValidateSkuUsingHashTable(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "cdsid", syndicationType, StringComparison.CurrentCultureIgnoreCase), cdsId, language.id)))
                    //{
                    //    retObj["ElementType"] = syndicationType.ToLower();
                    //    retObj["ElementAction"] = "none";
                    //    retObj["locationId"] = locationId;
                    //    retObj["show"] = "true";
                    //    retObj["extra"] = request.QueryString.AllKeys.Select(a => Tuple.Create(a, request.QueryString[a])).ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2);
                    //    retObj["mfrsku"] = pn;
                    //    retObj["lng"] = language.code;
                    //    retObj["cspId"] = VendorID_VendorName_VendorDCSID.Item1;
                    //}

                    //else
                    //{
                    //    retObj["show"] = "false";
                    //}

                    retObj["ElementType"] = syndicationType.ToLower();
                    retObj["ElementAction"] = "none";
                    retObj["locationId"] = locationId;
                    retObj["show"] = "true";
                    retObj["extra"] = request.QueryString.AllKeys.Select(a => Tuple.Create(a, request.QueryString[a])).ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2);
                    retObj["mfrsku"] = pn;
                    retObj["lng"] = language.id;
                    retObj["cspId"] = consumerOptions["companies_Id"].ToString();
                    retObj["projectId"] = project;
                    break;
                //inline showcase injection
                case "inline_showcase":
                    int ctfId = CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "mfrsku", syndicationType, StringComparison.CurrentCultureIgnoreCase);

                    if (!string.IsNullOrEmpty(pn))
                    {
                        pn = pn.ToUpper();
                    }

                    if (!string.IsNullOrEmpty(pn) && synDao.ValidateSkuUsingHashTable(ctfId, pn, language.id)
                        || (!string.IsNullOrEmpty(cdsId) && synDao.ValidateSkuUsingHashTable(CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname, "cdsid", syndicationType, StringComparison.CurrentCultureIgnoreCase), cdsId, language.id)))
                    {
                        retObj["show"] = "true";
                        syncQueryString = host + "/d1.aspx" + synOptions.GetUrl(syndicationType);
                        retObj["url"] = ReplaceTokens(syncQueryString, new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", language.code},
                                                                    {"[mfrsku]", pn},
                                                                    {"[mfrname]", mfrname},
                                                                    {"[cdsid]", cdsId},
                                                                    {"[locationId]", locationId},
                                                                    {"[p]", requestPageId},
                                                                    {"[keyword]",""}
                                                                });
                        retObj["ElementType"] = syndicationType.ToLower();
                        retObj["ElementAction"] = "none";
                        retObj["locationId"] = locationId;
                        retObj["p"] = requestPageId;
                    }
                    else if (!string.IsNullOrEmpty(pn) && synDao.ValidateSkuUsingHashTable("Keywords", ctfId, pn, language.id))
                    {

                        retObj["show"] = "true";
                        syncQueryString = host + "/d1.aspx" + synOptions.GetUrl(syndicationType);
                        retObj["url"] = ReplaceTokens(syncQueryString, new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", language.code},
                                                                    {"[mfrsku]", ""},
                                                                    {"[mfrname]", mfrname},
                                                                    {"[cdsid]", cdsId},
                                                                    {"[locationId]", locationId},
                                                                    {"[p]", requestPageId},
                                                                    {"[keywords]", pn}
                                                                });
                        log.Debug($"***INLINE_SHOWCASE: {retObj["url"]}");
                        retObj["ElementType"] = syndicationType.ToLower();
                        retObj["ElementAction"] = "none";
                        retObj["locationId"] = locationId;
                        retObj["p"] = requestPageId;
                    }
                    else
                    {
                        retObj["show"] = "false";
                    }

                    break;
                case "dynamicbox":
                    // note: this is an implementation for quantore only. 
                    //retObj["url"] = request.Url.Scheme + "://" + ReplaceTokens(host + "/d1.aspx" + synOptions.GetUrl(syndicationType), request.QueryString);
                    retObj["url"] = ReplaceTokens(host + "/d1.aspx" + synOptions.GetUrl(syndicationType), new Dictionary<string, string>
                                                                {
                                                                    {"[lng]", language.code},
                                                                    {"[mfrsku]", pn},
                                                                    {"[mfrname]", mfrname},
                                                                    {"[cdsid]", cdsId},
                                                                    {"[locationId]", locationId},
                                                                    {"[p]", requestPageId}
                                                                });
                    retObj["show"] = "true";
                    retObj["ElementType"] = syndicationType.ToLower();
                    retObj["ElementAction"] = "none";
                    retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                    break;
                #region ltu 12/8/2014 added logic to handle asset syndication
                case "assetsyndication":
                    // check to see if we are using CSP_Unique_Key or Local_Keywords
                    //customVariables

                    string value = customVariables["keyword"], key = "CSP_Unique_Key";
                    int count = synDao.GetValue<int>("select count(*) from Content_Field_HashTable a where a.ContentTypeID = @ctId and a.ContentTypesFieldName = @fieldName and a.value_text=@key",
                        synDao.ToSqlParameters(new Dictionary<string, object>
                        {
                            {"@key", value},
                            {"@ctId", 21000},
                            {"@fieldName", "CSP_Unique_Key"}
                        }).ToArray());
                    if (count == 0)
                    {
                        count = synDao.GetValue<int>("select count(*) from Content_Field_HashTable a where a.ContentTypeID = @ctId and a.ContentTypesFieldName = @fieldName and a.value_text=@key",
                            synDao.ToSqlParameters(new Dictionary<string, object>
                            {
                                {"@key", value},
                                {"@ctId", 21000},
                                {"@fieldName", "Local_Keywords"}
                            }).ToArray());
                        key = "Local_Keywords";
                    }

                    if (count > 0)
                    {
                        retObj["url"] = ReplaceTokens(host + "/d1.aspx" + synOptions.GetUrl(syndicationType), new Dictionary<string, string>
                        {
                            {"[lng]", language.code},
                            {"[mfrsku]", pn},
                            {"[mfrname]", mfrname},
                            {"[cdsid]", cdsId},
                            {"[locationId]", locationId},
                            {"[p]", requestPageId},
                            {"[Field]",key},
                            {"[Value]",value},
                            {"[banner]",GetQueryString(request, "banner")}
                        });
                        retObj["show"] = "true";
                        retObj["ElementType"] = syndicationType.ToLower();
                        retObj["ElementAction"] = "none";
                        retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                        retObj["banner"] = GetQueryString(request, "banner");
                    }
                    else
                    {
                        retObj["show"] = "false";
                    }
                    break;
                #endregion
                default:
                    if (syndicationType.StartsWith("key_"))
                    {
                        //int i = CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname ?? project, "mfrsku", StringComparison.OrdinalIgnoreCase);
                        int i = CtAliasAssociationCollection.TryGetCtfIdByAliasNameAndSearchField(mfrname ?? project, "mfrsku", syndicationType, StringComparison.OrdinalIgnoreCase);
                        if (synOptions.IsEmpty(syndicationType) || i == -1)
                        {
                            retObj["show"] = "false";
                            break;
                        }

                        if (synDao.ValidateKeywordUsingHashTable(i, pn, language.id))
                        {
                            retObj["show"] = "true";
                            retObj["url"] = host + "/d1.aspx" + ReplaceTokens(synOptions.GetUrl(syndicationType), request.QueryString);
                            retObj["hover_url"] = host + "/d1.aspx" + ReplaceTokens(synOptions.Get(syndicationType, "PublicationPageUrlForHover"), request.QueryString);
                            retObj["ElementType"] = "image";
                            retObj["ElementAction"] = "lightbox";
                            retObj["text"] = syndicationType;
                            retObj["src"] = consumerOptions.GetValueOrDefault(GetQueryString(request, "p")) ?? request.Url.Scheme + "://" + host + "/images/" + project + "/" + syndicationType + ".png";
                            retObj["src_hover"] = consumerOptions.GetValueOrDefault(GetQueryString(request, "p") + "_hover") ?? request.Url.Scheme + "://" + host + "/images/" + project + "/" + syndicationType + "_hover.png";
                            retObj["extra"] = synOptions.Get(syndicationType, "Js2");
                            retObj["options"] = synOptions.Get(syndicationType, "ActionCode");
                            retObj["mfrsku"] = pn;
                        }
                    }
                    else
                        retObj["show"] = "false";
                    break;

            }

            String scriptname = request.Url.Segments[request.Url.Segments.Length - 1] + request.Url.Query;
            return csp_json_template.Replace("{server:scriptname}", scriptname)
                .Replace("{server:retObj}", retObj.ToString())
                .Replace("{server:locationId}", locationId)
                .Replace("{server:requestPageId}", requestPageId);
        }

        private static string ReplaceConsumerOptionParameters(string text, JsonMap options)
        {
            try
            {
                StringBuilder sb = new StringBuilder(text);
                Regex r = new Regex(@"{consumer:F(?<var>\w*)}", RegexOptions.Compiled);
                Match m;
                object obj;
                while (r.IsMatch(sb.ToString()))
                {
                    m = r.Match(sb.ToString());
                    sb.Remove(m.Index, m.Length);
                    obj = options.GetValueOrDefault(m.Groups["var"].Value);
                    if (obj != null)
                        sb.Insert(m.Index, obj);
                }
                text = sb.ToString();
            }
            catch { }
            return text;
        }

        private static string ReplaceCustomRequestVariables(string text, NameValueCollection collection)
        {
            var s = text;
            foreach (string key in collection.AllKeys)
            {
                if (!string.IsNullOrEmpty(collection[key]))
                    s = s.Replace("{server:" + key + "}", collection[key]);
            }
            //remove all server variables that have no values
            Regex r = new Regex(@"{server:(?<var>\w*)}", RegexOptions.Compiled);
            Match m;
            while (r.IsMatch(s))
            {
                m = r.Match(s);
                s = s.Remove(m.Index, m.Length);
            }
            return s;
        }

        #endregion

        #region D1 Function

        public static ResponseResult D1Function(
            HttpContextBase context,
            ProxyModel proxyModel,
            ISynService service)
        {
            Request = context.Request;
            Context = context;
            Response = context.Response;
            Session = context.Session;

            var contentType = "text/html";
            var responseContent = "";

            var RelatedPubHTML = new List<HTMLwPubName>();
            int? intPageID = null;
            var strPageContent = "";
            var intPublicationType = 0;
            //  Holds the type for the publication being requested. If it's not 0, we don't have to search for header/CSS
            var strURLHost = "";
            //  Get the ApplicationPool for which we need to handle the request. Depending on the ApplicationPool name, we use
            //  a specific connectionstring in the Web.config with the same name. The only difference is the databasename.
            // Dim strApplicationPoolName As String = getApplicationPoolName(strAppPath)
            // connectionString = des.Decrypt(System.Configuration.ConfigurationManager.AppSettings.Item("JYtPEQEIUUhCxJ9hihnBE++k7Kc+1hEi"))

            connectionString = proxyModel.ConnectionString;
            Request_URL = proxyModel.RequestUrl;
            lngId = proxyModel.LngId;

            var daysToFetchCache = proxyModel.DaysToFetchCache;
            var IsInlineRequest = proxyModel.IsInlineRequest;

            // if dcm_search is being used, then skip cache
            var isNoCache = Request.Form.AllKeys.Any(formKey => string.Equals(formKey, "dcm_search"));
            // cache
            if (daysToFetchCache > 0 && isNoCache == false)
            {
                var cachedResult = service.RecallFromCache(Request_URL, daysToFetchCache, lngId);
                if (!string.IsNullOrEmpty(cachedResult))
                {
                    if (IsInlineRequest)
                    {
                        contentType = "application/javascript";
                        responseContent = InlineContent.CreateJavascript(cachedResult, connectionString, proxyModel.ProjectName, proxyModel.InlineRequestUri, proxyModel.ProxyQuery);
                        return new ResponseResult { Content = responseContent, Type = contentType };
                    }

                    // normal request
                    return new ResponseResult { Content = cachedResult, Type = contentType };
                }

            }

            URLEngine.CacheTime = GlobalConfiguration.CacheTime;

            if (!string.IsNullOrEmpty(proxyModel.RequestUrl.Host))
            {
                //  Check if we have a value for Request_URL.Host.
                //  #############################################################################################################################
                //  # GET PARAMETERS AND/OR PAGEID FOR THIS DOMAIN/COMPANY                                                                      #
                //  #############################################################################################################################
                strWorkURL = proxyModel.StrWorkUrl;
                strRawURL = proxyModel.StrRawUrl;
                intPageID = proxyModel.IntPageId;
                intPublicationID = proxyModel.IntPublicationId;

                //  ##############################################################################################################################   
                //  # SEARCH FOR CONTENT BASED ON USER INPUT                                                                                    #
                //  #############################################################################################################################
                //  If there is a search, build a list of all the values (mostly POST) and pass it on. We only do this if there is a dcm_search POST value.
                var searchNameValueList = new List<URLNameValuePair>();
                if (Request.Form.HasKeys())
                {
                    if (Request.Form["dcm_search"]?.Length > 0)
                    {
                        blUseSearchUrl = true;
                        var intTotal = Request.Form.Count;
                        if (intTotal > 0)
                        {
                            for (var i = 0; i <= intTotal - 1; i++)
                            {
                                //  Response.Write(Request.Form.Keys(i).ToString & " - " & Request.Form(i) & "<br>")
                                var vName = Request.Form.Keys[i];
                                var vValue = Request.Form[i];
                                searchNameValueList.Add(new URLNameValuePair(vName, vValue));
                            }
                        }
                    }
                }

                strReferer = Request.ServerVariables["HTTP_REFERER"] == null ? "" : Request.ServerVariables["HTTP_REFERER"];

                //  #############################################################################################################################
                //  # LOG REQUEST, GET CATEGORIES AND CALL URLPARSER WITH PARAMETERS                                                            #
                //  #############################################################################################################################
                //  If there is a search defined (using dcm_search), we use the URL from the action to replace some values.
                if (blUseSearchUrl)
                {
                    var holdLogger = new LoggingInfo(
                        Request.ServerVariables["REMOTE_ADDR"],
                        Request.ServerVariables["HTTP_USER_AGENT"],
                        strReferer, Request.ServerVariables["REMOTE_HOST"],
                        Request.ServerVariables["REQUEST_METHOD"],
                        Request.Form["dcm_search"],
                        GetContentIDFromURL(Request.Form["dcm_search"]),
                        Request_URL.AbsolutePath,
                        Request_URL.AbsoluteUri,
                        Request_URL.OriginalString,
                        Request_URL.Query,
                        Request.RawUrl);
                    //  Break the category parameters into an object if they exist, it should be run on every incoming URL, and will return nothing if no 
                    //  parameters are specified. The URL parser properly handles a null in the category parameters so no need to check for anything. 
                    var inUrl = "http://" + Request_URL.Host + Request_URL.AbsolutePath + "?" + strRawURL;
                    var catLoadParams = CategoryLoaderParams.GetNewCategoryLoaderParamsFromURL(inUrl, connectionString, service.GetConsumerIdFromBaseDomain(GetBaseDomainOutOfURL(inUrl)));
                    CategoryItem cleanTree = null;
                    var flatListOfAllCleanCategories = new List<CategoryItem>();
                    string rootCategoryId = null;
                    var supplierTrees = new List<SupplierCatTree>();

                    var supplierIds = new List<string>();

                    if (catLoadParams != null)
                    {
                        if (catLoadParams.SupplierIDs == null)
                        {
                            supplierIds = service.FillEmptySupplierList(catLoadParams.ConsumerID).ToList();
                        }
                        service.GetCleanCategoryTree(catLoadParams.RootCategory, catLoadParams.Depth, ref rootCategoryId, flatListOfAllCleanCategories, cleanTree);
                        service.GetListOfSupplierBasedCategoryTrees(cleanTree, catLoadParams.ConsumerID, supplierIds, supplierTrees, rootCategoryId);
                    }
                    var contextCache = Context.Cache;
                    Hold = URLEngine.GetOrCreateURLEngineFromCache(
                        Request.Form["dcm_search"].Replace("{Udesc}", Request.Form["desc"]),
                        connectionString,
                        Request_URL.Host,
                        ref searchNameValueList,
                        ref holdLogger,
                        true,
                        catLoadParams != null,
                        catLoadParams,
                        ref contextCache,
                        Session["language_code"].ToString(),
                        rootCategoryId, flatListOfAllCleanCategories, cleanTree, supplierTrees);

                    //  Since the user can define the pageID for the search results, we need to set intPageID to process the correct page. If we do not do this, 
                    //  the URLParser will always use the base_publications pageID for the search results.
                    if (searchNameValueList[0].Name.Equals("dcm_search"))
                    {
                        if (searchNameValueList[0].Value.StartsWith("p"))
                        {
                            var strTempPageId = searchNameValueList[0].Value.Remove(0, 1);
                            strTempPageId = strTempPageId.Substring(0, strTempPageId.IndexOf("("));
                            intPageID = int.Parse(strTempPageId);
                            strRawURL = searchNameValueList[0].Value;
                        }
                    }
                }
                else
                {
                    //  No search defined
                    var userAgent = string.Empty;
                    if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_USER_AGENT"]))
                    {
                        userAgent = Request.ServerVariables["HTTP_USER_AGENT"];
                    }

                    if (!string.IsNullOrEmpty(strWorkURL))
                    {
                        //  If there are no parameters, use the pageID to pass on to the URLParser
                        //  Break the category parameters into an object if they exist, it should be run on every incoming URL, and will return nothing if no 
                        //  parameters are specified. The URL parser properly handles a null in the category parameters so no need to check for anything. 
                        var inUrl = "http://" + (Request_URL.Host + Request_URL.AbsolutePath) + "?" + strRawURL;
                        var catLoadParams = CategoryLoaderParams.GetNewCategoryLoaderParamsFromURL(inUrl, service.GetConsumerIdFromBaseDomain(GetBaseDomainOutOfURL(inUrl)), connectionString);

                        CategoryItem cleanTree = null;
                        var flatListOfAllCleanCategories = new List<CategoryItem>();
                        string rootCategoryId = null;
                        var supplierTrees = new List<SupplierCatTree>();

                        var supplierIds = new List<string>();
                        if (catLoadParams != null)
                        {
                            if (catLoadParams.SupplierIDs == null)
                            {
                                supplierIds = service.FillEmptySupplierList(catLoadParams.ConsumerID).ToList();
                            }
                            service.GetCleanCategoryTree(catLoadParams.RootCategory, catLoadParams.Depth, ref rootCategoryId, flatListOfAllCleanCategories, cleanTree);
                            service.GetListOfSupplierBasedCategoryTrees(cleanTree, catLoadParams.ConsumerID, supplierIds, supplierTrees, rootCategoryId);
                        }

                        var holdLogger = new LoggingInfo(
                            Request.ServerVariables["REMOTE_ADDR"],
                            userAgent,
                            strReferer,
                            Request.ServerVariables["REMOTE_HOST"],
                            Request.ServerVariables["REQUEST_METHOD"],
                            strRawURL.Replace(" ", "%20"),
                            GetContentIDFromURL(strWorkURL),
                            Request_URL.AbsolutePath,
                            Request_URL.AbsoluteUri,
                            Request_URL.OriginalString,
                            Request_URL.Query,
                            Request.RawUrl);
                        var contextCache = Context.Cache;
                        Hold = URLEngine.GetOrCreateURLEngineFromCache(
                            strWorkURL,
                            connectionString,
                            Request_URL.Host,
                            ref searchNameValueList,
                            ref holdLogger,
                            true,
                            catLoadParams != null,
                            catLoadParams,
                            ref contextCache,
                            Session["language_code"].ToString(),
                            rootCategoryId, flatListOfAllCleanCategories, cleanTree, supplierTrees);
                    }
                    else
                    {
                        //  Break the category parameters into an object if they exist, it should be run on every incoming URL, and will return nothing if no 
                        //  parameters are specified. The URL parser properly handles a null in the category parameters so no need to check for anything. 
                        // Dim CatLoadParams As CategoryLoaderParams = CategoryLoaderParams.GetNewCategoryLoaderParamsFromURL(Request_URL.Scheme & Request_URL.SchemeDelimiter & Request_URL.Host & Request_URL.AbsolutePath & "?" & strRawURL, con, connectionString)
                        var inUrl = "http://" + Request_URL.Host + Request_URL.AbsolutePath + "?" + strRawURL;
                        var catLoadParams = CategoryLoaderParams.GetNewCategoryLoaderParamsFromURL(inUrl, service.GetConsumerIdFromBaseDomain(GetBaseDomainOutOfURL(inUrl)), connectionString);
                        CategoryItem cleanTree = null;
                        var flatListOfAllCleanCategories = new List<CategoryItem>();
                        string rootCategoryId = null;
                        var supplierTrees = new List<SupplierCatTree>();

                        var supplierIds = new List<string>();

                        if (catLoadParams != null)
                        {
                            if (catLoadParams.SupplierIDs == null)
                            {
                                supplierIds = service.FillEmptySupplierList(catLoadParams.ConsumerID).ToList();
                            }
                            service.GetCleanCategoryTree(catLoadParams.RootCategory, catLoadParams.Depth, ref rootCategoryId, flatListOfAllCleanCategories, cleanTree);
                            service.GetListOfSupplierBasedCategoryTrees(cleanTree, catLoadParams.ConsumerID, supplierIds, supplierTrees, rootCategoryId);
                        }
                        var holdLogger = new LoggingInfo(
                            Request.ServerVariables["REMOTE_ADDR"],
                            userAgent,
                            strReferer,
                            Request.ServerVariables["REMOTE_HOST"],
                            Request.ServerVariables["REQUEST_METHOD"],
                            "p" + (intPageID + "()"),
                            Guid.Empty,
                            Request_URL.AbsolutePath,
                            Request_URL.AbsoluteUri,
                            Request_URL.OriginalString,
                            Request_URL.Query,
                            Request.RawUrl);
                        var contextCache = Context.Cache;
                        Hold = URLEngine.GetOrCreateURLEngineFromCache(
                            "p" + (intPageID + "()"),
                            connectionString,
                            Request_URL.Host,
                            ref searchNameValueList,
                           ref holdLogger,
                            true,
                            catLoadParams != null,
                            catLoadParams,
                            ref contextCache,
                            Session["language_code"].ToString(),
                            rootCategoryId, flatListOfAllCleanCategories, cleanTree, supplierTrees);
                    }

                }

                //  #############################################################################################################################
                //  # FOR DEVELOPMENT ONLY: SHOW WHICH PARAMETERS ARE BEING USED                                                                #
                //  #############################################################################################################################
                // If blUseSearchUrl = True Then
                //     Response.Write("Working URL: " & Request.Form("dcm_search").ToString & " | pageID: " & intPageID & "<br><br>")
                //     For Each Item As URLNameValuePair In SearchNameValueList
                //         Response.Write("Name: " & Item.Name.ToString & " - Value: " & Item.Value.ToString & "<br>")
                //     Next
                //     Response.Write("<br>")
                // Else
                //     If strWorkURL <> "" Then
                //         Response.Write("Working URL: " & strWorkURL & " | pageID: " & intPageID & "<br><br>")
                //     Else
                //         Response.Write("Working URL: " & "p" & intPageID & "()" & " | pageID: " & intPageID & "<br><br>")
                //     End If
                // End If
                //  #############################################################################################################################
                //  # GET THE CONTENT FOR THIS PAGE, TOGETHER WITH THE COMPONENTS THAT BELONG TO THIS PUBLICATION                               #
                //  # ALl GENERAL, POST AND GET VALUES ARE REPLACED HERE AS WELL                                                                #
                //  #############################################################################################################################
                service.GetContents(intPageID, intPublicationID, ref strPageContent, ref intPublicationType, RelatedPubHTML, out var strErrorMsg, proxyModel, context);
                if (!string.IsNullOrEmpty(strErrorMsg))
                {
                    WriteError(strErrorMsg);
                }

                //  #############################################################################################################################
                //  # GET THE JAVASCRIPT FOR THIS PAGE, IF ANY                                                                                  #
                //  #############################################################################################################################
                if (strPageContent.Contains("{javascript:"))
                {
                    service.GetJavascripts(intPageID, ref strPageContent, out var errorMsg, proxyModel, context);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        WriteError(errorMsg);
                    }
                }

                //  #############################################################################################################################
                //  # CONSUMER SUBSCRIPTION: GET SUPPLIERS AND CATEGORIES FOR SELECTED THEME                                                    #
                //  #############################################################################################################################
                if (strPageContent.Contains("{consumer_subscription:"))
                {
                    //  To add a consumer to DCM online, we need a theme to subscribe the consumer to. This theme can be set bij using the following code:
                    //  {consumer_subscription:1:theme_name}. The second position (1) holds the theme ID. We now need to get all suppliers and
                    //  categories that belong to this theme. A consumer can then make a selection and submit his profile. To show the suppliers and categories,
                    //  the following codes can be used: {consumer_subscription:suppliers} and {consumer_subscription:categories}. A DCM user can place these codes
                    //  anywhere on the form where he wants the suppliers and categories to show up (using DIV's etc). A table with checkboxes will be placed so the 
                    //  consumer can make his selection.
                    var strLineToRemove = "";
                    string intThemeID = null;
                    Regex r5;
                    Match m5;
                    r5 = new Regex("{consumer_subscription:(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    m5 = r5.Match(strPageContent);
                    while (m5.Success)
                    {
                        strLineToRemove = m5.Groups[0].Value;
                        //  Holds the entire code, which we later on delete from the code.
                        intThemeID = m5.Groups[1].Value;
                        //  Set themeID
                        m5 = m5.NextMatch();
                    }
                    strPageContent = strPageContent.Replace(strLineToRemove, "");

                    //  Now get the suppliers and categories for this theme. We create a table with all suppliers an categories, with checkboxes.
                    service.GetSuppliers(intThemeID, ref strPageContent, out var errorMsg);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        WriteError(errorMsg);
                    }

                    service.GetCategories(intThemeID, ref strPageContent);
                }

                //  To show HTML code (needed for the implementation code for resellers) as HTML we need to change some characters in order to prevent the browser to render the code. 
                //  These codes can be placed inside the <IMPLEMENT_CODE1></IMPLEMENT_CODE1> and <IMPLEMENT_CODE2></IMPLEMENT_CODE2> tags.
                if (strPageContent.Contains("<IMPLEMENT_CODE1>"))
                {
                    var strOldResellerCode1 = strPageContent.Substring(strPageContent.IndexOf("<IMPLEMENT_CODE1>") + 17, strPageContent.IndexOf("</IMPLEMENT_CODE1>") - (strPageContent.IndexOf("<IMPLEMENT_CODE1>") - 45));
                    var strNewResellerCode1 = strOldResellerCode1.Replace("<", "<").Replace(">", ">").TrimEnd().TrimStart();
                    strPageContent = strPageContent.Replace(strOldResellerCode1, strNewResellerCode1);
                }

                if (strPageContent.Contains("<IMPLEMENT_CODE2>"))
                {
                    var strOldResellerCode2 = strPageContent.Substring(strPageContent.IndexOf("<IMPLEMENT_CODE2>") + 17, strPageContent.IndexOf("</IMPLEMENT_CODE2>") - (strPageContent.IndexOf("<IMPLEMENT_CODE2>") - 45));
                    var strNewResellerCode2 = strOldResellerCode2.Replace("<", "<").Replace(">", ">").TrimEnd().TrimStart();
                    strPageContent = strPageContent.Replace(strOldResellerCode2, strNewResellerCode2);
                }

                //  #############################################################################################################################
                //  # GET THE HEADER AND STYLESHEET, BUT ONLY IF intPublicationType = 0 (ONLY FOR A PAGE)                                       #
                //  #############################################################################################################################
                if (intPublicationType == 0)
                {
                    var strPageTitle = "";
                    var strMetaDescription = "";
                    var strMetaKeywords = "";
                    var strInlineStylesheet = "";
                    var strExternalStylesheet = "";
                    var strExternalJavaScript = "";
                    var strOnloadScript = "";
                    var strBodyTagExtension = "";
                    var strBackGroundColor = "";
                    var strBackgroundImage = "";
                    var strInlineJavaScript = "";
                    var strCssCode = "";

                    service.GetStyleSheet(
                        intPageID,
                        ref strPageTitle,
                        ref strMetaDescription,
                        ref strMetaKeywords,
                        ref strInlineStylesheet,
                        ref strExternalStylesheet,
                        ref strExternalJavaScript,
                        ref strBackGroundColor,
                        ref strBackgroundImage,
                        ref strOnloadScript,
                        ref strBodyTagExtension,
                        ref strInlineJavaScript,
                        ref strCssCode);

                    //  Build up header for this page
                    var strHeading = "<head>";
                    if (strPageTitle.Length > 0)
                    {
                        strHeading = strHeading + Environment.NewLine + "<title>" + strPageTitle + "</title>";
                    }

                    if (strMetaKeywords.Length > 0)
                    {
                        strHeading = strHeading + Environment.NewLine + "<meta name=\"keywords\" content=\"" + strMetaKeywords + "\" />";
                    }

                    if (strMetaDescription.Length > 0)
                    {
                        strHeading = strHeading + Environment.NewLine + "<meta name=\"description\" content=\"" + strMetaDescription + "\" />";
                    }

                    if (strExternalJavaScript != "")
                    {
                        if (strExternalJavaScript.StartsWith("<script"))
                        {
                            strHeading = strHeading + Environment.NewLine + strExternalJavaScript;
                        }
                        else
                        {
                            strHeading = strHeading + Environment.NewLine + "<script type=\"text/javascript\" src=" + (strExternalJavaScript + "></script>");
                        }

                    }

                    if (strInlineJavaScript != "")
                    {
                        if (strInlineJavaScript.StartsWith("<script"))
                        {
                            strHeading = strHeading + Environment.NewLine + strInlineJavaScript;
                        }
                        else
                        {
                            strHeading = strHeading + Environment.NewLine + "<script language=\"JavaScript\">" + Environment.NewLine + strInlineJavaScript + Environment.NewLine + "</script>";
                        }

                    }

                    if (strCssCode.Length > 0)
                    {
                        if (strCssCode.StartsWith("<style"))
                        {
                            strHeading = strHeading + Environment.NewLine + Environment.NewLine + strCssCode;
                        }
                        else
                        {
                            strHeading = strHeading + Environment.NewLine + "<style type=\"text/css\">" + Environment.NewLine + strCssCode + "</style>";
                        }

                    }

                    if ((strInlineStylesheet != ""))
                    {
                        if (strInlineStylesheet.StartsWith("<link"))
                        {
                            strHeading = (strHeading
                                        + (Environment.NewLine + strInlineStylesheet));
                        }
                        else
                        {
                            strHeading = (strHeading
                                        + (Environment.NewLine + ("<link href=\""
                                        + (strInlineStylesheet + "\" rel=\"stylesheet\" type=\"text/css\">"))));
                        }

                    }

                    if ((strExternalStylesheet != ""))
                    {
                        if (strExternalStylesheet.StartsWith("<link"))
                        {
                            strHeading = (strHeading
                                        + (Environment.NewLine + strExternalStylesheet));
                        }
                        else
                        {
                            strHeading = (strHeading
                                        + (Environment.NewLine + ("<link href=\""
                                        + (strExternalStylesheet + "\" rel=\"stylesheet\" type=\"text/css\">"))));
                        }

                    }

                    //  Process heading for this page
                    strPageContent = strPageContent.Replace("<head>", strHeading);
                    //  Build up the body tags
                    string strBodyTag = "<body";
                    if ((strBackGroundColor != ""))
                    {
                        strBodyTag = (strBodyTag + (" bgcolor=\""
                                    + (strBackGroundColor + "\"")));
                    }

                    if ((strBackgroundImage != ""))
                    {
                        strBodyTag = (strBodyTag + (" background=\""
                                    + (strBackgroundImage + "\"")));
                    }

                    if ((strOnloadScript != ""))
                    {
                        strBodyTag = (strBodyTag + (" onload=\""
                                    + (strOnloadScript + "\"")));
                    }

                    if ((strBodyTagExtension != ""))
                    {
                        strBodyTag = (strBodyTag + (" "
                                    + (strBodyTagExtension + "")));
                    }

                    //  Process body tag for this page
                    strPageContent = strPageContent.Replace("<body>", (strBodyTag + ">"));
                }

                //  #############################################################################################################################
                //  # PROCESS PAGE AND COMPONENTS FOR PUBLICATION AND HANDLE PAGING                                                             #
                //  #############################################################################################################################
                bool ranalready = false;
                foreach (Tuple<string, Task<ContentCollection>> Item in Hold.MyContentCollectionTasks)
                {
                    // For Each Item As ContentCollection In Hold.MyContentCollections
                    // If Item.fPublication = "p" & intPageID Then
                    if (ranalready)
                    {
                        break;
                    }

                    if (Item.Item1.Equals("p" + intPageID))
                    {
                        ranalready = true;
                        string strRawWorkURL;
                        var vRelatedContentCollection = Item;
                        var HTML = new DCMPublication(strPageContent, ref vRelatedContentCollection, Hold.MyContentCollectionTasks, RelatedPubHTML);
                        if (blUseSearchUrl)
                        {
                            //  If there is a search, replace the searchvalues in the URL, so paging will have the correct URL.
                            strRawWorkURL = strRawURL.Replace("{Udesc}", Request.Form["desc"].ToString());
                            strRawWorkURL = strRawWorkURL.Replace("#", "!");
                        }
                        else
                        {
                            strRawWorkURL = strRawURL;
                        }

                        HTML.SourceURL = "http://" + Request_URL.Host + ":" + Request.Url.Port + "/d1.aspx?" + strRawWorkURL;
                        //  Used for paging and to replace {PageRawURL}
                        if (proxyModel.OriginalRequestedUrl != null)
                        {
                            HTML.OriginalSourceDomain = proxyModel.OriginalRequestedUrl.Scheme + proxyModel.OriginalRequestedUrl.Scheme + proxyModel.OriginalRequestedUrl.Host;
                            if (proxyModel.OriginalRequestedUrl.Port != 80)
                            {
                                HTML.OriginalSourceDomain = HTML.OriginalSourceDomain + ":" + proxyModel.OriginalRequestedUrl.Port;
                            }

                            HTML.OriginalSourceDomain = HTML.OriginalSourceDomain + "/" + proxyModel.OriginalRequestedUrl.Query;
                        }

                        // HTML.SourceURL = "http://" & Request_URL.Host & "/d1.aspx?" & strRawWorkURL ' Used for paging and to replace {PageRawURL}
                        // Dim timerStart As DateTime = DateTime.Now
                        // Dim timerEnd As TimeSpan
                        // Function()
                        //          Return fRelatedContent.Item2.Result.GetCurPageAsNewContentCollection()
                        // End Sub

                        string HtmlResult = "\\\\/";
                        var holdtask = Task.Factory.StartNew(() => HTML.GetHTMLResult());
                        if (!holdtask.Wait(new TimeSpan(0, 0, GlobalConfiguration.ProxyTimeOut)))
                        {
                            HtmlResult = "\\\\/";
                            Task.Factory.StartNew(() =>
                                {
                                    Thread.Sleep(new TimeSpan(0, 0, 3));
                                    Process.GetCurrentProcess().Kill();
                                    return "";
                                });
                            Response.Redirect("/d1.aspx");
                            // HttpRuntime.UnloadAppDomain()
                            // Throw New Exception("PageTimeout")
                        }
                        else
                        {
                            HtmlResult = holdtask.Result;
                        }

                        // HtmlResult = holdtask.Result
                        // Dim strContent As String = replaceReplacementCodesAfter(HTML.GetHTMLResult())
                        string strContent = ReplaceReplacementCodesAfter(HtmlResult);
                        // timerEnd = DateTime.Now.Subtract(timerStart)
                        // strContent = strContent + String.Format("<div>strContent: {0} mm, {1} ss, {2} ms</div>", timerEnd.Minutes, timerEnd.Seconds, timerEnd.Milliseconds)
                        //  #############################################################################################################################
                        //  # PROCESS AND SEND EMAIL, IF ANY                                                                                            #
                        //  #############################################################################################################################
                        // Dim UserAgent As String = ""
                        // If Request.ServerVariables.Item("HTTP_USER_AGENT") Is Nothing Then
                        //     UserAgent = "Nothing"
                        // Else
                        //     UserAgent = Request.ServerVariables.Item("HTTP_USER_AGENT")
                        // End If
                        // If strContent.Contains("{email:") And Not (UserAgent.ToLower().Contains("bot")) Then
                        if (strContent.Contains("{email:"))
                        {
                            Regex r;
                            Match m;
                            //  Get the required emailfields. All fields are seperated by a :. So we need => {email: and then all fields between : and : until we reach the }.
                            //  The syntax is: {email:pageID:Recipient:Cc:Bcc:Subject:SenderEmailaddress:SenderName:Attachment}.
                            r = new Regex("{email:(.*?):(.*?):(.*?):(.*?):(.*?):(.*?):(.*?):(.*?)}", (RegexOptions.Compiled | RegexOptions.IgnoreCase));
                            m = r.Match(strContent);
                            while (m.Success)
                            {
                                //  Response.Write("Voor het versturen: 470<br><br>")
                                //  Exit Sub
                                SendEmail(Convert.ToInt32(m.Groups[1].Value), m.Groups[2].Value, m.Groups[3].Value, m.Groups[4].Value, m.Groups[5].Value, m.Groups[6].Value, m.Groups[7].Value, m.Groups[8].Value);
                                //  Response.Write("Na versturen: 472")
                                var strRemove = "{email:" + m.Groups[1].Value + ":" + m.Groups[2].Value + ":" + m.Groups[3].Value + ":" + m.Groups[4].Value + ":" + m.Groups[5].Value + ":" + m.Groups[6].Value + ":" + m.Groups[7].Value + ":" + m.Groups[8].Value + "}";
                                strContent = strContent.Replace(strRemove, "<b>Emailed " + m.Groups[2].Value + "</b>");
                                m = m.NextMatch();
                            }
                        }

                        //  Done with email handling
                        try
                        {
                            if (GlobalConfiguration.LogType != "NOTEST")
                            {
                                if (Request_URL.Host == GlobalConfiguration.LogDomain)
                                {
                                    var holdLogger = new HTMLRecord(connectionString, HTML.SourceURL, GlobalConfiguration.LogType, strContent);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }

                        // process replacement code for proxy variables
                        if (strContent.Contains("{get-proxy-project}"))
                        {
                            strContent = strContent.Replace("{get-proxy-project}", Context.Items["PROJECT_NAME"].ToString());
                        }

                        if (IsInlineRequest)
                        {
                            contentType = "application/javascript";
                            responseContent = InlineContent.CreateJavascript(strContent, connectionString, proxyModel.ProjectName, proxyModel.InlineRequestUri, proxyModel.ProxyQuery);
                        }
                        else if ((intPublicationType == 10))
                        {
                            SendEmail(
                                GetQueryParam("to"),
                                GetQueryParam("from"),
                                GetQueryParam("cc"),
                                GetQueryParam("bcc"),
                                GetQueryParam("subject"),
                                ReplaceReplacementCodes(strContent));
                        }
                        else
                        {
                            responseContent = strContent;
                        }

                        // d1 cache insert
                        if (daysToFetchCache > 0 && isNoCache == false)
                        {
                            service.InsertToCache(Request_URL, strContent, daysToFetchCache, lngId);
                        }
                    }
                }
            }
            else
            {
                //  No URL found at all...
                WriteError("Error: no URL (ErrorCode 65).");
            }

            return new ResponseResult { Type = contentType, Content = responseContent };
        }

        public static string ReplaceCharacters(string strContentToRender)
        {
            strContentToRender = strContentToRender.Replace("%", "~");
            strContentToRender = strContentToRender.Replace("#", "!");
            strContentToRender = strContentToRender.Replace("~", "%");
            strContentToRender = strContentToRender.Replace("!", "#");
            strContentToRender = strContentToRender.Replace("*LB*", "<");
            strContentToRender = strContentToRender.Replace("*RB*", ">");
            strContentToRender = strContentToRender.Replace("*LB*", "<");
            strContentToRender = strContentToRender.Replace("*RB*", ">");
            strContentToRender = strContentToRender.Replace("*Q*", "?");
            strContentToRender = strContentToRender.Replace("*Q*", "?");
            strContentToRender = strContentToRender.Replace("*FS*", "/");
            strContentToRender = strContentToRender.Replace("*FS*", "/");
            strContentToRender = strContentToRender.Replace("*BS*", "\\");
            strContentToRender = strContentToRender.Replace("*BS*", "\\");
            strContentToRender = strContentToRender.Replace("*AMP*", "&");
            strContentToRender = strContentToRender.Replace("*AMP*", "&");
            //  URL's from mail need the following fix
            strContentToRender = strContentToRender.Replace("%5b", "[");
            strContentToRender = strContentToRender.Replace("%5d", "]");
            //  04/13
            strContentToRender = strContentToRender.Replace("=%", "EQPERCENT123");
            strContentToRender = strContentToRender.Replace("%28", "(").Replace("%29", ")").Replace("*C*", ":");
            strContentToRender = strContentToRender.Replace("EQPERCENT123", "=%");
            // strContentToRender = strContentToRender.Replace("%28", "(")
            // strContentToRender = strContentToRender.Replace("%29", ")")
            // strContentToRender = strContentToRender.Replace("*C*", ":")
            return strContentToRender;
        }

        static void WriteError(string msg)
        {
            Response.Write(msg);
            Response.Write("<!-- " + Request_URL.OriginalString + " -->");
        }

        static Guid GetContentIDFromURL(string strURL)
        {
            if (strURL.Contains("cn"))
            {
                //  Het the contentID and return it
                string tempStr = strURL.Remove(0, (strURL.IndexOf("cn") + 2));
                string newStr = tempStr.Substring(0, 36);
                // 8/8/12 ltu  wrap a try/catch to prevent issues with CN misuses.
                Guid newCID;
                try
                {
                    newCID = new Guid(newStr);
                }
                catch (Exception)
                {
                    newCID = Guid.Empty;
                }

                return newCID;
            }

            //  No contentID specified in the URL
            return Guid.Empty;
        }

        static string LoadPubFromFile(string pubContent)
        {
            // assumes PubContent contains {LoadFromFile:
            string ErrorConcat = "";
            try
            {
                string rootdir;
                try
                {
                    rootdir = GlobalConfiguration.RootPubDir;
                }
                catch (Exception)
                {
                    rootdir = "H:\\ui\\UniversalResources\\Publications";
                    ErrorConcat = "<!-- web.config does not contain RootPubDir key, assuming h:\\\\ui\\\\pubs -->";
                }

                if (!rootdir.EndsWith("\\"))
                {
                    rootdir = rootdir + "\\";
                }

                string Landmark = "{LoadFromFile:";
                int pos = pubContent.IndexOf(Landmark);
                string filePath = pubContent.Substring((pos + Landmark.Length));
                //  read from : to end of line
                filePath = filePath.Substring(0, filePath.IndexOf("}"));
                filePath = (rootdir + filePath);
                pubContent = File.ReadAllText(filePath);
                return (pubContent + ErrorConcat);
            }
            catch (Exception ex)
            {
                return (ex.Message.Replace("\'", "").Replace("\\", "\\\\") + ErrorConcat);
            }
        }

        public static string ReplaceReplacementCodes(string strPageContent)
        {
            if (strPageContent.Contains("{LoadFromFile:"))
            {
                strPageContent = LoadPubFromFile(strPageContent);
            }

            if (strPageContent.Contains("{general:date:long}"))
            {
                strPageContent = strPageContent.Replace("{general:date:long}", DateTime.Now.ToString());
            }

            if (strPageContent.Contains("{general:date:short}"))
            {
                strPageContent = strPageContent.Replace("{general:date:full}", DateTime.Now.ToShortDateString());
            }

            if (strPageContent.Contains("{general:base_domain}"))
            {
                //  Domain only, e.g. sub.domain.com.
                strPageContent = strPageContent.Replace("{general:base_domain}", Request_URL.Host);
            }

            if (strPageContent.Contains("{general:domain_abs_uri}"))
            {
                //  The URL as requested, including parameters.
                strPageContent = strPageContent.Replace("{general:domain_abs_uri}", Request_URL.AbsoluteUri);
            }

            if (strPageContent.Contains("{general:base_parameters:"))
            {
                //  Get parameters from the base_paramaters. This is different from getting parameters from the URL.
                Regex r3;
                Match m3;
                r3 = new Regex("{general:base_parameters:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m3 = r3.Match(strPageContent);
                if (strWorkURL.Contains("&"))
                {
                    var strParametersFromWorkURL = strWorkURL.Split('&');
                    while (m3.Success)
                    {
                        foreach (var strGetString in strParametersFromWorkURL)
                        {
                            if (strGetString.StartsWith(m3.Groups[1].Value))
                            {
                                strPageContent = strPageContent.Replace("{general:base_parameters:" + m3.Groups[1].Value + "}", strGetString.Substring(strGetString.IndexOf("=") + 1, strGetString.Length - (strGetString.IndexOf("=") - 1)));
                                break;
                            }

                        }

                        m3 = m3.NextMatch();
                    }
                }
            }

            if (strPageContent.Contains("{get:"))
            {
                Regex r33;
                Match m33;
                r33 = new Regex("{get:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m33 = r33.Match(strPageContent);
                while (m33.Success)
                {
                    strPageContent = strPageContent.Replace("{get:" + m33.Groups[1].Value + "}", Request.QueryString[m33.Groups[1].Value]);
                    m33 = m33.NextMatch();
                }
            }

            if (strPageContent.Contains("{post:"))
            {
                Regex r34;
                Match m34;
                r34 = new Regex("{post:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m34 = r34.Match(strPageContent);
                while (m34.Success)
                {
                    strPageContent = strPageContent.Replace("{post:" + m34.Groups[1].Value + "}", Request.Form[m34.Groups[1].Value]);
                    m34 = m34.NextMatch();
                }
            }

            if (strPageContent.Contains("{encode_post:"))
            {
                Regex r34;
                Match m34;
                r34 = new Regex("{encode_post:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m34 = r34.Match(strPageContent);
                while (m34.Success)
                {
                    strPageContent = strPageContent.Replace("{encode_post:" + m34.Groups[1].Value + "}", HttpUtility.HtmlEncode(Request.Form[m34.Groups[1].Value]));
                    m34 = m34.NextMatch();
                }
            }

            if (strPageContent.Contains("{session:get:"))
            {
                Regex r3;
                Match m3;
                r3 = new Regex("{session:get:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                while (m3.Success)
                {
                    strPageContent = strPageContent.Replace("{session:get:" + m3.Groups[1].Value + "}", Session[m3.Groups[1].Value].ToString());
                    m3 = m3.NextMatch();
                }
            }

            //  Set Session.
            if (strPageContent.Contains("{session:set:"))
            {
                // Dim r37 As System.Text.RegularExpressions.Regex
                // Dim m37 As System.Text.RegularExpressions.Match
                // r37 = New System.Text.RegularExpressions.Regex("{session:set:(.*?):(.*?)}", RegexOptions.Compiled Or RegexOptions.Multiline Or RegexOptions.IgnoreCase)
                // m37 = r37.Match(strPageContent)
                // Dim strSessionValueToUse As String = ""
                // While m37.Success
                // If m37.Groups(2).Value.ToString.Contains("{") Then
                // strSessionValueToUse = m37.Groups(2).Value.ToString & "}"
                // End If
                // Session(m37.Groups(1).Value.ToString) = m37.Groups(2).Value.ToString
                // strPageContent = strPageContent.Replace("{session:set:" & m37.Groups(1).Value & ":" & m37.Groups(2).Value.ToString & "}", "")
                // m37 = m37.NextMatch()
                // End While
                Regex r37;
                Match m37;
                r37 = new Regex("{session:set:(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
                m37 = r37.Match(strPageContent);
                while (m37.Success)
                {
                    string strSessionValueToUse;
                    if (m37.Groups[2].Value.Contains("{"))
                    {
                        strSessionValueToUse = m37.Groups[2].Value + "}";
                    }
                    else
                    {
                        strSessionValueToUse = m37.Groups[2].Value;
                    }

                    Session[m37.Groups[1].Value] = strSessionValueToUse;
                    strPageContent = strPageContent.Replace("{session:set:" + m37.Groups[1].Value + (":"
                                                                                                     + (strSessionValueToUse + "}")), "");
                    m37 = m37.NextMatch();
                }
            }

            //  Delete all sessions
            if (strPageContent.Contains("{session:delete:all}"))
            {
                if ((Session.Count > 0))
                {
                    Session.RemoveAll();
                }

                strPageContent = strPageContent.Replace("{session:delete:all}", "");
            }

            //  Delete a specific Session.
            if (strPageContent.Contains("{session:delete:"))
            {
                Regex r32;
                Match m32;
                r32 = new Regex("{session:delete:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m32 = r32.Match(strPageContent);
                while (m32.Success)
                {
                    Session.Remove(m32.Groups[1].Value);
                    strPageContent = strPageContent.Replace("{session:delete:" + m32.Groups[1].Value + "}", "");
                    m32 = m32.NextMatch();
                }
            }

            if (strPageContent.Contains("{general:replace:"))
            {
                Regex r3;
                Match m3;
                r3 = new Regex("{general:replace:(.*?):(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                while (m3.Success)
                {
                    var strfinalContent = m3.Groups[1].Value.Replace(m3.Groups[2].Value, m3.Groups[3].Value);
                    strPageContent = strPageContent.Replace("{general:replace:" + m3.Groups[1].Value + ":" + m3.Groups[2].Value + ":" + m3.Groups[3].Value + "}", strfinalContent);
                    m3 = m3.NextMatch();
                }
            }

            return strPageContent;
        }

        static string ReplaceReplacementCodesAfter(string strPageContent)
        {
            if (strPageContent.Contains("{general:replace_after:"))
            {
                Regex r3;
                Match m3;
                string strfinalContent;
                r3 = new Regex("{general:replace_after:(.*?):(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                while (m3.Success)
                {
                    strfinalContent = m3.Groups[1].Value.Replace(m3.Groups[2].Value, m3.Groups[3].Value);
                    strPageContent = strPageContent.Replace("{general:replace_after:" + m3.Groups[1].Value + ":" + m3.Groups[2].Value + ":" + m3.Groups[3].Value + "}", strfinalContent);
                    m3 = m3.NextMatch();
                }
            }

            if (strPageContent.Contains("{general:redirect:"))
            {
                Regex r3;
                Match m3;
                r3 = new Regex("{general:redirect:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                if (m3.Success)
                {
                    Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", ("http://" + m3.Groups[1].Value));
                    Response.End();
                }
            }

            return strPageContent;
        }

        private static void SendEmail(string emailTo, string emailFrom, string cc, string bcc, string subject, string content)
        {
            // Dim oMail As SmtpMail = New SmtpMail("ES-AA1141023508-00676-ED46E93E2301C48B9A22F046B0A0F3D1")
            var mail = new MailMessage();
            try
            {
                if ((string.IsNullOrEmpty(emailTo) || string.IsNullOrEmpty(emailFrom)))
                {
                    return;
                }

                foreach (var toAddr in emailTo.Split(','))
                {
                    mail.To.Add(new MailAddress(toAddr));
                }

                mail.From = new MailAddress(emailFrom);
                if (!string.IsNullOrEmpty(cc))
                {
                    foreach (var ccAddr in cc.Split(','))
                    {
                        mail.CC.Add(new MailAddress(ccAddr));
                    }
                }

                mail.Subject = subject;
                mail.Body = content;
                mail.IsBodyHtml = true;
                mail.BodyEncoding = Encoding.UTF8;

                using (var smtpClient = new SmtpClient(GlobalConfiguration.SmtpServer))
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;

                    //  Get SMTP settings.
                    if (!string.IsNullOrEmpty(GlobalConfiguration.SmtpUserName) && !string.IsNullOrEmpty(GlobalConfiguration.SmtpPassword))
                    {
                        var user = CspDecoder.Decode(GlobalConfiguration.SmtpUserName);
                        var pwd = CspDecoder.Decode(GlobalConfiguration.SmtpPassword);
                        smtpClient.Credentials = new NetworkCredential(user, pwd);
                    }

                    smtpClient.Send(mail);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        static void SendEmail(int intPageID, string strRecipient, string strCC, string strBCC, string strSubject, string strSenderEmailAddress, string strSenderName, string strAttachment)
        {
            var oMail = new MailMessage();
            var con = new SqlConnection(connectionString);
            //  Database connection
            con.Open();
            //  Lets open the connection
            var errStr = "";
            bool blResult;
            var strEmailbody = "";
            try
            {
                oMail.From = new MailAddress(strSenderName, strSenderEmailAddress);
                //  Multiple address are separated by a comma ("name1@email.com, name2@email.nl")
                if (strRecipient.Contains(","))
                {

                    //  We have multiple recipients
                    var strTempRecipient = strRecipient.Split(',');
                    for (var i = 0; i <= strTempRecipient.GetUpperBound(1); i++)
                    {
                        oMail.To.Add(new MailAddress(strTempRecipient[i]));
                    }

                }
                else
                {
                    //  Only 1 recipient
                    oMail.To.Add(strRecipient.Replace(';', ','));
                }

                //  Add cc
                if (!string.IsNullOrEmpty(strCC) && !strCC.ToLower().Equals("cc"))
                {
                    if (strCC.Contains(","))
                    {
                        //  We have multiple cc's
                        var strTempCc = strCC.Split(',');
                        for (var i = 0; i <= strTempCc.Length - 1; i++)
                        {
                            oMail.CC.Add(new MailAddress(strTempCc[i]));
                        }
                    }
                    else
                    {
                        //  Only 1 cc
                        oMail.CC.Add(strCC.Replace(';', ','));
                    }
                }

                //  Add bcc
                if (!string.IsNullOrEmpty(strBCC) && !strBCC.ToLower().Equals("bcc"))
                {
                    if (strBCC.Contains(","))
                    {
                        //  We have multiple bcc's
                        var strTempBcc = strBCC.Split(',');
                        for (var i = 0; i <= strTempBcc.Length - 1; i++)
                        {
                            oMail.Bcc.Add(new MailAddress(strTempBcc[i]));
                        }
                    }
                    else
                    {
                        //  Only 1 bcc
                        oMail.Bcc.Add(strBCC.Replace(';', ','));
                    }
                }

                //  Add subject
                oMail.Subject = strSubject;
                //  Now get the emailbody using a pageID.
                var contentSelect = new SqlCommand("publish_2_get_content", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                //  Which stored procedure are we using
                contentSelect.Parameters.AddWithValue("@intPageID", intPageID);
                //  Pass on the pageID as parameter
                contentSelect.Parameters.AddWithValue("@intPublicationsID", intPublicationID);
                //  Pass on the publications ID as parameter
                var DbReader = contentSelect.ExecuteReader();
                if (DbReader.HasRows)
                {
                    while (DbReader.Read())
                    {
                        //  Loop through the available records
                        strEmailbody = ReplaceReplacementCodes(DbReader["content"].ToString());
                    }
                }
                DbReader.Close();

                var mail = new MailMessage
                {
                    Body = strEmailbody,
                    IsBodyHtml = true,
                    BodyEncoding = Encoding.UTF8,
                    Attachments = { new System.Net.Mail.Attachment(strAttachment) }
                };

                if (!string.IsNullOrEmpty(strAttachment))
                {
                    //  Could hold multiple attachments seperated by a comma.
                    mail.Attachments.Add(new System.Net.Mail.Attachment(strAttachment));
                }

                // Dim pattern As String = ".+@.+\..+"
                // Dim rgx As New Regex(pattern, RegexOptions.IgnoreCase)
                // Dim matches As MatchCollection = rgx.Matches(strEmailbody)
                // If matches.Count = 0 Then
                //     Throw New System.Exception("Empty email body error")
                // Else
                // '    Send email.
                //    oSmtp.SendMail(oServer, oMail)
                // End If
                using (var smtpClient = new SmtpClient(GlobalConfiguration.SmtpServer))
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;

                    //  Get SMTP settings.
                    if (!string.IsNullOrEmpty(GlobalConfiguration.SmtpUserName) && !string.IsNullOrEmpty(GlobalConfiguration.SmtpPassword))
                    {
                        var user = CspDecoder.Decode(GlobalConfiguration.SmtpUserName);
                        var pwd = CspDecoder.Decode(GlobalConfiguration.SmtpPassword);
                        smtpClient.Credentials = new NetworkCredential(user, pwd);
                    }

                    smtpClient.Send(mail);
                }
            }
            catch (SmtpException exp)
            {
                errStr = exp.Message;
            }
            catch (System.Net.Sockets.SocketException exp)
            {
                errStr = $"Exception: Networking Error: {exp.ErrorCode} {exp.Message}";
            }
            catch (System.ComponentModel.Win32Exception exp)
            {
                errStr = $"Exception: System Error: {exp.ErrorCode} {exp.Message}";
            }
            catch (Exception exp)
            {
                errStr = $"Exception: Common: {exp.Message}";
            }

            blResult = (errStr.Length <= 0);

            //  Write result and additional information to the database
            SqlCommand logEmail = new SqlCommand("email_log_insert", con);
            //  Which stored procedure are we using
            logEmail.CommandType = CommandType.StoredProcedure;
            //  Confirm it's a stored procedure we're using here
            logEmail.Parameters.AddWithValue("@recipients", strRecipient);
            //  Pass on the parameters
            logEmail.Parameters.AddWithValue("@cc", strCC);
            logEmail.Parameters.AddWithValue("@bcc", strBCC);
            logEmail.Parameters.AddWithValue("@subject", strSubject);
            logEmail.Parameters.AddWithValue("@emailbody", strEmailbody);
            logEmail.Parameters.AddWithValue("@result", blResult);
            logEmail.Parameters.AddWithValue("@additional_info", errStr);
            logEmail.Parameters.AddWithValue("@ipaddress", Request.UserHostAddress);
            logEmail.Parameters.AddWithValue("@domain", Request_URL.Host);
            logEmail.ExecuteReader();
            //  Execute the query
            ;
        }

        static string GetQueryParam(string val)
        {
            return Request[val];
        }

        public static string GetBaseDomainOutOfURL(string inUrl)
        {
            var holdUrl = inUrl.ToUpper();
            if (holdUrl.StartsWith("HTTP://"))
            {
                holdUrl = holdUrl.Substring(7);
            }
            else if (holdUrl.StartsWith("HTTPS://"))
            {
                holdUrl = holdUrl.Substring(8);
            }
            else
            {
                throw new Exception(
                    "Error breaking URL into base domain in CategoryLoaderParams.GetBaseDomainOutOfURL. URL causing error: " +
                    inUrl);
            }

            if (holdUrl.Contains("/"))
            {
                holdUrl = holdUrl.Substring(0, holdUrl.IndexOf("/", StringComparison.Ordinal));
            }

            return holdUrl;
        }
        #endregion

        #region D2 functions

        public static ResponseResult D2Function(
            HttpContextBase context,
            ProxyModel proxyModel,
            ISynService service)
        {
            Request = context.Request;
            Context = context;
            Response = context.Response;
            Session = context.Session;
            //  Define the referer.
            strReferer = Request.ServerVariables["HTTP_REFERER"] ?? "";

            var contentType = "text/html";
            var responseContent = "";

            //  Get the ApplicationPool for which we need to handle the request. Depending on the ApplicationPool name, we use
            //  a specific connectionstring in the Web.config with the same name. The only difference is the databasename.
            // Dim strAppPath As String = Request.ServerVariables("APPL_MD_PATH")
            // connectionString = des.Decrypt(System.Configuration.ConfigurationManager.AppSettings.Item(getApplicationPoolName(strAppPath).ToLower))
            // connectionString = des.Decrypt(System.Configuration.ConfigurationManager.AppSettings.Item("JYtPEQEIUUhCxJ9hihnBE++k7Kc+1hEi"))
            connectionString = proxyModel.ConnectionString;
            Request_URL = proxyModel.RequestUrl;

            var originalQuery = proxyModel.ProxyQuery;
            if (!originalQuery.StartsWith("/"))
            {
                originalQuery = "/" + originalQuery;
            }

            int daysToFetchCache = proxyModel.DaysToFetchCache;

            //  Lets open the connection
            string domainName = proxyModel.RequestUrl.Host.ToLower();
            //  Get the domainame.
            strWorkURL = proxyModel.StrWorkUrl;
            strRawURL = proxyModel.StrRawUrl;
            intPublicationID = proxyModel.IntPublicationId;

            // Response.Write(Request.ServerVariables("REMOTE_ADDR").ToString + "|" + Request.ServerVariables("HTTP_USER_AGENT").ToString + "|" + strReferer + "|" + Request.ServerVariables("REMOTE_HOST").ToString + "|" + Request.ServerVariables("REQUEST_METHOD").ToString + "|" + Request_URL.AbsoluteUri + "|" + Nothing + "|" + Request_URL.AbsolutePath + "|" + Request_URL.AbsoluteUri + "|" + Request_URL.OriginalString + "|" + Request_URL.Query + "|" + Request_URL.OriginalString)
            // Response.Write(connectionString + "|" + strWorkURL + "|" + domainName)
            // Response.End()
            strWorkURL = strWorkURL.Replace("%5B", "[").Replace("%5D", "]");


            var originalRequest = new Uri("http://" + Request_URL.Host + originalQuery);
            if (daysToFetchCache > 0)
            {
                // Dim cachedValue As String = RecallFromCache(Request_URL, daysToFetchCache)
                string cachedValue = service.RecallFromCache(originalRequest, daysToFetchCache, lngId);
                if (!string.IsNullOrEmpty(cachedValue))
                {
                    Response.AppendHeader("CachedKey", originalRequest.ToString());
                    responseContent += cachedValue;
                    Response.End();
                }

            }

            var holdLogger = new LoggingInfo(
                Request.ServerVariables["REMOTE_ADDR"],
                Request.ServerVariables["HTTP_USER_AGENT"],
                strReferer,
                Request.ServerVariables["REMOTE_HOST"],
                Request.ServerVariables["REQUEST_METHOD"],
                Request_URL.AbsoluteUri,
                Guid.Empty,
                Request_URL.AbsolutePath,
                Request_URL.AbsoluteUri,
                Request_URL.OriginalString,
                Request_URL.Query,
                Request_URL.OriginalString);
            Cache nonCache = null;
            var xmlExport = new DCMXMLExport(holdLogger, connectionString, strWorkURL, domainName, nonCache, true, Session["language_code"].ToString());
            Response.Write(xmlExport.XMLResult);
            if (daysToFetchCache > 0)
            {
                service.InsertToCache(originalRequest, xmlExport.XMLResult, daysToFetchCache, lngId);
            }

            return new ResponseResult { Type = contentType, Content = responseContent };
        }

        #endregion
    }
}
