﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Csp.Core.DataAccess.Context;
using Csp.Core.Infrastructure.DAO.Dto;
using Csp.Core.Infrastructure.DAO.Helpers;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication.Models;
using Csp.Core.Infrastructure.Publication.Repositories;
using Csp.Core.Infrastructure.UrlParse;
using Libcore.DataAccess.Uow;

namespace Csp.Core.Infrastructure.Publication.Services
{
    public interface ISynService
    {
        int GetCompanyId(string baseDomain);
        int? GetLngId(string lngStr);
        void InsertToCache(Uri url, string content, int daysToCache, int? lngId);
        string RecallFromCache(Uri url, int daysToFetchCache, int? lngId);
        int GetIntPublicationID(int? intPageId, string host, out string errorMsg);
        void GetParameters(string host, ProxyModel proxyModel, out string errorMsg);
        void GetParameters_D2(string host, ProxyModel proxyModel, out string errorMsg);
        void GetContents(int? intPageId, int intPublicationID, ref string strPageContent, ref int intPublicationType, List<HTMLwPubName> RelatedPubHTML, out string errorMsg, ProxyModel proxyModel, HttpContextBase context);
        void GetJavascripts(int? intPageId, ref string strPageContent, out string errorMsg, ProxyModel proxyModel, HttpContextBase context);
        void GetSuppliers(string intThemeId, ref string strPageContent, out string errorMsg);
        void GetCategories(string intThemeId, ref string strPageContent);

        void GetStyleSheet(int? intPageId,
            ref string strPageTitle,
            ref string strMetaDescription,
            ref string strMetaKeywords,
            ref string strInlineStylesheet,
            ref string strExternalStylesheet,
            ref string strExternalJavaScript,
            ref string strBackGroundColor,
            ref string strBackgroundImage,
            ref string strOnloadScript,
            ref string strBodyTagExtension,
            ref string strInlineJavaScript,
            ref string strCSSCode);

        string ValueOrDefault(string value, string defaultstr = "");
        string ReplaceCharacters(string strContentToRender);
        string GetConsumerIdFromBaseDomain(string baseDomain);
        IList<string> FillEmptySupplierList(string consumerId);
        void GetCleanCategoryTree(string rootCategory, int depth, ref string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategories, CategoryItem cleanTree);
        void GetListOfSupplierBasedCategoryTrees(CategoryItem cleanCategoryTree, string consumerId, List<string> supplierIDs, List<SupplierCatTree> supplierTrees, string rootCategoryId);
        Language getLanguage(int id);
        Language getLanguage(string code);
        bool IsBaseDomain(string baseDomain);
        JsonMap GetConsumerOptions(string consumerBase);
        int ClearCache(string url);
        int ClearFetchCache();
    }

    public class SynService : ISynService
    {
        private CspSqlRepository Repo => _unitOfWork.ExtendedRepo<CspSqlRepository>();

        private readonly IUnitOfWork<EfContext> _unitOfWork;

        public SynService(IUnitOfWork<EfContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int GetCompanyId(string baseDomain)
        {
            //  Database connection
            //  Lets open the connection
            var companyId = Repo.GetCompanyId(baseDomain);

            // check if no-cache user
            var noCacheUser = Repo.GetNoCacheUser(companyId);

            if ("TRUE".Equals(noCacheUser, StringComparison.CurrentCultureIgnoreCase))
            {
                return -2;
            }

            return companyId;
        }

        public int? GetLngId(string lngStr)
        {
            return GetLngId(lngStr);
        }

        public void InsertToCache(Uri url, string content, int daysToCache, int? lngId)
        {
            var companyId = GetCompanyId(url.Host);
            if (companyId == -2)
            {
                // signifies this company is for testing, no cache
                return;
            }

            Repo.InsertToCache(companyId, url.PathAndQuery, content, daysToCache, lngId);

            //}
            //catch (Exception ex)
            //{
            //    // TODO: #If Then ... Warning!!! not translated
            //    Response.Write("InsertToCache");
            //    Response.Write(ex.Message);
            //    Response.Write(ex.StackTrace);
            //    Response.End();
            //    // TODO: #End If ... Warning!!! not translated
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        public string RecallFromCache(Uri url, int daysToFetchCache, int? lngId)
        {
            var companyId = GetCompanyId(url.Host);
            if (companyId == -2)
            {
                // signifies this company is for testing, no cache
                return null;
            }

            string cachedContent = null;
            try
            {
                var result = Repo.RecallFromCache(companyId, url.PathAndQuery, daysToFetchCache, lngId);
                if (result != null && result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        var encodingCode = item.Encoding;
                        Encoding Encoding;
                        try
                        {
                            Encoding = Encoding.GetEncoding(encodingCode);
                        }
                        catch (Exception)
                        {
                            Encoding = Encoding.UTF8;
                        }

                        cachedContent = Encoding.GetString(item.Content);
                    }
                }
            }
            catch (Exception)
            {
                cachedContent = null;
            }

            return cachedContent;
        }

        public int GetIntPublicationID(int? intPageId, string host, out string errorMsg)
        {
            return Repo.GetIntPublicationId(intPageId, host, out errorMsg);
        }

        public void GetParameters(string host, ProxyModel proxyModel, out string errorMsg)
        {
            errorMsg = "";
            var result = Repo.GetParameters(host);
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    //  Loop through the available records, should be 1 record only here.
                    proxyModel.StrWorkUrl = item.base_publication_parameters;
                    proxyModel.StrRawUrl = proxyModel.StrWorkUrl;
                    //  We need the rawURL later on for passing it on to HTML.SourceURL for paging.
                    proxyModel.StrWorkUrl = ReplaceCharacters(proxyModel.StrWorkUrl);
                    //  Replace any dangerous/strange characters in the base parameters.
                    proxyModel.IntPublicationId = item.publications_Id;
                    //  Set the publications ID. If we have a index_page_Id and base_parameters, the pageID from the
                    //  base_parameters will be used. If no base_parameters are defined, use the index_page_Id.
                    if (proxyModel.StrWorkUrl != "")
                    {
                        //  Use the pageID from the base_parameters.
                        if (item.base_publication_parameters.StartsWith("p") && item.base_publication_parameters.Contains("("))
                        {
                            if (int.TryParse(item.base_publication_parameters.Substring(1, item.base_publication_parameters.IndexOf("(") - 1), out var iresult))
                            {
                                proxyModel.IntPageId = iresult;
                            }
                            else
                            {
                                errorMsg = "PageID is not valid (ErrorCode 20).<br><br>";
                            }
                        }
                        if (item.base_publication_parameters.StartsWith("p"))
                        {
                            if (int.TryParse(item.base_publication_parameters.Substring(1, item.base_publication_parameters.Length - 1), out var iresult))
                            {
                                proxyModel.IntPageId = iresult;
                            }
                            else
                            {
                                errorMsg += "PageID is not valid (ErrorCode 25).<br><br>";
                                return;
                            }
                        }
                        else
                        {
                            errorMsg += "Invalid base parameters defined. No page or ending brackets found (ErrorCode 30).";
                            return;
                        }
                    }
                    else if (item.index_page_Id.HasValue)
                    {
                        //  Use the index_page_Id.
                        proxyModel.IntPageId = item.index_page_Id.Value;
                    }
                    else
                    {
                        errorMsg += "There is no index page or base_parameter defined for this publication (ErrorCode 35).";
                        return;
                    }
                }
            }
            else
            {
                //  No rows found....strange...a domain should always have parameters or a index page ID
                errorMsg += "No parameters and/or index pageID found for this publication (ErrorCode 40).";
            }
        }

        public void GetParameters_D2(string host, ProxyModel proxyModel, out string errorMsg)
        {
            errorMsg = "";
            var result = Repo.GetParameters(host);
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    proxyModel.StrWorkUrl = item.base_publication_parameters;
                    proxyModel.StrRawUrl = proxyModel.StrWorkUrl;
                    //  We need the rawURL later on for passing it on to HTML.SourceURL for paging.
                    proxyModel.StrWorkUrl = ReplaceCharacters(proxyModel.StrWorkUrl);
                    //  Replace any dangerous/strange characters in the base parameters.
                    proxyModel.IntPublicationId = item.publications_Id;
                    //  Set the publications ID. If we have a index_page_Id and base_parameters, the pageID from the
                    //  base_parameters will be used. If no base_parameters are defined, use the index_page_Id.
                    if (item.index_page_Id.HasValue)
                    {
                        //  Use the index_page_Id.
                        proxyModel.IntPageId = item.index_page_Id.Value;
                    }
                    else
                    {
                        errorMsg += "There is no index page or base_parameter defined for this publication (ErrorCode 35).";
                        return;
                    }
                }
            }
            else
            {
                //  No rows found....strange...a domain should always have parameters or a index page ID
                errorMsg += "No parameters and/or index pageID found for this publication (ErrorCode 40).";
            }
        }

        public void GetContents(int? intPageId, int intPublicationID, ref string strPageContent, ref int intPublicationType, List<HTMLwPubName> RelatedPubHTML, out string errorMsg, ProxyModel proxyModel, HttpContextBase context)
        {
            errorMsg = null;
            try
            {
                List<PublicationContentModel> result = Repo.GetContents(intPageId, intPublicationID, out errorMsg);

                if (result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        //  Loop through the available records
                        if (item.type.HasValue)
                        {
                            if (item.type == 0)
                            {
                                //  It's a page
                                //  Response.Write("Voor: " & DbReader.Item("content").ToString & "<br><br>")
                                strPageContent = ReplaceReplacementCodes(item.content, proxyModel, context);
                                //  Response.Write("Na: " & strPageContent)
                                intPublicationType = item.type.Value;
                            }
                            else if (item.type == 1 && item.publications_pages_Id == intPageId)
                            {
                                //  A component is being requested directly (check with ID from URL)
                                strPageContent = ReplaceReplacementCodes(item.content, proxyModel, context);
                                intPublicationType = item.type.Value;
                                RelatedPubHTML.Add(new HTMLwPubName(ReplaceReplacementCodes(item.content, proxyModel, context), "c" + item.publications_pages_Id));
                            }
                            else if (item.type == 1)
                            {
                                //  We have a component. Now get the ID and content for the component and pass it on to the URLParser
                                RelatedPubHTML.Add(new HTMLwPubName(ReplaceReplacementCodes(item.content, proxyModel, context), "c" + item.publications_pages_Id));
                            }
                            else if (item.type == 3)
                            {
                                //  Javascript - if we get a type = 3 here, there is a javascipt requested directly
                                strPageContent = item.content;
                                // ltu 3/31/10: run the javascript through the publication
                                // strPageContent = replaceReplacementCodes(DbReader.Item("content"))
                                intPublicationType = item.type.Value;
                            }
                            else if (item.type == 10)
                            {
                                strPageContent = ReplaceReplacementCodes(item.content, proxyModel, context);
                                //  Response.Write("Na: " & strPageContent)
                                intPublicationType = item.type.Value;
                            }
                        }
                    }
                }
                else
                {
                    //  No rows available, so nothing found. Show message.
                    errorMsg = "Page (" + intPageId + ") does not exists (ErrorCode 45).";
                }
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
            }
        }

        public void GetJavascripts(int? intPageId, ref string strPageContent, out string errorMsg, ProxyModel proxyModel, HttpContextBase context)
        {
            errorMsg = "";
            var result = Repo.GetJavascripts(intPageId);
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    //  Loop through the available records
                    var strJavaScript = "{javascript:" + item.publications_pages_Id + ":" + item.description + "}";
                    strJavaScript = ReplaceReplacementCodes(strJavaScript, proxyModel, context);
                    strPageContent = strPageContent.Replace(strJavaScript, "<script language= \"JavaScript\">" + Environment.NewLine + item.content + (Environment.NewLine + "</script>"));
                    strPageContent = strPageContent.Replace(strJavaScript, "");
                }
            }
            else
            {
                //  No rows available, so nothing found. Show message.
                errorMsg = "No javascripts found (ErrorCode 50).";
            }
        }

        public void GetSuppliers(string intThemeId, ref string strPageContent, out string errorMsg)
        {
            errorMsg = "";
            var suppliers = Repo.GetSuppliers(intThemeId);
            var strSuppliers = "<SCRIPT LANGUAGE=\"JavaScript\">function checkCategories() {" + Environment.NewLine + ("for (i = 0; i < document.subscriptionform.category.length; i++)" + (Environment.NewLine + " document.subscriptionform.category[i].checked = true ;" + Environment.NewLine + "}" + Environment.NewLine + "function checkSuppliers() {" + Environment.NewLine + "for (i = 0; i < document.subscriptionform.supplier.length; i++)" + Environment.NewLine) + " document.subscriptionform.supplier[i].checked = true ;" + (Environment.NewLine + "}" + Environment.NewLine + "function uncheckCategories() {" + (Environment.NewLine + "for (i = 0; i < document.subscriptionform.category.length; i++)" + Environment.NewLine + " document.subscriptionform.category[i].checked = false ;" + Environment.NewLine + "}" + Environment.NewLine + "function uncheckSuppliers() {" + Environment.NewLine + "for (i = 0; i < document.subscriptionform.supplier.length; i++)" + Environment.NewLine + " document.subscriptionform.supplier[i].checked = false ;" + Environment.NewLine + "}" + Environment.NewLine))) + "</script>" + Environment.NewLine;
            strSuppliers = strSuppliers + "<a href=\"javascript:checkSuppliers()\">Select all suppliers</a> | " + "<a href=\"javascript:uncheckSuppliers()\">Clear all suppliers</a>";
            strSuppliers = strSuppliers + "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"WIDTH: 440px\">" + Environment.NewLine;
            //  Holds a table with checkboxes and suppliers
            if (suppliers != null && suppliers.Count > 0)
            {
                foreach (var item in suppliers)
                {
                    //  Loop through the available records and build up table.
                    strSuppliers = strSuppliers + "<tr>" + Environment.NewLine + "<td style=\"WIDTH: 40px\"><input type=\"checkbox\" id=\"supplier\" name=\"supplier\" value=" + item.companies_Id + " /></td>" + Environment.NewLine + "<td style=\"WIDTH: 400px\">" + item.companyname + "</td>" + Environment.NewLine + "</tr>" + Environment.NewLine;
                }

                strSuppliers = (strSuppliers + ("</table>" + Environment.NewLine));
                strPageContent = strPageContent.Replace("{consumer_subscription:suppliers}", strSuppliers);
            }
            else
            {
                //  No rows available, so nothing found. Show message.
                errorMsg = "No suppliers found for this theme (ErrorCode 55).";
            }
        }

        public void GetCategories(string intThemeId, ref string strPageContent)
        {
            var categories = Repo.GetCategories(intThemeId);
            var strCategories = "<a href=\"javascript:checkCategories()\">Select all categories</a> | " + "<a href=\"javascript:uncheckCategories()\">Clear all categories</a>"
                                + "<input type=\"hidden\" name=\"themeID\" value=" + intThemeId + ">"
                                + "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"WIDTH: 440px\">" + Environment.NewLine;
            if (categories != null && categories.Count > 0)
            {
                foreach (var item in categories)
                {
                    //  Loop through the available records and build up table.
                    strCategories = strCategories + "<tr>" + Environment.NewLine + "<td style=\"WIDTH: 40px\"><input type=\"checkbox\" id=\"category\" name=\"category\" value=" + item.category_Id + " /></td>" + Environment.NewLine + "<td style=\"WIDTH: 400px\">" + item.categoryText + "</td>" + Environment.NewLine + "</tr>" + Environment.NewLine;
                }

                strCategories = (strCategories + ("</table>" + Environment.NewLine));
                strPageContent = strPageContent.Replace("{consumer_subscription:categories}", strCategories);
            }
        }

        public void GetStyleSheet(int? intPageId,
            ref string strPageTitle,
            ref string strMetaDescription,
            ref string strMetaKeywords,
            ref string strInlineStylesheet,
            ref string strExternalStylesheet,
            ref string strExternalJavaScript,
            ref string strBackGroundColor,
            ref string strBackgroundImage,
            ref string strOnloadScript,
            ref string strBodyTagExtension,
            ref string strInlineJavaScript,
            ref string strCSSCode)
        {
            var headerCssSelect = Repo.GetStyleSheet(intPageId);
            if (headerCssSelect != null && headerCssSelect.Count > 0)
            {
                foreach (var item in headerCssSelect)
                {
                    //  Loop through the available records
                    strPageTitle = ValueOrDefault(item.page_title);
                    strMetaDescription = ValueOrDefault(item.page_title);
                    strMetaKeywords = ValueOrDefault(item.meta_keywords);
                    strInlineStylesheet = ValueOrDefault(item.inline_stylesheet);
                    strExternalStylesheet = ValueOrDefault(item.external_stylesheet);
                    strExternalJavaScript = ValueOrDefault(item.external_javascript);
                    strBackGroundColor = ValueOrDefault(item.backgroundcolor);
                    strBackgroundImage = ValueOrDefault(item.backgroundimage);
                    strOnloadScript = ValueOrDefault(item.onload_script);
                    strBodyTagExtension = ValueOrDefault(item.body_tag_extension);
                    strInlineJavaScript = ValueOrDefault(item.inline_javascript);

                    if (!string.IsNullOrEmpty(item.Class) && string.IsNullOrEmpty(item.element))
                    {
                        if (item.Class.StartsWith("#"))
                        {
                            //  Classname is an ID, so no period (.) before the classname.
                            strCSSCode = strCSSCode + item.Class.Trim('.').ToLower() + " {" + Environment.NewLine;
                        }
                        else
                        {
                            strCSSCode = strCSSCode + "." + item.Class.Trim('.').ToLower() + " {" + Environment.NewLine;
                        }

                        int j;
                        var a = item.style_content.Split(';');
                        for (j = 0; j <= a.GetUpperBound(0); j++)
                        {
                            if (a[j].Trim().ToLower() != "")
                            {
                                strCSSCode = strCSSCode + a[j].Trim().ToLower() + (";" + Environment.NewLine);
                            }
                        }

                        strCSSCode = (strCSSCode + ("}" + Environment.NewLine));
                    }
                    else if (!string.IsNullOrEmpty(item.Class) && !string.IsNullOrEmpty(item.element))
                    {
                        if (item.Class.StartsWith("#"))
                        {
                            //  Classname is an ID, so no period (.) before the classname.
                            strCSSCode = strCSSCode + item.element.ToLower() + item.Class.Trim('.').ToLower() + item.pseudo_class + " {" + Environment.NewLine;
                        }
                        else
                        {
                            strCSSCode = strCSSCode + item.element.ToLower() + "." + item.Class.Trim('.').ToLower() + item.pseudo_class + " {" + Environment.NewLine;
                        }

                        int j;
                        var a = item.style_content.Split(';');
                        for (j = 0; j <= a.GetUpperBound(0); j++)
                        {
                            if (a[j].Trim().ToLower() != "")
                            {
                                strCSSCode = strCSSCode + a[j].Trim().ToLower() + ";" + Environment.NewLine;
                            }
                        }

                        strCSSCode = strCSSCode + "}" + Environment.NewLine;
                    }
                    else if (string.IsNullOrEmpty(item.Class) && !string.IsNullOrEmpty(item.element))
                    {
                        if (!string.IsNullOrEmpty(item.pseudo_class))
                        {
                            strCSSCode = strCSSCode + item.element.ToLower() + " " + item.pseudo_class + " {" + Environment.NewLine;
                        }
                        else
                        {
                            strCSSCode = strCSSCode + item.element.ToLower() + " {" + Environment.NewLine;
                        }

                        int j;
                        var a = item.style_content.Split(';');
                        for (j = 0; j <= a.GetUpperBound(0); j++)
                        {
                            if (a[j].Trim().ToLower() != "")
                            {
                                strCSSCode = strCSSCode + a[j].Trim().ToLower() + ";" + Environment.NewLine;
                            }
                        }

                        strCSSCode = strCSSCode + "}" + Environment.NewLine;
                    }
                }
            }
        }

        public string ValueOrDefault(string value, string defaultstr = "")
        {
            return !string.IsNullOrEmpty(value) ? value : defaultstr;
        }

        private string LoadPubFromFile(string pubContent)
        {
            // assumes PubContent contains {LoadFromFile:
            var errorConcat = "";
            try
            {
                string rootdir;
                try
                {
                    rootdir = GlobalConfiguration.RootPubDir;
                }
                catch (Exception)
                {
                    rootdir = "H:\\ui\\UniversalResources\\Publications";
                    errorConcat = "<!-- web.config does not contain RootPubDir key, assuming h:\\\\ui\\\\pubs -->";
                }

                if (!rootdir.EndsWith("\\"))
                {
                    rootdir = (rootdir + "\\");
                }

                var Landmark = "{LoadFromFile:";
                var pos = pubContent.IndexOf(Landmark);
                var filePath = pubContent.Substring((pos + Landmark.Length));
                //  read from : to end of line
                filePath = filePath.Substring(0, filePath.IndexOf("}"));
                filePath = (rootdir + filePath);
                pubContent = File.ReadAllText(filePath);
                return (pubContent + errorConcat);
            }
            catch (Exception ex)
            {
                return (ex.Message.Replace("\'", "").Replace("\\", "\\\\") + errorConcat);
            }
        }

        private string ReplaceReplacementCodes(string strPageContent, ProxyModel proxyModel, HttpContextBase context)
        {
            var Request = context.Request;
            var Session = context.Session;
            if (strPageContent.Contains("{LoadFromFile:"))
            {
                strPageContent = LoadPubFromFile(strPageContent);
            }

            if (strPageContent.Contains("{general:date:long}"))
            {
                strPageContent = strPageContent.Replace("{general:date:long}", DateTime.Now.ToString());
            }

            if (strPageContent.Contains("{general:date:short}"))
            {
                strPageContent = strPageContent.Replace("{general:date:full}", DateTime.Now.ToShortDateString());
            }

            if (strPageContent.Contains("{general:base_domain}"))
            {
                //  Domain only, e.g. sub.domain.com.
                strPageContent = strPageContent.Replace("{general:base_domain}", proxyModel.RequestUrl.Host);
            }

            if (strPageContent.Contains("{general:domain_abs_uri}"))
            {
                //  The URL as requested, including parameters.
                strPageContent = strPageContent.Replace("{general:domain_abs_uri}", proxyModel.RequestUrl.AbsoluteUri);
            }

            if (strPageContent.Contains("{general:base_parameters:"))
            {
                //  Get parameters from the base_paramaters. This is different from getting parameters from the URL.
                Regex r3;
                Match m3;
                r3 = new Regex("{general:base_parameters:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m3 = r3.Match(strPageContent);
                if (proxyModel.StrWorkUrl.Contains("&"))
                {
                    var strParametersFromWorkURL = proxyModel.StrWorkUrl.Split('&');
                    while (m3.Success)
                    {
                        foreach (var strGetString in strParametersFromWorkURL)
                        {
                            if (strGetString.StartsWith(m3.Groups[1].Value))
                            {
                                strPageContent = strPageContent.Replace("{general:base_parameters:" + m3.Groups[1].Value + "}", strGetString.Substring(strGetString.IndexOf("=") + 1, strGetString.Length - (strGetString.IndexOf("=") - 1)));
                                break;
                            }

                        }

                        m3 = m3.NextMatch();
                    }
                }
            }

            if (strPageContent.Contains("{get:"))
            {
                Regex r33;
                Match m33;
                r33 = new Regex("{get:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m33 = r33.Match(strPageContent);
                while (m33.Success)
                {
                    strPageContent = strPageContent.Replace("{get:" + m33.Groups[1].Value + "}", Request.QueryString[m33.Groups[1].Value]);
                    m33 = m33.NextMatch();
                }
            }

            if (strPageContent.Contains("{post:"))
            {
                Regex r34;
                Match m34;
                r34 = new Regex("{post:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m34 = r34.Match(strPageContent);
                while (m34.Success)
                {
                    strPageContent = strPageContent.Replace("{post:" + m34.Groups[1].Value + "}", Request.Form[m34.Groups[1].Value]);
                    m34 = m34.NextMatch();
                }
            }

            if (strPageContent.Contains("{encode_post:"))
            {
                Regex r34;
                Match m34;
                r34 = new Regex("{encode_post:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m34 = r34.Match(strPageContent);
                while (m34.Success)
                {
                    strPageContent = strPageContent.Replace("{encode_post:" + m34.Groups[1].Value + "}", HttpUtility.HtmlEncode(Request.Form[m34.Groups[1].Value]));
                    m34 = m34.NextMatch();
                }
            }

            if (strPageContent.Contains("{session:get:"))
            {
                Regex r3;
                Match m3;
                r3 = new Regex("{session:get:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                while (m3.Success)
                {
                    strPageContent = strPageContent.Replace("{session:get:" + m3.Groups[1].Value + "}", Session[m3.Groups[1].Value].ToString());
                    m3 = m3.NextMatch();
                }
            }

            //  Set Session.
            if (strPageContent.Contains("{session:set:"))
            {
                // Dim r37 As System.Text.RegularExpressions.Regex
                // Dim m37 As System.Text.RegularExpressions.Match
                // r37 = New System.Text.RegularExpressions.Regex("{session:set:(.*?):(.*?)}", RegexOptions.Compiled Or RegexOptions.Multiline Or RegexOptions.IgnoreCase)
                // m37 = r37.Match(strPageContent)
                // Dim strSessionValueToUse As String = ""
                // While m37.Success
                // If m37.Groups(2).Value.ToString.Contains("{") Then
                // strSessionValueToUse = m37.Groups(2).Value.ToString & "}"
                // End If
                // Session(m37.Groups(1).Value.ToString) = m37.Groups(2).Value.ToString
                // strPageContent = strPageContent.Replace("{session:set:" & m37.Groups(1).Value & ":" & m37.Groups(2).Value.ToString & "}", "")
                // m37 = m37.NextMatch()
                // End While
                Regex r37;
                Match m37;
                r37 = new Regex("{session:set:(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);
                m37 = r37.Match(strPageContent);
                while (m37.Success)
                {
                    string strSessionValueToUse;
                    if (m37.Groups[2].Value.Contains("{"))
                    {
                        strSessionValueToUse = m37.Groups[2].Value + "}";
                    }
                    else
                    {
                        strSessionValueToUse = m37.Groups[2].Value;
                    }

                    Session[m37.Groups[1].Value] = strSessionValueToUse;
                    strPageContent = strPageContent.Replace("{session:set:" + m37.Groups[1].Value + (":"
                                                                                                     + (strSessionValueToUse + "}")), "");
                    m37 = m37.NextMatch();
                }
            }

            //  Delete all sessions
            if (strPageContent.Contains("{session:delete:all}"))
            {
                if (Session.Keys.Count > 0)
                {
                    Session.Clear();
                }

                strPageContent = strPageContent.Replace("{session:delete:all}", "");
            }

            //  Delete a specific Session.
            if (strPageContent.Contains("{session:delete:"))
            {
                Regex r32;
                Match m32;
                r32 = new Regex("{session:delete:(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                m32 = r32.Match(strPageContent);
                while (m32.Success)
                {
                    Session.Remove(m32.Groups[1].Value);
                    strPageContent = strPageContent.Replace("{session:delete:" + m32.Groups[1].Value + "}", "");
                    m32 = m32.NextMatch();
                }
            }

            if (strPageContent.Contains("{general:replace:"))
            {
                Regex r3;
                Match m3;
                r3 = new Regex("{general:replace:(.*?):(.*?):(.*?)}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                m3 = r3.Match(strPageContent);
                while (m3.Success)
                {
                    var strfinalContent = m3.Groups[1].Value.Replace(m3.Groups[2].Value, m3.Groups[3].Value);
                    strPageContent = strPageContent.Replace("{general:replace:" + m3.Groups[1].Value + ":" + m3.Groups[2].Value + ":" + m3.Groups[3].Value + "}", strfinalContent);
                    m3 = m3.NextMatch();
                }
            }

            return strPageContent;
        }

        public string ReplaceCharacters(string strContentToRender)
        {
            strContentToRender = strContentToRender.Replace("%", "~");
            strContentToRender = strContentToRender.Replace("#", "!");
            strContentToRender = strContentToRender.Replace("~", "%");
            strContentToRender = strContentToRender.Replace("!", "#");
            strContentToRender = strContentToRender.Replace("*LB*", "<");
            strContentToRender = strContentToRender.Replace("*RB*", ">");
            strContentToRender = strContentToRender.Replace("*LB*", "<");
            strContentToRender = strContentToRender.Replace("*RB*", ">");
            strContentToRender = strContentToRender.Replace("*Q*", "?");
            strContentToRender = strContentToRender.Replace("*Q*", "?");
            strContentToRender = strContentToRender.Replace("*FS*", "/");
            strContentToRender = strContentToRender.Replace("*FS*", "/");
            strContentToRender = strContentToRender.Replace("*BS*", "\\");
            strContentToRender = strContentToRender.Replace("*BS*", "\\");
            strContentToRender = strContentToRender.Replace("*AMP*", "&");
            strContentToRender = strContentToRender.Replace("*AMP*", "&");
            //  URL's from mail need the following fix
            strContentToRender = strContentToRender.Replace("%5b", "[");
            strContentToRender = strContentToRender.Replace("%5d", "]");
            //  04/13
            strContentToRender = strContentToRender.Replace("=%", "EQPERCENT123");
            strContentToRender = strContentToRender.Replace("%28", "(").Replace("%29", ")").Replace("*C*", ":");
            strContentToRender = strContentToRender.Replace("EQPERCENT123", "=%");
            // strContentToRender = strContentToRender.Replace("%28", "(")
            // strContentToRender = strContentToRender.Replace("%29", ")")
            // strContentToRender = strContentToRender.Replace("*C*", ":")
            return strContentToRender;
        }

        public string GetConsumerIdFromBaseDomain(string baseDomain)
        {
            return Repo.GetConsumerIdFromBaseDomain(baseDomain);
        }

        public IList<string> FillEmptySupplierList(string consumerId)
        {
            return Repo.FillEmptySupplierList(consumerId);
        }

        public void GetCleanCategoryTree(string rootCategory, int depth, ref string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategories, CategoryItem cleanTree)
        {
            Repo.GetCleanCategoryTree(rootCategory, depth, ref rootCategoryId, flatListOfAllCleanCategories, cleanTree);
        }

        public void GetListOfSupplierBasedCategoryTrees(CategoryItem cleanCategoryTree, string consumerId, List<string> supplierIDs, List<SupplierCatTree> supplierTrees, string rootCategoryId)
        {
            Repo.GetListOfSupplierBasedCategoryTrees(cleanCategoryTree, consumerId, supplierIDs, supplierTrees, rootCategoryId);
        }

        public Language getLanguage(int id)
        {
            return Repo.getLanguage(id);
        }

        public Language getLanguage(string code)
        {
            return Repo.getLanguage(code);
        }

        public bool IsBaseDomain(string baseDomain)
        {
            return Repo.IsBaseDomain(baseDomain);
        }

        public JsonMap GetConsumerOptions(string consumerBase)
        {
            return Repo.GetConsumerOptions(consumerBase);
        }

        public int ClearCache(string url)
        {
            return Repo.ClearCache(url);
        }

        public int ClearFetchCache()
        {
            return Repo.ClearFetchCache();
        }
    }
}