﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Csp.Core.Infrastructure.DAO;
using Csp.Core.Infrastructure.DAO.Helpers;
using Csp.Core.Infrastructure.Domain;

namespace Csp.Core.Infrastructure.Publication.Repositories
{
    public class SynSqlRepository : AdoNetRepository
    {
        public static SynSqlRepository Create(string connectionString)
        {
            return new SynSqlRepository(connectionString);
        }

        public SynSqlRepository(string connectionString) : base(connectionString)
        {
        }

        public bool IsBaseDomain(string baseDomain)
        {
            const string sql = "select count(base_domain) from companies_consumers where base_domain = @baseDomain ";
            return GetValue<int>(sql, new SqlParameter("baseDomain", baseDomain)) == 1;
        }

        public Language GetLanguage(int id)
        {
            const string sql = "select languagecode from languages where languages_Id = @id";
            var code = GetValue<string>(sql, new SqlParameter("id", id));
            return new Language(code, id);
        }

        public Language GetLanguage(string code)
        {
            const string sql = "select languages_Id from languages where lower(languagecode) = lower(@code)";
            var id = GetValue<int>(sql, new SqlParameter("code", code));
            return new Language(code, id);
        }

        //parses a 2 column resultset into a <key=column1,value=column2> dictionary
        public IDictionary<string, string> GetMap(string sql, Dictionary<string, object> parameters)
        {
            using (var reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.Text, sql, ToSqlParameters(parameters).ToArray()))
            {
                var dict = new Dictionary<string, string>();
                if (reader.Read())
                {
                    dict.Add(reader.GetFieldValue<string>(0), reader.GetFieldValue<string>(1));
                }

                return dict;
            }
        }

        public IDictionary<string, string> GetMapTranspose(string sql, Dictionary<string, object> parameters)
        {
            IDictionary<string, string> results = new Dictionary<string, string>();

            using (var reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.Text, sql, ToSqlParameters(parameters).ToArray()))
            {
                if (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var key = reader.GetName(i);
                        var value = reader.GetValue(i).ToString();
                        results[key] = value;
                    }
                }
            }

            return results;
        }

        public JsonMap GetConsumerOptions(string consumerBase)
        {
            var options = new JsonMap();
            const string consumerOptionsSql = @"
                                            select c.parametername,b.value_text 
                                            from companies_consumers a
                                                join companies_parameters b on a.companies_Id = b.companies_Id
                                                join companies_parameter_types c on b.companies_parameter_types_Id=c.companies_parameter_types_Id
                                                where a.base_domain = @base_dom 
                                            ";

            const string consumerOptionsSql2 = @"
                                                select a.*, b.*, c.*, d.companyname as sId, d.companies_Id as sName
                                                from companies_consumers a
                                                    join companies b on a.companies_Id = b.companies_Id
                                                    join languages c on a.default_language_Id = c.languages_Id
                                                    join companies d on d.companies_Id = b.parent_companies_Id
                                                where a.base_domain = @base_dom 
                                                ";
            options.AddRange(GetMap(consumerOptionsSql, new Dictionary<string, object> { { "base_dom", consumerBase } }));

            //get additional parameters
            var temp = GetMapTranspose(consumerOptionsSql2, new Dictionary<string, object> { { "base_dom", consumerBase } });

            foreach (var p in temp)
            {
                if (!options.ContainsKey(p.Key))
                {
                    options.Add(p.Key, p.Value);
                }
            }

            return options;
        }

        public EdTuple3<string, string, string> GetVendorCompanyInfoFromProjectAliasResult(string InProjectAliasResult)
        {
            string queryCompanyID = @"
            DECLARE @found AS INT = 0; 
            SELECT  @found = COUNT(*) FROM dbo.companies c WHERE c.externalCode = @Project
            IF (@found > 0)
	            begin
	            SELECT TOP 1 CAST (companies_Id AS NVARCHAR(50)) FROM dbo.companies c WHERE c.externalCode = @Project
	            end
            ELSE 
	            begin
	            SELECT '0000'
	            end
            ";

            //companyname

            string queryCompanyName = @"
            DECLARE @found AS INT = 0; 
            SELECT  @found = COUNT(*) FROM dbo.companies c WHERE c.externalCode = @Project
            IF (@found > 0)
	            begin
	            SELECT TOP 1 CAST (companyname AS NVARCHAR(500)) FROM dbo.companies c WHERE c.externalCode = @Project
	            end
            ELSE 
	            begin
	            SELECT 'CompanyNameNotFound'
	            end
            ";

            string queryDCSID = @"
                DECLARE @found AS INT = 0; 
                SELECT  @found = COUNT(*) FROM dbo.companies c
		            INNER JOIN dbo.companies_parameter_types cpt ON cpt.parametername = 'WebTrendsDCSID'
		            INNER JOIN dbo.companies_parameters cp ON cp.companies_Id = c.companies_Id AND cp.companies_parameter_types_Id = cpt.companies_parameter_types_Id
		            WHERE c.externalCode = @Project
                IF (@found > 0)
	                begin
	                    SELECT TOP 1 cp.value_text FROM dbo.companies c
		                INNER JOIN dbo.companies_parameter_types cpt ON cpt.parametername = 'WebTrendsDCSID'
		                INNER JOIN dbo.companies_parameters cp ON cp.companies_Id = c.companies_Id AND cp.companies_parameter_types_Id = cpt.companies_parameter_types_Id
		                WHERE c.externalCode = @Project
	                end
                ELSE 
	                begin                    
	                SELECT  'ZZZZZZZZZZZZZZZZZZZZZZZZZ_ZZZZ'
	                end
            ";
            string companyid = GetValue<string>(queryCompanyID, new SqlParameter("project", InProjectAliasResult));

            string companyname = GetValue<string>(queryCompanyName, new SqlParameter("project", InProjectAliasResult));
            string DCSID = GetValue<string>(queryDCSID, new SqlParameter("project", InProjectAliasResult));

            return new EdTuple3<string, string, string>(companyid, companyname, DCSID);
        }

        public string GetShowHoverValue(string InCompanyId)
        {
            const string getShowHoverSql =
                @"DECLARE @OffMatchCount INT = 0
                    SELECT @OffMatchCount = COUNT(1) FROM dbo.companies_parameter_types	cpt 
                    INNER JOIN dbo.companies_parameters cp ON cpt.companies_parameter_types_Id = cp.companies_parameter_types_Id
                    WHERE cpt.parametername = 'CSP_ShowHover' AND cp.value_text = '0'
                    AND cp.companies_Id = @InCompanyId
                    IF @OffMatchCount = 0
                    SELECT '1'
                    ELSE	
                    SELECT '0'
                    ";


            var obj = GetValue<string>(getShowHoverSql,
                new SqlParameter("@InCompanyId", InCompanyId));

            return obj ?? string.Empty;
        }
        /* @Name: getCustomContenFieldValue
        * @Description: Get the Custom Content Field Value for the Buy Now button.
        * @Parameters:  
        * contentId = the content_Id
        * @Created on: 06/11/2014
        * @Author: Victor Roman
        * */
        public string GetCustomContenFieldValue(string contentId, string consumerId, string customField)
        {
            const string getCustomContenFieldSql =
                @"if exists (
                                select ccf.value_text, ctf.fieldname,ccf.consumer_Id
                                from content c 
                                  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
                                  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id  
                                        INNER JOIN dbo.custom_content_fields ccf ON ccf.content_fields_Id = cf.content_fields_Id
                                WHERE (ccf.consumer_Id = @consumerid AND c.content_Id = @incontentid AND ctf.fieldname = @fieldName) 
                                )
                                select ccf.value_text, ctf.fieldname,ccf.consumer_Id
                                from content c 
                                  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
                                  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id  
                                        INNER JOIN dbo.custom_content_fields ccf ON ccf.content_fields_Id = cf.content_fields_Id
                                WHERE (ccf.consumer_Id = @consumerid AND c.content_Id = @incontentid AND ctf.fieldname = @fieldName) 
                                else
                                select cf.value_text, ctf.fieldname,@consumerid as 'consumer_Id'
                                from content c 
                                  INNER JOIN dbo.content_fields cf ON cf.content_Id = c.content_Id
                                  INNER JOIN dbo.content_types_fields ctf ON ctf.content_types_fields_Id = cf.content_types_fields_Id          
                                WHERE (c.content_Id = @incontentid AND ctf.fieldname = @fieldName)
                                ";
            var obj = GetValue<string>(getCustomContenFieldSql,
                ToSqlParameters(new Dictionary<string, object>
                {
                    {"@incontentid", contentId},
                    {"@consumerid", consumerId},
                    {"@fieldName", customField}
                }).ToArray());

            return obj ?? string.Empty;
        }

        public bool ValidateKeywordUsingHashTable(int ctfId, string keyword, int languageid)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            string validateSkuUsingHashTableSql = @"
                    select COUNT(*) from Content_Field_HashTable a 
	                    join content b on a.ContentGUID=b.content_Id
                    where ContentTypesFieldID=@ctfId and b.content_types_languages_Id=@lngId and b.stage_Id=50
                ";
            parameters.Add("ctfId", ctfId);
            parameters.Add("lngId", languageid);

            string[] keywords = keyword.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (keywords.Length == 0) return false;

            for (int i = 0; i < keywords.Length; i++)
            {
                parameters.Add("key" + i, keywords[i]);
                if (i == 0)
                    validateSkuUsingHashTableSql += " and (";
                else
                    validateSkuUsingHashTableSql += " or ";
                validateSkuUsingHashTableSql += "a.value_text=@key" + i + " ";
            }
            validateSkuUsingHashTableSql += ")";
            return GetValue<int>(validateSkuUsingHashTableSql, ToSqlParameters(parameters).ToArray()) > 0;
        }

        public bool ValidateSku(int ctfId, String sku, int lng)
        {
            const String validateSkuSql = "select COUNT(1) from content_main a " +
            " join content b on a.content_main_Id = b.content_main_Id " +
            " join content_fields c on b.content_id = c.content_Id " +
            " where b.content_types_languages_Id = @lng and b.stage_Id = 50 " +
            " and c.content_types_fields_Id = @ctfid and upper(c.value_text) like '%'+upper(@sku)+'%' ";
            int count = GetValue<int>(validateSkuSql, ToSqlParameters(new Dictionary<String, Object>(){
                {"ctfId",ctfId},
                {"sku", sku},
                {"lng", lng}
            }).ToArray());
            return (count > 0);
        }

        public bool ValidateSkuUsingHashTable(int ctfId, string sku, int lngId)
        {
            if (ctfId == -1)
                return false;

            int count = 0;
            string query = @"
if exists (select 1 from sys.columns col where col.name = N'UpperCaseHash' and col.object_id = OBJECT_ID(N'Content_Field_HashTable'))
	select COUNT(*)
	from Content_Field_HashTable a WITH (NOLOCK) 
		join content b WITH (NOLOCK)  on a.ContentGUID=b.content_Id
	where a.ContentTypesFieldID=@ctfId
		and HASHBYTES('SHA1',convert(nvarchar(max), @sku)) = a.UpperCaseHash
		and b.content_types_languages_Id=@lngId
		and b.stage_Id=50
else
	select COUNT(*)
	from Content_Field_HashTable a WITH (NOLOCK) 
		join content b WITH (NOLOCK)  on a.ContentGUID=b.content_Id
	where a.ContentTypesFieldID=@ctfId
		and HASHBYTES('SHA1',convert(nvarchar(max), @sku)) = a.HashValue
		and b.content_types_languages_Id=@lngId
		and b.stage_Id=50 ";
            try
            {
                count = GetValue<int>(query, ToSqlParameters(new Dictionary<string, object>()
                {
                    {"ctfId", ctfId},
                    {"sku", sku},
                    {"lngId", lngId}
                }).ToArray());
            }
            catch (Exception ex)
            {
                return ValidateSku(ctfId, sku, lngId);
            }
            return count > 0;
        }

        public bool ValidateSkuUsingHashTable(string fieldName, int ctfId, string sku, int lngId)
        {
            if (ctfId == -1)
                return false;

            string query = @"
select * from content_types_fields a
	join (
select ctf.content_types_Id
from content_types_fields ctf 
where ctf.content_types_fields_Id=@ctfId) b on a.content_types_Id=b.content_types_Id
where a.fieldname = @fieldName ";
            int id = GetValue<int>(query, ToSqlParameters(new Dictionary<string, object>
            {
                {"ctfId", ctfId},
                {"fieldName", fieldName}
            }).ToArray());
            return ValidateSkuUsingHashTable(id, sku, lngId);
        }

        public string FindXdmlInjectionQueryString(string queryType)
        {
            var obj = GetValue<string>("select QueryString from XDMLInjectionQueryMap where RequestType = @queryType",
                new SqlParameter("@queryType", queryType));
            //return "p157(ct15|ct6&fMfPN~1={mfrsku.value}!)";
            return obj ?? string.Empty;
        }

        public IDictionary<String, String> GetTranslatorRows()
        {
            const string getTranslatorRowsSql = "select condition,replacement from url_translate where priority is not null order by priority";
            return GetMap(getTranslatorRowsSql, null);
        }
    }
}
