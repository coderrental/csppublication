﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Csp.Core.DataAccess.Context;
using Csp.Core.DbHelpers;
using Csp.Core.Infrastructure.DAO.Dto;
using Csp.Core.Infrastructure.DAO.Helpers;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.UrlParse;
using Libcore.Core.Extensions;

namespace Csp.Core.Infrastructure.Publication.Repositories
{
    public class CspSqlRepository : GenericSqlRepository<EfContext>
    {
        public CspSqlRepository(EfContext context) : base(context)
        {
        }

        public int GetCompanyId(string baseDomain)
        {
            return GetValue<int>("select companies_Id from companies_consumers where base_domain = @Base_Domain ",
                new SqlParameter("@Base_Domain", baseDomain));
        }

        public string GetNoCacheUser(int companyId)
        {
            return GetValue<string>(
                "select a.value_text from companies_parameters a join companies_parameter_types b on a.companies_param" +
                "eter_types_Id = b.companies_parameter_types_Id where b.parametername = \'No_Cache_User\' and a.compani" +
                "es_Id = @CompanyId", new SqlParameter("@CompanyId", companyId));
        }

        public int? GetLngId(string lngStr)
        {
            return GetValue<int?>("select languages_Id from languages where languagecode=@LngCode ",
                new SqlParameter("@LngCode", lngStr));
        }

        public int ClearCache(string url)
        {
            return ExecuteNonQuery(@"
                                        update PubCache
                                        set IsExpired = 1
                                        from PubCache a 
	                                        join companies_consumers b on b.companies_Id = a.CompanyId
                                        where a.IsExpired = 0 and b.base_domain = @baseDomain
                                    ", new SqlParameter("@baseDomain", url));
        }

        public int ClearFetchCache()
        {
            return ExecuteNonQuery("delete from FetchCache");
        }

        public void InsertToCache(int companyId, string queryString, string content, int daysToCache, int? lngId)
        {
            var encoding = Encoding.UTF8;
            var result = GetValue<int>(
                " select count(*) from PubCache with (nolock) where HASHBYTES(\'sha1\',convert(nvarchar,@CompanyId) + co" +
                "nvert(nvarchar,@LngId) + @Query ) = Company_Lng_Query_Hash and (DATEDIFF(DAY,DateTimeStamp,CURRENT_T" +
                "IMESTAMP)) < @daysToCache and IsExpired = 0 ",
                new SqlParameter("@CompanyId", companyId),
                new SqlParameter("@LngId", lngId),
                new SqlParameter("@Query", queryString),
                new SqlParameter("@Encoding", encoding.WebName),
                new SqlParameter("@Content", encoding.GetBytes(content)),
                new SqlParameter("@IsCompressed", false),
                new SqlParameter("@IsExpired", false),
                new SqlParameter("@daysToCache", daysToCache));
            if (result == 0)
            {
                ExecuteNonQuery(
                    @"insert into PubCache(Company_Lng_Query_Hash,CompanyId,LngId,QueryString,Content,DateTimeStamp,Encoding, IsCompressed, IsExpired) values (HASHBYTES('sha1',convert(nvarchar,@CompanyId) + convert(nvarchar,@LngId) + @Query ),@CompanyId,@LngId,@Query,@Content,CURRENT_TIMESTAMP,@Encoding, @IsCompressed, @IsExpired)");
            }
        }

        public IList<RecallCacheModel> RecallFromCache(int companyId, string queryString, int daysToFetchCache, int? lngId)
        {
            return GetList<RecallCacheModel>(
                "SELECT TOP 1 Content,Encoding FROM PubCache with (nolock) where [Company_Lng_Query_Hash] = HASHBYTES(" +
                "\'sha1\', convert(nvarchar,@CompanyId)+convert(nvarchar,@LngId) + @Query) and (DATEDIFF(DAY,DateTimeSt" +
                "amp,GETDATE())) < @DaysToCache and IsExpired = 0 ",
                new SqlParameter("@CompanyId", companyId),
                new SqlParameter("@LngId", lngId),
                new SqlParameter("@Query", queryString),
                new SqlParameter("@DaysToCache", daysToFetchCache));
        }

        public int GetIntPublicationId(int? intPageId, string host, out string errorMsg)
        {
            errorMsg = null;
            int? result = null;
            try
            {
                result = GetSpValue<int?>("publish_0_check_page_domain",
                    new SqlParameter("@pageID", intPageId),
                    new SqlParameter("@domain", host));
                if (!result.HasValue)
                {
                    errorMsg =
                        "You are not allowed to request this publication (ErrorCode 70).<br><br><a href=http://" +
                        host + ">Go back to the homepage</a> ";
                }
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
            }

            return result ?? 0;
        }

        public IList<CspParameterModel> GetParameters(string host)
        {
            return GetSpList<CspParameterModel>("publish_1_get_domain_parameters",
                new SqlParameter("@domain", host));
        }

        public List<PublicationContentModel> GetContents(int? intPageId, int intPublicationId, out string errorMsg)
        {
            errorMsg = null;
            List<PublicationContentModel> result = null;
            try
            {
                result = GetSpList<PublicationContentModel>("publish_2_get_content",
                    new SqlParameter("@intPageID", intPageId),
                    new SqlParameter("@intPublicationsID", intPublicationId)).ToList();

                if (result.Count == 0)
                {
                    //  No rows available, so nothing found. Show message.
                    errorMsg = "Page (" + intPageId + ") does not exists (ErrorCode 45).";
                }
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
            }

            return result;
        }

        public IList<JavascriptModel> GetJavascripts(int? intPageId)
        {
            return GetSpList<JavascriptModel>("publish_4_get_javascript",
                new SqlParameter("@intPageID", intPageId));
        }

        public IList<CspSupplierModel> GetSuppliers(string intThemeId)
        {
            return GetSpList<CspSupplierModel>("add_new_consumer_get_suppliers_by_theme",
                new SqlParameter("@intThemeID", intThemeId));
        }

        public IList<CspCategoryModel> GetCategories(string intThemeId)
        {
            return GetSpList<CspCategoryModel>("add_new_consumer_get_categories_by_theme",
                new SqlParameter("@intThemeID", intThemeId));
        }

        public IList<StyleSheetModel> GetStyleSheet(int? intPageId)
        {
            return GetSpList<StyleSheetModel>("publish_3_get_pageproperties_header_css",
                new SqlParameter("@publicationpageID", intPageId));
        }

        public string GetConsumerIdFromBaseDomain(string baseDomain)
        {
            const string sql = @"SELECT *
                                            FROM companies_consumers
                                            WHERE UPPER(base_domain) = @basedomain";

            return GetValueList<string>(sql, new SqlParameter("@basedomain", baseDomain)).FirstOrDefault();
        }

        //parses a 2 column resultset into a <key=column1,value=column2> dictionary
        public IDictionary<string, string> GetMap(string sql, Dictionary<string, object> parameters)
        {
            return GetValueDictionary<string, string>(sql, ToSqlParameters(parameters));
        }

        public IDictionary<string, string> GetMapTranspose(string sql, Dictionary<string, object> parameters)
        {
            IDictionary<string, string> results = new Dictionary<string, string>();
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                command.Parameters.AddRange(ToSqlParameters(parameters).ToArray());
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        var key = reader.GetName(i);
                        var value = reader.GetValue(i).ToString();
                        results[key] = value;
                    }
                }
            }

            return results;
        }

        public JsonMap GetConsumerOptions(string consumer_base)
        {
            var options = new JsonMap();
            const string consumerOptionsSql = @"
                                            select c.parametername,b.value_text 
                                            from companies_consumers a
                                                join companies_parameters b on a.companies_Id = b.companies_Id
                                                join companies_parameter_types c on b.companies_parameter_types_Id=c.companies_parameter_types_Id
                                                where a.base_domain = @base_dom 
                                            ";

            const string consumerOptionsSql2 = @"
                                                select a.*, b.*, c.*, d.companyname as sId, d.companies_Id as sName
                                                from companies_consumers a
                                                    join companies b on a.companies_Id = b.companies_Id
                                                    join languages c on a.default_language_Id = c.languages_Id
                                                    join companies d on d.companies_Id = b.parent_companies_Id
                                                where a.base_domain = @base_dom 
                                                ";
            options.AddRange(GetMap(consumerOptionsSql, new Dictionary<string, object> { { "base_dom", consumer_base } }));

            //get additional parameters
            var temp = GetMapTranspose(consumerOptionsSql2, new Dictionary<string, object> { { "base_dom", consumer_base } });

            foreach (var p in temp)
            {
                if (!options.ContainsKey(p.Key))
                {
                    options.Add(p.Key, p.Value);
                }
            }

            return options;

        }

        public Language getLanguage(int id)
        {
            const string sql = "select languagecode from languages where languages_Id = @id";
            var code = GetValue<string>(sql, new SqlParameter("id", id));
            return new Language(code, id);
        }

        public Language getLanguage(string code)
        {
            const string sql = "select languages_Id from languages where lower(languagecode) = lower(@code)";
            var id = GetValue<int>(sql, new SqlParameter("code", code));
            return new Language(code, id);
        }

        public bool IsBaseDomain(string baseDomain)
        {
            const string sql = "select count(base_domain) from companies_consumers where base_domain = @baseDomain ";
            return GetValue<int>(sql, new SqlParameter("baseDomain", baseDomain)) == 1;
        }
        #region CategoryLoader

        public IList<string> FillEmptySupplierList(string consumerId)
        {
            const string sql = "SELECT DISTINCT supplier_Id FROM consumers_themes where consumer_id = @ConsumerID";
            return GetValueList<string>(sql, new SqlParameter("@ConsumerID", consumerId));
        }

        private CategoryItem GetNewCategoryItemFromReaderRow(DbDataReader inReader, CategoryItem parentCategoryItem, List<CategoryItem> FlatListOfAllCleanCategories)
        {
            var externalCatCode = inReader["ExternalCategoryCode"].ToString();
            var curCatId = inReader["categoryId"].ToString();
            var curDepth = int.Parse(inReader["depth"].ToString());
            var isPublishedToday = false;
            var publishedTodayStr = inReader["PubSRuleActive"].ToString();
            if (publishedTodayStr == "True")
            {
                isPublishedToday = true;
            }
            var curActive = inReader["active"] as bool? ?? false;
            var holdNewItem = new CategoryItem(parentCategoryItem, externalCatCode, curCatId, curDepth, curActive,
                isPublishedToday);
            FlatListOfAllCleanCategories.Add(holdNewItem);

            return holdNewItem;
        }

        private void StripOutItemsBelowDepth(int maxDepth, int curDepth, CategoryItem curCatItem)
        {
            if (curDepth >= maxDepth)
            {
                curCatItem.ChildCategories.Clear();
            }
            else
            {
                foreach (var item in curCatItem.ChildCategories)
                {
                    StripOutItemsBelowDepth(maxDepth, curDepth + 1, item);
                }
            }
        }

        public void GetListOfSupplierBasedCategoryTrees(CategoryItem cleanCategoryTree, string consumerId, List<string> supplierIDs, List<SupplierCatTree> supplierTrees, string rootCategoryId)
        {
            foreach (var item in supplierIDs)
            {
                var supplierTree = new SupplierCatTree(consumerId, item, cleanCategoryTree);
                supplierTrees.Add(supplierTree);
            }
            foreach (var item in supplierTrees)
            {
                var command = new SqlCommand(
                    "SELECT categories.categoryId, categories.parentId, categories.categoryText, categories.dateAdded, categories.depth, categories.active, categories.lineage, categories.publication_schemeID, categories.publication_schemeID_added, categories.apply_to_childs, categories.ExternalCategoryCode, consumers_themes.consumer_Id, consumers_themes.supplier_Id FROM consumers_themes INNER JOIN categories with (nolock) ON consumers_themes.category_Id = categories.categoryId AND consumers_themes.supplier_Id = @SupID AND consumers_themes.consumer_Id = @ConsumerID WHERE (categories.lineage LIKE @ParentCatID) ORDER BY categories.depth");
                command.Parameters.Add(new SqlParameter("@SupID", item.SupplierID));
                command.Parameters.Add(new SqlParameter("@ConsumerID", item.ConsumerID));
                command.Parameters.Add(new SqlParameter("@ParentCatID", "%/" + rootCategoryId + "/%"));

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    item.MarkCatIDAsInTheme(reader["CategoryId"].ToString());
                }
                reader.Close();
            }
        }

        public void GetCleanCategoryTree(string rootCategory, int depth, ref string rootCategoryId, List<CategoryItem> flatListOfAllCleanCategories, CategoryItem cleanTree)
        {
            var sql = "SELECT top 1 categoryId FROM categories with (nolock) WHERE categoryId  = @CatCode";
            rootCategoryId = GetValue<string>(sql, new SqlParameter("@CatCode", rootCategory));

            sql = "SELECT DISTINCT categories.categoryId, categories.parentId, categories.categoryText, categories.dateAdded, categories.depth, categories.active, categories.lineage, categories.publication_schemeID, categories.publication_schemeID_added, categories.apply_to_childs, categories.ExternalCategoryCode, publication_schemes.active AS PubSchemeActive, publication_schemes_rules.active AS PubSRuleActive FROM publication_schemes_rules INNER JOIN publication_schemes ON publication_schemes_rules.publication_schemes_Id = publication_schemes.publication_schemes_Id AND publication_schemes_rules.from_date < GETDATE() AND publication_schemes_rules.to_date > GETDATE() AND publication_schemes_rules.active = 1 AND publication_schemes.active = 1 RIGHT OUTER JOIN categories with (nolock) ON publication_schemes.publication_schemes_Id = categories.publication_schemeID WHERE        (categories.lineage LIKE @ParentCatID) ORDER BY categories.depth";

            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                command.Parameters.Add(new SqlParameter("@ParentCatID", "%/" + rootCategoryId + "/%"));
                var reader = command.ExecuteReader();


                var lastDepth = -1;

                DepthListOfCategories curDepthItemList = null;
                var depthList = new List<DepthListOfCategories>();

                var firstDepth = true;
                var lowestDepth = -99;

                try
                {
                    while (reader.Read())
                    {
                        var curDepth = reader["depth"].ChangeTypeTo<int>();
                        if (firstDepth)
                        {
                            firstDepth = false;
                            lowestDepth = curDepth;
                        }

                        if (curDepth != lastDepth)
                        {
                            lastDepth = curDepth;
                            curDepthItemList = DepthListOfCategories.GetOrAddByDepth(curDepth, ref depthList);
                        }

                        if (curDepthItemList != null)
                        {
                            CategoryItem holdCurCatItem;
                            if (curDepth > lowestDepth)
                            {
                                var curParentId = reader["parentID"].ToString();
                                var holdCurParentItem = DepthListOfCategories.GetOrAddByDepth(curDepth - 1, ref depthList).GetCategoryItemFromListByCategoryID(curParentId);
                                var foundParent = false;

                                if (holdCurParentItem == null)
                                {
                                    if (curDepth > lowestDepth + 1)
                                    {
                                        var altParentDepthList = DepthListOfCategories.GetOrAddByDepth(curDepth - 2, ref depthList);
                                        holdCurParentItem = altParentDepthList.GetCategoryItemFromListByCategoryID(curParentId);
                                        foundParent = holdCurParentItem != null;
                                    }
                                }
                                else
                                {
                                    foundParent = true;
                                }

                                if (!foundParent)
                                {
                                    throw new Exception("Parent Category not found! Parent Cat ID: " + curParentId + " CurCatID: " + reader["categoryId"]);
                                }
                                else
                                {
                                    holdCurCatItem = GetNewCategoryItemFromReaderRow(reader, holdCurParentItem, flatListOfAllCleanCategories);
                                    holdCurParentItem.ChildCategories.Add(holdCurCatItem);
                                    curDepthItemList.CategoryItems.Add(holdCurCatItem);
                                }
                            }
                            else
                            {
                                holdCurCatItem = GetNewCategoryItemFromReaderRow(reader, null, flatListOfAllCleanCategories);
                                curDepthItemList.CategoryItems.Add(holdCurCatItem);
                                cleanTree = holdCurCatItem;
                            }
                        }
                        else
                        {
                            throw new Exception("error loading category tree");
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            StripOutItemsBelowDepth(depth, 0, cleanTree);
        }
        #endregion

    }
}