﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Csp.Core.Infrastructure.DAO.Helpers;
using Libcore.Core.Extensions;

namespace Csp.Core.Infrastructure.Publication.Repositories
{
    public abstract class AdoNetRepository
    {
        public readonly string ConnectionString;

        protected AdoNetRepository(string connectionString)
        {
            ConnectionString = connectionString;
        }

        private T GetValue<T>(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            return SqlHelper.ExecuteScalar(ConnectionString, commandType, commandText, parameters).ChangeTypeTo<T>(default(T));
        }

        public T GetValue<T>(string sql, params SqlParameter[] parameters)
        {
            return GetValue<T>(sql, CommandType.Text, parameters);
        }

        public T GetSpValue<T>(string spName, params SqlParameter[] parameters)
        {
            return GetValue<T>(spName, CommandType.StoredProcedure, parameters);
        }

        private IList<T> GetList<T>(string commandText, CommandType commandType, params SqlParameter[] parameters) where T : class, new()
        {
            return CBO.FillCollection<T>(SqlHelper.ExecuteReader(ConnectionString, commandType, commandText, parameters));
        }

        public IList<T> GetList<T>(string sql, params SqlParameter[] parameters) where T : class, new()
        {
            return GetList<T>(sql, CommandType.Text, parameters);
        }

        public IList<T> GetSpList<T>(string spName, params SqlParameter[] parameters) where T : class, new()
        {
            return GetList<T>(spName, CommandType.StoredProcedure, parameters);
        }

        private int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            return SqlHelper.ExecuteNonQuery(ConnectionString, commandType, commandText, parameters);
        }

        public int ExecuteNonQuery(string sql, params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(sql, CommandType.Text, parameters);
        }

        public int ExecuteSpNonQuery(string spName, params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(spName, CommandType.StoredProcedure, parameters);
        }

        public DataTable GetDataTable(string commandText, params SqlParameter[] parameters) 
        {
            try
            {
                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, commandText, parameters).Tables[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IList<SqlParameter> ToSqlParameters(IDictionary<string, object> parameters)
        {
            var list = new List<SqlParameter>();
            if (parameters != null)
            {
                foreach (var key in parameters.Keys)
                {
                    list.Add(new SqlParameter(key, parameters[key]));
                }
            }
            return list;
        }
    }
}