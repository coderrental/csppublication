﻿using System;
using System.Text;
using UrlParse;
using System.Text.RegularExpressions;
using UrlParseNet;

namespace Proxy.Domain
{
    public class TextUtil
    {
        private static byte[] des_key = { 20, 21, 24, 32, 1, 11, 12, 22, 23, 13, 14, 34, 89, 31, 56, 15, 16, 17, 18, 19, 35, 59, 2, 3 };
        private static byte[] des_iv = { 8, 7, 6, 5, 4, 3, 2, 1 };
        public static cTripleDES des = new cTripleDES(des_key, des_iv);

        private static Regex nonAlphaNumeric = new Regex(@"[^0-9a-zA-Z]");

        /**
        * returns str2 if str1 is null or ""
        * otherwise return str1
        */
        public static String getNotEmpty(String str1, String str2)
        {
            if (str1 != null && str1.Length > 0)
                return str1;
            else if (str2 != null && str2.Length > 0)
                return str2;
            else
                return null;
        }

        public static String removeNonAlphaNumeric(String text)
        {
            if (text == null) return null;
            return nonAlphaNumeric.Replace(text, "");
        }
        public static string EncodeJsString(string s)
        {
            if (s == null) return null;
            StringBuilder sb = new StringBuilder();
            //sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            //sb.Append("\"");

            return sb.ToString();
        }
    }
}
