using System;
using System.Collections.Generic;
using Csp.Core.Infrastructure.MapApi.Utilities;

namespace Csp.Core.Infrastructure.Domain
{
    public class NewDomain : DomainUtil
    {
        public NewDomain(string host, string mf,string requestedType, IDictionary<string, string> lngMap)
            : base(host)
        {
            String tmp_lng = null;
            String[] parts = lowest_domain.Split('-');
            foreach (String part in parts)
            {
                String key = part.Substring(0, 1).ToUpper();
                String value = part.Substring(1, part.Length - 1);
                switch (key)
                {
                    case "C": consumer_guid = value; break;
                    case "P": project = value; break;
                    case "L": tmp_lng = value; break;
                    case "I": instanceId = value; break;
                }
            }

            //override from mf parameter
            if (!string.IsNullOrEmpty(mf))
            {
                // if mfrname is specified, then it's a script: mfrname=A&t=B
                project = FindProject(mf, requestedType);
                if (string.IsNullOrEmpty(project))
                    project = mf;
            }

            consumerOptions = getSynDao().getConsumerOptions(BaseDomain);
			if (consumerOptions.Keys.Count == 0 && remaining_domain_no_port.ToLower().StartsWith("urgent."))
			{
				remaining_domain_no_port = remaining_domain_no_port.ToLower().Replace("urgent.", "");
				consumerOptions = getSynDao().getConsumerOptions(BaseDomain);
			}

            if (tmp_lng != null)
            {
                var dao = getSynDao();
                try
                {
                    lng = dao.getLanguage(tmp_lng);
                }
                catch
                {
                    string lngTmp = TextUtil.removeNonAlphaNumeric(tmp_lng).ToLower();
                    if (lngMap!=null&&lngMap.ContainsKey(lngTmp))
                        lng = dao.getLanguage(lngMap[lngTmp]);
                    else
                        lng = dao.getLanguage((String)consumerOptions.GetStringValue("default_language_Id"));
                }
            }
            else
                lng = getSynDao().getLanguage((String)consumerOptions.GetStringValue("default_language_Id"));
        }

        #region Overrides of DomainUtil

        public override string BaseDomain
        {
            get
            {
                return consumer_guid + "." + remaining_domain_no_port;
            }
        }

        #endregion
    }

}