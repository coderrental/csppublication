﻿using System.Collections.Generic;
using Libcore.Core.Extensions;

namespace Csp.Core.Infrastructure.Domain
{
    public class JsonMap : Dictionary<string, object>
    {
        public override string ToString()
        {
            var retStr = "{";
            foreach (var key in Keys)
                if (this[key] is string)
                    retStr += key + ":\"" + this[key] + "\",";
                else if (this[key] is int || this[key] is double)
                    retStr += key + ":" + this[key] + ",";
                else if (this[key] is bool)
                    retStr += key + ":" + ((bool) this[key] ? "true" : "false") + ",";
                else if (this[key] is JsonMap)
                    retStr += key + ":" + (JsonMap) this[key] + ",";
                else if (this[key] is JsonArray)
                    retStr += key + ":" + (JsonArray) this[key] + ",";

            if (retStr.Length > 1) return retStr.Substring(0, retStr.Length - 1) + "}";
            return "{}";
        }

        /**
         * Overloaded method
         */
        public void AddRange(IDictionary<string, string> map)
        {
            foreach (var key in map.Keys)
                Add(key, map[key]);
        }

        public string GetDefaultLanguage()
        {
            return this.GetValueOrDefault("default_language_Id").ChangeTypeTo<string>();
        }

        public T GetValue<T>(string name)
        {
            return this.GetValueOrDefault(name).ChangeTypeTo<T>(default(T));
        }

        public string GetStringValue(string name)
        {
            return GetValue<string>(name);
        }
    }

    public class JsonArray : List<object>
    {
        public override string ToString()
        {
            var retStr = "[";
            foreach (var obj in this)
                if (obj is string)
                    retStr += "\"" + obj + "\",";
                else if (obj is int || obj is double)
                    retStr += obj + ",";
                else if (obj is bool)
                    retStr += ((bool) obj ? "true" : "false") + ",";
                else if (obj is JsonMap)
                    retStr += (JsonMap) obj + ",";
                else if (obj is JsonArray)
                    retStr += (JsonArray) obj + ",";

            if (retStr.Length > 1) return retStr.Substring(0, retStr.Length - 1) + "]";
            return "[]";
        }
    }
}