using System;
using System.Collections.Generic;

namespace Csp.Core.Infrastructure.Domain
{
    public class OldDomain : DomainUtil
    {
        public OldDomain(string host, string mf, string requestedType, IDictionary<string, string> @base)
            : base(host)
        {
            consumer_guid = lowest_domain;

            // find project using mf name and requested type
            project = FindProject(mf, requestedType);

            if (string.IsNullOrEmpty(project))
            {
                project = host.Split('.')[1].ToLower();
                if (!Connections.ContainsKey(project.ToLower()))
                {
                    project = defaultProject;
                }

                //override from mf parameter
                if (!string.IsNullOrEmpty(mf))
                    project = mf;
            }

            consumerOptions = getSynDao().getConsumerOptions(BaseDomain);
			if (consumerOptions.Keys.Count == 0)
			{
				RawUrlWithoutPort = RawUrlWithoutPort.ToLower().Replace(".urgent.", ".");
				consumerOptions = getSynDao().getConsumerOptions(BaseDomain);
			}
            lng = getSynDao().getLanguage((String)consumerOptions.GetStringValue("default_language_Id"));
        }

        #region Overrides of DomainUtil

        public override string BaseDomain
        {
            get
            {
                return RawUrlWithoutPort;
            }
        }

        #endregion
    }
    public class ObsoleteDomain : DomainUtil
    {
        public ObsoleteDomain(string host, string projectName) : base(host)
        {
            project = projectName;
            consumerOptions = getSynDao().getConsumerOptions(RawUrlWithoutPort);
            lng = getSynDao().getLanguage((String)consumerOptions.GetStringValue("default_language_Id"));
        }
        public override string BaseDomain
        {
            get
            {
                return rawUrl;
            }
        }
    }
}