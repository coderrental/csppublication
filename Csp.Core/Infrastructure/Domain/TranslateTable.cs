﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Csp.Core.Infrastructure.DAO;
using Csp.Core.Infrastructure.MapApi.Utilities;
using Csp.Core.Infrastructure.Publication.Repositories;
using Csp.Core.Infrastructure.Security;
using Serilog;

namespace Csp.Core.Infrastructure.Domain
{
    /**
     * The translate table consists of a set of conditions and it's corresponding replacement text
     */
    public class TranslateTable : List<TranslateRow>
    {
        private static readonly ILogger log = Log.Logger;
        private static IDictionary<String, TranslateTable> _translators;
        private static IDictionary<string, string> _connections = new Dictionary<string, string>();
        public static void init(IDictionary<String, String> connections)
        {
            _connections = connections;

            if (_translators == null)
                _translators = new Dictionary<string, TranslateTable>();
			log.Debug($"There are {connections.Count} connections");
            /*
            foreach (String project in connections.Keys)
            {
                // delete before deploy
                //if (project != "motorolasolutions") continue;

                try
                {
                    lock (translators)
                    {
                        if (!translators.ContainsKey(project))
                        {
                            SynDAO dao = new SynDAO(TextUtil.des.Decrypt(connections[project]));
                            log.Debug("Proxy is trying to use this: " + TextUtil.des.Decrypt(connections[project]));
                            TranslateTable translator = new TranslateTable(dao.getTranslatorRows());
                            translators[project] = translator;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("translate table failed, project: "+project);
					log.Error(ex.Message);
					log.Error(ex.StackTrace);
                }
            }
            */
        }

        public static TranslateTable GetTranslator(String project)
        {
            if (!_translators.ContainsKey(project))
            {
                AddTranslator(project);
            }
            return _translators[project];
        }

        private static void AddTranslator(string project)
        {
            if (!_connections.ContainsKey(project))
                throw new Exception($"{project} doesn't exist. Need to refresh the application.");

            lock (_translators)
            {
                if (_translators.ContainsKey(project)) return;

                SynSqlRepository dao = SynSqlRepository.Create(_connections[project]);
                log.Debug("Proxy is trying to use this: " + _connections[project]);
                TranslateTable translator = new TranslateTable(dao.GetTranslatorRows());
                _translators[project] = translator;
            }
        }

        //private constuctor so that TranslateTable can only be created from factory
        private TranslateTable(IDictionary<String, String> rows)
        {
            foreach (String key in rows.Keys)
            {
                this.Add(new TranslateRow(key, rows[key]));
            }
        }

        private TranslateRow getMatchingRow(IDictionary<String, String> terms)
        {
            foreach (TranslateRow row in this)
            {
                try
                {
                    if (row.isMatch(terms))
                        return row;
                }
                catch (Exception)
                {
                    //swallow expection so loop can continue;
                }
            }
            return null;
        }

        public String translate(IDictionary<String, String> terms)
        {
            TranslateRow targetRow = getMatchingRow(terms);
            if (targetRow != null)
                return targetRow.translate(terms);

            return null;
        }
    }

    public class TranslateRow
    {
        private Dictionary<String, Object> conditions;
        private String replacementText;

        private static Regex regex1 = new Regex("{(.*?)}");
        private static Regex regex2 = new Regex("<(.*?)>");

        //constructor
        public TranslateRow(String conditions, String replacementText)
        {
            this.replacementText = replacementText;
            this.conditions = new Dictionary<String, Object>();
            String [] parts = conditions.Split(',');
            foreach (String part in parts)
            {
                String[] pair = part.Split('=');
                String key = pair[0];
                String value = pair[1];
                if (value.StartsWith("/"))
                {
                    //this is a Regex Expression
                    int index = value.LastIndexOf('/');
                    String extra = value.Substring(index, value.Length - index);
                    Regex regex;
                    if (extra.Contains('i'))
                        regex = new Regex(value.Substring(1, index - 1), RegexOptions.IgnoreCase);
                    else
                        regex = new Regex(value.Substring(1, index - 1));

                    this.conditions[key] = regex;
                }
                else
                {
                    this.conditions[key] = value;
                }
            }
        }

        /**
         * Returns replacement text with the tokens filled in
         *      replacement format: ...{...<key>...}...
         *      key tokens will be replaced with key value
         *      null key values will remove contents withing braces
         *      only 1 key per brace set
         *      cannot be nested
         */
        public String translateText(IDictionary<String, String> terms, string text)
        {
            String retText = text;
            Match matches = regex1.Match(retText);
            while (matches.Success)
            {
                String subtextOriginal = matches.Groups[1].Value;
                String subtext = subtextOriginal;
                Match matches2 = regex2.Match(subtext);

                //only 1 match on subtext
                if (matches2.Success)
                {
                    String term = matches2.Groups[1].Value;
                    try
                    {
                        String replacement = terms[term];
                        subtext = subtext.Replace("<" + term + ">", replacement);
                    }
                    catch (KeyNotFoundException)
                    {
                        subtext = "";
                    }
                }

                retText = retText.Replace("{" + subtextOriginal + "}", subtext);
                matches = matches.NextMatch();
            }
            return retText;
        }
        public String translate(IDictionary<String, String> terms)
        {
            return translateText(terms, this.replacementText);
        }

        /**
         * Returns true when terms matches all conditions
         *      "*"         matches anything including null
         *      "/<text>/"  indicates regex expression: new Regex(<text>)
         *      "/<text>/i" indicates regex expression: new Regex(<text>,RegexOptions.CaseInsensitive)
         *      "*<text>"   matches <text> or null
         *      "<text>"    matches <text>
         *      
         * note: regex objects are built at TranslateRow constructor so they can be reused
         */
        public bool isMatch(IDictionary<String, String> terms)
        {
            //terms must be a proper subset of conditions
            foreach(String key in terms.Keys){
                if(!conditions.ContainsKey(key))
                    return false;
            }

            //check each condition
            foreach (String key in conditions.Keys)
            {
                Object expression = conditions[key];
                String value = null;                
                terms.TryGetValue(key,out value);

                if (expression is Regex)
                {
                    if (!((Regex)expression).IsMatch(value))
                        return false;
                }
                else 
                {
                    String expStr = (String)expression;
                    if (expStr == "*")
                    {
                        continue;
                    }
                    else if (expStr.StartsWith("*"))
                    {
                        if (value == null)
                            continue;
                        expStr = expStr.Substring(1);
                    }

                    if (expStr != value)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
