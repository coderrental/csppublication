﻿using System;
using System.Collections;
using System.Collections.Generic;
using Serilog;

namespace Csp.Core.Infrastructure.Domain
{
    public class MapBase<T> : IDictionary<string, T>
    {
        private static readonly ILogger log = Log.Logger;
        private readonly Dictionary<string, T> _dictionary = new Dictionary<string, T>();

        public T this[string key]
        {
            get
            {
                try
                {
                    return _dictionary[key];
                }
                catch (KeyNotFoundException e)
                {
                    log.Error("key [" + key + "] not found in dictionary\n" + e);
                    log.Information($"Available key count: {Count}");
                    foreach (var pair in this)
                        log.Information($"key: [{pair.Key}], value: [{pair.Value}]");
                    //throw e;
                    return default(T);
                }
                catch (ArgumentNullException e)
                {
                    return default(T);
                }
            }
            set => _dictionary[key] = value;
        }

        #region IEnumerable<KeyValuePair<string,T>> Members

        public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion

        public bool HasKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void AddRange(IDictionary<string, T> map)
        {
            foreach (var key in map.Keys)
                _dictionary.Add(key, map[key]);
        }

        public T TryGet(string key)
        {
            var temp = default(T);
            if (key == null)
                return temp;

            _dictionary.TryGetValue(key, out temp);
            return temp;
        }

        #region IDictionary<string,T> Members

        public void Add(string key, T value)
        {
            _dictionary.Add(key, value);
        }

        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public ICollection<string> Keys => _dictionary.Keys;

        public bool Remove(string key)
        {
            return _dictionary.Remove(key);
        }

        public bool TryGetValue(string key, out T value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public ICollection<T> Values => _dictionary.Values;

        #endregion

        #region ICollection<KeyValuePair<string,T>> Members

        public void Add(KeyValuePair<string, T> item)
        {
            _dictionary.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        public bool Contains(KeyValuePair<string, T> item)
        {
            return _dictionary.ContainsKey(item.Key) && _dictionary[item.Key].Equals(item.Value);
        }

        public void CopyTo(KeyValuePair<string, T>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count => _dictionary.Count;

        public bool IsReadOnly => false;

        public bool Remove(KeyValuePair<string, T> item)
        {
            return _dictionary.Remove(item.Key);
        }

        #endregion
    }
}