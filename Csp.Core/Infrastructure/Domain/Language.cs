﻿using System;

namespace Csp.Core.Infrastructure.Domain
{
    public class Language
    {
        public int id;
        public String code;
        public Language(String code, int id)
        {
            this.id = id;
            this.code = code;
        }

        public override bool Equals(object obj)
        {
            if (obj is Language)
            {
                Language _obj = (Language)obj;
                return (id.Equals(_obj.id) && code.Equals(_obj.code));
            }
            return (id.Equals(obj) || code.Equals(obj));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
