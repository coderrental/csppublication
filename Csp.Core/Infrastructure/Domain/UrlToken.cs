namespace Csp.Core.Infrastructure.Domain
{
    public class UrlToken
    {
        public int StartIndex, EndIndex;
        public string Token;
        public string OriginalText;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public UrlToken(int startIndex, int endIndex, string token, string originalText)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
            Token = token;
            OriginalText = originalText;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public UrlToken() : this(-1, -1, string.Empty, string.Empty)
        {
        }

        public static UrlToken GetToken(string text, char openDelimiter, char closeDelimiter,int startIndex)
        {
            UrlToken temp = new UrlToken();
            temp.OriginalText = text;
            temp.StartIndex = text.IndexOf(openDelimiter, startIndex);
            temp.EndIndex = text.IndexOf(closeDelimiter, temp.StartIndex >= 0 ? temp.StartIndex : startIndex);
            if (temp.StartIndex >= 0 && temp.EndIndex >= 0)
                temp.Token = text.Substring(temp.StartIndex, temp.EndIndex - temp.StartIndex + 1);
            else
                temp.Token = string.Empty;

            return string.IsNullOrEmpty(temp.Token) ? null : temp;
        }
    }
}