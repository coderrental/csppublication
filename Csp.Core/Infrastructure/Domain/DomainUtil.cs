using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using CommonServiceLocator;
using Csp.Core.Infrastructure.DAO;
using Csp.Core.Infrastructure.MapApi.Utilities;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Master.Services;
using Libcore.Core.Extensions;
using Serilog;

namespace Csp.Core.Infrastructure.Domain
{
    /**
     * DomainUtil logic
     *      The proxy can handle 3 domain types
     *      The DomainUtil abstracts this layer, so that proxy can access domain infomation in a uniform fashion
     *      OldDomain is for old "standard format" request: consumer_guid.<project>.domain.com
     *      MaskedDomain requires an entry in MaskedDomain table can handle requests from any domain
     *          - project parameter is associated with each mask
     *          - mask can be altered by script to carry additional overriding meta data (project, language, etc)
     *              as a result of the meta data delimeters there cannot be any dashes in the lowest level sub domain
     *      NewDomain uses the new style format to hold request meta data
     *          the lowest level domain is split by dashes
     *          the first character of each part (of the split) indicates the data type (consumer_guid, project, lanaguage, etc)
     *          
     *      each Domain implementation requires a callbackObj method.  This method returns a javascript manipulatable object.
     *          when getURL is called the resulting string can be used for validate requests
     *          this is required so that the domain will not need to be changed when accessing the publication 
     *              thus the session can persist from validate onwards
     */
    public class DomainUtil
    {
        protected IMasterDbService MasterService => ServiceLocator.Current.GetInstance<IMasterDbService>();
        protected IDictionary<string, string> Domains => MasterService.GetDomains();
        protected IDictionary<string, string> Connections => MasterService.GetConnections();

        // public variables
        public string rawUrl;
        public string project;
        public Language lng;
        public string consumer_guid;
        public string lowest_domain;
        public string remaining_domain;
        public string remaining_domain_no_port;
        public string instanceId;
        public JsonMap consumerOptions;
        public string RawUrlWithoutPort;
        internal int Port;

        protected static string defaultProject;
        private SynDAO syndao;

        private static Regex intString = new Regex("^[0-9]+$");
        
        /**
         * returns a new instance of DomainUtil
         * find the best implementation for the given host string
         * 
         * ltu: add another routine to find project based on requested type and manufacturer name
         */
        public static DomainUtil getNewInstance(String host, String mf, string requestedType, IDictionary<string, string> languageMap)
        {
            String lowest_domain = new DomainUtil(host).lowest_domain;

            defaultProject = ConfigurationManager.AppSettings["default_project"];
            if (string.IsNullOrEmpty(defaultProject))
                defaultProject = "lenovo";
            try
            {
                Log.Logger.Information("trying old domain format");
                if (intString.IsMatch(lowest_domain) && lowest_domain.Length == 18)
                    return new OldDomain(host, mf, requestedType, languageMap);
                throw new Exception("Not a valid old domain: " + host);
            }
            catch (Exception ex)
            {
                Log.Logger.Information(ex.Message);
                try
                {
                    Log.Logger.Information("trying new domain format");
                    DomainUtil domainUtil = new NewDomain(host, mf, requestedType, languageMap);
                    if (intString.IsMatch(domainUtil.consumer_guid) && domainUtil.consumer_guid.Length == 18)
                        return domainUtil;
                    throw new Exception("not a valid new domain: " + host);
                }
                catch (Exception ex2)
                {
                    Log.Logger.Information(ex2.Message);
                    Log.Logger.Information("trying masked domain format");
                    try
                    {
                        return new MaskedDomain(host, mf, requestedType, languageMap);
                    }
                    catch (Exception maskDomainEx)
                    {
                        Log.Logger.Error(maskDomainEx, "Error getting mask-domain. ");
                        Log.Logger.Information("trying legacy domain");
                        try
                        {
                            return new LegacyDomain(host, mf, languageMap);
                        }
                        catch (Exception ex3)
                        {
                            Log.Logger.Error(ex3, "Unable to get requested domain. ");
                            return null;
                        }
                    }
                }
            }
        }

        public static DomainUtil getNewInstance(String host, String mf, Dictionary<string,string> languageMap)
        {
            return getNewInstance(host, mf, string.Empty, languageMap);
        }

        public static DomainUtil getNewInstance(string host, string projectName, IDictionary<string, string> requestedType)
        {
            try
            {
                return new ObsoleteDomain(host, projectName);
            }
            catch (Exception err)
            {
                Log.Logger.Error(err, "getNewInstance");
                return null;
            }
        }

        public virtual String BaseDomain
        {
            get
            {
                return consumer_guid + "." + Domains.GetValueOrDefault(project);
            }
        }
        public DomainUtil(String host)
        {
            rawUrl = host;
            //split host
            int index = host.IndexOf('.');
            lowest_domain = host.Substring(0, index);
            remaining_domain = host.Substring(index + 1);

            String[] split = remaining_domain.Split(':');
            remaining_domain_no_port = split[0];

            RawUrlWithoutPort = rawUrl.Split(':')[0];
            try
            {
                Port = split.Length > 1 ? Port = Convert.ToInt32(split[1]) : 80;
            }
            catch (Exception)
            {
                Port = 80;
            }
        }

        public SynDAO getSynDao()
        {
            if (project == null)
                throw new Exception("getSynDao called before project was defined");

            if (syndao == null)
            {
                syndao = new SynDAO(MasterService.GetConnectionString(project));
            }

            return syndao;
        }

        //returns a json that can evaluate the callback url
        public virtual String getCallbackObj()
        {
            return "{l:'" + lng.id + "',i:'" + instanceId + "',p:'" + project + "',q:'',getUrl:function(){return 'c" + consumer_guid + "'+(this.p?'-p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-i'+this.i:'')+'." + remaining_domain + "';}}";
        }

        //c634371797330020649-plenovo-l3-i1.lenovo.coderrental.com:10555
        /// <summary>
        /// 		lowest_domain	"c634371797330020649-plenovo-l3-i1"	string
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public string UpdateUrl(Language language)
        {
            return string.Format("c{0}-p{1}-l{2}-i{3}.{4}", consumer_guid, project, language.id, instanceId, remaining_domain);
        }

        /// <summary>
        /// Find Project Name using mfrname and requested type
        /// </summary>
        /// <param name="mfrName"></param>
        /// <param name="requestedType"></param>
        /// <returns></returns>
        public string FindProject(string mfrName, string requestedType)
        {
            if (!string.IsNullOrEmpty(mfrName) && !string.IsNullOrEmpty(requestedType))
                return MasterService.GetProject(mfrName, requestedType);
            return !string.IsNullOrEmpty(mfrName) ? mfrName : null;
        }
    }
}
