using System;
using System.Collections.Generic;
using Csp.Core.Infrastructure.DAO;
using Csp.Core.Infrastructure.MapApi.Utilities;

namespace Csp.Core.Infrastructure.Domain
{
    public class LegacyDomain : DomainUtil
    {
        private readonly string _part1;
        private readonly string _part2;
        private string _part3;

        //this is the key for Domain mask table and company consumer table
        public override String BaseDomain
        {
            get { return _part2 + "." + remaining_domain_no_port; }
        }

        public LegacyDomain(string host, string mf, IDictionary<string, string> lngMap)
            : base(host)
        {
            int index = lowest_domain.LastIndexOf('-');
            if (index != -1)
            {
                _part1 = lowest_domain.Substring(0, index);
                _part2 = lowest_domain.Substring(index + 1);
            }
            else
            {
                _part1 = "";
                _part2 = lowest_domain;
            }

            
            if (!string.IsNullOrEmpty(_part1))
            {
                SynDAO dao = null; //getSynDao();
                String[] split = _part1.Split('-');
                foreach (String value in split)
                {
                    switch (value.ToUpper().ToCharArray()[0])
                    {
						case 'P':
							project = value.Substring(1);
                    		dao = getSynDao();
							break;
						case 'L':
                            try
                            {
                                lng = dao.getLanguage(value.Substring(1));
                                if (lng.code == null)
                                    throw new Exception();
                            }
                            catch
                            {
                                string lngTmp = TextUtil.removeNonAlphaNumeric(value.Substring(1)).ToLower();
                                if (lngMap != null && lngMap.ContainsKey(lngTmp))
                                    lng = dao.getLanguage(lngMap[lngTmp]);
                                else
                                    lng = dao.getLanguage((String) consumerOptions.GetStringValue("default_language_Id"));
                            }
                            break;
                        default:
                            instanceId = value;
                            break;
                    }
                }
            }
			if (string.IsNullOrEmpty(project))
			{
				project = !string.IsNullOrEmpty(mf) ? mf : MasterService.GetProject(remaining_domain_no_port);

				if (string.IsNullOrEmpty(project))
					throw new Exception("Unable to find domain that matches [" + remaining_domain_no_port + "]");
			}

        	_part3 = _part2 + "." + remaining_domain_no_port;

			if (!getSynDao().isBaseDomain(_part3))
				throw new Exception("Unable to find domain [" + _part2 + remaining_domain_no_port + "] for project [" + project + "]");

			consumerOptions = getSynDao().getConsumerOptions(_part3);

            if (lng == null)
                lng = getSynDao().getLanguage((String) consumerOptions.GetStringValue("default_language_Id"));

        }

        public override String getCallbackObj()
        {
            return "{mask:true,l:'" + lng.id + "',i:'" + instanceId + "',p:'" + project + "',getUrl:function(){return (this.p?'p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-'+this.i:'')+'-" + _part2 + "." + remaining_domain + "'}}";
        }
    }
}