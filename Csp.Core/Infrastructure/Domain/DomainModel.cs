using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using CommonServiceLocator;
using Csp.Core.Infrastructure.MapApi.Utilities;
using Csp.Core.Infrastructure.Publication.Repositories;
using Csp.Core.Master.Services;
using Libcore.Core.Extensions;
using Serilog;

namespace Csp.Core.Infrastructure.Domain
{
    public static class DomainFactory
    {
        public static DomainBase Get(string host, string mf = null, string requestedType = null)
        {
            var intString = new Regex("^[0-9]+$");

            var masterService = ServiceLocator.Current.GetInstance<IMasterDbService>();
            try
            {
                Log.Logger.Information("trying old domain format");
                var domainUtil = new OldDomainImpl(host, masterService, mf, requestedType);
                if (intString.IsMatch(domainUtil.LowestDomain) && domainUtil.LowestDomain.Length == 18)
                    return domainUtil;
                throw new Exception("Not a valid old domain: " + host);
            }
            catch (Exception ex)
            {
                Log.Logger.Information(ex.Message);
                try
                {
                    Log.Logger.Information("trying new domain format");
                    var domainUtil = new NewDomainImpl(host, masterService,mf, requestedType);
                    if (intString.IsMatch(domainUtil.ConsumerGuid) && domainUtil.ConsumerGuid.Length == 18)
                        return domainUtil;
                    throw new Exception("not a valid new domain: " + host);
                }
                catch (Exception ex2)
                {
                    Log.Logger.Information(ex2.Message);
                    Log.Logger.Information("trying masked domain format");
                    try
                    {
                        return new MaskedDomainImpl(host, masterService, mf, requestedType);
                    }
                    catch (Exception maskDomainEx)
                    {
                        Log.Logger.Error(maskDomainEx, "Error getting mask-domain. ");
                        Log.Logger.Information("trying legacy domain");
                        try
                        {
                            return new LegacyDomainImpl(host, masterService, mf);
                        }
                        catch (Exception ex3)
                        {
                            Log.Logger.Error(ex3, "Unable to get requested domain. ");
                            return null;
                        }
                    }
                }
            }
        }
    }

    public abstract class DomainBase
    {
        protected readonly IMasterDbService Service;
        protected IDictionary<string, string> Domains => Service.GetDomains();
        protected IDictionary<string, string> Connections => Service.GetConnections();
        protected IDictionary<string, string> LanguageMaps => Service.GetLanguageMap();

        private SynSqlRepository _synRepo;

        public string Project { get; set; }
        public string RawUrl { get; set; }
        public string RawUrlWithoutPort { get; set; }
        public string RemainingDomain { get; set; }
        public string LowestDomain { get; set; }
        public string InstanceId { get; set; }
        public string ConsumerGuid { get; set; }
        public string RemainingDomainNoPort { get; set; }
        internal int Port { get; set; }
        public Language Language { get; set; }
        public JsonMap ConsumerOptions { get; set; }

        public virtual string BaseDomain => ConsumerGuid + "." + Domains.GetValueOrDefault(Project);

        protected DomainBase(string host, IMasterDbService service)
        {
            Service = service;
            RawUrl = host;
            //split host
            var index = host.IndexOf('.');
            LowestDomain = host.Substring(0, index);
            RemainingDomain = host.Substring(index + 1);

            var split = RemainingDomain.Split(':');
            RemainingDomainNoPort = split[0];

            RawUrlWithoutPort = RawUrl.Split(':')[0];
            try
            {
                Port = split.Length > 1 ? Port = Convert.ToInt32(split[1]) : 80;
            }
            catch (Exception)
            {
                Port = 80;
            }
        }

        public string UpdateUrl(Language language)
        {
            return $"c{ConsumerGuid}-p{Project}-l{language.id}-i{InstanceId}.{RemainingDomain}";
        }

        protected SynSqlRepository GetSynRepo()
        {
            if (string.IsNullOrEmpty(Project))
            {
                throw new Exception("GetSynRepo called before project was defined");
            }

            return _synRepo ?? (_synRepo = SynSqlRepository.Create(Service.GetConnectionString(Project)));
        }

        public virtual string GetCallbackObj()
        {
            return "{l:'" + Language.id + "',i:'" + InstanceId + "',p:'" + Project +
                   "',q:'',getUrl:function(){return 'c" + ConsumerGuid +
                   "'+(this.p?'-p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-i'+this.i:'')+'." + RemainingDomain +
                   "';}}";
        }

        protected string FindProject(string mfrName, string requestedType)
        {
            if (!string.IsNullOrEmpty(mfrName) && !string.IsNullOrEmpty(requestedType))
                return Service.GetProject(mfrName, requestedType);
            return !string.IsNullOrEmpty(mfrName) ? mfrName : null;
        }

        protected string GetDefaultProject()
        {
            var defaultProject = ConfigurationManager.AppSettings["default_project"];
            if (string.IsNullOrEmpty(defaultProject))
                defaultProject = "lenovo";

            return defaultProject;
        }
    }

    public sealed class OldDomainImpl : DomainBase
    {
        public OldDomainImpl(string host, IMasterDbService service, string mf, string requestedType) : base(host, service)
        {
            ConsumerGuid = LowestDomain;

            // find project using mf name and requested type
            Project = FindProject(mf, requestedType);

            if (string.IsNullOrEmpty(Project))
            {
                Project = host.Split('.')[1].ToLower();
                if (!Connections.ContainsKey(Project.ToLower()))
                {
                    Project = GetDefaultProject();
                }

                //override from mf parameter
                if (!string.IsNullOrEmpty(mf))
                    Project = mf;
            }

            ConsumerOptions = GetSynRepo().GetConsumerOptions(BaseDomain);
            if (ConsumerOptions.Keys.Count == 0)
            {
                RawUrlWithoutPort = RawUrlWithoutPort.ToLower().Replace(".urgent.", ".");
                ConsumerOptions = GetSynRepo().GetConsumerOptions(BaseDomain);
            }
            Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
        }
    }

    public sealed class NewDomainImpl : DomainBase
    {
        public override string BaseDomain => ConsumerGuid + "." + RemainingDomainNoPort;

        public NewDomainImpl(string host, IMasterDbService service, string mf, string requestedType) : base(host, service)
        {
            string tempLanguage = null;
            var parts = LowestDomain.Split('-');
            foreach (var part in parts)
            {
                var key = part.Substring(0, 1).ToUpper();
                var value = part.Substring(1, part.Length - 1);
                switch (key)
                {
                    case "C":
                        ConsumerGuid = value;
                        break;
                    case "P":
                        Project = value;
                        break;
                    case "L":
                        tempLanguage = value;
                        break;
                    case "I":
                        InstanceId = value;
                        break;
                }
            }

            //override from mf parameter
            if (!string.IsNullOrEmpty(mf))
            {
                // if mfrname is specified, then it's a script: mfrname=A&t=B
                Project = FindProject(mf, requestedType);
                if (string.IsNullOrEmpty(Project))
                    Project = mf;
            }

            ConsumerOptions = GetSynRepo().GetConsumerOptions(BaseDomain);
            if (ConsumerOptions.Keys.Count == 0 && RemainingDomainNoPort.ToLower().StartsWith("urgent."))
            {
                RemainingDomainNoPort = RemainingDomainNoPort.ToLower().Replace("urgent.", "");
                ConsumerOptions = GetSynRepo().GetConsumerOptions(BaseDomain);
            }

            if (tempLanguage != null)
            {
                try
                {
                    Language = GetSynRepo().GetLanguage(tempLanguage);
                }
                catch
                {
                    string lngTmp = TextUtil.removeNonAlphaNumeric(tempLanguage).ToLower();
                    if (LanguageMaps != null && LanguageMaps.ContainsKey(lngTmp))
                        Language = GetSynRepo().GetLanguage(LanguageMaps[lngTmp]);
                    else
                        Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
                }
            }
            else
                Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
        }
    }

    public sealed class MaskedDomainImpl : DomainBase
    {
        private readonly string _part2;

        //this is the key for Domain mask table and company consumer table
        public override string BaseDomain => _part2 + "." + RemainingDomainNoPort;

        public MaskedDomainImpl(string host, IMasterDbService service, string mf, string requestedType) : base(host, service)
        {
            //l2-test.microsoft.coderrental.com
            // we want to check to see if the requested host is in the domain mask.

            Project = Service.GetDomainMask(RawUrlWithoutPort);

            if (Project == null && LowestDomain.IndexOf("-") > 0)
            {
                var subDomain = LowestDomain.Substring(LowestDomain.LastIndexOf('-') + 1);
                Project = Service.GetDomainMask(subDomain + "." + RemainingDomainNoPort);
            }
            
            //retrieve masked data
            if (GetSynRepo().IsBaseDomain(RawUrlWithoutPort))
            {
                ConsumerOptions = GetSynRepo().GetConsumerOptions(RawUrlWithoutPort);
                _part2 = LowestDomain;
            }
            else
            {
                var index = LowestDomain.LastIndexOf('-');
                var part1 = LowestDomain;
                if (index != -1)
                {
                    part1 = LowestDomain.Substring(0, index);
                    _part2 = LowestDomain.Substring(index + 1);
                }

                //get data from part2
                if (!string.IsNullOrEmpty(part1))
                {
                    var split = part1.Split('-').Select(x => x.ToUpper());
                    foreach (var value in split)
                    {
                        switch (value[0])
                        {
                            case 'L':
                                try
                                {
                                    //Always CreateSynRepo
                                    Language = GetSynRepo().GetLanguage(value.Substring(1));
                                    if (Language.code == null)
                                        throw new Exception();
                                }
                                catch
                                {
                                    // Old repo
                                    var lngTmp = TextUtil.removeNonAlphaNumeric(value.Substring(1)).ToLower();
                                    if (LanguageMaps != null && LanguageMaps.ContainsKey(lngTmp))
                                        Language = GetSynRepo().GetLanguage(LanguageMaps[lngTmp]);
                                    else
                                        Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
                                }
                                break;
                            case 'P':
                                Project = value.Substring(1);
                                break;
                            default:
                                InstanceId = value;
                                break;

                        }
                    }
                }

                //override from mf parameter
                if (!string.IsNullOrEmpty(mf))
                {
                    // if mfrname is specified, then it's a script: mfrname=A&t=B
                    Project = FindProject(mf, requestedType);
                    if (string.IsNullOrEmpty(Project))
                        Project = mf;
                }

                ConsumerOptions = GetSynRepo().GetConsumerOptions(BaseDomain);
            }

            if (Language == null)
            {
                var defaultLanguage = ConsumerOptions.GetDefaultLanguage();
                if (defaultLanguage != null)
                    Language = GetSynRepo().GetLanguage(defaultLanguage);
            }
        }

        public override string GetCallbackObj()
        {
            base.GetCallbackObj();
            return "{mask:true,l:'" + Language.id + "',i:'" + InstanceId + "',p:'" + Project + "',getUrl:function(){return (this.p?'p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-'+this.i:'')+'-" + _part2 + "." + RemainingDomain + "'}}";
        }
    }

    public class LegacyDomainImpl : DomainBase
    {
        private readonly string _part2;

        //this is the key for Domain mask table and company consumer table
        public override string BaseDomain => _part2 + "." + RemainingDomainNoPort;

        public LegacyDomainImpl(string host, IMasterDbService service, string mf) : base(host, service)
        {
            string part1;
            var index = LowestDomain.LastIndexOf('-');
            if (index != -1)
            {
                part1 = LowestDomain.Substring(0, index);
                _part2 = LowestDomain.Substring(index + 1);
            }
            else
            {
                part1 = "";
                _part2 = LowestDomain;
            }

            if (!string.IsNullOrEmpty(part1))
            {
                var split = part1.Split('-');
                foreach (var value in split)
                {
                    switch (value.ToUpper().ToCharArray()[0])
                    {
                        case 'P':
                            Project = value.Substring(1);
                            break;
                        case 'L':
                            try
                            {
                                Language = GetSynRepo().GetLanguage(value.Substring(1));
                                if (Language.code == null)
                                    throw new Exception();
                            }
                            catch
                            {
                                string lngTmp = TextUtil.removeNonAlphaNumeric(value.Substring(1)).ToLower();
                                if (LanguageMaps != null && LanguageMaps.ContainsKey(lngTmp))
                                    Language = GetSynRepo().GetLanguage(LanguageMaps[lngTmp]);
                                else
                                    Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
                            }
                            break;
                        default:
                            InstanceId = value;
                            break;
                    }
                }
            }
            if (string.IsNullOrEmpty(Project))
            {
                Project = !string.IsNullOrEmpty(mf) ? mf : Service.GetProject(RemainingDomainNoPort);

                if (string.IsNullOrEmpty(Project))
                    throw new Exception("Unable to find domain that matches [" + RemainingDomainNoPort + "]");
            }

            var part3 = _part2 + "." + RemainingDomainNoPort;

            if (!GetSynRepo().IsBaseDomain(part3))
                throw new Exception("Unable to find domain [" + _part2 + RemainingDomainNoPort + "] for project [" + Project + "]");

            ConsumerOptions = GetSynRepo().GetConsumerOptions(part3);

            if (Language == null)
                Language = GetSynRepo().GetLanguage(ConsumerOptions.GetDefaultLanguage());
        }

        public override string GetCallbackObj()
        {
            return "{mask:true,l:'" + Language.id + "',i:'" + InstanceId + "',p:'" + Project + "',getUrl:function(){return (this.p?'p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-'+this.i:'')+'-" + _part2 + "." + RemainingDomain + "'}}";
        }
    }
}