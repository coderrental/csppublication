using System;
using System.Collections.Generic;
using Csp.Core.Infrastructure.MapApi.Utilities;
using Libcore.Core.Extensions;

namespace Csp.Core.Infrastructure.Domain
{
    public class MaskedDomain : DomainUtil
    {
        private string part1, part2;

        //this is the key for Domain mask table and company consumer table
        public override String BaseDomain
        {
            get { return part2 + "." + remaining_domain_no_port; }
        }

        public MaskedDomain(string host, string mf, string requestedType, IDictionary<string, string> lngMap)
            : base(host)
        {
            //l2-test.microsoft.coderrental.com
            // we want to check to see if the requested host is in the domain mask.
            project = MasterService.GetDomainMask(RawUrlWithoutPort);

            if (project == null && lowest_domain.IndexOf("-") > 0)
            {
                string subDomain = lowest_domain.Substring(lowest_domain.LastIndexOf('-') + 1);
                project = MasterService.GetDomainMask(subDomain + "." + remaining_domain_no_port);
            }

            //retrieve masked data
            if (getSynDao().isBaseDomain(RawUrlWithoutPort))
            {
                consumerOptions = getSynDao().getConsumerOptions(RawUrlWithoutPort);
                part2 = lowest_domain;
            }
            else
            {
                int index = lowest_domain.LastIndexOf('-');
                if (index != -1)
                {
                    part1 = lowest_domain.Substring(0, index);
                    part2 = lowest_domain.Substring(index + 1);
                }
                else
                {
                    part1 = lowest_domain;
                    part2 = null;
                }

                //get data from part2
                if (!string.IsNullOrEmpty(part1))
                {
                    var dao = getSynDao();
                    String[] split = part1.Split('-');
                    foreach (String value in split)
                    {
                        switch (value.ToUpper().ToCharArray()[0])
                        {
                            case 'L':
                                try
                                {
                                    lng = getSynDao().getLanguage(value.Substring(1));
                                    if (lng.code == null)
                                        throw new Exception();
                                }
                                catch
                                {
                                    string lngTmp = TextUtil.removeNonAlphaNumeric(value.Substring(1)).ToLower();
                                    if (lngMap != null && lngMap.ContainsKey(lngTmp))
                                        lng = dao.getLanguage(lngMap[lngTmp]);
                                    else
                                        lng = dao.getLanguage((String)consumerOptions.GetStringValue("default_language_Id"));
                                }
                                break;
                            case 'P':
                                project = value.Substring(1);
                                break;
                            default:
                                instanceId = value;
                                break;

                        }
                    }
                }

                //override from mf parameter
                if (!string.IsNullOrEmpty(mf))
                {
                    // if mfrname is specified, then it's a script: mfrname=A&t=B
                    project = FindProject(mf, requestedType);
                    if (string.IsNullOrEmpty(project))
                        project = mf;
                }

                consumerOptions = getSynDao().getConsumerOptions(BaseDomain);
            }

            if (lng == null)
            {
                if (consumerOptions.GetValueOrDefault("default_language_Id") != null)
                    lng = getSynDao().getLanguage((String) consumerOptions.GetStringValue("default_language_Id"));
                else
                {
                    //lng = getSynDao().getLanguage()
                }
            }
        }

        //returns a json that can evaluate the callback url
        public override String getCallbackObj()
        {
            return "{mask:true,l:'" + lng.id + "',i:'" + instanceId + "',p:'" + project + "',getUrl:function(){return (this.p?'p'+this.p:'')+(this.l?'-l'+this.l:'')+(this.i?'-'+this.i:'')+'-" + part2 + "." + remaining_domain + "'}}";
        }
    }
}