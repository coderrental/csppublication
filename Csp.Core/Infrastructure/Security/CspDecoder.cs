namespace Csp.Core.Infrastructure.Security
{
    public static class CspDecoder
    {
        public static readonly byte[] DesKey =
            {20, 21, 24, 32, 1, 11, 12, 22, 23, 13, 14, 34, 89, 31, 56, 15, 16, 17, 18, 19, 35, 59, 2, 3};

        public static readonly byte[] DesIv = {8, 7, 6, 5, 4, 3, 2, 1};
        private static readonly TripleDes Des = new TripleDes(DesKey, DesIv);

        public static string Decode(string encodedString)
        {
            return Des.Decrypt(encodedString);
        }
    }
}