﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Csp.Core.Infrastructure.Security
{
    internal class TripleDes
    {
        private readonly TripleDESCryptoServiceProvider _des = new TripleDESCryptoServiceProvider();
        private readonly byte[] _mIv;
        private readonly byte[] _mKey;
        private readonly UTF8Encoding _utf8 = new UTF8Encoding();

        public TripleDes(byte[] mKey, byte[] mIv)
        {
            _mKey = mKey;
            _mIv = mIv;
        }

        public TripleDes()
        {
            _mKey = CspDecoder.DesKey;
            _mIv = CspDecoder.DesIv;
        }

        public byte[] Encrypt(byte[] input)
        {
            return Transform(input, _des.CreateEncryptor(_mKey, _mIv));
        }

        public byte[] Decrypt(byte[] input)
        {
            return Transform(input, _des.CreateDecryptor(_mKey, _mIv));
        }

        public string Encrypt(string text)
        {
            var input = _utf8.GetBytes(text);
            var output = Transform(input, _des.CreateEncryptor(_mKey, _mIv));
            return Convert.ToBase64String(output);
        }

        public string Decrypt(string text)
        {
            var input = Convert.FromBase64String(text);
            var output = Transform(input, _des.CreateDecryptor(_mKey, _mIv));
            return _utf8.GetString(output);
        }

        private static byte[] Transform(byte[] input, ICryptoTransform CryptoTransform)
        {
            //Create the necessary streams
            var memStream = new MemoryStream();
            var cryptStream = new CryptoStream(memStream, CryptoTransform, CryptoStreamMode.Write);

            // Transform the bytes as requested
            cryptStream.Write(input, 0, input.Length);
            cryptStream.FlushFinalBlock();

            // Read the memory stream and convert it back into byte array
            memStream.Position = 0;
            var result = new byte[memStream.Length];
            memStream.Read(result, 0, result.Length);

            // Close and release the streams
            memStream.Close();
            cryptStream.Close();

            // Hand back the encrypted buffer
            return result;
        }
    }
}