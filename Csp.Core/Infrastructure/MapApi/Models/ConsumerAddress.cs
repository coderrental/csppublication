namespace Csp.Core.Infrastructure.MapApi.Models
{
    public class ConsumerAddress
    {
        public string Address, FormatedAddress;
        public int Id;
        public float Latitude, Longtitude;
    }
}