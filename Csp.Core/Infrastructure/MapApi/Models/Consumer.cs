﻿namespace Csp.Core.Infrastructure.MapApi.Models
{
    public class Consumer
    {
        public int Id;
        public float Latitude, Longitude;
        public string Name, Address1, Address2, City, Zipcode, State, Country, Website, LongAddress;
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DeepLinkUrl { get; set; }

        #region Overrides of Object

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2} {3}, {4}", Address1, City, State, Zipcode, Country);
        }

        #endregion
    }
}