using System.Text;
using System.Text.RegularExpressions;

namespace Csp.Core.Infrastructure.MapApi.Utilities
{
    public static class TextUtil
    {
        private static readonly Regex nonAlphaNumeric = new Regex(@"[^0-9a-zA-Z]");

        /**
        * returns str2 if str1 is null or ""
        * otherwise return str1
        */
        public static string getNotEmpty(string str1, string str2)
        {
            if (!string.IsNullOrEmpty(str1))
                return str1;
            if (!string.IsNullOrEmpty(str2))
                return str2;
            return null;
        }

        public static string removeNonAlphaNumeric(string text)
        {
            if (text == null) return null;
            return nonAlphaNumeric.Replace(text, "");
        }

        public static string EncodeJsString(string s)
        {
            if (s == null) return null;
            var sb = new StringBuilder();
            //sb.Append("\"");
            foreach (var c in s)
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = c;
                        if (i < 32 || i > 127)
                            sb.AppendFormat("\\u{0:X04}", i);
                        else
                            sb.Append(c);
                        break;
                }

            return sb.ToString();
        }
    }
}