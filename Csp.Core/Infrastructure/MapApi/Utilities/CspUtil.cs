﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Csp.Core.Infrastructure.MapApi.Models;
using Csp.Core.Infrastructure.Security;
using Serilog;

namespace Csp.Core.Infrastructure.MapApi.Utilities
{
    public class CspUtil
    {
        private static readonly string GetProjectQuery =
            "select top 1 Connection from Connections where Project = @project and Mode = @mode";

        private static readonly string GetAllConsumerQuery = @"
select *
from (
select a.companies_Id as Id, a.companyname as Name,a.address1 as Address, a.city as City,a.state as State, a.zipcode as Zipcode, a.country as Country, a.website_url as Website, c.parametername, cast(b.value_text as nvarchar(max)) as v
from companies a
	left join companies_parameters b on a.companies_Id = b.companies_Id
	left join companies_parameter_types c on b.companies_parameter_types_Id = c.companies_parameter_types_Id
	left join companies_consumers d on d.companies_Id = a.companies_Id
)as data
pivot
(
	max(v) FOR parametername in ([ConsumerType], [Latitude], [Longitude], [Personalize_Contact_Emailaddress],[Personalize_Contact_Phonenumber],[Deeplink_Landingpage_Url])
)as pdata

where (pdata.ConsumerType is not null and pdata.ConsumerType <> 't') or (pdata.ConsumerType is null)

";

        private string _cspConnectionString, _submitQuery;
        private string _project = "", _mode = "";

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public CspUtil()
        {
            _project = "";
        }

        public List<Consumer> GetAllConsumers(bool refreshAll)
        {
            Log.Logger.Information("Get All Consumer [Refresh All: {0}]", refreshAll);
            SqlConnection c = null;
            SqlTransaction t = null;
            var consumers = new List<Consumer>();
            using (var cmd = GetCommand(GetAllConsumerQuery, true, out c, out t))
            {
                var reader = cmd.ExecuteReader();
                float lat, lng;
                while (reader.Read())
                    try
                    {
                        lat = DBNull.Value.Equals(reader[9]) ? 0 : Convert.ToSingle(reader[9]);
                        lng = DBNull.Value.Equals(reader[10]) ? 0 : Convert.ToSingle(reader[10]);
                        if (!refreshAll && (lat != 0 || lng != 0)) continue;
                        consumers.Add(new Consumer
                        {
                            Id = DBNull.Value.Equals(reader[0]) ? -1 : reader.GetInt32(0),
                            Name = DBNull.Value.Equals(reader[1]) ? "" : reader.GetString(1),
                            Address1 = DBNull.Value.Equals(reader[2]) ? "" : reader.GetString(2),
                            City = DBNull.Value.Equals(reader[3]) ? "" : reader.GetString(3),
                            State = DBNull.Value.Equals(reader[4]) ? "" : reader.GetString(4),
                            Zipcode = DBNull.Value.Equals(reader[5]) ? "" : reader.GetString(5),
                            Country = DBNull.Value.Equals(reader[6]) ? "" : reader.GetString(6),
                            Website = DBNull.Value.Equals(reader[7]) ? "" : reader.GetString(7),
                            //skip the consumer type (8)
                            Latitude = lat,
                            Longitude = lng,
                            Email = DBNull.Value.Equals(reader[11]) ? "" : reader.GetString(11),
                            PhoneNumber = DBNull.Value.Equals(reader[12]) ? "" : reader.GetString(12),
                            DeepLinkUrl = DBNull.Value.Equals(reader[13]) ? "" : reader.GetString(13)
                        });
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.Error(ex, $"{ex.Message}. Id: {reader[0]}");
                    }
                if (reader != null) reader.Close();
            }
            if (t != null) t.Commit();
            if (c != null) c.Close();
            return consumers;
        }

        private SqlCommand GetCommand(string query, bool IsRead, out SqlConnection connection,
            out SqlTransaction transaction)
        {
            SqlCommand command = null;
            try
            {
                connection = new SqlConnection(_cspConnectionString);
                connection.Open();
                transaction =
                    connection.BeginTransaction(IsRead ? IsolationLevel.ReadUncommitted : IsolationLevel.ReadCommitted);
                command = new SqlCommand(query, connection, transaction);
            }
            catch (Exception ex)
            {
                connection = null;
                transaction = null;
                Log.Error(ex.Message);
            }
            return command;
        }

        public void UpdateLocation(Consumer consumer)
        {
            if (string.IsNullOrEmpty(_submitQuery))
                _submitQuery = @"
declare @latId as int, @lngId as int, @tmpId int
select @latId = a.companies_parameter_types_Id from companies_parameter_types a where a.parametername='Latitude'
if @latId is null 
begin
	insert into companies_parameter_types(parametername,parametertype,sortorder,IsExport) values('Latitude', 1, 99, 0)
	select @latId = @@IDENTITY
end	
select @lngId = a.companies_parameter_types_Id from companies_parameter_types a where a.parametername='Longitude'
if @lngId is null 
begin
	insert into companies_parameter_types(parametername,parametertype,sortorder,IsExport) values('Longitude', 1, 99, 0)
	select @lngId = @@IDENTITY
end	
if @latId is not null and @lngId is not null 
begin
";
            if (consumer.Longitude != 0 && consumer.Latitude != 0)
                _submitQuery += string.Format(@"
	if exists (select * from companies_parameters a where a.companies_parameter_types_Id = @latId and a.companies_Id = {0})
		update companies_parameters set value_text = '{1}' where companies_parameter_types_Id = @latId and companies_Id = {0}
	else
		insert into companies_parameters(companies_Id,companies_parameter_types_Id,value_text) values ({0},@latId, '{1}')
	if exists (select * from companies_parameters a where a.companies_parameter_types_Id = @lngId and a.companies_Id = {0})
		update companies_parameters set value_text = '{2}' where companies_parameter_types_Id = @lngId and companies_Id = {0}
	else
		insert into companies_parameters(companies_Id,companies_parameter_types_Id,value_text) values ({0},@lngId, '{2}')			
", consumer.Id, consumer.Latitude, consumer.Longitude);
        }

        public bool Init(string m, string p)
        {
            var flag = true;
            string cspCs = "", cs = "";
            _project = p;
            _mode = m;
            SqlConnection con = null;
            try
            {
                if (ConfigurationManager.ConnectionStrings[m + ".master"] == null)
                    throw new Exception(m + ".master connection string isnt available.");
                cs = CspDecoder.Decode(ConfigurationManager.ConnectionStrings[m + ".master"].ConnectionString);
                con = new SqlConnection(cs);
                con.Open();
                using (var cmd = new SqlCommand(GetProjectQuery, con))
                {
                    cmd.Parameters.AddWithValue("@project", p);
                    cmd.Parameters.AddWithValue("@mode", m);
                    cspCs = (string) cmd.ExecuteScalar();
                }

                if (!string.IsNullOrEmpty(cspCs))
                    cspCs = CspDecoder.Decode(cspCs);
            }
            catch (Exception ex)
            {
                flag = false;
                Log.Error(ex.Message);
            }
            finally
            {
                if (con != null) con.Close();
            }
            _cspConnectionString = cspCs;
            return flag;
        }

        public void Submit()
        {
            if (string.IsNullOrEmpty(_submitQuery)) return;
            _submitQuery += "end";
            Log.Debug(_submitQuery);
            SqlConnection c = null;
            SqlTransaction t = null;
            try
            {
                using (var cmd = GetCommand(_submitQuery, false, out c, out t))
                {
                    Log.Logger.Debug($"Commited {cmd.ExecuteNonQuery()} records");
                }
                t.Commit();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (t != null) t.Rollback();
            }
            finally
            {
                if (c != null) c.Close();
            }
        }
    }
}