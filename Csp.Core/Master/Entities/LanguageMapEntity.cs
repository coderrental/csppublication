﻿using Libcore.DataAccess.Entities;

namespace Csp.Core.Master.Entities
{
    public class LanguageMapEntity : EntityBase
    {
        public string LanguageCode { get; set; }
        public string CspLanguageCode { get; set; }
    }
}