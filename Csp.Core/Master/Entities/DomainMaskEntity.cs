﻿using Libcore.DataAccess.Entities;

namespace Csp.Core.Master.Entities
{
    public class DomainMaskEntity : Entity<int>
    {
        public string Mask { get; set; }
        public string Project { get; set; }
    }
}
