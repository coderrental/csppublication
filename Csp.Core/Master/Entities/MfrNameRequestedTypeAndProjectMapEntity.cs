﻿using Libcore.DataAccess.Entities;

namespace Csp.Core.Master.Entities
{
    public class MfrNameRequestedTypeAndProjectMapEntity : EntityBase
    {
        public string Project { get; set; }
        public string ManufacturerName { get; set; }
        public string RequestedType { get; set; }
    }
}
