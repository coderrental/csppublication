﻿using Libcore.DataAccess.Entities;

namespace Csp.Core.Master.Entities
{
    public class AliasEntity : Entity<int>
    {
        public string Alias { get; set; }
        public string Value { get; set; }
    }
}