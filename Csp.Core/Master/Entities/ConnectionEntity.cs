﻿using Libcore.DataAccess.Entities;

namespace Csp.Core.Master.Entities
{
    public class ConnectionEntity : Entity<int>
    {
        public string Mode { get; set; }
        public string Project { get; set; }
        public string Connection { get; set; }
        public string Domain { get; set; }
    }
}
