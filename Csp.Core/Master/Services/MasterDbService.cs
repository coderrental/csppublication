﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using Csp.Core.Extensions;
using Csp.Core.Infrastructure.Domain;
using Csp.Core.Infrastructure.Publication.Models;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Master.Context;
using Csp.Core.Master.Entities;
using Csp.Core.Master.Repositories;
using Libcore.Core.Configuration;
using Libcore.Core.Extensions;
using Libcore.DataAccess.Extensions;
using Libcore.DataAccess.Uow;

namespace Csp.Core.Master.Services
{
    public interface IMasterDbService
    {
        string GetConnectionString();
        IDictionary<string, string> GetLanguageMap();
        IDictionary<string, string> GetAliasMap();
        IDictionary<string, string> GetConnections();
        IDictionary<string, string> GetDomains();
        AliasCollection GetCtAliasAssociationCollection();
        string GetProject(string mfrName, string requestedType);
        string GetProject(string domain);
        string GetContentCache(string url, string languageCode, int daysToCache);
        int ClearCache(string url);
        bool InsertToCache(string requestedUrl, string project, string syndicationType, string requestType, string text, int daysToCache, string languageCode);
        string GetDomainMask(string mask);
        string GetConnectionString(string project);
    }

    public class MasterDbService : IMasterDbService
    {
        private readonly IUnitOfWork<MasterEfContext> _unitOfWork;
        private readonly ISettingManager _settingManager;
        private readonly HttpContextBase _httpContext;
        private readonly MemoryCache _cache;

        private MasterSqlRepository SqlRepo => _unitOfWork.ExtendedRepo<MasterSqlRepository>();

        public MasterDbService(IUnitOfWork<MasterEfContext> unitOfWork, ISettingManager settingManager, HttpContextBase httpContext)
        {
            _unitOfWork = unitOfWork;
            _settingManager = settingManager;
            _httpContext = httpContext;
            _cache = MemoryCache.Default;
        }

        public string GetConnectionString()
        {
            var request = _httpContext.Request;
            var host = request.Headers["Host"] ?? request.Url?.Host;

            var aliasMap = GetAliasMap();

            var mfrname = request.GetQueryString("mfrname").ToLower().Trim();
            var aliasLookup = CspExtensions.GetNotEmpty(aliasMap.GetValueOrDefault(mfrname), mfrname);
            var domainUtil = DomainFactory.Get(host, aliasLookup, request.GetQueryString("t"));

            return GetConnectionString(domainUtil.Project);
        }

        public string GetConnectionString(string project)
        {
            return GetConnections()[project];
        }

        public IDictionary<string, string> GetLanguageMap()
        {
            return _cache.GetOrAdd("_MasterDbService.GetLanguageMap",
                () =>
                {
                    return _unitOfWork.Repo<LanguageMapEntity>().AsNoTracking()
                        .ToList()
                        .ToDictionary(x => x.LanguageCode, x => x.CspLanguageCode, StringComparer.OrdinalIgnoreCase);
                });
        }

        public IDictionary<string, string> GetAliasMap()
        {
            return _cache.GetOrAdd("_MasterDbService.GetAliasMap",
                () =>
                {
                    return _unitOfWork.Repo<AliasEntity>().AsNoTracking()
                        .ToList()
                        .ToSafeDictionary(x => x.Alias, x => x.Value, StringComparer.OrdinalIgnoreCase);
                });
        }

        public IDictionary<string, string> GetConnections()
        {
            return _cache.GetOrAdd("_MasterDbService.GetConnections",
                () =>
                {
                    var mode = _settingManager.Mode();
                    return _unitOfWork.Repo<ConnectionEntity>().AsNoTracking()
                                      .Where(x => x.Mode != "notused" && x.Mode == mode)
                                      .ToList()
                                      .ToDictionary(x => x.Project, x => CspDecoder.Decode(x.Connection), StringComparer.OrdinalIgnoreCase);
                });
        }

        public IDictionary<string, string> GetDomains()
        {
            return _cache.GetOrAdd("_MasterDbService.GetDomains",
                () => SqlRepo.GetDomains());
        }

        public AliasCollection GetCtAliasAssociationCollection()
        {
            return _cache.GetOrAdd(
                "_MasterDbService.GetCtAliasAssociationCollection",
                () => SqlRepo.GetCtAliasAssociationCollection(),
                DateTimeOffset.UtcNow.AddHours(MemoryCacheExtensions.CacheTimeInHours));
        }

        public string GetContentCache(string url, string languageCode, int daysToCache)
        {
            return SqlRepo.GetContentCache(url, languageCode, daysToCache);
        }

        public int ClearCache(string url)
        {
            return SqlRepo.ClearCache(url);
        }

        public bool InsertToCache(string requestedUrl, string project, string syndicationType, string requestType, string text, int daysToCache, string languageCode)
        {
            return SqlRepo.InsertToCache(requestedUrl, project, syndicationType, requestType, text, daysToCache, languageCode);
        }

        public string GetDomainMask(string mask)
        {
            return _unitOfWork.Repo<DomainMaskEntity>().AsNoTracking()
                .Where(x => x.Mask == mask)
                .Select(x => x.Project)
                .FirstOrDefault();
        }

        public string GetProject(string domain)
        {
            return _unitOfWork.Repo<ConnectionEntity>().AsNoTracking()
                .Where(x => x.Domain == domain)
                .Select(x => x.Project)
                .FirstOrDefault();
        }

        public string GetProject(string mfrName, string requestedType)
        {
            if (!string.IsNullOrEmpty(mfrName) && !string.IsNullOrEmpty(requestedType))
            {
                return _unitOfWork.Repo<MfrNameRequestedTypeAndProjectMapEntity>().AsNoTracking()
                    .Where(x => x.ManufacturerName == mfrName && x.RequestedType == requestedType)
                    .Select(x => x.Project)
                    .FirstOrDefault();
            }
            return !string.IsNullOrEmpty(mfrName) ? mfrName : null;
        }
    }
}
