﻿using AutoMapper;
using Csp.Core.Infrastructure.Security;
using Csp.Core.Master.Entities;
using Libcore.AutoMapper;

namespace Csp.Core.Master.Services
{
    public class ConnectionModel : ICustomMappings
    {
        public string Mode { get; set; }
        public string Project { get; set; }
        public string ConnectionString { get; set; }
        public string Domain { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<ConnectionEntity, ConnectionModel>()
                .ForMember(x => x.ConnectionString, opt => opt.ResolveUsing(x => CspDecoder.Decode(x.Connection)));
        }
    }
}
