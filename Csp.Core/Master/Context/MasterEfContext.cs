﻿using System.Data.Entity;
using Csp.Core.Master.Entities;
using Libcore.DataAccess.Context;

namespace Csp.Core.Master.Context
{
    public class MasterEfContext : DataContext
    {
        public DbSet<ConnectionEntity> Connections { get; set; }
        public DbSet<AliasEntity> Aliases { get; set; }
        public DbSet<LanguageMapEntity> LanguageMaps { get; set; }
        public DbSet<DomainMaskEntity> DomainMasks { get; set; }
        public DbSet<MfrNameRequestedTypeAndProjectMapEntity> MfrNames { get; set; }

        static MasterEfContext()
        {
            Database.SetInitializer<MasterEfContext>(null);
        }

        public MasterEfContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ConnectionEntity>().ToTable("Connections");
            builder.Entity<AliasEntity>().ToTable("Alias");
            builder.Entity<LanguageMapEntity>().ToTable("LanguageMap")
                .HasKey(x => x.LanguageCode);
            builder.Entity<DomainMaskEntity>().ToTable("Domain_Mask");
            builder.Entity<MfrNameRequestedTypeAndProjectMapEntity>().ToTable("MfrNameRequestedTypeAndProjectMap")
                .HasKey(x => new { x.ManufacturerName, x.RequestedType });
        }
    }
}
