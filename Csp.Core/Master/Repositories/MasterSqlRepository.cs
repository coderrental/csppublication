﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Csp.Core.DbHelpers;
using Csp.Core.Infrastructure.DAO.Helpers;
using Csp.Core.Infrastructure.Publication.Models;
using Csp.Core.Master.Context;

namespace Csp.Core.Master.Repositories
{
    public class MasterSqlRepository : GenericSqlRepository<MasterEfContext>
    {
        private string ConnectionString => DataContext.Database.Connection.ConnectionString;

        public IDictionary<string, string> GetAliasMap()
        {
            const string sql = "select lower(Alias),lower(Value) from Alias";
            return GetMap(sql, null);
        }

        //parses a 2 column resultset into a <key=column1,value=column2> dictionary
        public IDictionary<string, string> GetMap(string sql, IDictionary<string, object> parameters)
        {
            var dict = new Dictionary<string, string>();
            try
            {

                using (var reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.Text, sql, ToSqlParameters(parameters).ToArray()))
                {
                    while (reader.Read())
                    {
                        var key = reader.GetString(0);
                        var value = reader.GetString(1);
                        dict[key] = value;
                    }
                }
            }
            catch
            {
                // ignored
            }
            return dict;
        }

        public AliasCollection GetAliasCollection(string sql)
        {
            var collection = new AliasCollection();
            try
            {
                SqlTransaction transaction = null;
                using (var reader = SqlHelper.ExecuteReader(ConnectionString, CommandType.Text, sql))
                {
                    while (reader.Read())
                    {
                        // Colum 4 null exception int32
                        collection.Add(new Alias(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),
                            reader.GetString(3), reader.GetInt32(4), reader[5].ToString()));
                    }
                }
            }
            catch (Exception)
            {
                //var s = ex.Message;
            }

            return collection;
        }

        public IDictionary<string, string> GetLanguageMap()
        {
            const string sql = "select lower(LanguageCode), CspLanguageCode from LanguageMap";
            return GetMap(sql, null);
        }

        //returns encrypted connectionStr definition if found
        public IDictionary<string, string> GetConnections()
        {
            const string sql = "select lower(Project),Connection from Connections where Mode = @mode and mode <> 'notused'";
            return GetMap(sql, new Dictionary<string, object>
            {
                {"mode",GlobalConfiguration.Mode}
            });
        }

        public IDictionary<string, string> GetDomains()
        {
            const string sql = "select lower(Project),Domain from Connections where Mode = @mode";
            IDictionary<string, string> target = GetMap(sql, new Dictionary<string, object>
            {
                {"mode",GlobalConfiguration.Mode}
            });

            return target;
        }

        public AliasCollection GetCtAliasAssociationCollection()
        {
            return GetAliasCollection(AliasCollection.getAliasQuery);
        }

        public MasterSqlRepository(MasterEfContext context) : base(context)
        {
        }

        public string GetContentCache(string url, string languageCode, int daysToCache)
        {
            try
            {
                const string sql = @"
                                select top(1) Content
                                from ProxyCache with (nolock)
                                where HASHBYTES('sha1',convert(nvarchar(4000),@url)) = UrlHash and (DATEDIFF(DAY,CreatedOn,GETDATE())) < @daysToCache and IsExpired = 0 and [RequestLanguageCode] like @lng
                            ";
                var parameters = new Dictionary<string, object>
                {
                    {"@url", url},
                    {"@daysToCache", daysToCache},
                    {"@lng", string.IsNullOrEmpty(languageCode) ? "%" : languageCode}
                };

                return Encoding.UTF8.GetString(GetValue<byte[]>(sql, ToSqlParameters(parameters)));
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public int ClearCache(string url)
        {
            const string sql = @"
                    update [ProxyCache]
                    set IsExpired = 1
                    where [RawUrl] like @url+'%' and IsExpired = 0
                ";
            return ExecuteNonQuery(sql, new SqlParameter("@url", url));
        }

        public bool InsertToCache(string requestedUrl, string project, string syndicationType, string requestType, string text, int daysToCache, string languageCode)
        {
            try
            {
                //TODO : improve sql
                var item = GetContentCache(requestedUrl, project, daysToCache);
                if (string.IsNullOrEmpty(item))
                {
                    var parameters = new Dictionary<string, object>
                    {
                        {"@url", requestedUrl},
                        {"@project", project},
                        {"@syndicationType", syndicationType},
                        {"@requestType", requestType},
                        {"@content", Encoding.UTF8.GetBytes(text)},
                        {"@lng", languageCode}
                    };
                    const string sql = @"
                                            insert into [ProxyCache]
                                                   ([UrlHash]
                                                   ,[Project]
                                                   ,[SyndicationType]
                                                   ,[RequestType]
                                                   ,[RequestLanguageCode]
                                                   ,[Content]
                                                   ,[RawUrl]
                                                   ,[IsExpired]
                                                   ,[CreatedOn])
                                            values (
                                                HASHBYTES('sha1',convert(nvarchar(4000),@url))
                                                ,@project
                                                ,@syndicationType
                                                ,@requestType
                                                ,@lng
                                                ,@content
                                                ,@url
                                                ,0
                                                ,GETDATE())
                                        ";

                    ExecuteNonQuery(sql, ToSqlParameters(parameters));
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}