﻿using Csp.Core.Master.Entities;
using Libcore.AutoMapper;

namespace Csp.Core.Master.Models
{
    public class AliasModel : IMapFrom<AliasEntity>
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Value { get; set; }
        public string SearchFieldName { get; set; }
        public int SearchFieldValue { get; set; }
        public string SyndicationType { get; set; }
    }
}