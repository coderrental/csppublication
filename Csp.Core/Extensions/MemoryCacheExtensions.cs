﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Csp.Core.Extensions
{
    public static class MemoryCacheExtensions
    {
        public const double CacheTimeInHours = 4;

        public static T GetOrAdd<T>(this MemoryCache cache, string key, Func<T> func, DateTimeOffset absoluteExpiration) where T : class
        {
            if (!cache.Contains(key))
            {
                cache.Add(key, func(), absoluteExpiration);
            }
            return cache.Get(key) as T;
        }

        public static IDictionary<string, string> GetOrAdd<T>(this MemoryCache cache, string key, Func<T> func) where T : class
        {
            return GetOrAdd(cache, key, func, DateTimeOffset.UtcNow.AddHours(CacheTimeInHours)) as IDictionary<string, string>;
        }
    }
}