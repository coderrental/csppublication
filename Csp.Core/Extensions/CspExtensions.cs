﻿using System;
using System.Web;

namespace Csp.Core.Extensions
{
    public static class CspExtensions
    {
        public static string GetQueryString(this HttpRequestBase request, params string[] queryParams)
        {
            foreach (var param in queryParams)
            {
                foreach (var key in request.QueryString.AllKeys)
                {
                    if (param.Equals(key, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return request.QueryString[key];
                    }
                }
            }
            return string.Empty;
        }

        public static string GetNotEmpty(string str1, string str2)
        {
            if (!string.IsNullOrEmpty(str1))
                return str1;
            return !string.IsNullOrEmpty(str2) ? str2 : null;
        }
    }
}