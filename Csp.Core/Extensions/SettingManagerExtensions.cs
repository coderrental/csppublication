﻿using Csp.Core.Infrastructure.Security;
using Libcore.Core.Configuration;

namespace Csp.Core.Extensions
{
    public static class SettingManagerExtensions
    {
        public static string Mode(this ISettingManager settings)
        {
            return settings.GetNotEmptyValue("Mode");
        }

        public static string MasterConnectionString(this ISettingManager settings)
        {
            return CspDecoder.Decode(settings.GetNotEmptyValue("prod.csp_master"));
        }
    }
}
