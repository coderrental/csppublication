﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Libcore.DataAccess.Context;

namespace Csp.Core.DataAccess.Context
{
    public class EfContext : DataContext
    {
        static EfContext()
        {
            Database.SetInitializer<EfContext>(null);
        }

        public EfContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Conventions.Remove<PluralizingTableNameConvention>();
            builder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}
