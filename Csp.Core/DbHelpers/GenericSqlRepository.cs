﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Libcore.Core.Extensions;
using Libcore.DataAccess.Context;

namespace Csp.Core.DbHelpers
{
    public abstract class GenericSqlRepository<TContext> where TContext : DataContext
    {
        protected TContext DataContext { get; }

        protected GenericSqlRepository(TContext context)
        {
            DataContext = context;
        }

        protected void OpenConnection()
        {
            if (DataContext.Database.Connection.State != ConnectionState.Open)
            {
                DataContext.Database.Connection.Open();
            }
        }

        protected DbCommand CreateCommand()
        {
            return DataContext.Database.Connection.CreateCommand();
        }

        public int ExecuteNonQuery(string commandText, params object[] parameters)
        {
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandText = commandText;
                command.Parameters.AddRange(parameters);
                return command.ExecuteNonQuery();
            }
        }

        public T GetValue<T>(string sql, params object[] parameters)
        {
            return GetValue<T>(sql, CommandType.Text, parameters);
        }

        public T GetSpValue<T>(string sql, params object[] parameters)
        {
            return GetValue<T>(sql, CommandType.StoredProcedure, parameters);
        }

        public IList<T> GetList<T>(string sql, params object[] parameters) where T : class, new()
        {
            return GetList<T>(sql, CommandType.Text, parameters);
        }

        public IList<T> GetSpList<T>(string sql, params object[] parameters) where T : class, new()
        {
            return GetList<T>(sql, CommandType.StoredProcedure, parameters);
        }

        public IList<T> GetValueList<T>(string sql, params object[] parameters)
        {
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandText = sql;
                command.Parameters.AddRange(parameters);
                using (var dr = command.ExecuteReader())
                {
                    var list = new List<T>();
                    while (dr.Read())
                    {
                        list.Add(dr.GetValue(0).ChangeTypeTo<T>());
                    }
                    return list;
                }
            }
        }

        public IDictionary<TKey, TValue> GetValueDictionary<TKey, TValue>(string sql, params object[] parameters)
        {
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandText = sql;
                command.Parameters.AddRange(parameters);
                using (var dr = command.ExecuteReader())
                {
                    var dict = new Dictionary<TKey, TValue>();
                    while (dr.Read())
                    {
                        dict.Add(dr.GetFieldValue<TKey>(0), dr.GetFieldValue<TValue>(1));
                    }
                    return dict;
                }
            }
        }

        public T GetValue<T>(string commandText, CommandType commandType, params object[] parameters)
        {
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;
                command.Parameters.AddRange(parameters);
                return command.ExecuteScalar().ChangeTypeTo<T>(default(T));
            }
        }

        public IList<T> GetList<T>(string commandText, CommandType commandType, params object[] parameters) where T : class, new()
        {
            OpenConnection();
            using (var command = CreateCommand())
            {
                command.CommandType = commandType;
                command.CommandText = commandText;
                command.Parameters.AddRange(parameters);
                return CBO.FillCollection<T>(command.ExecuteReader());
            }
        }

        protected IList<SqlParameter> ToSqlParameters(IDictionary<string, object> parameters)
        {
            var list = new List<SqlParameter>();
            if (parameters != null)
            {
                foreach (var key in parameters.Keys)
                {
                    list.Add(new SqlParameter(key, parameters[key]));
                }
            }
            return list;
        }
    }
}
