﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Libcore.Core.Configuration;
using Libcore.Core.Net.Mail;
using Libcore.Core.Net.Mail.Smtp;
using Libcore.Core.Net.Mail.Template;
using SimpleInjector;

namespace Libcore.Dependency.Extensions
{
    public static class ContainerExtensions
    {
        [DebuggerStepThrough]
        public static void RegisterScopedByConventions(this Container container, Assembly assembly)
        {
            var allServices = assembly.GetTypes().Where(p => p.IsClass && !p.IsAbstract);
            foreach (var type in allServices)
            {
                var allInterfaces = type.GetInterfaces();
                var mainInterfaces = allInterfaces.Except(allInterfaces.SelectMany(t => t.GetInterfaces()));
                foreach (var itype in mainInterfaces)
                {
                    if (itype.Name.EndsWith(type.Name))
                    {
                        container.Register(itype, type, Lifestyle.Scoped);
                    }
                }
            }
        }

        [DebuggerStepThrough]

        public static void RegisterEmailSender(this Container container)
        {
            container.RegisterSingleton<ISmtpEmailSenderConfig, SmtpEmailSenderConfig>();
            container.RegisterSingleton<ISettingManager, FileSettingManager>();

            container.Register<IEmailSender, SmtpEmailSender>(Lifestyle.Scoped);
            container.Register<ITemplateEmailSender, TemplateEmailSender>(Lifestyle.Scoped);
            container.Register<IEmailTemplateService, EmailTemplateService>(Lifestyle.Scoped);
        }

        public static void RegisterOptions<T>(this Container container) where T : class, new()
        {
            container.RegisterInstance(SettingManagerHelper.Get<T>());
        }
    }
}