﻿using System;
using System.Linq;
using System.Reflection;
using SimpleInjector.Advanced;

namespace Libcore.Dependency.Behaviors
{
    internal class InjectPropertySelectionBehavior : IPropertySelectionBehavior
    {
        public bool SelectProperty(Type type, PropertyInfo prop)
        {
            return prop.GetCustomAttributes(typeof(InjectAttribute)).Any();
        }
    }
}