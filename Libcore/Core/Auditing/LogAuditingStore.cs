﻿using Serilog;

namespace Libcore.Core.Auditing
{
    public class LogAuditingStore : IAuditingStore
    {
        public void Save(AuditInfo auditInfo)
        {
            Log.ForContext<LogAuditingStore>().Information(auditInfo.Exception, "AUDIT LOG: {@AuditInfo}", auditInfo);
        }
    }
}