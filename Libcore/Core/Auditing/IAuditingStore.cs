﻿namespace Libcore.Core.Auditing
{
    public interface IAuditingStore
    {
        void Save(AuditInfo auditInfo);
    }
}