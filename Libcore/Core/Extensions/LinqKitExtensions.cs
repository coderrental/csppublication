﻿using System;
using System.Linq.Expressions;
using LinqKit;

namespace Libcore.Core.Extensions
{
    public static class LinqKitExtensions
    {
        public static Expression<Func<T, bool>> AndIf<T>(this Expression<Func<T, bool>> source, bool condition, Expression<Func<T, bool>> predicate)
            where T : class
        {
            if (condition)
            {
                source = source.And(predicate);
            }
            return source;
        }

        public static Expression<Func<T, bool>> AndIf<T>(this ExpressionStarter<T> source, bool condition, Expression<Func<T, bool>> predicate)
            where T : class
        {
            if (condition)
            {
                source = source.And(predicate);
            }
            return source;
        }

        public static Expression<Func<T, bool>> AndIfNotEmtpy<T>(this Expression<Func<T, bool>> source, string value, Expression<Func<T, bool>> predicate)
            where T : class
        {
            return source.AndIf(!string.IsNullOrWhiteSpace(value), predicate);
        }

        public static Expression<Func<T, bool>> AndIfNotEmtpy<T>(this ExpressionStarter<T> source, string value, Expression<Func<T, bool>> predicate)
            where T : class
        {
            return source.AndIf(!string.IsNullOrWhiteSpace(value), predicate);
        }

        public static Expression<Func<T, bool>> OrIf<T>(this ExpressionStarter<T> source, bool condition, Expression<Func<T, bool>> predicate)
            where T : class
        {
            if (condition)
            {
                source = source.Or(predicate);
            }
            return source;
        }

        public static Expression<Func<T, bool>> OrIf<T>(this Expression<Func<T, bool>> source, bool condition, Expression<Func<T, bool>> predicate)
            where T : class
        {
            if (condition)
            {
                source = source.Or(predicate);
            }
            return source;
        }
    }
}
