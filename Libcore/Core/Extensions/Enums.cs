﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Humanizer;
using Libcore.Core.Infrastructure.Objects;

namespace Libcore.Core.Extensions
{
    public static class Enums
    {
        public static T? ToEnum<T>(this object obj)
            where T : struct
        {
            if (obj != null && Enum.TryParse(obj.ToString(), out T result))
            {
                return result;
            }
            return null;
        }

        public static T ToEnum<T>(this object obj, T defaultValue)
            where T : struct
        {
            var result = obj.ToEnum<T>();
            return result ?? defaultValue;
        }

        public static bool IsEnumType<T>(this object obj)
            where T : struct
        {
            return ToEnum<T>(obj) != null;
        }

        public static string Description(this Enum obj)
        {
            return EnumDescription(obj);
        }

        public static string EnumDescription(this object obj)
        {
            var type = obj.GetType();
            var memInfo = type.GetMember(obj.ToString());
            if (memInfo.Length > 0)
            {
                var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            return obj.ToString();
        }

        public static IList<PairInt> ToEnumPairInts(this Type type, bool textAsDescrition = false)
        {
            var values = Enum.GetValues(type);
            var list = new List<PairInt>();
            foreach (var value in values)
            {
                list.Add(new PairInt
                {
                    Value = (int)value,
                    Text = textAsDescrition ? value.EnumDescription() : Enum.GetName(type, value).Titleize()
                });
            }
            return list;
        }

        public static IList<PairString> ToEnumPairStrings(this Type type)
        {
            var names = Enum.GetNames(type);
            return names.Select(name => new PairString
            {
                Value = name,
                Text = name.Titleize()
            })
                        .ToList();
        }
    }
}