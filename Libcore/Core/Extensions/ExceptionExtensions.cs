﻿using System;

namespace Libcore.Core.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetBaseErrorMessage(this Exception ex)
        {
            return ex.GetBaseException().Message;
        }
    }
}
