﻿using System;

namespace Libcore.Core.Extensions
{
    public static class DateExtensions
    {
        public static string ToDateFormat(this DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }
    }
}
