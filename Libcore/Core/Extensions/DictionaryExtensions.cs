﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Elmah;

namespace Libcore.Core.Extensions
{
    /// <summary>
    /// Extension methods for Dictionary.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// This method is used to try to get a value in a dictionary if it does exists.
        /// </summary>
        /// <typeparam name="T">Type of the value</typeparam>
        /// <param name="dictionary">The collection object</param>
        /// <param name="key">Key</param>
        /// <param name="value">Value of the key (or default value if key not exists)</param>
        /// <returns>True if key does exists in the dictionary</returns>
        internal static bool TryGetValue<T>(this IDictionary<string, object> dictionary, string key, out T value)
        {
            object valueObj;
            if (dictionary.TryGetValue(key, out valueObj) && valueObj is T)
            {
                value = (T)valueObj;
                return true;
            }

            value = default(T);
            return false;
        }

        /// <summary>
        /// Gets a value from the dictionary with given key. Returns default value if can not find.
        /// </summary>
        /// <param name="dictionary">Dictionary to check and get</param>
        /// <param name="key">Key to find the value</param>
        /// <typeparam name="TKey">Type of the key</typeparam>
        /// <typeparam name="TValue">Type of the value</typeparam>
        /// <returns>Value if found, default if can not found.</returns>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue obj;
            return dictionary.TryGetValue(key, out obj) ? obj : default(TValue);
        }

        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue = default(TValue))
        {
            TValue obj;
            return dictionary.TryGetValue(key, out obj) ? obj : defaultValue;
        }


        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value,
          bool replaceExisting = true)
        {
            if (replaceExisting || !dict.ContainsKey(key))
                dict[key] = value;
            return dict;
        }
       
        public static Dictionary<TKey, TElement> ToSafeDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
        {
            var dictionary = new Dictionary<TKey, TElement>(comparer);
            foreach (var source1 in source)
            {
                var key = keySelector(source1);
                if (key == null || dictionary.ContainsKey(key)) continue;
                dictionary.Add(key, elementSelector(source1));

            }
            return dictionary;
        }

        public static string ToQueryString(this NameValueCollection collection)
        {
            var list = collection.AllKeys.Select(name => $"{name}={collection[name]}");
            return string.Join("&", list);
        }
    }
}