﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Libcore.Core.Infrastructure.Objects;

namespace Libcore.Core.Extensions
{
    public static class Strings
    {
        public static string TrimStart(this string input, string trimString)
        {
            if (input != null && trimString != null && input.StartsWith(trimString))
            {
                return input.Substring(trimString.Length);
            }
            return input;
        }

        public static string TrimEnd(this string input, string suffixToRemove)
        {
            if (input != null && suffixToRemove != null && input.EndsWith(suffixToRemove))
            {
                return input.Substring(0, input.Length - suffixToRemove.Length);
            }
            return input;
        }

        public static bool TrimEquals(this string a, string b)
        {
            return string.Equals(a?.Trim(), b?.Trim(), StringComparison.OrdinalIgnoreCase);
        }

        public static int[] ToIntArray(this string source)
        {
            return source.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                         .Select(x => x.Trim().ChangeTypeTo<int>())
                         .Distinct()
                         .ToArray();
        }

        public static string StripHtml(this string htmlString, string htmlPlaceHolder)
        {
            const string pattern = @"<(.|\n)*?>";
            var sOut = Regex.Replace(htmlString, pattern, htmlPlaceHolder);
            sOut = sOut.Replace("&nbsp;", string.Empty);
            sOut = sOut.Replace("&amp;", "&");
            sOut = sOut.Replace("&gt;", ">");
            sOut = sOut.Replace("&lt;", "<");
            return sOut;
        }

        public static string ToSlug(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return null;

            value = value.RemoveDiacritics().ToLowerInvariant();
            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(value);
            value = Encoding.ASCII.GetString(bytes);
            value = Regex.Replace(value, @"\s+", "-", RegexOptions.Compiled);
            value = Regex.Replace(value, @"[^\w\s\p{Pd}]", "", RegexOptions.Compiled);
            value = value.Trim('-', '_');
            value = Regex.Replace(value, @"([-_]){2,}", "$1", RegexOptions.Compiled);
            return value;
        }

        private static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static IList<KeyValue> ToKeyValues(this string source)
        {
            return source.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries))
                .Where(x => x.Length == 2)
                .Select(x => new KeyValue
                {
                    Key = x[0].Trim(),
                    Value = x[1].Trim()
                }).ToList();
        }
    }
}
