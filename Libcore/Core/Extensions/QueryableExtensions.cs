﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Libcore.Core.Infrastructure.Paging;

namespace Libcore.Core.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IQueryable"/> and <see cref="IQueryable{T}"/>.
    /// </summary>
    public static class QueryableExtensions
    {

        /// <summary>
        /// Filters a <see cref="IQueryable{T}"/> by given predicate if given condition is true.
        /// </summary>
        /// <param name="query">Queryable to apply filtering</param>
        /// <param name="condition">A boolean value</param>
        /// <param name="predicate">Predicate to filter the query</param>
        /// <returns>Filtered or not filtered query based on <see cref="condition"/></returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition, Expression<Func<T, bool>> predicate)
        {
            return condition
                ? query.Where(predicate)
                : query;
        }

        /// <summary>
        /// Filters a <see cref="IQueryable{T}"/> by given predicate if given condition is true.
        /// </summary>
        /// <param name="query">Queryable to apply filtering</param>
        /// <param name="condition">A boolean value</param>
        /// <param name="predicate">Predicate to filter the query</param>
        /// <returns>Filtered or not filtered query based on <see cref="condition"/></returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition, Expression<Func<T, int, bool>> predicate)
        {
            return condition
                ? query.Where(predicate)
                : query;
        }

        public static IQueryable<T> Top<T>(this IQueryable<T> query, int skipCount, int maxResultCount)
        {
            return query.Skip(skipCount).Take(maxResultCount);
        }

        public static async Task<IPagedList<TResult>> PageByAsync<T, TResult>(this IQueryable<T> query, IQueryable<T> countQuery, int page, int pageSize, Func<IEnumerable<T>, IList<TResult>> mapping)
        {
            var list = await query.PageByAsync(countQuery, page, pageSize);
            return mapping(list).ToPagedList(list.GetMetaData());
        }

        public static Task<IPagedList<TResult>> PageByAsync<T, TResult>(this IQueryable<T> query, int page, int pageSize, Func<IEnumerable<T>, IList<TResult>> mapping)
        {
            return query.PageByAsync(query, page, pageSize, mapping);
        }

        public static Task<IPagedList<T>> PageByAsync<T>(this IQueryable<T> query, int page, int pageSize)
        {
            return query.PageByAsync(query, page, pageSize);
        }

        public static async Task<IPagedList<T>> PageByAsync<T>(this IQueryable<T> query, IQueryable<T> countQuery, int page, int pageSize)
        {
            var total = await countQuery.CountAsync();
            return await query.ToPagedListAsync(page, pageSize, total);
        }

        public static IPagedList<TResult> PageBy<T, TResult>(this IQueryable<T> query, int page, int pageSize, Func<IEnumerable<T>, IList<TResult>> mapping)
        {
            return query.PageBy(query, page, pageSize, mapping);
        }

        public static IPagedList<TResult> PageBy<T, TResult>(this IQueryable<T> query, IQueryable<T> countQuery, int page, int pageSize, Func<IEnumerable<T>, IList<TResult>> mapping)
        {
            var list = query.PageBy(countQuery, page, pageSize);
            return mapping(list).ToPagedList(list.GetMetaData());
        }

        public static IPagedList<T> PageBy<T>(this IQueryable<T> query, IQueryable<T> countQuery, int page, int pageSize)
        {
            var total = countQuery.Count();
            return query.ToPagedList(page, pageSize, total);
        }

        public static IPagedList<T> PageBy<T>(this IQueryable<T> query, int page, int pageSize)
        {
            return query.PageBy(query, page, pageSize);
        }
    }
}