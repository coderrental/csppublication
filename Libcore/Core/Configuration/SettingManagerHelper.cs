﻿using System.Collections.Generic;
using System.Configuration;
using Libcore.Core.Extensions;

namespace Libcore.Core.Configuration
{
    public static class SettingManagerHelper
    {
        public static T Get<T>() where T : class, new()
        {
            var prefix = typeof(T).Name;
            var list = typeof(T).GetProperties();
            var dict = new Dictionary<string, object>();
            foreach (var pi in list)
            {
                dict.Add(pi.Name, ConfigurationManager.AppSettings[$"{prefix}.{pi.Name}"]);
            }
            return dict.FromDictionary(new T());
        }
    }
}
