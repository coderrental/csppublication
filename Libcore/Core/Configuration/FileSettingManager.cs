using System;
using System.Configuration;
using Libcore.Core.Extensions;

namespace Libcore.Core.Configuration
{
    public class FileSettingManager : ISettingManager
    {
        public string GetNotEmptyValue(string name)
        {
            var value = GetValue(name);
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception($"Setting value for '{name}' is null or empty!");
            }
            return value;
        }

        public string GetValue(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public T GetValue<T>(string name, T defaultValue = default(T)) where T : struct
        {
            return GetValue(name).ChangeTypeTo<T>(defaultValue);
        }

        public T GetNotEmptyValue<T>(string name) where T : struct
        {
            return GetNotEmptyValue(name).ChangeTypeTo<T>();
        }
    }
}