﻿using System.Resources;
using Libcore.Web.MetadataModel.Localization;

namespace Libcore.Core.Localization
{
    public static class SharedResource
    {
        public static ILocalizationSource Current { get; private set; }

        public static void SetSources(params ResourceManager[] resources)
        {
            Current = new LocalizationSource(resources);
        }

        public static string Get(string name)
        {
            return IsEnabled ? (Current.GetString(name) ?? name) : name;
        }

        public static bool IsEnabled => Current != null;
    }
}
