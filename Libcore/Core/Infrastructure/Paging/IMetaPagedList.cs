﻿using System.Collections.Generic;

namespace Libcore.Core.Infrastructure.Paging
{
    public interface IMetaPagedList<out T> : IPagedList
    {
        IEnumerable<T> Results { get; }
    }
}