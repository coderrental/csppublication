﻿using System.Collections.Generic;

namespace Libcore.Core.Infrastructure.Paging
{
    public class MetaPagedList<T> : PagedListMetaData, IMetaPagedList<T>
    {
        public IEnumerable<T> Results { get; protected set; }

        public MetaPagedList(IPagedList<T> pagedList)
            : base(pagedList)
        {
            Results = pagedList;
        }
    }
}
