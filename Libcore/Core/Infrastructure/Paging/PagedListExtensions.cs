﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Libcore.Core.Infrastructure.Paging
{
    /// <summary>
    /// Container for extension methods designed to simplify the creation of instances of <see cref="PagedList{T}"/>.
    /// </summary>
    public static class PagedListExtensions
    {
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, int pageNumber, int pageSize, int totalCount)
        {
            var list = new List<T>();
            if (superset != null && totalCount > 0)
            {
                list.AddRange(pageNumber == 1
                    ? await superset.Skip(0).Take(pageSize).ToListAsync()
                    : await superset.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync());
            }
            return new PagedList<T>(list, pageNumber, pageSize, totalCount);
        }

        public static IPagedList<T> ToPagedList<T>(this IQueryable<T> superset, int pageNumber, int pageSize, int totalCount)
        {
            var list = new List<T>();
            if (superset != null && totalCount > 0)
            {
                list.AddRange(pageNumber == 1
                    ? superset.Skip(0).Take(pageSize).ToList()
                    : superset.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
            }
            return new PagedList<T>(list, pageNumber, pageSize, totalCount);
        }

        public static IPagedList<T> ToPagedList<T>(this IList<T> superset, IPagedList metadata)
        {
            return new PagedList<T>(superset, metadata);
        }

        public static IMetaPagedList<T> ToMetaPagedList<T>(this IPagedList<T> source)
        {
            return new MetaPagedList<T>(source);
        }
    }
}