﻿using Libcore.Core.Configuration;

namespace Libcore.Core.Net.Mail
{
    /// <summary>
    /// Implementation of <see cref="IEmailSenderConfig"/> that reads settings
    /// from <see cref="ISettingManager"/>.
    /// </summary>
    public abstract class EmailSenderConfig : IEmailSenderConfig
    {
        public string DefaultFromAddress
        {
            get { return SettingManager.GetNotEmptyValue(EmailSettingNames.DefaultFromAddress); }
        }

        public string DefaultFromDisplayName
        {
            get { return SettingManager.GetValue(EmailSettingNames.DefaultFromDisplayName); }
        }

        protected readonly ISettingManager SettingManager;

        /// <summary>
        /// Creates a new <see cref="EmailSenderConfig"/>.
        /// </summary>
        protected EmailSenderConfig(ISettingManager settingManager)
        {
            SettingManager = settingManager;
        }
    }
}