﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Libcore.Core.Configuration;
using Libcore.Core.IO;

namespace Libcore.Core.Net.Mail.Template
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly ISettingManager _settings;
        private readonly HttpContextBase _context;
        private const string StyleName = "styles.min.css";

        public EmailTemplateService(ISettingManager settings, HttpContextBase context)
        {
            _settings = settings;
            _context = context;
        }

        public async Task<string> Get(string name)
        {
            if (!Path.HasExtension(name))
            {
                name = $"{name}.html";
            }

            var templatePath = _settings.GetNotEmptyValue("Mail.TemplatePath").TrimEnd('/');

            var absoluteStylePath = string.Format("{0}/{1}/{2}",
                _context.Request.Url?.GetLeftPart(UriPartial.Authority),
                templatePath.TrimStart('~', '/'),
                StyleName);

            // ReSharper disable once AssignNullToNotNullAttribute
            var text = await FileHelper.ReadTextAsync(Path.Combine(HostingEnvironment.MapPath(templatePath), name));
            text = text.Replace(StyleName, absoluteStylePath);
            return text;
        }
    }
}