﻿using System.Threading.Tasks;

namespace Libcore.Core.Net.Mail.Template
{
    public interface IEmailTemplateService
    {
        Task<string> Get(string name);
    }
}