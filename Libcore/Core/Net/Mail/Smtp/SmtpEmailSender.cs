using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Libcore.Core.Net.Mail.Smtp
{
    /// <summary>
    /// Used to send emails over SMTP.
    /// </summary>
    public class SmtpEmailSender : EmailSenderBase, ISmtpEmailSender
    {
        private readonly ISmtpEmailSenderConfig _config;

        /// <summary>
        /// Creates a new <see cref="SmtpEmailSender"/>.
        /// </summary>
        /// <param name="config">Configuration</param>
        public SmtpEmailSender(ISmtpEmailSenderConfig config)
            : base(config)
        {
            _config = config;
        }

        public SmtpClient BuildClient()
        {
            var host = _config.Host;
            var port = _config.Port;

            var smtpClient = new SmtpClient(host, port);
            try
            {
                if (_config.EnableSsl)
                {
                    smtpClient.EnableSsl = true;
                }

                if (_config.UseDefaultCredentials)
                {
                    smtpClient.UseDefaultCredentials = true;
                }
                else
                {
                    smtpClient.UseDefaultCredentials = false;

                    var userName = _config.UserName;
                    if (!string.IsNullOrWhiteSpace(userName))
                    {
                        var password = _config.Password;
                        var domain = _config.Domain;
                        smtpClient.Credentials = !string.IsNullOrWhiteSpace(domain)
                            ? new NetworkCredential(userName, password, domain)
                            : new NetworkCredential(userName, password);
                    }
                }

                return smtpClient;
            }
            catch
            {
                smtpClient.Dispose();
                throw;
            }
        }

        protected override async Task SendEmailAsync(MailMessage mail)
        {
            using (var smtpClient = BuildClient())
            {
                await smtpClient.SendMailAsync(mail);
            }
        }

        protected override void SendEmail(MailMessage mail)
        {
            using (var smtpClient = BuildClient())
            {
                smtpClient.Send(mail);
            }
        }
    }
}