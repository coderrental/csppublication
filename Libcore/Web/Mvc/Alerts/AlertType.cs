﻿namespace Libcore.Web.Mvc.Alerts
{
    public enum AlertType
    {
        Success,
        Warning,
        Info,
        Error
    }
}