using System.Collections.Generic;
using System.Web.Mvc;

namespace Libcore.Web.Mvc.Alerts
{
    public static class AlertExtensions
    {
        const string Alerts = "_Alerts";

        public static List<Alert> GetAlerts(this TempDataDictionary tempData)
        {
            if (!tempData.ContainsKey(Alerts))
            {
                tempData[Alerts] = new List<Alert>();
            }

            return (List<Alert>)tempData[Alerts];
        }

        public static ActionResult WithSuccess(this ActionResult result, string message, string title = null)
        {
            return new AlertDecoratorResult(result, AlertType.Success, message, title);
        }

        public static ActionResult WithInfo(this ActionResult result, string message, string title = null)
        {
            return new AlertDecoratorResult(result, AlertType.Info, message, title);
        }

        public static ActionResult WithWarning(this ActionResult result, string message, string title = null)
        {
            return new AlertDecoratorResult(result, AlertType.Warning, message, title);
        }

        public static ActionResult WithError(this ActionResult result, string message, string title = null)
        {
            return new AlertDecoratorResult(result, AlertType.Error, message, title);
        }
    }
}