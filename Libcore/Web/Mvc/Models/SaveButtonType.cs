﻿using System;

namespace Libcore.Web.Mvc.Models
{
    [Flags]
    public enum SaveButtonType
    {
        Save = 1,
        SaveNew = 2,
        SaveEdit = 4,
        SaveCopy = 8,
        All = Save | SaveNew | SaveEdit | SaveCopy
    }
}