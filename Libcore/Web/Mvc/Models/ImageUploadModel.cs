﻿using System.Web;
using Libcore.Web.Extensions;

namespace Libcore.Web.Mvc.Models
{
    public class ImageUploadModel
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public object RouteValues { get; set; }

        public static ImageUploadModel Create(HttpContextBase context, string action = "Upload", object routeValues = null)
        {
            return new ImageUploadModel
            {
                Controller = context.GetRouteValue("controller").ToString(),
                Action = action,
                RouteValues = routeValues,
            };
        }

        public static ImageUploadModel Create(string controller, string action = "Upload", object routeValues = null)
        {
            return new ImageUploadModel
            {
                Controller = controller,
                Action = action,
                RouteValues = routeValues
            };
        }
    }
}
