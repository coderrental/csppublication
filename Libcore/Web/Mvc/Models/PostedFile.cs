﻿using System.IO;
using System.Web;

namespace Libcore.Web.Mvc.Models
{
    public class PostedFile : HttpPostedFileBase
    {
        public PostedFile(byte[] stream, string contentType, string fileName)
        {
            InputStream = new MemoryStream(stream);
            ContentType = contentType;
            FileName = fileName;
        }

        public override int ContentLength => (int)InputStream.Length;

        public override string ContentType { get; }

        public override string FileName { get; }

        public override Stream InputStream { get; }

        public override void SaveAs(string filename)
        {
            using (var file = File.Open(filename, FileMode.CreateNew))
                InputStream.CopyTo(file);
        }
    }
}