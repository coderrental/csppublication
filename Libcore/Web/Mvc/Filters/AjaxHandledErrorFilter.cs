﻿using System;
using System.Web.Mvc;
using Libcore.Core.Timing;
using Libcore.Web.Mvc.Ajax;

namespace Libcore.Web.Mvc.Filters
{
    public class AjaxHandledErrorFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException(nameof(filterContext));

            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
#if DEBUG
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = AjaxResult.Error().WithException(filterContext.Exception)
                                     .ToResult()
                };
#else
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = AjaxResult.Error().WithMessages($"An error has occurred at {Clock.Now:yy/MM/dd HH:mm:ss}")
                                     .ToResult()
                };
#endif

                filterContext.ExceptionHandled = true;

                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            }
        }
    }
}
