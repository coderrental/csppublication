﻿using System.Web.Mvc;
using Libcore.Core.Localization;
using Libcore.Web.Security;

namespace Libcore.Web.Mvc.Views
{
    public abstract class AppWebViewPage : WebViewPage
    {

    }

    /// <summary>
    /// Base page adding support for the new helpers in all views.
    /// </summary>
    public abstract class AppWebViewPage<TModel> : WebViewPage<TModel>
    {
        public ICurrentUser CurrentUser => GetService<ICurrentUser>();

        private class View
        {
        }

        public T GetService<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }

        public string T(string name)
        {
            return SharedResource.Get(name);
        }
    }
}