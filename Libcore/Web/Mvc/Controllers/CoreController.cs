﻿using System;
using System.Data.SqlClient;
using System.Web.Mvc;
using Libcore.Web.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Libcore.Web.Mvc.Controllers
{
    public class CoreController : Controller
    {
        protected void HandleError(Exception ex)
        {
            if (ex.GetBaseException() is SqlException sqlException)
            {
                ModelState.AddModelError("", GetSqlExceptionMessage(sqlException));
            }
            else
            {
                ModelState.AddException(ex);
            }
        }

        protected virtual string GetSqlExceptionMessage(SqlException sqlException)
        {
            return sqlException.Message;
        }

        protected ActionResult JsonContent(object data, bool camelCasePropertyNames = false)
        {
            return Content(JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ContractResolver = camelCasePropertyNames
                ? new CamelCasePropertyNamesContractResolver()
                : new DefaultContractResolver()
            }), "application/json");
        }
    }
}
