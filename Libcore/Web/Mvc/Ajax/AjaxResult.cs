using System.Collections.Generic;

namespace Libcore.Web.Mvc.Ajax
{
    public class AjaxResult
    {
        public AjaxResultType ResultType { get; set; }
        public object Data { get; set; }
        public IList<string> Errors { get; set; }

        public AjaxResult()
        {
            Errors = new List<string>();
        }

        public void AddErrors(params string[] errors)
        {
            foreach (var error in errors)
            {
                Errors.Add(error);
            }
        }

        public static AjaxResultFluent Success()
        {
            return new AjaxResultFluent().Result(AjaxResultType.Success);
        }

        public static AjaxResultFluent Error()
        {
            return new AjaxResultFluent().Result(AjaxResultType.Error);
        }
    }
}