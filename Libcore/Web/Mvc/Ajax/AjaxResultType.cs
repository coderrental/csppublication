namespace Libcore.Web.Mvc.Ajax
{
    public enum AjaxResultType
    {
        Error,
        Success
    }
}