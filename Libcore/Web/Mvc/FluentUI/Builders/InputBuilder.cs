using Libcore.Web.Mvc.FluentUI.Models;

namespace Libcore.Web.Mvc.FluentUI.Builders
{
    public class InputBuilder : BuilderBase<InputTagModel, InputBuilder>
    {
        public InputBuilder Type(string type)
        {
            Model.Type = type;
            return this;
        }

        public InputBuilder Value(string value)
        {
            Model.Value = value;
            return this;
        }
    }
}