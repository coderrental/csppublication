﻿using Libcore.Web.Mvc.FluentUI.Models;

namespace Libcore.Web.Mvc.FluentUI.Builders
{
    public class GenericTagBuilder : BuilderBase<GenericTagModel, GenericTagBuilder>
    {
        public GenericTagBuilder(string tagName, string name)
        {
            Model.TagName = tagName;
            Model.Name = name;
        }

        public GenericTagBuilder Text(string text)
        {
            Model.Text = text;
            return this;
        }
    }
}