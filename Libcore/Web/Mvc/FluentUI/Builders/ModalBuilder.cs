using Libcore.Web.Mvc.FluentUI.Models;

namespace Libcore.Web.Mvc.FluentUI.Builders
{
    public class ModalBuilder : BuilderBase<ModalModel, ModalBuilder>
    {
        public ModalBuilder Id(string id)
        {
            Model.Id = id;
            return this;
        }
        public ModalBuilder Size(string size)
        {
            Model.Size = size;
            return this;
        }

        public ModalBuilder Title(string title)
        {
            Model.Title = title;
            return this;
        }

        public ModalBuilder Effect(string value)
        {
            Model.Effect = value;
            return this;
        }

        public ModalBuilder CloseOnClick(bool value = true)
        {
            Model.CloseOnClick = value;
            return this;
        }
        public ModalBuilder Keyboard(bool value = true)
        {
            Model.Keyboard = value;
            return this;
        }
        public ModalBuilder Show(bool value = true)
        {
            Model.Show = value;
            return this;
        }

        public ModalBuilder Type(ModalType type)
        {
            Model.Type = type;
            return this;
        }

        public ModalBuilder IframeSource(string source)
        {
            Model.IframeSrc = source;
            return this;
        }

        public ModalBuilder RemoveContentOnClose(bool value = true)
        {
            Model.RemoveContentOnClose = value;
            return this;
        }

        public ModalBuilder Height(string value)
        {
            Model.Height = value;
            return this;
        }
    }
}