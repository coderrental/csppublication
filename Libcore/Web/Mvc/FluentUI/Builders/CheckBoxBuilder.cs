using Libcore.Web.Mvc.FluentUI.Models;

namespace Libcore.Web.Mvc.FluentUI.Builders
{
    public class CheckBoxBuilder : BuilderBase<CheckBoxModel, CheckBoxBuilder>
    {
        public CheckBoxBuilder Checked(bool isChecked = true)
        {
            Model.Checked = isChecked;
            return this;
        }

        public CheckBoxBuilder Value(bool value)
        {
            Model.Value = value;
            return this;
        }

        public CheckBoxBuilder DisplayName(string value)
        {
            Model.DisplayName = value;
            return this;
        }

        public CheckBoxBuilder ShowLabel(bool isShow)
        {
            Model.ShowLabel = isShow;
            return this;
        }
    }
}