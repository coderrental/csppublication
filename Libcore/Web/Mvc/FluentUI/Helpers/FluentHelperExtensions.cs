﻿using System.Web.Mvc;

namespace Libcore.Web.Mvc.FluentUI.Helpers
{
    public static class FluentHelperExtensions
    {
        public static FluentHelper<TModel> Fluent<TModel>(this HtmlHelper<TModel> helper)
        {
            return new FluentHelper<TModel>(helper);
        }
    }
}