﻿namespace Libcore.Web.Mvc.FluentUI.Models
{
    public enum ModalType
    {
        Normal,
        Iframe
    }
}