﻿using HtmlTags;
using Libcore.Web.Extensions;

namespace Libcore.Web.Mvc.FluentUI.Models
{
    public class GenericTagModel : ModelBase
    {
        public string TagName { get; set; }
        public string Text { get; set; }

        public override HtmlTag Render()
        {
            var tag = new HtmlTag(TagName)
                .Text(Text)
                .Attr(HtmlAttributes);
            return tag;
        }
    }
}