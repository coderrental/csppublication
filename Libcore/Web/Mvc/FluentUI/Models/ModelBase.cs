﻿using System.Collections.Generic;
using HtmlTags;

namespace Libcore.Web.Mvc.FluentUI.Models
{
    public abstract class ModelBase
    {
        public IDictionary<string, object> HtmlAttributes { get; set; }
        public string Name { get; set; }

        protected ModelBase()
        {
            HtmlAttributes = new Dictionary<string, object>();
        }

        public string ToHtmlString()
        {
            return Render().ToHtmlString();
        }

        public abstract HtmlTag Render();
    }
}