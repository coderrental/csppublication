﻿using HtmlTags;
using Libcore.Web.Extensions;

namespace Libcore.Web.Mvc.FluentUI.Models
{
    public class InputTagModel : ModelBase
    {
        public string Type { get; set; }
        public string Value { get; set; }

        public override HtmlTag Render()
        {
            var tag = new HtmlTag("input")
                .NameAndId(Name)
                .Value(Value)
                .AttrIfNotNull("type", Type)
                .Attr(HtmlAttributes);
            return tag.NoClosingTag();
        }
    }
}