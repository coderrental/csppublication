﻿using HtmlTags;
using Libcore.Web.Extensions;

namespace Libcore.Web.Mvc.FluentUI.Models
{
    public class ModalModel : ModelBase
    {
        public string Id { get; set; }
        public string Effect { get; set; }
        public bool CloseOnClick { get; set; }
        public bool Keyboard { get; set; }
        public bool Show { get; set; }
        public string Size { get; set; }
        public string Title { get; set; }
        public ModalType Type { get; set; }
        public string IframeSrc { get; set; }
        public bool RemoveContentOnClose { get; set; }
        public string Height { get; set; }

        public override HtmlTag Render()
        {
            var contentTag = new DivTag()
                .AddClass("modal-content");
            contentTag.Children.Add(HeaderTag());
            contentTag.Children.Add(BodyTag());

            var modalTag = new DivTag()
                .AddClass("modal-dialog")
                .AddClassIfNotNull(Size);
            modalTag.Children.Add(contentTag);

            var mainTag = new DivTag()
                .Id(Id)
                .AddClass("modal modal-custom")
                .AddClassIfNotNull(Effect)
                .Attr("role", "dialog")
                .AttrIfTrue(!CloseOnClick, "data-backdrop", "static")
                .AttrIfTrue(!Keyboard, "data-keyboard", "false")
                .AttrIfTrue(Show, "data-show", "true");
            mainTag.Children.Add(modalTag);
            return mainTag;
        }

        private HtmlTag BodyTag()
        {
            var placeholderTag = new DivTag()
                //.Id($"{Id}-placeholder")
                .AddClass("placeholder")
                .AttrIfTrue(RemoveContentOnClose, "data-remove", "true")
                .AttrIfTrue(string.IsNullOrWhiteSpace(Height), "style", "min-height:350px")
                .AttrIfTrue(!string.IsNullOrWhiteSpace(Height), "style", $"height:{Height}");

            switch (Type)
            {
                case ModalType.Iframe:
                    var iframe = new HtmlTag("iframe")
                        .AddClass("embed-iframe")
                        .Attr("frameborder", "0")
                        .AttrIfNotNull("data-src", IframeSrc);
                    placeholderTag.Children.Add(iframe);
                    break;
            }

            var contentTag = new DivTag()
                .AddClass("ibox-content");
            contentTag.Children.Add(SpinnerTag());
            contentTag.Children.Add(placeholderTag);

            var bodyTag = new DivTag().AddClass("modal-body");
            bodyTag.Children.Add(contentTag);
            return bodyTag;
        }

        private HtmlTag HeaderTag()
        {
            var closeButton = new HtmlTag("button")
                .AddClass("close btn")
                .Attr("type", "button")
                .Data("dismiss", "modal");

            closeButton.Children.Add(new HtmlTag("span")
                .Attr("aria-hidden", "true")
                .AppendHtml("&times;"));

            closeButton.Children.Add(new HtmlTag("span")
                .AddClass("sr-only")
                .Text("Close"));

            if (string.IsNullOrWhiteSpace(Title))
            {
                return closeButton;
            }

            var headerTag = new DivTag()
                .AddClass("modal-header");

            headerTag.Children.Add(closeButton);
            headerTag.Children.Add(new HtmlTag("h4")
                .AddClass("modal-title")
                .Text(Title));
            return headerTag;
        }

        private HtmlTag SpinnerTag()
        {
            var divTag = new DivTag()
                .AddClass("sk-spinner sk-spinner-double-bounce");
            divTag.Children.Add(new DivTag().AddClass("sk-double-bounce1"));
            divTag.Children.Add(new DivTag().AddClass("sk-double-bounce2"));
            return divTag;
        }
    }
}
