﻿using HtmlTags;
using Libcore.Web.Extensions;

namespace Libcore.Web.Mvc.FluentUI.Models
{
    public class CheckBoxModel : ModelBase
    {
        public bool Checked { get; set; }
        public bool Value { get; set; }
        public string DisplayName { get; set; }
        public bool ShowLabel { get; set; }

        public override HtmlTag Render()
        {
            var mainTag = new DivTag().AddClass("checkbox checkbox-primary");

            var checkboxTag = new CheckboxTag(Checked)
                .NameAndId(Name)
                .AttrIfTrue(Value, "value", Value)
                .Attr(HtmlAttributes);

            var labelTag = new HtmlTag("label").Attr("for", Name).AppendHtmlIfTrue(ShowLabel, DisplayName);
            
            mainTag.Children.Add(checkboxTag);
            mainTag.Children.Add(labelTag);

            return mainTag;
        }
    }
}