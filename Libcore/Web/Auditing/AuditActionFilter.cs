﻿using System.Diagnostics;
using System.Web.Mvc;
using Libcore.Core.Auditing;
using Libcore.Core.Extensions;
using Libcore.Core.Timing;
using Libcore.Web.Security;

namespace Libcore.Web.Auditing
{
    public class AuditActionFilter : ActionFilterAttribute
    {
        private static IAuditingStore AuditingStore => DependencyResolver.Current.GetService<IAuditingStore>();
        private static ICurrentUser CurrentUser => DependencyResolver.Current.GetService<ICurrentUser>();

        private AuditInfo _auditInfo;
        private Stopwatch _actionStopwatch;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HandleAuditingBeforeAction(filterContext);
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            HandleAuditingAfterAction(filterContext);
        }

        private void HandleAuditingBeforeAction(ActionExecutingContext filterContext)
        {
            if (!ShouldSaveAudit(filterContext))
            {
                _auditInfo = null;
                return;
            }

            _actionStopwatch = Stopwatch.StartNew();
            _auditInfo = new AuditInfo
            {
                UserId = CurrentUser?.Id,
                ServiceName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                MethodName = filterContext.ActionDescriptor.ActionName,
                Parameters = filterContext.ActionParameters,
                ExecutionTime = Clock.Now
            };
        }

        private void HandleAuditingAfterAction(ActionExecutedContext filterContext)
        {
            if (_auditInfo == null || _actionStopwatch == null)
            {
                return;
            }

            _actionStopwatch.Stop();

            _auditInfo.ExecutionDuration = _actionStopwatch.Elapsed.TotalMilliseconds.ChangeTypeTo<int>(0);
            _auditInfo.Exception = filterContext.Exception;
            _auditInfo.ClientIpAddress = filterContext.RequestContext.HttpContext.Request.UserHostAddress;

            AuditingStore.Save(_auditInfo);
        }

        private static bool ShouldSaveAudit(ActionExecutingContext filterContext, bool defaulValue = true)
        {
            var actionDescriptor = filterContext.ActionDescriptor;
            if (actionDescriptor.ControllerDescriptor.IsDefined(typeof(DisableAuditingAttribute), true))
            {
                return false;
            }

            if (actionDescriptor.IsDefined(typeof(AuditedAttribute), true))
            {
                return true;
            }

            if (actionDescriptor.IsDefined(typeof(DisableAuditingAttribute), true))
            {
                return false;
            }

            return defaulValue;
        }
    }
}
