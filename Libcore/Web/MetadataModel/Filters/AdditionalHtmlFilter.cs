﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Attributes;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Filters
{
    public class AdditionalHtmlFilter : IModelMetadataFilter
    {
        public void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes)
        {
            var dict = new HtmlAttributeDictionary();

            foreach (var attribute in attributes.OfType<IHaveAdditionalHtml>())
            {
                attribute.AddAttributes(metadata, dict);
            }

            metadata.AdditionalAttributes().Merge(dict);
        }
    }
}