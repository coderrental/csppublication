﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Libcore.Web.MetadataModel.Filters
{
    public interface IModelMetadataFilter
    {
        void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes);
    }
}