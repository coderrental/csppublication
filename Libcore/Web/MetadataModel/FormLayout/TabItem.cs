﻿using System.Collections.Generic;

namespace Libcore.Web.MetadataModel.FormLayout
{
    public class TabItem
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int Index { get; set; }
        public IList<TabModelMetadata> Properties { get; set; } = new List<TabModelMetadata>();
    }
}