﻿using System;

namespace Libcore.Web.MetadataModel.FormLayout
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TabDisplayAttribute : Attribute
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int Order { get; set; }
    }
}
