﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Libcore.Web.MetadataModel.FormLayout
{
    public class TabModelMetadata
    {
        public TabDisplayAttribute Display { get; set; }
        public ModelMetadata ModelMetadata { get; set; }

        public static IList<TabItem> Create(IEnumerable<ModelMetadata> metadatas)
        {
            var list = metadatas.Select(CreateTabMetadata).ToList();
            TabDisplayAttribute lastTab = null;
            foreach (var item in list)
            {
                if (item.Display != null)
                {
                    lastTab = item.Display;
                }
                else
                {
                    item.Display = lastTab;
                }
            }

            return list.OrderBy(x => x.Display.Order)
                .GroupBy(x => new { x.Display.Name, x.Display.Title })
                .Select((group, index) => new TabItem
                {
                    Index = index,
                    Name = group.Key.Name,
                    Title = group.Key.Title ?? group.Key.Name,
                    Properties = group.ToList()
                })
                .ToList();
        }

        private static TabModelMetadata CreateTabMetadata(ModelMetadata metadata)
        {
            var display = metadata.ContainerType
                .GetProperty(metadata.PropertyName)
                .GetCustomAttributes(typeof(TabDisplayAttribute), false)
                .FirstOrDefault() as TabDisplayAttribute;

            return new TabModelMetadata
            {
                ModelMetadata = metadata,
                Display = display
            };
        }
    }
}