﻿using System;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Attributes
{
    public abstract class AdditionalHtmlAttribute : Attribute, IHaveAdditionalHtml
    {
        public abstract void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict);
    }
}