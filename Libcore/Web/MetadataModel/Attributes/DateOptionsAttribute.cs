﻿using System;

namespace Libcore.Web.MetadataModel.Attributes
{
    public class DateOptionsAttribute : Attribute
    {
        public bool FutureDate { get; set; }
        public bool PastDate { get; set; }
    }
}