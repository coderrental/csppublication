﻿using System;

namespace Libcore.Web.MetadataModel.Attributes
{
    public class UIOptionsAttribute : Attribute
    {
        public bool Deferred { get; set; } = true;
        public bool ShowLabel { get; set; } = true;
        public string HelpText { get; set; }
        public string OptionLabel { get; set; }
        public string ImagePickerGridId { get; set; }
        public bool Disabled { get; set; }
        public object Custom1 { get; set; }
        public object Custom2 { get; set; }
        public object Custom3 { get; set; }
    }
}
