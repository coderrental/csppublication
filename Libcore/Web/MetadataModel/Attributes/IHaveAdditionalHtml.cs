﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Attributes
{
    public interface IHaveAdditionalHtml
    {
        void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict);
    }
}