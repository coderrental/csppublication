﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class BooleanRequiredTransformer : GenericTransformer<BooleanRequiredAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, BooleanRequiredAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("boolean", "required");
            dict.AddRuleMessage("boolRequired", ErrorMessage(metadata, attribute));
        }
    }
}
