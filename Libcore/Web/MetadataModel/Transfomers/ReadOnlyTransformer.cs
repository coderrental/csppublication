using System.ComponentModel;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class ReadOnlyTransformer : GenericTransformer<ReadOnlyAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, ReadOnlyAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Attr("readonly", "");
        }
    }
}