﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class PhoneTransformer : GenericTransformer<PhoneAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, PhoneAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.AddRuleMessage("mask", ErrorMessage(metadata, attribute));
        }
    }
}