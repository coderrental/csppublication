using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class EmailAddressTransformer : GenericTransformer<EmailAddressAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, EmailAddressAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.AddRuleMessage("email", ErrorMessage(metadata, attribute));
        }
    }
}