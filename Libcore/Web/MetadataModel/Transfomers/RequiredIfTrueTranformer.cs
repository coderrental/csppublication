﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class RequiredIfTrueTranformer : GenericTransformer<RequiredIfTrueAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredIfTrueAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("otherprop", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("deprequiredif", ErrorMessage(metadata, attribute));
        }
    }
}
