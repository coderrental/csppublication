﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Core.Extensions;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class RequiredTransformer : GenericTransformer<RequiredAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Merge("required", string.Empty);
            dict.AddRuleMessage("required", ErrorMessage(metadata, attribute));
        }
    }
}