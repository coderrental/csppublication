﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Localization;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public abstract class GenericTransformer<T> : IAttributeTransformer where T : Attribute
    {
        public void Transform(ModelMetadata metadata, Attribute attribute, HtmlAttributeDictionary dict)
        {
            if (!(attribute is T attr)) return;

            SafeTransform(metadata, attr, dict);
        }

        protected abstract void SafeTransform(ModelMetadata metadata, T attribute, HtmlAttributeDictionary dict);

        protected string ErrorMessage<TAttribute>(ModelMetadata metadata, TAttribute attribute)
            where TAttribute : ValidationAttribute
        {
            return ModelMetadataLocalizer.IsEnabled
                    ? ModelMetadataLocalizer.Current.ErrorMessage(metadata, attribute, metadata.DisplayName)
                    : attribute.ErrorMessage;
        }

        protected string FormatErrorMessage<TAttribute>(ModelMetadata metadata, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            return ModelMetadataLocalizer.IsEnabled
                    ? ModelMetadataLocalizer.Current.FormatErrorMessage(metadata, attribute, parameters)
                    : attribute.FormatErrorMessage((string)parameters[0]);
        }
    }
}