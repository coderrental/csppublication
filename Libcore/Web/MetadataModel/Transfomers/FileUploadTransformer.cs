﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class FileUploadTransformer : GenericTransformer<FileUploadAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, FileUploadAttribute attribute, HtmlAttributeDictionary dict)
        {
            if (attribute.IsRequired)
            {
                dict.Data("upload", "required");
                dict.AddRuleMessage("upload", ErrorMessage(metadata, attribute));
            }
        }
    }
}