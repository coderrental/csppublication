using System;
using System.Collections.Generic;
using System.Linq;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public static class AttributeTransformers
    {
        private static readonly Dictionary<Type, IAttributeTransformer> Transformers
            = new Dictionary<Type, IAttributeTransformer>();

        static AttributeTransformers()
        {
            var list = GetTransformerTypes();

            foreach (var tuple in list)
            {
                Transformers.Add(tuple.Attribute, Activator.CreateInstance(tuple.Transformer) as IAttributeTransformer);
            }

        }

        public static IAttributeTransformer Create(Attribute attribute)
        {
            var type = attribute.GetType();
            return Transformers.ContainsKey(type) ? Transformers[type] : NullTransformer.Instance.Value;
        }

        private static IEnumerable<TransformerType> GetTransformerTypes()
        {
            var types = typeof(AttributeTransformers).Assembly.GetExportedTypes();
            var transType = typeof(GenericTransformer<>).FullName;
            var nullTransType = typeof(NullTransformer);

            var list = from t in types
                       where t != nullTransType && 
                             t.BaseType != null &&
                             $"{t.BaseType.Namespace}.{t.BaseType.Name}" == transType
                       select new TransformerType
                       {
                           Attribute = t.BaseType.GenericTypeArguments[0],
                           Transformer = t
                       };
            return list;
        }

        private class TransformerType
        {
            public Type Attribute { get; set; }
            public Type Transformer { get; set; }
        }
    }
}