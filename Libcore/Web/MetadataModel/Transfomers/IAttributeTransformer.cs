﻿using System;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public interface IAttributeTransformer
    {
        void Transform(ModelMetadata metadata, Attribute attribute, HtmlAttributeDictionary dict);
    }
}