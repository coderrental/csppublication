using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class RangeTransformer : GenericTransformer<RangeAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RangeAttribute attribute, HtmlAttributeDictionary dict)
        {
            if (attribute.Minimum != null)
            {
                dict.Attr("k-min", attribute.Minimum);
            }

            if (attribute.Minimum != null)
            {
                dict.Attr("k-max", attribute.Maximum);
            }
        }
    }
}