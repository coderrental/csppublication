using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class RegexPatternTransformer : GenericTransformer<RegexPatternAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RegexPatternAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("pattern", attribute.Pattern);
            dict.AddRuleMessage("pattern", ErrorMessage(metadata, attribute));
        }
    }
}