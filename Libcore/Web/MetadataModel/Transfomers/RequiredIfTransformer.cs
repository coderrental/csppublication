﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class RequiredIfTransformer : GenericTransformer<RequiredIfAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredIfAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("otherprop", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("deprequired", ErrorMessage(metadata, attribute));
            if (attribute.OtherValue != null)
            {
                dict.Data("otherpropvalue", attribute.OtherValue);
            }
        }
    }
}
