﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Localization;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class CompareTransformer : GenericTransformer<System.ComponentModel.DataAnnotations.CompareAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, System.ComponentModel.DataAnnotations.CompareAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("compare", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("compare", FormatErrorMessage(metadata, attribute,
                metadata.DisplayName,
                ModelMetadataLocalizer.IsEnabled
                    ? ModelMetadataLocalizer.Current.GetModelString(metadata.ContainerType, attribute.OtherProperty)
                    : attribute.OtherPropertyDisplayName));
        }
    }
}