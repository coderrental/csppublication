﻿using System.Web.Mvc;
using Libcore.Web.MetadataModel.Common;
using Libcore.Web.MetadataModel.Validations;

namespace Libcore.Web.MetadataModel.Transfomers
{
    public class SelfReferenceTransformer : GenericTransformer<SelfReferenceAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, SelfReferenceAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("ref", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("selfref", ErrorMessage(metadata, attribute));
        }
    }
}