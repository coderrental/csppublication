﻿using Libcore.Web.MetadataModel.Attributes;

namespace Libcore.Web.MetadataModel.Common
{
    public class UIOptions
    {
        public bool Deferred => Common.Deferred;
        public bool ShowLabel => Common.ShowLabel;
        public string HelpText => Common.HelpText;
        public string OptionLabel => Common.OptionLabel;
        public bool Disabled => Common.Disabled;
        public string ImagePickerGrid => Common.ImagePickerGridId;
        public object Custom1 => Common.Custom1;
        public object Custom2 => Common.Custom2;
        public object Custom3 => Common.Custom3;

        public DateOptionsAttribute Date { get; set; }
        internal UIOptionsAttribute Common { get; set; }
    }
}
