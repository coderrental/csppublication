﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Core.Extensions;

namespace Libcore.Web.MetadataModel.Localization
{
    public static class LocalizationSourceExtensions
    {
        public static string GetModelString(this ILocalizationSource source, ModelMetadata context, string metadataName = null)
        {
            return source.GetModelString(context.ContainerType, context.PropertyName, metadataName);
        }

        public static string GetModelString(this ILocalizationSource source, Type containerType, string propertyName, string metadataName = null)
        {
            var name = LocalizationSourceHelper.GenerateModelName(containerType, propertyName, metadataName);
            var text = source.GetString(name);
            if (string.IsNullOrWhiteSpace(text))
            {
                text = source.GetString(LocalizationSourceHelper.GenerateCommonName(propertyName, metadataName));
            }
            return text;
        }

        private static string GetErrorMessageOrDefault<T>(ILocalizationSource source, string defaultText, object[] parameters)
            where T : ValidationAttribute
        {
            var name = typeof(T).Name;
            var text = source.GetString(name);
            return string.IsNullOrWhiteSpace(text) ? defaultText : string.Format(text, parameters);
        }

        public static string ErrorMessage<TAttribute>(this ILocalizationSource source, ModelMetadata context, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            var errorMessasge = GetMetadataErrorMessasge<TAttribute>(source, context);
            return string.IsNullOrWhiteSpace(errorMessasge) ? source.ErrorMessage(attribute, parameters) : errorMessasge;
        }

        public static string FormatErrorMessage<TAttribute>(this ILocalizationSource source, ModelMetadata context, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            var errorMessasge = GetMetadataErrorMessasge<TAttribute>(source, context);
            return string.IsNullOrWhiteSpace(errorMessasge) ? source.FormatErrorMessage(attribute, parameters) : errorMessasge;
        }

        private static string GetMetadataErrorMessasge<TAttribute>(ILocalizationSource source, ModelMetadata context)
            where TAttribute : ValidationAttribute
        {
            return source.GetModelString(context, typeof(TAttribute).Name.TrimEnd("Attribute"));
        }

        private static string ErrorMessage<T>(this ILocalizationSource source, T attribute, params object[] parameters) where T : ValidationAttribute
        {
            return GetErrorMessageOrDefault<T>(source, attribute.ErrorMessage, parameters);
        }

        private static string FormatErrorMessage<T>(this ILocalizationSource source, T attribute, params object[] parameters) where T : ValidationAttribute
        {
            return GetErrorMessageOrDefault<T>(source, attribute.FormatErrorMessage((string)parameters[0]), parameters);
        }

    }
}
