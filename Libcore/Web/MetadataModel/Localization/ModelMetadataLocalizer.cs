using System.Resources;

namespace Libcore.Web.MetadataModel.Localization
{
    public static class ModelMetadataLocalizer
    {
        public static ILocalizationSource Current { get; private set; }

        public static void SetSources(ILocalizationSource localizationSource)
        {
            Current = localizationSource;
        }

        public static void SetSources(params ResourceManager[] resources)
        {
            Current = new LocalizationSource(resources);
        }

        public static bool IsEnabled => Current != null;
    }
}