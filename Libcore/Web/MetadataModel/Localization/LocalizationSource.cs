using System.Linq;
using System.Resources;

namespace Libcore.Web.MetadataModel.Localization
{
    public class LocalizationSource : ILocalizationSource
    {
        private readonly ResourceManager[] _sources;

        public LocalizationSource(ResourceManager[] resources)
        {
            _sources = resources;
        }

        public string GetString(string name)
        {
            return _sources.Select(source => source.GetString(name)).FirstOrDefault(text => text != null);
        }
    }
}