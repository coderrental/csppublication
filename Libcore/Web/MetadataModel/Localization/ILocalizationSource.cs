﻿namespace Libcore.Web.MetadataModel.Localization
{
    public interface ILocalizationSource
    {
        string GetString(string name);
    }
}
