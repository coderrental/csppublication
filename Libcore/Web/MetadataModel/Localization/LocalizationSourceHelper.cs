﻿using System;

namespace Libcore.Web.MetadataModel.Localization
{
    public static class LocalizationSourceHelper
    {
        public static string GenerateCommonName(string propertyName, string metadataName)
        {
            return GenerateModelName("_", propertyName, metadataName);
        }

        public static string GenerateModelName(Type containerType, string propertyName, string metadataName)
        {
            return GenerateModelName(containerType.Name, propertyName, metadataName);
        }

        private static string GenerateModelName(string prefix, string propertyName, string metadataName)
        {
            var name = $"{prefix}_{propertyName}";
            if (!string.IsNullOrWhiteSpace(metadataName))
            {
                name = $"{name}_{metadataName}";
            }
            return name;
        }
    }
}
