﻿using System.Web.Mvc;

namespace Libcore.Web.MetadataModel.MetadataAware
{
    public class RenderAttribute : MetadataAwareAttribute
    {
        public bool ShowForEdit { get; set; } = true;
        public bool ShowForDisplay { get; set; } = true;

        public override void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.ShowForEdit = ShowForEdit;
            metadata.ShowForDisplay = ShowForDisplay;
        }
    }
}