using System.Web.Mvc;

namespace Libcore.Web.MetadataModel.MetadataAware
{
    public class HtmlEditorAttribute : MetadataAwareAttribute
    {
        public override void OnMetadataCreated(ModelMetadata metadata)
        {
            if (string.IsNullOrWhiteSpace(metadata.TemplateHint))
            {
                metadata.TemplateHint = "HtmlEditor";
            }
            metadata.RequestValidationEnabled = false;
        }
    }
}