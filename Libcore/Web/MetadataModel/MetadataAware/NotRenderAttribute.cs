namespace Libcore.Web.MetadataModel.MetadataAware
{
    public class NotRenderAttribute : RenderAttribute
    {
        public NotRenderAttribute()
        {
            ShowForEdit = false;
        }

    }
}