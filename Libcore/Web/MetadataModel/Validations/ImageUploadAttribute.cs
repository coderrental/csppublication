using System;

namespace Libcore.Web.MetadataModel.Validations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ImageUploadAttribute : FileUploadAttribute
    {
        public static readonly string[] DefaultExtensions = { ".gif", ".jpg", ".jpeg", ".png" };

        public ImageUploadAttribute()
        {
            AllowedExtensions = DefaultExtensions;
        }
    }
}