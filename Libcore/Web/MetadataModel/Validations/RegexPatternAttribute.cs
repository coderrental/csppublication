using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Libcore.Web.MetadataModel.Validations
{
    public class RegexPatternAttribute : ValidationAttribute
    {
        public string Pattern { get; }

        public RegexPatternAttribute(string pattern)
        {
            Pattern = pattern;
            ErrorMessage = "Invalid pattern: " + pattern;
        }

        public override bool IsValid(object value)
        {
            return value == null || Regex.IsMatch(value.ToString(), Pattern);
        }
    }
}