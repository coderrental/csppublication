﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Humanizer;
using Libcore.Web.MetadataModel.Filters;
using Libcore.Web.MetadataModel.Localization;

namespace Libcore.Web.MetadataModel
{
    /// <summary>
    /// Custom DataAnnotationsModelMetadataProvider to use custom filters
    /// </summary>
    public class ExtensibleModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        private readonly IModelMetadataFilter[] _metadataFilters;

        public ExtensibleModelMetadataProvider(IModelMetadataFilter[] metadataFilters)
        {
            _metadataFilters = metadataFilters;
        }

        protected override ModelMetadata CreateMetadata(
         IEnumerable<Attribute> attributes,
         Type containerType,
         Func<object> modelAccessor,
         Type modelType,
         string propertyName)
        {
            var attributeList = new List<Attribute>(attributes);
            var metadata = base.CreateMetadata(attributeList, containerType, modelAccessor, modelType, propertyName);

            if (containerType == null || propertyName == null) return metadata;

            TranslateMetadata(metadata, containerType, propertyName);

            TransformMetadata(metadata, attributeList);

            return metadata;
        }

        private void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributeList)
        {
            foreach (var modelMetadataFilter in _metadataFilters)
            {
                modelMetadataFilter.TransformMetadata(metadata, attributeList);
            }
        }

        private void TranslateMetadata(ModelMetadata metadata, Type containerType, string propertyName)
        {
            if (metadata.DisplayName == null)
                metadata.DisplayName = Translate(containerType, propertyName);

            if (metadata.Watermark == null)
                metadata.Watermark = TranslateOrDefault(containerType, propertyName, "Watermark", metadata.DisplayName);
        }

        protected virtual string Translate(Type containerType, string propertyName, string metadataName = null)
        {
            return ModelMetadataLocalizer.IsEnabled
                ? ModelMetadataLocalizer.Current.GetModelString(containerType, propertyName, metadataName) ?? propertyName.Titleize()
                : propertyName.Titleize();
        }

        protected virtual string TranslateOrDefault(Type containerType, string propertyName, string metadataName, string defaultText)
        {
            if (ModelMetadataLocalizer.IsEnabled)
            {
                var text = ModelMetadataLocalizer.Current.GetModelString(containerType, propertyName, metadataName);
                if (string.IsNullOrWhiteSpace(text))
                {
                    text = defaultText;
                }
                return text;
            }
            return defaultText;
        }
    }
}