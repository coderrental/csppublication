﻿using System.Security.Claims;
using Libcore.Web.Extensions;

namespace Libcore.Web.Security
{
    public static class CurrentUserExtensions
    {
        public static string UserName(this ICurrentUser currentUser)
        {
            return currentUser.Principal.GetClaimValue(ClaimTypes.Name);
        }

        public static string GivenName(this ICurrentUser currentUser)
        {
            return currentUser.Principal.GetClaimValue(ClaimTypes.GivenName);
        }

        public static string Email(this ICurrentUser currentUser)
        {
            return currentUser.Principal.GetClaimValue(ClaimTypes.Email);
        }
    }
}
