﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using Libcore.Web.Extensions;

namespace Libcore.Web.Security
{
    public class CurrentUser : ICurrentUser
    {
        public int Id => Principal.GetClaimValue<int>(ClaimTypes.NameIdentifier);
        public IList<string> Roles => Principal.GetRoles();
        public ClaimsPrincipal Principal { get; }

        public CurrentUser()
        {
            Principal = Thread.CurrentPrincipal as ClaimsPrincipal;
        }

        public bool IsInRole(params string[] roles)
        {
            return roles.Any(role => Roles.Contains(role.ToString()));
        }
    }
}

