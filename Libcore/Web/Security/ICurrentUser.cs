using System.Collections.Generic;
using System.Security.Claims;

namespace Libcore.Web.Security
{
    public interface ICurrentUser
    {
        int Id { get; }
        IList<string> Roles { get; }
        bool IsInRole(params string[] roles);
        ClaimsPrincipal Principal { get; }
    }
}