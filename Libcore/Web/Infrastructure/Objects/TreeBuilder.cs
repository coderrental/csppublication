﻿using System.Collections.Generic;
using System.Linq;

namespace Libcore.Web.Infrastructure.Objects
{
    public static class TreeBuilder
    {
        public static IList<T> BuildFlat<T>(IList<T> source) where T : class, IFlatTreeNode
        {
            return BuildFlatRecursive(new List<T>(), source, null, 0);
        }

        private static IList<T> BuildFlatRecursive<T>(IList<T> result, IList<T> allItems, T parent, int level) where T : class, IFlatTreeNode
        {
            var children = allItems
                .Where(x => x.ParentId == parent?.Id)
                .OrderBy(x => x.SortOrder)
                .ToList();
            if (parent != null)
            {
                parent.HasChildren = children.Any();
            }

            foreach (var child in children)
            {
                child.Level = level;
                result?.Add(child);
                BuildFlatRecursive(result, allItems, child, level + 1);
            }
            return result;
        }

        public static IList<T> BuildTree<T>(IList<T> all) where T : class, ITreeNode<T>
        {
            var nodes = new List<T>();
            foreach (var item in all.Where(x => !x.ParentId.HasValue))
            {
                BuildTreeRecursive(all, item);
                nodes.Add(item);
            }
            return nodes;
        }

        private static void BuildTreeRecursive<T>(IList<T> all, T parent) where T : class, ITreeNode<T>
        {
            foreach (var item in all.Where(x => x.ParentId == parent.Id)
                                    .OrderBy(x => x.SortOrder))
            {
                parent.Items.Add(item);
            }

            parent.HasChildren = parent.Items.Any();

            foreach (var item in parent.Items)
            {
                BuildTreeRecursive(all, item);
            }
        }
    }
}
