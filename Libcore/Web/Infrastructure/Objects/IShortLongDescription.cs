﻿namespace Libcore.Web.Infrastructure.Objects
{
    public interface IShortLongDescription
    {
        string ShortDescription { get; set; }
        string LongDescription { get; set; }
    }
}
