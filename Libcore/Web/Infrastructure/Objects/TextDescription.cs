using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace Libcore.Web.Infrastructure.Objects
{
    public class TextDescription
    {
        private const string ReadMore = "<!--more-->";

        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }

        public static TextDescription Parse(string html)
        {
            var content = ParseAndReplace(html);
            var index = content.IndexOf(ReadMore, StringComparison.OrdinalIgnoreCase);
            if (index >= 0)
            {
                return new TextDescription
                {
                    ShortDescription = content.Substring(0, index),
                    LongDescription = content.Substring(index + ReadMore.Length)
                };
            }
            return new TextDescription
            {
                ShortDescription = content,
                LongDescription = string.Empty
            };
        }

        public static string ToFullText(IShortLongDescription text)
        {
            if (string.IsNullOrEmpty(text.ShortDescription))
            {
                return text.LongDescription;
            }
            if (string.IsNullOrEmpty(text.LongDescription))
            {
                return text.ShortDescription;
            }

            return string.Format("{0}<hr id='system-readmore'/>{1}", text.ShortDescription, text.LongDescription);
        }

        private static string ParseAndReplace(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var node = doc.QuerySelector("hr#system-readmore");
            var images = doc.QuerySelectorAll("img");
            foreach (var image in images)
            {
                if (!image.Attributes.Contains("class"))
                {
                    image.Attributes.Append("class");
                }
                var cssClasses =
                    new HashSet<string>(image.Attributes["class"].Value.Split(' ').Select(x => x.Trim()))
                    {
                        "img-responsive"
                    };
                image.Attributes["class"].Value = string.Join(" ", cssClasses);
            }

            if (node == null)
            {
                return doc.DocumentNode.InnerHtml;
            }

            if (node.ParentNode == doc.DocumentNode)
            {
                // does not containt wrapper
                doc.DocumentNode.ReplaceChild(doc.CreateComment(ReadMore), node);
            }
            else
            {
                // get parent near <body>
                var currentNode = node;
                while (currentNode.ParentNode != doc.DocumentNode)
                {
                    currentNode = currentNode.ParentNode;
                }

                doc.DocumentNode.InsertBefore(doc.CreateComment(ReadMore), currentNode);
                if (node.ParentNode.ChildNodes.Count == 1)
                {
                    node.ParentNode.Remove();
                }
                else
                {
                    node.Remove();
                }
            }
            return doc.DocumentNode.InnerHtml;
        }
    }
}