﻿namespace Libcore.Web.Infrastructure.Objects
{
    public interface IFlatTreeNode
    {
        int Id { get; set; }
        int? ParentId { get; set; }
        string Text { get; set; }
        int SortOrder { get; set; }
        int Level { get; set; }
        bool HasChildren { get; set; }
    }
}
