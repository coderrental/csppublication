﻿using System.Collections.Generic;

namespace Libcore.Web.Infrastructure.Objects
{
    public interface ITreeNode<T> : IFlatTreeNode
    {
        IList<T> Items { get; set; }
    }
}
