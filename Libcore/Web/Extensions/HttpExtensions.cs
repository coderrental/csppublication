﻿using System;
using System.Net.Http;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Libcore.Web.Extensions
{
    public static class HttpExtensions
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            return string.Empty;
        }

        public static void AddContentDispositionHeader(this HttpResponseBase response, string fileName, bool inline = false)
        {
            var cd = new ContentDisposition
            {
                FileName = fileName,
                Inline = inline
            };

            response.Headers.Add("Content-Disposition", cd.ToString());
        }

        public static string AuthorityUrl(this ViewContext context)
        {
            return context.RequestContext.HttpContext.AuthorityUrl();
        }

        public static string AuthorityUrl(this HttpContextBase context)
        {
            return context.Request.AuthorityUrl();
        }

        public static string AuthorityUrl(this HttpRequestBase request)
        {
            return request.Url?.GetLeftPart(UriPartial.Authority);
        }

        public static object GetRouteValue(this HttpContextBase httpContext, string key)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            return httpContext.Request.RequestContext.RouteData.Values[key];
        }
    }
}
