﻿using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using Libcore.Dependency.Fake;
using Libcore.Web.MetadataModel;
using Libcore.Web.MetadataModel.Filters;
using SimpleInjector;

namespace Libcore.Web.Extensions
{
    public static class ContainerExtensions
    {
        [DebuggerStepThrough]

        public static void RegisterHttpContext(this Container container)
        {
            container.Register<HttpContextBase>(
                instanceCreator: () =>
                {
                    if (container.IsVerifying) return new FakeHttpContext();
                    return new HttpContextWrapper(HttpContext.Current);
                }, lifestyle: Lifestyle.Scoped);

            container.Register<HttpSessionStateBase>(
                instanceCreator: () =>
                {
                    if (container.IsVerifying) return new FakeHttpSessionState();
                    return new HttpSessionStateWrapper(HttpContext.Current.Session);
                }, lifestyle: Lifestyle.Scoped);
        }

        [DebuggerStepThrough]

        public static void RegisterExtensibleModelMetadataProvider(this Container container)
        {
            container.Collection.Register<IModelMetadataFilter>(typeof(IModelMetadataFilter).Assembly);
            container.Register<ModelMetadataProvider, ExtensibleModelMetadataProvider>();
        }
    }
}