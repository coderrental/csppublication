﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using Libcore.Core.Timing;

namespace Libcore.Web.Extensions
{
    public static class PathExtensions
    {
        public static string ToUniqueFileName(this HttpPostedFileBase file)
        {
            return $"{Path.GetFileNameWithoutExtension(file.FileName)}-{Clock.Now:yyMMddHHmmss}{Path.GetExtension(file.FileName)}";
        }

        public static string ToAbsolutePath(this string path)
        {
            return HostingEnvironment.MapPath(path);
        }
    }
}
