﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Libcore.Web.Extensions
{
    public static class HtmlBlockExtensions
    {
        internal const string Key = "__HtmlBlocks";

        private static IList<string> GetBlocks(this HtmlHelper htmlHelper)
        {
            if (!(htmlHelper.ViewContext.HttpContext.Items[Key] is IList<string> list))
            {
                list = new List<string>();
                htmlHelper.ViewContext.HttpContext.Items[Key] = list;
            }
            return list;
        }

        public static MvcHtmlString AddHtmlBlock(this HtmlHelper htmlHelper, string template)
        {
            var blocks = htmlHelper.GetBlocks();
            blocks.Add(template.Trim(' ', '\r', '\n'));
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AddHtmlBlock(this HtmlHelper htmlHelper, Func<dynamic, HelperResult> template)
        {
            return htmlHelper.AddHtmlBlock(template(null).ToString());
        }

        public static IHtmlString RenderHtmlBlocks(this HtmlHelper htmlHelper)
        {
            if (htmlHelper.ViewContext.HttpContext.Items[Key] is IList<string> list &&
                list.Any())
            {
                return new HtmlString(string.Join("", list));
            }
            return MvcHtmlString.Empty;
        }
    }
}
