﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Libcore.Core.Exceptions;
using Libcore.Core.Extensions;
using Microsoft.AspNet.Identity;

namespace Libcore.Web.Extensions
{
    public static class ModelStateExtensions
    {
        public static void AddException(this ModelStateDictionary dict, Exception ex)
        {
            string message;
            if (ex is BusinessException)
            {
                message = ex.Message;
            }
            else
            {
                message = ex.GetBaseErrorMessage();
            }
            dict.AddModelError("", message);
        }

        public static bool IsModelValid(this object source)
        {
            var validationResults = new List<ValidationResult>();
            return source.IsModelValid(validationResults);
        }

        public static bool IsModelValid(this object source, List<ValidationResult> results)
        {
            var context = new ValidationContext(source);
            return Validator.TryValidateObject(source, context, results, true);
        }

        public static void AddIdentityResultErrors(this ModelStateDictionary modelState, IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                modelState.AddModelError("", error);
            }
        }
    }
}
