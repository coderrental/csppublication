﻿using System.Linq;
using System.Security.Claims;
using Libcore.Core.Extensions;

namespace Libcore.Web.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string[] GetRoles(this ClaimsPrincipal claimsPrincipal)
        {
            var claimsIdentity = claimsPrincipal?.Identity as ClaimsIdentity;
            return claimsIdentity?.Claims.Where(c => c.Type == ClaimTypes.Role).Select(x => x.Value).ToArray() ?? new string[0];
        }

        public static T GetClaimValue<T>(this ClaimsPrincipal claimsPrincipal, string claimType)
        {
            return claimsPrincipal.GetClaimValue(claimType).ChangeTypeTo<T>(default(T));
        }

        public static string GetClaimValue(this ClaimsPrincipal claimsPrincipal, string claimType)
        {
            var claimsIdentity = claimsPrincipal?.Identity as ClaimsIdentity;
            return claimsIdentity?.Claims.FirstOrDefault(c => c.Type == claimType)?.Value;
        }
    }
}
