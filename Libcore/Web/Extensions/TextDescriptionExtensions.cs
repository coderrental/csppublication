﻿using Libcore.Core.Infrastructure.Objects;
using Libcore.Web.Infrastructure.Objects;

namespace Libcore.Web.Extensions
{
    public static class TextDescriptionExtensions
    {
        public static void SetDescription(this IShortLongDescription source, string html)
        {
            var result = TextDescription.Parse(html);
            source.ShortDescription = result.ShortDescription;
            source.LongDescription = result.LongDescription;
        }

        public static string ToFullText(this IShortLongDescription source)
        {
            return TextDescription.ToFullText(source);
        }
    }
}
