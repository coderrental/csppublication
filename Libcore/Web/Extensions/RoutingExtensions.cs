﻿using System.Web.Mvc;
using System.Web.Routing;
using Libcore.Web.Mvc.Routing;

namespace Libcore.Web.Extensions
{
    public static class RoutingExtensions
    {
        public static RouteValueDictionary SetIfNotNull(this RouteValueDictionary dictionary, string key, object value)
        {
            if (value != null)
            {
                dictionary[key] = value;
            }
            return dictionary;
        }

        public static RouteCollection FriendlyRoute(this RouteCollection routes, string name, string url, object defaults)
        {
            routes.Add(name, new FriendlyRoute(url,
                new RouteValueDictionary(defaults),
                new MvcRouteHandler()));
            return routes;
        }
    }
}
