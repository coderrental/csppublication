using AutoMapper;

namespace Libcore.AutoMapper
{
    public interface ICustomMappings
    {
        void CreateMappings(IMapperConfigurationExpression configuration);

    }
}