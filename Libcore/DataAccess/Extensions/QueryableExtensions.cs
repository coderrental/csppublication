﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Libcore.DataAccess.Repositories;

namespace Libcore.DataAccess.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> IncludeIf<T, TProperty>(this IQueryable<T> source, bool condition, Expression<Func<T, TProperty>> path)
            where T : class
        {
            return condition
                ? source.Include(path)
                : source;
        }

        public static IQueryable<T> IncludeIf<T>(this IQueryable<T> source, bool condition, string path) where T : class
        {
            return condition
               ? source.Include(path)
               : source;
        }

        public static IQueryable<T> AsNoTracking<T>(this IRepository<T> source)
            where T : class
        {
            return source.Queryable().AsNoTracking();
        }
    }
}
