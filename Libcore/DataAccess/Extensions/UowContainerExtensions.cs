﻿using Libcore.DataAccess.Context;
using Libcore.DataAccess.Identity;
using Libcore.DataAccess.Uow;
using SimpleInjector;

namespace Libcore.DataAccess.Extensions
{
    public static class UowContainerExtensions
    {
        public static Container RegisterDefaultUnitOfWork<TContext>(this Container services)
            where TContext : DataContext
        {
            services.Register<IUnitOfWork, UnitOfWork<TContext>>(Lifestyle.Scoped);
            return services;
        }

        public static Container RegisterUnitOfWork<TContext>(this Container services)
            where TContext : DataContext
        {
            services.Register<IUnitOfWork<TContext>, UnitOfWork<TContext>>(Lifestyle.Scoped);
            return services;
        }

        public static Container RegisterDefaultUnitOfWork<TContext, TUser, TRole>(this Container services)
            where TContext : IdentityDataContext<TUser, TRole>
            where TUser : IdentityUserInt
            where TRole : IdentityRoleInt
        {
            services.Register<IUnitOfWork, UnitOfWork<TContext, TUser, TRole>>(Lifestyle.Scoped);
            return services;
        }

        public static Container RegisterUnitOfWork<TContext, TUser, TRole>(this Container services)
            where TContext : IdentityDataContext<TUser, TRole>
            where TUser : IdentityUserInt
            where TRole : IdentityRoleInt
        {
            services.Register<IUnitOfWork<TContext, TUser, TRole>, UnitOfWork<TContext, TUser, TRole>>(Lifestyle.Scoped);
            return services;
        }

    }
}
