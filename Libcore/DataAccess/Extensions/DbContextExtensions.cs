﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Reflection;
using Libcore.Core.Timing;
using Libcore.DataAccess.Entities;

namespace Libcore.DataAccess.Extensions
{
    internal static class DbContextExtensions
    {
        public static void PostCommit(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries())
            {
                var entity = entry.Entity as IObjectState;
                if (entity == null) continue;
                entity.ObjectState = StateConverter.ToObjectState(entry.State);
            }
        }

        public static void PreCommit(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries())
            {
                SyncEntityState(entry);
                AuditEntity(context, entry);
            }
        }

        private static void SyncEntityState(DbEntityEntry entry)
        {
            if (!(entry.Entity is IObjectState entity)) return;
            entry.State = StateConverter.ToEntityState(entity.ObjectState, entry.State);
        }

        private static void AuditEntity(DbContext context, DbEntityEntry entry)
        {
            if (entry.State == EntityState.Deleted) return;
            switch (entry.State)
            {
                case EntityState.Added:
                    SetCreationProperties(entry);
                    break;
                case EntityState.Modified:
                    SetUpdatedProperties(context, entry);
                    break;
            }
        }

        private static void SetCreationProperties(DbEntityEntry entry)
        {
            if (entry.Entity is IHasCreationTime creationTimeEntity)
            {
                creationTimeEntity.CreatedDate = Clock.Now;
            }

            if (entry.Entity is IHasModificationTime modificationTimeEntity)
            {
                modificationTimeEntity.UpdatedDate = Clock.Now;
            }

            if (entry.Entity is ICreationAudited creationAuditedEntity)
            {
                SetAuditedUserProperties(creationAuditedEntity, x => x.CreatedBy);
            }

            if (entry.Entity is IModificationAudited modificationAuditedEntity)
            {
                SetAuditedUserProperties(modificationAuditedEntity, x => x.UpdatedBy, modificationAuditedEntity.CreatedBy);
            }
        }

        private static void SetUpdatedProperties(DbContext context, DbEntityEntry entry)
        {
            if (entry.Entity is IHasModificationTime timeEntity)
            {
                timeEntity.UpdatedDate = Clock.Now;
                context.Entry(entry.Entity).Property("CreatedDate").IsModified = false;
            }

            if (entry.Entity is IModificationAudited auditedEntity)
            {
                auditedEntity.UpdatedDate = Clock.Now;
                //modificationAuditedEntity.UpdatedBy = UserSessions.UserId.GetValueOrDefault();

                context.Entry(entry.Entity).Property("CreatedDate").IsModified = false;
                context.Entry(entry.Entity).Property("CreatedBy").IsModified = false;
            }
        }

        private static void SetAuditedUserProperties<T>(T target, Expression<Func<T, int>> memberLamda, int? value = null)
        {
            if (!(memberLamda.Body is MemberExpression expr)) return;
            var property = expr.Member as PropertyInfo;
            if (property == null) return;

            if (!property.GetValue(target).Equals(0)) return;

            if (!value.HasValue)
            {
                //value = UserSessions.UserId.GetValueOrDefault();
            }
            property.SetValue(target, value, null);
        }
    }
}
