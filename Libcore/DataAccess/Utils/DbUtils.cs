﻿using System;
using Libcore.DataAccess.Context;
using Libcore.DataAccess.Entities;
using Libcore.DataAccess.Identity;
using Libcore.DataAccess.Repositories;

namespace Libcore.DataAccess.Utils
{
    public static class DbUtils
    {
        public static T CreateExtendedRepo<T>(DataContext dataContext) where T : class
        {
            return (T)Activator.CreateInstance(typeof(T), dataContext);
        }

        public static IRepository<T> CreateRepo<T>(DataContext dataContext) where T : EntityBase
        {
            return new Repository<T>(dataContext);
        }

        public static IRepository<T> CreateRepo<T, TUser, TRole>(IdentityDataContext<TUser, TRole> dataContext)
            where T : EntityBase
            where TUser : IdentityUserInt
            where TRole : IdentityRoleInt
        {
            return new Repository<T>(dataContext);
        }

        public static T CreateExtendedRepo<T, TUser, TRole>(IdentityDataContext<TUser, TRole> dataContext)
            where T : class
            where TUser : IdentityUserInt
            where TRole : IdentityRoleInt
        {
            return (T)Activator.CreateInstance(typeof(T), dataContext);
        }
    }
}
