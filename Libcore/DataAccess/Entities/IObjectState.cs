﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Libcore.DataAccess.Entities
{
    public interface IObjectState
    {
        [NotMapped]
        ObjectState ObjectState { get; set; }
    }
}