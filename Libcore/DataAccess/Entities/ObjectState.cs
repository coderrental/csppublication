﻿namespace Libcore.DataAccess.Entities
{
    public enum ObjectState
    {
        NotSet,
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}