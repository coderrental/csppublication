using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Libcore.DataAccess.Entities
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }

    public abstract class EntityBase : IObjectState
    {
        [NotMapped]
        [JsonIgnore]
        public ObjectState ObjectState { get; set; }

        public void MarkAsDeleted()
        {
            ObjectState = ObjectState.Deleted;
        }
    }

    public abstract class Entity<T> : EntityBase, IEntity<T>
    {
        public virtual T Id { get; set; }
    }
}
