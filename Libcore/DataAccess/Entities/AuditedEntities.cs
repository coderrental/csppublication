﻿using System;

namespace Libcore.DataAccess.Entities
{
    public interface IHasCreationTime
    {
        DateTime CreatedDate { get; set; }
    }

    public interface IHasModificationTime : IHasCreationTime
    {
        DateTime UpdatedDate { get; set; }
    }

    public interface ICreationAudited : IHasCreationTime
    {
        int CreatedBy { get; set; }
    }

    public interface IModificationAudited : ICreationAudited, IHasModificationTime
    {
        int UpdatedBy { get; set; }
    }

    public abstract class CreationTimeEntity<T> : Entity<T>, IHasCreationTime
    {
        public DateTime CreatedDate { get; set; }
    }

    public abstract class ModificationTimeEntity<T> : CreationTimeEntity<T>, IHasModificationTime
    {
        public DateTime UpdatedDate { get; set; }
    }

    public abstract class CreationAuditedEntity<T> : CreationTimeEntity<T>
    {
        public int CreatedBy { get; set; }
    }

    public abstract class ModificationAuditedEntity<T> : ModificationTimeEntity<T>
    {
        public int UpdatedBy { get; set; }
    }

}
