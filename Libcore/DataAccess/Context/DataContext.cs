using System.Data.Entity;
using System.Threading.Tasks;
using Libcore.DataAccess.Extensions;

namespace Libcore.DataAccess.Context
{
    public abstract class DataContext : DbContext
    {
        protected DataContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public override int SaveChanges()
        {
            this.PreCommit();
            var result = base.SaveChanges();
            this.PostCommit();
            return result;
        }

        public override Task<int> SaveChangesAsync()
        {
            this.PreCommit();
            var result = base.SaveChangesAsync();
            this.PostCommit();
            return result;
        }
    }
}