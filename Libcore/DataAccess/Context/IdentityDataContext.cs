using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Libcore.DataAccess.Extensions;
using Libcore.DataAccess.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Libcore.DataAccess.Context
{
    public abstract class IdentityDataContext<TUser, TRole> : IdentityDbContext<TUser, TRole, int, IdentityUserLoginInt, IdentityUserRoleInt, IdentityUserClaimInt>
        where TUser : IdentityUserInt
        where TRole : IdentityRoleInt
    {
        protected IdentityDataContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public override int SaveChanges()
        {
            this.PreCommit();
            var result = base.SaveChanges();
            this.PostCommit();
            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            this.PreCommit();
            var result = base.SaveChangesAsync(cancellationToken);
            this.PostCommit();
            return result;
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityUserRoleInt>().ToTable("UserRole");
            builder.Entity<IdentityUserLoginInt>().ToTable("UserLogin");
            builder.Entity<IdentityUserClaimInt>().ToTable("UserClaim");
        }
    }
}