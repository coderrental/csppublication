using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Libcore.DataAccess.Entities;
using Z.EntityFramework.Plus;

namespace Libcore.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IObjectState
    {
        protected DbContext DbContext { get; }
        protected virtual DbSet<T> DbSet { get; }

        public Repository(DbContext dataContext)
        {
            DbContext = dataContext;
            DbSet = DbContext.Set<T>();
        }

        public virtual T Get(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public virtual Task<T> GetAsync(params object[] keyValues)
        {
            return DbSet.FindAsync(keyValues);
        }

        public virtual List<T> GetAll()
        {
            return DbSet.AsNoTracking().ToList();
        }

        public virtual Task<List<T>> GetAllAsync()
        {
            return DbSet.AsNoTracking().ToListAsync();
        }

        public virtual IQueryable<T> Queryable()
        {
            return DbSet;
        }

        public IQueryable<T> AsNoFilter()
        {
            return DbSet.AsNoFilter();
        }

        public virtual T Single(Expression<Func<T, bool>> predicate)
        {
            return DbSet.SingleOrDefault(predicate);
        }

        public virtual Task<T> SingleAsync(Expression<Func<T, bool>> predicate)
        {
            return DbSet.SingleOrDefaultAsync(predicate);
        }

        public virtual T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        public virtual Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefaultAsync(predicate);
        }

        public virtual int Count()
        {
            return DbSet.Count();
        }

        public virtual Task<int> CountAsync()
        {
            return DbSet.CountAsync();
        }

        public virtual int Count(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).Count();
        }

        public virtual Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).CountAsync();
        }

        public virtual bool Any(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Any(predicate);
        }

        public virtual Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return DbSet.AnyAsync(predicate);
        }

        public void Insert(T entity)
        {
            entity.ObjectState = ObjectState.Added;
            DbSet.Add(entity);
        }

        public void Insert(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public void Update(T entity)
        {
            AttachIfNot(entity);
            entity.ObjectState = ObjectState.Modified;
        }

        public void Update(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public void Delete(T entity)
        {
            AttachIfNot(entity);
            entity.ObjectState = ObjectState.Deleted;
            DbSet.Remove(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        protected virtual void AttachIfNot(T entity)
        {
            var entry = DbContext?.ChangeTracker.Entries().FirstOrDefault(ent => ent.Entity == entity);
            if (entry != null)
            {
                return;
            }
            DbSet.Attach(entity);
        }
    }
}