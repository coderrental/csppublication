using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Libcore.DataAccess.Identity
{
    public abstract class RoleStoreInt<TRole> : RoleStore<TRole, int, IdentityUserRoleInt>
        where TRole : IdentityRoleInt, new()
    {
        protected RoleStoreInt(DbContext context)
            : base(context)
        {
        }
    }
}