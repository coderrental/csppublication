using System.ComponentModel.DataAnnotations.Schema;
using Libcore.DataAccess.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Libcore.DataAccess.Identity
{
    public abstract class IdentityUserInt : IdentityUser<int, IdentityUserLoginInt, IdentityUserRoleInt, IdentityUserClaimInt>, IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}