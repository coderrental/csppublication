using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Libcore.DataAccess.Identity
{
    public abstract class UserStoreInt<TUser, TRole> : UserStore<TUser, TRole, int, IdentityUserLoginInt, IdentityUserRoleInt, IdentityUserClaimInt>
        where TUser : IdentityUserInt
        where TRole : IdentityRoleInt
    {
        protected UserStoreInt(DbContext context)
            : base(context)
        {
        }
    }
}