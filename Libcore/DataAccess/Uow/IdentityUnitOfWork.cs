﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Libcore.DataAccess.Context;
using Libcore.DataAccess.Entities;
using Libcore.DataAccess.Identity;
using Libcore.DataAccess.Repositories;
using Libcore.DataAccess.Utils;

namespace Libcore.DataAccess.Uow
{
    public interface IUnitOfWork<out TContext, TUser, TRole> : IUnitOfWork
        where TContext : IdentityDataContext<TUser, TRole>
        where TUser : IdentityUserInt
        where TRole : IdentityRoleInt
    {
        TContext DataContext { get; }
    }

    public class UnitOfWork<TContext, TUser, TRole> : IUnitOfWork<TContext, TUser, TRole>
       where TContext : IdentityDataContext<TUser, TRole>
       where TUser : IdentityUserInt
       where TRole : IdentityRoleInt
    {
        private bool _disposed;
        private readonly object _lock = new object();

        private Dictionary<Type, dynamic> _repositories;
        private Dictionary<Type, dynamic> Repositories => _repositories ?? (_repositories = new Dictionary<Type, dynamic>());
        public TContext DataContext { get; private set; }
        private DbContextTransaction _transaction;

        public UnitOfWork(TContext dataContext)
        {
            DataContext = dataContext;
        }

        public T ExtendedRepo<T>() where T : class
        {
            return AddOrGetRepo(() => DbUtils.CreateExtendedRepo<T, TUser, TRole>(DataContext));
        }

        public IRepository<T> Repo<T>() where T : EntityBase
        {
            return AddOrGetRepo(() => DbUtils.CreateRepo<T, TUser, TRole>(DataContext));
        }

        public int SaveChanges()
        {
            return DataContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return DataContext.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return DataContext.SaveChangesAsync(cancellationToken);
        }

        private T AddOrGetRepo<T>(Func<T> func) where T : class
        {
            var type = typeof(T);
            if (!Repositories.ContainsKey(type))
            {
                lock (_lock)
                {
                    if (!Repositories.ContainsKey(type))
                    {
                        Repositories.Add(type, func());
                    }
                }
            }
            return Repositories[type];
        }

        #region Unit of Work Transactions

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            if (DataContext is DbContext dbContext)
            {
                _transaction = dbContext.Database.BeginTransaction(isolationLevel);
            }
        }

        public void Commit()
        {
            _transaction?.Commit();
        }

        public void Rollback()
        {
            _transaction?.Rollback();
        }

        #endregion

        #region Destructors

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _repositories?.Clear();
                    DataContext.Dispose();
                    DataContext = null;
                }
                _disposed = true;
            }
        }
        #endregion

    }

}