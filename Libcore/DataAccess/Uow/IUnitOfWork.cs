using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Libcore.DataAccess.Entities;
using Libcore.DataAccess.Repositories;

namespace Libcore.DataAccess.Uow
{
    public interface IUnitOfWork : IDisposable
    {
        T ExtendedRepo<T>() where T : class;
        IRepository<T> Repo<T>() where T : EntityBase;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
        void Commit();
        void Rollback();
    }
}